select * from memodoc.reserva where id_cliente= 674;
select * from memodoc.os where id_cliente= 674;

select * from memodoc.checkin where dt_checkin is null;
select id_caixa from memodoc.checkin where id_os_chave = 57849;
update memodoc.documento set emcasa = 1 , primeira_entrada = '2017-01-15' where id_caixa in (select id_caixa from memodoc.checkin where id_os_chave = 57849);

update memodoc.cliente set fl_ativo = 0 where id_cliente <> 674;
update memodoc.checkin set dt_checkin = '2017-01-15' where dt_checkin is null;

-- Primera OS de acervo 30 caixas
update memodoc.reserva set data_reserva = '2016-06-15' where id_reserva = 10443;
update memodoc.documento set emcasa = 1, primeira_entrada = '2016-06-15' where id_reserva = 10443;
update memodoc.os set entregar_em = '2016-06-15' where id_os_chave = 57847; -- antiga 

-- Caixas não usadas mas cobradas por estarem além da carência (20 caixas)
update memodoc.reserva set data_reserva = '2017-01-15' where id_reserva = 10441;
update memodoc.documento set emcasa = 0, primeira_entrada = null where id_reserva = 10442;
update memodoc.os set entregar_em = '2017-01-15' where id_os_chave = 57846;

-- Caixas não usadas mas cobradas por estarem além da carência (20 caixas)
update memodoc.reserva set data_reserva = '2016-09-15' where id_reserva = 10441;
update memodoc.documento set emcasa = 0, primeira_entrada = null where id_reserva = 10441;
update memodoc.os set entregar_em = '2016-09-15' where id_os_chave = 57845; -- antiga 

-- Checkouts
update memodoc.osdesmembrada set data_checkout = '2017-02-15',  status = 1 where id_os_chave = 57848;
update memodoc.osdesmembrada set data_checkout = '2017-02-15',  status = 1 where id_os_chave = 57849;
update memodoc.checkin set dt_checkin = '2017-01-15' where id_os_chave = 57849;
update memodoc.osdesmembrada set data_checkout = '2017-02-15',  status = 1 where id_os_chave = 57850;

-- Primera OS de acervo 30 caixas
update memodoc.reserva set data_reserva = '2016-06-15' where id_reserva = 10446;
update memodoc.documento set emcasa = 1, primeira_entrada = '2016-06-15' where id_reserva = 10446;
update memodoc.os set entregar_em = '2016-06-15' where id_os_chave = 57854; -- antiga 

-- Caixas não usadas mas cobradas por estarem além da carência (20 caixas)
update memodoc.reserva set data_reserva = '2016-09-15' where id_reserva = 10447;
update memodoc.documento set emcasa = 0, primeira_entrada = null where id_reserva = 10447;
update memodoc.os set entregar_em = '2016-09-15' where id_os_chave = 57855; -- antiga 


-- Caixas não cobrada por estarem na carencia (10 caixas)
update memodoc.reserva set data_reserva = '2017-01-15' where id_reserva = 10448;
update memodoc.documento set emcasa = 0, primeira_entrada = null where id_reserva = 10448;
update memodoc.os set entregar_em = '2017-01-15' where id_os_chave = 57856;
