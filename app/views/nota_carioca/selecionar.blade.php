@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Gerar notas fiscais</h1></div>
    </header> <!--/ content header -->

    <!-- content page -->
    <article class="content-page clearfix">
        <!-- main page -->
        <div class="main-page" id="">
            <div class="content-inner">
                <!-- search box -->
                {{Form::open(array('class' => 'form-horizontal'))}}
                <div class="widget border-cyan">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-archive"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Informe o período de faturamento</h4>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content bg-silver">
                        <div class="control-group">
                            <div class='input-append'>
                               <input type="submit" class="btn bg-cyan" class="button" id="adicionaCaixa" value="Recuperar"/>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}

        @include('_layout.msg')

        @if(isset($faturaveis))

                <div class="widget border-green">
                    <!-- widget header -->
                    <div class="widget-header bg-green">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-reorder"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Clientes que serão faturados</h4>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        <table class='table table-striped table-hover table-bordered table-condensed table-menor'>
                            <thead>
                                <tr>
                                    <th style="width:50px; text-align: center;">Ação</th>
                                    <th style="width:auto; text-align: center;">ID</th>
                                    <th style="width:auto; ">Cliente</th>
                                    <th style="width:80px;text-align: center;">Início do contrato</th>
                                    <th style="width:80px;text-align: center;">Último reajuste</th>
                                    <th>Valor total (R$)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($faturaveis as $cliente)
                                    <tr>
                                        <td style="text-align: center;">
                                            <a href="/notacarioca/suspendecliente/{{ $cliente->id_cliente}}/" title="Retirar cliente" class="btn btn-small bg-red"><i class="aweso-remove"></i></a>
                                        <td style="text-align: center;">{{ $cliente->id_cliente }}</td>
                                        <td style="">{{ $cliente->razaosocial }} (<span style="font-size:10px;">{{ $cliente->nomefantasia }}</span>)</td>
                                        <td style="text-align: center;">{{ $cliente->iniciocontrato}}</td>
                                        <td style="text-align: center;">{{ $cliente->ultimoreajuste() }}</td>
                                        <td style="text-align: right">&nbsp;</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
        @endif

        @if(isset($naofaturaveis))
                <div class="widget border-red">
                    <!-- widget header -->
                    <div class="widget-header bg-red">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-reorder"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Clientes que NÃO serão faturados</h4>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        <table class='table table-striped table-hover table-bordered table-condensed table-menor'>
                            <thead>
                                <tr>
                                    <th style="width:50px; text-align: center;">Ação</th>
                                    <th style="width:auto; ">Cliente</th>
                                    <th style="width:80px;text-align: center;">Início do contrato</th>
                                    <th style="width:80px;text-align: center;">Último reajuste</th>
                                    <th>Valor total (R$)</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach($naofaturaveis as $cliente)
                                    <tr>
                                        <td style="text-align: center;">
                                            <a href="/notacarioca/suspendecliente/{{ $cliente->id_cliente}}/" title="Retirar cliente" class="btn btn-small bg-red"><i class="aweso-remove"></i></a>
                                        <td style="">{{ $cliente->razaosocial }} <br/><span style="font-size:10px;">{{ $cliente->nomefantasia }}</span></td></td>
                                        <td style="text-align: center;">{{ $cliente->iniciocontrato}}</td>
                                        <td style="text-align: center;">{{ $cliente->ultimoreajuste() }}</td>
                                        <td style="text-align: right">&nbsp;</td>
                                        </tr>
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
        @endif

            </div>
        </div>
    </article> <!-- /content page -->
@stop

@section('scripts')
<script>
  $(function(){
    $departamento = $('#id_departamento');
    $setor = $('#id_setor');
    $centrocusto = $('#id_centro');
    $cliente.change(function(){

       $('.load').show();
       $('#titulo').focus();
       $('.documentos').hide();

       // limpando combos
       $departamento.html('');
       $setor.html('');
       $centrocusto.html('');

       // ajax departamento
       $.get('publico/documento/ajaxdepartamento/'+$(this).val(),function(combo){
           $departamento.html(combo);
       });
       // ajax centro custo
       $.get('publico/documento/ajaxcentrocusto/'+$(this).val(),function(combo){
           $centrocusto.html(combo);
       });
       // ajax setor
       $.get('publico/documento/ajaxsetor/'+$(this).val(),function(combo){
           $setor.html(combo);
       });

       // Remove o load após 2.5 segundos
       setInterval(function(){
           $('.load').hide();
       },2500);

    }).focus();
  });
</script>
@stop