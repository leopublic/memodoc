@extends('stilearn-metro')


@section('conteudo') 
        @include('cliente_tab_header')
        
        <!-- #tab_emails - Begin Content  -->
         <div class="tab-pane" id="tab_EmailCliente">
            <?php
            
               $hasmany = array('title' => 'Emails',
                                'fields' => array(
                                                    'correioeletronico' => 'Correio eletrônico',
                                                    'descricao_tipo_email' => 'Tipo',
                                                 )          
                                ); 
            ?>
            @include('padrao/hasmany_list', $hasmany)
         </div>
        <!-- #tab_emails - End Content  -->  
        @include('cliente_tab_footer')
@stop
