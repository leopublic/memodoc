@extends('stilearn-metro')

@section('conteudo')

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Monitoramento da franquia de custódia</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-search"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Pesquisar</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                    {{ Form::open(array('class'=> 'form-horizontal')) }}
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label">Razão social ou fantasia</label>
                                <div class="controls">
                                    {{Form::text('search', Input::old('search'),array('class'=>'input-block-level'))}}
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="fl_ativo">Status</label>
                                <div class="controls">
                                    <label class="radio">
                                        {{ Form::radio('fl_ativo', '1', (Input::old('fl_ativo') == '1' || Input::old('fl_ativo') == '') ? true : false, array('id'=>'1')) }}
                                        somente ativos
                                    </label>
                                    <label class="radio">
                                        {{ Form::radio('fl_ativo', '0', (Input::old('status') == '0') ? true : false, array('id'=>'0')) }}
                                        somente inativos
                                    </label>
                                    <label class="radio">
                                        {{ Form::radio('fl_ativo', '-1', (Input::old('fl_ativo') == '-1') ? true : false, array('id'=>'-1')) }}
                                        todos
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" class="btn bg-orange">Buscar</button>
                        <input type="submit" name="xls"  value="XLS" class="btn bg-lime"/>
                        <button type="reset" class="btn">Limpar</button>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
            <!-- List -->
            <table class="listagem table table-striped table-condensed" data-sorter="true" style="width:100%;">
                <colgroup>
                    <col width="50px"/>
                    <col width="50px"/>
                    <col width="50px"/>
                    <col width="auto"/>
                    <col width="100px"/>
                    <col width="100px"/>
                    <col width="auto"/>
                </colgroup>
                <thead>
                    <tr>
                        <th style="text-align:center;">ID</th>
                        <th style="text-align: center;">Situação</th>
                        <th style="text-align: center;">Inadimpl.</th>
                        <th>Razão social<br><span style="font-size:10px;">Nome fantasia</span></th>
                        <th style="text-align:center;">Início contrato</th>
                        <th  style="text-align:right;">Franquia custódia</th>
                        <th  style="text-align:left;">Observação</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clientes as $r)
                    @if ($r->franquiacustodia ==0)
                    <tr class="warning">
                    @else
                    <tr>
                    @endif
                        <td style="text-align:center;">{{$r->id_cliente}}</td>
                        <td style="text-align:center;">
                            @if($r->fl_ativo)
                            ativo
                            @else
                            inativo
                            @endif
                        </td>
                        <td style="text-align:center;">
                            @if($r->nivel_inadimplencia == '')
                            em dia
                            @elseif ($r->nivel_inadimplencia == '1')
                            notificação
                            @else 
                            bloqueado
                            @endif
                        </td>
                        <td><a href="/cliente/contrato/{{$r->id_cliente}}#tab_contrato" target="_blank" title="Clique para acessar a aba 'Contrato' do cliente" >{{$r->razaosocial }}<br/><span style="font-size:10px;">{{$r->nomefantasia }}</span></a></td>
                        <td style="text-align:right;">{{$r->iniciocontrato_fmt}}</td>
                        <td style="text-align:right;">{{$r->franquiacustodia}}</td>
                        <td style="text-align:left;">{{$r->obs}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .c{
        width:200px;
        text-align:center;
    }
</style>
@stop