<html>
    <head>
        <style>
            table tr td {border: thin solid #000; vertical-align: top;}
            table tr th {border: thin solid #000; vertical-align: middle; }
        </style>
            
    </head>
<body>
<table>
    <colgroup>
        <col width="50px"/>
        <col width="50px"/>
        <col width="auto"/>
        <col width="100px"/>
        <col width="100px"/>
        <col width="100px"/>
        <col width="100px"/>
        <col width="100px"/>
        <col width="100px"/>
        <col width="200px"/>
    </colgroup>
    <thead>
        <tr>
            <th rowspan="2" style="text-align:center;">ID</th>
            <th rowspan="2" style="text-align: center;">Situação</th>
            <th rowspan="2" style="text-align: center;">Inadimplência</th>
            <th rowspan="2" >Razão social</th>
            <th rowspan="2" >Nome fantasia</th>
            <th colspan="4" style="text-align:center;">Caixas (total disponível: {{number_format($disponiveis, 0, ",", ".")}})</th>
            <th rowspan="2"  style="text-align:right;">Nunca movimentadas</th>
            <th rowspan="2"  style="text-align:right;">Armazenamento (R$)</th>
            <th rowspan="2"  style="text-align:right;">Fat. mínimo (R$)</th>
            <th rowspan="2"  style="text-align:left;">Observação</th>
        </tr>
        <tr>
            <th style="text-align:right;">No galpão</th>
            <th style="text-align:right;">Em consulta</th>
            <th style="text-align:right;">Só reservadas</th>
            <th style="text-align:right;">Total</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($clientes as $r)
        <tr>
            <td style="text-align:center;">{{$r->id_cliente}}</td>
            <td style="text-align:center;">@if($r->fl_ativo)
                ativo
                @else
                inativo
                @endif</td>
            <td style="text-align:center;">
                @if($r->nivel_inadimplencia == '')
                em dia
                @elseif ($r->nivel_inadimplencia == '1')
                notificação
                @else 
                bloqueado
                @endif
            </td>
            <td>{{$r->razaosocial }}</td>
            <td>{{$r->nomefantasia }}</td>
            <td style="text-align:right;">{{number_format($r->qtd_caixas_galpao, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($r->qtd_caixas_consulta, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($r->qtd_caixas_reservadas, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($r->qtd_caixas_total, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($r->qtd_caixas_nunca_movimentadas, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($r->valor_contrato, 2, ",", ".") }}</td>
            <td style="text-align:right;">{{number_format($r->valorminimofaturamento, 2, ",", ".") }}</td>
            <td style="text-align:right;">{{$r->obs}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</body>
</html>