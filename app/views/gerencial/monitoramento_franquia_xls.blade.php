<html>
    <head>
        <style>
            table tr td {border: thin solid #000; vertical-align: top;}
            table tr th {border: thin solid #000; vertical-align: middle; }
        </style>
            
    </head>
<body>
            <table class="listagem table table-striped table-condensed" data-sorter="true" style="width:100%;">
                <thead>
                    <tr>
                        <th style="text-align:center;">ID</th>
                        <th style="text-align: center;">Situação</th>
                        <th style="text-align: center;">Inadimpl.</th>
                        <th>Razão social</th>
                        <th>Nome fantasia</th>
                        <th style="text-align:center;">Início contrato</th>
                        <th  style="text-align:right;">Franquia custódia</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clientes as $r)
                    @if ($r->franquiacustodia ==0)
                    <tr class="warning">
                    @else
                    <tr>
                    @endif
                        <td style="text-align:center;">{{$r->id_cliente}}</td>
                        <td style="text-align:center;">
                            @if($r->fl_ativo)
                            ativo
                            @else
                            inativo
                            @endif
                        </td>
                        <td style="text-align:center;">
                            @if($r->nivel_inadimplencia == '')
                            em dia
                            @elseif ($r->nivel_inadimplencia == '1')
                            notificação
                            @else 
                            bloqueado
                            @endif
                        </td>
                        <td>{{$r->razaosocial }}</td>
                        <td>{{$r->nomefantasia }}</td>
                        <td style="text-align:right;">{{$r->iniciocontrato_fmt}}</td>
                        <td style="text-align:right;">{{$r->franquiacustodia}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
</body>
</html>