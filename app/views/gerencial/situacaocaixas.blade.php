@extends('stilearn-metro')

@section('conteudo')

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>{{$titulo}}</h1></div>
    <div class='content-action pull-right'>
        <a href='/gerencial/atualizeestatisticas' class='btn btn-cyan'><i class="aweso-refresh"></i> Atualizar estatísticas</a>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-search"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Pesquisar</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                    {{ Form::open(array('class'=> 'form-horizontal')) }}
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label">Razão social ou fantasia</label>
                                <div class="controls">
                                    {{Form::text('search', Input::old('search'),array('class'=>'input-block-level'))}}
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="fl_ativo">Status</label>
                                <div class="controls">
                                    <label class="radio">
                                        {{ Form::radio('fl_ativo', '1', (Input::old('fl_ativo') == '1' || Input::old('fl_ativo') == '') ? true : false, array('id'=>'1')) }}
                                        somente ativos
                                    </label>
                                    <label class="radio">
                                        {{ Form::radio('fl_ativo', '0', (Input::old('status') == '0') ? true : false, array('id'=>'0')) }}
                                        somente inativos
                                    </label>
                                    <label class="radio">
                                        {{ Form::radio('fl_ativo', '-1', (Input::old('fl_ativo') == '-1') ? true : false, array('id'=>'-1')) }}
                                        todos
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" class="btn bg-orange">Buscar</button>
                        <input type="submit" name="xls"  value="XLS" class="btn bg-lime"/>
                        <button type="reset" class="btn">Limpar</button>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
            <!-- List -->
            <table class="listagem table table-striped table-condensed" data-sorter="true" style="width:100%;">
                <colgroup>
                    <col width="50px"/>
                    <col width="50px"/>
                    <col width="50px"/>
                    <col width="auto"/>
                    <col width="100px"/>
                    <col width="100px"/>
                    <col width="100px"/>
                    <col width="100px"/>
                    <col width="100px"/>
                    <col width="100px"/>
                    <col width="100px"/>
                    <col width="200px"/>
                </colgroup>
                <thead>
                    <tr>
                        <th rowspan="2" style="text-align:center;">ID</th>
                        <th rowspan="2" style="text-align: center;">Situação</th>
                        <th rowspan="2" style="text-align: center;">Inadimpl.</th>
                        <th rowspan="2" >Razão social<br><span style="font-size:10px;">Nome fantasia</span></th>
                        <th colspan="4" style="text-align:center;">Caixas (total disponível: {{number_format($disponiveis, 0, ",", ".")}})</th>
                        <th rowspan="2"  style="text-align:right;">Nunca movimentadas</th>
                        <th rowspan="2"  style="text-align:right;">Armazenamento (R$)</th>
                        <th rowspan="2"  style="text-align:right;">Fat. mínimo (R$)</th>
                        <th rowspan="2"  style="text-align:left;">Observação</th>
                    </tr>
                    <tr>
                        <th style="text-align:right;">No galpão</th>
                        <th style="text-align:right;">Em consulta</th>
                        <th style="text-align:right;">Só reservadas</th>
                        <th style="text-align:right;">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clientes as $r)
                    <tr>
                        <td style="text-align:center;">{{$r->id_cliente}}</td>
                        <td style="text-align:center;">
                            @if($r->fl_ativo)
                            ativo
                            @else
                            inativo
                            @endif
                        </td>
                        <td style="text-align:center;">
                            @if($r->nivel_inadimplencia == '')
                            em dia
                            @elseif ($r->nivel_inadimplencia == '1')
                            notificação
                            @else
                            bloqueado
                            @endif
                        </td>
                        <td><a href="/cliente/contrato/{{$r->id_cliente}}#tab_contrato" target="_blank" title="Clique para acessar a aba 'Contrato' do cliente" >{{$r->razaosocial }}<br/><span style="font-size:10px;">{{$r->nomefantasia }}</span></a></td>
                        <td style="text-align:right;">{{number_format($r->qtd_caixas_galpao, 0, ",", ".")}}</td>
                        <td style="text-align:right;">{{number_format($r->qtd_caixas_consulta, 0, ",", ".")}}</td>
                        <td style="text-align:right;">{{number_format($r->qtd_caixas_reservadas, 0, ",", ".")}}</td>
                        <td style="text-align:right;">{{number_format($r->qtd_caixas_total, 0, ",", ".")}}</td>
                        <td style="text-align:right;">{{number_format($r->qtd_caixas_nunca_movimentadas, 0, ",", ".")}}</td>
                        <td style="text-align:right;">{{number_format($r->valor_contrato, 2, ",", ".") }}</td>
                        <td style="text-align:right;">{{number_format($r->valorminimofaturamento, 2, ",", ".") }}</td>
                        <td style="text-align:left;">{{$r->obs}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .c{
        width:200px;
        text-align:center;
    }
</style>
@stop
