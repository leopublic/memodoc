@extends('stilearn-metro')

@section('conteudo')

@include('cliente_tab_header')

<!-- #tab_departamentos - Begin Content  -->
<div class="tab-pane" id="tab_DepCliente">
    <?php
    $hasmany = array('title' => 'Departamentos',
        'fields' => array(
            'nome' => 'Nome',
            'qtd_documentos' => 'Documentos',
        )
    );
    ?>
    <div class="tab">
        <div class="listbox">
            <div class="box">
                <div class='form clearfix'>
                    <div class='subtitle' style="text-align: left;">
                        <h3 class="pull-left">Departamentos</h3> 
                        <a href="/{{$route_add}}" class="btn pull-right ajaxForm"><i class="icon-plus-sign"></i> Novo </a>
                    </div>
                </div>
                <table class='table table-striped table-hover'>
                    <thead>
                        <th style="width: 20px; text-align: center;">#</th>
                        <th style="width: 80px; text-align: center;">Ações</th>
                        <th>Nome</th>
                        <th style="text-align:right;">Qtd docs</th>
                    </thead>
                    <tbody>
                        <? $i=0; ?>                        
                        @foreach($model as $data)
                        <? $i++; ?>
                        <tr>
                            <td style="width: 20px; text-align: center;">{{$i}}</td>
                            <td style="text-align: center;"> 
                                <a title='Editar' href="/{{$route_edit}}/{{$data->getKey()}}" class="btn btn-mini btn-primary ajaxForm"><i class="icon-white icon-edit"></i></a>
                              <div class="btn-group">
                                  <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-danger btn-mini" title="Excluir esse item"><i class="icon-white icon-remove-sign"></i></a>
                                  <ul class="dropdown-menu">
                                      <li><a title='Deletar' href="/{{$route_delete}}/{{$data->getKey()}}" class="btn btn-danger" title="Confirmação">Clique aqui para confirmar a exclusão</a></li>
                                  </ul>
                              </div>
                            </td>
                            <td> {{$data['nome']}}</td>
                            <td style="text-align:right;"> {{number_format($data['qtd_documentos'], 0, ",", ".")}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<!-- #tab_departamentos - End Content  -->   

@include('cliente_tab_footer')
@stop
