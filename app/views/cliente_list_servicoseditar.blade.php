@extends('stilearn-metro')

@section('conteudo') 

        @include('cliente_tab_header')
        
        <!-- #tab_servicos - Begin Content  -->
         <div class="tab-pane" id="tab_servicos">
             <div class="tab listbox">
                <h3 class="pull-left">Editando serviços <span class="badge badge-info">{{$servicos->count()}}</span></h3> 
                {{Form::open(array('id'=>'FrmServico'))}}
                <input type='submit' class='pull-right btn btn-success' value='Salvar serviços' for='FrmServico' />
                <div class='clearfix'></div>
                <table class='table table-hover table-striped'>
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th style="text-align: right;">Valor normal R$</th>
                            <th style="text-align: right;">Valor urgência R$</th>
                            <th style="text-align: right;">Mínimo </th>
                        </tr>
                    </thead> 
                    <tbody>
                        <tr>
                           <td>Valor faturamento (mínimo em R$)</td> 
                           <td style="text-align: right;">--</td> 
                           <td style="text-align: right;">--</td> 
                           <td style="text-align: right;">{{Form::text('valorminimofaturamento', $cliente->valorminimofaturamento, array( 'data-fx'=>"masked" , 'data-inputmask'=>"'mask': '999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true", 'style'=>'text-align: right;'))}}</td> 
                        </tr>
                       
                        @foreach($servicos as $s)
                        <tr>
                           <td>{{$s->tipoproduto->descricao}}</td> 
                           <td style="text-align: right;">{{Form::text('valor_contrato['.$s->id_tipoproduto.']', $s->valor_contrato, array( 'data-fx'=>"masked" , 'data-inputmask'=>"'mask': '999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true", 'style'=>'text-align: right;'))}}</td> 
                           <td style="text-align: right;">{{Form::text('valor_urgencia['.$s->id_tipoproduto.']', $s->valor_urgencia, array( 'data-fx'=>"masked" , 'data-inputmask'=>"'mask': '999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true", 'style'=>'text-align: right;'))}}</td> 
                           <td style="text-align: right;">{{Form::text('minimo['.$s->id_tipoproduto.']',$s->minimo, array('style'=>'text-align: right;'))}}</td> 
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
                {{Form::close()}}
             </div>
         </div>
        <!-- #tab_servicos - End Content  -->   
        
        @include('cliente_tab_footer')
@stop
