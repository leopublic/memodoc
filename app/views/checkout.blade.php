@extends('stilearn-metro')

@section('conteudo')
@include('padrao/admin/content-header', array('titulo' => 'Checkout'))
@if (Session::has('success'))
<audio autoplay>
    <source src="/sons/ok.mp3" type="audio/mp3">
</audio>
@endif
@if (Session::has('errors'))
<audio autoplay>
    <source src="/sons/erro.mp3" type="audio/mp3">
</audio>
@endif
<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            <!-- widget form horizontal -->
            {{ Form::open(array('class'=>'form-horizontal', 'id' => 'form-checkin')) }}
            <div class="widget border-cyan" id="widget-horizontal">
                @include('padrao/widget_header', array('titulo' => 'Informe o ID', 'icone' => '<i class="aweso-archive"></i>'))
                <!-- widget content -->
                <div class="widget-content">
                    <div class="control-group">
                        <div class='input-append'>
                            {{Form::text('id_caixa', null, array('placeholder'=>'ID Caixa', 'class'=>'input-small', 'id' => 'id_caixa', 'autofocus'))}}
                            <button type="submit" name="Adicionar" id="Adicionar" class="btn btn-primary">Checkout</button>
                        </div>
                    </div>
                    @include('padrao/mensagens')
                </div>
            </div>
            {{Form::close()}}
            <!-- widget form horizontal -->
            <div class="widget border-cyan" id="widget-horizontal">
                @include('padrao/widget_header', array('titulo' => 'Caixas solicitadas hoje', 'icone' => '<i class="aweso-archive"></i>'))
                <div class="widget-content">
                    @if(isset($caixas))
                    <table  data-sorter="true" class='table table-condensed table-bordered'>
                        <thead>
                        <th style="width:70px;text-align: center;">Status</th>
                        <th style="width:120px;text-align: left;">Checkout por</th>
                        <th style="width:50px;text-align: center;">ID</th>
                        <th style="width:60px;text-align: center;">Referência</th>
                        <th style="width:auto;">Cliente</th>
                        <th style="width:60px;text-align: center;">OS</th>
                        <th style="width:100px;text-align: center;">Executar em</th>
                        </thead>
                        <tbody>
                            @foreach($caixas as $caixa)
                            @if ( $caixa['data_checkout'] == '' && $caixa['entregar_em'] < date('Y-m-d'))
                            <tr class="error">
                                @else
                            <tr>
                                @endif
                                <td style="text-align: center;">
                                    @if($caixa['data_checkout'] == '')
                                    (pendente)
                                    @else
                                    <i class="aweso-ok green"></i>
                                    @endif
                                </td>
                                <td style="text-align: left;">{{$caixa['nomeusuario']}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa['id_caixa'], -6)}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa['id_caixapadrao'], -6)}}</td>
                                <td>{{$caixa['razaosocial'] }}</td>
                                <td style="text-align: center;">
                                    <a href="{{URL::to('/ordemservico/visualizar/'.$caixa['id_os_chave'])}}" target="_blank" style="color:inherit;text-decoration: underline;" title="Clique para consultar a OS numa nova tela..">{{substr('000000'.$caixa['id_os'], -6) }}</a>
                                </td>
                                <td style="text-align: center;">{{$caixa['entregar_em_fmt'] }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>

        </div>
    </div>
</article> <!-- /content page -->

@stop
