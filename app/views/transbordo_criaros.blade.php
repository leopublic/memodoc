@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Criar transbordo</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-share-alt"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Adicione as caixas a serem transbordadas ({{$qtd_disp}} vagas disponíveis para movimentação)</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal', 'id' => 'frmcriatransbordo'))}}
                    {{Form::hidden('id_cliente_antes', Input::old('id_cliente_antes')) }}
                    {{Form::hidden('id_os_chave', $os->id_os_chave) }}
                    {{Form::hidden('efetivar', '', array('id' => 'efetivar')) }}
                     @if (isset($id_caixas))
                        @foreach($id_caixas as $i => $id_caixa)
                        <input type="hidden" name="id_caixas[]" value="{{$id_caixa}}"/>
                        @endforeach
                    @endif
                   <div class='control-group'>
                        {{Form::label('id_cliente','Cliente origem',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('cliente_antes', $cliente_antes->razaosocial.' ('.$cliente_antes->id_cliente.')' , array('disabled' => 'disabled',   'class' => 'input-block-level'))}}
                        </div>
                    </div>
                   <div class='control-group'>
                        {{Form::label('id_os','OS',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('id_os', substr('000000'.$os->id_os, -6) , array('class' => 'input-medium', 'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente destino',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('razaosocial', $os->cliente->razaosocial.' - '.$os->cliente->nomefantasia.' ('.$os->id_cliente.')' , array('class' => 'input-block-level', 'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::hidden('cortesia_movimentacao', '0') }}
                        {{Form::label('cortesia_movimentacao','Cortesia movimentação?',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::checkbox('cortesia_movimentacao', 1, $cortesia_movimentacao)}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('observacao','Observações',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::textarea('observacao', Input::old('observacao'), array('rows' => 4, 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        {{Form::label('id_caixapadrao','Referências ',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('id_caixapadrao_ini', null ,array('class'=>'input-small'))}}
                            </div>
                            A
                            <div class="input-append">
                                {{Form::text('id_caixapadrao_fim', null,array('class'=>'input-small'))}}
                            </div>
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button name="adicionar" class="btn btn-primary" id="adicionar" ><i class="aweso-plus"></i> Adicionar caixas</button>
                        @if (isset($caixas))
                        <div class="btn-group" id="envia">
                            <a href="#" id="buttonSubmit" data-toggle="dropdown" class="btn dropdown-toggle bg-orange" title="Realiza o transbordo"><i class="aweso-share-alt"></i> Efetivar</a>
                            <ul class="dropdown-menu">
                                <!--<li><input type="submit" name="efetivar" class="btn bg-orange" value="Clique aqui   para efetivar o transbordo. ATENÇÃO ESSA OPERAÇÃO NÃO PODE SER DESFEITA!!" /></li>-->
                                <li><a href="javascript:enviar();" class="btn bg-orange" >Clique aqui para efetivar o transbordo. ATENÇÃO ESSA OPERAÇÃO NÃO PODE SER DESFEITA!!</a></li>
                            </ul>
                        </div>
                        @endif
                        <span id="enviando" class="btn bg-orange" style="display:none;" ><i class="aweso-spinner aweso-spin"></i> Executando o transbordo (por favor aguarde)</span>
                    </div>
                    {{ Form::close() }}
                    @if (isset($caixas))
                    <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                        <thead>
                            <th style="width:80px; text-align:center;">#</th>
                            <th style="width:auto; text-align:center;">ID</th>
                            <th style="width:auto; text-align:center;">Referência</th>
                            <th style="width:auto;">&nbsp;</th>
                        </thead>
                            <? $i = 1;?>
                        <tbody>
                            @foreach($caixas as $caixa)
                            <tr class="">
                                <td style="text-align: center;">{{$i}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa, -6)}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao, -6)}}</td>
                                <td style="width:auto;">&nbsp;</td>
                            </tr>
                            <? $i++;?>
                            @endforeach

                        </tbody>
                    </table>
                    @endif

                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop
@section('scripts')
<script type='text/javascript'>
    function enviar(){
        $('#envia').hide();
        $('#adicionar').hide();
        $('#efetivar').val('sim');
        $('#enviando').show();
        $('#frmcriatransbordo').submit();
    }
    
</script>

@stop
