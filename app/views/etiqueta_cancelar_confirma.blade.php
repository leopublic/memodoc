@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Cancelar etiquetas</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-tags"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Confirme as etiquetas a serem canceladas</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal', 'url'=> '/etiqueta/cancelarconfirmado'))}}
                    {{Form::hidden('id_cliente', Input::old('id_cliente'))}}
                    {{Form::hidden('id_caixapadrao_ini', Input::old('id_caixapadrao_ini'))}}
                    {{Form::hidden('id_caixapadrao_fim', Input::old('id_caixapadrao_fim'))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('razaosocial', $razaosocial, array('class' => 'input-block-level', 'disabled' => 'disabled'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        {{Form::label('id_caixapadrao','Referências ',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('id_caixapadrao_ini', Input::old('id_caixapadrao_ini') ,array('class'=>'input-small', 'disabled' => 'disabled'))}}
                            </div>
                            A
                            <div class="input-append">
                                {{Form::text('id_caixapadrao_fim', Input::old('id_caixapadrao_fim') ,array('class'=>'input-small', 'disabled' => 'disabled'))}}
                            </div>
                        </div>
                    </div>

                    <div class="alert alert-error">
                       @if(Session::has('errors'))
                            {{Session::get('errors')}}
                       @else
                           Confirma o cancelamento das etiquetas relacionadas abaixo? ATENÇÃO: ESSA OPERAÇÃO NÃO PODE SER DESFEITA!!! <button type="submit" name="buscar" class="btn btn-primary" id="submit-pesquisa"><i class="aweso-remove"></i> Confirmar</button> <a href="{{URL::to('/expurgo/caixas')}}" class="btn"> Desistir</a>
                       @endif
                    </div>

                    {{ Form::close() }}

                    <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                        <thead>
                            <th style="width:auto; text-align:center;">ID</th>
                            <th style="width:auto; text-align:center;">Referência</th>
                            <th style="width:auto;">Situação</th>
                        </thead>
                        <tbody>

                            @foreach($caixas as $caixa)
                            @if ($caixa->itemalterado == 1)
                            <tr class="error">
                            @else
                            <tr class="">
                            @endif
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa, -6)}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao, -6)}}</td>
                                <td style="width:auto;">
                                    @if ($caixa->itemalterado == 1)
                                    ETIQUETA USADA
                                    @else
                                    <i class="aweso-ok"></i>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop