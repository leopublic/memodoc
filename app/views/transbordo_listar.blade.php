@extends('layout')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Transbordo de caixas</h1></div>
<!--     <div class='content-action pull-right'>
        <a href='/transbordo/novo' class='btn'>Criar novo</a>
    </div>
 -->
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <!-- search box -->
            @if(isset($transbordos))
            <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                <thead>
                    <th style="width:130px; text-align:center;">Ação</th>
                    <th style="width:auto; text-align:center;">ID</th>
                    <th style="width:auto; text-align:center;">OS</th>
                    <th style="width:auto; text-align: center;">Data da solicitação</th>
                    <th style="width:auto; text-align: center;">Caixas</th>
                    <th style="width:auto; text-align:center;">Reserva</th>
                    <th style="width:auto; text-align:center;">Expurgo</th>
                    <th style="width:auto; text-align:center;">Cliente origem</th>
                    <th style="width:auto; text-align:center;">Cliente destino</th>
                    <th style="width:auto; text-align: center;">Cadastrado por</th>
                </thead>
                <tbody>
                    @foreach($transbordos as $transbordo)
                    <tr class="">
                        <td style="text-align:center;">
                            <a href="/transbordo/caixas/{{$transbordo->id_transbordo}}" class="btn btn-xs btn-primary" title="Clique para ver os detalhes transbordo"><i class="aweso-edit"></i></a>
                            <a class="btn btn-xs btn-warning" href="/etiqueta/gere/{{ $transbordo->id_reserva }}" target="_blank" title="Imprimir etiquetas"><i class="aweso-tags"></i></a>
                            <a class="btn btn-xs bg-lime" href="/transbordo/depara/{{ $transbordo->id_transbordo}}" target="_blank" title="Imprimir de-para"><i class="aweso-print"></i></a>
                        </td>
                        <td style="text-align: center;">{{substr('000000'.$transbordo->id_transbordo, -6)}}</td>
                        <td style="text-align: center;">
                        @if($transbordo->id_os_chave > 0)
                            <a href="/osok/visualizar/{{$transbordo->id_os_chave}}" class="btn btn-primary" target="_blank">{{$transbordo->num_os}}</a>
                        @else
                            {{$transbordo->num_os}}
                        @endif
                        </td>
                        <td style="text-align: center;">{{$transbordo->created_at}}</td>
                        <td style="text-align: center;">{{$transbordo->qtd_caixas}}</td>
                        <td style="text-align: center;"><a class="btn btn-sm btn-primary" href="/reserva/visualizar/{{ $transbordo->id_reserva }}" target="_blank" title="Ver a reserva criada">{{ $transbordo->id_reserva }}</a></td>
                        <td style="text-align: center;"><a class="btn btn-sm btn-primary" href="/expurgo/caixasgravadas/{{ $transbordo->id_expurgo_pai }}" target="_blank" title="Ver a reserva criada">{{ $transbordo->id_expurgo_pai }}</a></td>
                        <td style="text-align: center;">{{$transbordo->clienteantes->razaosocial.' ('.$transbordo->id_cliente_antes.')'}}</td>
                        <td style="text-align: center;">{{$transbordo->clientedepois->razaosocial.' ('.$transbordo->id_cliente_depois.')'}}</td>
                        <td style="text-align: center;">
                            @if (is_object($transbordo->usuario))
                            {{$transbordo->usuario->nomeusuario}}
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .table tbody{
        font:12px arial;
    }
</style>
@stop
@section('scripts')
<script>
@if ($id_transbordo > 0)
    window.open('{{URL::to('/transbordo/depara/'.$id_transbordo)}}', 'depara');   
@endif
</script>
@stop

