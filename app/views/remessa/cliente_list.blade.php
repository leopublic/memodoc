@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <h3 class="page-title">Remessas</h3>
        <div class='content-action pull-right'>
        @if( Auth::user()->id_nivel==1 )    
            <a href='/publico/remessa/nova' class='btn btn-primary'>Carregar nova</a>
        @endif
        </div>
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio conteúdo -->
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-list"></i>Remessas cadastradas</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body ">
                <div class="row">
                    <!-- Inicio primeira coluna-->                        
                    <div class="col-md-12">
                        <table class="listagem table table-hover table-striped table-condensed" data-sorter="true" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:160px;">Ações</th>
                                    <th style="width:auto;">Data</th>
                                    <th style="width:auto;">Arquivo</th>
                                    <th style="width:auto; text-align: center;">Registros</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($instancias as $r)
                                <tr>
                                    <td class="c">
                                        @if( Auth::user()->id_nivel==1 )
                                        <a class="btn btn-primary" title="Editar" href="/publico/remessa/itens/{{ $r->getKey() }}"><i class="fa fa-edit"></i></a>
                                        @include('_layout.link-exclusao', array('msg'=> 'Clique para confirmar a exclusão da remessa', 'link'=>'/publico/remessa/excluir/'.$r->getKey()))
                                        @endif
                                    </td>
                                    <td>{{ $r->created_at }}</td>
                                    <td>{{ $r->nome_anexo }}</td>
                                    <td>{{ $r->qtd_registros }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
