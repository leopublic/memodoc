@extends('base')

@section('estilos')
    @include('_layout.estilos-forms')
@stop

@section('conteudo')
    @include('_layout.page-header', array('title'=>'Editar remessa'))
    @include('_layout.msg')
<!-- inicio conteúdo -->
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-user"></i>Informe os dados dessa remessa</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form">
                {{Form::model($model, array('class' => 'form-horizontal', 'files' => true))}}
                {{Form::hidden('id_remessa', $model->id_remessa)}}
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->
                        <div class="col-md-6">
                            <div class='form-group'>
                                {{Form::label('dt_inicial','Período',array('class'=>'control-label col-md-3'))}} 
                                <div class="col-md-9">
                                      <div class="input-group input-large">
                                          <span class="input-group-addon "> De </span><input type="text" name="dt_inicial" id="dt_inicial" value="{{ Input::old('dt_inicial') }}" class="form-control date-picker" data-date-format="dd/mm/yyyy" /><span class="input-group-addon "> até </span>
                                          <input type="text" name="dt_final" id="dt_final" value="{{ Input::old('dt_final') }}" class="form-control date-picker" data-date-format="dd/mm/yyyy" />
                                      </div>
                                </div>
                            </div>
                            <div class='form-group'>
                                {{Form::label('alfa','Descrição',array('class'=>'control-label col-md-3'))}} 
                                <div class="col-md-9">
                                      <div class="input-group">
                                          <span class="input-group-addon "> De </span><input type="text" name="alfa_inicial" id="alfa_inicial" value="{{ Input::old('alfa_inicial') }}" class="form-control" /><span class="input-group-addon "> até </span>
                                          <input type="text" name="alfa_final" id="alfa_final" value="{{ Input::old('alfa_final') }}" class="form-control" />
                                      </div>
                                </div>
                            </div>
                            <div class='form-group'>
                                {{Form::label('arquivo','Arquivo',array('class'=>'control-label col-md-3'))}} 
                                <div class="col-md-9">
                                    <input type="file" name="arquivo" class="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fim form-body -->
                <!-- INICIO FORM-ACTIONS -->
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-offset-3 col-md-9">
                                <input type="submit" class="btn btn-primary" id="submit-pesquisa" value="Salvar"></>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- FIM FORM-ACTIONS -->
                {{ Form::close() }}
            </div>
            <!-- Fim portlet-body -->
        </div>
        <!-- Fim portlet -->
    </div>
    <!-- fim  da coluna unica -->    
</div>
                        
@stop

@section('scripts')
    @include('_layout.scripts-forms')
    <script>
    </script>
@stop