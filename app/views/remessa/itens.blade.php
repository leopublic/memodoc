@extends('layout')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Itens da remessa {{$remessa->id_remessa}}</h1></div>
<!--     <div class='content-action pull-right'>
        <a href='/transbordo/novo' class='btn'>Criar novo</a>
    </div>
 -->
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <!-- search box -->
            @if(isset($itens))
            <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                <thead>
                    <th style="width:130px; text-align:center;">Ação</th>
                    <th style="width:auto; text-align:center;">ID</th>
                    <th style="width:auto; text-align: center;">Código</th>
                </thead>
                <tbody>
                    @foreach($itens as $item_remessa)
                    <tr class="">
                        <td style="text-align:center;">&nbsp;</td>
                        <td style="text-align: center;">{{substr('000000'.$item_remessa->id_remessa, -6)}}</td>
                        <td style="text-align: center;">{{substr('000000'.$item_remessa->codigo, -6)}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .table tbody{
        font:12px arial;
    }
</style>
@stop
@section('scripts')
@stop

