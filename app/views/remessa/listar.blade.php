@extends('layout')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Remessas</h1></div>
<!--     <div class='content-action pull-right'>
        <a href='/transbordo/novo' class='btn'>Criar novo</a>
    </div>
 -->
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <!-- search box -->
            @if(isset($remessas))
            <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                <thead>
                    <th style="width:130px; text-align:center;">Ação</th>
                    <th style="width:auto; text-align:center;">ID</th>
                    <th style="width:auto; text-align: center;">Data da carga</th>
                    <th style="width:auto; text-align: center;">Cadastrado por</th>
                </thead>
                <tbody>
                    @foreach($remessas as $remessa)
                    <tr class="">
                        <td style="text-align:center;">
                            <a href="/remessa/itens/{{$remessa->id_remessa}}" class="btn btn-xs btn-primary" title="Clique para ver os itens da remessa"><i class="aweso-edit"></i></a>
                        </td>
                        <td style="text-align: center;">{{substr('000000'.$remessa->id_remessa, -6)}}</td>
                        <td style="text-align: center;">{{$remessa->created_at}}</td>
                        <td style="text-align: center;">&nbsp;</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .table tbody{
        font:12px arial;
    }
</style>
@stop
@section('scripts')
@stop

