@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Cadastrar/alterar documentos</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            <!-- search box -->
            @include('padrao.mensagens')

            <div class="row-fluid">
                <div class="widget border-cyan" id="novo-documento">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-edit"></i></div>
                        <!-- widget title -->
                        @if (intval($documento->id_item) == 0)
                        <h4 class="widget-title">Referência {{$documento->id_caixapadrao}} - Novo item</h4>
                        @else
                        <h4 class="widget-title">Referência {{$documento->id_caixapadrao}} - Item {{$documento->id_item}}</h4>
                        @endif
                        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        <div class="widget-action color-cyan">
                        </div>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        {{ Form::model($documento, array('url' => '/documento/editar/'.$documento->id_cliente.'/'.$documento->id_caixapadrao.'/'.$documento->id_item, 'class'=>'form-horizontal')) }}
                        <div class="row-fluid">
                            <div class='span12'>
                                <div class='control-group'>
                                    {{Form::label('cliente','Cliente',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Cliente::find($id_cliente)->razaosocial}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class='span6 border-cyan'>
                                <div class='control-group'>
                                    {{Form::label('titulo','Título',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('titulo', null, array('class'=>'input-block-level', 'required'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('id_centro','Centro de custo',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('id_centro', $centros, null, array('id'=>'id_centro', 'class' => 'input-block-level'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('id_departamento','Departamento',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('id_departamento', $departamentos,null, array('id'=>'id_departamento', 'class' => 'input-block-level'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('id_setor','Setor',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('id_setor', $setores ,null, array('id'=>'id_setor', 'class' => 'input-block-level'))}}
                                    </div>
                                </div>
                            </div>

                            <div class='span6'>
                                <div class="control-group">
                                    <label class="control-label">Intervalo cronológico</label>
                                    <div class="controls">
                                        {{Form::text('inicio', null ,array('data-fx'=>"masked", 'data-inputmask'=>"'alias': 'dd/mm/yyyy'"))}}
                                        {{Form::text('fim', null ,array('data-fx'=>"masked", 'data-inputmask'=>"'alias': 'dd/mm/yyyy'"))}}
                                    </div>
                                </div>

                                <div class='control-group'>
                                    <label class="control-label">Intervalo numérico</label>
                                    <div class="controls">
                                        {{Form::text('nume_inicial', null ,array('placeholder'=>"De", 'class'=>'number'))}}
                                        {{Form::text('nume_final', null ,array('placeholder'=>"Até", 'class'=>'number'))}}
                                    </div>
                                </div>

                                <div class='control-group'>
                                    <label class="control-label">Intervalo alfabético</label>
                                    <div class="controls">
                                        {{Form::text('alfa_inicial', null ,array('placeholder'=>"De", 'class'=>'alfa'))}}
                                        {{Form::text('alfa_final', null ,array('placeholder'=>"Até", 'class'=>'alfa'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('migrada_de','Ref. de migração 1 e 2',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('migrada_de', null)}}
                                        {{Form::text('request_recall', null)}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('expurgo_programado','Previsão de expurgo',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('expurgo_programado_mes', null, array('placeholder' => "mês", "disabled"))}}
                                        {{Form::text('expurgo_programado', null, array("placeholder" => "ano", "disabled"))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class='control-group'>
                                {{Form::label('conteudo','Conteúdo', array('class'=>'control-label'))}}
                                <div class="controls">
                                    @if (isset($documento))
                                    {{Form::textarea('conteudo', str_replace('<br/>',"\n" , $documento->conteudo) , array('class'=>'input-block-level'))}}
                                    @else
                                    {{Form::textarea('conteudo', null , array('class'=>'input-block-level'))}}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class='clearfix'></div>
                        <div class="form-actions bg-silver">
                            <button type="submit" id="salvar" class="btn btn-primary">Salvar</button>
                        @if ($cliente->fl_controle_revisao)
                            @if ($documento->id_documento > 0)
                                @if ($documento->fl_revisado)
                                    <input type="submit" id="liberar" name="liberar" class="btn bg-green" title="Liberar alterações no conteúdo" value="Liberar alterações">
                                @else
                                    <input  type="submit" id="revisar"  name="revisar" class="btn bg-red" title="Indicar que o item foi revisado e bloquear alterações no conteúdo" value="Revisar e bloquear">
                                @endif
                            @endif
                        @endif
                            <button type="reset" class="btn">Cancelar</button>
                        </div>
                        {{Form::close()}}
                    </div><!-- /widget content -->
                </div> <!-- /widget form horizontal -->

            </div>

            @if ($documento->id_documento > 0)
            <div class="row-fluid">
                <div class="widget border-cyan" id="novo-documento">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-cloud-upload"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Adicionar digitalização</h4>
                        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        <div class="widget-action color-cyan">
                        </div>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        {{ Form::open(array('url' => '/anexo/adicionar/', 'class'=>'form-horizontal', 'files' => true)) }}
                        {{ Form::hidden('id_documento', $documento->id_documento)}}
                        {{ Form::hidden('id_cliente', $documento->id_cliente)}}
                        {{ Form::hidden('id_caixapadrao', $documento->id_caixapadrao)}}
                        {{ Form::hidden('id_item', $documento->id_item)}}
                        <div class="row-fluid">
                            <div class='span12'>
                                <div class="control-group">
                                    <label class="control-label" for="inputUpload">Arquivo</label>
                                    <div class="controls">
                                        <input type="file" id="arquivo" name="arquivo" />
                                    </div>
                                </div><!-- /control-group -->
                                <div class="control-group">
                                    <label class="control-label">Descrição</label>
                                    <div class="controls">
                                        {{Form::text('descricao', null,  array('class' => 'input-block-level'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='clearfix'></div>
                        <div class="form-actions bg-silver">
                            <button type="submit" class="btn btn-primary">Adicionar</button>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>

            <div class="row-fluid">
                <div class="widget border-cyan" id="novo-documento">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-edit"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Digitalizações</h4>
                        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        <div class="widget-action color-cyan">
                        </div>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        <div class="row-fluid">
                            <div class='span12'>
                                <table class='table table-striped table-hover table-bordered table-condensed'>
                                    <thead>
                                        <tr>
                                            <th style="width:120px; text-align: center;">Ações</th>
                                            <th style="width:auto">Descrição</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($anexos as $anexo)
                                        <tr>
                                            <td style="text-align:center;">
                                                <a href="/anexo/download/{{$anexo->id_anexo}}" class="btn btn-primary" title="Baixar uma cópia do arquivo"><i class="aweso-cloud-download"></i></a>
                                                <div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-danger " title="Excluir a digitalização"><i class="aweso-trash"></i></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="/anexo/excluir/{{$anexo->id_anexo}}">Clique aqui para confirmar a exclusão dessa digitalização</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>{{$anexo->descricao}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</article> <!-- /content page -->
@stop

@section('scripts')
<script>
    $(function () {
        $departamento = $('#id_departamento');
        $setor = $('#id_setor');
        $centrocusto = $('#id_centro');
        $cliente.change(function () {

            $('.load').show();
            $('#titulo').focus();
            $('.documentos').hide();

            // limpando combos
            $departamento.html('');
            $setor.html('');
            $centrocusto.html('');

            // ajax departamento
            $.get('publico/documento/ajaxdepartamento/' + $(this).val(), function (combo) {
                $departamento.html(combo);
            });
            // ajax centro custo
            $.get('publico/documento/ajaxcentrocusto/' + $(this).val(), function (combo) {
                $centrocusto.html(combo);
            });
            // ajax setor
            $.get('publico/documento/ajaxsetor/' + $(this).val(), function (combo) {
                $setor.html(combo);
            });

            // Remove o load após 2.5 segundos
            setInterval(function () {
                $('.load').hide();
            }, 2500);

        }).focus();
    });
</script>
@stop