@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Cancelar etiquetas</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-tags"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Informe o cliente e o número inicial e final das etiquetas a cancelar</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente', $clientes, Input::old('id_cliente', Session::get('id_cliente')) , array('id'=>'id_cliente','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        {{Form::label('id_caixapadrao','Referências ',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('id_caixapadrao_ini', Input::old('id_caixapadrao_ini') ,array('class'=>'input-small'))}}
                            </div>
                            A
                            <div class="input-append">
                                {{Form::text('id_caixapadrao_fim', Input::old('id_caixapadrao_fim') ,array('class'=>'input-small'))}}
                            </div>
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" name="buscar" class="btn btn-primary" id="submit-pesquisa"><i class="aweso-remove"></i> Cancelar</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop