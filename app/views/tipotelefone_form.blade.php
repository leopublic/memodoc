@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header">
            <h1>Tipo de telefone</h1>
        </div>

        <!-- content breadcrumb -->
        <ul class="breadcrumb breadcrumb-inline clearfix">
            <li><a href="#">Cadastros</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
            <li class="active">Tipo telefone</li>
        </ul>
        
        <div class='content-action pull-right'>
           
        </div>
    </header> <!--/ content header -->
    
    <!-- content page -->
    <article class="content-page clearfix">
        
        <!-- main page -->
        <div class="main-page">
            <div class="content-inner">
                
                        <div class="box clearfix">
                            <ul class="form">
                                <li class="subtitle">
                                    @if(isset($model))
                                      {{ Form::model($model) }} 
                                      <h4 class="pull-left">Editar {{$model->descricao}}</h4>
                                    @else
                                      {{ Form::open() }} 
                                      <h4 class="pull-left">Adicionar tipo de telefone</h4>
                                    @endif
                                    <div class="pull-right clearfix">
                                     {{ Form::submit('Salvar',array('class'=>'btn btn-primary pull-right')) }}
                                    </div>
                                </li>
                                <li>
                                    {{ Form::label('descricao','Descrição') }} 
                                    {{ Form::text('descricao') }}
                                </li>
                                {{ Form::close() }}  
                            </ul>    
                        </div>
                       
                    </div>
           
        </div>
    </article> <!-- /content page -->  
@stop