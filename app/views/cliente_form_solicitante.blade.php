<div class="boxbform">
    @if(isset($many))
      {{ Form::model($many) }}  
    @else
      {{ Form::open(array('url'=>$_SERVER['REQUEST_URI'].'?tab=tab_solicitantes')) }}    
    @endif
    <div class="box">
        <ul class="form"> 
            <li class='subtitle'>
                @if(isset($many))
                    Editar solicitante
                @else
                    Adicionar novo solicitante
                @endif
                
                {{ Form::submit('Salvar',array('class'=>'btn btn-primary pull-right')) }}
            </li>
            <li>
                {{ Form::hidden('tab','tab_solicitantes')}}
                {{ Form::label('nome','Nome do solicitante') }} 
                {{ Form::text('nome') }}
            </li>
            <li>
                {{ Form::label('id_departamento','Departamento') }} 
                {{ Form::select('id_departamento', FormSelect($select['DepCliente'])) }}
            </li>
            <li>
                {{ Form::label('id_centrocusto','Centro de custo') }} 
                {{ Form::select('id_centrocusto', FormSelect($select['CentroDeCusto'])) }}
            </li>
            <li>
                {{ Form::label('id_setor','Setor') }} 
                {{ Form::select('id_setor', FormSelect($select['SetCliente'])) }}
            </li>
        </ul>    
    </div>
    {{ Form::close() }}  
</div>