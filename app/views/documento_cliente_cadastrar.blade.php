@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Cadastrar documentos</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-archive"></i>Informe o número da referência</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form grey-steel filtro">
                {{Form::open(array('url' => '/publico/documento/carregacaixa', 'class' => 'form-horizontal'))}}
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group input-large">
                                        {{Form::text('id_caixapadrao', Input::old('id_caixapadrao', $id_caixapadrao), array('class'=>'form-control') )}}
                                        <div class="input-group-btn">
                                            <input type="submit" class="btn yellow-gold" class="button" id="adicionaCaixa" value="Localizar"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (isset($documentos) && count($documentos) > 0)
                    <div class="row">
                        <!-- Inicio primeira coluna-->
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <a class="btn btn-primary" href="/publico/documento/editar/{{$id_caixapadrao}}"><i class="fa fa-plus"></i> Incluir item</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@if(isset($documentos) && count($documentos) > 0)
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-folder-open"></i>Documentos cadastrados</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form grey-steel">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class='table table-striped table-hover table-bordered table-condensed'>
                                <thead>
                                    <tr>
                                        <th style="width:100px; text-align: center;">Ação</th>
                                        <th style="width:50px; text-align: center;">Item</th>
                                        <th style="width:auto">Título/Conteúdo</th>
                                        <th style="width:210px">Área</th>
                                        <th style="width:180px">Classificação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($documentos as $doc)
                                    <tr>
                                        <td style="text-align: center;">
                                            @if (Auth::user()->id_nivel <= 2)
                                            <a href="/publico/documento/editar/{{ $doc->id_caixapadrao}}/{{ $doc->id_item}}" class="btn btn-primary" title="Clique para editar esse item"><i class="fa fa-edit"></i></a>
                                            @endif
                                        </td>
                                        <td style="text-align: center;">{{ substr("000".$doc->id_item, -3)}}</td>
                                        <td><b>{{$doc->titulo}}</b><br/>{{$doc->conteudo}}
                                            @if ($cliente->fl_controle_revisao)
                                            <br/><span style="font-style: italic;font-size: 11px;">{{$doc->situacao_revisao_cliente}}</span>
                                            @endif
                                        </td>
                                        <td style="padding:0;">
                                            <table >
                                                <tr><td><b>Setor:</b></td><td>{{isset($doc->setcliente->nome) ? $doc->setcliente->nome : '--' }}</td></tr>
                                                <tr><td><b>Departamento:</b></td><td>{{isset($doc->depcliente->nome) ? $doc->depcliente->nome : '--' }}</td></tr>
                                                <tr><td><b>Centro de custos:</b></td><td>{{isset($doc->centrodecusto->nome) ? $doc->centrodecusto->nome : '--' }}</td></tr>
                                                @if (isset($propriedade))
                                                <tr><td><b>{{$propriedade->nome}}:</b></td><td>{{count($doc->valor) > 0 ? $doc->valor->nome : '--' }}</td></tr>
                                                @endif
                                            </table>
                                        </td>
                                        <td style="padding:0;">
                                            <table style="width:100%;">
                                                <tr><td>Tipo</td><td>{{$doc->tipodocumento->descricao}}</td></tr>
                                                <tr><td>Origem 1</td><td>{{$doc->migrada_de}}</td></tr>
                                                <tr><td>Origem 2</td><td>{{$doc->request_recall}}</td></tr>
                                                <tr><td>Início</td><td>
                                                        @if ($doc->inicio != '')
                                                        {{$doc->inicio_fmt}}
                                                        @endif
                                                    </td></tr>
                                                <tr><td>Fim</td><td>
                                                        @if ($doc->fim != '')
                                                        {{$doc->fim_fmt}}
                                                        @endif
                                                    </td></tr>
                                                <tr><td>N&ordm; inicial</td><td>{{$doc->nume_inicial}}</td></tr>
                                                <tr><td>N&ordm; final</td><td>{{$doc->nume_final}}</td></tr>
                                                <tr><td>Alfa inicial</td><td>{{$doc->alfa_inicial}}</td></tr>
                                                <tr><td>Alfa final</td><td>{{$doc->alfa_final}}</td></tr>
                                                <tr><td>Expurgar após</td><td>{{$doc->expurgo_programado_completo}}</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@stop
@section('scripts')
@include('_layout.scripts-forms')
@stop
