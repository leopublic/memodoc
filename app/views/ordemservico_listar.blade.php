@extends('layout')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>{{ $titulo }}</h1></div>
    </header> <!--/ content header -->

    <!-- content page -->
    <article class="content-page clearfix">

        <!-- main page -->
        <div class="main-page">
            <div class="content-inner">

                @include ('padrao/mensagens')

                <!-- search box -->
                {{Form::model($model, array('url'=>'/ordemservico/listar', 'method' => 'get', 'class' => 'form-horizontal'))}}
                <div class="search-box">
                    <div class="row-fluid">
                        <div class="span6">
                            <div class='control-group'>
                                {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                                <div class="controls">
                                {{Form::select('id_cliente', $clientes, null , array('id'=>'id_cliente','data-fx'=>'select2','class'=>'input-block-level'))}}
                                </div>
                            </div>
                            
                            <div class="control-group">
                                {{Form::label('responsavel','Responsável', array('class'=>'control-label'))}}
                                <div class="controls">
                                   {{Form::text('responsavel', null ,array('placeholder'=>"Nome do responsável",'class'=>'input-block-level','style'=>''))}}
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class='control-group'>
                                <label class="control-label">Solicitado em</label>
                                <div class="controls">
                                    <div class="input-append input-append-inline ">
                                        {{Form::text('solicitado_em_ini', null ,array('placeholder'=>"desde", 'class'=>'input-small', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                        <span class="add-on"><i class="icomo-calendar"></i> </span>
                                    </div>
                                    <div class="input-append input-append-inline ">
                                        {{Form::text('solicitado_em_fim', null ,array('placeholder'=>"até", 'class'=>'input-small', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                        <span class="add-on"><i class="icomo-calendar"></i> </span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class='control-group'>
                                {{Form::label('id_status_os','Status',array('class'=>'control-label'))}}
                                <div class="controls">
                                {{Form::select('id_status_os', $status, null , array('id'=>'id_status_os','data-fx'=>'select2'))}}
                                </div>
                            </div>
                        </div>
                        <div class='control-group'>
                            <label class="control-label">&nbsp;</label>
                            <div class="controls">
                                <button class='btn btn-success' id="submit-pesquisa">Buscar</button>
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>

          {{ $oss->appends(Input::except('page'))->links() }}
           <table  data-sorter="true" class='table table-hover table-condensed table-bordered' style=" font-size:11px;">
               <thead>
                      <th style="width:80px;text-align:center">Ações</th>
                      <th style="width:30px; text-align: center;" title="Urgente"><i class="aweso-exclamation red"></i></th>
                     <th style="width:40px; text-align:center;">OS</th>
                     <th style="width:auto">Aberta por</th>
                     <th style="width:auto">Responsável</th>
                     <th style="width:auto;">Cliente</th>
                     <th style='width:130px;text-align:center;'>Solicitado em</th>
                     <th style='width:90px;text-align:center;'>Entregar</th>
                     <th style='width:80px;text-align:center;'>Status</th>
                     <th style='width:60px;text-align:center;'>Etiq. + cart.</th>
                     <th style='width:60px;text-align:center;'>Cartonagem</th>
                     <th style='width:60px;text-align:center;'>Etiquetas</th>
                     <th style='width:60px;text-align:center;'>Apanha</th>
                     <th style='width:70px;text-align:center;'>Empréstimo</th>

               </thead>
               <tbody>

                   @foreach($oss as $data)
                      @if($data->urgente == 1)
                      <tr class="bg-red" >
                      @else
                      <tr class="" >
                      @endif
                          <td style="text-align: center;">
                              <a href="{{\Memodoc\Servicos\Rotas::ordemservico()}}/visualizarlimpo/{{$data->getKey()}}" title="Ver a ordem de serviço" class="btn btn-primary btn-small"><i class="aweso-folder-open"></i></a>
                              @include('padrao.link_exclusao', array('url'=> '/ordemservico/cancelar/'.$data->getKey(), 'msg_confirmacao' => 'Clique para confirmar o cancelamento da ordem de serviço', 'class'=>'btn-small'))
                          </td>
                          <td style="text-align: center; vertical-align: middle;">
                              @if($data->urgente == 1)
                              <i class="aweso-exclamation red aweso-2x"></i>
                              @endif
                          </td>
                          <td style="text-align: center;">{{substr('000000'.$data->id_os, -6)}}</td>
                          <td style="">
                              @if (is_object($data->usuariocadastro))
                                @if ($data->usuariocadastro->id_nivel < 10)
                                <i class="aweso-star"></i>
                                <span style="font-weight:bold;">{{ucwords(strtolower($data->usuariocadastro->nomeusuario))}}</span>
                                @else
                                <i class="aweso-star-empty " style="color:transparent;"></i>
                              {{ucwords(strtolower($data->usuariocadastro->nomeusuario))}}
                                @endif
                              @endif
                          </td>
                          <td>{{ucwords(strtolower($data->responsavel))}}</td>
                          <td>{{$data->cliente->razaosocial}}</td>

                          <td style="text-align:center;">{{$data->solicitado_em}}</td>
                          <td style="text-align:center;">{{$data->entregar_em}}</td>
                          <td style="text-align:center;">{{isset($data->status_os->descricao) ? $data->status_os->descricao : '' }}</td>
                          <td style="text-align:center;">{{ $data->qtdenovascaixas; }}</td>
                          <td style="text-align:center;">{{ $data->qtd_cartonagens_cliente; }}</td>
                          <td style="text-align:center;">{{ $data->qtdenovasetiquetas; }}</td>
                          <td style="text-align:center;">{{$data->apanhanovascaixas + $data->apanhacaixasemprestadas}}</td>
                          <td style="text-align:center;">{{OsDesmembrada::where('id_os_chave', '=', $data->id_os_chave)->select('id_caixapadrao')->distinct()->count()}}</td>
                      </tr>
                   @endforeach

               </tbody>
           </table>

          {{ $oss->appends(Input::except('page'))->links() }}

            </div>
        </div>
    </article> <!-- /content page -->
@stop