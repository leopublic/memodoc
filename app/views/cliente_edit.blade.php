@include('cliente_masterpage_header')
@section('conteudo')  
        <!-- Tab identificação -->
         <div class="tab-pane active" id="tab_indentificacao">

             <div class="row tab">
                 <div class="slot-0-1-2-3-4-5">
                     {{ Form::model($cliente) }}
                     <div class="box">
                         <ul class="form"> 
                             <li class='subtitle'>
                                 Identificação
                                 <button class="btn btn-primary btn-mini pull-right"><i class="icon-white icon-ok-sign"></i> Salvar</button>
                             </li>

                             <li>
                                 {{ Form::label('logradouro','Logradouro') }} <br />
                                 {{ Form::text('logradouro') }}
                             </li>
                             <li>
                                 {{ Form::label('endereco','Endereço') }} <br />
                                 {{ Form::text('endereco') }}
                             </li>
                             <li>
                                 {{ Form::label('numero','Número') }} <br />
                                 {{ Form::text('numero') }}
                             </li>
                             <li>
                                 {{ Form::label('complemento','Complemento') }} <br />
                                 {{ Form::text('complemento') }}
                             </li>
                             <li>
                                 {{ Form::label('bairro','Bairro') }} <br />
                                 {{ Form::text('bairro') }}
                             </li>
                              <li>
                                 {{ Form::label('cidade','Cidade') }} <br />
                                 {{ Form::text('cidade') }}
                             </li>
                             <li>
                                 {{ Form::label('estado','Estado') }} <br />
                                 {{ Form::text('estado') }}
                             </li>
                             <li>
                                 {{ Form::label('cep','CEP') }} <br />
                                 {{ Form::text('cep') }}
                             </li>

                             <li class='subtitle'>
                                 Dados do contrato
                             </li>
                             <li>
                                 {{ Form::label('razaosocial','Razão Social') }} <br />
                                 {{ Form::text('razaosocial') }}
                             </li>
                             <li>
                                 {{ Form::label('nomefantasia','Fantasia') }} <br />
                                 {{ Form::text('nomefantasia') }}
                             </li>
                             <li>
                                 {{ Form::label('cgc','CGC') }} <br />
                                 {{ Form::text('cgc', null, array( 'data-fx'=>"masked" , 'data-inputmask'=>"'mask': '99.999.999/9999-99'")) }}
                             </li>
                             <li>
                                 {{ Form::label('ie','Inscrição estadual') }} <br />
                                 {{ Form::text('ie') }}
                             </li>
                              <li>
                                 {{ Form::label('im','Inscrição municipal') }} <br />
                                 {{ Form::text('im') }}
                             </li>
                              <li>
                                 {{ Form::label('obs','Observação') }} <br />
                                 {{ Form::textarea('obs') }}
                             </li>
                              <li>
                                 {{ Form::label('valorminimofaturamento','Valor min faturamento') }} <br />
                                 {{ Form::text('valorminimofaturamento') }}
                             </li>
                              <li>
                                 {{ Form::label('iniciocontrato','Início do contrato') }} <br />
                                 {{ Form::text('iniciocontrato') }}
                             </li>
                              <li>
                                 {{ Form::label('franquiacustodia','Franquia de custódia deve ser cobrada após') }} <br />
                                 {{ Form::text('franquiacustodia') }}
                             </li>
                              <li>
                                 {{ Form::label('atendimento','Tipo de atendimento') }} <br />
                                 {{ Form::checkbox('atendimento') }}
                             </li>
                              <li>
                                  {{ Form::label('franquiamovimentacao','Franquia movimentação (%) **sobre o valor de caixas') }} <br />
                                 {{ Form::text('franquiamovimentacao') }}
                             </li>  
                              <li>
                                 {{ Form::label('issporfora','ISS por fora') }} <br />
                                 {{ Form::checkbox('issporfora') }}
                             </li> 
                              <li>
                                 {{ Form::label('descontocustodiapercentual','Desconto custo dia percentual') }} <br />
                                 {{ Form::text('descontocustodiapercentual') }}
                             </li> 
                              <li>
                                 {{ Form::label('descontocustodialimite','Desconto custo dia limite') }} <br />
                                 {{ Form::text('descontocustodialimite') }}
                             </li>

                              <li>
                                 {{ Form::label('obsdesconto','Obs desconto') }} <br />
                                 {{ Form::textarea('obsdesconto') }}
                             </li> 
                              <li>
                                 {{ Form::label('movimentacaocortesia','Movimentação cortesia') }} <br />
                                 {{ Form::text('movimentacaocortesia') }}
                             </li> 
                              <li>
                                 {{ Form::label('responsavelcobranca','Responsável da cobrança') }} <br />
                                 {{ Form::text('responsavelcobranca') }}
                             </li> 
                              <li>
                                 {{ Form::label('indicereajuste','Índice de ajuste') }} <br />
                                 {{ Form::text('indicereajuste') }}
                             </li> 


                         </ul>    

                         <div style="width:100%; text-align: center;clear:both">
                                 {{ Form::submit('Salvar') }}								
                         </div>
                     </div>
                     {{ Form::close() }}
                 </div>
             </div>

          </div><!-- Tab identificação -->
@stop
@include('cliente_masterpage_footer')