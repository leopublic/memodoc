<div class="span12">
    <div class="row-fluid">
    @foreach($dias_vencimento as $dia_vencimento)
        @if($dia_vencimento->fl_default == 1)
            <?php $estilo = "bg-amber"; ?>
        @else
            <?php $estilo = ""; ?>
        @endif        
        <div class="span2">
            <div class="control-group">
                <div class="controls">
                    <div class="input-append">
                    @if($dia_vencimento->fl_default == 0)
                        <a href="javascript:excluir_dia('{{ $dia_vencimento->dia}}');"  class="btn bg-red" id="btnRemove{{ $dia_vencimento->dia}}" title="Clique para remover esse dia"><i class="aweso-remove"></i></a>
                        <a href="javascript:tornar_default('{{ $dia_vencimento->dia}}');"  class="btn bg-green" id="btnStar{{ $dia_vencimento->dia}}" title="Clique para tornar esse dia o dia default"><i class="aweso-star"></i></a>
                    @else
                        <span  class="btn"  title="Você não pode excluir o dia default"><i class="aweso-remove"></i></span>
                        <span  class="btn"  title="Este já é o dia default"><i class="aweso-star"></i></span>
                    @endif        
                        <a id="btnSpin{{ $dia_vencimento->dia}}" class="btn " style="display:none;"><i class="aweso-spin aweso-spinner"></i></a>
                        <span class="btn {{$estilo}}">{{ $dia_vencimento->dia}}</span>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    </div>
</div>
