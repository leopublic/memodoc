@extends('layout')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Dias de faturamento</h1></div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')
            <div class="widget border-cyan" id="widget-tdefault">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-plus"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Adicionar dia</h4>
                    <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                {{ Form::open(array('class'=>'form-horizontal', 'id'=> 'frmDados')) }}
                    <div class="control-group">
                        <label class="control-label">Adicionar dia </label>
                        <div class="controls">
                            <div class="input-append">
                                {{Form::text('dia', null ,array('class'=>'input-mini', 'id'=> 'dia'))}}
                                <input type="button" id="btnAdicionar" value="Adicionar" class="btn" onclick="javascript:adicionar_dia();"/>
                                <a id="btnAguarde" class="btn" style="display:none;"><i class="aweso-spin aweso-spinner"></i> aguarde...</a>
                            </div>
                        </div>
                    </div>
                {{ Form::close()}}
                </div><!-- /widget content -->
            </div> <!-- /widget tdefault -->

            <div class="widget border-cyan" id="widget-tdefault">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-table"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Dias cadastrados</h4>
                    <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                    <div class="row-fluid" id="tabela" style="display:none;">
                    </div>
                    <div class="row-fluid" id="hour" >
                        <div class="span12" style="text-align:center; font-size:14px;">
                            <i class="aweso-spin aweso-spinner" style="font-size:28px;"></i>
                        </div>
                    </div>
                </div><!-- /widget content -->
            </div> <!-- /widget tdefault -->

        </div>
    </div>
</article> <!-- /content page -->
@stop

@section('scripts')
<script>
$(document).ready(function(){
    $("btnAguarde").hide();
    recarrega();
});

function adicionar_dia(){
    $("#btnAdicionar").hide();
    $("#btnAguarde").show();

    let dia = $('#dia').val();
    let url = "/diavencimento/novo";
    let dados = $('#frmDados').serialize();
    $.post( url, dados )
        .done(function( data ) {
            console.log(data);
            recarrega();
                $.pnotify({title: "Ok!", text: data.msg, type: "success"});
            $("#dia").val("");
            $("#btnAdicionar").show();
            $("#btnAguarde").hide();
        });

}

function recarrega(){
    $("#tabela").hide();
    $("#hour").show();
    $("#tabela").load('/diavencimento/tabeladias', function(){
        $("#tabela").show();
        $("#hour").hide();
    });
}

function excluir_dia(dia){
    let url = "/diavencimento/exclui";
    $("#btnRemove"+dia).hide();
    $("#btnSpin"+dia).removeClass("bg-green");
    $("#btnSpin"+dia).addClass("bg-red");
    $("#btnSpin"+dia).show();

    $.post( url, {dia: dia} )
        .done(function( data ) {
            $.pnotify({title: "Ok!", text: data.msg, type: "success"});
            recarrega();
        });

}

function tornar_default(dia){
    let url = "/diavencimento/tornardefault/"+dia;
    $("#btnStar"+dia).hide();
    $("#btnSpin"+dia).removeClass("bg-red");
    $("#btnSpin"+dia).addClass("bg-green");
    $("#btnSpin"+dia).show();

    $.get( url )
        .done(function( data ) {
            $.pnotify({title: "Ok!", text: data.msg, type: "success"});
            recarrega();
        });

}

</script>
@stop
