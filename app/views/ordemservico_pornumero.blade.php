@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Localizar ordem de serviço pelo número</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-truck"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Informe o cliente e o número da ordem de serviço</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente', $clientes, Input::old('id_cliente', Session::get('id_cliente')) , array('id'=>'id_cliente','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('id_os','Número',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('id_os', Input::old('id_os'), array('class'=>'input-medium','required'))}}
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" name="buscar" class="btn btn-primary" id="submit-pesquisa"><i class="aweso-search"></i> Buscar</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop