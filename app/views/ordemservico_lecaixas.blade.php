@extends('stilearn-metro')
@section('styles')
video{
width: 200px; height:100px;
}
@stop

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Ordem de serviço 
            {{ substr('000000'.$os->id_os, -6) }}  - {{ $os->statusos->descricao }}</h1>
    </div>


</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
                                <div id="interactive" class="viewport"></div>

    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">
            <!-- widget form horizontal -->
            <div class="row-fluid">
                <div class="widget border-cyan span12" id="widget-horizontal">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-truck"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Ler caixas</h4>
                        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        <div class="widget-action">
                            <div class="btn-group">
                            </div>
                        </div>

                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        {{ Form::model(array('class'=>'form-horizontal', 'id'=>'form_os')) }}
                        {{ Form::hidden('id_os_chave', null) }}
                        <div class="row-fluid">
                            <div class="span12">
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12" id="ids">
                            </div>
                        </div>
                        <div class="form-actions bg-silver">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>

                        {{Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</article> <!-- /content page -->

@stop

@section('scripts')
<style>
    .date{
        width:96px;
    }
    .form-horizontal .control-group{
        margin-bottom:5px !important;
    }
</style>

<script>

    $(document).ready(function () {
    });
$(function() {
    var App = {
        init : function() {
            Quagga.init(this.state, function() {
//                App.attachListeners();
                Quagga.start();
            });
        },
        _accessByPath: function(obj, path, val) {
            var parts = path.split('.'),
                depth = parts.length,
                setter = (typeof val !== "undefined") ? true : false;

            return parts.reduce(function(o, key, i) {
                if (setter && (i + 1) === depth) {
                    o[key] = val;
                }
                return key in o ? o[key] : {};
            }, obj);
        },
        _convertNameToState: function(name) {
            return name.replace("_", ".").split("-").reduce(function(result, value) {
                return result + value.charAt(0).toUpperCase() + value.substring(1);
            });
        },
        detachListeners: function() {
            $(".controls").off("click", "button.stop");
            $(".controls .reader-config-group").off("change", "input, select");
        },
        setState: function(path, value) {
            var self = this;

            if (typeof self._accessByPath(self.inputMapper, path) === "function") {
                value = self._accessByPath(self.inputMapper, path)(value);
            }

            self._accessByPath(self.state, path, value);

            console.log(JSON.stringify(self.state));
            App.detachListeners();
            Quagga.stop();
            App.init();
        },
        inputMapper: {
            inputStream: {
                constraints: function(value){
                    var values = value.split('x');
                    return {
                        width: parseInt(values[0]),
                        height: parseInt(values[1]),
                        facing: "environment"
                    }
                }
            },
            numOfWorkers: function(value) {
                return parseInt(value);
            },
            decoder: {
                readers: function(value) {
                    return [value + "_reader"];
                }
            }
        },
        state: {
            inputStream: {
                type : "LiveStream",
                constraints: {
                    width: 300,
                    height: 200,
                    facing: "environment" // or user
                }
            },
            locator: {
                patchSize: "small",
                halfSample: true,
                showCanvas: false
            },
            numOfWorkers: 1,
            decoder: {
                readers : ["code_39_reader"]
            },
            locate: true
        },
        lastResult : null
    };

    App.init();

    Quagga.onProcessed(function(result) {
        var drawingCtx = Quagga.canvas.ctx.overlay,
            drawingCanvas = Quagga.canvas.dom.overlay;

        if (result) {
            if (result.boxes) {
                drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                result.boxes.filter(function (box) {
                    return box !== result.box;
                }).forEach(function (box) {
                    Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: "green", lineWidth: 2});
                });
            }

            if (result.box) {
                Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: "#00F", lineWidth: 2});
            }

            if (result.codeResult && result.codeResult.code) {
                Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, {color: 'red', lineWidth: 3});
            }
        }
    });

    Quagga.onDetected(function(result) {
        var code = result.codeResult.code;

        if (App.lastResult !== code) {
            App.lastResult = code;
            var $node = null, canvas = Quagga.canvas.dom.image;

            $node = $('<li><div class="thumbnail"><div class="imgWrapper"><img /></div><div class="caption"><h4 class="code"></h4></div></div></li>');
            $node.find("img").attr("src", canvas.toDataURL());
            $node.find("h4.code").html(code);
            
            //$("#ids").prepend($node);
            $("#ids").prepend(code+"\n");
            
            
        }
    });

});

</script>
@stop