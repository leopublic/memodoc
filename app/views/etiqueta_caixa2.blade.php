@if($break)
<pagebreak />
@endif
<table>
<tr>
<td>
<div style="width:150px;margin:0px;padding:0px;font-size:10px;font-family:'DejaVuSansCondensed';float:left;">
	@if(isset($etiqueta['esq']))
	<table style="rotate: 90;font-size:12px;font-weight:bold;font-family:arial;">
		<tr><td colspan="2" style="font-size:50px;line-height: 80%;padding:0; margin:0;">{{ $etiqueta['esq']['numero'] }}</td></tr>
		<tr><td colspan="2">MEMODOC GUARDA DE DOCUMENTOS LTDA</td></tr>
		<tr><td >{{ $etiqueta['esq']['tipo'] }}</td><td style="text-align: right">Reserva: {{ $etiqueta['esq']['reserva'] }}</td></tr>
		<tr><td >ID: {{ $etiqueta['esq']['id'] }}</td><td style="text-align: right;font-size:25px">{{ $etiqueta['esq']['endereco'] }}</td></tr>
	</table>
	@else
	   &nbsp;
	@endif
</div>
</td>
<td>
<div style="width:150px; margin:0px;rotate: 90;padding:0px;font-size:10px;font-family:'DejaVuSansCondensed';">
	@if(isset($etiqueta['dir']))
		<table style="rotate: 90;font-size:12px;font-weight:bold;font-family:arial;">
			<tr><td colspan="2" style="font-size:50px;line-height: 80%;padding:0; margin:0;">{{ $etiqueta['dir']['numero'] }}</td></tr>
			<tr><td colspan="2">MEMODOC GUARDA DE DOCUMENTOS LTDA</td></tr>
			<tr><td >{{ $etiqueta['dir']['tipo'] }}</td><td style="text-align: right">Reserva: {{ $etiqueta['dir']['reserva'] }}</td></tr>
			<tr><td >ID: {{ $etiqueta['dir']['id'] }}</td><td style="text-align: right;font-size:25px">{{ $etiqueta['dir']['endereco'] }}</td></tr>
		</table>
	@else
	   &nbsp;
	@endif
</div>
</td>
</tr>
</table>