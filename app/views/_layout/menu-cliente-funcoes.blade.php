<!-- BEGIN HORIZANTAL MENU -->
<!-- DOC: Remove "hor-menu-light" class to have a horizontal menu with theme background instead of white background -->
<!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) in the responsive menu below along with sidebar menu. So the horizontal menu has 2 seperate versions -->
<div class="hor-menu hor-menu-light hidden-sm hidden-xs">
    <ul class="nav navbar-nav">
        <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;"><i class="fa fa-archive fa-large"></i>&nbsp;Documentos<i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu pull-left">
                <li><a href="{{ URL::to('publico/documento') }}">Pesquisar conteúdo</a></li>
            @if(Auth::user()->id_nivel <= 2)
                <li><a href="{{ URL::to('publico/documento/cadastrar') }}">Cadastrar documentos</a></li>
            @endif
            </ul>
        </li>
        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;"><i class="fa fa-gears fa-large"></i> &nbsp;Cadastros <i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu pull-left">
                <li><a href="{{ URL::to('publico/centrodecusto') }}">Centros de custo</a></li>
                <li><a href="{{ URL::to('publico/departamento') }}">Departamentos</a></li>
                <li><a href="{{ URL::to('publico/setor') }}">Setores</a></li>
                @if(isset($propriedade))
                    <li><a href="{{ URL::to('publico/propriedade') }}">{{$propriedade->nome}}</a></li>
                @endif
            </ul>
        </li>
        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;">
                <i class="fa fa-truck fa-large"></i> &nbsp;Solicitações <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
                @if(Auth::user()->id_nivel <= 2)
                <li class="inativo"><a href="{{ URL::to('publico/os/encerrarpendente') }}">Encerrar solicitação em aberto</a></li>
                <li class="inativo"><a href="{{ URL::to('publico/os/criar') }}">Abrir solicitação</a></li>
                @endif
                <li class="inativo"><a href="{{ URL::to('publico/os') }}">Consultar solicitações</a></li>
            </ul>
        </li>
        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;">
                <i class="fa fa-lock fa-large"></i> &nbsp;Senha/Usuários <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
                <li class="inativo"><a href="{{ URL::to('publico/usuario/meusdados') }}">Alterar meus dados</a></li>
                @if(Auth::user()->id_nivel==1)
                <li class="inativo"><a href="{{ URL::to('publico/usuario/novo') }}">Cadastrar usuário</a></li>
                @endif
                <li class="inativo"><a href="{{ URL::to('publico/usuario') }}">Listar usuários</a></li>
            </ul>
        </li>
        <li class="classic-menu-dropdown">
            <a data-toggle="dropdown" href="javascript:;">
                <i class="fa fa-laptop fa-large"></i> &nbsp;Consultas diversas <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-left">
                <li class="inativo"><a href="{{ URL::to('publico/caixa/expurgadas') }}">Listar referências expurgadas</a></li>
                <li class="inativo"><a href="{{ URL::to('publico/reserva') }}">Reservas realizadas</a></li>
            </ul>
        </li>
    </ul>
</div>
<!-- END HORIZANTAL MENU -->
