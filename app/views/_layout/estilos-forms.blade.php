<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/clockface/css/clockface.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/bootstrap-datetimepicker/css/datetimepicker.css')}}"/>

<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/bootstrap-select/bootstrap-select.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/select2/select2.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/jquery-multi-select/css/multi-select.css')}}"/>

<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/jquery-tags-input/jquery.tagsinput.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('/assets/metronic/global/plugins/typeahead/typeahead.css')}}">
