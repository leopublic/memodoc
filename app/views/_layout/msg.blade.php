<!-- mensagens -->
@if (Session::has('msg') || Session::has('success') || Session::has('errors'))
<?
$estilo = "alert-info";
if (Session::has('msg')) {
    $estilo = 'alert-info';
    $titulo = "Atenção";
    $msg = Session::get('msg');
}
if (Session::has('success')) {
    $titulo = "Sucesso!";
    $estilo = 'alert-success';
    $msg = Session::get('success');
}
if (Session::has('errors')) {
    $titulo = "Erro";
    $estilo = 'alert-danger';
    $msg = '';
    if (is_object(Session::get('errors')) && get_class(Session::get('errors')) == 'Illuminate\Support\MessageBag') {
        $br = '';
        foreach (Session::get('errors')->all() as $error) {
            $msg .= $br . $error;
            $br = '<br/>';
        }
    } elseif (is_array(Session::get('errors'))) {
        $br = '';
        foreach (Session::get('errors') as $error) {
            $msg .= $br . $error;
            $br = '<br/>';
        }
    } else {
        $msg = Session::get('errors');
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-block {{$estilo}} fade in">
            <button type="button" class="close" data-dismiss="alert"></button>
            <h4 class="alert-heading">{{$titulo}}</h4>
            <p>{{$msg}}</p>
        </div>        
    </div>
    <? Session::remove('msg'); ?>
    <? Session::remove('success'); ?>
    <? Session::remove('errors'); ?>
</div>
@endif
<!-- /mensagens -->