@if (Auth::user()->id_nivel < 10 && Auth::user()->id_cliente > 0)
    <div class="cliente-atual page-header-inner bg-grey-cascade" style="padding:3px;font-size:11px;vertical-align: middle; ">
        Você está na empresa: "{{Auth::user()->cliente->razaosocial}} ({{Auth::user()->cliente->id_cliente}} - {{Auth::user()->cliente->nomefantasia}}) "
        @if ((Auth::user()->adm == 1 || Auth::user()->qtdClientes() > 0) && Auth::user()->id_cliente > 0)
        <a class="" href="/publico/usuario/escolherempresa" style="margin:0;padding:2px; font-size:11px; font-weight: bold; padding-left:12px; padding-right:12px; border: solid 1px #cccccc; color:white; margin-left:20px; background-color: #578EBE;">Alterar</a>
        @endif
        <span class="pull-right">Usuário: {{Auth::user()->nomeusuario}}</span>
    </div><!--/container-->
@endif
