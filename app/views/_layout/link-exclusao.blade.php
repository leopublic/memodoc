@if (!isset($msg))
<? $msg = '';?>
@endif
@if (!isset($title))
<? $title = '';?>
@endif
@if (!isset($classe_btn))
<? $classe_btn = 'btn-danger'; ?>
@endif
@if (!isset($onclick))
<? $onclick = ''; ?>
@endif
@if (!isset($link))
<? $link = '#'; ?>
@endif
<div class="btn-group">
    <button class="btn dropdown-toggle {{$classe_btn}}" data-toggle="dropdown" title="{{$title}}" type="button">
        <i class="fa fa-trash-o"></i>
    </button>
    <ul class="dropdown-menu red" role="menu">
        <li><a href="{{$link}}" onclick="{{$onclick}}">{{$msg}}</a></li>
    </ul>
</div>
