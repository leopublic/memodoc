<!-- Inicio portlet -->
<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-sitemap"></i>Adicionar novo</div>
        <div class="tools">
        </div>
    </div>
    <!-- INICIO PORTLET-BODY -->
    <div class="portlet-body form grey-steel filtro">
        {{Form::open(array('url' => '/publico/setor/adicionar', 'class' => 'form-horizontal'))}}
        <div class="form-body">
            <div class="row">
                <!-- Inicio primeira coluna-->                        
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group input-large">
                                {{Form::text('nome', Input::old('nome'), array('class'=>'form-control') )}}
                                <div class="input-group-btn">
                                    <input type="submit" class="btn yellow-gold" class="button" id="adicionaCaixa" value="Adicionar"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
