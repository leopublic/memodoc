	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- BEGIN HORIZONTAL RESPONSIVE MENU -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<ul class="page-sidebar-menu" data-slide-speed="200" data-auto-scroll="true">
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<!-- DOC: This is mobile version of the horizontal menu. The desktop version is defined(duplicated) in the header above -->
				<li>
					<a href="#"><i class="fa fa-archive fa-large"></i>&nbsp;Documentos <span class="arrow"></span></a>
					<ul class="sub-menu">
						<li><a href="{{ URL::to('publico/documento') }}">Pesquisar conteúdo</a></li>
						@if(Auth::user()->id_nivel <= 2)
						<li><a href="{{ URL::to('publico/documento/cadastrar') }}">Cadastrar documentos</a></li>
						@endif
					</ul>
				</li>
				<li>
					<a href="#"><i class="fa fa-gears fa-large"></i> &nbsp;Cadastros <span class="arrow"></span></a>
					<ul class="sub-menu">
						<li><a href="{{ URL::to('publico/centrodecusto') }}">Centros de custo</a></li>
						<li><a href="{{ URL::to('publico/departamento') }}">Departamentos</a></li>
						<li><a href="{{ URL::to('publico/setor') }}">Setores</a></li>
					</ul>
				</li>
				<li>
					<a href="#"><i class="fa fa-truck fa-large"></i> &nbsp;Solicitações<span class="arrow"></span></a>
					<ul class="sub-menu">
						@if(Auth::user()->id_nivel <= 2)
						<li class="inativo"><a href="{{ URL::to('publico/os/encerrarpendente') }}">Encerrar solicitação em aberto</a></li>
						<li class="inativo"><a href="{{ URL::to('publico/os/criar') }}">Abrir solicitação</a></li>
						@endif
						<li class="inativo"><a href="{{ URL::to('publico/os') }}">Consultar solicitações</a></li>
					</ul>
				</li>
				<li>
					<a href="#"><i class="fa fa-lock fa-large"></i> &nbsp;Senha/Usuários <span class="arrow"></span></a>
					<ul class="sub-menu">
						<li class="inativo"><a href="{{ URL::to('publico/usuario/meusdados') }}">Alterar meus dados</a></li>
						@if(Auth::user()->id_nivel==1)
						<li class="inativo"><a href="{{ URL::to('publico/usuario/novo') }}">Cadastrar usuário</a></li>
						@endif
						<li class="inativo"><a href="{{ URL::to('publico/usuario') }}">Listar usuários</a></li>
					</ul>
				</li>
				<li>
					<a href="#"><i class="fa fa-laptop fa-large"></i> &nbsp;Consultas diversas<span class="arrow"></span></a>
					<ul class="sub-menu">
						<li class="inativo"><a href="{{ URL::to('publico/caixa/emconsulta') }}">Listar referências em consulta</a></li>
						<li class="inativo"><a href="{{ URL::to('publico/caixa/naoinventariadas') }}">Listar referências não enviadas para custódia e não inventariadas</a></li>
						<li class="inativo"><a href="{{ URL::to('publico/caixa/inventariadas') }}">Listar referências não enviadas para custódia e inventariadas</a></li>
		                @if (\Auth::user()->cliente->franquiacustodia > 0)
		                <li class="inativo"><a href="{{ URL::to('publico/caixa/emcarencia') }}">Listar referências não enviadas para custódia mas sendo faturadas (em carência)</a></li>
		                @endif
						<li class="inativo"><a href="{{ URL::to('publico/caixa/expurgadas') }}">Listar referências expurgadas</a></li>
						<li class="inativo"><a href="{{ URL::to('publico/reserva') }}">Reservas realizadas</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<!-- END HORIZONTAL RESPONSIVE MENU -->
	</div>
	<!-- END SIDEBAR -->
