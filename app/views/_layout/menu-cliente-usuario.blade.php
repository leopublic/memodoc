<!-- BEGIN TOP NAVIGATION MENU -->
<div class="top-menu">
    <ul class="nav navbar-nav pull-right" style="margin-right:0px;">
        <!-- BEGIN USER LOGIN DROPDOWN -->
        <li class="dropdown dropdown-user">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <i class="fa fa-user"></i>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="/publico/usuario/meusdados"><i class="fa fa-male"></i>Meus dados</a></li>
                <li><a href="/publico/usuario/alterarsenha"><i class="fa fa-lock"></i>Alterar senha</a></li>
            @if (Auth::user()->id_nivel < 10 && (Auth::user()->adm == 1 || Auth::user()->qtdClientes() > 0))
                <li><a href="/publico/usuario/escolherempresa"><i class="fa fa-refresh"></i>Alterar o cliente</a></li>
            @endif
                <li class="divider"></li>
                <li><a href="/auth/logout"><i class="fa fa-power-off"></i>Sair</a></li>
            </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN -->
    </ul>
</div>
<!-- END TOP NAVIGATION MENU -->
