<script type="text/javascript" src="{{URL::to('/assets/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::to('/assets/metronic/global/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js')}}"></script>
<script type="text/javascript" src="{{URL::to('/assets/metronic/global/plugins/bootstrap-daterangepicker/moment.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('/assets/metronic/global/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{URL::to('/assets/metronic/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('/assets/metronic/global/plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('/assets/metronic/global/plugins/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('/assets/metronic/global/plugins/select2/select2_locale_pt-BR.js')}}"></script>
<script type="text/javascript" src="{{URL::to('/assets/metronic/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript" src="{{URL::to('/assets/metronic/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}"></script>
<script src="{{URL::to('/assets/metronic/admin/pages/scripts/components-dropdowns.js')}}"></script>
<script src="{{URL::to('/assets/metronic/admin/pages/scripts/components-pickers.js')}}"></script>

<script>
jQuery(document).ready(function() {
    // initiate layout and plugins
    ComponentsPickers.init();
    ComponentsDropdowns.init();
    $.extend($.inputmask.defaults, {
        'autounmask': true
    });
    $(".mask_date").inputmask("d/m/y", {
        autoUnmask: true
    }); //direct mask     
    $(".mask_mes").inputmask("99", {
        autoUnmask: true
    }); //direct mask     
    $(".mask_ano").inputmask("9999", {
        autoUnmask: true
    }); //direct mask     
});
</script>
