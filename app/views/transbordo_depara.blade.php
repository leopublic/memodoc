<html>
    <head>
        <style>
            td{vertical-align: top}
            th{vertical-align: top; font-weight: bold; text-align:right;}
        </style>
    </head>
    <body>
        <div style='font-family:arial;border:1px solid #000;border-top:0px; width:100%;padding:10px; '>
            <? $qtd = 1; ?>
            <? $i = 1; ?>
            <table style="width:100%; border-collapse: collapse;">
                <tr>
                    <th width="10%" style="text-align:center; color:#828f97;">&nbsp;</th>
                    <th width="40%" style="text-align:center;">&nbsp;</th>
                    <th width="10%" style="text-align:center;">&nbsp;</th>
                    <th width="40%" style="text-align:center;">&nbsp;</th>
                </tr>
                <tr>
                    <th width="10%" style="text-align:center; color:#828f97;">#</th>
                    <th width="40%" style="text-align:center;">De</th>
                    <th width="10%" style="text-align:center;">&nbsp;</th>
                    <th width="40%" style="text-align:center;">Para</th>
                </tr>
                @foreach($transbordo_caixas as $transbordo_caixa)
                <tr>
                    <td  style="text-align:center; border-bottom:solid 1px #828f97; vertical-align: middle; font-size:14px; font-weight: normal; color:#828f97;">{{$i}}</td>
                    <td  style="text-align:center; border-bottom:solid 1px #828f97;">
                        <span style="font-size:25px; font-weight: bold">{{substr('000000'.$transbordo_caixa->id_caixapadrao_antes, -6)}}</span>
                        <br/><span style="font-size:14px; font-weight: normal">{{$transbordo_caixa->enderecoantes->enderecoCompleto()}}</span>
                    </td>
                    <td style="font-size:30px; font-weight: bold; border-bottom:solid 1px #828f97; vertical-align: middle;">=></td>
                    <td style="text-align:center; border-bottom:solid 1px #828f97;">
                        <span style="font-size:25px; font-weight: bold">{{substr('000000'.$transbordo_caixa->id_caixapadrao_depois, -6)}}</span>
                        <br/><span style="font-size:14px; font-weight: normal">{{$transbordo_caixa->enderecodepois->enderecoCompleto()}}</span>
                    </td>
                </tr>
            <? $i++; ?>
                @endforeach
            </table>
    </body>
</html>
