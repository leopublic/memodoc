<tr id="checkin_{{$doc->id_caixapadrao}}" class="{{$checkin->classe}} checkin_{{$doc->id_caixapadrao}}">
	<td style="text-align:center">{{$checkin->numero}}</td>
	<td style="text-align:center">
		@if ($checkin->classe == 'success')
			<button title="Retirar essa caixa do checkin" onclick="removeCheckin('{{$os->id_os_chave}}','{{$doc->id_caixa_fmt}}'); return false;" class="btn btn-mini btn-danger"><i class="aweso-trash"></i></button>
		@else
			<button title="Limpar esse erro" onclick="$(this).closest('tr').remove(); return false;"><i class="aweso-exclamation"></i></button>
		@endif
		<a href="/digitacao/itens/{{ $os->id_os_chave}}/{{$doc->id_caixa}}" target="_blank" title="Fazer digitação de documentos recebidos" class="btn btn-mini bg-violet"><i class="aweso-keyboard"></i></a>
	</td>
	<td style="text-align:center;">
		@if ($doc->id_caixa > 0)
			<a href="/caixa/historico/{{$doc->id_cliente}}/{{$doc->id_caixapadrao}}" target="_blank" class="btn btn-small">{{$doc->id_caixapadrao_fmt}}</a>
		@else
			<a href="javascript:alert('Essa caixa não existe.');" target="_blank" class="btn btn-small">{{$doc->id_caixapadrao_fmt}}</a>
		@endif
	</td>
	<td style="text-align:center;">{{$doc->id_caixa_fmt}}</td>
	<td>
		{{$checkin->msg}}
		@if ($checkin->exibirConfirmacao)
			<button id="btnConfirma{{$doc->id_caixapadrao}}" title="Retirar essa linha" onclick="confirmaInclusaoCheckin(this, '{{$os->id_os_chave}}','{{$doc->id_caixapadrao}}'); return false;">Confirmar recebimento de caixa não emprestada</button>
		@endif
	</td>
	<td style="text-align:center;">--</td>
	<td style="text-align:center;">--</td>
</tr>