@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Ordens de serviço</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')

<!-- inicio conteúdo -->
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-filter"></i>Filtrar por</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form grey-steel filtro">
                {{Form::model(array('class' => 'form-horizontal'))}}
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Responsável</label>
                                <div class="col-md-9">
                                    {{Form::text('responsavel', null ,array('class'=>'form-control fluid'))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Solicitado em</label>
                                <div class="col-md-9">
                                    {{Form::text('solicitado_em', null ,array('class'=>'form-control input-small mask_date'))}}
                                </div>
                            </div>
                        </div>
                        <!-- Inicio segunda coluna-->                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Status</label>
                                <div class="col-md-9">
                                    {{Form::select('id_status_os', array('' => '(todas)', '4' => 'Somente atendidas', '3' => 'Somente não atendidas'), null, array('id'=>'id_status_os', 'class' => 'form-control select2me'))}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fim form-body -->
                <!-- INICIO FORM-ACTIONS -->
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-offset-3 col-md-9">
                                <input type="submit" class="btn yellow-gold" id="submit-pesquisa" value="Buscar"></>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- FIM FORM-ACTIONS -->
                {{ Form::close() }}
            </div>
            <!-- Fim portlet-body -->
        </div>
        <!-- Fim portlet -->
    </div>
    <!-- fim  da coluna unica -->    
</div>

<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-list"></i>Ordens de serviço encontradas</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body grey-steel">
                <div class="row">
                    <!-- Inicio primeira coluna-->                        
                    <div class="col-md-12">
                        @if(count($oss) > 0)
                        {{$oss->links()}}
                        <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                            <thead>
                                <tr>
                                    <th style="width:60px;text-align:center;">Ações</th>
                                    <th style="width:auto;text-align:center;">Número</th>
                                    <th style='width:95px;text-align:center;'>Solicitada em</th>
                                    <th style='width:95px;text-align:center;'>Status</th>
                                    <th style="width:auto;">Solicitante/Endereço</th>
                                    <th style="width:auto;text-align: right;">Caixas<br/>com end.</th>
                                    <th style="width:auto;text-align: right;">Caixas<br/>sem end.</th>
                                    <th style="width:auto;text-align: right;">End.<br/>sem cartonagem</th>
                                    <th style="width:auto;text-align: right;">Retirada<br/>de caixas</th>
                                    <th style="width:auto;text-align: right;">Entrega<br/>de caixas</th>
                                    <th style="width:auto;text-align: center;">Tipo de entrega</th>
                                    <th style="width:130px;text-align: center;">Emergencial</th>
                                    <th>Observações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($oss as $data)
                                <tr>
                                    <td>
                                        @if ($data->id_status_os == 1)
                                        <a title='Detalhes' href="/publico/os/encerrarpendente/" class="btn btn-sm btn-primary">Detalhes</a>
                                        @else
                                        <a title='Detalhes' href="/publico/os/visualizar/{{$data->getKey()}}" class="btn btn-sm btn-primary">Detalhes</a>
                                        @endif
                                    </td>
                                    <td style="text-align: center;">{{substr('000000'.$data->id_os, -6)}}</td>
                                    <td style="text-align: center;">
                                        @if ($data->id_status_os == 1)
                                        --
                                        @else
                                        {{$data->solicitado_em}}
                                        @endif
                                    </td>
                                    <td style="text-align: center;">{{$data->statusos->descricao_cliente}}</td>
                                    <td>{{$data->responsavel}}
                                        <br/><b>Entregar em:</b>
                                        @if(is_object($data->endeentrega))
                                        {{$data->endeentrega->completo()}}
                                        @else
                                        --
                                        @endif

                                    </td>
                                    <td style="text-align: right;">{{$data->qtdenovascaixas}}</td>
                                    <td style="text-align: right;">{{$data->qtd_cartonagens_cliente}}</td>
                                    <td style="text-align: right;">{{$data->qtdenovasetiquetas}}</td>
                                    <td style="text-align: right;">{{$data->qtd_caixas_retiradas_cliente}}</td>
                                    <td style="text-align: right;">{{count($data->referenciasConsulta())}}</td>
                                    <td style="text-align: center;">
                                        @if($data->tipodeemprestimo == 0)
                                        Entregue pela Memodoc
                                        @elseif($data->tipodeemprestimo == 1)
                                        Consulta na Memodoc
                                        @else
                                        Retirada sem frete (portador próprio)
                                        @endif
                                    </td>
                                    <td style="text-align: center;">
                                        @if($data->urgente)
                                        Sim
                                        @else
                                        Não
                                        @endif
                                    </td>
                                    <td>{{nl2br($data->observacao)}}</td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        {{$oss->links()}}
                        @endif
                    </div>
                </div>
            </div>
            <!-- Fim portlet-body -->
        </div>
        <!-- Fim portlet -->
    </div>
    <!-- fim  da coluna unica -->    
</div>
@stop
@section('scripts')
@include('_layout.scripts-forms')
@stop
