<div class="boxbform">
    @if(isset($many))
      {{ Form::model($many) }}  
    @else
      {{ Form::open(array('url'=>$_SERVER['REQUEST_URI'].'?tab=tab_contatos')) }}    
    @endif
    <div class="box">
        <ul class="form"> 
            <li class='subtitle'>
                @if(isset($many))
                    Editar contato
                @else
                    Adicionar contato
                @endif
                {{ Form::submit('Salvar',array('class'=>'btn btn-primary pull-right')) }}
                {{ Form::hidden('tab','tab_contatos')}}
            </li>
            <li>
                {{ Form::label('nome','Nome') }} 
                {{ Form::text('nome') }}
            </li>
            <li>
                {{ Form::label('cargo','Cargo') }} 
                {{ Form::text('cargo') }}
            </li>

        </ul>    
    </div>
    {{ Form::close() }}  
</div>