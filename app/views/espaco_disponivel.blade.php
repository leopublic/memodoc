@extends('stilearn-metro')

@section('conteudo')

<header class="content-header">

    <!-- content title-->
    <div class="page-header"><h1>Total de vagas disponíveis por galpão</h1></div>

</header> <!--/ content header -->


<!-- content page -->
<article class="content-page">
    <!-- main page, you're application here -->
    <div class="main-page">
        <div class="content-inner">
            <!-- row #1 -->
            <div class="row-fluid">
                <!-- span6 -->
                @foreach ($tipodocumentos as $tipodocumento)
                <div class="span6">
                    <!-- widget tdefault -->
                    <div class="widget border-cyan" id="widget-tdefault">
                        <!-- widget header -->
                        <div class="widget-header bg-cyan">
                            <!-- widget icon -->
                            <div class="widget-icon"><i class="aweso-table"></i></div>
                            <!-- widget title -->
                            <h4 class="widget-title">{{ $tipodocumento->descricao }}</h4>
                            <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        </div><!-- /widget header -->

                        <!-- widget content -->
                        <div class="widget-content">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Galpão</th>
                                        <th style="text-align:right;">Total de vagas</th>
                                        <th style="text-align:right;">Vagas bloqueadas</th>
                                        <th style="text-align:right;">Vagas alugadas</th>
                                        <th style="text-align:right;">Vagas disponíveis</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? 
                                        $total = 0;
                                        $total_total = 0;
                                        $total_bloqueado = 0;
                                        $total_alugado = 0;
                                        $total_naousado = 0;
                                    ?>
                                    @foreach ($galpoes as $galpao)
                                    <?php 
                                        $qtd_total = Endereco::totalPorGalpaoTipo($galpao->id_galpao, $tipodocumento->id_tipodocumento);
                                        $qtd_bloqueado = Endereco::bloqueadosPorGalpaoTipo($galpao->id_galpao, $tipodocumento->id_tipodocumento);
                                        $qtd_alugado = Endereco::alugadosPorGalpaoTipo($galpao->id_galpao, $tipodocumento->id_tipodocumento);
                                        $qtd_naousado = Endereco::naousadosPorGalpaoTipo($galpao->id_galpao, $tipodocumento->id_tipodocumento);
                                    ?>
                                    <tr>
                                        <td>{{ $galpao->id_galpao }}</td>
                                        <td style="text-align:right;">{{ number_format($qtd_total, 0, ',', '.') }}</td>
                                        <td style="text-align:right;">{{ number_format($qtd_bloqueado, 0, ',', '.') }}</td>
                                        <td style="text-align:right;">{{ number_format($qtd_alugado, 0, ',', '.') }}</td>
                                        <td style="text-align:right;">{{ number_format($qtd_naousado, 0, ',', '.') }}</td>
                                    </tr>
                                    <?php 
                                        $total += $qtd_total;
                                        $total_bloqueado += $qtd_bloqueado;
                                        $total_alugado += $qtd_alugado;
                                        $total_naousado += $qtd_naousado;
                                    ?>
                                    @endforeach
                                    <tr>
                                        <td style="font-weight: bold;">Total</td>
                                        <td style="text-align:right; font-weight: bold;">{{ number_format($total, 0, ',', '.') }}</td>
                                        <td style="text-align:right; font-weight: bold;">{{ number_format($total_bloqueado, 0, ',', '.') }}</td>
                                        <td style="text-align:right; font-weight: bold;">{{ number_format($total_alugado, 0, ',', '.') }}</td>
                                        <td style="text-align:right; font-weight: bold;">{{ number_format($total_naousado, 0, ',', '.') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- /widget content -->
                    </div> <!-- /widget tdefault -->
                </div><!-- span6 -->
                @endforeach

                <!-- /out of widget demo -->

            </div><!-- /content-inner-->
        </div><!-- /main-page-->

</article> <!-- /content page -->

@stop