@extends('stilearn-metro')

@section('conteudo')

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Consultas de boletos</h1></div>

    <!-- content breadcrumb -->
    <div class='content-action pull-right'>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-search"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Enviar nova consulta</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                    {{ Form::open(array('class'=> 'form-horizontal', 'files'=> true, 'url'=> '/consultaboletos/novaconsulta')) }}
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label">Arquivo</label>
                                <div class="controls">
                                    {{Form::file('arquivo',array('class'=>'input-block-level'))}}
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" class="btn bg-orange">Enviar</button>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
            <!-- List -->
            <table class="listagem table table-striped table-condensed" data-sorter="true" style="width:100%;">
                <colgroup>
                    <col width="80px"/>
                    <col width="50px"/>
                    <col width="180px"/>
                    <col width="auto"/>
                    <col width="100px"/>
                </colgroup>
                <thead>
                    <tr>
                        <th style="text-align:center;">Ações</th>
                        <th style="text-align:center;">ID</th>
                        <th>Data carga</th>
                        <th style="text-align:left;">Arquivo</th>
                        <th style="text-align:center;">Status</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($consultas as $consulta)
                    <tr>
                        <td style="text-align:center;">
                            <a class="btn btn-small" title="Ver registros" href="/consultaboletos/detalhe/{{ $consulta->id_consulta_boletos }}"><i class="aweso-list"></i></a>
                            <a class="btn btn-small" title="Reprocessar arquivo" href="/consultaboletos/processar/{{ $consulta->id_consulta_boletos }}"><i class="aweso-refresh"></i></a>
                        </td>
                        <td style="text-align:center;">{{ $consulta->id_consulta_boletos }}</td>
                        <td style="text-align:center;">{{ $consulta->data_carga }}</td>
                        <td>{{ $consulta->nome_arquivo }}</td>
                        <td style="text-align:center;">&nbsp;</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .c{
        width:200px;
        text-align:center;
    }
</style>
@stop