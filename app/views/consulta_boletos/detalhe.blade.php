@extends('stilearn-metro')

@section('conteudo')

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Registros do arquivo {{ $consulta->nome_arquivo }}</h1></div>

    <!-- content breadcrumb -->
    <div class='content-action pull-right'>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <!-- List -->
            <table class="listagem table table-striped table-condensed table-menor" data-sorter="true" style="width:100%;">
                <colgroup>
                    <col width="80px"/>
                    <col width="auto"/>
                    <col width="180px"/>
                    <col width="180px"/>
                    <col width="100px"/>
                </colgroup>
                <thead>
                    <tr>
                        <th style="text-align:center;">Ações</th>
                        <th style="text-align:left;">Razao social</th>
                        <th style="text-align:center;">Cliente</th>
                        <th style="text-align:center;">Nosso número</th>
                        <th style="text-align:center;">Seu número</th>
                        <th style="text-align:center;">Data entrada</th>
                        <th style="text-align:center;">Data vencimento</th>
                        <th style="text-align:center;">Status</th>
                        <th style="text-align:right;">Valor</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($regs as $reg)
                    <tr>
                        <td style="text-align:center;">
                        </td>
                        <td style="text-align:left;">{{ $reg->razaosocial }}</td>
                        <td style="text-align:center;">{{ $reg->id_cliente }}</td>
                        <td style="text-align:center;">{{ $reg->nosso_numero }}</td>
                        <td style="text-align:center;">{{ $reg->seu_numero }}</td>
                        <td style="text-align:center;">{{ $reg->dt_entrada }}</td>
                        <td style="text-align:center;">{{ $reg->dt_vencimento }}</td>
                        <td style="text-align:center;">{{ $reg->status }}</td>
                        <td style="text-align:right;">{{ number_format($reg->valor,2, ",", ".") }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .c{
        width:200px;
        text-align:center;
    }
</style>
@stop