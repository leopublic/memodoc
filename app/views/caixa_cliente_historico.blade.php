@extends('stilearn-metro')

@section('conteudo')

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Histórico de movimentação da referência {{$id_caixapadrao}} - Situação: {{$situacao}}</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">
            @include('padrao.mensagens')
            @if (count($instancias) > 0)
            <!-- List -->
            <table class="listagem table table-hover table-striped table-condensed" data-sorter="true" style="width:100%">
                <thead>
                    <tr>
                        <th style="text-align: center; width:auto">Direção</th>
                        <th style="text-align: center; width:auto">OS</th>
                        <th style="text-align: center; width:auto;">Solicitante</th>
                        <th style="text-align: center; width:auto;">Solicitada em</th>
                        <th style="text-align: center; width:auto;">Entregue em</th>
                        <th style="text-align: center; width:auto;">Tipo da solicitação</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($instancias as $r)
                            @if ($r->tipo == 'in')
                    <tr style="background-color: #d8ffdf ;">
                        <td style="text-align: left;">
                            <i class="aweso-building"><i class="aweso-arrow-right"></i></i> Enviada à Memodoc
                            @else
                    <tr style="background-color: #fcd1d1;">
                        <td style="text-align: left;">
                            <i class="aweso-building"></i><i class="aweso-arrow-left"></i> Solicitada à Memodoc
                            @endif
                        </td>
                        <td style="text-align: center;"><a href="/publico/os/visualizar/{{$r->id_os_chave}}" class="btn btn-small" target="_blank">{{substr('000000'.$r->id_os, -6)}}</a></td>
                        <td style="text-align: center;">{{$r->responsavel}}</td>
                        <td style="text-align: center;">{{$r->solicitado_em_fmt}}</td>
                        <td style="text-align: center;">{{$r->entregar_em_fmt}}</td>
                        <td style="text-align: center;">
                              @if($r->tipodeemprestimo == 0)
                              Entregue pela Memodoc
                              @elseif($r->tipodeemprestimo == 1)
                              Consulta na Memodoc
                              @else
                              Retirada sem frete (portador próprio)
                              @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
                Nenhuma movimentação encontrada para essa caixa.
            @endif
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .c{
        width:200px;
        text-align:center;
    }
</style>
@stop