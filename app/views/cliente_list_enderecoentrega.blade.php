@extends('stilearn-metro')

@section('conteudo') 

@include('cliente_tab_header')


<!-- #tab_enderecos - Begin Content  -->
<div class="tab-pane" id="tab_EnderecoEntrega">
    <div class="tab">
        <div class="listbox">
            <div class="box">
                <div class='form clearfix'>
                    <div class='subtitle' style="text-align: left;">
                        <h3 class="pull-left"> Endereços de entrega <span class="badge badge-info">{{$enderecos->count()}}</span></h3> 
                        <a href="/cliente/endeentregaedit/{{$id_cliente}}/0#tab_EnderecoEntrega" class="btn pull-right" style="margin-left:5px;"><i class="icon-plus-sign"></i> Novo </a>
                        <a href="/cliente/endeentregaclonarcorr/{{$id_cliente}}" class="btn pull-right" style="margin-left:5px;"><i class="icon-plus-sign"></i> Copiar do corresp. </a>
                        <a href="/cliente/endeentregaclonarprincipal/{{$id_cliente}}" class="btn pull-right" style="margin-left:5px;"><i class="icon-plus-sign"></i> Copiar do principal </a>&nbsp;
                    </div>
                </div>
                <table class='table table-striped table-hover'>
                    <thead>
                    <th style="width:20px;text-align:center">#</th>
                    <th style="width:80px;text-align:center">Ações</th>
                    <th>Logradouro</th>
                    <th>Endereço</th>
                    <th>Número</th>
                    <th>Compl.</th>
                    <th>Bairro</th>
                    <th>Cidade</th>
                    <th>Estado</th>
                    <th>CEP</th>
                    <th>#OS</th>
                    </thead>
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($enderecos as $end)
                        <? $i++; ?>
                        <tr>
                            <td style="text-align:center;"> {{$i}}</td>
                            <td style="text-align:center;"> 
                                <a title='Editar' href="/cliente/endeentregaedit/{{$id_cliente}}/{{$end->id_endeentrega}}#tab_EnderecoEntrega" class="btn btn-mini btn-primary" title="Editar"><i class="icon-white icon-edit"></i></a>
                                <div class="btn-group">
                                    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-danger btn-mini" title="Excluir esse item"><i class="icon-white icon-remove-sign"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a title='Deletar' href="/cliente/endeentregadel/{{$id_cliente}}/{{$end->id_endeentrega}}" class="btn btn-danger" title="Confirmação">Clique aqui para confirmar a exclusão</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td> {{$end->logradouro}}</td>
                            <td> {{$end->endereco}}</td>
                            <td> {{$end->numero}}</td>
                            <td> {{$end->complemento}}</td>
                            <td> {{$end->bairro}}</td>
                            <td> {{$end->cidade}}</td>
                            <td> {{$end->estado}}</td>
                            <td> {{$end->cep}}</td>
                            <td style="text-align: right"> {{number_format($end->qtdOs, 0, ',', '.')}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>                
</div>
<!-- #tab_departamentos - End Content  --> 

@include('cliente_tab_footer')

@stop
