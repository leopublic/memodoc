@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Criar novo usuário</h1>
    </div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">

            @include('padrao.mensagens')

            <!-- widget form horizontal -->
            <div class="widget border-cyan span6" id="widget-horizontal">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-user"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Usuário</h4>
                    <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                    <div class="widget-action color-cyan">
                        <button data-toggle="collapse" data-collapse="#widget-horizontal" class="btn">
                            <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                        </button>
                    </div>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                    {{ Form::model($model, array('id'=>'frmusuario', 'class' => 'form-horizontal')) }}

                    <div class="row-fluid">
                        <div class='span6'>


                            <div class='control-group'>
                                {{Form::label('nomeusuario','Nome usuário',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::text('nomeusuario', null, array('placeholder'=>'nome do pessoa usuário', 'class'=>'input-xlarge','required'))}}
                                </div>
                            </div>
                            <div class='control-group'>
                                {{Form::label('id_nivel','Nível de acesso',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::select('id_nivel', array('' =>'(selecione)') + Nivel::administrativos()->lists('descricao', 'id_nivel') , null, array('placeholder'=>'nível', 'class'=>'input-xlarge'))}}
                                </div>
                            </div>
                            <div class='control-group'>
                                {{Form::label('username','Login',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::text('username', null, array('placeholder'=>'Login', 'class'=>'input-xlarge','required'))}}
                                </div>
                            </div>
                            <div class='control-group'>
                                {{Form::label('password','Senha',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::password('password', array('id'=>'senha','placeholder'=>'Senha', 'class'=>'input-xlarge'))}}
                                </div>
                            </div>

                            <div class='control-group'>
                                {{Form::label('password_confirmation','Confirmação',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::password('password_confirmation', array('id'=>'password_confirmation', 'placeholder'=>'Confirmação da senha', 'class'=>'input-xlarge'))}}
                                </div>
                            </div>
                            <div class='control-group'>
                                {{Form::label('bloqueio','Bloquear em',array('class'=>'control-label'))}}
                                <div class="controls">
                                    <div class="input-append input-append-inline ">
                                        {{Form::text('bloqueio', null ,array('data-fx' =>"datepicker", "class"=>"input-small"))}}
                                        <span class="add-on"><i class="icomo-calendar"></i> </span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button type="reset" class="btn">Cancelar</button>
                    </div>

                    {{Form::close()}}
                </div><!-- /widget content -->
            </div> <!-- /widget form horizontal -->



        </div>
    </div>
</article> <!-- /content page -->

@stop

@section('scripts')
<script>


    $(function() {



    });


</script>
<style>
    .date{
        width:96px;
    }
    .form-horizontal .control-group{
        margin-bottom:5px !important;
    }
</style>
@stop