<div class="boxbform">
    @if(isset($many))
      {{ Form::model($many) }}  
    @else
      {{ Form::open(array('url'=>$_SERVER['REQUEST_URI'].'?tab=tab_departamentos')) }}    
    @endif
    <div class="box">
        <ul class="form"> 
            <li class='subtitle'>
                @if(isset($many))
                    Editar departamento
                @else
                    Adicionar departamento
                @endif
                {{ Form::submit('Salvar',array('class'=>'btn btn-primary pull-right')) }}
                {{ Form::hidden('tab','tab_departamentos')}}
            </li>
            <li>
                {{ Form::label('nome','Nome do departamento') }} 
                {{ Form::text('nome') }}
            </li>
        </ul>    
    </div>
    {{ Form::close() }}  
</div>