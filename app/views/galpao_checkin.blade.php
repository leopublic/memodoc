@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Checkin</h1></div>
    </header> <!--/ content header -->
    
    <!-- content page -->
    <article class="content-page clearfix">        
        <!-- main page -->
        <div class="main-page" id="">
            <div class="content-inner">
                <!-- search box -->
                {{Form::open(array('url' => '/publico/documento/carregacaixa', 'class' => 'form-horizontal'))}}
                <div class="widget border-cyan">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-archive"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Informe o ID da caixa</h4>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content bg-silver">
                        <div class="control-group">
                            <div class='input-append'>
                                {{Form::text('id_caixa', null, array('id'=>'id_caixa'))}}
                               <input type="submit" class="btn bg-cyan" class="button" id="adicionaCaixa" value="Adicionar"/>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}

        @if (Session::has('msg'))
        <div class="alert alert-info"><h4>Mensagem</h4>{{ Session::get('msg') }}</div>
        @endif
        
        @if (Session::has('success'))
        <div class="alert alert-success"><h4>Sucesso!</h4>{{ Session::get('success') }}</div>
        @endif
        
        @if (Session::has('error'))
        <div class="alert alert-error"><h4>Erro!</h4>{{ Session::get('error') }}</div>
        @endif

        @if (isset($caixas))
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">{{$caixas->count()}} documentos encontrados </h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                <table class='table table-striped table-hover table-bordered table-condensed'>
                    <thead>
                        <tr>
                            <th style="width:50px; text-align: center;">ID</th>
                            <th style="width:auto">Cliente</th>
                            <th style="width:350px">Referência</th>
                            <th style="width:auto">Reserva</th>
                            <th>Situação</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($caixas as $caixa)
                            <tr>
                                <td style="text-align: center;">{{ substr("000".$caixa->id_item, -3)}}</td>
                                <td>{{$caixa->titulo}}</td>
                                <td>{{$caixa->conteudo}}</td>
                                <td>{{isset($caixa->setcliente->nome) ? $caixa->setcliente->nome : '' }}</td>
                                <td>{{isset($caixa->depcliente->nome) ? $caixa->depcliente->nome : '' }}</td>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
                
            </div>
            @endif    
            </div>
        </div>
    </article> <!-- /content page -->           
@stop

@section('scripts')

@stop