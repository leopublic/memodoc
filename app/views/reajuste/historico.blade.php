@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Histórico de reajustes por cliente</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-archive"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Informe o cliente</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente', $clientes, Input::old('id_cliente') , array('id'=>'id_cliente','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <input type="submit" class="btn btn-primary" value="Consultar">
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            @if(isset($registros))
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget border-cyan">
                        <!-- widget header -->
                        <div class="widget-header bg-cyan">
                            <!-- widget icon -->
                            <div class="widget-icon"><i class="aweso-archive"></i></div>
                            <!-- widget title -->
                            <h4 class="widget-title">Histórico</h4>
                        </div><!-- /widget header -->

                        <!-- widget content -->
                        <div class="widget-content bg-silver">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th rowspan="2">Ano</th>
                                        <th rowspan="2">Mês</th>
                                        <th rowspan="2" style="text-align:right;">Faturamento mínimo</th>
                                        <th rowspan="2" style="text-align:right;">Cartonagem</th>
                                        <th rowspan="2" style="text-align:right;">Armazenamento</th>
                                        <th colspan="2" style="text-align:center;">Normal</th>
                                        <th colspan="2"  style="text-align:center;">Urgente</th>
                                    </tr>
                                    <tr>
                                        <th style="text-align:right;">Frete</th>
                                        <th style="text-align:right;">Movimentação</th>
                                        <th style="text-align:right;">Frete</th>
                                        <th style="text-align:right;">Movimentação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? $total = 0;?>
                                    @foreach ($registros)
                                        <? $qtd = Endereco::naousadosPorGalpaoTipo($galpao->id_galpao, $tipodocumento->id_tipodocumento); ?>
                                    <tr>
                                        <td>{{ $galpao->id_galpao }}</td>
                                        <td style="text-align:right;">{{ number_format($qtd, 0, ',', '.') }}</td>
                                        <? $total += $qtd; ?>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td style="font-weight: bold;">Total</td>
                                        <td style="text-align:right; font-weight: bold;">{{ number_format($total, 0, ',', '.') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
</article> <!-- /content page -->
@stop
