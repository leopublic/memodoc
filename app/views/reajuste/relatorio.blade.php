<style>
    
    table{ border-collapse: collapse; width:100%; margin:0px; font-family: arial}
    table.comborda tr td{ border:solid 1px black; margin:0px; padding:2px;}
    table.semborda tr td{ border:none;}
    td {border-top: solid 1px #e0e0e0;}
    tr.bg-yellow{background-color: #e0e0e0;}
	.col-esq{
		border-left:solid 1px black !important;
	}  
	
	.col-dir{
		border-right:solid 1px black !important;
	}  

	.border-top{ border-top: solid 1px black;}

	.valor_calc{
		background-color:#cccccc  !important;
	}
	.valor_antes{
		background-color:#cccccc !important;		
	}
	.cliente{page-break-inside:avoid;}
</style>
<body>
<table style="width:100%;">
        <colgroup>
            <col style="width:auto;"/>
            <col style="width:200px;"/>
            <col style="width:300px;"/>
            <col style="width:150px;"/>
            <col style="width:300px;"/>
            <col style="width:150px;"/>
        </colgroup>
	
        <? $primeiro_cli = true; ?>
   @foreach($clientes as $cliente)
   	<tbody class="cliente">
		@if(!$primeiro_cli)   		   	     
        <tr><td colspan="8" style="font-size:8px; border-top: solid 1px black; border-bottom: solid 1px black;">&nbsp;</td></tr>
		@endif	       
        <? $primeiro_cli = false; ?>
        <tr class="bg-yellow" style="color:black !important; margin-top:5px;">
            <td colspan="4"><strong>{{$cliente->razaosocial}}</strong>
                @if ($cliente->fl_ativo != 1)
                <span style="color:red;font-weight:bold; font-size: 12px;">INATIVO</span>
                @endif
                <br/><span style="font-size: 9px">{{$cliente->nomefantasia}} - {{substr('000'.$cliente->id_cliente, -3)}}</span>
            <td colspan="2" style="font-size:9px; text-align: left;">
                Qtd caixas: <strong>{{number_format($cliente->qtdCaixas(), 0, ",", ".")}}</strong>
                <br/>
                Índice: <strong>
                    @if (is_object($cliente->indice))
                    {{$cliente->indice->descricao}}
                    @else
                    (não informado)
                    @endif
                </strong>
            </td>    
            <td colspan="2" style="font-size:9px; text-align: left;">    
                Data contrato: <strong>{{$cliente->formatDate('iniciocontrato')}}</strong>
				<br/>Data último reaj.: <strong>{{$cliente->formatDate('dt_ultimo_reajuste')}}</strong>
            </td>
        </tr>
                                        <tr>
                                        	<th rowspan="2" style="text-align: center; border-bottom: solid 1px black;">Item</th>
                                        	<th rowspan="2" style="text-align: center; border-bottom: solid 1px black;">Mínimo</th>
                                        	<th colspan="2" style="text-align: center; border-bottom: solid 1px black;" class="valor_antes col-esq col-dir">Valor antes do reajuste</th>
                                        	<th colspan="2" style="text-align: center; border-bottom: solid 1px black;" class="valor_calc col-esq col-dir">Valor calculado</th>
                                        	<th colspan="2" style="text-align: center; border-bottom: solid 1px black;" class="valor_atual col-esq ">Valor atual</th>
                                        </tr>
                                        <tr>
                                        	<th style="text-align: center; border-bottom: solid 1px black;" class="valor_antes col-esq">Normal</th>
                                        	<th style="text-align: center; border-bottom: solid 1px black;" class="valor_antes col-dir">Urgente</th>
                                        	<th style="text-align: center; border-bottom: solid 1px black;" class="valor_calc col-esq">Normal</th>
                                        	<th style="text-align: center; border-bottom: solid 1px black;" class="valor_calc col-dir">Urgente</th>
                                        	<th style="text-align: center; border-bottom: solid 1px black;" class="valor_atual col-esq">Normal</th>
                                        	<th style="text-align: center; border-bottom: solid 1px black;" class="valor_atual ">Urgente</th>
                                        </tr>
        <? $primeiro = true; ?>
            @foreach($cliente->cursorReajuste($mes, $ano) as $tipoproduto)
                @if ($primeiro)
                <? $primeiro = false; ?>
                                            <tr class="" style="">
                                                <td>Valor Contrato Mínimo</td>
                                                <td style="text-align: center;">--</td>
                                                <td style="text-align: center;" class="valor_antes col-esq">{{number_format($tipoproduto->valorminimofaturamento_ant,2, ",", ".")}}</td>
                                                <td style="text-align: center;" class="valor_antes col-dir">--</td>
                                                <td style="text-align: center;" class="valor_calc col-esq">{{number_format($tipoproduto->valorminimofaturamento_reajustado,2, ",", ".")}}</td>
                                                <td style="text-align: center;" class="valor_calc col-dir">--</td>
                                                <td style="text-align: center;" class="valor_atual col-esq">{{ number_format($tipoproduto->valorminimofaturamento,2,",",".") }}</td>
                                                <td style="text-align: center;" class="valor_atual">--</td>
                                            </tr>
                @endif
                                            <tr class="">
                                                <td>{{$tipoproduto->descricao}}</td>
                                                <td style="text-align: center;">{{$tipoproduto->minimo}}</td>
                                                <td style="text-align: center;" class="valor_antes col-esq">{{number_format($tipoproduto->valor_contrato_ant,2, ",", ".")}}</td>

                                           	@if($tipoproduto->fl_tem_urgencia == "1")
                                               	<td style="text-align: center;" class="valor_antes col-dir">{{number_format($tipoproduto->valor_urgencia_ant,2, ",", ".")}}</td>
                                           	@else
                                           		<td style="text-align: center;" class="valor_antes col-dir">--</td> 
                                           	@endif

                                                <td style="text-align: center;" class="valor_calc col-esq">{{number_format($tipoproduto->valor_contrato_reajustado,2, ",", ".")}}</td>
                                                
                                           	@if($tipoproduto->fl_tem_urgencia == "1")
                                               	<td style="text-align: center;" class="valor_calc col-dir">{{number_format($tipoproduto->valor_urgencia_reajustado,2, ",", ".")}}</td>
                                           	@else
                                           		<td style="text-align: center;" class="valor_calc col-dir">--</td> 
                                           	@endif
                                                
                                                <td style="text-align: center;" class="valor_atual col-esq">{{ number_format($tipoproduto->valor_contrato, 2, ",", ".")}}</td>
                                                <td style="text-align: center;" class="valor_atual">
                                                	@if($tipoproduto->fl_tem_urgencia == "1")
                                                		{{ number_format($tipoproduto->valor_urgencia,2, ",", ".") }}
                                                	@else
                                                		-- 
                                                	@endif
                                                </td>
                                            </tr>
            @endforeach
    </tbody>
        @endforeach                                        
</table>

