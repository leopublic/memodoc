@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Reajustar clientes</h1>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">
            @include ('padrao/mensagens')
            <!-- widget form horizontal -->
            <div class="row-fluid">
                <div class="widget border-cyan span12" id="widget-horizontal">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <div class="widget-icon"><i class="aweso-dollar"></i></div>
                        <h4 class="widget-title">Informe o período do reajuste</h4>
                        <div class="widget-action">
                        </div>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        {{ Form::open(array('class'=>'form-horizontal')) }}
                        <div class="row-fluid">
                            <div class="span4">
                                <div class='control-group'>
                                    {{Form::label('mes','Mês',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('mes', Input::old('mes', $mes), array('class'=>'input-small'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('ano','Ano',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('ano', Input::old('ano', $ano), array('class'=>'input-small'))}}
                                    </div>
                                </div>
                            </div>
                    @if($mes > 0 && $ano > 0)
                            <div class="span8">
                                @foreach($valores as $indicereajuste)
                                <?php 
                                	$qtd_reajustados = $indicereajuste->clientes_reajustados($mes, $ano);
                                	$qtd_reajustando = $indicereajuste->clientes_reajustando_em($mes, $ano);
                                	$qtd_restantes = $qtd_reajustando - $qtd_reajustados;
                                	if ($qtd_reajustando == 0 && $qtd_restantes == 0){
                                		$cor = "success";
                                	} else {
                                		$cor = "primary";
                                	}
                                ?>
                                
                                <div class='control-group'>
                                    {{Form::label('indice',$indicereajuste->descricao,array('class'=>'control-label'))}}
                                    <div class="controls">
                                    	<span class="input-small uneditable-input " style="text-align: right;">{{ number_format($indicereajuste->percentual, 2, ",", '.') }} </span>
                                        <a href="/tipoproduto/reajustar/{{$ano}}/{{$mes}}/{{$indicereajuste->id_indice}}" class="btn btn-{{$cor}}" > {{$qtd_restantes}}/{{$qtd_reajustando;}} clientes para reajustar nesse mês</a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                    @endif
                        </div>
                        <div class="form-actions bg-silver">
                    @if($mes > 0 && $ano > 0)                    
                            <a href="/tipoproduto/periodo" class="btn btn-success">Voltar</a>
							<a href="/tipoproduto/relatorio/{{$ano}}/{{$mes}}" target="_blank" class="btn btn-primary bg-orange">Relatório geral do mês</a>                    
					@else                            
                            <button type="submit" class="btn btn-primary">Recuperar índices</button>
                    @endif                            
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>

            </div>
        </div>
</article> <!-- /content page -->

@stop
