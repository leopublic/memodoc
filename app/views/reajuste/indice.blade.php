@extends('stilearn-metro')

@section('styles')

.table input[type="text"]{
	margin-bottom: 0px !important;
	text-align:center;
	padding: 2px 3px;
}
.table td{
	padding: 4px;
}
.valor_calc{
	background-color:#cccccc  !important;
}
.valor_antes{
	background-color:#cccccc !important;
	
}
.valor_atual{
}

.col-esq{
	border-left:solid 1px black !important;
}  

.col-dir{
	border-right:solid 1px black !important;
}  

@stop

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Reajustar clientes</h1>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">
            @include ('padrao/mensagens')
            <!-- widget form horizontal -->
            <div class="row-fluid">
                <div class="widget border-cyan span12" id="widget-horizontal">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <div class="widget-icon"><i class="aweso-dollar"></i></div>
                        <h4 class="widget-title">Informe os percentuais para cada índice</h4>
                        <div class="widget-action">
                        </div>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        {{ Form::open(array('class'=>'form-horizontal')) }}
                        {{Form::hidden('mes', $mes, array("id" => 'mes'))}}
                        {{Form::hidden('ano', $ano, array("id" => 'ano'))}}
                        {{Form::hidden('id_indice', $id_indice, array("id" => 'id_indice'))}}
                        <div class="row-fluid">
                            <div class="span4">
                                <div class='control-group'>
                                    {{Form::label('mes','Mês',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('mes', $mes, array('class'=>'input-small', 'disabled'=>'disabled'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('ano','Ano',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('ano', $ano, array('class'=>'input-small', 'disabled'=>'disabled'))}}
                                    </div>
                                </div>
                            </div>
                            <div class="span8">
                                @foreach($valores as $indicereajuste)
                                <div class='control-group'>
                                    {{Form::label('indice', $indicereajuste->indiceent->descricao, array('class'=>'control-label') ) }}
                                    <div class="controls">
                                        {{Form::hidden('id_indicereajuste[]', $indicereajuste->id_indicereajuste)}}
                                        {{Form::text('percentual[]', number_format($indicereajuste->percentual, 2, ",", '.'), array('class'=>'input-small',  'data-fx'=>"masked" , 'data-inputmask'=>"'mask': '[-]999,99', 'greedy': false, 'numericInput': true, 'placeholder':' '", 'style'=>'text-align: right;'))}}
                                        &nbsp;(última atualização em {{$indicereajuste->ultima_atualizacao()}} ) 
                                    </div>
                                </div>
                                @endforeach
                                <div class='control-group'>
                                    {{Form::label('indice',"Mudar para", array('class'=>'control-label') ) }}
                                    <div class="controls">
                                    @foreach($indices as $indice)
                                    	@if($indice->id_indice != $id_indice && $indice->qtd_clientes_restantes($mes, $ano) > 0)
	                                    	<a class="btn btn-primary" href="/tipoproduto/reajustar/{{$ano}}/{{$mes}}/{{$indice->id_indice}}">{{$indice->descricao}} ({{$indice->qtd_clientes_restantes($mes, $ano)}}/{{$indice->clientes_reajustando_em($mes)}})</a>
	                                    @endif
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions bg-silver">
                            <a href="/tipoproduto/periodo" class="btn btn-primary">Alterar período</a>
                            <button type="submit" class="btn btn-primary">Salvar e Recalcular</button>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="widget border-cyan span12" id="widget-horizontal">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <div class="widget-icon"><i class="aweso-dollar"></i></div>
                        <h4 class="widget-title">Clientes com início de contrato em {{$mes}}</h4>
                        <div class="widget-action">
                            <div class="btn-group">
                                <a href="/tipoproduto/relatorio/{{$ano}}/{{$mes}}/{{$id_indice}}" target="_blank" class="btn btn-primary bg-orange">Imprimir</a>
                            </div>
                        </div>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
					@if ($clientes->count() > 0)
                        <div class="row-fluid">
                            <div class="span12">
                                <table class="table">
                                    <colgroup>
                                        <col style="width:auto;"/>
                                        <col style="width:150px;"/>
                                        <col style="width:150px;"/>
                                        <col style="width:90px;"/>
                                        <col style="width:150px;"/>
                                        <col style="width:90px;"/>
                                    </colgroup>
                              @foreach($clientes as $cliente)
                              		@if($cliente->mes_ultimo_reajuste() >= $mes && $cliente->ano_ultimo_reajuste() >= $ano)
                              			<?php $cor = "#e5ffe8;"; 
                              			$reajustado = true;
                              			?> 
                              		@else 
                              			<?php $cor = "";
                              			$reajustado = false;
                              			 ?>                               			
                              		@endif
                                    <tbody id="cliente_{{ $cliente->id_cliente }}" style="background-color:{{ $cor }}">
                                        <tr class="bg-yellow" style="color:black !important;">
                                            <td colspan="4"><strong><a href="/cliente/servicoseditar/{{$cliente->id_cliente}}#tab_servicos" target="_blank">{{$cliente->razaosocial}}</a></strong>
                                                @if ($cliente->fl_ativo != 1)
                                                <strong style="color:red;"> <i class="aweso-hand-right"></i> ATENÇÃO: CLIENTE INATIVO !!!</strong>
                                                @endif
                                                <br/><span style="font-size: 11px">{{$cliente->nomefantasia}} - {{substr('000'.$cliente->id_cliente, -3)}}</span>
                                            <td colspan="5" style="font-size:11px; text-align: right;">    
                                                Qtd caixas: <strong>{{number_format($cliente->qtdCaixas(), 0, ",", ".")}}</strong>
                                                &nbsp;&nbsp;&nbsp;&nbsp;Data contrato: <strong>{{$cliente->formatDate('iniciocontrato')}}</strong>
                                                &nbsp;&nbsp;&nbsp;&nbsp;Índice: <strong>
                                                @if (is_object($cliente->indice))
                                                {{$cliente->indice->descricao}}
                                                @else
                                                (não informado)
                                                @endif
                                                </strong>
                                                &nbsp;&nbsp;&nbsp;&nbsp;Data último reaj.: <strong>{{$cliente->formatDate('dt_ultimo_reajuste')}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                        	<th rowspan="2" style="text-align: center;" >Item</th>
                                        	<th rowspan="2" style="text-align: center;">Mínimo</th>
                                        	<th colspan="2" style="text-align: center;" class="valor_antes col-esq col-dir " >Valor antes do reajuste</th>
                                        	<th colspan="2" style="text-align: center;" class="valor_calc col-esq col-dir " >Valor calculado</th>
                                        	<th style="text-align: center;">&nbsp;</th>
                                        	<th colspan="2" style="text-align: center;" class="valor_atual col-esq " >
                                                @if($cliente->mes_ultimo_reajuste() != $mes || $cliente->ano_ultimo_reajuste() != $ano)
                                                    <span style="color:red;">
                                                @endif
                                                        Valor atual (reajustado em {{$cliente->formatDate('dt_ultimo_reajuste')}})
                                                @if($cliente->mes_ultimo_reajuste() != $mes || $cliente->ano_ultimo_reajuste() != $ano)
                                                    </span>
                                                @endif
                                            </th>
                                        </tr>
                                        <tr>
                                        	<th style="text-align: center;" class="valor_antes col-esq " >Normal</th>
                                        	<th style="text-align: center;" class="valor_antes col-dir " >Urgente</th>
                                        	<th style="text-align: center;" class="valor_calc col-esq " >Normal</th>
                                        	<th style="text-align: center;" class="valor_calc col-dir " >Urgente</th>
                                        	<th style="text-align: center;">&nbsp;</th>
                                        	<th style="text-align: center;" class="valor_atual col-esq " >Normal</th>
                                        	<th style="text-align: center;" class="valor_atual " >Urgente</th>
                                        </tr>
                                        <? $primeiro = true; ?>
                                            @foreach($cliente->cursorReajuste($mes, $ano) as $tipoproduto)
                                            @if ($primeiro)
                                                <? $primeiro = false; ?>
                                            <tr class="" style="">
                                                <td>Valor Contrato Mínimo</td>
                                                <td style="text-align: center;">--</td>
                                                <td style="text-align: center;" class="valor_antes col-esq">{{number_format($tipoproduto->valorminimofaturamento_ant,2, ",", ".")}}</td>
                                                <td style="text-align: center;" class="valor_antes col-dir">--</td>
                                                <td style="text-align: center;" class="valor_calc col-esq"  id="minimo_reajustado">{{number_format($tipoproduto->valorminimofaturamento_reajustado,2, ",", ".")}}</td>
                                                <td style="text-align: center;" class="valor_calc col-dir">--</td>
                                                <td style="text-align: center; vertical-align:middle;" class="" rowspan="6"><button class="btn btn-primary" title="Digitar valores reajustados" onclick="javascript:atualiza_valores('{{ $cliente->id_cliente }}');"><i class="aweso-arrow-right"></i></button></td>
                                                <td style="text-align: center;" class="valor_atual col-esq"><input id="minimo" type="text" value="{{ number_format($tipoproduto->valorminimofaturamento,2,",",".") }}" data-fx="masked"  data-inputmask="'mask': '9.999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true" style="text-align:right;" /></td>
                                                <td style="text-align: center;" class="valor_atual ">--</td>
                                            </tr>
                                            @endif
                                            <tr class="">
                                                <td>{{$tipoproduto->descricao}}</td>
                                                <td style="text-align: center;">{{$tipoproduto->minimo}}</td>
                                                <td style="text-align: center;" class="valor_antes col-esq">{{number_format($tipoproduto->valor_contrato_ant,2, ",", ".")}}</td>

                                           	@if($tipoproduto->fl_tem_urgencia == "1")
                                               	<td style="text-align: center;" class="valor_antes col-dir">{{number_format($tipoproduto->valor_urgencia_ant,2, ",", ".")}}</td>
                                           	@else
                                           		<td style="text-align: center;" class="valor_antes col-dir">--</td> 
                                           	@endif

                                                <td style="text-align: center;" class="valor_calc col-esq" id="tipo_{{ $tipoproduto->id_tipoproduto }}_valor_reajustado">{{number_format($tipoproduto->valor_contrato_reajustado,2, ",", ".")}}</td>
                                                
                                           	@if($tipoproduto->fl_tem_urgencia == "1")
                                               	<td style="text-align: center;" class="valor_calc col-dir" id="tipo_{{ $tipoproduto->id_tipoproduto }}_urgente_reajustado">{{number_format($tipoproduto->valor_urgencia_reajustado,2, ",", ".")}}</td>
                                           	@else
                                           		<td style="text-align: center;" class="valor_calc col-dir" id="tipo_{{ $tipoproduto->id_tipoproduto }}_urgente_reajustado">--</td> 
                                           	@endif
                                                
                                                <td style="text-align: center;" class="valor_atual col-esq">
                                                	<input id="tipo_{{ $tipoproduto->id_tipoproduto }}_valor" type="text" value="{{ number_format($tipoproduto->valor_contrato, 2, ",", ".")}}"  data-fx="masked"  data-inputmask="'mask': '999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true" style="text-align:right;" />
                                               	</td>
                                                <td style="text-align: center;" class="valor_atual ">
                                                	@if($tipoproduto->fl_tem_urgencia == "1")
                                                		<input id="tipo_{{ $tipoproduto->id_tipoproduto }}_urgente" type="text" value="{{ $tipoproduto->valor_urgencia }}"  data-fx="masked"  data-inputmask="'mask': '999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true" style="text-align:right;" />
                                                	@else
                                                		-- 
                                                	@endif
                                                </td>
                                            </tr>
                                            @endforeach
                                            <tr class="">
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td style="text-align: center;">&nbsp;</td>
                                                <td style="text-align: center;">&nbsp;</td>
                                                <td style="text-align: center;">&nbsp;</td>
                                                <td style="text-align: center;">&nbsp;</td>
                                                <td style="text-align: center;">&nbsp;</td>
                                                <td colspan="2" style="text-align: center;">
                                                	<a href="javascript:salvar('{{ $cliente->id_cliente}}');" class="btn btn-primary" id="btnSalvar">Salvar</a>
                                                	<a href="javascript:alert('Aguarde...');" class="btn btn-primary" id="btnSalvando" style="display:none;"><i class="aweso-spin aweso-spinner"></i> Salvando...</a>
                                                </td>
                                            </tr>                                        
                                    </tbody>
                              @endforeach
                                </table>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
</article> <!-- /content page -->

@stop

@section('scripts')
<script>

	function salvar(id_cliente){
		$('#cliente_'+id_cliente+' #btnSalvar').hide();
		$('#cliente_'+id_cliente+' #btnSalvando').show();
		var dados = { id_cliente : id_cliente
					, valor_contrato_1: $('#cliente_'+id_cliente+' #tipo_1_valor').val()
				    , valor_urgencia_1: $('#cliente_'+id_cliente+' #tipo_1_urgente').val()
					, valor_contrato_2: $('#cliente_'+id_cliente+' #tipo_2_valor').val()
				    , valor_urgencia_2: $('#cliente_'+id_cliente+' #tipo_2_urgente').val()
					, valor_contrato_3: $('#cliente_'+id_cliente+' #tipo_3_valor').val()
				    , valor_urgencia_3: $('#cliente_'+id_cliente+' #tipo_3_urgente').val()
					, valor_contrato_4: $('#cliente_'+id_cliente+' #tipo_4_valor').val()
				    , valor_urgencia_4: $('#cliente_'+id_cliente+' #tipo_4_urgente').val()
					, valor_contrato_5: $('#cliente_'+id_cliente+' #tipo_5_valor').val()
				    , valor_urgencia_5: $('#cliente_'+id_cliente+' #tipo_5_urgente').val()
				    , minimo: $('#cliente_'+id_cliente+' #minimo').val()
				    , mes: $('#mes').val()
				    , ano: $('#ano').val()
		};
		var url = "/tipoproduto/atualizarajax";
		$.post(url , dados, function(data){
				$('#cliente_'+id_cliente+' #btnSalvando').hide();
				$('#cliente_'+id_cliente+' #btnSalvar').show();
				console.log(data['ret']);
				if(data.ret == "0"){
					$('#cliente_'+id_cliente).animate({ backgroundColor: "#a5ffb1" }, "fast");
					$('#cliente_'+id_cliente).animate({ backgroundColor: "#e5ffe8" }, "slow");
					alert('Cliente atualizado com sucesso');
				} else {
					alert('Não foi possível atualizar o cliente:'+data.msg);
				}
			}, 'json');
	}

	function atualiza_valores(id_cliente){
		$('#cliente_'+id_cliente+' #minimo').val($('#cliente_'+id_cliente+' #minimo_reajustado').html())
		for (var i = 1; i <= 5; i++) {
		    var selector = '#cliente_'+id_cliente+' #tipo_'+i+'_valor';
			$(selector).val($(selector+'_reajustado').html());
			
		    var selector_u = '#cliente_'+id_cliente+' #tipo_'+i+'_urgente';
			var val_novo = $(selector_u+'_reajustado').html();
			$(selector_u).val(val_novo);
		}		
	}
</script>
@stop