@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Reajustar clientes</h1>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">
            @include ('padrao/mensagens')
            <!-- widget form horizontal -->
            <div class="row-fluid">
                <div class="widget border-cyan span12" id="widget-horizontal">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <div class="widget-icon"><i class="aweso-dollar"></i></div>
                        <h4 class="widget-title">Percentuais informados para cada índice</h4>
                        <div class="widget-action">
                        </div>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        {{ Form::open(array('class'=>'form-horizontal')) }}
                        <div class="row-fluid">
                            <div class="span12">
                                <div class='control-group'>
                                    {{Form::label('indice','IGPM',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('percentual_igpm', null, array('class'=>'input-small', 'disabled'=>'disabled'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('indice','IPCA',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('percentual_ipca', null, array('class'=>'input-small', 'disabled'=>'disabled'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('indice','INPC',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('percentual_inpc', null, array('class'=>'input-small', 'disabled'=>'disabled'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('indice','IGPM-FGV',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('percentual_fgv', $percentual_fgv, array('class'=>'input-small', 'disabled'=>'disabled'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('indice','INCC-M',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('percentual_incc', $percentual_incc, array('class'=>'input-small', 'disabled'=>'disabled'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions bg-silver">
                            <button type="submit" class="btn btn-primary">Simular reajustes</button>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
            
            <div class="row-fluid">
                <div class="widget border-cyan span12" id="widget-horizontal">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <div class="widget-icon"><i class="aweso-dollar"></i></div>
                        <h4 class="widget-title">Clientes com início de contrato em {{$mes}}</h4>
                        <div class="widget-action">
                        </div>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        {{ Form::open(array('class'=>'form-horizontal')) }}
                        <div class="row-fluid">
                            <div class="span12">
                                <table>
                                    <tr>Cliente</tr>
                                    <tr>Índice</tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article> <!-- /content page -->

@stop
