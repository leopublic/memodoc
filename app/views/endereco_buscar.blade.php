@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Alteração de particularidade de endereços</h1>
            
        </div>

        <!-- content breadcrumb -->
        <ul class="breadcrumb breadcrumb-inline clearfix">
            <li><a href="#">Endereço</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
            <li class="active">Alteração</li>
        </ul>
        
        
    </header> <!--/ content header -->
    
    <!-- content page -->
    <article class="content-page clearfix">
        
        <!-- main page -->
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">

                
                @if (Session::has('msg'))
                <div class="alert alert-info"><h4>Mensagem</h4>{{ Session::get('msg') }}</div>
                @endif
                
                @if (Session::has('success'))
                <div class="alert alert-success"><h4>Sucesso!</h4>{{ Session::get('success') }}</div>
                @endif
                
                @if (Session::has('error'))
                <div class="alert alert-error"><h4>Erro!</h4>{{ Session::get('error') }}</div>
                @endif    

        @if(!isset($endereco))        
        <!-- widget form horizontal -->
        <div class="widget border-cyan" id="widget-horizontal">
            <!-- widget header -->
            <div class="widget-header bg-cyan">
                <!-- widget icon -->
                <div class="widget-icon"><i class="aweso-edit"></i></div>
                <!-- widget title -->
                <h4 class="widget-title">Busque o endereço que deseja alterar</h4>
                <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                <div class="widget-action color-cyan">
                    <button data-toggle="collapse" data-collapse="#widget-horizontal" class="btn">
                        <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                    </button>
                </div>
            </div><!-- /widget header -->   
            <!-- widget content -->
            <div class="widget-content">
                {{ Form::model($model, array('class'=>'')) }}
                 <div class='pull-left form-horizontal'>   
                     <div class='control-group'>
                         {{Form::label('id_galpao','Galpão',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_galpao', null, array('placeholder'=>'Número do galpão', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_rua','Rua',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_rua', null, array('placeholder'=>'Número da rua', 'class'=>'input-xlarge','required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_predio','Número Prédio',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_predio', null, array('placeholder'=>'Número do prédio', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_andar','Número andar',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_andar', null, array('placeholder'=>'Número do andar', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>

                     
                 </div>
                   <div class='clearfix'></div>
                   <div class="form-actions bg-silver">
                        <button type="submit" class="btn btn-primary">Buscar endereço</button>
                        
                   </div>
                </div><!-- /widget content -->
                {{Form::close()}}
            </div> <!-- /widget form horizontal -->
            
            @else
            
                <!-- widget form horizontal -->
                <div class="widget border-cyan" id="widget-horizontal">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-edit"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Endereço</h4>
                        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        <div class="widget-action color-cyan">
                            <button data-toggle="collapse" data-collapse="#widget-horizontal" class="btn">
                                <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                            </button>
                        </div>
                    </div><!-- /widget header -->   
                    <!-- widget content -->
                    <div class="widget-content">
                        {{ Form::model($endereco, array('url'=> URL::to('/endereco/editar'))) }}
                         <div class='pull-left form-horizontal'> 
                             <div class="alert alert-info">
                                Este endereço possui ({{$qtd_endereco}}) unidades por andar
                             </div>
                             <div class='control-group'>
                                 {{Form::label('id_galpao','Galpão',array('class'=>'control-label'))}} 
                                 <div class="controls">
                                 {{Form::text('id_galpao', null, array('placeholder'=>'Número do galpão', 'class'=>'input-xlarge', 'required'))}} 
                                 </div>
                             </div>
                             <div class='control-group'>
                                 {{Form::label('id_rua','Rua',array('class'=>'control-label'))}} 
                                 <div class="controls">
                                 {{Form::text('id_rua', null, array('placeholder'=>'Número da rua', 'class'=>'input-xlarge','required'))}} 
                                 </div>
                             </div>
                             <div class='control-group'>
                                 {{Form::label('id_predio','Número Prédio',array('class'=>'control-label'))}} 
                                 <div class="controls">
                                 {{Form::text('id_predio', null, array('placeholder'=>'Número do prédio', 'class'=>'input-xlarge', 'required'))}} 
                                 </div>
                             </div>
                             <div class='control-group'>
                                 {{Form::label('id_andar','Número andar',array('class'=>'control-label'))}} 
                                 <div class="controls">
                                 {{Form::text('id_andar', null, array('placeholder'=>'Número do andar', 'class'=>'input-xlarge', 'required'))}} 
                                 </div>
                             </div>

                              <h4>Informe abaixo as novas propriedades do endereço:</h4>
                            <div class="well">
                               
                                <div class='control-group'>
                                    {{Form::label('id_tipodocumento','Tipo de documento',array('class'=>'control-label'))}} 
                                    <div class="controls">
                                    {{Form::select('id_tipodocumento', $tipodocumento)}} 
                                    </div>
                                </div>
                             <div class='control-group'>
                                 {{Form::label('qtd_unidade','Unidades por andar',array('class'=>'control-label'))}} 
                                 <div class="controls">
                                 {{Form::text('qtd_unidade', null, array('placeholder'=>'Unidades por andar', 'class'=>'input-xlarge', 'required'))}} 
                                 </div>
                             </div>

                            </div>

                         </div>
                           <div class='clearfix'></div>
                           <div class="form-actions bg-silver">
                                <button type="submit" class="btn btn-primary">Salvar alterações</button>

                           </div>
                        </div><!-- /widget content -->
                        {{Form::close()}}
                    </div> <!-- /widget form horizontal -->
            @endif


            </div>
        </div>
    </article> <!-- /content page -->  
    <style>
        .control-group{
            margin-bottom: 0px !important;
        }
    </style>
@stop