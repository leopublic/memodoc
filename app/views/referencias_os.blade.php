<table class='table table-striped  table-condensed' id="emprestimos">
    <thead>
        <tr>
            <th style='width:100px; text-align:center;'>Ação</th>
            <th style='width:100px; text-align:center;'>Referência</th>
            <th style='width:50px; text-align: center;'>Id</th>
            <th style='width:auto; text-align: center;'>&nbsp;</th>
        </tr>
    </thead>
    @if(isset($caixas))
    <tbody class='caixa-content'>
        @foreach($caixas as $caixa)
        <tr>
            <td style="text-align: center;">
                @if ($caixa->id_checkout != '')
                    <a href="#" class="btn btn-success btn-small" disabled="disabled" title="Referência movimentada"><i class="fa fa-check"></i></a>
                 @else
                    @include ('_layout.link-exclusao', array('onclick'=> "removeEmprestimo('".$caixa->id_osdesmembrada."'); return false;", 'msg' => 'Clique para confirmar a exclusão desse empréstimo', 'title'=> 'Clique para excluir o empréstimo'))
                 @endif
            </td>
            <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao, -6)}}</td>
            <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa, -6)}}</td>
            <td>&nbsp;</td>
        </tr>
        @endforeach
    </tbody>
    @endif
</table>
