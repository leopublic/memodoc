@extends('stilearn-metro')

@section('styles')
    table.table-condensed th, table.table-condensed td{
        line-height: 105% !important;
    }
@stop

@section('conteudo')
<!-- content header -->
@if ($perfat->finalizado == 0)
    <? $style = ''; ?>
@else
    <? $style = 'display:none;'; ?>
@endif
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Faturamento {{$perfat->ano."/".$perfat->mes."  (de ".$perfat->formatDate('dt_inicio').' até '.$perfat->formatDate('dt_fim').')'}}</h1></div>

    <ul class="content-action pull-right">
        <li><a class="btn bg-lime " href="{{URL::to('/periodofat/recalcular/'.$perfat->id_periodo_faturamento)}}"><i class="aweso-refresh"></i> Recalcular</a></li>
        <li id="inicia" style="display:none;"><a class="btn bg-lime " onClick="javascript:iniciaGeracaoDemonstrativos({{$perfat->id_periodo_faturamento}})" ><i class="aweso-print" title="Cria todos os demonstrativos"></i> Criar demonstrativos</a></li>
        <li id="cancela" style="display:none;"><a class="btn bg-lime " href="{{URL::to('/periodofat/cancelarprocessamento/'.$perfat->id_periodo_faturamento)}}"><i class="aweso-stop" title="Cria todos os demonstrativos"></i> Cancelar processamento</a></li>
        <li id="link" style="display:none;" class="link"><a class="btn bg-lime" href="{{URL::to('/periodofat/demonstrativo/'.$perfat->id_periodo_faturamento)}}" target="_blank" ><i class="aweso-print" title="Baixa arquivo completo"></i> Demonstrativos correio</a></li>
        <li id="link" style="display:none;" class="link"><a class="btn bg-lime" href="{{URL::to('/periodofat/demonstrativozip/'.$perfat->id_periodo_faturamento)}}" target="_blank" ><i class="aweso-envelope-alt" title="Baixa arquivo completo"></i> Demonstrativos email</a></li>
        <li><a class="btn bg-lime " href="{{URL::to('/periodofat/recarregaprecos/'.$perfat->id_periodo_faturamento)}}" ><i class="aweso-dollar" title="Atualiza os preços do faturamento com os valores atuais"></i> Recarrega preços</a></li>
        <li><a class="btn bg-lime " href="{{URL::to('/faturamento/conferencia/'.$perfat->id_periodo_faturamento)}}" class="btn btn-sm btn-success" style=""><i class="aweso aweso-check"></i> Conferência</a>
        <li><a class="btn bg-lime " href="{{URL::to('/periodofat/sinteticoexcel/'.$perfat->id_periodo_faturamento)}}" target="_blank"><i class="aweso-table" title="Gera sintético em excel"></i></a></li>
    </ul>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            <!-- search box -->

            @include('padrao.mensagens')
            <div class="widget border-cyan" id="gerademo" style="{{$style}}">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-spinner aweso-spin" id="icone"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title" id="status">{{$perfat->status}}</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    <div class="progress progress-info">
                        <div id="progresso" class="bar" style="width: {{$perfat->progresso}}%"></div>
                    </div>
                </div>
            </div>

            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Clientes faturados</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    @if(isset($faturados))
                    <table class='table table-striped table-hover table-bordered table-condensed table-fixed-header' style="font-size:11px;">
                        <thead class="header">
                            <tr>
                                <th colspan="17" style="font-weight: normal;">Cálculo geral em: <span style="font-weight: bold;">{{$perfat->formatDate('dt_recalculo', 'd/m/Y H:i:s')}}</span> por:
                                <span style="font-weight: bold;">
                                @if (is_object($perfat->calculadopor))
                                {{$perfat->calculadopor->nomeusuario}}
                                @else
                                --
                                @endif
                                </span>
                                </th>
                            </tr>
                            <tr>
                                <th style="width:140px;" rowspan="2">Ação</th>
                                <th style="width:auto; " rowspan="2">Cliente</th>
                                <th style="width:auto; text-align: center;" colspan="8">Custódia</th>
                                <th style="width:auto; text-align: center; " colspan="3">Atendimento</th>
                                <th style="width:auto; text-align: center; " colspan="4">Totais</th>
                            </tr>
                            <tr>
                                <th style="width:40px;text-align: right;">Qtd. Cx.<br/>mês ant.</th>
                                <th style="width:40px;text-align: right;">1&ordf; entr.<br/>Cx Nova</th>
                                <th style="width:40px;text-align: right;">Qtd.<br/>Expur.</th>
                                <th style="width:40px;text-align: right;">Qtd<br/>Total</th>
                                <th style="width:40px;text-align: right;">Carênc.<br/>> 90d</th>
                                <th style="width:40px;text-align: right;">Carênc.<br/>< 90d</th>
                                <th style="width:60px;text-align: right;">Contrato Mínimo</th>
                                <th style="width:80px;text-align: right;">Valor<br><span class="diferente">(Qtd)</span></th>

                                <th style="width:65px;text-align: right;">Cart. nova<br><span class="diferente">(Qtd)</span></th>
                                <th style="width:65px;text-align: right;">Frete<br><span class="diferente">(Qtd)</span></th>
                                <th style="width:65px;text-align: right;">Movim.<br><span class="diferente">(Qtd)</span></th>

                                <th style="width:80px;text-align: right;">Base<br>ISS</th>
                                <th style="width:65px;text-align: right;">ISS</th>
                                <th style="width:65px;text-align: right;">Dif.<br>mín.</th>
                                <th style="width:80px;text-align: right;">Faturado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $tot_custodia = 0;
                            $tot_cartonagem = 0;
                            $tot_frete = 0;
                            $tot_mov = 0;
                            $tot_base = 0;
                            $tot_iss = 0;
                            $tot_nota = 0;
                            ?>
                            @foreach($faturados as $cliente)
                            <tr>
                                <td style="text-align: center;">
                                    @if (file_exists('../public/arquivos/faturamento/'.$perfat->id_periodo_faturamento.'/analitico/'.$cliente->id_cliente.".pdf"))
                                    <a href="/arquivos/faturamento/{{$perfat->id_periodo_faturamento}}/analitico/{{$cliente->id_cliente}}.pdf" target="_blank" title="Gerar faturamento" class="btn btn-xs btn-primary"><i class="aweso-download"></i></a>
                                    @else
                                    <a href="/faturamento/criardemonstrativo/{{$cliente->id_faturamento}}" target="_blank" title="Gerar faturamento" class="btn btn-xs btn-primary"><i class="aweso-download"></i></a>
                                    @endif
                                    <a href="/faturamento/visualizar/{{ $cliente->id_faturamento}}" title="Visualizar faturamento" class="btn btn-xs btn-primary"><i class="aweso-folder-open"></i></a>
                                    @include('padrao.link_exclusao', array('msg_confirmacao'=>'Clique para confirmar a retirada desse cliente do faturamento', 'url'=>'/faturamento/excluir/'.$cliente->id_faturamento))
                                </td>
                                <td style="">
                                    {{ $cliente->razaosocial }}
                                    <br/><span class="diferente" style="font-size:10px;">{{ $cliente->nomefantasia }} - {{ substr('000'.$cliente->id_cliente, -3) }}</span>
                                    @if ($cliente->dt_recalculo != $perfat->dt_recalculo)
                                    <br/><span style="color:red">Recalculado em {{ $cliente->dt_recalculo_fmt }} ({{$cliente->recalculado_por}})
                                    @endif
                                </td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_mes_anterior, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_novas, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_expurgos, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_periodo, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_em_carencia, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_nao_cobrada, 0, ",", ".")}}</td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->cliente->valorminimofaturamento, 2, ",", ".")}}
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_custodia_periodo_cobrado, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_custodia_periodo + $cliente->qtd_custodia_em_carencia, 0, ",", ".")}})</span>
                                </td>

                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_cartonagem_enviadas, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_cartonagem_enviadas, 0, ",", ".")}})</span>
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_frete + $cliente->val_frete_urgente, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_frete_dem, 0, ",", ".")}})</span>
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_movimentacao + $cliente->val_movimentacao_urgente, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_movimentacao + $cliente->qtd_movimentacao_urgente, 0, ",", ".")}})</span>
                                </td>

                                <td style="text-align: right;">{{ number_format($cliente->val_servicos_sem_iss , 2, ",", ".")}}</td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_iss, 2, ",", ".")}}
                                    @if ($cliente->cliente->issporfora)
                                    <br/><span class="diferente">(x0,05)</span>
                                    @else
                                    <br/><span class="diferente">(/0,95)</span>
                                    @endif
                                </td>
                                <td style="text-align: right;">{{ number_format($cliente->val_dif_minimo, 2, ",", ".") }}</td>
                                <td style="text-align: right;">{{ number_format($cliente->val_servicos_sem_iss + $cliente->val_iss + $cliente->val_cartonagem_enviadas, 2, ",", ".")}}</td>
                            </tr>
                            <?php
                            $tot_custodia =$tot_custodia+ $cliente->val_custodia_periodo_cobrado;
                            $tot_cartonagem =$tot_cartonagem+ $cliente->val_cartonagem_enviadas;
                            $tot_frete = $tot_frete+$cliente->val_frete + $cliente->val_frete_urgente;
                            $tot_base = $tot_base+$cliente->val_servicos_sem_iss;
                            $tot_iss = $tot_iss+$cliente->val_iss;
                            $tot_mov = $tot_mov+$cliente->val_movimentacao + $cliente->val_movimentacao_urgente;
                            $tot_nota = $tot_nota+$cliente->val_servicos_sem_iss + $cliente->val_iss + $cliente->val_cartonagem_enviadas;
                            ?>
                            @endforeach
                            <tr>
                                <td style="text-align: right;" colspan="9">TOTAL</td>
                                <td style="text-align: right;">{{ number_format($tot_custodia, 2, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($tot_cartonagem, 2, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($tot_frete, 2, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($tot_mov, 2, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($tot_base, 2, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($tot_iss, 2, ",", ".")}}</td>
                                <td style="text-align: right;">--</td>
                                <td style="text-align: right;">{{ number_format($tot_nota, 2, ",", ".")}}</td>
                            </tr>
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>


            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Clientes fora do faturamento</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    @if(isset($naofaturados))
                    <table class='table table-striped table-hover table-bordered table-condensed'>
                        <thead>
                            <tr>
                                <th style="width:80px;text-align: center;">Ação</th>
                                <th style="width:auto; ">Cliente</th>
                                <th style="width:120px;text-align: center;">Início do contrato</th>
                                <th style="width:80px;text-align: center;">Último reajuste</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($naofaturados as $cliente)
                            <tr>
                                <td style="text-align: center;">
                                    <a href="/periodofat/adicionar/{{ $perfat->id_periodo_faturamento }}/{{$cliente->id_cliente}}" title="Incluir cliente no faturamento" class="btn btn-sm btn-primary"><i class="aweso-plus"></i></a>
                                    <a href="/periodofat/inativar/{{$cliente->id_cliente}}" title="Inativar o cliente" class="btn btn-sm btn-primary"><i class="aweso-moon"></i></a>
                                </td>
                                <td style="">{{ $cliente->razaosocial }} <br/><span style="font-size:10px;">{{ $cliente->nomefantasia }} - {{ substr('000'.$cliente->id_cliente, -3) }}</span></td>
                                <td style="text-align: center;">{{ $cliente->formatDate('iniciocontrato')}}</td>
                                <td style="text-align: center;">{{ $cliente->dt_ultimo_reajuste }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
</article> <!-- /content page -->
@stop
@section('scripts')
<script>
    function monitoraGeracaoDemonstrativos(id_periodo_faturamento) {
        var intervalId = setInterval(function () {
                var url = "/periodofat/status/"+id_periodo_faturamento;
                $.getJSON(url, function (data) {
                    var progresso = data.progresso;
                    var status = data.status;
                    $('#progresso').css('width', progresso+'%');
                    $('#status').html(status);
                    if (data.finalizado != 0){
                        clearInterval(intervalId);
                        $('#inicia').css('display', 'inline');
                        $('#cancela').css('display', 'none');
                        if (data.finalizado == 1){
                            $('#link').css('display', 'inline');
                        }
                    }
               });
             }, 2000);
    }

    function iniciaGeracaoDemonstrativos(id_periodo_faturamento) {
        // Chama a geracao em background
        var urlInicia = "/periodofat/criardemonstrativos/"+id_periodo_faturamento;
        $.get(urlInicia);
        $('#progresso').css('width', '0%');
        $('#status').html('Iniciando a geração dos demonstrativos, por favor aguarde...');
        $('#gerademo').css('display', 'block');
        monitoraGeracaoDemonstrativos({{$perfat->id_periodo_faturamento}})

//        location.reload();
    }

    $(document).ready(function(){
       var finalizado = '{{$perfat->finalizado}}'
        $('#inicia').css('display', 'none');
        $('#cancela').css('display', 'none');
        $('.link').css('display', 'none');
       if (finalizado == '0'){
            monitoraGeracaoDemonstrativos({{$perfat->id_periodo_faturamento}})
            $('#cancela').css('display', 'inline');
       } else {
            $('#inicia').css('display', 'inline');
            if (finalizado == '1'){
                $('.link').css('display', 'inline');
            }
       }

    });

</script>
@stop
