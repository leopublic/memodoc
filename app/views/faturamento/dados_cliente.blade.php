<div class="row-fluid">
    <div class="widget border-cyan span12" id="widget-horizontal">
        <div class="widget-header bg-cyan">
            <div class="widget-icon"><i class="aweso-building"></i></div>
            <h4 class="widget-title">Cliente</h4>
            <div class="widget-action">
                <div class="btn-group">
                </div>
            </div>
        </div>
        <?
        $larg_label_cli = 'span5';
        $larg_campo_cli = 'span7';
        ?>

        <!-- widget content -->
        <div class="widget-content form-horizontal">
            <div class="span6" style="font-size:12px;">
                <div class='control-group row-fluid'>
                    {{Form::label('val_movimentacao','Fantasia',array('class'=>'control-label '.$larg_label_cli))}}
                    <div class="controls {{$larg_campo_cli}}">
                        {{Form::text('x', $cliente->nomefantasia.' ('.substr('000'.$cliente->id_cliente, -3).")", array('class'=>'input-block-level','style'=>'text-align:left', 'disabled'=>'disabled'))}}
                    </div>
                </div>
                <div class='control-group row-fluid'>
                    {{Form::label('val_movimentacao','ISS por fora?',array('class'=>'control-label '.$larg_label_cli))}}
                    <div class="controls {{$larg_campo_cli}}">
                        {{Form::text('x', $cliente->issporfora == 1 ? 'Sim' : 'Não', array('class'=>'input-block-level','style'=>'text-align:left', 'disabled'=>'disabled'))}}
                    </div>
                </div>
                <div class='control-group row-fluid'>
                    {{Form::label('val_movimentacao','Início contrato',array('class'=>'control-label '.$larg_label_cli))}}
                    <div class="controls {{$larg_campo_cli}}">
                        {{Form::text('x', $cliente->formatDate('iniciocontrato'), array('class'=>'input-block-level','style'=>'text-align:left', 'disabled'=>'disabled'))}}
                    </div>
                </div>
            </div>
            <div class="span6" style="font-size:12px;">
                <div class='control-group row-fluid'>
                    {{Form::label('val_movimentacao','Faturamento mínimo',array('class'=>'control-label '.$larg_label_cli))}}
                    <div class="controls {{$larg_campo_cli}}">
                        {{Form::text('x', number_format($cliente->valorminimofaturamento, 2, ",", "."), array('class'=>'input-block-level','style'=>'text-align:right', 'disabled'=>'disabled'))}}
                    </div>
                </div>
                <div class='control-group row-fluid'>
                    {{Form::label('val_movimentacao','Franquia moviment. (% do total)',array('class'=>'control-label '.$larg_label_cli))}}
                    <div class="controls {{$larg_campo_cli}}">
                        {{Form::text('x', number_format($cliente->franquiamovimentacao, "0", ",", "."), array('class'=>'input-block-level','style'=>'text-align:left', 'disabled'=>'disabled'))}}
                    </div>
                </div>
                <div class='control-group row-fluid'>
                    {{Form::label('val_movimentacao','Franquia custódia',array('class'=>'control-label '.$larg_label_cli))}}
                    <div class="controls {{$larg_campo_cli}}">
                        {{Form::text('x', number_format($cliente->franquiacustodia, "0", ",", "."), array('class'=>'input-block-level','style'=>'text-align:left', 'disabled'=>'disabled'))}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
