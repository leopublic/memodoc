<!-- Ordens de servico -->
<div class="row-fluid">
    <div class="widget border-cyan span12" id="widget-horizontal">
        <div class="widget-header bg-cyan">
            <div class="widget-icon"><i class="aweso-file"></i></div>
            <h4 class="widget-title">Caixas não cobradas apuradas no período</h4>
            <div class="widget-action">
                <div class="btn-group">
                </div>
            </div>
        </div>

        <!-- widget content -->
        <div class="widget-content">
            {{ Form::model($fat, array('class'=>'form-horizontal')) }}
            <div class="row-fluid">
            <? 
            	$i = 0;
	            $id_caixapadrao_ant = '';
            ?>
                <table class="listagem table table-hover table-condensed" data-sorter="true" style="width:100%; font-size: 11px;">
                    <thead>
                        <tr>
                            <th style="text-align: center; width:40px;">#</th>
                            <th style="text-align: center; width:50px;">ID</th>
                            <th style="text-align: center; width:110px;">Referência</th>
                            <th style="text-align: center; width:180px;">Primeira entrada</th>
                            <th style="text-align: center; width:80px;">Reserva</th>
                            <th style="text-align: center; width:110px;">Data reserva</th>
                            <th style="text-align: center; width:50px;">Item</th>
                            <th style="text-align: left; width:auto;">Título/conteúdo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($fat->naocobradas() as $doc)
                        <tr>
                        @if ($id_caixapadrao_ant != $doc->id_caixapadrao)
                        <? 
                        	$i++;
                        	$id_caixapadrao_ant = $doc->id_caixapadrao;
                        ?>
                            <td style="text-align: center;">{{$i}}</td>
                            <td style="text-align: center;">{{$doc->id_caixa}}</td>
                            <td style="text-align: center;">{{$doc->id_caixapadrao}}</td>
                            <td style="text-align: center;">{{$doc->primeira_entrada}}</td>
                            <td style="text-align: center;"><a href="/reserva/visualizar/{{$doc->id_reserva}}" target="_blank" class="btn btn-mini">{{$doc->id_reserva}}</a></td>
                            <td style="text-align: center;">{{$doc->data_reserva}}</td>
                       @else 
                            <td style="text-align: center;">&nbsp;</td>
                            <td style="text-align: center;">&nbsp;</td>
                            <td style="text-align: center;">&nbsp;</td>
                            <td style="text-align: center;">&nbsp;</td>
                            <td style="text-align: center;">&nbsp;</td>
                            <td style="text-align: center;">&nbsp;</td>
	                       @endif
                            <td style="text-align: center;">{{$doc->id_item}}</td>
                            <td>{{$doc->titulo}}<br/>{{$doc->conteudo}}</td>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
