@extends('stilearn-metro')

@section('styles')
table.table-condensed th, table.table-condensed td{
line-height: 105% !important;
}
@stop

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Calcular a prévia do faturamento</h1></div>

    <ul class="content-action pull-right">
    </ul>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            <!-- search box -->
            @include('padrao.mensagens')
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-chevron-right"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title" id="status">Selecione o cliente</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente', $clientes, Input::old('id_cliente', $id_cliente) , array('id'=>'id_cliente','data-fx'=>'select2', 'required', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        <div class='input-append'>
                            <div class="control-group">
                                {{Form::label('dt_inicio_fmt','Período',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::text('dt_inicio_fmt', Input::old('dt_inicio_fmt', $dt_inicio), array('class'=>'input-medium', 'placeholder'=>"desde", 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                    {{Form::text('dt_fim_fmt', Input::old('dt_fim_fmt', $dt_fim), array('class'=>'input-medium', 'placeholder'=>"até", 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <input type="submit" class="btn btn-primary" value="Calcular">
            @if (isset($fat))                        
                <a href="{{$caminho}}" class="btn btn-primary">Demonstrativo</a>
            @endif                                       
                    </div>
                    {{ Form::close() }}
                </div>                
            </div>
            <?
            $larg_label_cli = 'span5';
            $larg_campo_cli = 'span7';
            $larg_label = 'span5';
            $larg_campo = 'span7';
            $larg_qtd = 'span4';
            $larg_unit = 'span3';
            $larg_tot = 'span5';

            ?>
            
            @if (isset($cliente))
                @include('faturamento.dados_cliente')
            @endif               
            @if (isset($fat))
                @include('faturamento.demonstrativo_online')
            @endif               
            @if (isset($precos))
                @include('faturamento.valores_unitarios')
            @endif               
            @if (isset($fat))
                @include('faturamento.ordens_de_servico')
            @endif               
        </div>
    </div>
</article> <!-- /content page -->
@stop

@section('scripts')
<script>

</script>
@stop
