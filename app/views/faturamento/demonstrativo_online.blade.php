<div class="row-fluid">
    <div class="widget border-cyan span12" id="widget-horizontal">
        <div class="widget-header bg-cyan">
            <div class="widget-icon"><i class="aweso-dollar"></i></div>
            <h4 class="widget-title">Demonstrativo</h4>
            <div class="widget-action">
                <div class="btn-group">
                </div>
            </div>
        </div>

        <!-- widget content -->
        <div class="widget-content form-horizontal">
            <div class="row-fluid">
                <?
                $larg_label = 'span5';
                $larg_campo = 'span7';
                $larg_qtd = 'span4';
                $larg_unit = 'span3';
                $larg_tot = 'span5';
                ?>

                <div class="span6">
                    <div class='row-fluid'>
                        <div class="{{$larg_label}}">&nbsp;</div>
                        <div class="controls controls-row {{$larg_campo}}">
                            <div class="{{$larg_qtd}}" style="text-align: right; font-weight:bold;">Qtd.</div>
                            <div class="{{$larg_tot}}" style="text-align: right; font-weight:bold;">Total(R$)</div>
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('qtd_cartonagem_enviadas','Cartonagens enviadas',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('qtd_cartonagem_enviadas', number_format($fat->qtd_cartonagem_enviadas, 0, ",", "."), array('class'=>$larg_qtd,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                            {{Form::text('val_cartonagem_enviadas', number_format($fat->val_cartonagem_enviadas, 2, ",", "."), array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_movimentacao','Consultas',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('qtd_movimentacao', number_format($fat->qtd_movimentacao + $fat->qtd_movimentacao_urgente, 0, ",", "."), array('class'=>$larg_qtd,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                            {{Form::text('val_movimentacao', number_format($fat->val_movimentacao + $fat->val_movimentacao_urgente, 2, ",", "."), array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_frete','Frete apurado',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('qtd_frete_dem', number_format($fat->qtd_frete_dem, 0, ",", "."), array('class'=>$larg_qtd,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_frete','Frete cobrado',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('qtd_frete', number_format($fat->qtd_frete + $fat->qtd_frete_urgente, 0, ",", "."), array('class'=>$larg_qtd,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                            {{Form::text('val_frete', number_format($fat->val_frete + $fat->val_frete_urgente, 2, ",", "."), array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_custodia','Custódia apurada',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('qtd_custodia', number_format($fat->qtd_custodia_periodo, 0, ",", ".").' + '.number_format($fat->qtd_custodia_em_carencia, 0, ",", "."), array('class'=>$larg_qtd,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                            {{Form::text('val_custodia', number_format($precos->valorUnitarioArmazenamento() * ($fat->qtd_custodia_periodo + $fat->qtd_custodia_em_carencia), 2, ",", "."), array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_custodia','Custódia cobrada',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            <div class="{{$larg_qtd}}">&nbsp;</div>
                            {{Form::text('val_custodia', number_format($fat->val_custodia_periodo_cobrado, 2, ",", "."), array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_custodia','Custódia não cobrada',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('nao_cobrada', number_format($fat->qtd_custodia_nao_cobrada, 0, ",", "."), array('class'=>$larg_qtd,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_custodia','Expurgos',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('qtd_expurgo', number_format($fat->qtd_expurgos, 0, ",", "."), array('class'=>$larg_qtd,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                            {{Form::text('val_expurgo', number_format($fat->val_expurgos, 2, ",", "."), array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>

                </div>
                <div class="span6">
                    <div class='row-fluid'>
                        <div class="span12" style="text-align: right; font-weight:bold;">Totais (R$)</div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('qtd_custodia_em_carencia','Total geral',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls {{$larg_campo}}">
                            {{Form::text('val_total', number_format($fat->val_total, 2, ",", "."), array('class'=>'input-block-level','style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('qtd_custodia_novas','Outros (sem ISS)',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls {{$larg_campo}}">
                            {{Form::text('qtd_custodia_novas', number_format($fat->val_outros, 2, ",", "."), array('class'=>'input-block-level','style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('qtd_expurgos','Base ISS',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls {{$larg_campo}}">
                            {{Form::text('qtd_expurgos', number_format($fat->val_servicos_sem_iss, 2, ",", "."), array('class'=>'input-block-level','style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('qtd_expurgos','ISS',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls {{$larg_campo}}">
                            {{Form::text('qtd_expurgos', number_format($fat->val_iss, 2, ",", "."), array('class'=>'input-block-level','style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('qtd_custodia_periodo','Valor nota de serviços' ,array('class'=>'control-label '.$larg_label))}}
                        <div class="controls {{$larg_campo}}">
                            {{Form::text('qtd_custodia_periodo', number_format($fat->val_totalNota(), 2, ",", "."), array('class'=>'input-block-level','style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('qtd_custodia_em_carencia','Total fatura',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls {{$larg_campo}}">
                            {{Form::text('qtd_custodia_em_carencia', number_format($fat->val_totalFatura(), 2, ",", "."), array('class'=>'input-block-level','style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('qtd_custodia_em_carencia','Dif. mínimo',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls {{$larg_campo}}">
                            {{Form::text('qtd_custodia_em_carencia', number_format($fat->val_dif_minimo, 2, ",", "."), array('class'=>'input-block-level','style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
