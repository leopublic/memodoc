<!-- Ordens de servico -->
<div class="row-fluid">
    <div class="widget border-cyan span12" id="widget-horizontal">
        <div class="widget-header bg-cyan">
            <div class="widget-icon"><i class="aweso-truck"></i></div>
            <h4 class="widget-title">Ordens de serviço (de {{$fat->periodo_faturamento->dt_inicio_fmt}} até {{$fat->periodo_faturamento->dt_fim_fmt}})</h4>
            <div class="widget-action">
                <div class="btn-group">
                </div>
            </div>
        </div>

        <!-- widget content -->
        <div class="widget-content">
            {{ Form::model($fat, array('class'=>'form-horizontal')) }}
            {{ Form::hidden('id_os_chave', null) }}
            <div class="row-fluid">
                <table class="listagem table table-hover table-condensed" data-sorter="true" style="width:100%; font-size: 11px;">
                    <thead>
                        <tr>
                            <th width="6%" rowspan="3" style="text-align: center;">DATA</th>
                            <th width="8%" rowspan="3" style="text-align: center;">N&ordm; DA OS</th>
                            <th width="8%" rowspan="3" style="text-align: center; width:40px;">Urg.</th>
                            <th width="8%" rowspan="3" style="text-align: center; width:40px;">Tipo</th>
                            <th width="8%" rowspan="3" style="text-align: center; width:40px;">Cobra<br>frete?</th>
                            <th colspan="4" style="text-align: center;">ENTREGAS</th>
                            <th colspan="2" style="text-align: center;">APANHA</th>
                            <th rowspan="3" style="text-align: right; width:70px;">Qtd<br/>Frete<br>Apurada</th>
                            <th rowspan="3" style="text-align: right; width:70px;">Qtd<br/>Frete<br>Cobr.</th>
                            <th rowspan="2" colspan="5" style="text-align: right;">Expurgo</th>
                            <th rowspan="3" style="text-align: right; width:80px;">FRETE R$</th>
                            <th rowspan="3" style="text-align: right; width:110px;">CARTON.<br/>+<br/>MOV. R$</th>
                            <th width="7%" rowspan="3" style="text-align: right; ">TOTAL <br/>OS R$</th>
                        </tr>
                        <tr>
                            <th colspan="2" style="text-align: center;">CARTONAGEM NOVA</th>
                            <th colspan="2" style="text-align: center;">CAIXAS P/ CONSULTA</th>
                            <th style="text-align: center;">CAIXAS NOVAS</th>
                            <th style="text-align: center;">CAIXAS CONSULTA</th>
                        </tr>
                        <tr>
                            <th style="text-align: center; width:60px;">QTDE</th>
                            <th style="text-align: right; width:110px;">VALOR R$</th>
                            <th style="text-align: center; width:60px;">QTDE</th>
                            <th style="text-align: right; width:110px;">MOV. R$</th>
                            <th style="text-align: center; width:100px;">QTDE</th>
                            <th style="text-align: center; width:100px;">QTDE</th>
                            <th style="text-align: center; width:60px;">Cortesia mov.</th>
                            <th style="text-align: center; width:60px;">Cortesia</th>
                            <th style="text-align: center; width:60px;">QTDE</th>
                            <th style="text-align: right; width:110px;">VALOR R$</th>
                            <th style="text-align: right; width:110px;">MOV. R$</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        $tot_cart = 0;
                        $tot_val_cart = 0;
                        $tot_cons = 0;
                        $tot_cons_u = 0;
                        $tot_val_cons = 0;
                        $tot_val_cons_u = 0;
                        $tot_apan = 0;
                        $tot_nova = 0;
                        $tot_apan_u = 0;
                        $tot_nova_u = 0;
                        $tot_frete = 0;
                        $tot_frete_u = 0;
                        $tot_cartmov = 0;
                        $tot_cartmov_u = 0;
                        $tot_qtd_frete_dem = 0;
                        $tot_qtd_frete = 0;
                        $tot_os = 0;
                        $tot_os_u = 0;
                        $tot_val_expurgo = 0;
                        $tot_val_mov_expurgo = 0;
                        $tot_qtd_expurgos = 0;
                        ?>

                        @foreach($fat->ordensdeservico as $os)
                        @if ($os->urgente == 1)
                        <tr class="bg-amber">
                            @else
                        <tr>
                            @endif
                            <td style="text-align: center;">{{$os->entregar_em}}</td>
                            <td style="text-align: center;"><a href="/ordemservico/visualizar/{{$os->id_os_chave}}" target="_blank">{{$os->getNumeroFormatado()}}</a></td>
                            <td style="text-align: center;">{{$os->urgente == 1 ? '<i class="aweso-ok"></i>' : '' }}</td>
                            <td style="text-align: center;">
                                @if (intval($os->tipodeemprestimo) == 0)
                                <i class="aweso-truck" title="Entrega MEMODOC"></i>
                                @elseif ($os->tipodeemprestimo == 1)
                                <i class="aweso-home" title="Consulta local"></i>
                                @else
                                <i class="aweso-user" title="Retirado pelo cliente"></i>
                                @endif
                            </td>
                            <td style="text-align: center;">
                                @if ($os->cobrafrete == 1)
                                <i class="aweso-check"></i>
                                @else
                                <i class="aweso-check-empty"></i>
                                @endif
                            </td>
                            <td style="text-align: center;">{{number_format($os->qtdenovascaixas + $os->qtd_cartonagens_cliente, 0, ',', '.') }}</td>
                            <td style="text-align:right;">{{number_format($os->valorcartonagem, 2, ',', '.')}}</td>
                            <td style="text-align: center;">{{number_format($os->qtd_movimentacao, 0, ',', '.') }}</td>
                            <td style="text-align:right;">{{number_format($os->valormovimento, 2, ',', '.') }}</td>
                            <td style="text-align: center;">{{number_format($os->apanhanovascaixas, 0, ',', '.') }}</td>
                            <td style="text-align: center;">{{number_format($os->apanhacaixasemprestadas, 0, ',', '.') }}</td>
                            <td style="text-align:right;">{{number_format($os->qtd_frete_dem, 0, ',', '.')}}</td>
                            <td style="text-align:right;">{{number_format($os->qtd_frete, 0, ',', '.')}}</td>

                            <td style="text-align: center;">
                                @if ($os->expurgo_cortesia_movimentacao() == 1)
                                <i class="aweso-check"></i>
                                @else
                                <i class="aweso-check-empty"></i>
                                @endif
                            </td>
                            <td style="text-align: center;">
                                @if ($os->expurgo_cortesia() == 1)
                                <i class="aweso-check"></i>
                                @else
                                <i class="aweso-check-empty"></i>
                                @endif
                            </td>
                            <td style="text-align:right;">{{number_format($os->qtd_expurgos, 0, ',', '.')}}</td>
                            <td style="text-align:right;">{{number_format($os->val_expurgo, 2, ',', '.')}}</td>
                            <td style="text-align:right;">{{number_format($os->val_mov_expurgo, 2, ',', '.')}}</td>

                            <td style="text-align:right;">{{number_format($os->valorfrete, 2, ',', '.')}}</td>
                            <td style="text-align:right;">{{number_format($os->valormovimento + $os->valorcartonagem + $os->val_mov_expurgo, 2, ',', '.') }}</td>
                            <td style="font-weight: bold; text-align:right;">{{number_format($os->valortotal(), 2, ',', '.') }}</td>
                        </tr>
                        <?
                        $tot_cart += $os->qtdenovascaixas;
                        $tot_val_cart += $os->valorcartonagem;
                        $tot_qtd_frete_dem += $os->qtd_frete_dem;
                        $tot_qtd_frete += $os->qtd_frete;
                        $tot_val_expurgo += $os->val_expurgo;
                        $tot_val_mov_expurgo += $os->val_mov_expurgo;
                        $tot_qtd_expurgos += $os->qtd_expurgos;

                        if ($os->urgente == 1) {
                            $tot_cons_u += $os->qtd_movimentacao;
                            $tot_val_cons_u += $os->valormovimento;
                            $tot_apan_u += $os->apanhacaixasemprestadas;
                            $tot_nova_u += $os->apanhanovascaixas;
                            $tot_frete_u += $os->valorfrete;
                            $tot_cartmov_u += $os->valormovimento + $os->valorcartonagem;
                            $tot_os_u += $os->valortotal();
                        } else {
                            $tot_cons += $os->qtd_movimentacao;
                            $tot_val_cons += $os->valormovimento;
                            $tot_apan += $os->apanhacaixasemprestadas;
                            $tot_nova += $os->apanhanovascaixas;
                            $tot_frete += $os->valorfrete;
                            $tot_cartmov += $os->valormovimento + $os->valorcartonagem + $os->val_mov_expurgo;
                            $tot_os += $os->valortotal();
                        }
                        ?>
                        @endforeach
                        <tr>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align: left; " colspan="5">TOTAL NORMAL</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align: center;">{{number_format($tot_cart, 0, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_val_cart, 2, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align: center;">{{number_format($tot_cons, 0, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_val_cons, 2, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align: center;">{{number_format($tot_nova, 0, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align: center;">{{number_format($tot_apan, 0, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">--</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">--</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">--</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">--</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_qtd_expurgos, 0, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_val_expurgo, 2, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_val_mov_expurgo, 2, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_frete, 2, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_cartmov, 2, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_os, 2, ',', '.') }}</td>
                        </tr>
                        <tr class="bg-amber">
                            <td style="font-weight: bold; text-align: left; " colspan="5">TOTAL URGENTE</td>
                            <td style="font-weight: bold; text-align: center;">--</td>
                            <td style="font-weight: bold; text-align:right;">--</td>
                            <td style="font-weight: bold; text-align: center;">{{number_format($tot_cons_u, 0, ',', '.') }}</td>
                            <td style="font-weight: bold; text-align:right;">{{number_format($tot_val_cons_u, 2, ',', '.') }}</td>
                            <td style="font-weight: bold; text-align: center;">{{number_format($tot_nova_u, 0, ',', '.') }}</td>
                            <td style="font-weight: bold; text-align: center;">{{number_format($tot_apan_u, 0, ',', '.') }}</td>
                            <td style="font-weight: bold; text-align:right;">--</td>
                            <td style="font-weight: bold; text-align:right;">--</td>
                            <td style="font-weight: bold; text-align:right;">--</td>
                            <td style="font-weight: bold; text-align:right;">--</td>
                            <td style="font-weight: bold; text-align:right;">--</td>
                            <td style="font-weight: bold; text-align:right;">--</td>
                            <td style="font-weight: bold; text-align:right;">--</td>
                            <td style="font-weight: bold; text-align:right;">{{number_format($tot_frete_u, 2, ',', '.')}}</td>
                            <td style="font-weight: bold; text-align:right;">{{number_format($tot_cartmov_u, 2, ',', '.') }}</td>
                            <td style="font-weight: bold; text-align:right;">{{number_format($tot_os_u, 2, ',', '.') }}</td>
                        </tr>
                        <tr>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align: left; " colspan="5">TOTAL GERAL</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align: center;">{{number_format($tot_cart, 0, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_val_cart, 2, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align: center;">{{number_format($tot_cons + $tot_cons_u, 0, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_val_cons + $tot_val_cons_u, 2, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align: center;">{{number_format($tot_nova + $tot_nova_u, 0, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align: center;">{{number_format($tot_apan + $tot_apan_u, 0, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_qtd_frete_dem, 0, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_qtd_frete, 0, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">--</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">--</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_qtd_expurgos, 0, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_val_expurgo, 2, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_val_mov_expurgo, 2, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_frete + $tot_frete_u, 2, ',', '.')}}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_cartmov + $tot_cartmov_u, 2, ',', '.') }}</td>
                            <td style="border-top: solid 2px black; font-weight: bold; text-align:right;">{{number_format($tot_os + $tot_os_u, 2, ',', '.') }}</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
