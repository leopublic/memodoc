        </tbody>
    </table>
    <br/>
    <table class="comborda">
        <tr>
            <td width="23%">Total a pagar em Cartonagens Novas</td>
            <td width="5%" class="c">{{ number_format($faturamento->qtd_cartonagem_enviadas, 0, ',', '.') }}</td>
            <td width="10%" class="r">R$ {{ number_format($faturamento->val_cartonagem_enviadas, 2, ',', '.') }}</td>
            <td width="2%" class="sep">&nbsp;</td>
            <td width="23%" class="cab">Total dos Serviços</td>
            <td width="5%" class="r b">R$ {{ number_format($faturamento->val_totalServico(), 2, ',', '.') }}</td>
            <td width="2%"  rowspan="6" class="sep" style="border:none;">&nbsp;</td>
            <td width="30%" rowspan="6" colspan="2" style="border:none; font-size:12px;font-weight: bold; width:250px;">
                @if ($cliente->issporfora)
                &nbsp;
                @else
                Imposto Sobre Serviços já incluído<br/>no preço, foi calculado pela alíquota<br/>de 5% de acordo com a Lei.
                @endif
            </td>
        </tr>
        <tr>
            <td>Total a pagar em Caixas para consulta</td>
            <td class="c">{{ number_format($faturamento->qtd_movimentacao + $faturamento->qtd_movimentacao_urgente , 0, ',', '.') }}</td>
            <td class="r">R$ {{ number_format($faturamento->val_movimentacao + $faturamento->val_movimentacao_urgente , 2, ',', '.') }}</td>
            <td class="sep">&nbsp;</td>
            <td width="15%" class="cab">Outros (com ISS)</td>
            <td width="15%" class="r b">R$ {{ number_format($faturamento->val_outros_com_iss, 2, ',', '.') }}</td>
        </tr>
        <tr>
            <td>Total a pagar em Frete</td>
            <td class="c">{{ number_format($faturamento->qtd_frete_dem, 0, ',', '.') }}</td>
            <td class="r">R$ {{ number_format($faturamento->val_frete + $faturamento->val_frete_urgente, 2, ',', '.') }}</td>
            <td class="sep">&nbsp;</td>
            <td class="cab">&nbsp;</td><td class="r">&nbsp;</td>
        </tr>
        <tr>
            <td>Total a pagar em Guarda e Custódia</td>
            <td class="c">{{ number_format($faturamento->qtd_custodia_periodo + $faturamento->qtd_custodia_em_carencia, 0, ',', '.') }}</td>
            <td class="r">R$ {{ number_format($faturamento->val_custodia_periodo_cobrado, 2, ',', '.') }}</td>
            <td class="sep">&nbsp;</td>
            <td class="cab">Imposto sobre Serviço (5%)</td><td class="r b">R$ {{ number_format($faturamento->val_iss, 2, ',', '.') }}</td>
        </tr>
        <tr>
            <td>Total a pagar em Expurgo</td>
            <td class="c">{{ number_format($faturamento->qtd_expurgos_cobravel, 0, ',', '.') }}</td>
            <td class="r">R$ {{ number_format($faturamento->val_expurgos + $faturamento->val_movimentacao_expurgo, 2, ',', '.') }}</td>
            <td class="sep">&nbsp;</td>
            <td class="cab">&nbsp;</td><td class="r">&nbsp;</td>
        </tr>
        <tr>
            <td>Outros</td>
            <td class="c">{{ number_format($faturamento->qtd_desconto, 0, ',', '.') }}</td>
            <td class="r">R$ {{ number_format($faturamento->val_outros, 2, ',', '.') }}</td>
            <td class="sep">&nbsp;</td>
            <td class="cab">Valor Total Nota de Serviço:</td><td class="r b">R$ {{ number_format($faturamento->val_totalNota(), 2, ',', '.') }}</td>
        </tr>
        <tr>
            <td colspan="2" class="r cab">Total Geral:</td><td class="r b">R$ {{ number_format($faturamento->val_servicos_sem_iss + $faturamento->val_cartonagem_enviadas, 2, ',', '.') }}</td><td class="sep">&nbsp;</td>
            <td class="cab">Valor Total da Fatura:</td><td class="r b">R$ {{ number_format($faturamento->val_totalFatura(), 2, ',', '.') }}</td>
            <td width="2%" class="sep">&nbsp;</td>
            <td class="cab">Dif. de Faturamento Mínimo:</td><td class="r b">R$ {{ number_format($faturamento->val_dif_minimo, 2, ',', '.') }}</td>
        </tr>
    </table>
    @if (isset($obs_desconto) && $obs_desconto != '')
    <p style="margin-top:0px; font-weight: bold;">* (Outros) {{$obs_desconto}}</p>
    @endif
