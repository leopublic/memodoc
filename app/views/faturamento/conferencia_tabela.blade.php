@extends('stilearn-metro')

@section('styles')
    table.table-condensed th, table.table-condensed td{
        line-height: 105% !important;
    }
@stop

@section('conteudo')
<!-- content header -->
@if ($perfat->finalizado == 0)
    <? $style = ''; ?>
@else
    <? $style = 'display:none;'; ?>
@endif
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Faturamento {{$perfat->ano."/".$perfat->mes."  (de ".$perfat->formatDate('dt_inicio').' até '.$perfat->formatDate('dt_fim').')'}}</h1></div>

    <ul class="content-action pull-right">
        <li><a class="btn bg-lime " href="{{URL::to('/periodofat/sinteticoexcel/'.$perfat->id_periodo_faturamento).'/correios/conferir'}}" target="_blank"><i class="aweso-table" title="Gera sintético em excel"></i> Correios</a></li>
        <li><a class="btn bg-lime " href="{{URL::to('/periodofat/sinteticoexcel/'.$perfat->id_periodo_faturamento).'/email/conferir'}}" target="_blank"><i class="aweso-table" title="Gera sintético em excel"></i> Email</a></li>
    </ul>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            <!-- search box -->

            @include('padrao.mensagens')

            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Clientes enviados pelo correio</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    <table class='table table-striped table-hover table-bordered table-condensed table-fixed-header' style="font-size:11px;">
                        <thead class="header">
                            <tr>
                                <th style="width:120px;" rowspan="2">Ação</th>
                                <th style="width:auto; " rowspan="2">Cliente</th>
                                <th style="width:auto; text-align: center;" colspan="6">Custódia</th>
                                <th style="width:auto; text-align: center; " colspan="3">Atendimento</th>
                                <th style="width:auto; text-align: center; " colspan="4">Totais</th>
                            </tr>
                            <tr>
                                <th style="width:40px;text-align: right;">Qtd ant.</th>
                                <th style="width:40px;text-align: right;">Novas</th>
                                <th style="width:40px;text-align: right;">Total</th>
                                <th style="width:40px;text-align: right;">Carênc.</th>
                                <th style="width:60px;text-align: right;">Mínimo</th>
                                <th style="width:60px;text-align: right;">Valor<br><span class="diferente">(Qtd)</span></th>

                                <th style="width:60px;text-align: right;">Cart.<br><span class="diferente">(Qtd)</span></th>
                                <th style="width:60px;text-align: right;">Frete<br><span class="diferente">(Qtd)</span></th>
                                <th style="width:60px;text-align: right;">Movim.<br><span class="diferente">(Qtd)</span></th>

                                <th style="width:60px;text-align: right;">Base<br>ISS</th>
                                <th style="width:50px;text-align: right;">ISS</th>
                                <th style="width:60px;text-align: right;">Dif.<br>mín.</th>
                                <th style="width:60px;text-align: right;">Nota</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($correio as $cliente)
                            <tr id="{{$cliente->id_faturamento}}" name="{{$cliente->id_faturamento}}">
                                <td style="text-align: center;">
                                    <a id="btnunco{{$cliente->id_faturamento}}" href="javascript:desconferir({{$cliente->id_faturamento}});"" title="Clique para desconferir" class="btn btn-sm btn-success" style="@if ($cliente->conferido) @else display:none @endif"><i class="aweso aweso-check"></i></a>
                                    <a id="btnconf{{$cliente->id_faturamento}}" href="javascript:conferir({{$cliente->id_faturamento}});" title="Clique para conferir" class="btn btn-sm btn-warning" style="@if ($cliente->conferido) display:none @endif"><i class="aweso aweso-unchecked"></i></a>
                                    <a id="btnspin{{$cliente->id_faturamento}}" href="#" title="Aguarde..." class="btn btn-sm" style="display:none;"><i class="aweso-spinner aweso-spin"></i></a>
                                </td>
                                <td style="">
                                    {{ $cliente->cliente->razaosocial }}
                                    <br/><span class="diferente" style="font-size:10px;">{{ $cliente->cliente->nomefantasia }} - {{ substr('000'.$cliente->id_cliente, -3) }}</span>
                                    @if ($cliente->dt_recalculo != $perfat->dt_recalculo)
                                    <br/><span style="color:red">Recalculado em {{ $cliente->dt_recalculo_fmt }} ({{$cliente->recalculado_por}})
                                    @endif
                                </td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_mes_anterior, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_novas, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_periodo, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_em_carencia, 0, ",", ".")}}</td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->cliente->valorminimofaturamento, 2, ",", ".")}}
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_custodia_periodo_cobrado, 2, ",", ".") }}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_custodia_periodo + $cliente->qtd_custodia_em_carencia, 0, ",", ".")}})</span>
                                </td>

                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_cartonagem_enviadas, 2, ",", ".") }}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_cartonagem_enviadas, 0, ",", ".")}})</span>
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_frete + $cliente->val_frete_urgente, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_frete_dem, 0, ",", ".")}})</span>
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_movimentacao + $cliente->val_movimentacao_urgente, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_movimentacao + $cliente->qtd_movimentacao_urgente, 0, ",", ".")}})</span>
                                </td>

                                <td style="text-align: right;">{{ number_format($cliente->val_servicos_sem_iss , 2, ",", ".")}}</td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_iss, 2, ",", ".")}}
                                    @if ($cliente->cliente->issporfora)
                                    <br/><span class="diferente">(x0,05)</span>
                                    @else
                                    <br/><span class="diferente">(/0,95)</span>
                                    @endif
                                </td>
                                <td style="text-align: right;">{{ number_format($cliente->val_dif_minimo, 2, ",", ".") }}</td>
                                <td style="text-align: right;">{{ number_format($cliente->val_servicos_sem_iss + $cliente->val_iss + $cliente->val_cartonagem_enviadas, 2, ",", ".") }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Clientes enviados por e-mail</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    <table class='table table-striped table-hover table-bordered table-condensed table-fixed-header' style="font-size:11px;">
                        <thead class="header">
                            <tr>
                                <th style="width:120px;" rowspan="2">Ação</th>
                                <th style="width:auto; " rowspan="2">Cliente</th>
                                <th style="width:auto; text-align: center;" colspan="6">Custódia</th>
                                <th style="width:auto; text-align: center; " colspan="3">Atendimento</th>
                                <th style="width:auto; text-align: center; " colspan="4">Totais</th>
                            </tr>
                            <tr>
                                <th style="width:40px;text-align: right;">Qtd ant.</th>
                                <th style="width:40px;text-align: right;">Novas</th>
                                <th style="width:40px;text-align: right;">Total</th>
                                <th style="width:40px;text-align: right;">Carênc.</th>
                                <th style="width:60px;text-align: right;">Mínimo</th>
                                <th style="width:60px;text-align: right;">Valor<br><span class="diferente">(Qtd)</span></th>

                                <th style="width:60px;text-align: right;">Cart.<br><span class="diferente">(Qtd)</span></th>
                                <th style="width:60px;text-align: right;">Frete<br><span class="diferente">(Qtd)</span></th>
                                <th style="width:60px;text-align: right;">Movim.<br><span class="diferente">(Qtd)</span></th>

                                <th style="width:60px;text-align: right;">Base<br>ISS</th>
                                <th style="width:50px;text-align: right;">ISS</th>
                                <th style="width:60px;text-align: right;">Dif.<br>mín.</th>
                                <th style="width:60px;text-align: right;">Nota</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($email as $cliente)
                            <tr id="{{$cliente->id_faturamento}}" name="{{$cliente->id_faturamento}}">
                                <td style="text-align: center;">
                                    <a id="btnunco{{$cliente->id_faturamento}}" href="javascript:desconferir({{$cliente->id_faturamento}});"" title="Clique para desconferir" class="btn btn-sm btn-success" style="@if ($cliente->conferido) @else display:none @endif"><i class="aweso aweso-check"></i></a>
                                    <a id="btnconf{{$cliente->id_faturamento}}" href="javascript:conferir({{$cliente->id_faturamento}});" title="Clique para conferir" class="btn btn-sm btn-warning" style="@if ($cliente->conferido) display:none @endif"><i class="aweso aweso-unchecked"></i></a>
                                    <a id="btnspin{{$cliente->id_faturamento}}" href="#" title="Aguarde..." class="btn btn-sm" style="display:none;"><i class="aweso-spinner aweso-spin"></i></a>
                                </td>
                                <td style="">
                                    {{ $cliente->cliente->razaosocial }}
                                    <br/><span class="diferente" style="font-size:10px;">{{ $cliente->cliente->nomefantasia }} - {{ substr('000'.$cliente->id_cliente, -3) }}</span>
                                    @if ($cliente->dt_recalculo != $perfat->dt_recalculo)
                                    <br/><span style="color:red">Recalculado em {{ $cliente->dt_recalculo_fmt }} ({{$cliente->recalculado_por}})
                                    @endif
                                </td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_mes_anterior, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_novas, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_periodo, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_em_carencia, 0, ",", ".")}}</td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->cliente->valorminimofaturamento, 2, ",", ".")}}
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_custodia_periodo_cobrado, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_custodia_periodo + $cliente->qtd_custodia_em_carencia, 0, ",", ".")}})</span>
                                </td>

                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_cartonagem_enviadas, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_cartonagem_enviadas, 0, ",", ".")}})</span>
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_frete + $cliente->val_frete_urgente, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_frete_dem, 0, ",", ".")}})</span>
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_movimentacao + $cliente->val_movimentacao_urgente, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_movimentacao + $cliente->qtd_movimentacao_urgente, 0, ",", ".")}})</span>
                                </td>

                                <td style="text-align: right;">{{ number_format($cliente->val_servicos_sem_iss , 2, ",", ".")}}</td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_iss, 2, ",", ".")}}
                                    @if ($cliente->cliente->issporfora)
                                    <br/><span class="diferente">(x0,05)</span>
                                    @else
                                    <br/><span class="diferente">(/0,95)</span>
                                    @endif
                                </td>
                                <td style="text-align: right;">{{ number_format($cliente->val_dif_minimo, 2, ",", ".") }}</td>
                                <td style="text-align: right;">{{ number_format($cliente->val_servicos_sem_iss + $cliente->val_iss + $cliente->val_cartonagem_enviadas, 2, ",", ".")}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</article> <!-- /content page -->
@stop
@section('scripts')
<script>
@if ($id_faturamento)
    $("html, body").animate({ scrollTop: $('#{{$id_faturamento}}').offset().top - 100 }, 1000);
@endif
    function monitoraGeracaoDemonstrativos(id_periodo_faturamento) {
        var intervalId = setInterval(function () {
                var url = "/periodofat/status/"+id_periodo_faturamento;
                $.getJSON(url, function (data) {
                    var progresso = data.progresso;
                    var status = data.status;
                    $('#progresso').css('width', progresso+'%');
                    $('#status').html(status);
                    if (data.finalizado != 0){
                        clearInterval(intervalId);
                        $('#inicia').css('display', 'inline');
                        $('#cancela').css('display', 'none');
                        if (data.finalizado == 1){
                            $('#link').css('display', 'inline');
                        }
                    }
               });
             }, 2000);
    }

    function iniciaGeracaoDemonstrativos(id_periodo_faturamento) {
        // Chama a geracao em background
        var urlInicia = "/periodofat/criardemonstrativos/"+id_periodo_faturamento;
        $.get(urlInicia);
        $('#progresso').css('width', '0%');
        $('#status').html('Iniciando a geração dos demonstrativos, por favor aguarde...');
        $('#gerademo').css('display', 'block');
        monitoraGeracaoDemonstrativos({{$perfat->id_periodo_faturamento}})

//        location.reload();
    }

    $(document).ready(function(){
       var finalizado = '{{$perfat->finalizado}}'
        $('#inicia').css('display', 'none');
        $('#cancela').css('display', 'none');
        $('#link').css('display', 'none');
       if (finalizado == '0'){
            monitoraGeracaoDemonstrativos({{$perfat->id_periodo_faturamento}})
            $('#cancela').css('display', 'inline');
       } else {
            $('#inicia').css('display', 'inline');
            if (finalizado == '1'){
                $('#link').css('display', 'inline');
            }
       }

    });

    function conferir(id_faturamento){
        var url = "/faturamento/conferir/"+id_faturamento;
        $('#btnconf'+id_faturamento).hide();
        $('#btnspin'+id_faturamento).show();
        $.get( url, function( data ) {
          $('#btnunco'+id_faturamento).show();
          $('#btnspin'+id_faturamento).hide();
          $( "#"+id_faturamento ).animate({ backgroundColor: "#00A600" }, "100");;
          $( "#"+id_faturamento ).animate({ backgroundColor: "#fff" }, "500");;
        });
    }

    function desconferir(id_faturamento){
        var url = "/faturamento/desconferir/"+id_faturamento;
        $('#btnunco'+id_faturamento).hide();
        $('#btnspin'+id_faturamento).show();
        $.get( url, function( data ) {
          $('#btnconf'+id_faturamento).show();
          $('#btnspin'+id_faturamento).hide();
          $( "#"+id_faturamento ).animate({ backgroundColor: "#00A600" }, "100");;
          $( "#"+id_faturamento ).animate({ backgroundColor: "#fff" }, "500");;
        });
    }


</script>
@stop
