@if ($os['urgente'] == 1)
<tr class="urgente">
@else
<tr>
@endif
    <td class="c">{{$os['entregar_em_fmt']}}</td>
    <td class="c">{{$os['numeroFormatado']}}</td>
    <td class="c">{{number_format($os['qtdenovascaixas'] + $os['qtd_cartonagens_cliente'], 0, ',', '.') }}</td>
    <td class="r">R$ {{number_format($os['valorcartonagem'], 2, ',', '.')}}</td>
    <td class="c">{{number_format($os['qtd_movimentacao'], 0, ',', '.') }}</td>
    <td class="r">R$ {{number_format($os['valormovimento'], 2, ',', '.') }}</td>
    <td class="c">{{number_format($os['apanhanovascaixas'], 0, ',', '.') }}</td>
    <td class="c">{{number_format($os['apanhacaixasemprestadas'], 0, ',', '.') }}</td>
    <td class="r">{{number_format($os['qtd_expurgos'], 0, ',', '.')}}</td>
    <td class="r">R$ {{number_format($os['val_expurgo'], 2, ',', '.')}}</td>
    <td class="r">R$ {{number_format($os['valorfrete'], 2, ',', '.')}}</td>
    <td class="r">R$ {{number_format($os['val_mov_expurgo'] + $os['valormovimento'] + $os['valorcartonagem'], 2, ',', '.') }}</td>
    <td class="r" style="font-weight: bold;">R$ {{number_format($os['valorcartonagem'] + $os['valorfrete'] + $os['val_expurgo'] + $os['valormovimento'] + $os['val_mov_expurgo'], 2, ',', '.') }}</td>
</tr>
