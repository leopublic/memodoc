@extends('stilearn-metro')

@section('styles')
    table.table-condensed th, table.table-condensed td{
        line-height: 105% !important;
    }
@stop

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Faturamentos por cliente</h1></div>

    <ul class="content-action pull-right">
    </ul>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            <!-- search box -->
            @include('padrao.mensagens')
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-chevron-right"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title" id="status">Selecione o cliente</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente', $clientes, $id_cliente , array('id'=>'id_cliente','data-fx'=>'select2', 'required', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <input type="submit" class="btn btn-primary" value="Buscar">
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
@if (isset($id_cliente) && $id_cliente > 0)
            <div class="row-fluid">
                <div class="widget border-cyan span12" id="widget-horizontal">
                    <div class="widget-header bg-cyan">
                        <div class="widget-icon"><i class="aweso-building"></i></div>
                        <h4 class="widget-title">Cliente</h4>
                        <div class="widget-action">
                            <div class="btn-group">
                            </div>
                        </div>
                    </div>
                    <?
                    $larg_label_cli = 'span5';
                    $larg_campo_cli = 'span7';
                    ?>

                    <!-- widget content -->
                    <div class="widget-content form-horizontal">
                        <div class="span6" style="font-size:12px;">
                            <div class='control-group row-fluid'>
                                {{Form::label('val_movimentacao','Fantasia',array('class'=>'control-label '.$larg_label_cli))}}
                                <div class="controls {{$larg_campo_cli}}">
                                    {{Form::text('x', $cliente->nomefantasia.' ('.substr('000'.$cliente->id_cliente, -3).")", array('class'=>'input-block-level','style'=>'text-align:left', 'disabled'=>'disabled'))}}
                                </div>
                            </div>
                            <div class='control-group row-fluid'>
                                {{Form::label('val_movimentacao','ISS por fora?',array('class'=>'control-label '.$larg_label_cli))}}
                                <div class="controls {{$larg_campo_cli}}">
                                    {{Form::text('x', $cliente->issporfora == 1 ? 'Sim' : 'Não', array('class'=>'input-block-level','style'=>'text-align:left', 'disabled'=>'disabled'))}}
                                </div>
                            </div>
                            <div class='control-group row-fluid'>
                                {{Form::label('val_movimentacao','Início contrato',array('class'=>'control-label '.$larg_label_cli))}}
                                <div class="controls {{$larg_campo_cli}}">
                                    {{Form::text('x', $cliente->formatDate('iniciocontrato'), array('class'=>'input-block-level','style'=>'text-align:left', 'disabled'=>'disabled'))}}
                                </div>
                            </div>
                        </div>
                        <div class="span6" style="font-size:12px;">
                            <div class='control-group row-fluid'>
                                {{Form::label('val_movimentacao','Faturamento mínimo',array('class'=>'control-label '.$larg_label_cli))}}
                                <div class="controls {{$larg_campo_cli}}">
                                    {{Form::text('x', number_format($cliente->valorminimofaturamento, 2, ",", "."), array('class'=>'input-block-level','style'=>'text-align:right', 'disabled'=>'disabled'))}}
                                </div>
                            </div>
                            <div class='control-group row-fluid'>
                                {{Form::label('val_movimentacao','Franquia moviment. (% do total)',array('class'=>'control-label '.$larg_label_cli))}}
                                <div class="controls {{$larg_campo_cli}}">
                                    {{Form::text('x', number_format($cliente->franquiamovimentacao, "0", ",", "."), array('class'=>'input-block-level','style'=>'text-align:left', 'disabled'=>'disabled'))}}
                                </div>
                            </div>
                            <div class='control-group row-fluid'>
                                {{Form::label('val_movimentacao','Franquia custódia',array('class'=>'control-label '.$larg_label_cli))}}
                                <div class="controls {{$larg_campo_cli}}">
                                    {{Form::text('x', number_format($cliente->franquiacustodia, "0", ",", "."), array('class'=>'input-block-level','style'=>'text-align:left', 'disabled'=>'disabled'))}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endif
            
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Faturamentos</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    @if(isset($faturamentos))
                    <table class='table table-striped table-hover table-bordered table-condensed table-fixed-header' style="font-size:11px;">
                        <thead class="header">
                            <tr>
                                <th style="width:130px;" rowspan="2">Ação</th>
                                <th style="width:auto; " rowspan="2">Faturamento</th>
                                <th style="width:auto; text-align: center;" colspan="9">Custódia</th>
                                <th style="width:auto; text-align: center; " colspan="3">Atendimento</th>
                                <th style="width:auto; text-align: center; " colspan="4">Totais</th>
                            </tr>
                            <tr>
                                <th style="width:40px;text-align: right;">Qtd ant.</th>
                                <th style="width:40px;text-align: right;">Novas</th>
                                <th style="width:40px;text-align: right;">Exp. total</th>
                                <th style="width:40px;text-align: right;">Exp. usadas</th>
                                <th style="width:40px;text-align: right;">Total</th>
                                <th style="width:40px;text-align: right;">Carênc.<br/>> 90d</th>
                                <th style="width:40px;text-align: right;">Carênc.<br/>< 90d</th>
                                <th style="width:60px;text-align: right;">Mínimo</th>
                                <th style="width:60px;text-align: right;">Valor<br><span class="diferente">(Qtd)</span></th>

                                <th style="width:45px;text-align: right;">Cart.<br><span class="diferente">(Qtd)</span></th>
                                <th style="width:60px;text-align: right;">Frete<br><span class="diferente">(Qtd)</span></th>
                                <th style="width:60px;text-align: right;">Movim.<br><span class="diferente">(Qtd)</span></th>

                                <th style="width:60px;text-align: right;">Base<br>ISS</th>
                                <th style="width:50px;text-align: right;">ISS</th>
                                <th style="width:60px;text-align: right;">Dif.<br>mín.</th>
                                <th style="width:60px;text-align: right;">Nota</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($faturamentos as $cliente)
                            <tr>
                                <td style="text-align: center;">
                                    <a href="/faturamento/recalcularparalistaporcliente/{{$cliente->id_faturamento}}" title="Recalcular" class="btn btn-sm btn-primary"><i class="aweso-refresh"></i></a>
                                    <a href="/faturamento/criardemonstrativo/{{$cliente->id_faturamento}}" target="_blank" title="Gerar faturamento" class="btn btn-sm btn-primary"><i class="aweso-download"></i></a>
                                    <a href="/faturamento/visualizar/{{ $cliente->id_faturamento}}/1" target="_blank" title="Visualizar faturamento" class="btn btn-sm btn-primary"><i class="aweso-folder-open"></i></a>
                                </td>
                                <td style="">
                                    {{$cliente->periodofaturamento->ano."/".$cliente->periodofaturamento->mes."  (de ".$cliente->periodofaturamento->formatDate('dt_inicio').' até '.$cliente->periodofaturamento->formatDate('dt_fim').')'}}
                                    @if ($cliente->dt_recalculo != $cliente->periodofaturamento->dt_recalculo)
                                    <br/><span style="color:red">Recalculado em {{ $cliente->dt_recalculo_fmt }} ({{$cliente->recalculado_por}})
                                    @endif
                                </td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_mes_anterior, 0, ",", ".")}}</td>
                                <td style="text-align: right;">+{{ number_format($cliente->qtd_custodia_novas, 0, ",", ".")}}</td>
                                <td style="text-align: right;">-{{ number_format($cliente->qtd_expurgos, 0, ",", ".")}}</td>
                                <td style="text-align: right;">-{{ number_format($cliente->qtd_expurgos_usadas, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_periodo, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_em_carencia, 0, ",", ".")}}</td>
                                <td style="text-align: right;">{{ number_format($cliente->qtd_custodia_nao_cobrada, 0, ",", ".")}}</td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->cliente->valorminimofaturamento, 2, ",", ".")}}
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_custodia_periodo_cobrado, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_custodia_periodo + $cliente->qtd_custodia_em_carencia, 0, ",", ".")}})</span>
                                </td>

                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_cartonagem_enviadas, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_cartonagem_enviadas, 0, ",", ".")}})</span>
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_frete + $cliente->val_frete_urgente, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_frete_dem, 0, ",", ".")}})</span>
                                </td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_movimentacao + $cliente->val_movimentacao_urgente, 2, ",", ".")}}
                                    <br/><span class="diferente">({{ number_format($cliente->qtd_movimentacao + $cliente->qtd_movimentacao_urgente, 0, ",", ".")}})</span>
                                </td>

                                <td style="text-align: right;">{{ number_format($cliente->val_servicos_sem_iss , 2, ",", ".")}}</td>
                                <td style="text-align: right;">
                                    {{ number_format($cliente->val_iss, 2, ",", ".")}}
                                </td>
                                <td style="text-align: right;">{{ number_format($cliente->val_dif_minimo, 2, ",", ".") }}</td>
                                <td style="text-align: right;">{{ number_format($cliente->val_servicos_sem_iss + $cliente->val_iss + $cliente->val_cartonagem_enviadas, 2, ",", ".")}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>


            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Rastreamento de custódia</h4>
                </div><!-- /widget header -->


        </div>
</article> <!-- /content page -->
@stop
@section('scripts')
<script>

</script>
@stop
