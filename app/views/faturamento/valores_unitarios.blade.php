<div class="row-fluid">
    <div class="widget border-cyan span12" id="widget-horizontal">
        <div class="widget-header bg-cyan">
            <div class="widget-icon"><i class="aweso-tags"></i></div>
            <h4 class="widget-title">Valores unitários</h4>
            <div class="widget-action">
                <div class="btn-group">
                </div>
            </div>
        </div>

        <!-- widget content -->
        <div class="widget-content form-horizontal">
            <div class="row-fluid">
                <div class="span6">
                    <div class='row-fluid'>
                        <div class="{{$larg_label}}">&nbsp;</div>
                        <div class="controls controls-row {{$larg_campo}}">
                            <div class="{{$larg_qtd}}" style="text-align: right; font-weight:bold;">Normal (R$)</div>
                            <div class="{{$larg_tot}}" style="text-align: right; font-weight:bold;">Urgente (R$)</div>
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_unit_cartonagem','Cartonagem',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('val_unit_cartonagem', number_format($precos->valorUnitarioCartonagem(), 2, ",", "."), array('class'=>$larg_qtd,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                            {{Form::text('val_unit_cartonagem', '--', array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_unit_mov','Movimentação',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('val_unit_mov', number_format($precos->valorUnitarioMovimentacao(), 2, ",", "."), array('class'=>$larg_qtd,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                            {{Form::text('val_unit_mov', number_format($precos->valorUnitarioMovimentacao(1), 2, ",", "."), array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_frete','Frete ',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('val_unit_mov', number_format($precos->valorUnitarioFrete(), 2, ",", "."), array('class'=>$larg_qtd,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                            {{Form::text('val_unit_mov', number_format($precos->valorUnitarioFrete(1), 2, ",", "."), array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                </div>
                <div class="span6">
                    <div class='row-fluid'>
                        <div class="{{$larg_label}}">&nbsp;</div>
                        <div class="controls controls-row {{$larg_campo}}">
                            <div class="{{$larg_tot}}" style="text-align: right; font-weight:bold;">Normal (R$)</div>
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_custodia','Custódia',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('val_unit_mov', number_format($precos->valorUnitarioArmazenamento(), 2, ",", "."), array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_custodia','Expurgos',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('val_unit_mov', number_format($precos->valorUnitarioExpurgo(), 2, ",", "."), array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group row-fluid'>
                        {{Form::label('val_custodia','Mínimo frete',array('class'=>'control-label '.$larg_label))}}
                        <div class="controls controls-row {{$larg_campo}}">
                            {{Form::text('val_unit_mov', number_format($precos->qtdMinimoFrete(), 0, ",", "."), array('class'=>$larg_tot,'style'=>'text-align:right', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
