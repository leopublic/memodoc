<style>
    table{ border-collapse: collapse; width:100%; margin:0px;}
    table.comborda tr td{ border:solid 1px black; margin:0px; padding:2px;}
    table.semborda tr td{ border:none;}
    td.l {text-align:left;}
    td.c {text-align:center;}
    td.r {text-align:right;}
    table thead td{font-weight:bold; background-color: #e0e0e0;}
    table tr td.cab{font-weight:bold; background-color: #e0e0e0;}
    table tr td.sep{border-top:none; border-bottom:none;}
    table tr td.sem{border:none; }
    table tr td.b {font-weight: bold;}
    table tr.urgente td {background-color: #e0e0e0;}
</style>
<body>
    <table class="comborda">
        <tr>
            <td width="28%">Total de caixas custodiadas (mês anterior)</td><td width="10%" class="r">{{ number_format($faturamento->qtd_custodia_mes_anterior, 0, ',', '.') }}</td>
            <td width="24%" class="sem">&nbsp;</td>
            <td width="28%">Retorno de consulta</td><td width="10%" class="r">{{ number_format($faturamento->qtd_retornos_de_consulta, 0, ',', '.') }}</td>
        </tr>
        <tr>
            <td>Caixas novas (1&ordf; Entrada)</td><td class="r">{{ number_format($faturamento->qtd_custodia_novas, 0, ',', '.') }}</td>
            <td class="sem">&nbsp;</td>
            <td>Total de cartonagens enviadas</td><td class="r">{{ number_format($faturamento->qtd_cartonagem_enviadas, 0, ',', '.') }}</td>
        </tr>
        <tr>
            <td>Expurgo de caixas no período</td><td class="r">{{ number_format($faturamento->qtd_expurgos, 0, ',', '.') }}</td>
            <td class="sem">&nbsp;</td>
            <td rowspan="2">Caixas reservadas que ainda não deram entrada na custódia (na franquia)</td><td class="r" rowspan="2">{{ number_format($faturamento->qtd_custodia_nao_cobrada, 0, ',', '.') }}</td>
        </tr>
        <tr>
            <td>Total de caixas custodiadas</td><td class="r">{{ number_format($faturamento->qtd_custodia_periodo, 0, ',', '.') }}</td>
            <td class="sem">&nbsp;</td>
        </tr>
        <tr>
            <td>Excedente de franquia (a ser cobrado)</td><td class="r">{{ number_format($faturamento->qtd_custodia_em_carencia, 0, ',', '.')  }}</td>
            <td class="sem">&nbsp;</td>
            <td class="sem" colspan="2" style="font-weight:bold;">Período: {{ Carbon::createFromFormat('Y-m-d', $faturamento->dt_inicio)->format('d/m/Y') }} a {{ Carbon::createFromFormat('Y-m-d', $faturamento->dt_fim)->format('d/m/Y') }}</td>
        </tr>
    </table>
    <br/>
    <table class="comborda os">
        <thead>
            <tr>
                <td width="6%" rowspan="3" style="text-align: center;">DATA</td>
                <td width="8%" rowspan="3" style="text-align: center;">N&ordm; DA OS</td>
                <td colspan="4" style="text-align: center;">ENTREGAS</td>
                <td colspan="2" style="text-align: center;">APANHA</td>
                <td colspan="2" rowspan="2" style="text-align: center; width:80px;">EXPURGO</td>
                <td rowspan="3" style="text-align: center; width:80px;">FRETE</td>
                <td rowspan="3" style="text-align: center; width:110px;">CARTONAGEM<br/>+<br/>MOVIMENTAÇÃO</td>
                <td width="7%" rowspan="3" style="text-align: center; width:110px;">TOTAL DA OS</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">CARTONAGEM NOVA</td>
                <td colspan="2" style="text-align: center;">CAIXAS P/ CONSULTA</td>
                <td style="text-align: center;">CAIXAS NOVAS</td>
                <td style="text-align: center;">CAIXAS CONSULTA</td>
            </tr>
            <tr>
                <td style="text-align: center; width:60px;">QTDE</td>
                <td style="text-align: center; width:100px;">VALOR</td>
                <td style="text-align: center; width:60px;">QTDE</td>
                <td style="text-align: center; width:110px;">MOVIMENTAÇÃO</td>
                <td style="text-align: center; width:100px;">QTDE</td>
                <td style="text-align: center; width:60px;">QTDE</td>
                <td style="text-align: center; width:60px;">QTDE</td>
                <td style="text-align: center; width:100px;">VALOR</td>
            </tr>
        </thead>
        <tbody>
