@extends('stilearn-metro')

@section("styles")
table tr.total td{
    font-weight:bold;
    padding-top:10px;
    border-top-width:2px;
}
@stop

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Rateio de caixas por departamento, setor, centro de custos</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-archive"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Informe o cliente</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente', $clientes, Input::old('id_cliente') , array('id'=>'id_cliente','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                @if (isset($cliente))
                    <div class='control-group'>
                        {{Form::label('','Total de caixas',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('total', number_format($total_caixas, 0, ",", "."), array('class' => 'input-block-level', 'disabled'))}}
                        </div>
                    </div>
                @endif
                    <div class="form-actions bg-silver">
                        <input type="submit" name="adicionar" class="btn btn-primary" value="Buscar">
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            @if (isset($cliente))
                @include("caixa/tabela_rateio")
            @endif
        </div>
    </div>
</article> <!-- /content page -->
@stop