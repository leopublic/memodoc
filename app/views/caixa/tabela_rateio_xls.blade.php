<html>
    <head>
        <style>
            table tr td {border: thin solid #000; vertical-align: top;}
            table tr.total td {font-weight: bold; border-top: 1px solid #000;}
            table tr td.titulo {border: none; vertical-align: top; font-size:18px; font-weight: bold;}
            table tr th {border: thin solid #000; vertical-align: middle; }
        </style>
            
    </head>
<body>
    <table class="table table-condensed table-hover">
        <tr>
            <td colspan="10"  class="titulo">{{$cliente->razaosocial}} ({{$cliente->id_cliente}} - {{$cliente->nomefantasia}})</td>
        </tr>
        <tr>
            <td  class="titulo">&nbsp;</td>
        </tr>
        <tr>
            <td  colspan="3" class="titulo">Departamentos</td>
        </tr>
        <tr>
            <th>Departamento</th>
            <th style="text-align:right;">Qtd de caixas</th>
            <th style="text-align:right;">%</th>
        </tr>
        <? $total = 0; ?>
        @foreach($departamentos as $dep)
        <? $total+= $dep->qtd; ?>
        <tr>
            <td>{{$dep->departamentos}}</td>
            <td style="text-align:right;">{{number_format($dep->qtd, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format(($dep->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
        </tr>
        @endforeach
        <tr>
            <td>(sem departamento)</td>
            <td style="text-align:right;">{{number_format($sem_departamento[0]->qtd, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format(($sem_departamento[0]->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
        </tr>
        <? $total+= $sem_departamento[0]->qtd; ?>
        <tr class="total">
            <td>TOTAL</td>
            <td style="text-align:right;">{{number_format($total, 0, ",", ".")}}</td>
            <td style="text-align:right;">100,0</td>
        </tr>

        <tr>
            <td  class="titulo">&nbsp;</td>
        </tr>
        <tr>
            <td  colspan="3" class="titulo">Centros de custo</td>
        </tr>
        <tr>
            <th>Centro de custo</th>
            <th style="text-align:right;">Qtd de caixas</th>
            <th style="text-align:right;">%</th>
        </tr>
        <? $total = 0; ?>
        @foreach($centros as $centro)
        <? $total+= $centro->qtd; ?>
        <tr>
            <td>{{$centro->centros}}</td>
            <td style="text-align:right;">{{number_format($centro->qtd, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format(($centro->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
        </tr>
        @endforeach
        <tr>
            <td>(sem centro de custo)</td>
            <td style="text-align:right;">{{number_format($sem_centro[0]->qtd, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format(($sem_centro[0]->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
        </tr>
        <? $total+= $sem_centro[0]->qtd; ?>
        <tr class="total">
            <td>TOTAL</td>
            <td style="text-align:right;">{{number_format($total, 0, ",", ".")}}</td>
            <td style="text-align:right;">100,0</td>
        </tr>

        <tr>
            <td  class="titulo">&nbsp;</td>
        </tr>
        <tr>
            <td  colspan="3" class="titulo">Setores</td>
        </tr>
        <tr>
            <th>Setor</th>
            <th style="text-align:right;">Qtd de caixas</th>
            <th style="text-align:right;">%</th>
        </tr>
        <? $total = 0; ?>
        @foreach($setores as $setor)
        <? $total+= $setor->qtd; ?>
        <tr>
            <td>{{$setor->setores}}</td>
            <td style="text-align:right;">{{number_format($setor->qtd, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format(($setor->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
        </tr>
        @endforeach
        <tr>
            <td>(sem setor)</td>
            <td style="text-align:right;">{{number_format($sem_setor[0]->qtd, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format(($sem_setor[0]->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
        </tr>
        <? $total+= $sem_setor[0]->qtd; ?>
        <tr class="total">
            <td>TOTAL</td>
            <td style="text-align:right;">{{number_format($total, 0, ",", ".")}}</td>
            <td style="text-align:right;">100,0</td>
        </tr>
    </table>
</body>
</html>