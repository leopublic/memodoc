<html>
    <head>
        <style>
            table tr td {border: thin solid #000; vertical-align: top;}
            table tr.total td {font-weight: bold; border-top: 1px solid #000;}
            table tr td.titulo {border: none; vertical-align: top; font-size:18px; font-weight: bold;}
            table tr th {border: thin solid #000; vertical-align: middle; }
        </style>
            
    </head>
<body>
    <table class="table table-condensed table-hover">
        <tr>
            <td colspan="3"  class="titulo">{{$cliente->razaosocial}} ({{$cliente->id_cliente}} - {{$cliente->nomefantasia}})</td>
        </tr>
        <tr>
            <td colspan="3"  class="titulo">Faturamento do período entre {{$perfat->dt_inicio_fmt}} e {{$perfat->dt_fim_fmt}}</td>
        </tr>
        <tr>
            <td  class="titulo">&nbsp;</td>
        </tr>
        <tr>
            <td  colspan="3" class="titulo">Departamentos</td>
        </tr>
        <tr>
            <th>Departamento</th>
            <th style="text-align:right;">Qtd de caixas</th>
            <th style="text-align:right;">Valor caixa mês {{number_format($valor, 2, ",", ".")}}</th>
            <th style="text-align:right;">Com ISS (5%)</th>
        </tr>
        <? $total = 0; ?>
        <? $totalvalor = 0; ?>
        <? $totalvalorcomiss = 0; ?>
        @foreach($departamentos as $dep)
        <? $total+= $dep->qtd; ?>
        <? $totalvalor+= $dep->valor; ?>
        <? $totalvalorcomiss+= $dep->valorcomiss; ?>
        <tr>
            <td>{{$dep->departamentos}}</td>
            <td style="text-align:right;">{{number_format($dep->qtd, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($dep->valor, 2, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($dep->valorcomiss, 2, ",", ".")}}</td>
        </tr>
        @endforeach
        <tr class="total">
            <td>TOTAL</td>
            <td style="text-align:right;">{{number_format($total, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($totalvalor, 2, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($totalvalorcomiss, 2, ",", ".")}}</td>
        </tr>

        <tr>
            <td  class="titulo">&nbsp;</td>
        </tr>
        <tr>
            <td  colspan="3" class="titulo">Centros de custo</td>
        </tr>
        <tr>
            <th>Centro de custo</th>
            <th style="text-align:right;">Qtd de caixas</th>
            <th style="text-align:right;">Valor caixa mês {{number_format($valor, 2, ",", ".")}}</th>
            <th style="text-align:right;">Com ISS (5%)</th>
        </tr>
        <? $total = 0; ?>
        <? $totalvalor = 0; ?>
        <? $totalvalorcomiss = 0; ?>
        @foreach($centros as $centro)
            <? $total+= $centro->qtd; ?>
            <? $totalvalor+= $centro->valor; ?>
            <? $totalvalorcomiss+= $centro->valorcomiss; ?>
            <tr>
                <td>{{$centro->centros}}</td>
                <td style="text-align:right;">{{number_format($centro->qtd, 0, ",", ".")}}</td>
                <td style="text-align:right;">{{number_format($centro->valor, 2, ",", ".")}}</td>
                <td style="text-align:right;">{{number_format($centro->valorcomiss, 2, ",", ".")}}</td>
            </tr>
        @endforeach
        <tr class="total">
            <td>TOTAL</td>
            <td style="text-align:right;">{{number_format($total, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($totalvalor, 2, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($totalvalorcomiss, 2, ",", ".")}}</td>
        </tr>

        <tr>
            <td  class="titulo">&nbsp;</td>
        </tr>
        <tr>
            <td  colspan="3" class="titulo">Setores</td>
        </tr>
        <tr>
            <th>Setor</th>
            <th style="text-align:right;">Qtd de caixas</th>
            <th style="text-align:right;">Valor caixa mês {{number_format($valor, 2, ",", ".")}}</th>
            <th style="text-align:right;">Com ISS (5%)</th>
        </tr>
        <? $total = 0; ?>
        <? $totalvalor = 0; ?>
        <? $totalvalorcomiss = 0; ?>
        @foreach($setores as $setor)
            <? $total+= $setor->qtd; ?>
            <? $totalvalor+= $setor->valor; ?>
            <? $totalvalorcomiss+= $setor->valorcomiss; ?>
            <tr>
                <td>{{$setor->setores}}</td>
                <td style="text-align:right;">{{number_format($setor->qtd, 0, ",", ".")}}</td>
                <td style="text-align:right;">{{number_format($setor->valor, 2, ",", ".")}}</td>
                <td style="text-align:right;">{{number_format($setor->valorcomiss, 2, ",", ".")}}</td>
            </tr>
        @endforeach
        <tr class="total">
            <td>TOTAL</td>
            <td style="text-align:right;">{{number_format($total, 0, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($totalvalor, 2, ",", ".")}}</td>
            <td style="text-align:right;">{{number_format($totalvalorcomiss, 2, ",", ".")}}</td>
        </tr>
    </table>
</body>
</html>