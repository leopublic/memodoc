<div class="row-fluid">
    <div class="span4">
        <div class="widget border-cyan">
            <div class="widget-header bg-cyan">
                <div class="widget-icon"><i class="aweso-archive"></i></div>
                <h4 class="widget-title">Departamentos</h4>
            </div><!-- /widget header -->
            <div class="widget-content">
                <table class="table table-condensed table-hover">
                <colgroup>
                    <col width="auto"/>
                    <col width="150px"/>
                    <col width="80px"/>
                </colgroup>
                <thead>
                    <tr>
                        <th>Departamento</th>
                        <th style="text-align:right;">Qtd de caixas</th>
                        <th style="text-align:right;">%</th>
                    </tr>
                </thead>
                <tbody>
                    <? $total = 0; ?>
                    @foreach($departamentos as $dep)
                    <? $total+= $dep->qtd; ?>
                    <tr>
                        <td>{{$dep->departamentos}}</td>
                        <td style="text-align:right;">{{number_format($dep->qtd, 0, ",", ".")}}</td>
                        <td style="text-align:right;">{{number_format(($dep->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td>(sem departamento)</td>
                        <td style="text-align:right;">{{number_format($sem_departamento[0]->qtd, 0, ",", ".")}}</td>
                        <td style="text-align:right;">{{number_format(($sem_departamento[0]->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
                    </tr>
                    <? $total+= $sem_departamento[0]->qtd; ?>
                    <tr class="total">
                        <td>TOTAL</td>
                        <td style="text-align:right;">{{number_format($total, 0, ",", ".")}}</td>
                        <td style="text-align:right;">100,0</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="span4">
        <div class="widget border-cyan">
            <div class="widget-header bg-cyan">
                <div class="widget-icon"><i class="aweso-archive"></i></div>
                <h4 class="widget-title">Centros de custo</h4>
            </div><!-- /widget header -->
            <div class="widget-content">
                <table class="table table-condensed table-hover">
                <colgroup>
                    <col width="auto"/>
                    <col width="150px"/>
                    <col width="80px"/>
                </colgroup>
                <thead>
                    <tr>
                        <th>Centro de custo</th>
                        <th style="text-align:right;">Qtd de caixas</th>
                        <th style="text-align:right;">%</th>
                    </tr>
                </thead>
                <tbody>
                    <? $total = 0; ?>
                    @foreach($centros as $centro)
                    <? $total+= $centro->qtd; ?>
                    <tr>
                        <td>{{$centro->centros}}</td>
                        <td style="text-align:right;">{{number_format($centro->qtd, 0, ",", ".")}}</td>
                        <td style="text-align:right;">{{number_format(($centro->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td>(sem centro de custo)</td>
                        <td style="text-align:right;">{{number_format($sem_centro[0]->qtd, 0, ",", ".")}}</td>
                        <td style="text-align:right;">{{number_format(($sem_centro[0]->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
                    </tr>
                    <? $total+= $sem_centro[0]->qtd; ?>
                    <tr class="total">
                        <td>TOTAL</td>
                        <td style="text-align:right;">{{number_format($total, 0, ",", ".")}}</td>
                        <td style="text-align:right;">100,0</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="span4">
        <div class="widget border-cyan">
            <div class="widget-header bg-cyan">
                <div class="widget-icon"><i class="aweso-archive"></i></div>
                <h4 class="widget-title">Setores</h4>
            </div><!-- /widget header -->
            <div class="widget-content">
                <table class="table table-condensed table-hover">
                <colgroup>
                    <col width="auto"/>
                    <col width="150px"/>
                    <col width="80px"/>
                </colgroup>
                <thead>
                    <tr>
                        <th>Setor</th>
                        <th style="text-align:right;">Qtd de caixas</th>
                        <th style="text-align:right;">%</th>
                    </tr>
                </thead>
                <tbody>
                    <? $total = 0; ?>
                    @foreach($setores as $setor)
                    <? $total+= $setor->qtd; ?>
                    <tr>
                        <td>{{$setor->setores}}</td>
                        <td style="text-align:right;">{{number_format($setor->qtd, 0, ",", ".")}}</td>
                        <td style="text-align:right;">{{number_format(($setor->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td>(sem setor)</td>
                        <td style="text-align:right;">{{number_format($sem_setor[0]->qtd, 0, ",", ".")}}</td>
                        <td style="text-align:right;">{{number_format(($sem_setor[0]->qtd/$total_caixas) * 100, 1, ",", ".")}}</td>
                    </tr>
                    <? $total+= $sem_setor[0]->qtd; ?>
                    <tr class="total">
                        <td>TOTAL</td>
                        <td style="text-align:right;">{{number_format($total, 0, ",", ".")}}</td>
                        <td style="text-align:right;">100,0</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>