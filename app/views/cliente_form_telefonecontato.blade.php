<div class="boxbform">
    @if(isset($many))
      {{ Form::model($many) }}  
    @else
      {{ Form::open(array('url'=>$_SERVER['REQUEST_URI'])) }}    
    @endif
    <div class="box">
        <ul class="form"> 
            <li class='subtitle'>
                @if(isset($many))
                    Editar contato
                @else
                    Adicionar contato
                @endif
                {{ Form::submit('Salvar',array('class'=>'btn btn-primary pull-right')) }}		
            </li>
            <li>
                {{ Form::label('ddd','DDD') }}
                {{ Form::text('ddd') }}
            </li>
            <li>
                {{ Form::label('numero','Número') }} 
                {{ Form::text('numero') }}
            </li>
            <li>
                {{ Form::label('ramal','Ramal') }} 
                {{ Form::text('ramal') }}
            </li>
            <li>
                {{ Form::label('id_tipotelefone','Tipo do telefone') }} 
                {{ Form::select('id_tipotelefone', FormSelect($select['TipoTelefone'],'descricao')) }}
            </li>
            <li>
                {{ Form::label('id_contato','Contato') }} 
                {{ Form::select('id_contato', FormSelect($select['Contato'])) }}
            </li>
        </ul>    
    </div>
    {{ Form::close() }}  
</div>