@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header">
        <h1>
            @if ($model->id_usuario > 0)
                Editar usuário externo (cliente)
            @else
                Criar novo usuário externo (cliente)
            @endif
        </h1>

    </div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">

            @include('padrao.mensagens')

            <!-- widget form horizontal -->
            <div class="widget border-cyan" id="widget-horizontal">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-user"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Usuário</h4>
                    <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                </div><!-- /widget header -->
                <!-- widget content -->
                <div class="widget-content">
                    {{ Form::model($model, array('id'=>'frmusuario')) }}
                        {{Form::hidden('id_usuario', $model->id_usuario)}}

                    <div class="row-fluid">
                        <div class='span6 form-horizontal'>
                        @if ($model->id_usuario > 0)
                        	@if ($model->id_cliente_principal > 0)
	                        	<div class='control-group'>
	                                {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
	                                <div class="controls">
	                                    {{Form::text('razaosocial', $model->clienteprincipal->razaosocial , array('class' => 'input-block-level', 'disabled'=>'disabled')) }}
	                                </div>
	                            </div>
	                        @else
	                        	<div class='control-group'>
	                                {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
	                                <div class="controls">
	                                    {{Form::text('razaosocial', 'Administrador (acesso a todos clientes)' , array('class' => 'input-block-level', 'disabled'=>'disabled')) }}
	                                </div>
	                            </div>
                        	@endif
                        @else
                            <div class='control-group'>
                                {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::select('id_cliente', $clientes, Input::old('id_cliente', Session::get('id_cliente')) , array('id'=>'id_cliente','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                                </div>
                            </div>
                        @endif
                            <div class='control-group'>
                                {{Form::label('nomeusuario','Nome usuário',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::text('nomeusuario', Input::old('nomeusuario', $model->nomeusuario), array('placeholder'=>'nome do pessoa usuário', 'class'=>'input-xlarge','required'))}}
                                </div>
                            </div>
                            <div class='control-group'>
                                {{Form::label('username','Login',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::text('username', null, array('placeholder'=>'Login', 'class'=>'input-xlarge','required'))}}
                                </div>
                            </div>
                            <div class='control-group'>
                            {{Form::label('password','Senha',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::password('senha', array('id'=>'senha','placeholder'=>'Senha', 'class'=>'input-xlarge'))}}
                                </div>
                            </div>

                            <div class='control-group'>
                                {{Form::label('confirmacao','Confirmação',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::password('confirmacao', array('id'=>'confirmacao', 'placeholder'=>'Confirmação da senha', 'class'=>'input-xlarge'))}}
                                </div>
                            </div>
                            <div class='control-group'>
                                {{Form::label('id_nivel','Nível',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::select('id_nivel', array('' => '(não informado)', '1' => 'Administrador - 1', '2' => 'Operacional - 2', '3' => 'Consulta - 3'), null, array('class' => 'input-xlarge') )}}
                                </div>
                            </div>
                            <div class='control-group'>
                                {{Form::label('datacadastro','Cadastrado em',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::text('datacadastro_fmt', null, array('class'=>'input-xlarge', 'disabled'))}}
                                </div>
                            </div>
                            <div class='control-group'>
                                {{Form::label('validadelogin','Bloquear em (dias)',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::text('validadelogin', null, array('placeholder'=>'Validade do login', 'class'=>'input-xlarge'))}}
                                    @if ($bloqueio)
                                    <span class="help-block" style="font-weight: bold; color:red;">{{$bloqueio}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="control-group">
                                {{Form::label('aniversario','Dia do Aniversário',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::text('aniversario_dia', null, array('class'=>'form-control input-small'))}} 
                                </div>
                            </div>
                            <div class="control-group">
                                {{Form::label('aniversario','Mês do Aniversário',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::select('aniversario_mes', array('1'=>'Janeiro','2'=>'Fevereiro','3'=>'Março','4'=>'Abril','5'=>'Maio','6'=>'Junho','7'=>'Julho','8'=>'Agosto','9'=>'Setembro','10'=>'Outubro','11'=>'Novembro','12'=>'Dezembro') , null, array('mes'=>'descricao', 'class'=>'form-control input-medium'))}}
                                </div>
                            </div>
                            </div>
                        @if ($model->id_cliente_principal > 0)
                        <!-- Observações -->
                        <div class="span6">
                            <!-- widget Caixas emprestimo -->
                            <div class="widget border-cyan" id="widget-obs">
                                <!-- widget header -->
                                <div class="widget-header bg-cyan">
                                    <!-- widget icon -->
                                    <div class="widget-icon"><i class="aweso-location-arrow"></i></div>
                                    <!-- widget title -->
                                    <h4 class="widget-title">Ambiente de acesso</h4>
                                </div><!-- /widget header -->
                                <!-- widget content -->
                                <div class="widget-content bg-silver">

                                    <div class='control-group'>
                                        {{Form::label('id_departamento[]','Departamento',array('class'=>'control-label'))}}
                                        <div class="controls">
                                            {{Form::select('id_departamento[]', FormSelect($model->cliente->departamento,'nome'), FormSelectValue($model->departamentos), array('data-fx'=>'select2', 'multiple'=>'true', 'placeholder'=> '(todos)', 'class'=>'input-block-level'))}}
                                        </div>
                                    </div>

                                    <div class='control-group'>
                                        {{Form::label('id_setor[]','Setores',array('class'=>'control-label'))}}
                                        <div class="controls">
                                            {{Form::select('id_setor[]', FormSelect($model->cliente->setor,'nome'), FormSelectValue($model->setores), array('data-fx'=>'select2', 'multiple'=>'true', 'placeholder'=> '(todos)', 'class'=>'input-block-level'))}}
                                        </div>
                                    </div>

                                    <div class='control-group'>
                                        {{Form::label('id_centrodecusto[]','Centro de custos',array('class'=>'control-label'))}}
                                        <div class="controls">
                                            {{Form::select('id_centrodecusto[]', FormSelect($model->cliente->centrocusto,'nome'), FormSelectValue($model->centrocustos), array('data-fx'=>'select2', 'multiple'=>'true', 'placeholder'=> '(todos)', 'class'=>'input-block-level'))}}
                                        </div>
                                    </div>

                                    <div class='control-group'>
                                        {{Form::label('id_clientes[]','Clientes adicionais',array('class'=>'control-label'))}}
                                        <div class="controls">
                                            {{Form::select('id_clientes[]', FormSelect(Cliente::getAtivos(), 'razaosocial'), FormSelectValue($model->clientes), array('data-fx'=>'select2', 'multiple'=>'true', 'placeholder'=> '(só o principal)', 'class'=>'input-block-level'))}}
                                        </div>
                                    </div>
                                </div><!-- /widget content -->
                            </div><!-- /widget Observações -->
                        </div><!-- /Observações -->
                        @endif
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                    {{Form::close()}}

                </div><!-- /widget content -->
            </div> <!-- /widget form horizontal -->


        </div>

    </div>
</div>
</article> <!-- /content page -->

@stop

@section('scripts')
<script>


    $(function() {

        $('#frmusuario').submit(function() {
            if ($.trim($('#senha').val()) != '') {
                if ($('#senha').val() !== $('#confirmacao').val()) {
                    alert('A senha e a confirmação da senha não são iguais, verifique!');
                    return false;
                }
            }
        });

        $('[data-toggle="add-others"]').click(function() {
            $(".add-to-chat").slideToggle("fast", "easeOutBack");
        });

    });


</script>
<style>
    .date{
        width:96px;
    }
    .form-horizontal .control-group{
        margin-bottom:5px !important;
    }
</style>
@stop