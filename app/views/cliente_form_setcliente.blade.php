<div class="boxbform">
    @if(isset($many))
      {{ Form::model($many) }}  
    @else
      {{ Form::open(array('url'=>$_SERVER['REQUEST_URI'].'?tab=tab_setores')) }}    
    @endif
    <div class="box">
        <ul class="form"> 
            <li class='subtitle'>
                @if(isset($many))
                    Editar Setor
                @else
                    Adicionar Setor
                @endif
                {{ Form::submit('Salvar',array('class'=>'btn btn-primary pull-right')) }}
                {{ Form::hidden('tab','tab_setores')}}
            </li>
            <li>
                {{ Form::label('nome','Nome do setor') }} 
                {{ Form::text('nome') }}
            </li>
        </ul>    
    </div>
    {{ Form::close() }}  
</div>