@extends('stilearn-metro')

@section('conteudo')

    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>{{$titulo}}</h1></div>

        <!-- content breadcrumb -->
        <ul class="breadcrumb breadcrumb-inline clearfix">
            <li><a href="#">Cadastros</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
            <li class="active">{{$titulo}}</li>
        </ul>

        <div class='content-action pull-right'>
           @if(isset($linkCriacao))
            <a href='/{{$linkCriacao}}' class='btn'>Novo</a>
            @endif
        </div>
    </header> <!--/ content header -->

    <!-- content page -->
    <article class="content-page clearfix">

        <!-- main page -->
        <div class="main-page">
            <div class="content-inner">

                @if (Session::has('msg'))
                <div class="alert alert-info"><h4>Mensagem</h4>{{ Session::get('msg') }}</div>
                @endif

                @if (Session::has('success'))
                <div class="alert alert-success"><h4>Sucesso!</h4>{{ Session::get('success') }}</div>
                @endif

                @if (Session::has('error'))
                <div class="alert alert-error"><h4>Erro!</h4>{{ Session::get('error') }}</div>
                @endif

                <!-- search box -->
                <div class="search-box">
                    <form class="form-vertical" id="form-search" />
                        <div class="control-group">
                            <div class="controls">
                                <div class="input-append input-append-inline" style="display: block;">
                                    <input class="input-block-level" name="search" placeholder="Buscar..." type="text" />
                                    <button class="btn bg-cyan" type="submit">
                                        <i class="aweso-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /search box -->

                <!-- List -->
                <table class="listagem table table-hover table-striped table-condensed" data-sorter="true" style="width:100%">
                        <colgroup>
                                @foreach ($colunas as $coluna)
                                        @if (isset($coluna['width']))
                                        <col width="{{ $coluna['width'] }}"/>
                                        @else
                                        <col width="auto"/>
                                        @endif
                                @endforeach
                        </colgroup>
                        <thead>
                                <tr>
                                        @foreach ($colunas as $coluna)
                                                <th>{{ $coluna['titulo'] }}</th>
                                        @endforeach
                                        @if(isset($linkEdicao))
                                                <th class="c">Ações</th>
                                        @endif
                                </tr>
                        </thead>
                        <tbody>
                                @foreach ($instancias as $instancia)
                                <tr>
                                        @foreach ($colunas as $coluna)
                                                <td>{{ $instancia->$coluna['campo'] }}</td>
                                        @endforeach
                                        @if(isset($linkEdicao) or isset($linkRemocao))
                                                <td class="c">
                                                    @if($instancia->pode_alterar())
                                                        @if(isset($linkEdicao))
                                                            <a class="btn btn-small" title="Editar" href="/{{ $linkEdicao }}/{{ $instancia->getKey() }}"><i class="icon-pencil"></i></a> 
                                                        @endif
                                                    @endif
                                                    @if($instancia->pode_excluir())
                                                        @if(isset($linkRemocao))
                                                            <a class="btn btn-small" title="Remover" href="/{{ $linkRemocao }}/{{ $instancia->getKey() }}"><i class="icon-remove"></i></a> 
                                                        @endif
                                                    @endif
                                                </td>
                                        @endif
                                </tr>
                                @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </article> <!-- /content page -->
    <style>
        .c{
            width:200px;
            text-align:center;
        }
    </style>
@stop