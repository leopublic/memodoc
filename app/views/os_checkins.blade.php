<table  data-sorter="true" class='table table-striped table-condensed' id="tabela-checkins">
    <thead>
         <th style="width:30px;text-align:center">#</th>
         <th style="width:60px;text-align:center">Ações</th>
         <th style="width:80px;text-align: center;">Referência</th>
         <th style="width:50px;text-align: center;">ID</th>
         <th style="width:auto">Tipo</th>
         <th style="width:200px;text-align: center;">Data checkin</th>
         <th style='width:auto; text-align: center;'>Por</th>
</thead>
    <tbody>
        <? $i =1; ?>
        @foreach($checkins as $checkin)
           @if ($checkin->dt_checkin != '')
               <tr class="success">
           @else
               <tr>
           @endif
           <td style="text-align:center;">{{$i}}</td>
               <td style="text-align:center;">
                   @if ($checkin->dt_checkin != '')
                   <a href="#" class="btn btn-success btn-mini" disabled="disabled"><i class="aweso-ok"></i></a>
                    @else
                       <button title='Retirar essa caixa do checkin' onclick="removeCheckin('{{$checkin->id_os_chave}}','{{$checkin->id_caixa}}'); return false;" class="btn btn-mini btn-danger"><i class="aweso-trash"></i></button>
                    @endif
                    <a href="/digitacao/itens/{{ $model->id_os_chave}}/{{$checkin->id_caixa}}" target="_blank" title="Fazer digitação de documentos recebidos" class="btn btn-mini bg-violet"><i class="aweso-keyboard"></i></a>
               </td>
               <td style="text-align: center;"><a href="{{URL::to('/caixa/historico/'.$checkin->id_cliente.'/'.$checkin->getId_caixapadrao())}}" target="_blank" class="btn btn-small">{{$checkin->getId_caixapadrao()}}</a></td>
               <td style="text-align: center;">{{substr('000000'.$checkin->id_caixa, -6)}}</td>
               <td>
                   @if ($checkin->emcasa)
                      retorno de empréstimo (apesar da caixa constar no galpão no momento do recebimento)
                   @else
                     @if ($checkin->fl_primeira_entrada)
                      caixa nova (primeira entrada)
                     @else
                      retorno de empréstimo
                     @endif
                  @endif
               </td>
               <td style="text-align: center;">
                   @if ($checkin->dt_checkin != '')
                   {{substr($checkin->dt_checkin, 8,2).'/'.substr($checkin->dt_checkin, 5,2).'/'.substr($checkin->dt_checkin, 0,4).' '.substr($checkin->dt_checkin, 10,9) }}
                   @else
                   --
                   @endif
               </td>
               <td style="text-align: center;">
                   @if ($checkin->id_usuario_checkin > 0)
                   {{$checkin->usuario->nomeusuario }}
                   @else
                   --
                   @endif
               </td>
           </tr>
           <? $i++; ?>
        @endforeach
    </tbody>
</table>
