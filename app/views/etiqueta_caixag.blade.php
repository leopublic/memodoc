<?php
$largura = "155px";
$altura = "415px";
$fontsize_titulo = "86px";
$fontsize_texto = "11px";
$fontsize_endereco = "30px";
$largura_endereco = "300px";
$fontsize_id = "10px";
?>
@if($break)
<pagebreak />
@endif
<table style="margin:0px; padding:0px;width:100%;">
	<tr>
		@if(isset($etiqueta['esq']))
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }};"><barcode code="{{ $etiqueta['esq']['id'] }}" type="C39" class="barcode" size="2" height="2.5"/></td>
		@else
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }};">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		@endif
		@if(isset($etiqueta['dir']))
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }}"><barcode code="{{ $etiqueta['dir']['id'] }}" type="C39" class="barcode"  size="2" height="2.5"/></td>
		@else
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		@endif
	</tr>
</table>			
<table style="border-spacing: 1px;rotate: 90;padding:none; margin:none;font-size:10px;font-family:'DejaVuSansCondensed';">
	<tr>
		<td style="font-weight:bold; padding:none; margin:none;vertical-align: top;height:{{ $largura }};width:{{ $altura }};">
		@if(isset($etiqueta['dir']))
			<table style="border-spacing: none;font-size:{{ $fontsize_titulo }};font-weight:bold;padding:none; margin:none;width:100%;">
				<tr>
					<td style="width:80%; padding:none; margin:none;">{{ $etiqueta['dir']['numero'] }}</td>
					<td style="width:20%;  font-size:{{ $fontsize_id }};font-weight:bold;font-family:arial;vertical-align: top; text-align: right;">ID: {{ $etiqueta['dir']['id'] }}</td>
				</tr>
			</table>
			<table style="font-size:{{ $fontsize_texto }};font-weight:bold;font-family:arial;margin-top: -20px;">
				<tr>
					<td style="width:100%;" colspan="2">{{ $etiqueta['dir']['tipo'] }}&nbsp;&nbsp;&nbsp;&nbsp;Reserva: {{ $etiqueta['dir']['reserva'] }}</td>
				</tr>
				<tr><td colspan="2">{{ $etiqueta['dir']['cliente'] }}</td></tr>
				<tr>
					<td>ID: {{ $etiqueta['dir']['id'] }}</td>
					<td style="width:{{ $largura_endereco }};text-align: right;"><span style="font-size:{{ $fontsize_endereco }};">{{ $etiqueta['dir']['endereco'] }}</span></td>
				</tr>
			</table>
		@else
		   &nbsp;
		@endif
		</td>
	</tr>
	<tr>
		<td style="font-weight:bold; padding:none; margin:none;vertical-align: top;height:{{ $largura }};width:{{ $altura }};">
		@if(isset($etiqueta['esq']))
			<table style="border-spacing: none;font-size:{{ $fontsize_titulo }};font-weight:bold;padding:none; margin:none;width:100%;">
				<tr>
					<td style="width:80%; padding:0; margin:0;">{{ $etiqueta['esq']['numero'] }}</td>
					<td style="width:20%; font-size:{{ $fontsize_id }};font-weight:bold;font-family:arial;vertical-align: top; text-align: right;">ID: {{ $etiqueta['esq']['id'] }}</td>
				</tr>
			</table>
			<table style="border-spacing: none;font-size:{{ $fontsize_texto }};font-weight:bold;font-family:arial;margin-top: -20px; width:100%;">
				<tr>
					<td style="width:100%;" colspan="2">{{ $etiqueta['esq']['tipo'] }}&nbsp;&nbsp;&nbsp;&nbsp;Reserva: {{ $etiqueta['esq']['reserva'] }}</td>
				</tr>
				<tr><td colspan="2">{{ $etiqueta['esq']['cliente'] }}</td></tr>
				<tr>
					<td>ID: {{ $etiqueta['esq']['id'] }}</td>
					<td style="width:{{ $largura_endereco }};text-align: right;"><span style="font-size:{{ $fontsize_endereco }};">{{ $etiqueta['esq']['endereco'] }}</span></td>
				</tr>
			</table>
		@else
		   &nbsp;
		@endif
		</td>
	</tr>
</table>
