@extends('stilearn-metro')

@section('conteudo')

@include('cliente_tab_header')

<?php
$atendimento = array(
    '-1' => '(n/a)',
    '0' => '12 horas',
    '1' => '24 horas',
);

$estados = array("AC" => "Acre", "AL" => "Alagoas", "AM" => "Amazonas", "AP" => "Amapá", "BA" => "Bahia", "CE" => "Ceará", "DF" => "Distrito Federal", "ES" => "Espírito Santo", "GO" => "Goiás", "MA" => "Maranhão", "MT" => "Mato Grosso", "MS" => "Mato Grosso do Sul", "MG" => "Minas Gerais", "PA" => "Pará", "PB" => "Paraíba", "PR" => "Paraná", "PE" => "Pernambuco", "PI" => "Piauí", "RJ" => "Rio de Janeiro", "RN" => "Rio Grande do Norte", "RO" => "Rondônia", "RS" => "Rio Grande do Sul", "RR" => "Roraima", "SC" => "Santa Catarina", "SE" => "Sergipe", "SP" => "São Paulo", "TO" => "Tocantins");
?>

<!-- Aba identificação -->
<div class="tab-pane" id="tab_indentificacao">
    {{ Form::model($cliente) }}
    <ul class="form">
        <li class='subtitle'>
            <h4>Identificação</h4>
        </li>

        <li>
            {{ Form::label('razaosocial','Razão Social') }}
            {{ Form::text('razaosocial') }}
        </li>
        <li>
            {{ Form::label('nomefantasia','Fantasia') }}
            {{ Form::text('nomefantasia') }}
        </li>
        <li>
            {{ Form::label('fl_pessoa_fisica','Tipo pessoa') }}
            {{ Form::select('fl_pessoa_fisica', ["0" => "Jurídica", "1" => "Física"], null, array( "id" => 'fl_pessoa_fisica')) }}
        </li>
        <li>
            {{ Form::label('ie','Inscrição estadual') }}
            {{ Form::text('ie') }}
        </li>
        <li>
            {{ Form::label('cgc','CNPJ') }}

            {{ Form::text('cgc', null, array(  "id" => 'cgc')) }}
        </li>
        <li>
            {{ Form::label('im','Inscrição municipal') }}
            {{ Form::text('im') }}
        </li>

    </ul>

    <div style="width:100%; text-align: center;clear:both">
        {{ Form::submit('Salvar') }}
    </div>
    {{ Form::close() }}

</div><!-- Fim Aba identificação -->

@include('cliente_tab_footer')
@stop
@section('scripts')

<script type="text/javascript">
$(document).ready(function(){
    if($("#fl_pessoa_fisica").val() == "1"){
        $("#cgc").inputmask({ mask: "999.999.999-99"});
    } else {
        $("#cgc").inputmask({ mask: "99.999.999/9999-99"});
    }    
});
$('#fl_pessoa_fisica').on("change", function(e) { 
    if($("#fl_pessoa_fisica").val() == "1"){
        $("#cgc").inputmask({ mask: "999.999.999-99"});
    } else {
        $("#cgc").inputmask({ mask: "99.999.999/9999-99"});
    }    
});
</script>


@stop
