@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Criar transbordo para o(s) galpõe(s) {{$galpoes}}</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-share-alt"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Informe o cliente de onde as caixas estão vindo </h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                    {{Form::hidden('id_os_chave', $os->id_os_chave)}}
                   <div class='control-group'>
                        {{Form::label('id_cliente','Cliente origem',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente_antes', $clientes, Input::old('id_cliente_antes') , array('id'=>'id_cliente_antes','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                   <div class='control-group'>
                        {{Form::label('id_os','OS',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('id_os', substr('000000'.$os->id_os, -6) , array('class' => 'input-medium', 'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente destino',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('razaosocial', $os->cliente->razaosocial.' - '.$os->cliente->nomefantasia.' ('.$os->id_cliente.')' , array('class' => 'input-block-level', 'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('cortesia_movimentacao','Cortesia movimentação?',array('class'=>'control-label'))}}
                        {{Form::hidden('cortesia_movimentacao', '0') }}
                        <div class="controls">
                            {{Form::checkbox('cortesia_movimentacao', 1, Input::old('cortesia_movimentacao')) }}
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" name="adicionar" class="btn btn-primary" id="adicionar" ><i class="aweso-plus"></i> Adicionar caixas</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop
