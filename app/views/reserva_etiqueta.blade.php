              
                @if(isset($reservas)) 
                
                <img src='/img/logo.png' /> <h1>Reserv</h1>

                <h5 class="pull-left">{{$documentos->count()}} documentos encontrados </h5>
               
                <div class="clearfix"></div>
                <table class='table table-striped table-hover table-bordered table-condensed'>
                    <thead>
                        <tr>
                            <th style="width:80px">Data inicio</th>
                            <th style="width:80px">Data final</th>
                            <th style="width:80px">Num caixa</th>
                            <th style="width:90px">Caixa padrão</th>
                            <th style="width:50px">Item</th>
                            <th style="width:250px">Título</th>
                            <th style="width:150px">Setor</th>
                            <th style="width:150px">Departamento</th>
                            <th style="width:150px">Centro</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($documentos as $doc)
                            <tr>
                                <td>{{$doc->inicio}}</td>
                                <td>{{$doc->fim}}</td>
                                <td>{{$doc->id_caixa}}</td>
                                <td>{{$doc->id_caixapadrao}}</td>
                                <td>{{$doc->id_item}}</td>
                                <td>{{$doc->titulo}}</td>
                                <td>{{isset($doc->setcliente->nome) ? $doc->setcliente->nome : '' }}</td>
                                <td>{{isset($doc->depcliente->nome) ? $doc->depcliente->nome : '' }}</td>
                                <td>{{isset($doc->centrodecusto->nome) ? $doc->centrodecusto->nome : '' }}</td>
                                
                            </tr>
                            @endforeach
                    </tbody>
                </table>
                
                
                <style>
                    body{
                        font:11px arial;
                        border:1px solid #000;
                    }
                    table thead th{
                        background: #c0c0c0;
                        
                    }
                </style>
                @endif    