@extends('stilearn-metro')

@section('conteudo') 

    @include('cliente_tab_header')

    <!-- #tab_centrocusto - Begin Content  -->
    <div class="tab-pane" id="tab_CentroDeCusto">
        <?php
        $hasmany = array('title' => 'Centro de custo',
            'fields' => array(
                'nome' => 'Centro de custo',
                'qtd_documentos' => 'Qtd documentos',
            )
        );
        ?>
        @include('padrao/hasmany_list', $hasmany)
    </div>
    <!-- #tab_centrocusto - End Content  -->  

    @include('cliente_tab_footer')
@stop