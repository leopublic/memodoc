<html>
    <meta charset="UTF-8" />
    <head>
        <style>
            td{vertical-align: top; font-size: 14px;}
            th{vertical-align: top; font-weight: bold; text-align:right; font-size: 14px;}
            table, tr, td, th, tbody, thead, tfoot {
                   page-break-inside: avoid !important;
            }
        </style>

    </head>
<body>
<table border='0' cellspacing='0' width="100%" style='border:1px solid #000;font:12px arial;mar'>
     <tr>
         <td width="80%" style="font-size:20px; font-weight: bold; vertical-align: top; padding:0;">
             <img src='{{public_path()}}/img/logo_subtitulo.jpg' style="float:left; vertical-align: text-top; " />
             RELAÇÃO DE CAIXAS PARA EXPURGO

             <table width='100%' style="margin:0; font-size:14px;">
                 <tr>
                     <th align='right' style='width:120px !important'>
                        <b>Razão Social:</b>
                     </th>
                     <td>
                         {{$exp->cliente->razaosocial}}
                     </td>
                 </tr>
                 <tr>
                     <th align='right'>
                        <b> <b>Nome fantasia:</b></b>
                     </th>
                     <td>
                         {{$exp->cliente->nomefantasia}}
                     </td>
                 </tr>
                 <tr>
                     <th align='right'>
                       <b>Telefones:</b>
                     </th>
                     <td><? $sep = ''; ?>
                           @foreach($exp->cliente->telefonecliente as $tel)
                                {{$sep.$tel->numero}}
                                <? $sep = ' / '; ?>
                           @endforeach
                     </td>
                 </tr>
             </table>
         </td>
     </tr>
 </table>

@if(count($desmembrada) > 0)
<div style="width:100%;padding:10px;">
    <?php //pr($desmembrada); ?>
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="font-size:14px;font-family: arial;">
        <? $qtd = 1; ?>
        @foreach($desmembrada as $r)
        <tr>
            <td width="30px;" style="text-align:right;">{{$qtd;}}:</td>
            <td width="30px;" style="border:solid 1px black;">&nbsp;&nbsp;</td>
            <td width="120px;">Caixa: <strong>{{substr('000000'.$r->id_caixapadrao, -6)}}</strong></td>
            <td>Localização:<strong>{{$r->id_galpao}}.{{substr('00'.$r->id_rua, -2)}}.{{substr('00'.$r->id_predio, -2)}}.{{substr('00'.$r->id_andar, -2)}}.{{substr('0000'.$r->id_unidade, -4)}}</strong>
            </td>
        </tr>
        <? $qtd++; ?>
        @endforeach
    </table>
    <br />
    Total de caixas relacionadas: {{count($desmembrada)}}
    <br />
    <br />
</div>
@else
--
@endif
</body>
</html>
