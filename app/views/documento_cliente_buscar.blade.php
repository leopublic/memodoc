@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Localizar documentos</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio conteúdo -->
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-filter"></i>Filtrar por</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form grey-steel filtro">
                {{Form::open(array('id'=>'frmdocumento', 'class' => 'form-horizontal'))}}
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Título ou conteúdo</label>
                                <div class="col-md-9">
                                    {{Form::text('texto', null ,array( 'class'=>'form-control input-block-level'))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Inter. cronológico</label>
                                <div class="col-md-9">
                                    <div class="input-group input-large">
                                        <span class="input-group-addon">de</span>
                                        {{Form::text('inicio', null ,array('class'=>'form-control input-small date-picker', 'data-date-format'=>'dd/mm/yyyy'))}}
                                        <span class="input-group-addon">até</span>
                                        {{Form::text('fim', null ,array('class'=>'form-control input-small date-picker', 'data-date-format'=>'dd/mm/yyyy'))}}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Intervalo numérico</label>
                                <div class="col-md-9">
                                    <div class="input-group input-large">
                                        <span class="input-group-addon">de</span>
                                        {{Form::text('nume_inicial', null ,array('class'=>'form-control number input-small'))}}
                                        <span class="input-group-addon">até</span>
                                        {{Form::text('nume_final', null ,array('class'=>'form-control number input-small'))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Intervalo alfabético</label>
                                <div class="col-md-9">
                                    <div class="input-group input-large">
                                        <span class="input-group-addon">de</span>
                                        {{Form::text('alfa_inicial', null ,array('class'=>'form-control alfa input-small'))}}
                                        <span class="input-group-addon">até</span>
                                        {{Form::text('alfa_final', null ,array('class'=>'form-control alfa input-small'))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Previsão de expurgo de </label>
                                <div class="col-md-9">
                                    <div class="input-group input-large">
                                        <span class="input-group-addon">mês</span>
                                        {{Form::text('exp_progr_mes_ini', null ,array('class'=>'form-control number input-xsmall'))}}
                                        <span class="input-group-addon">ano</span>
                                        {{Form::text('exp_progr_ano_ini', null ,array('class'=>'form-control number input-small'))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Previsão de expurgo até </label>
                                <div class="col-md-9">
                                    <div class="input-group input-large">
                                        <span class="input-group-addon">mês</span>
                                        {{Form::text('exp_progr_mes_fim', null ,array('class'=>'form-control number input-xsmall'))}}
                                        <span class="input-group-addon">ano</span>
                                        {{Form::text('exp_progr_ano_fim', null ,array('class'=>'form-control number input-small'))}}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Ref. MEMODOC</label>
                                <div class="col-md-9">
                                    <div class="input-group input-large">
                                        <span class="input-group-addon">de</span>
                                        {{Form::text('id_caixapadrao_ini', null ,array('class'=>'form-control number input-small'))}}
                                        <span class="input-group-addon">até</span>
                                        {{Form::text('id_caixapadrao_fim', null ,array('class'=>'form-control number input-small'))}}
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Fim primeira coluna-->
                        <!-- Inicio segunda coluna-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Setores</label>
                                <div class="col-md-9">
                                    {{Form::select('id_setor', $setores, null, array('id'=>'id_setor', 'class' => 'form-control select2me', 'data-placeholder'=>'(todos)'))}}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Departamentos</label>
                                <div class="col-md-9">
                                    {{Form::select('id_departamento', $departamentos, null, array('id'=>'id_departamento', 'class' => 'form-control select2me'))}}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Centro de custos</label>
                                <div class="col-md-9">
                                    {{Form::select('id_centro', $centrocustos, null, array('id'=>'id_centro', 'class' => 'form-control select2me'))}}
                                </div>
                            </div>
                            @if (isset($propriedade))
                            <div class="form-group">
                                <label class="control-label col-md-3">{{$propriedade->nome}}</label>
                                <div class="col-md-9">
                                    {{Form::select('id_valor', $valores, null, array('id'=>'id_valor', 'class' => 'form-control select2me'))}}
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="control-label col-md-3">Referência migração</label>
                                <div class="col-md-9">
                                    {{Form::text('referencia', null ,array('class'=>'form-control alfa input-medium'))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="id_cliente">Status</label>
                                <div class="radio-list col-md-9">
                                    <label title="Todas as caixas, sem restrição">
                                        {{ Form::radio('status', 'todas', (Input::old('status') == 'todas' || Input::old('status') == '') ? true : false, array('id'=>'todas')) }}
                                        todas
                                    </label>
                                    <label title="Em custódia no galpão da MEMODOC">
                                        {{ Form::radio('status', 'emcasa', (Input::old('status') == 'emcasa') ? true : false, array('id'=>'emcasa')) }}
                                        na MEMODOC
                                    </label>
                                    <label title="Enviadas para custódia e depois emprestadas para consulta" style="background-color:#FA6800">
                                        {{ Form::radio('status', 'emconsulta', (Input::old('status') == 'emconsulta') ? true : false, array('id'=>'emconsulta')) }}
                                        em consulta
                                    </label>
                                    <label title="Nunca enviadas para custódia e inventariadas (que tiveram o conteúdo alterado)" style="background-color:#A4C400;">
                                        {{ Form::radio('status', 'naoenviadas', (Input::old('status') == 'naoenviadas') ? true : false, array('id'=>'naoenviadas')) }}
                                        não enviadas e inventariadas
                                    </label>
                                    <label title="Nunca enviadas para custódia e não inventariadas" style="background-color:#F0A30A;">
                                        {{ Form::radio('status', 'reservadas', (Input::old('status') == 'reservadas') ? true : false, array('id'=>'reservadas')) }}
                                        não enviadas e não inventariadas
                                    </label>
                                    @if(\Auth::user()->cliente->franquiacustodia > 0)
                                    <label title="Nunca enviadas, inventariadas ou não, reservadas a mais tempo que a carência" style="background-color:#E3C800">
                                        {{ Form::radio('status', 'emcarencia', (Input::old('status') == 'emcarencia') ? true : false, array('id'=>'emcarencia')) }}
                                        em carência
                                    </label>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- Fim segunda coluna-->
                    </div>
                </div>
                <!-- Fim form-body -->
                <!-- INICIO FORM-ACTIONS -->
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-offset-3 col-md-9">
                                <input type="submit" class="btn yellow-gold" id="submit-pesquisa" value="Buscar"></>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- FIM FORM-ACTIONS -->
                {{ Form::close() }}
            </div>
            <!-- Fim portlet-body -->
        </div>
        <!-- Fim portlet -->
    </div>
    <!-- fim  da coluna unica -->
</div>
<!-- fim linha do painel de busca -->
@if(isset($documentos))
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-folder"></i>{{ number_format($qtdDocumentos, 0, ',', '.') }} documentos encontrados em {{ number_format($qtdCaixas, 0, ',', '.') }} referências</div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body">
                @include('documento_cliente_tabela', array('documentos'=> $documentos, 'cliente' => $cliente))
            </div>
        </div>
    </div>
</div>
@endif

@stop
@section('scripts')
@include('_layout.scripts-forms')
@stop