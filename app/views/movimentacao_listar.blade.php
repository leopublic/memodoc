@extends('layout')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Movimentações de endereço</h1></div>
    <div class='content-action pull-right'>
        <a href='/caixa/movimentar' class='btn'>Criar nova</a>
    </div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <!-- search box -->
            @if(isset($movimentacoes))
            <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                <thead>
                    <th style="width:130px; text-align:center;">Ação</th>
                    <th style="width:80px; text-align:center;">ID</th>
                    <th style="width:150px; text-align: center;">Data da solicitação</th>
                    <th style="width:80px; text-align: center;">Caixas</th>
                    <th style="width:auto; text-align: center;">Cadastrado por</th>
                    <th style="width:auto; text-align: left;">Observação</th>
                </thead>
                <tbody>
                    @foreach($movimentacoes as $movimentacao)
                    <tr class="">
                        <td style="text-align:center;">
                            <a href="/caixa/movimentacaodetalhe/{{$movimentacao->id_movimentacao}}" class="btn btn-sm btn-primary" title="Clique para ver os detalhes da movimentação"><i class="aweso-edit"></i></a>
                            <a class="btn btn-sm btn-warning" href="/etiqueta/geremovimentado/{{ $movimentacao->id_movimentacao }}" target="_blank" title="Imprimir etiquetas"><i class="aweso-tags"></i></a>
                            <a class="btn btn-sm bg-lime" href="/caixa/movimentacaodepara/{{ $movimentacao->id_movimentacao}}" target="_blank" title="Imprimir de-para"><i class="aweso-print"></i></a>
                        </td>
                        <td style="text-align: center;">{{substr('000000'.$movimentacao->id_movimentacao, -6)}}</td>
                        <td style="text-align: center;">{{$movimentacao->created_at}}</td>
                        <td style="text-align: center;">{{$movimentacao->qtd_caixas}}</td>
                        <td style="text-align: center;">
                            @if (is_object($movimentacao->usuario))
                            {{$movimentacao->usuario->nomeusuario}}
                            @endif
                        </td>
                        <td style="text-align: left;">{{$movimentacao->observacao}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .table tbody{
        font:12px arial;
    }
</style>
@stop
@section('scripts')
<script>
@if ($id_movimentacao > 0)
    window.open('{{URL::to('/caixa/movimentacaodepara/'.$id_movimentacao)}}', 'depara');
@endif
</script>
@stop
