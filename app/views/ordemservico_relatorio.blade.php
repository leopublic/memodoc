<html>
    <head>
        <style>
            *{font-family:arial;}
            td{vertical-align: top}
            th{vertical-align: top; font-weight: bold; text-align:right;}
            h2{margin:none; padding: 0;font-size:18px;}
        </style>

    </head>
<body>
@include('ordemservico_cabecalho_rel', array('os' => $os, 'titulo'=> '') )

<div style='font-family:arial;border:1px solid #000;border-top:0px; width:100%;padding:10px; padding-top:0; font-size:16px;'>
    <h2 align='center'>ENTREGA DE CAIXAS</h2>
    <table style="border-collapse: collapse; border: none;font-family:arial;">
        <tr><td>Qtde de novas caixas com endereçamento</td><td>:</td><td>{{intval($os->qtdenovascaixas)}}</td></tr>
    @if ($os->qtd_cartonagens_cliente > 0)
        <tr><td>Qtde de novas caixas sem endereçamento</td><td>:</td><td>{{intval($os->qtd_cartonagens_cliente)}}</td></tr>
    @endif
    @if ($os->qtdenovasetiquetas > 0)
        <tr><td>Qtde de novos endereços sem cartonagem</td><td>:</td><td>{{intval($os->qtdenovasetiquetas)}}</td></tr>
    @endif
        <tr><td>Qtde de caixas enviadas para consulta</td><td>:</td><td>{{intval($os->qtdcaixasconsulta())}}</td></tr>
    </table>
    <br />
    <br />
</div>
<div style='font-family:arial;border:1px solid #000; border-top:0px; padding:10px; font-size:16px; '>
    <h2 align='center'>APANHA DE CAIXAS</h2>

    Apanhar {{number_format($os->qtd_caixas_novas_cliente, 0, ",", ".")}} caixa  CHEIAS NOVAS <br />
    Apanhar {{number_format($os->qtd_caixas_retiradas_cliente, 0, ",", ".")}} caixa(s) de DEVOLUÇÃO DE CONSULTA
    <br />
    <br />
    <br />
</div>

<div style='font-family:arial;border:1px solid #000;border-top:0px; padding:10px; font-size:16px; '>

   <br />
   @if ($os->cobrafrete == 1)
    @if ($os->fl_frete_manual)
        Total do frete R$ {{ number_format($os->val_frete_manual,2,',','.')}}  <br />
    @else
        Total do frete R$ {{ number_format($os->valorfreteprevisto,2,',','.')}}  <br />
    @endif
   @else
    Total do frete R$ 0,00
   @endif
    <br />
</div>
<div style="font:12px arial;border:1px solid #000; border-top:0px;padding:10px; font-size:14px; height: 250px; ">
    <h2 align='center'>OBSERVAÇÕES</h2>
    <br />
    {{nl2br($os->observacao)}}
</div>
<div style='font-family:arial;border:1px solid #000;border-top:0px; padding:10px; '>
    <br />
    <table align='center'>
        <tr>
            <td>_________________________________________</td>
            <td>_________________________________________</td>
            <td>____/_____/_______</td>
        </tr>
        <tr>
            <td>Motorista</td>
            <td>Ajudante</td>
            <td>Data</td>
        </tr>
    </table>
    <h4 align='center'>DECLARO TER RECEBIDO/ENVIADO AS CAIXAS ACIMA RELACIONADAS</h4>
    <br />

    <table align='center'>
        <tr>
            <td>__________________________________________________________________________________</td>
            <td>____/_____/_______</td>
        </tr>
        <tr>
            <td>Cliente</td>
            <td>Data</td>
        </tr>
    </table>
    <br />
</div>

</body>
</html>
