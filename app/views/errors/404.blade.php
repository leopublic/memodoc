@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Caminho inválido</h1>

        </div>

    </header> <!--/ content header -->

    <!-- content page -->
    <article class="content-page clearfix">
        <!-- main page -->
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">
                <div class="widget" id="filtro">
                    <!-- widget header -->
                    <div class="widget-header">
                        <!-- widget title -->
                        <h4 class="widget-title"><i class="aweso-exclamation"></i> Erro</h4>
                        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                    	Não foi possível atender à sua solicitação pois o caminho solicitado é inválido.
					</div>
				</div>
			</div>
		</div>
	</article>
@stop