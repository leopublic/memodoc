@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Não foi possível atender à sua solicitação</h1>

        </div>

    </header> <!--/ content header -->

    <!-- content page -->
    <article class="content-page clearfix">

        <!-- main page -->
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">
                <div class="widget" id="filtro">
                    <!-- widget header -->
                    <div class="widget-header">
                        <!-- widget title -->
                        <h4 class="widget-title"><i class="aweso-exclamation"></i> Envie esses dados para o suporte</h4>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
						<table>
						{{ $exception->xdebug_message }}
						</table>
					</div>
				</div>
			</div>
		</div>
	</article>
@stop