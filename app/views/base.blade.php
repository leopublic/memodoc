<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.1.1
Version: 3.1
Author: KeenThemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" >
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <title>::Memodoc::</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/> -->
        <link href="{{URL::to('/assets/metronic/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/assets/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/assets/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet')}}" type="text/css"/>
        <link href="{{URL::to('/assets/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet')}}" type="text/css"/>
        <link href="{{URL::to('/assets/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/assets/metronic/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        @yield('estilos')
        <!-- BEGIN THEME STYLES -->
        <link href="{{URL::to('/assets/metronic/global/css/components.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/assets/metronic/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/assets/metronic/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
        <link id="style_color" href="{{URL::to('/assets/metronic/admin/layout/css/themes/memodoc.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to('/assets/metronic/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="/favicon.ico"/>
        <style>
            .page-header.navbar{
                height: 40px !important;
                min-height:40px !important;

            }
            .page-header.navbar .hor-menu .navbar-nav > li{
                height: 40px !important;
            }
            .page-header.navbar .hor-menu .navbar-nav > li > a{
                padding: 10px 10px !important;
            }
            .page-header.navbar .hor-menu .navbar-nav > li > a
            ,.page-header.navbar .hor-menu .navbar-nav > li > ul > li > a
            {
                font-size: 11px !important;
            }
            .page-header.navbar .top-menu .navbar-nav > li.dropdown{
                height: 40px !important;
            }
            .page-header.navbar .page-logo{
                height: 40px !important;
            }

            .filtro .form-group{
                margin-bottom: 5px !important;
            }
            .filtro .form-actions{
                margin-top: 0px !important;
            }
            .filtro .form-actions.fluid{
                padding: 10px 0px !important;
            }

            .table-hover > tbody > tr.emcasa > td{
                background-color: #ffcb72 !important;
            }

            .table-hover > tbody > tr.emcasa:hover > td{
                background-color: #FFA500 !important;
            }

            .table-hover > tbody > tr.emconsulta > td{
                background-color: #FA6800 !important;
            }
            .table-hover > tbody > tr.emconsulta:hover > td{
                background-color: #FA6800 !important;
            }

            .table-hover > tbody > tr.naoenviadas > td{
                background-color: #A4C400 !important;
            }
            .table-hover > tbody > tr.naoenviadas:hover > td{
                background-color: #A4C400 !important;
            }

            .table-hover > tbody > tr.reservadas > td{
                background-color: #F0A30A !important;
            }
            .table-hover > tbody > tr.reservadas:hover > td{
                background-color: #F0A30A !important;
            }

            .table-hover > tbody > tr.emcarencia > td{
                background-color: #E3C800 !important;
            }
            .table-hover > tbody > tr.emcarencia:hover > td{
                background-color: #E3C800 !important;
            }

        </style>

    </head>
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-header-fixed page-full-width">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                @include('_layout.logo')
                @include('_layout.menu-cliente-funcoes')
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                @include('_layout.menu-cliente-usuario')
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            @include('_layout.cliente-atual')
            @include('_layout.notificacao_inadimplente')
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE CONTENT-->
                    @yield('conteudo')
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner">
                2016 &copy; Memodoc. Desenvolvido por <a href="http://www.m2software.com.br" target="_blank">M2 Software</a>.
            </div>
            <div class="page-footer-tools">
                <span class="go-top">
                    <i class="fa fa-angle-up"></i>
                </span>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="{{URL::to('assets/metronic/global/plugins/respond.min.js')}}"></script>
        <script src="{{URL::to('assets/metronic/global/plugins/excanvas.min.js')}}"></script>
        <![endif]-->
        <script src="{{URL::to('assets/metronic/global/plugins/jquery-1.11.0.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::to('assets/metronic/global/plugins/jquery-migrate-1.2.1.min.js')}}" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="{{URL::to('assets/metronic/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::to('assets/metronic/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::to('assets/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::to('assets/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::to('assets/metronic/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::to('assets/metronic/global/plugins/jquery.cokie.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::to('assets/metronic/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::to('assets/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::to('assets/metronic/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
        <!-- END CORE PLUGINS -->
        <script src="{{URL::to('assets/metronic/admin/pages/scripts/ui-toastr.js')}}"></script>
        <script src="{{URL::to('assets/metronic/global/scripts/metronic.js')}}" type="text/javascript"></script>
        <script src="{{URL::to('assets/metronic/admin/layout/scripts/layout.js')}}" type="text/javascript"></script>
        @yield('scripts')
        <script>
            $(document).ready(function(){
                $('#submit-pesquisa').click(function(event){
                    event.preventDefault();
                    $(this).html('<i class="fa fa-spinner fa-spin"></i> Aguarde...');
                    $(this).attr('disabled', 'disabled');
                    $(this).parents('form:first').submit();
                });

                //$(".tablesorter").tablesorter(); 
            });

            jQuery(document).ready(function() {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                UIToastr.init();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>
