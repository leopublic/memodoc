{{$documentos->links()}}
<table class='table table-hover table-bordered table-condensed'>
    <thead>
    <tr>
        <th style="width:120px;text-align:center;">Ref.</th>
        <th>Título/Conteúdo</th>
        @if ($cliente->fl_digitalizacao)
        <th>Digitalizações</th>
        @endif
        <th style="width:200px">Área</th>
        <th style="width:200px">Classificação</th>
    </tr>
    </thead>
    <tbody>
    @foreach($documentos as $doc)
    @if ($doc->emcasa == 0 )
        <tr class="{{$doc->status}}">
    @else
    <tr>
    @endif
        <td style="text-align: center;" name="#{{ substr("000000".$doc->id_caixapadrao, -6) }}_{{ substr("000".$doc->id_item, -3)}}">
            <b>{{ substr("000000".$doc->id_caixapadrao, -6) }}</b>
            <br/>Item {{ substr("000".$doc->id_item, -3)}}

            @if ($doc->emcasa == 1 && Auth::user()->id_nivel <= 2)
            <br/><a href="#" onclick="javascript:adicionaCaixa(this, '{{$doc->id_caixa}}');" class="btn btn-sm btn-primary" style="margin-bottom:3px;">Solicitar ref.</a>
            @endif
            <br/><a href="/publico/caixa/historico/{{$doc->id_caixapadrao}}" target="_blank" class="btn  btn-sm btn-primary">Ver movim.</a>
        </td>
        <td><b>Título: {{$doc->titulo}}</b><br/><hr style="color: #555; margin:2px;">
            {{$doc->conteudo}}
            @if ($cliente->fl_controle_revisao)
            <br/><span style="font-style: italic;font-size: 11px;">{{$doc->situacao_revisao_cliente}}</span>
            @endif
        </td>
        @if ($cliente->fl_digitalizacao)
        <td style="padding:0;">
            @foreach ($doc->anexos as $anexo)
            <p><a href="/publico/anexo/download/{{$anexo->id_anexo}}" class="btn btn-sm btn-primary"><i class="fa fa-cloud-download"></i></a> {{$anexo->descricao}}</p>
            @endforeach
        </td>
        @endif
        <td style="padding:0;">
            <table style="width:100%;border:solid 1px #ddd !important;">
                <tr><td><b>Setor:</b></td><td style="min-width: 50px;">{{isset($doc->setcliente->nome) ? $doc->setcliente->nome : '--' }}</td></tr>
                <tr><td><b>Departamento:</b></td><td>{{isset($doc->depcliente->nome) ? $doc->depcliente->nome : '--' }}</td></tr>
                <tr><td><b>Centro de custos:</b></td><td>{{isset($doc->centrodecusto->nome) ? $doc->centrodecusto->nome : '--' }}</td></tr>
                @if (isset($propriedade))
                <tr><td><b>{{$propriedade->nome}}:</b></td><td>{{count($doc->valor) >0 ? $doc->valor->nome : '--' }}</td></tr>
                @endif
                <tr><td style="border-right: none;border-left: none;">&nbsp;</td><td style="border-right: none;border-left: none;">&nbsp;</td></tr>
            </table>
        </td>
        <td style="padding:0;">
            <table style="width:100%;">
                <tr><td><b>Tipo</b></td><td>{{$doc->tipodocumento->descricao}}</td></tr>
                <tr><td><b>Ref. migração 1</b></td><td>{{$doc->migrada_de}}</td></tr>
                <tr><td><b>Ref. migração 2</b></td><td>{{$doc->request_recall}}</td></tr>
                <tr><td><b>Início</b></td><td>
                        @if ($doc->inicio != '')
                        {{$doc->formatDate('inicio')}}
                        @endif
                    </td></tr>
                <tr><td><b>Fim</b></td><td>
                        @if ($doc->fim != '')
                        {{$doc->formatDate('fim')}}
                        @endif
                    </td></tr>
                <tr><td><b>N&ordm; inicial</b></td><td>{{$doc->nume_inicial}}</td></tr>
                <tr><td><b>N&ordm; final</b></td><td>{{$doc->nume_final}}</td></tr>
                <tr><td><b>Alfa inicial</b></td><td>{{$doc->alfa_inicial}}</td></tr>
                <tr><td><b>Alfa final</b></td><td>{{$doc->alfa_final}}</td></tr>
                <tr><td><b>Alfa final</b></td><td>{{$doc->alfa_final}}</td></tr>
                <tr><td><b>Expurgar após</b></td><td>{{$doc->expurgo_programado_completo}}</td></tr>
            </table>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
{{$documentos->links()}}
<script>

    function adicionaCaixa(botao, id_caixa){
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "positionClass": "toast-top-right",
          "onclick": null,
          "showDuration": "1000",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
//        event.preventDefault();
        $(botao).html('<i class="aweso-spinner aweso-spin"></i> Aguarde...');
        $(botao).attr('disabled', 'disabled');
        $(botao).parents('form:first').submit();

        var url = '/publico/os/adicionarcaixa/'+id_caixa;
        $.ajax({
            type: "GET",
            url: url,
            dataType: "json"
        })
        .done(function( data) {
            if (data.status == 0){
                $(botao).html('adicionado');
                toastr['success'](data.msg, "Ok")
            } else {
                toastr['error'](data.msg, "Erro")
                $(botao).removeAttr('disabled');
                $(botao).html('(erro)');
            }
         });
    }
</script>