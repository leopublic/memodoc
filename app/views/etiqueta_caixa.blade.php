<?php 
$border_out = 'white';
if(array_key_exists('border', $especs) && $especs['border'] == 1){
   $border_out = "blue";
}
?>
@if($break)
	<pagebreak />
@endif
<table style="border-spacing: 1px; padding:0; margin:0; margin-bottom: 0px; font-size:10px;font-family:'DejaVuSansCondensed'; width:{{ $especs['altura'] }}px;  border: solid 1px {{$border_out}};">
@if(isset($etiqueta['dir']))
	@include('etiqueta_caixa_cada' , array('xetiqueta' => $etiqueta['dir'], 'lado' => 'dir'))
      <tr>
         <td colspan="2" style="padding:{{$especs['margem_central']}}px; border: solid 1px {{$border_out}};">&nbsp;</td>
      </tr>
@else
   	<tr>
   		<td>&nbsp;</td><td>&nbsp;</td>
   	</tr>
@endif
@if(isset($etiqueta['esq']))
	@include('etiqueta_caixa_cada', array('xetiqueta' => $etiqueta['esq'], 'lado' => 'esq'))
@else
   	<tr>
         <td>&nbsp;</td><td>&nbsp;</td>
   	</tr>
@endif
</table>
