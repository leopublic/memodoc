@extends('base')

@section('estilos')
    @include('_layout.estilos-forms')
    <style>
    div.checker{padding-top: 6px !important;}
    </style>
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Criar Ordem de serviço</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio conteúdo -->
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-truck"></i>Preencha os dados da ordem de serviço</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form grey-steel">
                {{ Form::model($model, array('class'=>'form-horizontal')) }}
                {{ Form::hidden('id_os_chave', $model->id_os_chave)}}
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->
                        <div class="col-md-12">
                            <div class='form-group'>
                                {{Form::label('urgente', 'Atender em caráter EMERGENCIAL?',array('class'=>'control-label col-md-4'))}}
                                <div class="col-md-8">
                                    {{Form::checkbox('urgente', 1,null, array('id' => 'urgente'))}}
                                </div>
                            </div>

                            <div class='form-group'>
                                {{Form::label('tipoemprestimo','Você deseja',array('class'=>'control-label col-md-4'))}}
                                <div class="col-md-8">
                                    <div class="radio-list">
                                        <label>
                                            {{ Form::radio('tipodeemprestimo', '0', (Input::old('tipodeemprestimo') == '0' ) ? true : false, array('id'=>'0')) }}
                                            que a solicitação seja entregue pela Memodoc
                                        </label>
                                        <label>
                                            {{ Form::radio('tipodeemprestimo', '1', (Input::old('tipodeemprestimo') == '1' ) ? true : false, array('id'=>'1')) }}
                                            fazer a consulta da sua solicitação na própria Memodoc
                                        </label>
                                        <label>
                                            {{ Form::radio('tipodeemprestimo', '2', (Input::old('tipodeemprestimo') == '2' ) ? true : false, array('id'=>'2')) }}
                                            efetuar a retirada da solicitação sem cobrança de frete (seu próprio portador)
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class='form-group'>
                                {{Form::label('id_endentrega','Informe o local entrega/retirada',array('class'=>'control-label col-md-4'))}}
                                <div class="col-md-8">
                                    {{Form::select('id_endeentrega', $endeentrega , null, array( 'id'=>'endereco', 'class' => 'form-control select2me fluid'))}}
                                </div>
                            </div>

                            <div class='form-group'>
                                <label class="control-label col-md-4">Quantidade de novas caixas, <strong>com</strong> endereçamento</label>
                                <div class="col-md-8">
                                    {{Form::text('qtdenovascaixas', null ,array("class"=> "form-control fluid input-small",  "onkeydown"=>"javascript:doNothing();"))}}
                                </div>
                            </div>

                            <div class='form-group'>
                                <label class="control-label col-md-4">Quantidade de novas caixas, <strong>sem</strong> endereçamento</label>
                                <div class="col-md-8">
                                    {{Form::text('qtd_cartonagens_cliente', null ,array("class"=>"form-control input-small",  "onkeydown"=>"javascript:doNothing();"))}}
                                </div>
                            </div>

                            <div class='form-group'>
                                <label class="control-label col-md-4">Quantidade de novos endereços <strong>sem cartonagem</strong></label>
                                <div class="col-md-8">
                                    {{Form::text('qtdenovasetiquetas', null ,array("class"=>"form-control input-small",  "onkeydown"=>"javascript:doNothing();"))}}
                                </div>
                            </div>

                            <div class='form-group'>
                                {{Form::label('qtd_caixas_retiradas_cliente','Quantidade de caixas que você possui para serem retiradas',array('class'=>'control-label col-md-4'))}}
                                <div class="col-md-8">
                                    {{Form::text('qtd_caixas_retiradas_cliente', null ,array('class'=>'form-control fluid',  "onkeydown"=>"javascript:doNothing();"))}}
                                </div>
                            </div>
                            <div class='form-group'>
                                {{Form::label('observacoes','Observações',array('class'=>'control-label col-md-4'))}}
                                <div class="col-md-8">
                                    {{Form::textarea('observacao', null ,array('class'=>'form-control fluid'))}}
                                    <span class="help-block">Utilize este campo para informações complementares. Exemplo:<br/>&nbsp;&nbsp;&nbsp;"Entregar para o João no segundo andar ramal 123"<br/>&nbsp;&nbsp;&nbsp;"Entregar a João no 2º andar"<br/>&nbsp;&nbsp;&nbsp;"Enviar 1 rolo de fita"<br/>&nbsp;&nbsp;&nbsp;"Efetuar a entrega pela parte da tarde"</span>
                                </div>
                            </div>
                        </div>
                        <!-- Fim primeira coluna-->
                    </div>
                    <!-- Inicio linha REFERENCIAS SOLICITADAS -->
                    <div class="row">
                        <!-- INICIO COLUNA REFERENCIAS SOLICITADAS -->
                        <div class="col-md-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption"><i class="fa fa-archive"></i>Referências solicitadas</div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <!-- INICIO PORTLET-BODY -->
                                <div class="portlet-body ">
                                    <div class="row">
                                        <!-- Inicio primeira coluna-->
                                        <div class="col-md-12">
                                            <div class='input-group'>
                                                <input type='text' id='id_caixapadrao' class="form-control input-small" onkeydown="javascript:doNothing();" />
                                                <a href="" class="btn btn-primary" class="button" id="adicionaCaixa">Adicionar</a>
                                            </div>
                                            @include('referencias_os', array('id_os_chave' => $model->id_os_chave, 'caixas'=> $model->referenciasConsulta()))
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM COLUNA REFERENCIAS SOLICITADAS -->
                    </div>
                    <!-- FIM linha REFERENCIAS SOLICITADAS -->
                </div>
                <!-- Fim form-body -->
                <!-- INICIO FORM-ACTIONS -->
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-offset-3 col-md-9">
                                <input type="submit" class="btn yellow-gold" name="acaoEncerrar" value="Salvar e efetivar a OS"></>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- FIM FORM-ACTIONS -->
                {{ Form::close() }}
            </div>
            <!-- Fim portlet-body -->
        </div>
        <!-- Fim portlet -->
    </div>
    <!-- fim  da coluna unica -->
</div>

@stop

@section('scripts')
@include('_layout.scripts-forms')
<script>

    function doNothing() {
    var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        if( keyCode == 13 ) {


            if(!e) var e = window.event;

            e.cancelBubble = true;
            e.returnValue = false;

            if (e.stopPropagation) {
                    e.stopPropagation();
                    e.preventDefault();
            }
        }
    }
    $(document).ready(function() {
        $('#adicionaCaixa').click(function(event) {
            event.preventDefault();
            $(this).html('<i class="aweso-spinner aweso-spin"></i> Aguarde...');
            $(this).attr('disabled', 'disabled');
            adicionaCaixa('{{$model->id_os_chave}}', $('#id_caixapadrao').val());
        });
    });

    function adicionaCaixa(id_os_chave, id_caixapadrao) {
        var url = '/publico/os/addcaixa/' + id_os_chave + '/' + id_caixapadrao;
        $.ajax({
            type: "GET",
            url: url,
            dataType: "json"
        })
        .done(function(data) {
            if (data.status == 'success') {
                $('#emprestimos').html(data.conteudo);
            }
            $('#id_caixapadrao').val('');
            $('#adicionaCaixa').removeAttr('disabled');
            $('#adicionaCaixa').html('Adicionar');
            toastr[data.status](data.msg, data.titulo);
        });
        return false;
    }



    function removeEmprestimo(id_osdesmembrada) {
        var url = '/publico/os/delemprestimo/' + id_osdesmembrada;
        $('#emprestimos').html('<i class="aweso-spinner aweso-spin"></i>');
        $.ajax({
            type: "GET",
            url: url,
            dataType: "json"
        })
                .done(function(data) {
                    if (data.status == 'success') {
                        $('#emprestimos').html(data.conteudo);
                    }
                    toastr[data.status](data.msg, data.titulo);
                });
        return false;
    }

</script>
@stop