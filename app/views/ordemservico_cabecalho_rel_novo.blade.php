<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
<script>
  function subst() {
      var vars = {};
      var query_strings_from_url = document.location.search.substring(1).split('&');
      for (var query_string in query_strings_from_url) {
          if (query_strings_from_url.hasOwnProperty(query_string)) {
              var temp_var = query_strings_from_url[query_string].split('=', 2);
              vars[temp_var[0]] = decodeURI(temp_var[1]);
          }
      }
      var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate', 'time', 'title', 'doctitle', 'sitepage', 'sitepages'];
      for (var css_class in css_selector_classes) {
          if (css_selector_classes.hasOwnProperty(css_class)) {
              var element = document.getElementsByClassName(css_selector_classes[css_class]);
              for (var j = 0; j < element.length; ++j) {
                  element[j].textContent = vars[css_selector_classes[css_class]];
              }
          }
      }
  }
  </script>
    </head>
<body  onload="subst()">
    <table border='0' cellspacing='0' width="100%" style='border:1px solid #000;font: 9px arial; font-family:arial;'>
         <tr>
             <td width="80%" style="font-size:20px; font-weight: bold; vertical-align: top; padding:0;">
                 <img src='{{public_path()}}/img/logo_subtitulo.jpg' style="width:200px; float:left; vertical-align: text-top; " />
                 @if(isset($brtitulo))
                 <br/>{{$brtitulo}}
                 @else
                 &nbsp;&nbsp;&nbsp;
                 @endif
                 <br />

                 <table width='100%' style="margin:0; font-size:10px;">
                     <tr>
                         <th align='right' style='width:120px !important'>
                            <b>Razão Social:</b>
                         </th>
                         <td>
                             {{$os->cliente->razaosocial}}
                         </td>
                     </tr>
                     <tr>
                         <th align='right'><b>Nome fantasia:</b></th>
                         <td>
                             {{$os->cliente->nomefantasia}}
                         </td>
                     </tr>
                     <tr>
                         <th align='right'>Endereço:</th>
                         <td>{{$os->enderecoEntregaRelatorios()}}</td>
                     </tr>
                     <tr>
                         <th align='right'><b>Contato:</b></th>
                         <td>
                            {{$os->responsavel}}
                         </td>
                     </tr>
                     <tr>
                         <th align='right'>
                           <b>Departamento:</b>
                         </th>
                         <td>
                            {{isset($os->departamento->nome) ? $os->departamento->nome : '' }}
                         </td>
                     </tr>
                     <tr>
                         <th align='right'>
                           <b>Telefones:</b>
                         </th>
                         <td><? $sep = ''; ?>
                               @foreach($os->cliente->telefonecliente as $tel)
                                    {{$sep.$tel->numero}}
                                    <? $sep = ' / '; ?>
                               @endforeach
                         </td>
                     </tr>
                 </table>
             </td>
             <td style="vertical-align: top; width:220px;">
                 <table>
                     <tr>
                         <th align='right'><b>Ordem de serviço: </b></th>
                         <td> {{$os->getNumeroFormatado()}}</td>
                     </tr>
                     <tr>
                         <th align='right'><b>Solicitado em:</b></th>
                         <td> {{substr($os->solicitado_em, 0, 10)}}</td>
                     </tr>
                     <tr>
                         <th align='right'><b>Entrega em:</b></th>
                         <td> {{$os->entregar_em}}</td>
                     </tr>
                     <tr>
                         <th align='right'><b>Período:</b> </th>
                         <td>{{($os->entrega_pela) ? 'Tarde' : 'Manhã' }} </td>
                     </tr>
                     <tr>
                         <th align='right'><b>Urgente:</b> </th>
                         <td>{{($os->urgente) ? 'SIM' : 'NÃO' }} </td>
                     </tr>
                     <tr>
                         <th colspan="2">
                             @if ($os->tipodeemprestimo == 0)
                             Entrega MEMODOC
                             @elseif ($os->tipodeemprestimo == 1)
                             Consulta no local
                             @else
                             Retirado pelo cliente
                             @endif
                         </th>
                     </tr>
                     <tr>
                         <th align='right'><b>Página </b> </th>
                         <td><b><span class="page"></span> / <span class="topage"></span> </td>
                     </tr>
                 </table>
             </td>
         </tr>
     </table>
    </body>
</html>