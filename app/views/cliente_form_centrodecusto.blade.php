<div class="boxbform">
    @if(isset($many))
      {{ Form::model($many) }}  
    @else
      {{ Form::open(array('url'=>$_SERVER['REQUEST_URI'].'?tab=tab_centrocusto')) }}    
    @endif
    <div class="box">
        <ul class="form"> 
            <li class='subtitle'>
                @if(isset($many))
                    Editar Centro de custos
                @else
                    Adicionar novo centro de custos
                @endif
                {{ Form::submit('Salvar',array('class'=>'btn btn-primary pull-right')) }}
                {{ Form::hidden('tab','tab_centrocusto')}}
            </li>
            <li>
                {{ Form::label('nome','Nome do centro de custos') }} 
                {{ Form::text('nome') }}
            </li>
        </ul>    
    </div>
    {{ Form::close() }}  
</div>