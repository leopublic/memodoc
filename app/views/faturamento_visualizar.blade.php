@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header">
        <h1 ><span style="font-size:14px;">Faturamento {{ $fat->mes }}/{{$fat->ano}} <strong>(de {{$fat->periodo_faturamento->dt_inicio_fmt}} até {{$fat->periodo_faturamento->dt_fim_fmt}})</strong> </span><br/>{{ $fat->cliente->razaosocial }}</h1>
    </div>
    <ul class="content-action pull-right">
        @if (isset($fat_ant))
        <li><a class="btn bg-silver" href="{{URL::to('/faturamento/visualizar/'.$fat_ant->id_faturamento.'/'.$origem)}}" title="{{$fat_ant->cliente->razaosocial}}"><i class="aweso-chevron-left"></i> Anterior</a></li>
        @endif
        @if($origem == 1)
        <li><a class="btn bg-lime" href="{{URL::to('/faturamento/porcliente/'.$fat->id_cliente)}}" ><i class="aweso-backward"></i> Ver todos</a></li>
        @else
        <li><a class="btn bg-lime" href="{{URL::to('/periodofat/detalhe/'.$fat->id_periodo_faturamento)}}" ><i class="aweso-backward"></i> Ver todos</a></li>
        @endif
        <li><a class="btn bg-lime " href="{{URL::to('/faturamento/criardemonstrativo/'.$fat->id_faturamento)}}" target="_blank"><i class="aweso-print"></i> Demonstrativo</a></li>
        <li><a class="btn bg-lime " href="{{URL::to('/faturamento/recalcular/'.$fat->id_faturamento)}}" ><i class="aweso-refresh"></i> Recalcular</a></li>
        <li><a class="btn bg-lime " href="{{URL::to('/caixa/rateiofaturamento/'.$fat->id_faturamento)}}" ><i class="aweso-bar-chart"></i> Rateio</a></li>
        @if (isset($fat_prox))
        <li><a class="btn bg-silver" href="{{URL::to('/faturamento/visualizar/'.$fat_prox->id_faturamento.'/'.$origem)}}" title="{{$fat_prox->cliente->razaosocial}}"><i class="aweso-chevron-right"></i> Próximo</a></li>
        @endif
    </ul>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
                    <?
                    $larg_label_cli = 'span5';
                    $larg_campo_cli = 'span7';
                    $larg_label = 'span5';
                    $larg_campo = 'span7';
                    $larg_qtd = 'span4';
                    $larg_unit = 'span3';
                    $larg_tot = 'span5';

                    ?>

    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">

            @include ('padrao/mensagens')
            <!-- Itens -->
            @if (isset($cliente))
                @include('faturamento.dados_cliente')
            @endif               
            @if (isset($fat))
                @include('faturamento.demonstrativo_online')
            @endif               
            @if (isset($precos))
                @include('faturamento.valores_unitarios')
            @endif               
            @if (isset($fat))
                @include('faturamento.ordens_de_servico')
            @endif               
            @if (isset($fat))
                @include('faturamento.emcarencia')
            @endif               
            @if (isset($fat))
                @include('faturamento.naocobradas')
            @endif               



        </div>
    </div>
</article> <!-- /content page -->

@stop

@section('scripts')
@stop