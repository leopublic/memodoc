<div class="boxbform">
    @if(isset($many))
      {{ Form::model($many) }}  
    @else
      {{ Form::open(array('url'=>$_SERVER['REQUEST_URI'].'?tab=tab_enderecos')) }}    
    @endif
    <?php
        $estados = array("AC"=>"Acre", "AL"=>"Alagoas", "AM"=>"Amazonas", "AP"=>"Amapá","BA"=>"Bahia","CE"=>"Ceará","DF"=>"Distrito Federal","ES"=>"Espírito Santo","GO"=>"Goiás","MA"=>"Maranhão","MT"=>"Mato Grosso","MS"=>"Mato Grosso do Sul","MG"=>"Minas Gerais","PA"=>"Pará","PB"=>"Paraíba","PR"=>"Paraná","PE"=>"Pernambuco","PI"=>"Piauí","RJ"=>"Rio de Janeiro","RN"=>"Rio Grande do Norte","RO"=>"Rondônia","RS"=>"Rio Grande do Sul","RR"=>"Roraima","SC"=>"Santa Catarina","SE"=>"Sergipe","SP"=>"São Paulo","TO"=>"Tocantins");
    ?>
    <div class="box">
        <ul class="form"> 
            <li class='subtitle'>
                @if(isset($many))
                    Editar endereço de entrega
                @else
                    Adicionar novo endereço de entrega
                @endif
                {{ Form::submit('Salvar',array('class'=>'btn btn-primary pull-right')) }}
                {{ Form::hidden('tab','tab_enderecos')}}
            </li>
            <li>
                {{ Form::label('logradouro','Logradouro') }} 
                {{ Form::text('logradouro') }}
            </li>
            <li>
                {{ Form::label('endereco','Endereço') }} 
                {{ Form::text('endereco') }}
            </li>
            <li>
                {{ Form::label('numero','Número') }} 
                {{ Form::text('numero') }}
            </li>
            <li>
                {{ Form::label('complemento','Complemento') }} 
                {{ Form::text('complemento') }}
            </li>
            <li>
                {{ Form::label('bairro','Bairro') }} 
                {{ Form::text('bairro') }}
            </li>
            <li>
                {{ Form::label('cidade','Cidade') }} 
                {{ Form::text('cidade') }}
            </li>
            <li>
                {{ Form::label('estado','Estado') }} 
                {{ Form::select('estado', $estados, null, array('data-fx'=>'select2')) }}
            </li>
            <li>
                {{ Form::label('cep','CEP') }} 
                {{ Form::text('cep') }}
            </li>
        </ul>    
    </div>
    {{ Form::close() }}  
</div>