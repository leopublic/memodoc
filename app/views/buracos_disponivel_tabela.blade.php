@if (isset($xls))
<html>
    <head>
        <style>
            table { border: 1px solid black; }
            td { border: 1px solid black; }
            td { border: 1px solid black; }
        </style>
    <head>
    <body>
@endif
@if (isset($xls))
        <table>
@else
        <table class="table table-striped dataTable">
@endif
@if (!isset($xls))
                <thead>
@endif
                    <tr>
                        <th>ID</th>
                        <th>Cliente</th>
                        <th style="text-align:right;">Franquia (dias)</th>
                        <th style="text-align:right;">Reservadas a mais de 180 dias, não recebida mas inventariada</th>
                        <th style="text-align:right;">Reservadas a mais de 180 dias, não recebida nem inventariada</th>
                        <th style="text-align:right;">Total reservado a mais de 180 dias</th>
                        <th style="text-align:right;">Total reservado não enviado</th>
                        <th style="text-align:right;">Total reservado</th>
                    </tr>
@if (!isset($xls))
                </thead>
                <tbody>
@endif
                    @foreach ($totais as $cliente)
                    <tr>
                        <td><a href="/cliente/contrato/{{$cliente->id_cliente}}#tab_contrato" target="_blank">{{ $cliente->id_cliente }}</a></td>
                        <td><a href="/cliente/contrato/{{$cliente->id_cliente}}#tab_contrato" target="_blank">{{ $cliente->razaosocial }} ({{ $cliente->nomefantasia }})</a></td>
                        <td style="text-align:right;">{{ $cliente->franquiacustodia }}</td>
                        <td style="text-align:right;">{{ number_format($cliente->qtd_180_inventariado, 0, ',', '.') }}</td>
                        <td style="text-align:right;">{{ number_format($cliente->qtd_180_nao_inventariado, 0, ',', '.') }}</td>
                        <td style="text-align:right;">{{ number_format($cliente->qtd_180_nao_inventariado + $cliente->qtd_180_inventariado, 0, ',', '.') }}</td>
                        <td style="text-align:right;">{{ number_format($cliente->qtd, 0, ',', '.') }}</td>
                        <td style="text-align:right;">{{ number_format($cliente->qtd_total, 0, ',', '.') }}</td>
                    </tr>
                    @endforeach
@if (!isset($xls))
                </tbody>
@endif
        </table>
@if (isset($xls))
    </body>
</html>
@endif