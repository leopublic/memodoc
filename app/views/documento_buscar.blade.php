@extends('stilearn-metro')

@section('styles')
tr.nogalpao{
    
}
tr.emconsulta{
    background-color:#FA6800;
}
tr.reservadas{
    background-color:#F0A30A;
}
tr.naoenviadas{
    background-color:#A4C400;
}
tr.emcarencia{
    background-color:#E3C800;
}
tr.emfranquia{
    background-color:#4b8df8;
}
@stop

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>{{ $titulo }}</h1>

        </div>

    </header> <!--/ content header -->

    <!-- content page -->
    <article class="content-page clearfix">

        <!-- main page -->
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">
            @include('padrao/mensagens')
                    <div class="widget" id="filtro">
                    <!-- widget header -->
                    <div class="widget-header">
                        <!-- widget title -->
                        <h4 class="widget-title"><i class="aweso-filter"></i> Filtrar</h4>
                        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        <div class="widget-action">
                            <button data-toggle="collapse" data-collapse="#filtro" class="btn">
                                <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                            </button>
                        </div>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        {{Form::open(array('class' => 'form-horizontal', 'id'=>'frmdocumento', 'style'=>'margin-bottom:none;'))}}
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Cliente</label>
                                    <div class="controls">
                                        {{Form::select('id_cliente', $clientes, $id_cliente, array('data-fx'=>'select2', 'class'=> 'input-block-level', 'id'=>'id_cliente','required'))}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="titulo">Título / Conteúdo</label>
                                    <div class="controls">
                                        {{Form::text('titulo', null ,array('placeholder'=>"Pesquisar no título do documento...", 'class'=>'input-block-level','id'=>'titulo'))}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Reservadas entre</label>
                                    <div class="controls">
                                        <div class="input-append input-append-inline ">
                                            {{Form::text('reserva_inicio', null ,array('placeholder'=>"Data início", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                                        </div>
                                        <div class="input-append input-append-inline ">
                                            {{Form::text('reserva_fim', null ,array('placeholder'=>"Data final", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Setor</label>
                                    <div class="controls">
                                        {{Form::select('id_setor', $setores, null, array('data-fx'=>'select2','id'=>'id_setor'))}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Departamento</label>
                                    <div class="controls">
                                        {{Form::select('id_departamento', $departamentos, null, array('data-fx'=>'select2','id'=>'id_departamento'))}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Centro de custos</label>
                                    <div class="controls">
                                        {{Form::select('id_centro', $centrocustos, null, array('data-fx'=>'select2','id'=>'id_centro'))}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Status*</label>
                                    <div class="controls">
                                        <label class="radio" title="Todas as caixas, sem restrição">
                                            {{ Form::radio('status', 'todas', (Input::old('status') == 'todas' || Input::old('status') == '') ? true : false, array('id'=>'todas')) }}
                                            todas
                                        </label>
                                        <label class="radio" title="No galpão">
                                            {{ Form::radio('status', 'emcasa', (Input::old('status') == 'emcasa') ? true : false, array('id'=>'emcasa')) }}
                                            no galpão
                                        </label>
                                        <label class="radio" title="Enviadas para custódia e depois emprestadas" style="background-color:#FA6800">
                                            {{ Form::radio('status', 'emconsulta', (Input::old('status') == 'emconsulta') ? true : false, array('id'=>'emconsulta')) }}
                                            em consulta
                                        </label>
                                        <label class="radio" title="Nunca enviadas para custódia e inventariadas (que tiveram o conteúdo alterado)" style="background-color:#A4C400;">
                                            {{ Form::radio('status', 'naoenviadas', (Input::old('status') == 'naoenviadas') ? true : false, array('id'=>'naoenviadas')) }}
                                            não enviadas e inventariadas
                                        </label>
                                        <label class="radio" title="Nunca enviadas para custódia e não inventariadas" style="background-color:#F0A30A;">
                                            {{ Form::radio('status', 'reservadas', (Input::old('status') == 'reservadas') ? true : false, array('id'=>'reservadas')) }}
                                            não enviadas e não inventariadas
                                        </label>
                                        <label class="radio" title="Nunca enviadas, inventariadas ou não, reservadas a mais tempo que a franquia do cliente" style="background-color:#E3C800">
                                            {{ Form::radio('status', 'emcarencia', (Input::old('status') == 'emcarencia') ? true : false, array('id'=>'emcarencia', $status_carencia)) }}
                                            {{$titulo_carencia}}
                                        </label>
                                        <label class="radio" title="Nunca enviadas, inventariadas ou não, reservadas a menos tempo que a franquia do cliente" style="background-color:#4b8df8">
                                            {{ Form::radio('status', 'emfranquia', (Input::old('status') == 'emfranquia') ? true : false, array('id'=>'emfranquia', $status_franquia)) }}
                                            {{$titulo_franquia}}
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Movimentação</label>
                                    <div class="controls">
                                        <label class="radio">
                                            {{ Form::radio('movimentacao', 'todas', (Input::old('movimentacao') == 'todas' || Input::old('movimentacao') == '') ? true : false) }}
                                            todas
                                        </label>
                                        <label class="radio">
                                            {{ Form::radio('movimentacao', 'sim', (Input::old('movimentacao') == 'sim') ? true : false) }}
                                            já movimentadas
                                        </label>
                                        <label class="radio">
                                            {{ Form::radio('movimentacao', 'nao', (Input::old('movimentacao') == 'nao') ? true : false) }}
                                            nunca movimentadas
                                        </label>
                                    </div>
                                </div>
                            @if ($propriedade)
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">{{$propriedade->nome}}</label>
                                    <div class="controls">
                                        {{Form::select('id_valor', $valores, null, array('data-fx'=>'select2','id'=>'id_valor'))}}
                                    </div>
                                </div>
                            @endif


                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Intervalo cronológico</label>
                                    <div class="controls">
                                        <div class="input-append input-append-inline ">
                                            {{Form::text('inicio', null ,array('placeholder'=>"De", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                                        </div>
                                        <div class="input-append input-append-inline ">
                                            {{Form::text('fim', null ,array('placeholder'=>"Até", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Intervalo numérico</label>
                                    <div class="controls">
                                        <div class="input-append input-append-inline">
                                            {{Form::text('nume_inicial', null ,array('placeholder'=>"De", 'class'=>'number'))}}
                                        </div>
                                        <div class="input-append input-append-inline">
                                            {{Form::text('nume_final', null ,array('placeholder'=>"Até", 'class'=>'number'))}}
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Intervalo alfabético</label>
                                    <div class="controls">
                                        <div class="input-append input-append-inline">
                                            {{Form::text('alfa_inicial', null ,array('placeholder'=>"De", 'class'=>'alfa'))}}
                                        </div>
                                        <div class="input-append input-append-inline">
                                            {{Form::text('alfa_final', null ,array('placeholder'=>"Até", 'class'=>'alfa'))}}
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Primeira entrada entre</label>
                                    <div class="controls">
                                        <div class="input-append input-append-inline ">
                                            {{Form::text('primeira_entrada_inicio', null ,array('placeholder'=>"De", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                                        </div>
                                        <div class="input-append input-append-inline ">
                                            {{Form::text('primeira_entrada_fim', null ,array('placeholder'=>"Até", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Referência</label>
                                    <div class="controls">
                                        <div class="input-append input-append-inline">
                                            {{Form::text('id_caixapadrao', null ,array('placeholder'=>"De", 'class'=>'alfa'))}}
                                        </div>
                                        <div class="input-append input-append-inline">
                                            {{Form::text('id_caixapadrao_fim', null ,array('placeholder'=>"Até", 'class'=>'alfa'))}}
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Id</label>
                                    <div class="controls">
                                        {{Form::text('id_caixa', null ,array('placeholder'=>"id", 'class'=>'alfa'))}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Ref. migração 1/2</label>
                                    <div class="controls">
                                        {{Form::text('referencia', null ,array('placeholder'=>"", 'class'=>'input-block-level','id'=>'referencia'))}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Migradas</label>
                                    <div class="controls">
                                        <label class="radio">
                                            {{ Form::radio('migradas', 'todas', (Input::old('migradas') == 'todas' || Input::old('migradas') == '') ? true : false, array('id'=>'todas')) }}
                                            todas
                                        </label>
                                        <label class="radio">
                                            {{ Form::radio('migradas', 'sim', (Input::old('migradas') == 'sim') ? true : false) }}
                                            somente caixas migradas
                                        </label>
                                        <label class="radio">
                                            {{ Form::radio('migradas', 'nao', (Input::old('migradas') == 'nao') ? true : false) }}
                                            somente não migradas
                                        </label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="id_cliente">Ordenar por (impressão)</label>
                                    <div class="controls">
                                        {{Form::select('ordenarpor', array("id_caixapadrao,id_item"=>"Referência","dep_cliente.nome,id_caixapadrao,id_item"=>"Departamento","set_cliente.nome,id_caixapadrao,id_item"=>"Setor","c.nome,id_caixapadrao,id_item"=>"Centro de custos"), Input::old('ordenarpor', 'id_caixapadrao,id_item'), array('data-fx'=>'select2','id'=>'ordenarpor'))}}
                                    </div>
                                </div>

                            </div>
                        </div>



                        <div class="row-fluid">
                            <span style="font-style: italic; color:#999966; font-size:10px;">* Atenção: o status da caixa no sistema pode não refletir a situação real, principalmente para caixas que não foram movimentadas recentemente. Confira sempre no galpão.</span>
                        </div>
                        <div class="form-actions bg-silver">
                            <button type="submit" class="btn bg-orange" id="submit-pesquisa"><i class="aweso-search"></i> Buscar</button>
                            <button type="reset" class="btn">Limpar</button>
                            <a href="/documento/acervoselecionado" target="_blank" class="btn bg-lime">Imprimir acervo selecionado</a>
                            <a href="/documento/acervoselecionadoxls" target="_blank" class="btn bg-lime">Imprimir acervo selecionado XLS</a>
                        </div>
                        {{Form::close()}}
                    </div><!-- /widget content -->
                </div>

        <!-- search box -->

                @if (isset($documentos))
                <div class="row">
                <div class="span12">
                    <p>{{number_format($qtdDocumentos, 0, ",", ".")}} documentos encontrados em {{number_format($qtdCaixas, 0, ",", ".")}} referências</p>
                </div>
                </div>
                <div class='documentos'>
                    {{$documentos->links()}}
                    <table class='table table-hover table-bordered table-condensed'>
                        <thead>
                            <tr>
                                <th style="width:120px;text-align:center;">Referência</th>
                                <th>Título/Conteúdo</th>
                                <th>Digitalizações</th>
                                <th style="width:200px">Área</th>
                                <th style="width:200px">Classificação</th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach($documentos as $doc)
                                <tr class="{{$doc->status}}" title="{{$doc->title}}">
                                    <td style="text-align:center;">
                                        <b>{{ substr("000000".$doc->id_caixapadrao, -6) }}</b>
                                        <br/>(ID: {{ substr("000000".$doc->id_caixa, -6) }})</b>
                                        <br/>Item {{ substr("000".$doc->id_item, -3)}}
                                        <p><a href="/caixa/historico/{{$doc->id_cliente}}/{{$doc->id_caixapadrao}}" target="_blank" class="btn">Ver movim.</a></p>
                                        <p><a href="/documento/editar/{{$doc->id_cliente}}/{{$doc->id_caixapadrao}}/{{$doc->id_item}}" target="_blank" class="btn">Editar</a></p>
                                        Reservado em <br/>{{$doc->reserva->data_reserva_fmt}}
                                        <p><a href="/reserva/visualizar/{{$doc->id_reserva}}" target="_blank" class="btn">Reserva {{$doc->id_reserva}}</a></p>
                                    <td><b>{{$doc->titulo}}</b>
                                        <br/>{{$doc->conteudo}}
                                        @if ($cliente->fl_controle_revisao)
                                        <br/><br/><span style="font-style: italic;font-size: 11px;">{{$doc->situacao_revisao}}</span>
                                        @endif                                        
                                    </td>
                                    <td>
                                    @foreach ($doc->anexos as $anexo)
                                    <p><a href="/anexo/download/{{$anexo->id_anexo}}" class="btn btn-primary"><i class="aweso-cloud-download"></i></a> {{$anexo->descricao}}</p>
                                    @endforeach
                                        
                                    </td>
                                    <td style="padding:0;">
                                        <table style="width:100%;">
                                            <tr><td><b>Setor:</b></td><td>{{isset($doc->setcliente->nome) ? $doc->setcliente->nome : '--' }}</td></tr>
                                            <tr><td><b>Departamento:</b></td><td>{{isset($doc->depcliente->nome) ? $doc->depcliente->nome : '--' }}</td></tr>
                                            <tr><td><b>Centro de custos:</b></td><td>{{isset($doc->centrodecusto->nome) ? $doc->centrodecusto->nome : '--' }}</td></tr>
                                            @if($propriedade)
                                            <tr><td><b>{{$propriedade->nome}}:</b></td><td>{{count($doc->valor)>0 ? $doc->valor->nome : '--' }}</td></tr>
                                            @endif
                                            <tr><td style="border-right: none;border-left: none;">&nbsp;</td><td style="border-right: none;border-left: none;">&nbsp;</td></tr>
                                        </table>
                                    </td>
                                    <td style="padding:0;">
                                        <table style="width:100%;">
                                            <tr><td><b>Tipo</b></td><td>{{$doc->tipodocumento->descricao}}</td></tr>
                                            <tr><td><b>Ref. migração 1</b></td><td>{{$doc->migrada_de}}</td></tr>
                                            <tr><td><b>Ref. migração 2</b></td><td>{{$doc->request_recall}}</td></tr>
                                            <tr><td><b>Início</b></td><td>
                                                    @if ($doc->inicio != '')
                                                    {{$doc->formatDate('inicio')}}
                                                    @endif
                                                </td></tr>
                                            <tr><td><b>Fim</b></td><td>
                                                    @if ($doc->fim != '')
                                                    {{$doc->formatDate('fim')}}
                                                    @endif
                                                </td></tr>
                                            <tr><td><b>N&ordm; inicial</b></td><td>{{$doc->nume_inicial}}</td></tr>
                                            <tr><td><b>N&ordm; final</b></td><td>{{$doc->nume_final}}</td></tr>
                                            <tr><td><b>Alfa inicial</b></td><td>{{$doc->alfa_inicial}}</td></tr>
                                            <tr><td><b>Alfa final</b></td><td>{{$doc->alfa_final}}</td></tr>
                                        </table>
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                    {{$documentos->links()}}
                    <?php
                        $appends = Input::all();
                        unset($appends['page']);
                    ?>
                     <br />
                    Exibindo {{$documentos->getFrom()}} ao {{$documentos->getTo()}}


                </div>
                @endif
            </div>
        </div>
    </article> <!-- /content page -->
@stop

@section('scripts')
<script>
  $(function(){
    $cliente = $('#id_cliente');
    $departamento = $('#id_departamento');
    $setor = $('#id_setor');
    $centrocusto = $('#id_centro');
    $cliente.change(function(){

       $('.load').show();
       $('.nomecliente').html($(this).find("option:selected").text());
       $('#titulo').focus();
       $('.documentos').hide();
       var x = $('#submit-pesquisa').html();
        $('#submit-pesquisa').html('<i class="aweso-spinner aweso-spin"></i> Recarregando...');
        $('#submit-pesquisa').attr('disabled', 'disabled');
       // limpando combos
       $departamento.html('<option value="">(carregando...)</option>');
       $setor.html('<option value="">(carregando...)</option>');
       $centrocusto.html('<option value="">(carregando...)</option>');

       // ajax departamento
       $.get('/documento/ajaxdepartamento/'+$(this).val(),function(combo){
           $departamento.html(combo);
       });
       // ajax centro custo
       $.get('/documento/ajaxcentrocusto/'+$(this).val(),function(combo){
           $centrocusto.html(combo);
       });
       // ajax setor
       $.get('/documento/ajaxsetor/'+$(this).val(),function(combo){
           $setor.html(combo);
       });

        $('#submit-pesquisa').html(x);
        $('#submit-pesquisa').removeAttr('disabled');

       // Remove o load após 2.5 segundos
       setInterval(function(){
           $('.load').hide();
       },2500);

    }).focus();
  });
</script>
<style>
    .controls{ margin-right: 20px;}
    .date,.number,.alfa{width:90px}
    .table{font-size:11px}
    #invisivel{display:none}
</style>
@stop