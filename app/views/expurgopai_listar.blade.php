@extends('layout')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>{{$titulo}}</h1></div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <!-- search box -->
            <div class="search-box form-horizontal">
                {{Form::open()}}
                <div class='control-group'>
                    {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                    <div class="controls">
                        {{Form::select('id_cliente', $clientes, $id_cliente , array('id'=>'id_cliente','data-fx'=>'select2','class'=>'input-block-level'))}}
                    </div>
                </div>

                <div class='controls pull-left'>
                    <label> &nbsp;</label>
                    <div class="controls">
                        <button class='btn btn-success' id="submit-pesquisa">Buscar</button>
                    </div>
                </div>
                <div class='clearfix'></div>

                {{Form::close()}}
            </div>
            @if(isset($expurgos))
            {{ $expurgos->links() }}
            <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                <thead>
                    <th style="width:auto; text-align:center;">Ação</th>
                    <th style="width:auto; text-align:center;">Expurgo</th>
                    <th style="width:auto; text-align:center;">OS</th>
                    <th style="width:auto; text-align:center;">Transbordo</th>
                    <th style="width:auto; text-align:left;">Cliente</th>
                    <th style="width:auto; text-align:center;">Qtd caixas</th>
                    <th style="width:auto; text-align:center;">Cortesia exp.?</th>
                    <th style="width:auto; text-align:center;">Cortesia mov.?</th>
                    <th style="width:auto; text-align: center;">Data da solicitação</th>
                    <th style="width:auto; text-align: center;">Cadastrado por</th>
                    <th style="width:auto;">&nbsp;</th>
                </thead>
                <tbody>

                    @foreach($expurgos as $expurgo)
                    <tr class="">
                        <td style="text-align:center;">
                            <a href="/expurgo/caixasgravadas/{{$expurgo->id_expurgo_pai}}" class="btn btn-primary btn-sm" title="Clique para ver as caixas desse expurgo"><i class="aweso-edit"></i></a>
                            <a href="/expurgo/relacaooperacional/{{$expurgo->id_expurgo_pai}}" class="btn btn-warning btn-sm" title="Clique para gerar a realação de caixas para o operacional" target="_blank"><i class="aweso-print"></i></a>
                            <a href="/expurgo/relacaodocumentos/{{$expurgo->id_expurgo_pai}}" class="btn bg-lime btn-sm" title="Clique para gerar a realação de documentos para o cliente" target="_blank"><i class="aweso-print"></i></a>
                        </td>
                        <td style="text-align: center;">{{substr('000000'.$expurgo->id_expurgo_pai, -6)}}</td>
                        <td style="text-align: center;">
                        @if($expurgo->id_os_chave > 0)
                            <a href="/osok/visualizar/{{$expurgo->id_os_chave}}" class="btn btn-primary" target="_blank">{{$expurgo->num_os}}</a>
                        @else
                            {{$expurgo->num_os}}
                        @endif
                        </td>
                        <td style="text-align: center;">
                            @if ($expurgo->id_transbordo > 0)
                                <a class="btn" href="/transbordo/caixas/{{$expurgo->id_transbordo}}" target="_blank">{{substr('000000'.$expurgo->id_transbordo, -6)}}
                            @else
                                --
                            @endif
                        </td>
                        <td style="text-align: left;">{{$expurgo->cliente->razaosocial.' ('.$expurgo->id_cliente.')'}}</td>
                        <td style="text-align:center;">{{$expurgo->qtd_caixas()}}
                        <td style="text-align: center;">
                            @if ($expurgo->cortesia == 1)
                            <i class="aweso-check"></i>
                            @else 
                            <i class="aweso-check-empty"></i>
                            @endif
                        </td>
                        <td style="text-align: center;">
                            @if ($expurgo->cortesia_movimentacao == 1)
                            <i class="aweso-check"></i>
                            @else 
                            <i class="aweso-check-empty"></i>
                            @endif
                        </td>
                        <td style="text-align: center;">{{$expurgo->data_solicitacao}}</td>
                        <td style="text-align: center;">
                            @if (is_object($expurgo->cadastradopor))
                            {{$expurgo->cadastradopor->nomeusuario}}
                            @endif
                        </td>
                        <td >&nbsp;</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

            {{ $expurgos->links() }}
            @endif
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .table tbody{
        font:12px arial;
    }
</style>
@stop

@section('scripts')
<script>
@if ($id_expurgo_pai != '')
window.open('{{URL::to('/expurgo/relacaodocumentos/'.$id_expurgo_pai)}}', 'RelacaoDocumentos');
window.open('{{URL::to('/expurgo/relacaooperacional/'.$id_expurgo_pai)}}', 'RelacaoOperacional');
@endif
</script>
@stop
