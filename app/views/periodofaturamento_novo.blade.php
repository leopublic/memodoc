@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        @if ($perfat->id_periodo_faturamento > 0) <div
        class="page-header"><h1>Alterar faturamento</h1></div> @else <div
        class="page-header"><h1>Criar novo faturamento</h1></div> @endif
        </header> <!--/ content header -->


    <!-- content page -->
    <article class="content-page clearfix">
        <!-- main page -->
        <div class="main-page" id="">
            <div class="content-inner">
                <!-- search box -->
                @include('padrao.mensagens')
                <div class="widget border-cyan">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-dollar"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Informe o período de faturamento</h4>
                    </div><!-- /widget header -->

                    <div class="widget-content">
                    <!-- widget content -->
                    {{ Form::model($perfat, array('class'=>'form-horizontal')) }}
                        {{Form::hidden('id_periodo_faturamento', null)}}
                        <div class="control-group">
                            <div class='input-append'>
                                <div class="control-group">
                                    {{Form::label('dt_inicio_fmt','Período',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('dt_inicio_fmt', $perfat->dt_inicio_fmt, array('class'=>'input-medium', 'placeholder'=>"desde", 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                        {{Form::text('dt_fim_fmt', $perfat->dt_fim_fmt, array('class'=>'input-medium', 'placeholder'=>"até", 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions bg-silver">
                            @if ($perfat->id_periodo_faturamento > 0)
                               <input type="submit" class="btn bg-cyan" class="button" id="submit-pesquisa" value="Alterar faturamento"/>
                            @else
                                @if ($pode_abrir)
                                   <input type="submit" class="btn bg-cyan" class="button" id="submit-pesquisa" value="Criar faturamento"/>
                                @else
                                    @if(count($pendencias) > 0)
                                        <p>Resolva as pendências para poder criar o faturamento.</p>
                                    @endif
                                    @if($existem_pendentes)
                                        <p>Existe pelo um faturamento que ainda não foi fechado. Feche o faturamento aberto antes de abrir um novo.</p>
                                    @endif
                                @endif
                            @endif
                        </div>
                    {{ Form::close() }}
                    </div>
                </div>
            @if (count($pendencias) > 0)
                <div class="widget border-cyan">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-dollar"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Pendências</h4>
                    </div>
                    <div class="widget-content">
                        <p>
                            As seguintes ordens de serviço possuem pendências que se não forem resolvidas poderão gerar problemas no faturamento desse mês ou do mês seguinte. Resolva as pendências para poder criar o faturamento.
                        </p>
                        <table class="table">
                            <tr>
                                <th style="text-align:center;">#</th>
                                <th style="text-align:center;">OS</th>
                                <th>Cliente</th>
                                <th style="text-align:center;">Data atendimento</th>
                                <th style="text-align:center;">Status</th>
                            </tr>
                <? $i = 0;?>
                @foreach ($pendencias as $os)
                            <tr>
                                <td style="text-align:center;">{{$i}}</td>
                                <td style="text-align:center;"><a href="/osok/visualizar/{{$os->id_os_chave}}" class="btn" target="_blank">{{substr('000000'.$os->id_os, -6)}}</a></td>
                                <td>{{$os->razaosocial}}<br/>{{$os->nomefantasia}} ({{$os->id_cliente}})</td>
                                <td style="text-align:center;">{{$os->entregar_em}}</td>
                                <td style="text-align:center;">{{$os->descricao}}</td>
                            </tr>
                    <? $i++;?>
                @endforeach
                        </table>
                    </div>
                </div>
            @endif
            </div>
        </div>
    </article> <!-- /content page -->
@stop
