@extends('stilearn-metro')

@section('styles')
tr.nogalpao{
    
}
tr.emconsulta{
    background-color:#FA6800;
}
tr.reservadas{
    background-color:#F0A30A;
}
tr.naoenviadas{
    background-color:#A4C400;
}
tr.emcarencia{
    background-color:#E3C800;
}
tr.emfranquia{
    background-color:#4b8df8;
}
@stop

@section('conteudo')
    <header class="content-header">
        <div class="page-header"><h1>Relatório de itens digitados na conferência de remessa</h1></div>
    </header>
    <audio id="snderro">
        <source src="/sons/erro.mp3" type="audio/mp3">
    </audio>

    <article class="content-page clearfix">
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">
                @include('padrao/mensagens')
                <div class="widget" id="filtro">
                    <div class="widget-header bg-cyan">
                        <h4 class="widget-title "><i class="aweso-archive"></i> O cliente e a data para qual deseja listar o que foi digitado</h4>
                    </div>

                    <div class="widget-content">
                        {{Form::open(array('class' => 'form-horizontal', 'id'=>'frmdocumento', 'style'=>'margin-bottom:none;'))}}
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="titulo">Cliente</label>
                                    <div class="controls">
                                        {{Form::select('id_cliente', $clientes, Input::old('id_cliente') , array('id'=>'id_cliente','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="titulo">Data</label>
                                    <div class="controls">
                                        {{Form::text('data', $data ,array('placeholder'=>"", 'class'=>'input-small','id'=>'data' ))}} 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="titulo">Enviar para </label>
                                    <div class="controls">
                                        {{Form::text('email', $email ,array('placeholder'=>"", 'class'=>'input-block-level','id'=>'email' ))}} 
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="form-actions bg-silver">
                            <button type="submit" class="btn bg-orange" id="recuperar" name="pesquisar"><i class="aweso-search"></i> Gerar relatório</button>
                        </div>
                            {{Form::close()}}
                    </div>
                </div><!-- /widget content -->


            </div>
        </div>
    </article> <!-- /content page -->
@stop

@section('scripts')
<script>
@if (Session::has('enviar'))
    var url = 'digitacao/dodia';
    window.open(url);
}
@endif
</script>
@stop