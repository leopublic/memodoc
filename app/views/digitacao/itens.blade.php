@extends('stilearn-metro')

@section('styles')
@stop

@section('conteudo')
    <header class="content-header">
        <div class="page-header"><h1>Conferência de itens para a OS {{$os->id_os}} ({{$nomecliente}})</h1></div>
    </header>
    <audio id="snderro">
        <source src="/sons/erro.mp3" type="audio/mp3">
    </audio>

    <article class="content-page clearfix">
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">
                @include('padrao/mensagens')
                <div class="widget" id="filtro">
                    <div class="widget-header bg-cyan">
                        <h4 class="widget-title "><i class="aweso-archive"></i> Informe os dados da caixa que será cadastrada</h4>
                    </div>

                    <div class="widget-content">
                {{Form::open(array('class' => 'form-horizontal', 'id'=>'frmdocumento', 'style'=>'margin-bottom:none;'))}}
                    {{Form::hidden('ids', '', array('id' => 'ids'))}}
                        <input type="hidden" name="id_os_chave" value="{{$id_os_chave}}"/>
                        <input type="hidden" name="id_caixa" value="{{$id_caixa}}"/>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="control-group">
                                    <label class="control-label" for="titulo">Referência</label>
                                    <div class="controls">
                                        {{Form::text('id_caixapadrao', $documento->id_caixapadrao ,array('placeholder'=>"", 'class'=>'input-small', 'disabled'))}} 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="nume_inicial">ID</label>
                                    <div class="controls">
                                        {{Form::text('nume_inicial', null ,array('placeholder'=>"", 'class'=>'input-small','id'=>'nume_inicial'))}} 
                                        <span id="msgerro"></span>
                                        <span class="help-block">Digite (ou leia com a pistola) os IDs dos itens. Ao final, clique no botão em "ENVIAR" abaixo.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12" id="total">
                            </div>
                        </div>
                        <div>
                            <div class="row-fluid">
                                    <div class="span1" id="lidos1"></div>
                                    <div class="span1" id="lidos2"></div>
                                    <div class="span1" id="lidos3"></div>
                                    <div class="span1" id="lidos4"></div>
                                    <div class="span1" id="lidos5"></div>
                                    <div class="span1" id="lidos6"></div>
                                    <div class="span1" id="lidos7"></div>
                                    <div class="span1" id="lidos8"></div>
                                    <div class="span1" id="lidos9"></div>
                                    <div class="span1" id="lidos10"></div>
                                    <div class="span1" id="lidos11"></div>
                                    <div class="span1" id="lidos12"></div>
                            </div>
                        </div>

                         <div class="form-actions bg-silver">
                            <button type="submit" class="btn bg-orange" id="submit-pesquisa" name="criar"><i class="aweso-upload"></i> Enviar</button>
                            <button type="button" class="btn " id="btnLimpar" onClick="javascript:limpar();"><i class="aweso-trash"></i> Limpar</button>
                        </div>
                {{Form::close()}}
                    </div>
                </div><!-- /widget content -->

                @if(isset($nao_encontrados) && count($nao_encontrados) > 0)
                    <div class="widget border-red">
                        <div class="widget-header bg-red">
                            <h4 class="widget-title"><i class="aweso-question"></i> Itens com erro: não encontrados nas remessas informadas pelo cliente</h4>
                            <div class="widget-action">
                                <button data-toggle="collapse" data-collapse="#filtro" class="btn">
                                    <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                                </button>
                            </div>
                        </div>

                        <div class="widget-content">
                        <?php $qtd = 0; ?>
                        @foreach($nao_encontrados as $item)
                            <div class="row-fluid">
                                <div class="span1">{{ $item['id'] }}</div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                @endif

                @if(isset($ja_cadastrados) && count($ja_cadastrados) > 0)
                    <div class="widget border-red">
                        <div class="widget-header bg-red">
                            <h4 class="widget-title"><i class="aweso-exclamation"></i> Itens com erro: já cadastrados anteriormente</h4>
                            <div class="widget-action">
                                <button data-toggle="collapse" data-collapse="#filtro" class="btn">
                                    <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                                </button>
                            </div>
                        </div>

                        <div class="widget-content">
                        <?php $qtd = 0; ?>
                        @foreach($ja_cadastrados as $item)
                            <div class="row-fluid">
                                <div class="span1">{{ $item->id }}</div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                @endif

            @if(isset($itens))
                <div class="widget border-green">
                    <div class="widget-header bg-green">
                        <h4 class="widget-title"><i class="aweso-ok"></i> Itens cadastrados com sucesso nessa caixa</h4>
                        <div class="widget-action">
                            <button data-toggle="collapse" data-collapse="#filtro" class="btn">
                                <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="widget-content">
                        <?php $qtd = 0; ?>
                        <table class="table">
                        <tr>
                            <th>Item</th>
                            <th>Número</th>
                            <th>Título</th>
                            <th>Conteúdo</th>
                        </tr>
                        @foreach($itens as $item)
                            <tr>
                                <td>{{ $item->id_item }}</td>
                                <td>{{ $item->nume_inicial }}</td>
                                <td>{{ $item->titulo }}</td>
                                <td>{{ $item->conteudo }}</td>
                            </tr>
                            <?php $qtd++; ?>
                        @endforeach
                        </table>
                    </div>
                </div>
            @endif

            </div>
        </div>
    </article> <!-- /content page -->
@stop

@section('scripts')
<script>
 var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");

var total = 0;
var max = 10;
var itens_remessa = [ {{ $itens_remessa }}];

    $( "#nume_inicial" ).focusout(function() {
        if($('#nume_inicial').val() != ''){
            if (total == max){
                document.getElementById('snderro').play();
                $('#msgerro').html('Número máximo atingido. Envie os itens lidos para continuar digitando.');
                $('#nume_inicial').val('');
                $('#nume_inicial').focus();
            } else {
                var digitado = $('#nume_inicial').val().trim();
                //Verificação se o item informado já existe
                // TODO: Adicionar um campo para verificar em itens pré-cadastrados
                var estilo_adicional = '';
                if ($.inArray($('#nume_inicial').val().trim(), itens_remessa) == -1){
                    document.getElementById('snderro').play();
                    $('#msgerro').html('ID '+$('#nume_inicial').val().trim()+' não existe ou já foi digitado. Confira a leitura.');
                    $('#nume_inicial').val('');
                    $('#nume_inicial').focus();
                } else {
                    if ($('#ids').val().indexOf('##'+digitado+'##') > -1){
                            estilo_adicional = 'background-color:#F0AF2D; color:#fff;'
                    }
                    total = total + 1;
                    var col = total % 12 ;
                    if (col == 0){
                        col = 12;
                    }
                    var sep = '';
                    if($('#ids').val() == ''){
                        sep = '##';
                    }
                    $('#ids').val($('#ids').val()+sep+digitado+'##');
                    $( "#lidos"+col ).append( '<div class="row-fluid"><div class="span12" style="border:solid 1px black; text-align:center; padding-top:3px; font-size:11px; font-weight:bold;'+estilo_adicional+'">'+digitado+'</div></div>' );
                    $('#nume_inicial').val('');
                    $('#nume_inicial').focus();
                    snd.play();
                    $('#total').html('Total :'+total);
                }
            }
        }
    });

    $( "#nume_inicial" ).keypress(function() {
        $('#msgerro').html('');
    });


    $('#submit-pesquisa').click(function(event){
        event.preventDefault();
        $(this).html('<i class="fa fa-spinner fa-spin"></i> Aguarde...');
        $(this).attr('disabled', 'disabled');
        $('#frmdocumento').submit();
    });

    function limpar(){
        total = 0;
        $('#ids').val('');
        $('#total').html('');
        $('#lidos1').html('');
        $('#lidos2').html('');
        $('#lidos3').html('');
        $('#lidos4').html('');
        $('#lidos5').html('');
        $('#lidos6').html('');
        $('#lidos7').html('');
        $('#lidos8').html('');
        $('#lidos9').html('');
        $('#lidos10').html('');
        $('#lidos11').html('');
        $('#lidos12').html('');
    }
</script>
@stop