<html>
    <head>
        <style>
            td{vertical-align: top}
            th{vertical-align: top; font-weight: bold; text-align:right;}
        </style>
    </head>
    <body>
        <div style='font-family:arial;border:1px solid #000;border-top:0px; width:100%;padding:10px; '>
        <? $qtd = 1; ?>
        <? $i = 1; ?>
        <? $id_caixapadrao_ant = ""; ?>
            <table style="width:100%; border-collapse: collapse;">
        @foreach($itens as $item)
            @if ($id_caixapadrao_ant == '' || $id_caixapadrao_ant != $item->id_caixapadrao)
                <tr>
                    <th colspan="4" style="text-align:right;">Caixa {{ $item->id_caixapadrao }}</th>
                </tr>
                <tr>
                    <th style="">#</th>
                    <th style="">Código</th>
                    <th style="">Nome</th>
                    <th style="">Período</th>
                    <th style="">CPF</th>
                </tr>
            @endif
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $item->nume_inicial }}</td>
                    <td>{{ $item->alfa_inicial }}</td>
                    <td>{{ $item->dt_inicial }} - {{ $item->dt_final }}</td>
                    <td>&nbsp;</td>
                </tr>
            <? $i++; ?>
        @endforeach
            </table>
    </body>
</html>