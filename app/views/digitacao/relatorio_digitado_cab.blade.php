<table border='0' cellspacing='0' width="100%" style='border:1px solid #000;font:12px arial;'>
    <tr>
        <td width="50%" style="font-size:20px; font-weight: bold; vertical-align: top; padding:0;">
            <img src='../public/img/logo_subtitulo.jpg' style="float:left; vertical-align: text-top; " />
        </td>
        <td width="50%" style="vertical-align: top; width:220px; text-align: right;">
            {{date('d/m/Y')}}<br/>{{date('H:i:s')}}
        </td>
    </tr>
    <tr>
        <td colspan=2 style="font-size:20px; font-weight: bold; vertical-align: top; padding:0; text-align: center;">
            RELATÓRIO DE DIGITAÇÃO REFERENTE AO DIA {{ $dia }}
        </td>
    </tr>
    <tr>
        <td colspan="2" style="font-size:10px; font-weight: normal; vertical-align: top; padding:5px;">
            {{ $cliente->razaosocial }}
        </td>
    </tr>
</table>
