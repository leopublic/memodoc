<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header">
        <h1>
            @if ($cliente['id_cliente'] > 0)
            {{$cliente['razaosocial']. " (".$cliente['id_cliente']." - ".$cliente['nomefantasia'].")"}}
            @else
            Novo cliente
            @endif
        </h1>
    </div>

    <!-- content action (optional)-->
    <ul class="content-action pull-right">
        <li>
            <div class="btn-group">
                <a class="btn btn-primary" href="#"><i class="aweso-folder-close"></i> {{number_format($cliente->qtdDocs(), 0, ",", ".")}} documentos</a>
            </div>
            <div class="btn-group">
                <a class="btn btn-primary" href="#"><i class="aweso-archive"></i> {{number_format($cliente->qtdCaixas(), 0, ",", ".")}} caixas</a>
            </div>
        </li>
    </ul> <!-- /content action -->
</header>
<!--/ content header -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">

            @include ('padrao/mensagens')

            <div class="row-fluid">

            </div>


            <div class="widget border-cyan">
                <div class="widget-content tabbable">
                    <ul id="tabs" class="nav nav-tabs full-widget"  data-tabs="tabs">
                        <li><a href="/cliente/edit/{{$cliente['id_cliente']}}#tab_indentificacao" data-anchor="#tab_indentificacao"><i class="aweso-info icon-large"></i> Identificação</a> </li>
                        @if ($cliente['id_cliente'] > 0)
                        <li><a href="/cliente/enderecoprincipal/{{$cliente['id_cliente']}}#tab_enderecoprincipal" data-anchor="#tab_enderecoprincipal"><i class="aweso-building"></i> Endereço principal</a> </li>
                        <li><a href="/cliente/enderecocor/{{$cliente['id_cliente']}}#tab_enderecocor" data-anchor="#tab_enderecocor"><i class="aweso-envelope"></i> Endereço correspondência</a> </li>
                        <li><a href="/cliente/contrato/{{$cliente['id_cliente']}}#tab_contrato" data-anchor="#tab_contrato"><i class="aweso-folder-open"></i> Contrato</a> </li>
                        <li><a href="/cliente/auto/TelefoneCliente/list/{{$cliente['id_cliente']}}#tab_TelefoneCliente" data-anchor="#tab_TelefoneCliente"><i class="aweso-phone"></i> Telefones</a></li>
                        <!--<li><a href="/cliente/auto/TelefoneContato/list/{{$cliente['id_cliente']}}#tab_contatos" data-anchor="#tab_contatos"><i class="aweso-building"></i> Contatos </a></li> -->
                        <li><a href="/cliente/auto/Contato/list/{{$cliente['id_cliente']}}#tab_Contato" data-anchor="#tab_Contato"><i class="aweso-group"></i> Contatos </a></li>
                        <li><a href="/cliente/endeentrega/{{$cliente['id_cliente']}}#tab_EnderecoEntrega" data-anchor="#tab_EnderecoEntrega"><i class="aweso-truck"></i> Endereços de entrega</a></li>
                        <li><a href="/cliente/auto/CentroDeCusto/list/{{$cliente['id_cliente']}}#tab_CentroDeCusto" data-anchor="#tab_CentroDeCusto"><i class="aweso-usd"></i> Centro de custo</a></li>
                        <li><a href="/cliente/auto/DepCliente/list/{{$cliente['id_cliente']}}#tab_DepCliente" data-anchor="#tab_DepCliente"><i class="aweso-sitemap"></i> Departamentos</a></li>
                        <li><a href="/cliente/auto/SetCliente/list/{{$cliente['id_cliente']}}#tab_SetCliente" data-anchor="#tab_SetCliente"><i class="aweso-tasks"></i> Setores</a></li>
                        <li><a href="/cliente/rateio/{{$cliente['id_cliente']}}#tab_Rateio" data-anchor="#tab_Rateio"><i class="aweso-usd"></i> Rateio deps/centros/setores</a></li>
                        <li><a href="/cliente/propriedade/{{$cliente['id_cliente']}}#tab_Propriedade" data-anchor="#tab_Propriedade"><i class="aweso-tags"></i> Campo personalizado</a></li>
                        <li><a href="/cliente/auto/EmailCliente/list/{{$cliente['id_cliente']}}#tab_EmailCliente" data-anchor="#tab_EmailCliente"><i class="aweso-laptop"></i> E-mails</a></li>
                        <li><a href="/cliente/auto/Solicitante/list/{{$cliente['id_cliente']}}#tab_Solicitante" data-anchor="#tab_Solicitante"><i class="aweso-key"></i> Solicitantes</a></li>
                        <li><a href="/cliente/servicos/{{$cliente['id_cliente']}}#tab_servicos" data-anchor="#tab_servicos"><i class="aweso-shopping-cart"></i> Serviços</a></li>
                        <li><a href="/cliente/pagadores/{{$cliente['id_cliente']}}#tab_pagadores" data-anchor="#tab_pagadores"><i class="aweso-money"></i> Pagadores</a></li>
                        @endif
                    </ul>

                    <div id="my-tab-content" class="tab-content">
