<?php
$largura = "145px";
$altura = "350px";
$fontsize_titulo = "84px";
$fontsize_texto = "11px";
$fontsize_endereco = "30px";
$largura_endereco = "300px";
$fontsize_id = "11px";
$style_endereco_tab = "margin: auto; width:100%; font-size:11px;font-weight:bold;font-family:arial;margin-top: -20px;"
?>
@if($break)
<pagebreak />
@endif
<table style="margin:0px; padding:0px; margin-top:8px; width:100%;">
	<tr>
		@if(isset($etiqueta['esq']))
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }};"><barcode code="{{ $etiqueta['esq']['id'] }}" type="C39" class="barcode" size="2" height="2.5"/></td>
		@else
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }};">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		@endif
		@if(isset($etiqueta['dir']))
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }}"><barcode code="{{ $etiqueta['dir']['id'] }}" type="C39" class="barcode"  size="2" height="2.5"/></td>
		@else
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		@endif
	</tr>
</table>
<table style="border-spacing: 1px; rotate: 90; padding:0; margin:0; margin-bottom: 5px; font-size:10px;font-family:'DejaVuSansCondensed'; height:{{ $largura }};width:{{ $altura }};">
	<tr>
		<td style="font-weight:bold; padding:0; padding-bottom:16px; margin:0; vertical-align: top;">
		@if(isset($etiqueta['dir']))
			<table style="border-spacing: 0px; font-size:{{ $fontsize_titulo }};font-weight:bold;padding:0;">
				<tr>
					<td style="width:310px; padding:0; margin:0; line-height:88%;">{{ $etiqueta['dir']['numero'] }}</td>
					<td style="width:70px;  font-size:{{ $fontsize_id }};font-weight:bold;font-family:arial;vertical-align: top; text-align: right; padding-top:7px;">ID: {{ $etiqueta['dir']['id'] }}</td>
				</tr>
			</table>
			<table style="border-spacing: 0px; padding: 0px; font-size:11px;font-weight:bold;font-family:arial;margin-top: -10px;">
				<tr>
					<td style="font-size:1px; width: 70px;">&nbsp;</td>
					<td style="font-size:1px;">&nbsp;</td>
					<td style="font-size:1px;">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">{{ $etiqueta['dir']['tipo'] }}&nbsp;&nbsp;&nbsp;&nbsp;Reserva: {{ $etiqueta['dir']['reserva'] }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$etiqueta['dir']['migrada_de']}}</td>
				</tr>
				<tr>
					<td colspan="3">{{ $etiqueta['dir']['cliente'] }}</td>
				</tr>
				<tr>
					<td style="vertical-align: bottom;">ID: {{ $etiqueta['dir']['id'] }}</td>
					<td colspan="2" style="text-align: right;"><span style="font-size:{{ $fontsize_endereco }};">{{ $etiqueta['dir']['endereco'] }}</span></td>
				</tr>
			</table>
		@else
		   &nbsp;
		@endif
		</td>
	</tr>
	<tr>
		<td style="font-weight:bold; padding:0; margin:0;vertical-align: top;">
		@if(isset($etiqueta['esq']))
			<table style="border-spacing: 0px; font-size:{{ $fontsize_titulo }};font-weight:bold;padding:0;">
				<tr>
					<td style="width:310px; padding:0; margin:0; line-height:88%;">{{ $etiqueta['esq']['numero'] }}</td>
					<td style="width:70px; font-size:{{ $fontsize_id }};font-weight:bold;font-family:arial;vertical-align: top; text-align: right; padding-top:7px;">ID: {{ $etiqueta['esq']['id'] }}</td>
				</tr>
			</table>
			<table style="border-spacing: 0px; padding: 0px; font-size:11px;font-weight:bold;font-family:arial;margin-top: -10px;">
				<tr>
					<td style="font-size:1px; width: 70px;">&nbsp;</td>
					<td style="font-size:1px;">&nbsp;</td>
					<td style="font-size:1px;">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">{{ $etiqueta['esq']['tipo'] }}&nbsp;&nbsp;&nbsp;&nbsp;Reserva: {{ $etiqueta['esq']['reserva'] }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$etiqueta['esq']['migrada_de']}}</td>
				</tr>
				<tr><td colspan="3">{{ $etiqueta['esq']['cliente'] }}</td></tr>
				<tr>
					<td style="vertical-align: bottom;">ID: {{ $etiqueta['esq']['id'] }}</td>
					<td colspan="2" style="text-align: right;"><span style="font-size:{{ $fontsize_endereco }};">{{ $etiqueta['esq']['endereco'] }}</span></td>
				</tr>
			</table>
		@else
		   &nbsp;
		@endif
		</td>
	</tr>
</table>
