@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Movimentação {{substr('000000'.$movimentacao->id_movimentacao, -6)}}</h1></div>
    <div class='content-action pull-right'>
        <a class="btn btn-sm btn-warning" href="/etiqueta/geremovimentado/{{ $movimentacao->id_movimentacao }}" target="_blank" title="Imprimir etiquetas"><i class="aweso-tags"></i> Etiquetas novas</a>
        <a href='/caixa/movimentacaodepara/{{$movimentacao->id_movimentacao}}' class='btn bg-lime' target="_blank"><i class="aweso-print"></i> De->Para</a>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-share-alt"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Dados da movimentação</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                    <div class='control-group'>
                        {{Form::label('observacao','Cancelar endereço original?',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::checkbox('fl_cancelar_endereco', 1, $movimentacao->fl_cancelar_endereco, array('class' => 'input-sm', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('observacao','Observações',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::textarea('observacao', $movimentacao->observacao, array('rows' => 4, 'class' => 'input-block-level', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    {{Form::close()}}
                    @if (isset($caixas))
                    <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                        <thead>
                            <tr>
                                <th style="width:auto; text-align:center;">ID</th>
                                <th style="width:auto; text-align:center;">Referência</th>
                                <th style="width:auto; text-align:center;">DE</th>
                                <th style="width:auto; text-align:center; border-top: none; border-bottom: none;">&nbsp;</th>
                                <th style="width:auto; text-align:center;">PARA</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($caixas as $caixa)
                            <tr class="">
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa, -6)}}</td>
                                <td style="text-align: center;">{{$caixa->id_caixapadrao}}</td>
                                <td style="text-align: center;">{{$caixa->endereco_antes}}</td>
                                <th style="width:auto; text-align:center; border-top: none; border-bottom: none;"><i class="aweso-arrow-right aweso-2x"></i></th>
                                <td style="text-align: center;">{{$caixa->endereco_depois}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop
