<html>
    <head>
        <style>
            td{vertical-align: top}
            th{vertical-align: top; font-weight: bold; text-align:right;}
        </style>
    </head>
    <body>
        <div style='font-family:arial; width:100%;padding:10px; '>
            <? $qtd = 1; ?>
            <? $i = 1; ?>
            <table style="width:100%; border-collapse: collapse;">
                <tr>
                    <th width="5%" style="text-align:center; color:#828f97;">&nbsp;</th>
                    <th width="10%" style="text-align:center;">&nbsp;</th>
                    <th width="10%" style="text-align:center;">&nbsp;</th>
                    <th width="30%" style="text-align:center;">&nbsp;</th>
                    <th width="10%" style="text-align:center;">&nbsp;</th>
                    <th width="30%" style="text-align:center;">&nbsp;</th>
                </tr>
                <tr>
                    <th width="10%" style="text-align:center; color:#828f97;">#</th>
                    <th width="10%" style="text-align:center;">Caixa</th>
                    <th width="10%" style="text-align:center;">ID</th>
                    <th width="30%" style="text-align:center;">De</th>
                    <th width="10%" style="text-align:center;">&nbsp;</th>
                    <th width="30%" style="text-align:center;">Para</th>
                </tr>
                @foreach($caixas as $caixa)
                <tr style="font-size:14px; font-weight: bold;">
                    <td rowspan="2" style="text-align:center; border-bottom:solid 1px #828f97; vertical-align: top; font-size:14px; font-weight: normal; color:#828f97;">{{$i}}</td>
                    <td style="text-align:center;">
                        {{substr('000000'.$caixa->id_caixapadrao, -6)}}
                    </td>
                    <td style="text-align:center; ">
                        {{substr('000000'.$caixa->id_caixa, -6)}}
                    </td>
                    <td style="text-align:center;">
                        {{$caixa->endereco_antes}}
                    </td>
                    <td style="text-align:center; ">=></td>
                    <td style="text-align:center; ">
                        {{$caixa->endereco_depois}}
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align:left; border-bottom:solid 1px #828f97;">
                        <span style="font-size:9px; font-weight: normal">{{$caixa->razaosocial}} ({{$caixa->id_cliente}})</span>
                    </td>
                </tr>
            <? $i++; ?>
                @endforeach
            </table>
    </body>
</html>
