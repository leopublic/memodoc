@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Novo endereço</h1>
            
        </div>

        <!-- content breadcrumb -->
        <ul class="breadcrumb breadcrumb-inline clearfix">
            <li><a href="#">Cadastro</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
            <li class="active">Endereço</li>
        </ul>
        
        
    </header> <!--/ content header -->
    
    <!-- content page -->
    <article class="content-page clearfix">
        
        <!-- main page -->
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">
    

        <!-- widget form horizontal -->
        <div class="widget border-cyan" id="widget-horizontal">
            <!-- widget header -->
            <div class="widget-header bg-cyan">
                <!-- widget icon -->
                <div class="widget-icon"><i class="aweso-edit"></i></div>
                <!-- widget title -->
                <h4 class="widget-title">Novo endereço</h4>
                <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                <div class="widget-action color-cyan">
                    <button data-toggle="collapse" data-collapse="#widget-horizontal" class="btn">
                        <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                    </button>
                </div>
            </div><!-- /widget header -->
            
                @if (Session::has('msg'))
                <div class="alert alert-info"><h4>Mensagem</h4>{{ Session::get('msg') }}</div>
                @endif
                
                @if (Session::has('success'))
                <div class="alert alert-success"><h4>Sucesso!</h4>{{ Session::get('success') }}</div>
                @endif
                
                @if (Session::has('error'))
                <div class="alert alert-error"><h4>Erro!</h4>{{ Session::get('error') }}</div>
                @endif
                
            <!-- widget content -->
            <div class="widget-content">
                {{ Form::model($model, array('class'=>'')) }}
                 <div class='span5 pull-left form-horizontal'>   
                     <div class='control-group'>
                         {{Form::label('id_galpao','Galpão',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_galpao', null, array('placeholder'=>'Número do galpão', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_rua','Rua',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_rua', null, array('placeholder'=>'Número da rua', 'class'=>'input-xlarge','required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('qtd_predio','Qtde de Predios por rua',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('qtd_predio', null, array('placeholder'=>'Quantidade de Prédios por rua', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('qtd_andar_par','Andares do lado PAR',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('qtd_andar_par', null, array('placeholder'=>'O lado PAR possui quantos andares ?', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('qtd_andar_impar','Andares do lado IMPAR',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('qtd_andar_impar', null, array('placeholder'=>'O lado IMPAR possui quantos andares ?', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('qtd_apartamento_andar','Qtde de apartamentos',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('qtd_apartamento_andar', null, array('placeholder'=>'Qtde de apartamentos por andar', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         
                         
                         
                         {{Form::label('id_tipodocumento','Tipo de documento',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::select('id_tipodocumento', $tipodocumento)}} 
                         </div>
                     </div>
                     
                 </div>
                @if(isset($tags))
                <div class='pull-left span5'>
                    <h2>Total de ({{count($tags)}}) endereços foram criados, veja:</h2>
                    @foreach($tags as $tag)
                        {{$tag}} <br />
                    @endforeach
                </div>
                @endif
                   <div class='clearfix'></div>
                   <div class="form-actions bg-silver">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button type="reset" class="btn">Cancelar</button>
                   </div>
                </div><!-- /widget content -->
            </div> <!-- /widget form horizontal -->





              

                 {{Form::close()}}



            </div>
        </div>
    </article> <!-- /content page -->           
@stop