@extends('stilearn-metro')

@section('conteudo')

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>{{$titulo}}</h1></div>

    <!-- content breadcrumb -->
    <ul class="breadcrumb breadcrumb-inline clearfix">
        <li><a href="#">Cadastros</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
        <li class="active">{{$titulo}}</li>
    </ul>

    <div class='content-action pull-right'>
        @if(isset($linkCriacao))
        <a href='/{{$linkCriacao}}' class='btn'>Novo</a>
        @endif
        <a class="btn bg-lime " href="/cliente/excel" target="_blank"><i class="aweso-table"></i> Excel</a>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-search"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Pesquisar</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                    {{ Form::open(array('class'=> 'form-horizontal')) }}
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label">Razão social ou fantasia</label>
                                <div class="controls">
                                    {{Form::text('search', Input::old('search'),array('class'=>'input-block-level'))}}
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label" for="fl_ativo">Status</label>
                                <div class="controls">
                                    <label class="radio">
                                        {{ Form::radio('fl_ativo', '1', (Input::old('fl_ativo') == '1' || Input::old('fl_ativo') == '') ? true : false, array('id'=>'1')) }}
                                        somente ativos
                                    </label>
                                    <label class="radio">
                                        {{ Form::radio('fl_ativo', '0', (Input::old('status') == '0') ? true : false, array('id'=>'0')) }}
                                        somente inativos
                                    </label>
                                    <label class="radio">
                                        {{ Form::radio('fl_ativo', '-1', (Input::old('fl_ativo') == '-1') ? true : false, array('id'=>'-1')) }}
                                        todos
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" class="btn bg-orange">Buscar</button>
                        <button type="reset" class="btn">Limpar</button>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
            <!-- List -->
            <table class="listagem table table-striped table-condensed" data-sorter="true" style="width:100%;">
                <colgroup>
                    <col width="80px"/>
                    <col width="50px"/>
                    <col width="auto"/>
                    <col width="180px"/>
                    <col width="100px"/>
                </colgroup>
                <thead>
                    <tr>
                        @if(isset($linkEdicao))
                        <th style="text-align:center;">Ações</th>
                        @endif
                        <th style="text-align:center;">ID</th>
                        <th>Razão social<br><span style="font-size:10px;">Nome fantasia</span></th>
                        <th style="text-align:center;">CNPJ/CPF</th>
                        <th style="text-align:center;">Status</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($instancias as $r)
                    @if($r->fl_ativo == 0)
                    <tr class="warning">
                    @else
                    <tr>
                    @endif
                        <td style="text-align:center;">
                            @if(isset($linkEdicao))
                            <a class="btn btn-small" title="Editar" href="/{{ $linkEdicao }}/{{ $r->getKey() }}"><i class="aweso-pencil"></i></a>
                            @endif
                        @if (Auth::user()->previlegio >=5)
                            @if(intval($r->fl_ativo) == 1)
                            <a class="btn btn-small" title="Inativar esse cliente" href="/cliente/inative/{{ $r->getKey() }}"><i class="aweso-thumbs-down"></i></a>
                            @endif
                            @if(isset($linkRemocao))
                            <a class="btn btn-small" title="Remover" href="/{{ $linkRemocao }}/{{ $r->getKey() }}"><i class="aweso-remove"></i></a>
                            @endif
                        @endif
                        </td>
                        <td style="text-align:center;">{{$r->id_cliente}}</td>
                        <td>{{$r->razaosocial }}<br/><span style="font-size:10px;">{{$r->nomefantasia }}</span></td>
                        <td style="text-align:center;">
                            @if(trim($r->cgc) != '')
                                @if(strlen(trim($r->cgc)) > 11)
                                {{ substr($r->cgc, 0,2).'.'.substr($r->cgc, 2,3).'.'.substr($r->cgc, 5,3).'/'.substr($r->cgc, 8,4).'-'.substr($r->cgc, 12,2) }}
                                @else
                                {{substr($r->cgc, 0,3).'.'.substr($r->cgc, 3,3).'.'.substr($r->cgc, 6,3).'-'.substr($r->cgc, 9,2)}}
                                @endif
                            @else
                            --
                            @endif
                        </td>
                        <td style="text-align:center;">@if($r->fl_ativo)
                            ativo
                            @else
                            inativo
                            @endif</td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .c{
        width:200px;
        text-align:center;
    }
</style>
@stop