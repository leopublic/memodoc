@extends('stilearn-metro')

@section('conteudo')

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Bem-vindo!</h1></div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">
            @include('padrao.mensagens')
            <div class="row-fluid">
                @if (Auth::user()->nivel->minimoAtendimentoNivel2())
                <div class="span2 tile bg-teal outline-teal">
                    <a href="/cliente">
                        <div class="tile-content">
                            <i class="icomo-address-book"></i>
                        </div>
                        <div class="tile-peek">
                            <span class="brand">Clientes</span>
                        </div>
                    </a>
                </div> <!-- /tile -->

                <div class="span2 tile bg-emerald outline-emerald">
                    <a href="{{\Memodoc\Servicos\Rotas::ordemservico()}}/cliente">
                        <div class="tile-content">
                            <i class="aweso-plus"></i>
                        </div>
                        <div class="tile-peek">
                            <span class="brand">Abrir OS</span>
                        </div>
                    </a>
                </div> <!-- /tile -->
                <div class="span2 tile bg-orange outline-orange">
                    <a href="/usuario">
                        <div class="tile-content">
                            <i class="icomo-users-2"></i>
                        </div>
                        <div class="tile-peek">
                            <span class="brand">Usuários</span>
                        </div>
                    </a>
                </div>
                @endif
                <div class="span2 tile bg-cobalt outline-cobalt">
                    <a href="/checkout">
                        <div class="tile-content">
                            <i class="icomo-box-add"></i>
                        </div>
                        <div class="tile-peek">
                            <span class="brand">Checkout</span>
                        </div>
                    </a>
                </div>
                <div class="span2 tile bg-amber outline-amber">
                    <a href="/checkin">
                        <div class="tile-content">
                            <i class="icomo-box-remove"></i>
                        </div>
                        <div class="tile-peek">
                            <span class="brand">Checkin</span>
                        </div>
                    </a>
                </div>
                <!-- /tile -->
            </div> <!-- /row -->
            
        @if (Auth::user()->nivel->minimoAtendimentoNivel2())
            <div class="row-fluid">
                <div class="widget border-cyan " id="widget-horizontal">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-truck"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Novas ordens de serviço não atendidas</h4>
                        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        <div class="widget-action color-cyan">
                        </div>
                    </div><!-- /widget header -->
                    <!-- widget content -->
                    <div class="widget-content">
                        @if(isset($os) && count($os) > 0)
                        @include('ordemservico_pendente', array('os' => $os)  )
                        @else
                        Nenhuma nova OS. (Que tal um café?)
                        @endif
                    </div>
                </div>
            </div>
        @endif
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .c{
        width:200px;
        text-align:center;
    }
</style>
@stop