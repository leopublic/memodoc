@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Expurgar caixas</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-remove"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Informe o cliente e o número inicial e final das caixas (referências) a serem expurgadas</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal', 'url'=>'expurgo/caixasconfirmado'))}}
                    {{Form::hidden('id_cliente', Input::old('id_cliente'))}}
                    {{Form::hidden('id_caixapadrao_ini', Input::old('id_caixapadrao_ini'))}}
                    {{Form::hidden('id_caixapadrao_fim', Input::old('id_caixapadrao_fim'))}}
                    {{Form::hidden('cortesia', Input::old('cortesia'))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('razaosocial', $razaosocial, array('class' => 'input-block-level', 'disabled' => 'disabled'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        {{Form::label('id_caixapadrao','Referências ',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('id_caixapadrao_ini', Input::old('id_caixapadrao_ini') ,array('class'=>'input-small', 'disabled' => 'disabled' ))}}
                            </div>
                            A
                            <div class="input-append">
                                {{Form::text('id_caixapadrao_fim', Input::old('id_caixapadrao_fim') ,array('class'=>'input-small', 'disabled' => 'disabled'))}}
                            </div>
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('cortesia','Cortesia?',array('class'=>'control-label'))}}
                        <div class="controls">
                            @if (Input::old('cortesia') == 1)
                            {{Form::text('xxx', "SIM", array('disabled' => 'disabled'))}}
                            @else
                            {{Form::text('xxx', "NÃO", array('disabled' => 'disabled'))}}
                            @endif
                        </div>
                    </div>
                    <div class="alert alert-error">
                       Confirma a exclusão das caixas relacionadas abaixo? ATENÇÃO: ESSA OPERAÇÃO NÃO PODE SER DESFEITA!!! <button type="submit" name="buscar" class="btn btn-primary" id="submit-pesquisa"><i class="aweso-remove"></i> Confirmar</button> <a href="{{URL::to('/expurgo/caixas')}}" class="btn"> Desistir</a>
                    </div>

                    {{ Form::close() }}

                    <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                        <thead>
                            <th style="width:auto; text-align:center;">ID</th>
                            <th style="width:auto; text-align:center;">Referência</th>
                            <th style="width:auto; text-align:center;">Primeira entrada</th>
                            <th style="width:auto; text-align:center;">Guarda em dias</th>
                            <th style="width:auto; text-align:center;">Guarda em anos</th>
                            <th style="width:auto;">&nbsp;</th>
                        </thead>
                        <tbody>

                            @foreach($caixas as $caixa)
                            <tr class="">
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa, -6)}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao, -6)}}</td>
                                <td style="text-align: center;">{{$caixa->primeira_entrada}}</td>
                                <td style="text-align: center;">{{number_format($caixa->guardaDias, 0, ",", "." )}}</td>
                                <td style="text-align: center;">{{number_format($caixa->guardaAnos, 1, ",", "." )}}</td>
                                <td style="width:auto;">&nbsp;</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop