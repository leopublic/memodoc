@extends('stilearn-metro')

@section('conteudo')
    @include('padrao/admin/content-header', array('titulo' => 'Checkin'))
    <!-- content page -->
    @if (Session::has('success'))
        <audio autoplay>
            <source src="/sons/ok.mp3" type="audio/mp3">
        </audio>
    @endif
    @if (Session::has('errors'))
        <audio autoplay>
            <source src="/sons/erro.mp3" type="audio/mp3">
        </audio>
    @endif
    @if (Session::has('warning'))
        <audio autoplay>
            <source src="/sons/erro.mp3" type="audio/mp3">
        </audio>
    @endif

    <article class="content-page clearfix">

        <!-- main page -->
        <div class="main-page" id="">
            <div class="content-inner">
                <!-- widget form horizontal -->
                    @include('padrao/mensagens')
                {{ Form::open(array('class'=>'form-horizontal', 'id' => 'form-checkin')) }}
                <div class="widget border-cyan" id="widget-horizontal">
                    @include('padrao/widget_header', array('titulo' => 'Informe o ID', 'icone' => '<i class="aweso-archive"></i>'))
                    <!-- widget content -->
                    <div class="widget-content">
                        @if (!$readonly)
                        <div class="control-group">
                            <div class='input-append'>
                                 {{Form::text('id_caixa', null, array('placeholder'=>'ID Caixa', 'class'=>'input-small', 'id' => 'id_caixa', 'autofocus'))}}
                                 <button type="submit" name="Adicionar" id="Adicionar" class="btn btn-primary">Checkin</button>
                             </div>
                        </div>
                        @else
                            <div class="row-fluid">
                                <div class="span12 bg-orange" style="padding:10px;margin-bottom:10px;">Existe um faturamento fechado na data de hoje, e novos checkins poderão afetar o faturamento. Entre em contato com a coordenação.</div>
                            </div>
                        @endif
                    </div>
                </div>
                <!-- widget form horizontal -->
                <div class="widget border-cyan" id="widget-horizontal">
                    @include('padrao/widget_header', array('titulo' => 'Caixas pendentes e caixas recebidas hoje', 'icone' => '<i class="aweso-archive"></i>'))
                    <div class="widget-content">
                    @if(isset($caixas))
                       <table  data-sorter="true" class='table table-condensed table-bordered'>
                           <thead>
                                <th style="width:70px;text-align: center;">Status</th>
                                <th style="width:auto;">Cliente</th>
                                <th style="width:90px;text-align: center;">OS</th>
                                <th style="width:100px;text-align: center;" title="Data de atendimento da OS">Atendida em</th>
                                <th style="width:50px;text-align: center;">ID</th>
                                <th style="width:60px;text-align: center;">Referência</th>
                                <th style="width:50px;text-align: center;">&nbsp;</th>
                           </thead>
                           <tbody>
                                @foreach($caixas as $caixa)
                                @if ($caixa->id_os_chave > 0)
                                    @if ($caixa->entregar_em_Ymd < date('Y-m-d') && $caixa->dt_checkin == '')
                                        <tr class="error">
                                    @endif
                                @else
                                    <tr>
                                @endif
                                        <td style="text-align: center;">
                                            @if($caixa->dt_checkin == '')
                                            (pendente)
                                            @else
                                            <i class="aweso-ok green"></i>
                                            @endif
                                        </td>
                                        <td>{{ $caixa->razaosocial}} ({{$caixa->id_cliente}} -{{$caixa->nomefantasia}})</td>
                                    @if ($caixa->id_os_chave > 0)
                                        <td style="text-align: center;"><a href="{{URL::to('/ordemservico/visualizar/'.$caixa->id_os_chave)}}" target="_blank" style="color:inherit;text-decoration: underline;" title="Clique para consultar a OS numa nova tela..">OS {{$caixa->id_os }}</a></td>
                                        <td style="text-align: center;">{{$caixa->entregar_em_fmt }}</td>
                                    @else
                                        <td style="text-align: center;"><a href="{{URL::to('/transbordo/caixas/'.$caixa->id_transbordo)}}" target="_blank" style="color:inherit;text-decoration: underline;" title="Clique para consultar o transbordo numa nova tela..">TB {{$caixa->id_transbordo}}</a></td>
                                        <td style="text-align: center;">--</td>
                                    @endif
                                        <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa, -6)}}</td>
                                        <td style="text-align: center;">{{$caixa->id_caixapadrao_fmt}}</td>
                                        <td>&nbsp;</td>
                                  </tr>
                               @endforeach
                           </tbody>
                       </table>
                    @endif
                    </div>
                </div>
                {{Form::close()}}

            </div>
        </div>
    </article> <!-- /content page -->

@stop
@scripts
<script>
    function PlaySound(soundObj) {
        var sound = document.getElementById(soundObj);
        sound.Play();
    }
</script>
@stop
