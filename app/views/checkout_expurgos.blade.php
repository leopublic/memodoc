@extends('stilearn-metro')

@section('conteudo')
@include('padrao/admin/content-header', array('titulo' => 'Checkout - EXPURGOS'))
@if (Session::has('success'))
<audio autoplay>
    <source src="/sons/ok.mp3" type="audio/mp3">
</audio>
@endif
@if (Session::has('errors'))
<audio autoplay>
    <source src="/sons/erro.mp3" type="audio/mp3">
</audio>
@endif
<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            <!-- widget form horizontal -->
            {{ Form::open(array('class'=>'form-horizontal', 'id' => 'form-checkin')) }}
            <div class="widget border-cyan" id="widget-horizontal">
                @include('padrao/widget_header', array('titulo' => 'Informe o ID', 'icone' => '<i class="aweso-archive"></i>'))
                <!-- widget content -->
                <div class="widget-content">
                    <div class="control-group">
                        <div class='input-append'>
                            {{Form::text('id_caixa', null, array('placeholder'=>'ID Caixa', 'class'=>'input-small', 'id' => 'id_caixa', 'autofocus'))}}
                            <button type="submit" name="Adicionar" id="Adicionar" class="btn btn-primary">Checkout</button>
                        </div>
                    </div>
                    @include('padrao/mensagens')
                </div>
            </div>
            {{Form::close()}}
            <div class="widget border-cyan" id="widget-horizontal">
                @include('padrao/widget_header', array('titulo' => 'Expurgos aguardando checkout', 'icone' => '<i class="aweso-trash"></i>'))
                <div class="widget-content">
                    @if(isset($caixas_expurgadas))
                        @if (Auth::user()->nivel->minimoAdministrador() && $qtd_expurgos > 0)
                            <div class="btn-group">
                                <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-danger " title="Dar checkout em todos os expurgos pendentes">Fazer checkout em todos os {{number_format($qtd_expurgos, 0, ",", ".")}} expurgos pendentes</a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ URL::to('/checkout/baixarexpurgos') }}">Clique aqui para confirmar o checkout em todos os {{number_format($qtd_expurgos, 0, ",", ".")}} expurgos pendentes</a></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <br/>
                        @endif
                    <table  data-sorter="true" class='table table-condensed table-bordered'>
                        <thead>
                        <th style="width:70px;text-align: center;">Status</th>
                        <th style="width:120px;text-align: left;">Checkout por</th>
                        <th style="width:50px;text-align: center;">ID</th>
                        <th style="width:60px;text-align: center;">Referência</th>
                        <th style="width:auto;">Cliente</th>
                        <th style="width:60px;text-align: center;">Expurgo</th>
                        </thead>
                        <tbody>
                            @foreach($caixas_expurgadas as $caixa)
                            <tr>
                                <td style="text-align: center;">
                                    @if($caixa['data_checkout'] == '')
                                    (pendente)
                                    @else
                                    <i class="aweso-ok green"></i>
                                    @endif
                                </td>
                                <td style="text-align: left;">{{$caixa['nomeusuario']}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa['id_caixa'], -6)}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa['id_caixapadrao'], -6)}}</td>
                                <td>{{ $caixa['razaosocial'].' ('. $caixa['id_cliente']. ' - '. $caixa['nomefantasia'] .')' }}</td>
                                <td style="text-align: center;">
                                    <a href="{{URL::to('/expurgo/caixasgravadas/'.$caixa['id_expurgo_pai'])}}" target="_blank" style="color:inherit;text-decoration: underline;" title="Clique para consultar os detalhes desse expurgo">{{substr('000000'.$caixa['id_expurgo_pai'], -6) }}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>

        </div>
    </div>
</article> <!-- /content page -->

@stop
