{{ $os->links() }}
<table  data-sorter="true" class='table table-hover table-condensed table-bordered' style="font-size:11px;">
    <thead>
        <th style="width:50px; text-align: center;">OS</th>
        <th style="width:30px; text-align: center;" title="Urgente"><i class="aweso-exclamation red"></i></th>
        <th style="width:150px;">Aberta por</th>
        <th style="width:150px;">Responsável</th>
        <th style="width:auto;">Cliente</th>
        <th style='width:130px; text-align: center;'>Solicitado em</th>
        <th style='width:90px; text-align: center;'>Entregar</th>
    </thead>
    <tbody>
        @foreach($os as $data)
        @if($data->urgente == 1)
        <tr class="bg-red" onclick="window.location.assign('/ordemservico/visualizarlimpo/{{$data->getKey()}}')" style="cursor: pointer;">
        @else
        <tr onclick="window.location.assign('/ordemservico/visualizarlimpo/{{$data->getKey()}}')" style="cursor: pointer;">
        @endif
            <td style="text-align: center;">{{substr('00000'.$data->id_os, -6)}}</td>
            <td style="text-align: center;">
                @if($data->urgente == 1)
                <i class="aweso-exclamation red aweso-2x"></i>
                @endif
            </td>
            <td style="">
                @if (is_object($data->usuariocadastro))
                    @if ($data->usuariocadastro->id_nivel < 10)
                    <i class="aweso-star"></i>
                    <span style="font-weight:bold;">{{ucwords(strtolower($data->usuariocadastro->nomeusuario))}}</span>
                    @else
                    <i class="aweso-star-empty " style="color:transparent;"></i>
                    {{ucwords(strtolower($data->usuariocadastro->nomeusuario))}}
                    @endif
                @endif
            </td>
            <td>{{ucwords(strtolower($data->responsavel))}}</td>
            <td>{{$data->cliente->razaosocial}}</td>
            <td style="text-align: center;">{{$data->solicitado_em}}</td>
            <td style="text-align: center;">{{$data->entregar_em}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
