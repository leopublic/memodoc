@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>{{$cliente->nomefantasia}}</h1>
            
        </div>
       

        <!-- content breadcrumb -->
        <ul class="breadcrumb breadcrumb-inline clearfix">
            <li><a href="#">Cadastro</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
            <li class="active">Realizar OS</li>
        </ul>
        
        
    </header> <!--/ content header -->
    
    <!-- content page -->
    <article class="content-page clearfix">
        
        <!-- main page -->
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">
    
            @if (Session::has('msg'))
            <div class="alert alert-info"><h4>Mensagem</h4>{{ Session::get('msg') }}</div>
            @endif

            @if (Session::has('success'))
            <div class="alert alert-success"><h4>Sucesso!</h4>{{ Session::get('success') }}</div>
            @endif

            @if (Session::has('error'))
            <div class="alert alert-error"><h4>Erro!</h4>{{ Session::get('error') }}</div>
            @endif  
                
                
                
         {{ Form::model($model, array('class'=>'')) }}         
            <!-- widget form horizontal -->
            <div class="widget border-cyan span6" id="widget-horizontal">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-edit"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Ordem de serviço</h4>
                    <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                    <div class="widget-action color-cyan">
                        <button data-toggle="collapse" data-collapse="#widget-horizontal" class="btn">
                            <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                        </button>
                    </div>
                </div><!-- /widget header -->
            

                
               
                <!-- widget content -->
                <div class="widget-content">
               

                 <div class='span5 pull-left form-horizontal'>   

                     <div class='control-group'>
                         {{Form::label('id_os','Num da OS',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_os', $id_os, array('placeholder'=>'Numero da OS', 'class'=>'input-xlarge', 'required'))}} 
                         {{Form::hidden('id_cliente', $cliente->id_cliente, array('id'=>'id_cliente'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('responsavel','Solicitado por',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('responsavel', null, array('placeholder'=>'nome da pessoa responsável pela OS', 'class'=>'input-xlarge','required'))}} 
                         </div>
                     </div>
                    <div class="control-group">
                       {{Form::label('solicitado_em','Solicitado em',array('class'=>'control-label'))}} 
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('solicitado_em', null ,array('placeholder'=>"Solicitado em",  'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                <span class="add-on"><i class="icomo-calendar"></i> </span>
                            </div>
                        </div> 
                    </div>
                    <div class="control-group">
                       {{Form::label('entregar_em','Executar em',array('class'=>'control-label'))}} 
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('entregar_em', null ,array('placeholder'=>"Executar em", 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}} 
                                <span class="add-on"><i class="icomo-calendar"></i> </span>
                            </div>
                        </div>
                    </div>
                        
                    
                     <div class='control-group'>
                         {{Form::label('id_departamento','Departamento',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::select('id_departamento', FormSelect($cliente->departamento,'nome'), null, array('data-fx'=>'select2', 'required'))}} 
                         </div>
                     </div>

                     <div class='control-group'>
                         {{Form::label('tipoemprestimo','Tipo de empréstimo',array('class'=>'control-label'))}} 
                         <div class="controls">
                             <select name='tipoemprestimo' id='tipoemprestimo' data-fx='select2'>
                                 <option value='0'>Entrega MEMODOC</option>
                                 <option value='1'>Consulta no local</option>
                                 <option value='2'>Retirado pelo cliente</option>
                             </select>
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_endentrega','Local entrega/retirada',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::select('id_endentrega', FormSelect($cliente->enderecoentrega,'logradouro'), null, array('data-fx'=>'select2'))}}
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('urgente', 'Urgente',array('class'=>'control-label'))}}
                         <div class="controls">
                         {{Form::checkbox('urgente','1',true)}}
                         </div>
                     </div> 
                     
                     <div class='control-group'>
                         {{Form::label('qtdenovascaixas','Qtde de novas caixas',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('qtdenovascaixas', null ,array('placeholder'=>"Qtd novas caixas"))}} 
                         </div>
                     </div> 
                     <div class='control-group'>
                         {{Form::label('naoecartonagemcortesia','Cortesia ?',array('class'=>'control-label'))}} 
                         <div class="controls">
                          {{Form::checkbox('naoecartonagemcortesia','0',array('class'=>'input-fx'))}} 
                         </div>
                     </div> 
                    
                     <div class='control-group'>
                         {{Form::label('apanhanovascaixas','Caixas cheias novas',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('apanhanovascaixas', null ,array('placeholder'=>"Caixas cheias novas"))}} 
                         </div>
                     </div> 
                     <div class='control-group'>
                         {{Form::label('apanhacaixasemprestadas','Caixas de devolução',array('class'=>'control-label'))}} 
                         <div class="controls">
                          {{Form::text('apanhacaixasemprestadas',null ,array('placeholder'=>"Caixas de devolução"))}} 
                         </div>
                     </div> 
                     <div class='control-group'>
                         {{Form::label('cobrafrete', 'Cobrar frete',array('class'=>'control-label'))}}
                         <div class="controls">
                         {{Form::checkbox('cobrafrete','1',true)}}
                         </div>
                     </div>  
                    </div>
               
               
                    <div class='clearfix'></div>
                 </div><!-- /widget content -->
             </div> <!-- /widget form horizontal -->

        

                <!-- Caixas emprestimo -->
                <div class="span6">
                    <!-- widget Caixas emprestimo -->
                    <div class="widget border-cyan" id="widget-chat">
                        <!-- widget header -->
                        <div class="widget-header bg-cyan">
                            <!-- widget icon -->
                            <div class="widget-icon"><i class="aweso-truck"></i></div>
                            <!-- widget title -->
                            <h4 class="widget-title">Caixas para o empréstimo</h4>
                            <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                            <div class="widget-action color-cyan">
                                <a href='#' class="btn color-white" data-toggle="add-others">&plus;</a>
                                <button data-toggle="collapse" data-collapse="#widget-chat" class="btn">
                                    <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                                </button>
                            </div>
                        </div><!-- /widget header -->

                        <!-- widget content -->
                        <div class="widget-content bg-silver">
                            <div class="_add-to-chat bg-silver">
                                <div id="form-addtochat" />
                                   <input type='text' id='id_caixapadrao' placeholder='Caixa padrão' style='width:140px' />
                                   <input type='text' id='id_box'  placeholder='Box'  style='width:140px'  />
                                   <input type='text' id='id_item'  placeholder='Item'  style='width:140px'  />
                                   <a href='#' class="add-caixa btn bg-cyan bordered">add</a>
                                </div>
                            </div>
                            <table class='table table-striped  table-condensed'>
                                <thead>
                                    <tr>
                                        <th style='width:100px'>Caixa padrão</th>
                                        <th style='width:300px'>Título</th>
                                        <th>Box</th>
                                        <th>Item</th>
                                    </tr>
                                </thead>
                                <tbody class='caixa-content'>
                                </tbody>
                            </table> 

                        </div><!-- /widget content -->
                    </div><!-- /widget Caixas emprestimo -->
                </div><!-- /Caixas emprestimo -->  

                
                <!-- Observações -->
                <div class="span6">
                    <!-- widget Caixas emprestimo -->
                    <div class="widget border-cyan" id="widget-obs">
                        <!-- widget header -->
                        <div class="widget-header bg-cyan">
                            <!-- widget icon -->
                            <div class="widget-icon"><i class="aweso-comment"></i></div>
                            <!-- widget title -->
                            <h4 class="widget-title">Observações</h4>
                        </div><!-- /widget header -->
                        <!-- widget content -->
                        <div class="widget-content bg-silver"> 

                                {{Form::textarea('observacao', null, array('style'=>'height:40px','class'=>'span5'))}}

                                <div class='clearfix'></div>
                        </div><!-- /widget content -->
                    </div><!-- /widget Observações -->
                </div><!-- /Observações -->  
                
                
                
                
                <div class='span6'> 
                   <div class="form-actions bg-silver">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button type="reset" class="btn">Cancelar</button>
                   </div> 
                </div>
                
                 {{Form::close()}}
            </div>
        </div>
    </article> <!-- /content page -->  
    
@stop

@section('scripts')
<script>

    
  $(function(){   

    $('.add-caixa').click(function(){
        dados = {
                id_caixapadrao:$('#id_caixapadrao').val(),
                id_item:$('#id_item').val(),
                id_box:$('#id_box').val(),
                id_cliente:$('#id_cliente').val()
        };
        $(this).html('...');
        $caixa = $('.caixa-content');
        
        $.ajax({
            type: "POST",
            url: '/ordemservico/addcaixa',
            data: dados,
            dataType: 'json',
            success: function(data){
                $('.add-caixa').html('add');
                $('.add-to-chat').hide(200);
                if(data.length === 0){
                    alert('Nenhum registro foi encontrado para este cliente');
                }
                $('#id_caixapadrao').focus();
                $.each(data, function(i, item) {

                     $caixa.append('<tr>'+
                                         '<input type="hidden" name="id_documento['+item.id_documento+']" value="'+item.id_caixapadrao+'" />'+
                                         '<td>'+item.id_caixapadrao+'</td>'+
                                         '<td>'+item.titulo+'</td>'+
                                         '<td>'+item.id_box+'</td>'+
                                         '<td>'+item.id_item+'</td>'+
                                   '</tr>'); 
                           
                           
                })                       
                
            }
        });
        
    });
    
    $('[data-toggle="add-others"]').click(function(){
        $(".add-to-chat").slideToggle("fast","easeOutBack");
    });
   
  });

  
</script>
<style>
    .date{
        width:96px;
    }
    .form-horizontal .control-group{
        margin-bottom:5px !important;
    }
    .select2-container .select2-choice{
        font-size:14px !important;
    }
</style>
@stop