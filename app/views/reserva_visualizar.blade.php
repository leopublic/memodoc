@extends('stilearn-metro')

@section('conteudo')

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>{{$reserva->cliente->nomefantasia}} > Reserva {{$reserva->id_reserva}} (criada em {{$reserva->data_reserva_fmt}})</h1></div>

    <ul class="content-action pull-right">
        <li><a class="btn bg-lime " href="{{URL::to('/etiqueta/gere/'.$reserva->id_reserva)}}" target="_blank"><i class="aweso-tags"></i> Etiqueta</a></li>
        <li><a class="btn bg-lime " href="{{URL::to('/reserva/relatorio/'.$reserva->id_reserva)}}" target="_blank"><i class="aweso-print"></i> Relação de endereços</a></li>
        <li><a class="btn bg-lime " href="{{URL::to('/reserva/inventario/'.$reserva->id_reserva)}}" target="_blank"><i class="aweso-edit"></i> Planilha</a></li>
    </ul>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">
            @include('/padrao/mensagens')



            <h5 class="pull-left"> Foram localizados {{$documentos->count()}} endereços associados à reserva {{substr('000000'.$reserva->id_reserva, -6)}}</h5>
            <div class="clearfix"></div>


            <table class='table table-striped table-hover table-bordered table-condensed'>
                <thead>
                    <tr>
                        <th style="text-align:center;">&nbsp;</th>
                        <th style="width:80px; text-align: center;">ID</th>
                        <th style="width:90px; text-align:center;">Referência</th>
                        <th style="width:50px; text-align:center;">Endereço</th>
                        <th style="text-align:center;">Tipo documento</th>
                        <th style="text-align:center;">Primeira entrada</th>
                        <th style="">Quantidade documentos</th>
                        <th style="">Quantidade alterada</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($enderecos as $end)
                    @if ($end->endcancelado == 1)
                    <tr class="error">
                        @else
                    <tr>
                        @endif
                        <td style="text-align: center;">
                            @if ($end->endcancelado == 1)
                            <i class="aweso-remove"></i>
                            @else
                            <i class="aweso-ok"></i>
                            @endif
                        </td>
                        <td style="text-align: center;">{{substr('000000'.$end->id_caixa, -6)}}</td>
                        <td style="text-align: center;">{{substr('000000'.$end->id_caixapadrao, -6)}}</td>
                        <td style="text-align: center;">{{$end->id_galpao.".".substr('00'.$end->id_rua, -2).".".substr('00'.$end->id_predio, -2).".".substr('00'.$end->id_andar, -2).".".substr('0000'.$end->id_unidade, -4)}}</td>
                        <td style="text-align:center;">{{$end->descricao_tipo }}</td>
                        <td style="text-align:center;">{{$end->primeira_entrada}}</td>
                        <td>{{$end->qtdDocs}}</td>
                        <td>{{$end->qtdAlterada}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</article> <!-- /content page -->
<style>
    .table{
        font-size:11px
    }
</style>

@stop

@section('scripts')

<script>

@if ($acao == 'print')
    window.open('{{URL::to('/etiqueta/gere/'.$reserva->id_reserva)}}', 'etiqueta');
    window.open('{{URL::to('/reserva/relatorio/'.$reserva->id_reserva)}}', 'etiqueta');
@endif
</script>

@stop