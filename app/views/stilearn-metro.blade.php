<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MEMODOC</title>
        <meta charset="utf-8" />
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Sistema de controle de documentos - MEMODOC" />
        <meta name="author" content="M2 Software" />

        <!-- styles -->
        <link href="/stilearn-metro/css/bootstrap.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/bootstrap-responsive.css" rel="stylesheet" />
        <!-- default theme -->
        <link href="/stilearn-metro/css/metro-bootstrap.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/metro.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/metro-responsive.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/metro-helper.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/font-awesome.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/bootstrap-rowlink.min.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/morrisjs/morris.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/m-scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" />

        <!-- other -->
        <link href="/stilearn-metro/css/select2/select2-metro.css" rel="stylesheet" />
        <link href="/vitalets-bootstrap-datepicker-c7af15b/css/datepicker.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/timepicker/bootstrap-timepicker.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/datetimepicker/datetimepicker.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/daterangepicker/daterangepicker.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/icomoon.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/pnotify/jquery.pnotify.default.css" rel="stylesheet" />
        <link href="/css/table-fixed-header.css" rel="stylesheet" />

        @yield('links')

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="/stilearn-metro/js/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/stilearn-metro/ico/apple-touch-icon-144-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/stilearn-metro/ico/apple-touch-icon-114-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/stilearn-metro/ico/apple-touch-icon-72-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" href="/stilearn-metro/ico/apple-touch-icon-57-precomposed.png" />
        <link rel="shortcut icon" href="/stilearn-metro/ico/favicon.png" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style>

            .form-horizontal .control-group{
                margin-bottom:5px !important;
            }
            .select2-container .select2-choice{
                font-size:14px !important;
            }
            .form-horizontal .control-group .controls input[type=text]
            ,.form-horizontal .control-group .controls input[type=password]
            {
                margin-bottom: 0;
            }

            tr.status1 td:first-child{
                border-left: solid 3px red;
            }
            tr.status2 td:first-child{
                border-left: solid 3px yellow;
            }
            tr.status3 td:first-child{
                border-left: solid 3px green;
            }
            tr.status4 td:first-child{
                border-left: solid 3px blue;
            }
            tr.status5 td:first-child{
                border-left: solid 3px gray;
            }
            .form-horizontal .control-group{
                margin-bottom:5px !important;
            }
            .select2-container .select2-choice{
                font-size:14px !important;
            }
            .form-horizontal .control-group .controls input[type=text]{
                margin-bottom: 0;

            }
            ul.form{
                list-style: none outside none;
            }
            ul.form li.subtitle{
                clear:both;
                width:100%;

            }
            ul.form li input[type="text"],ul.form li textarea{
                width:95%;
            }
            ul.form li textarea{
                height:60px;
            }
            ul.form li{
                width: 47%;
                float: left;
                font-size: 1.1em;
                margin-right: 3%;
                padding: 5px 0;
                position: relative;
            }
            .tabs-right > .nav-tabs .active > a, .tabs-right > .nav-tabs .active > a:hover, .tabs-right > .nav-tabs .active > a:focus{
                border-color: #efefef #efefef #efefef transparent;
                border: none;
                border-bottom:1px solid #efefef;
                border-top:1px solid #efefef
            }
            .tabs-right > .nav-tabs {
                border:1px solid #efefef;
                border-top:0px solid #efefef;
            }
            .tabs-right > .nav-tabs > li >a:hover, .tabs-right>.navtabs > li > a:focus{
                border:none;
            }
            .tabs-right > .nav-tabs > li >a:hover, .tabs-right>.navtabs > li > a{
                border:2px solid transparent;
            }
            .listbox{
                margin:20px;
            }
            body{
                background: #F3F3F3;
            }
            .pagination{
                margin: 5px 0px !important;
            }

            .select2-container .select2-choice span {
                font-size: 14px !important;
            }

            div.menu ul li a{
                font-size: 11px !important;
            }

            .header > .navbar .brand {
                width: auto;
            }
            ::selection {
                background-color: #666 !important;
                color: #FFF !important;
            }
            ::-moz-selection {
                background-color: #666 !important;
                color: #FFF !important;
            }
            span.diferente{color:#666; font-style: italic;}

            .table-menor{
                font-size:11px;
            }

            @yield('styles')
        </style>
    </head>
    <body>
        <!-- start header-->
        <header class="header ">
            <!-- start navbar, this navbar on top -->
            <div id="navbar-top" class="navbar navbar-cyan "> <!-- fixed: navbar-fixed-top -->
                <!-- navbar inner-->

        @if (Auth::check())
            @if (Auth::user()->id_nivel >= 10)
                <div class="navbar-inner bg-black">
            @else
                <div class="navbar-inner">
            @endif
        @endif
                    <!-- container-->
                    <div class="container">
                        <!-- Your brand here, images or text -->
                        <a class="brand" href="/">
                            <img src='/img/logo.png' style='width:120px' />
                        </a>

                        @if (Auth::check())
                            @if (Auth::user()->id_nivel >= 10)
                                @include('padrao/menu')
                            @else
                                @include('padrao/menu_cliente')
                            @endif
                        @endif
                    </div><!--/container-->
                </div><!--/navbar-inner-->

            </div> <!--/ navbar-->
            @if (Auth::user()->id_nivel < 10 && Auth::user()->id_cliente > 0)
            <div id="navbar-top" class="navbar navbar-cobalt"> <!-- fixed: navbar-fixed-top -->
                <div class="navbar-inner bg-steel color-silver" style="height: auto !important; min-height: 28px;">
                    <div class="container" style="padding-top:3px;font-size:11px;vertical-align: middle;">
                        Você está na empresa: "{{Auth::user()->cliente->razaosocial}}"
                        @if ((Auth::user()->adm == 1 || Auth::user()->qtdClientes() > 0) && Auth::user()->id_cliente > 0)
                        <a class="btn btn-mini" href="/publico/usuario/escolherempresa" style="margin:0;">Alterar</a>
                        @endif
                    </div><!--/container-->
                </div><!--/navbar-inner-->
            </div> <!--/ navbar-->
            @endif

        </header> <!--/ end header-->

        <!-- start section content-->
        <section class="section-content">

            <!-- start content -->
            <div class="content_">


                <!-- write your app here -->
                @yield('conteudo')



            </div><!--/ end content -->
        </section> <!-- /end section content-->


        <!-- footer, I place the footer on here. -->
        <footer class="footer">
            <p>Copyright &copy; Memodoc 2016. Todos os direitos reservados.</p>
        </footer><!--/ footer -->




        <!-- javascript
        ================================================== -->
        <!-- required js -->
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/jquery-ui.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/jquery.ui.touch-punch.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/bootstrap.min.js')}}"></script>

        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/holder/holder.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/metro-base.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/bootstrap-rowlink.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/js/jquery.tokeninput.js')}}"></script>

        <!-- apps component js, optional -->
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/select2/select2.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/bootstrap-fileupload/bootstrap-fileupload.js')}}"></script>
        <!--<script type="text/javascript" src="{{URL::to('/stilearn-metro/js/inputmask/jquery.inputmask.bundle.min.js')}}"></script>-->
        <script type="text/javascript" src="{{URL::to('/js/jquery.inputmask.bundle.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/js/quagga.min.js')}}"></script>

        <script type="text/javascript" src="{{URL::to('/vitalets-bootstrap-datepicker-c7af15b/js/bootstrap-datepicker.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/vitalets-bootstrap-datepicker-c7af15b/js/locales/bootstrap-datepicker.pt-BR.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/timepicker/bootstrap-timepicker.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/daterangepicker/moment.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/daterangepicker/daterangepicker.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/colorpicker/bootstrap-colorpicker.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/pnotify/jquery.pnotify.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/bootbox/bootbox.min.js')}}"></script>


        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/jquery.tablesorter.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/m-scrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/morrisjs/raphael-min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/morrisjs/morris.min.js')}}"></script>
        <!-- metro js, required! -->
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/metro-base.js')}}"></script>

        <!-- demo js -->
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/demo/form-elements.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/js/table-fixed-header.js')}}"></script>
        <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>

        <script>


            $(document).ready(function(){
                $('#submit-pesquisa').click(function(event){
                    event.preventDefault();
                    $(this).html('<i class="aweso-spinner aweso-spin"></i> Aguarde...');
                    $(this).attr('disabled', 'disabled');
                    $(this).parents('form:first').submit();
                });
            });

            $(document).ready(function(){
                $('.submit-spinner').click(function(event){
                    event.preventDefault();
                    $(this).html('<i class="aweso-spinner aweso-spin"></i> Aguarde...');
                    $(this).attr('disabled', 'disabled');
                    $(this).parents('form:first').submit();
                });
            });


            $(document).ready(function() {

                $('table[data-sorter="true"]').tablesorter();

                $('[data-fx="select2"]').each(function() {
                    var e = $(this), t = e.attr("data-min-input") == undefined ? 0 : parseInt(e.attr("data-min-input"));
                    e.select2({minimumInputLength: t})
                });
                $('[data-fx="masked"]').inputmask();
                $('[data-fx="datepicker"]').datepicker({format: 'dd/mm/yyyy', language: 'pt-BR', autoclose: true});

                // ========= Start Globals ============
                //
                //
                // Habilita a ABA de acordo com a ancora.
                var url = document.location.toString();
                if (url.match('#')) {
                    $anchor = '#' + url.split('#')[1];
                    $.each($('.nav-tabs a'), function(i, val) {
                        $item = $('.nav-tabs a:eq(' + i + ')');
                        $data_anchor = $item.attr('data-anchor');
                        if ($anchor === $data_anchor) {
                            $item.tab('show');
                        }
                    });
                } else {
                    $('.nav-tabs a:first').tab('show');
                }
                // ajaxForm: Abre formulário ajax dentro do .box
                $('.ajaxForm').on('click', function() {
                    $content = $(this).closest('.box');
                    $.get($(this).attr('href'), function(data) {
                        $content.html(data);
                    })
                    return false;
                });
                //
                // =========== End Globals =========



                $('#pdf').click(function() {

                    $form = $('#frmdocumento').clone().attr('id', 'newid').appendTo('#invisivel');
                    $form.attr('target', 'frame');
                    $form.attr('method', 'post');
                    $form.attr('action', $(this).attr('href'));
                    $form.submit();

                    return false;
                });

                // make the header fixed on scroll
                 $('.table-fixed-header').fixedHeader();
            });



        </script>

        @section('scripts')

        @show
    </body>
</html>
