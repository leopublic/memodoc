@extends('base')

@section('estilos')
    @include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Reservas realizadas</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-tags"></i>Reservas</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body">
                <div class="row">
                    <!-- Inicio primeira coluna-->                        
                    <div class="col-md-12">
                        <table class="table table-hover  table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th style='width:100px;text-align: center;'>Data</th>
                                    <th style='width:40px;text-align: center;'>Reserva n&ordm;</th>
                                    <th style='width:50px;text-align: center;'>Qtd</th>
                                    <th style='width:100px;text-align: center;'>Tipo documento</th>
                                    <th style='width:120px;text-align: center;'>Referência inicial</th>
                                    <th style='width:120px;text-align: center;'>Referência final</th>
                                    <th style='width:auto;'>Planilha em papel</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($reservas as $r)
                                <tr>
                                    <td style="text-align: center;">{{$r->formatDate('data_reserva')}}</td>
                                    <td style="text-align: center;">{{$r->id_reserva}}</td>
                                    <td style="text-align: center;">{{$r->caixas}}</td>
                                    <td style="text-align: center;">{{$r->tipodocumento->descricao}}</td>
                                    <td style="text-align: center;">{{$r->primeiraCaixa()}}</td>
                                    <td style="text-align: center;">{{$r->ultimaCaixa()}}</td>
                                    <td>
                                        @if($r->inventario_impresso)
                                        Sim
                                        @else
                                        Não
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

