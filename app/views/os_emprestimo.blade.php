           @if ($osdesmembrada->status == 1)
               <tr class="success">
           @elseif ($osdesmembrada->status == 2)
               <tr class="error">
           @else
                @if ($osdesmembrada->emcasa == 0)
                    <tr class="info">
                @else
                    <tr>
                @endif
           @endif
           <td style="text-align:center;">{{$i}}</td>
               <td style="text-align:center;">
                   @if ($osdesmembrada->status == 1)
                       <a href="#" class="btn btn-success btn-small" disabled="disabled"><i class="aweso-ok"></i></a>
                    @else
                       @include ('padrao.link_exclusao', array('onclick'=> "removeEmprestimo('".$osdesmembrada->id_osdesmembrada."'); return false;", 'msg_confirmacao' => 'Clique para confirmar a exclusão desse empréstimo', 'title'=> 'Clique para excluir o empréstimo', 'class' =>'btn-mini'))
                    @endif
               </td>
               <td style="text-align: center;"><a href="{{URL::to('/caixa/historico/'.$osdesmembrada->id_cliente.'/'.$osdesmembrada->id_caixapadrao)}}" target="_blank" class="btn btn-small" title="Clique para ver o histórico de movimentação dessa referência">{{substr('000000'.$osdesmembrada->id_caixapadrao, -6)}}</a></td>
               <td style="text-align: center;">
                  {{substr('000000'.$osdesmembrada->id_caixa, -6)}}
               </td>
               <td style="text-align: center;">
                  @if($osdesmembrada->id_expurgo > 0)
                      (expurgada)
                  @else
                    {{$osdesmembrada->endereco_completo()}}
                  @endif
               </td>
               <td>
                   @if(isset($msg))
                        {{ $msg }}
                   @else
                        @if ($osdesmembrada->status == 1)
                        retirada
                        @elseif ($osdesmembrada->status == 0 && $osdesmembrada->emcasa == 0)
                        aguardando checkout (ATENÇÃO: CAIXA CONSTA COMO EMPRESTADA NO SISTEMA!!! VERIFIQUE.)
                        @else
                        aguardando checkout
                        @endif
                   @endif
               </td>
               <td style="text-align: center;">
                   @if ($osdesmembrada->status == 1)
                   {{$osdesmembrada->data_checkout }}
                   @else
                   --
                   @endif
               </td>
               <td>
                   @if ($osdesmembrada->status == 1)
                   {{$osdesmembrada->nomeusuario_checkout }}
                   @else
                   --
                   @endif
               </td>
           </tr>
