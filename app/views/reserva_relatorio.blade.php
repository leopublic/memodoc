<table border='0' cellspacing='0' width="100%" style='border:1px solid #000;font:12px arial;mar'>
     <tr>
         <td width="500px" style="font-size:22px; font-weight: bold; vertical-align: top; padding:0;">
             <img src="../public/img/logo_subtitulo.jpg" style="float:left; vertical-align: text-top; " />
             <br/>Relação de caixas novas enviadas a cliente
             <br />

             <table width='100%' style="margin:0; font-size:14px;">
                 <tr>
                     <th align='right' style='width:120px !important'>
                        <b>Razão Social:</b>
                     </th>
                     <td>
                         {{$reserva->cliente->razaosocial}}
                     </td>
                 </tr>
                 <tr>
                     <th align='right'>
                        <b> <b>Nome fantasia:</b></b>
                     </th>
                     <td>
                         {{$reserva->cliente->nomefantasia}}
                     </td>
                 </tr>
             </table>
         </td>
         <td style="vertical-align: top; width:250px;">
             <table>
                 <tr>
                     <th align='right'><b>Reserva n&ordm;:</b></th>
                     <td> {{substr('000000'.$reserva->id_reserva, -6)}}</td>
                 </tr>
                 <tr>
                     <th align='right'><b>Ordem de serviço: </b></th>
                     <td>
                    @if (is_object($reserva->os))
                        {{$reserva->os->getNumeroFormatado()}}
                    @else
                        --
                    @endif
                     </td>
                 </tr>
                 <tr>
                     <th align='right'><b>Data da reserva:</b></th>
                     <td> {{$reserva->formatdate('data_reserva')}}</td>
                 </tr>
                 <tr>
                     <th align='right'><b>Reimpressão:</b></th>
                     <td> {{date('d/m/Y')}}</td>
                 </tr>
                 <tr>
                     <th align='right'><b>Pagina:</b></th>
                     <td> {{$pag}}/{{$qtdPag}}</td>
                 </tr>
             </table>
         </td>
     </tr>
 </table>
@if(isset($documentos))
<div style='font-family:arial;border:1px solid #000;border-top:0px; width:100%;padding:10px; '>
    <? $i = 0; ?>
    <div>
    @foreach($documentos as $doc)
        <? $ind++; ?>
        @if ($i == 0)
            <div style="float:left ; width:116px; text-align: center;">
        @endif
        @if ($i == 33)
            </div>
            <div style="float:left ;width:116px; text-align: center;">
        <? $i = 0; ?>
        @endif
        <div style="width:116px;border: solid 1px black;font-size: 16px; text-align: center;">{{$ind}}: <strong>{{substr('000000'.$doc->id_caixapadrao, -6)}}</strong></div>
        <? $i++; ?>
    @endforeach
    @if ($i > 0 && $i < 33)
        @while ($i < 33)
            <div style="width:116px;border: none;font-size: 13px; text-align: center;">&nbsp;</div>
            <? $i++; ?>
        @endwhile
        </div>
    @endif
    </div>

</div>
<table style='font-family:arial;padding:10px; border: solid 1px black;width:100%;'>
    <tr><td colspan="2"><strong>Total de caixas : {{$qtd}}</strong></td></tr>
    <tr>
        <td style="text-align:center;">__________________________________________________</td>
        <td style="text-align:center;">____/_____/_______</td>
    </tr>
    <tr>
        <td style="text-align:center;">Cliente</td>
        <td style="text-align:center;">Data</td>
    </tr>
</table>
@endif
