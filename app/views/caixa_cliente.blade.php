@extends('layout')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Localizar caixas do cliente</h1></div>

        <!-- content breadcrumb -->
        <ul class="breadcrumb breadcrumb-inline clearfix">
            <li><a href="#">Caixa</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
            <li class="active">Clientes</li>
        </ul>
        
    </header> <!--/ content header -->
    
    <!-- content page -->
    <article class="content-page clearfix">
        
        <!-- main page -->
        <div class="main-page">
            <div class="content-inner">

            
           <table class='table table-hover' data-provides="rowlink">
               <thead>
                     <th>Razão social</th>
                     <th>Nome fantasia</th>
                  
                  <th style="width:80px;text-align:center">#</th>
               </thead>
               <tbody>
                   @foreach($clientes as $data)
                      <tr>

                          <td> {{$data['razaosocial']}}</td>
                          <td> {{$data['nomefantasia']}}</td>
                          <td> 
                              <a title='Editar' href="/caixa/localizar/{{$data->getKey()}}" class="btn btn-mini"><i class="icon-white icon-arrow-right"></i></a>
                          </td>
                      </tr>
                   @endforeach
               </tbody>
           </table>

                
            </div>
        </div>
    </article> <!-- /content page -->
@stop