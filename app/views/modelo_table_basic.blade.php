@extends('stilearn-metro')

@section('conteudo') 
        
        <!-- start section content-->
        <section class="section-content">
            <!-- side left, its part to menu on left-->
            <div id="navside" class="side-left" data-collapse="navbar">
                <form class="form-inline search-module" action="?" method="post" />
                    <div class="input-append input-append-inline">
                        <input name="search" class="input-block-level" type="text" placeholder="Type to search" />
                        <button class="btn bg-cyan" type="button">
                            <i class="aweso-search"></i>
                        </button>
                    </div>
                </form>
                <!--nav, this structure create with nav (find the bootstrap doc about .nav list) -->
                <ul class="nav nav-list">
                    <li class="dropdown-list"> <!-- example use with dropdown list -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Dashboard</a>
                        <ul class="dropdown-menu">
                            <li><a href="index.html">Dashboard #1</a></li>
                            <li><a href="index2.html">Dashboard #2</a></li>
                            <li><a href="index3.html">Dashboard #3</a></li>
                            <li><a href="index4.html">Dashboard #4</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-list">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Layouts</a>
                        <ul class="dropdown-menu">
                            <li><a href="layout-blank.html">Blank</a></li>
                            <li><a href="layout-full-width.html">Full Width</a></li>
                            <li><a href="layout-top-menus.html">Top Menus</a></li>
                        </ul>
                    </li>
                    <li><a href="widgets.html">Widgets</a></li>
                    <li class="dropdown-list">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Components</a>
                        <ul class="dropdown-menu">
                            <li><a href="ui-general.html">General</a></li>
                            <li><a href="ui-typography.html">Typography</a></li>
                            <li><a href="ui-buttons.html">Buttons</a></li>
                            <li><a href="ui-alert-notify.html">Alert & Notify</a></li>
                            <li><a href="ui-gauge.html">Gauge</a></li>
                            <li><a href="ui-calendar.html">Calendar</a></li>
                            <li><a href="ui-scrollbar.html">Scrollbar</a></li>
                            <li><a href="ui-tabs-collapse.html">Tabs & Collapses</a></li>
                            <li><a href="ui-sliders-bars.html">Sliders & Bars</a></li>
                            <li><a href="ui-tiles.html">Tiles</a></li>
                            <li><a href="ui-appbar.html">Appbar</a></li>
                            <li><a href="ui-splash-page.html">Splash Page</a></li>
                            <li><a href="ui-media-object.html">Media Object</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-list">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Form</a>
                        <ul class="dropdown-menu">
                            <li><a href="form-widget.html">Form Widget</a></li>
                            <li><a href="form-elements.html">Elements</a></li>
                            <li><a href="form-validation.html">Validation</a></li>
                            <li><a href="form-wizard.html">Wizard</a></li>
                            <li><a href="form-wysiwyg.html">Wysiwyg</a></li>
                            <li><a href="form-code-editor.html">Code editor</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-list">
                        <a href="charts.html" class="dropdown-toggle" data-toggle="dropdown-list">Charts</a>
                        <ul class="dropdown-menu">
                            <li><a href="chart-line.html">Lines Charts</a></li>
                            <li><a href="chart-bar.html">Bar Charts</a></li>
                            <li><a href="chart-pie.html">Pie Charts</a></li>
                            <li><a href="chart-others.html">Other Charts</a></li>
                        </ul>
                    </li>
                    <li><a href="grids.html">Grids</a></li>
                    <li class="dropdown-list active">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Tables</a>
                        <ul class="dropdown-menu">
                            <li class="active"><a href="table-basic.html">Basic</a></li>
                            <li><a href="table-datatables.html">DataTables</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="icons.html"><span class="label">3</span> Icons</a>
                    </li>
                    <li class="dropdown-list">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Pages</a>
                        <ul class="dropdown-menu">
                            <li><a href="page-login.html">Login</a></li>
                            <li class="divider"></li>
                            <li><a href="page-search.html">Search</a></li>
                            <li><a href="page-invoices.html">Invoices</a></li>
                            <li><a href="page-inbox.html">Inbox</a></li>
                            <li><a href="page-gallery.html">Gallery</a></li>
                            <li class="divider"></li>
                            <li><a href="page-error-404.html">Error 404</a></li>
                            <li><a href="page-error-500.html">Error 500</a></li>
                            <li><a href="page-coming-soon.html">Coming Soon</a></li>
                        </ul>
                    </li>
                </ul><!-- /nav -->
            </div><!-- /side left-->
            
            
            
            <!-- start content -->
            <div class="content">
                <!-- content header -->
                <header class="content-header">
                    <!-- content action (optional)-->
                    <ul class="content-action pull-right">
                        <li>
                            <div class="btn-group">
                                <a class="btn btn-link dropdown-toggle" data-toggle="dropdown" href="#">
                                    Actions <i class="aweso-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu black">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Some Action</a></li>
                                    <li><a href="#">Other Action</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Something else</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="btn-group" data-toggle="buttons-radio">
                                <button class="btn btn-link" data-content="widget"><i class="aweso-list-ul aweso-large" title="details view"></i></button>
                                <button class="btn btn-link" data-content="tile"><i class="aweso-th aweso-large" title="tile view"></i></button>
                            </div>
                        </li>
                        <li><button class="btn btn-link" data-toggle="button" data-content="toggle-pane"><i class="aweso-exchange aweso-large" title="toggle pane"></i></button></li>
                    </ul> <!-- /content action -->

                    <!-- content title-->
                    <div class="page-header"><h1>Basic Tables</h1></div>

                    <!-- content breadcrumb -->
                    <ul class="breadcrumb breadcrumb-inline">
                        <li><a href="#">Home</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
                        <li class="active">Basic tables</li>
                    </ul>
                </header> <!--/ content header -->
                

                <!-- content page -->
                <article class="content-page">
                    <!-- main page, you're application here -->
                    <div class="main-page">
                        <div class="content-inner">
                            <!-- row #1 -->
                            <div id="row1" class="row-fluid">
                                <!-- span6 -->
                                <div class="span6">
                                    <!-- widget tdefault -->
                                    <div class="widget border-cyan" id="widget-tdefault">
                                        <!-- widget header -->
                                        <div class="widget-header bg-cyan">
                                            <!-- widget icon -->
                                            <div class="widget-icon"><i class="aweso-table"></i></div>
                                            <!-- widget title -->
                                            <h4 class="widget-title">Default styles</h4>
                                            <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                                            <div class="widget-action color-cyan">
                                                <button data-toggle="collapse" data-collapse="#widget-tdefault" class="btn">
                                                    <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                                                </button>
                                            </div>
                                        </div><!-- /widget header -->

                                        <!-- widget content -->
                                        <div class="widget-content">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div><!-- /widget content -->
                                    </div> <!-- /widget tdefault -->
                                </div><!-- span6 -->

                                <!-- span6 -->
                                <div class="span6">
                                    <!-- widget tstriped -->
                                    <div class="widget bg-cyan" id="widget-tstriped">
                                        <!-- widget header -->
                                        <div class="widget-header">
                                            <!-- widget icon -->
                                            <div class="widget-icon"><i class="aweso-table"></i></div>
                                            <!-- widget title -->
                                            <h4 class="widget-title">Table Striped</h4>
                                            <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                                            <div class="widget-action">
                                                <button data-toggle="collapse" data-collapse="#widget-tstriped" class="btn">
                                                    <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                                                </button>
                                            </div>
                                        </div><!-- /widget header -->

                                        <!-- widget content -->
                                        <div class="widget-content">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div><!-- /widget content -->
                                    </div> <!-- /widget tstriped -->
                                </div><!-- span6 -->
                            </div> <!-- /row #1 -->


                            <!-- row #2 -->
                            <div class="row-fluid">
                                <!-- span6 -->
                                <div class="span6">
                                    <!-- widget tbordered -->
                                    <div class="widget border-steel" id="widget-tbordered">
                                        <!-- widget header -->
                                        <div class="widget-header bg-steel">
                                            <!-- widget icon -->
                                            <div class="widget-icon"><i class="aweso-table"></i></div>
                                            <!-- widget title -->
                                            <h4 class="widget-title">Table Bordered</h4>
                                            <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                                            <div class="widget-action color-steel">
                                                <button data-toggle="collapse" data-collapse="#widget-tbordered" class="btn">
                                                    <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                                                </button>
                                            </div>
                                        </div><!-- /widget header -->

                                        <!-- widget content -->
                                        <div class="widget-content">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div><!-- /widget content -->
                                    </div> <!-- /widget tbordered -->
                                </div><!-- span6 -->

                                <!-- span6 -->
                                <div class="span6">
                                    <!-- widget thover -->
                                    <div class="widget bg-steel" id="widget-thover">
                                        <!-- widget header -->
                                        <div class="widget-header">
                                            <!-- widget icon -->
                                            <div class="widget-icon"><i class="aweso-table"></i></div>
                                            <!-- widget title -->
                                            <h4 class="widget-title">Table Hover</h4>
                                            <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                                            <div class="widget-action">
                                                <button data-toggle="collapse" data-collapse="#widget-thover" class="btn">
                                                    <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                                                </button>
                                            </div>
                                        </div><!-- /widget header -->

                                        <!-- widget content -->
                                        <div class="widget-content">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div><!-- /widget content -->
                                    </div> <!-- /widget thover -->
                                </div><!-- span6 -->
                            </div> <!-- /row #2 -->


                            <!-- row #3 -->
                            <div class="row-fluid">
                                <!-- span6 -->
                                <div class="span6">
                                    <!-- widget trowclass -->
                                    <div class="widget border-mauve" id="widget-trowclass">
                                        <!-- widget header -->
                                        <div class="widget-header bg-mauve">
                                            <!-- widget icon -->
                                            <div class="widget-icon"><i class="aweso-table"></i></div>
                                            <!-- widget title -->
                                            <h4 class="widget-title">Optional row classes</h4>
                                            <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                                            <div class="widget-action color-mauve">
                                                <button data-toggle="collapse" data-collapse="#widget-trowclass" class="btn">
                                                    <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                                                </button>
                                            </div>
                                        </div><!-- /widget header -->

                                        <!-- widget content -->
                                        <div class="widget-content">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>First Name</th>
                                                        <th>Last Name</th>
                                                        <th>Username</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="success">
                                                        <td>1</td>
                                                        <td>Mark</td>
                                                        <td>Otto</td>
                                                        <td>@mdo</td>
                                                    </tr>
                                                    <tr class="warning">
                                                        <td>2</td>
                                                        <td>Jacob</td>
                                                        <td>Thornton</td>
                                                        <td>@fat</td>
                                                    </tr>
                                                    <tr class="error">
                                                        <td>3</td>
                                                        <td>Larry</td>
                                                        <td>the Bird</td>
                                                        <td>@twitter</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div><!-- /widget content -->
                                    </div> <!-- /widget trowclass -->
                                </div><!-- span6 -->

                                <!-- span6 -->
                                <div class="span6">
                                    <!-- widget tfull -->
                                    <div class="widget bg-mauve" id="widget-tfull">
                                        <!-- widget header -->
                                        <div class="widget-header">
                                            <!-- widget icon -->
                                            <div class="widget-icon"><i class="aweso-table"></i></div>
                                            <!-- widget title -->
                                            <h4 class="widget-title">Table Hover & full widget</h4>
                                            <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                                            <div class="widget-action">
                                                <button data-toggle="collapse" data-collapse="#widget-tfull" class="btn">
                                                    <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                                                </button>
                                            </div>
                                        </div><!-- /widget header -->

                                        <!-- widget content -->
                                        <div class="widget-content">
                                            <div class="full-widget">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>First Name</th>
                                                            <th>Last Name</th>
                                                            <th>Username</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Mark</td>
                                                            <td>Otto</td>
                                                            <td>@mdo</td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Jacob</td>
                                                            <td>Thornton</td>
                                                            <td>@fat</td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Larry</td>
                                                            <td>the Bird</td>
                                                            <td>@twitter</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- /widget content -->
                                    </div> <!-- /widget tfull -->
                                </div><!-- span6 -->
                            </div> <!-- /row #3 -->

                            <!-- /out of widget demo -->

                        </div><!-- /content-inner-->
                    </div><!-- /main-page-->

                </article> <!-- /content page -->
                
                
            </div> <!--/ end content -->
        </section> <!-- /end section content-->
        
 @stop