<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
<script>
  function subst() {
      var vars = {};
      var query_strings_from_url = document.location.search.substring(1).split('&');
      for (var query_string in query_strings_from_url) {
          if (query_strings_from_url.hasOwnProperty(query_string)) {
              var temp_var = query_strings_from_url[query_string].split('=', 2);
              vars[temp_var[0]] = decodeURI(temp_var[1]);
          }
      }
      var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate', 'time', 'title', 'doctitle', 'sitepage', 'sitepages'];
      for (var css_class in css_selector_classes) {
          if (css_selector_classes.hasOwnProperty(css_class)) {
              var element = document.getElementsByClassName(css_selector_classes[css_class]);
              for (var j = 0; j < element.length; ++j) {
                  element[j].textContent = vars[css_selector_classes[css_class]];
              }
          }
      }
  }
  </script>
</head>
<body  onload="subst()">
    <table border='0' cellspacing='0' style='border:1px solid #000;font:12px arial; width:100%;'>
         <tr>
             <td width="80%" style="font-size:20px; font-weight: bold; vertical-align: top; padding:0;">
                 <img src='{{public_path()}}/img/logo_subtitulo.jpg' style="vertical-align: text-top; " />
                 RELAÇÃO DE CAIXAS PARA EXPURGO

                 <table width='100%' style="margin:0; font-size:14px;">
                     <tr>
                         <th align='right' style='width:120px !important'>
                            <b>Razão Social:</b>
                         </th>
                         <td>
                             {{$exp->cliente->razaosocial}}
                         </td>
                     </tr>
                     <tr>
                         <th align='right'>
                            <b> <b>Nome fantasia:</b></b>
                         </th>
                         <td>
                             {{$exp->cliente->nomefantasia}}
                         </td>
                     </tr>
                     <tr>
                         <th align='right'>
                           <b>Telefones:</b>
                         </th>
                         <td><? $sep = ''; ?>
                               @foreach($exp->cliente->telefonecliente as $tel)
                                    {{$sep.$tel->numero}}
                                    <? $sep = ' / '; ?>
                               @endforeach
                         </td>
                     </tr>
                 </table>
             </td>
             <td style="vertical-align: top; text-align: right;">
                <b>Data: {{$data}}</b>
                <br/><b>Expurgo: {{ substr('000000'.$exp->id_expurgo_pai, -6) }}</b>
                @if($os->id_os > 0)
                     <br/><b>Ordem de serviço: {{ substr('000000'.$os->id_os, -6) }}</b>
                @endif
                <br/><b>Página <span class="page"></span> / <span class="topage"></span> </b>
             </td>
         </tr>
     </table>
</body>
</html>