<?php
$largura = "146px";
$altura = "415px";
$fontsize_titulo = "86px";
$fontsize_texto = "11px";
$fontsize_endereco = "25px";
$largura_endereco = "300px";
$fontsize_id = "11px";
$style_div_interno = "width: 500px; height: 100px;rotate: 90;"
?>
@if($break)
<pagebreak />
@endif
<table style="margin:0px; padding:0px;width:100%;">
	<tr>
		@if(isset($etiqueta['esq']))
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }};"><barcode code="{{ $etiqueta['esq']['id'] }}" type="C39" class="barcode" size="2" height="2.5"/></td>
		@else
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }};">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		@endif
		@if(isset($etiqueta['dir']))
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }}"><barcode code="{{ $etiqueta['dir']['id'] }}" type="C39" class="barcode"  size="2" height="2.5"/></td>
		@else
			<td style="padding:0px; margin:0px;text-align: center; width:{{ $largura }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		@endif
	</tr>
</table>
<div style="">
	<div style="position: absolute; top: 100px; left:0px ; border: solid 1px blue;{{ $style_div_interno }}">
		{{ $etiqueta['dir']['numero'] }}
	</div>
	<div style="position: absolute; top: 100px; left:300px ; border: solid 1px red; {{ $style_div_interno }}">
		{{ $etiqueta['esq']['numero'] }}
	</div>
</div>
