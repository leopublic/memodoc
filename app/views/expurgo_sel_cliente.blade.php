@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Criar expurgo</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-archive"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Informe o cliente</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    @if (!$readonly)
                        {{Form::open(array('class' => 'form-horizontal'))}}
                        <div class='control-group'>
                            {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                            <div class="controls">
                                {{Form::select('id_cliente', $clientes, Input::old('id_cliente') , array('id'=>'id_cliente','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                            </div>
                        </div>
                        <div class='control-group'>
                            {{Form::label('cortesia','Cortesia?',array('class'=>'control-label'))}}
                            <div class="controls">
                                {{Form::checkbox('cortesia', 1)}}
                            </div>
                        </div>
                        <div class='control-group'>
                            {{Form::label('fl_bloqueia_liberados','Bloqueia endereços liberados?',array('class'=>'control-label'))}}
                            <div class="controls">
                                {{Form::checkbox('fl_bloqueia_liberados', 1)}}
                            </div>
                        </div>
                        <div class="form-actions bg-silver">
                            <input type="submit" name="adicionar" class="btn btn-primary" value="Criar">
                        </div>
                        {{ Form::close() }}
                    @else
                    <div class="row-fluid">
                        <div class="span12 bg-orange" style="padding:10px;margin-bottom:10px;">Existe um faturamento fechado na data de hoje, e novos expurgos poderão afetar o faturamento. Entre em contato com a coordenação.</div>
                    </div>
                    @endif
                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop
