@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Alterar senha</h1>
            
        </div>
       

        <!-- content breadcrumb -->
        <ul class="breadcrumb breadcrumb-inline clearfix">
            <li><a href="#">Usuário</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
            <li class="active">Alterar senha</li>
        </ul>
        
        
    </header> <!--/ content header -->
    
    <!-- content page -->
    <article class="content-page clearfix">
        
        <!-- main page -->
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">
    
            @if (Session::has('msg'))
            <div class="alert alert-info"><h4>Mensagem</h4>{{ Session::get('msg') }}</div>
            @endif

            @if (Session::has('success'))
            <div class="alert alert-success"><h4>Sucesso!</h4>{{ Session::get('success') }}</div>
            @endif

            @if (Session::has('error'))
            <div class="alert alert-error"><h4>Erro!</h4>{{ Session::get('error') }}</div>
            @endif  
               
           

                
            <!-- widget form horizontal -->
            <div class="widget border-cyan span6" id="widget-horizontal">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-user"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Informe a nova senha</h4>
                    <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                    <div class="widget-action color-cyan">
                        <button data-toggle="collapse" data-collapse="#widget-horizontal" class="btn">
                            <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                        </button>
                    </div>
                </div><!-- /widget header -->
            

                
               
                <!-- widget content -->
                <div class="widget-content">
                {{ Form::model($model, array('id'=>'frmusuario')) }}         

                     <div class='control-group'>
                         {{Form::label('password','Senha atual',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::password('password', null, array('class'=>'input-medium required'))}} 
                         {{Form::hidden('id_usuario')}} 
                         {{Form::hidden('id_cliente')}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('password_nova','Nova senha',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::password('password_nova', null, array('class'=>'input-medium required'))}} 
                         </div>
                     </div>
                    <div class='control-group'>
                         {{Form::label('password_nova_rep','Repita',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::password('password_nova_rep', null, array('id'=>'senha','class'=>'input-medium required'))}} 
                         </div>
                    </div>
                        
                    <div class="form-actions bg-silver">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button type="reset" class="btn">Cancelar</button>
                    </div> 
               
                 {{Form::close()}}
                </div> <!-- /widget content -->

            </div>
        </div>
    </article> <!-- /content page -->  
    
@stop

@section('scripts')
<script>

    
  $(function(){   

    $('#frmusuario').submit(function(){
       if($.trim($('#senha').val())!= ''){ 
            if($('#senha').val() !== $('#confirmacao').val()){
                alert('A senha e a confirmação da senha não são iguais, verifique!');
                return false;
            } 
       }
    });

    $('.add-caixa').click(function(){
        dados = {
                id_caixapadrao:$('#id_caixapadrao').val(),
                id_item:$('#id_item').val(),
                id_box:$('#id_box').val(),
                id_cliente:$('#id_cliente').val()
        };
        $(this).html('...');
        $caixa = $('.caixa-content');
        
        $.ajax({
            type: "POST",
            url: '/ordemservico/addcaixa',
            data: dados,
            dataType: 'json',
            success: function(data){
                $('.add-caixa').html('add');
                $('.add-to-chat').hide(200);
                if(data.length === 0){
                    alert('Nenhum registro foi encontrado para este cliente');
                }
                $('#id_caixapadrao').focus();
                $.each(data, function(i, item) {

                     $caixa.append('<tr>'+
                                         '<input type="hidden" name="id_documento['+item.id_documento+']" value="'+item.id_caixapadrao+'" />'+
                                         '<td>'+item.id_caixapadrao+'</td>'+
                                         '<td>'+item.titulo+'</td>'+
                                         '<td>'+item.id_box+'</td>'+
                                         '<td>'+item.id_item+'</td>'+
                                   '</tr>'); 
                           
                           
                })                       
                
            }
        });
        
    });
    
    $('[data-toggle="add-others"]').click(function(){
        $(".add-to-chat").slideToggle("fast","easeOutBack");
    });
   
  });

  
</script>
<style>
    .date{
        width:96px;
    }
    .form-horizontal .control-group{
        margin-bottom:5px !important;
    }
</style>
@stop