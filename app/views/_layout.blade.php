<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>:: MEMODOC ::</title>

	{{ HTML::style("css/720_grid.css", array("media" => "screen and (min-width: 720px)")  ) }}
	{{ HTML::style("css/986_grid.css", array("media" => "screen and (min-width: 986px)")  ) }}
	{{ HTML::style("css/1236_grid.css", array("media" => "screen and (min-width: 1236px)")  ) }}
	{{ HTML::style("css/reset.css") }}
	{{ HTML::style("css/style.css") }}
	{{ HTML::style("css/style_mini.css", array("media" => "screen and (max-width: 720px)")  ) }}
	{{ HTML::style("css/font-awesome/css/font-awesome.css") }}
 
	{{ HTML::style("css/footable-0.1.css") }}
	{{ HTML::style("css/tabs.css") }}
	{{ HTML::style("css/token-input-facebook.css") }}
	{{ HTML::style("css/token-input.css") }}

	{{ HTML::script('js/jquery-1.9.1.js') }}
	{{ HTML::script('http://code.jquery.com/jquery-migrate-1.2.1.js') }}
	{{ HTML::script('js/script.js') }}
	{{ HTML::script('js/noty/jquery.noty.js') }}
	{{ HTML::script('js/jquery.floatheader.min.js') }}
	{{ HTML::script('js/jquery.tokeninput.js') }}
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
	<script>
		$(document).ready(function(){
		    $('table.listagem').find('> tbody > tr:nth-child(even)').addClass('even');
		    $('table.listagem').find('> tbody > tr > td.iso:even').addClass('red');
		});

		$(document).ready(function(){
			$.noty.closeAll();
			@if (Session::has('msg'))
				var msg = '{{ Session::get('msg') }}';
				var timeout = 2000;
				if (msg != ''){
					var xx= noty({text: msg, timeout: timeout, layout:'topCenter' });
				}
			@endif	
		});
		
		$(document).ready(function(){
			$('.listagem').floatHeader();					// Ativa o cabecalho fixo
                       
                        // Habilita a ABA de acordo com a ancora.
                        var url = document.location.toString();
                       
                        if (url.match('#')) {
                            $anchor = '#'+url.split('#')[1];
                            $.each($('.nav-tabs a'), function(i, val) {
                                $item = $('.nav-tabs a').eq(i);
                                $data_anchor = $item.attr('data-anchor');
                                if ($anchor == $data_anchor){
                                    $item.tab('show');
                                }
                               });
                           
                            // Change hash for page-reload
                            $('.nav-tabs a').on('shown', function (e) {
                                window.location.hash = e.target.hash;
                            });
                        }else{
                            $('.nav-tabs a:first').tab('show');
                        }
               });
		
	</script>
</head>
<body>
	<div id="barra" style="clear:both;padding:5px; ">
		@if (Auth::check())
		
		<div class="xmenu">
			<i class="icon-archive" style="font-size: 22px;color:#ff9900;"></i>
			<ul>
				<li>Cadastro
					<ul>
						<li><a href="{{ URL::to('cliente') }}">Clientes</a></li>
						<li><a href="{{ URL::to('nivel') }}">Níveis de acesso</a></li>
						<li><a href="{{ URL::to('tipodocumento') }}">Tipos de documento</a></li>
						<li><a href="{{ URL::to('tipoemail') }}">Tipos de email</a></li>
						<li><a href="{{ URL::to('tipoproduto') }}">Tipos de produto</a></li>
						<li><a href="{{ URL::to('tipotelefone') }}">Tipos de telefone</a></li>
						<li><a href="{{ URL::to('usuario') }}">Usuários</a></li>
						<li><a href="{{ URL::to('centrocusto') }}">Centro de custos</a></li>
					</ul>
				</li>
				<li>Atendimento
					<ul>
						<li class="inativo"><a href="#">Inventário</a></li>
						<li><a href="{{ URL::to('documento/cliente') }}">Localizar documentos</a></li>
						<li>Ordens de serviço
							<ul>
								<li class="inativo"><a href="{{ URL::to('ordemservico/realizar') }}">Realizar ordem de serviço</a></li>
								<li class="inativo"><a href="{{ URL::to('ordemservico/documentos') }}">Consultar movimentação de documento por ordem de serviço</a></li>
								<li class="inativo"><a href="{{ URL::to('ordemservico/listar') }}">Consultar ordens de serviço realizadas</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li>Operacional
					<ul>
						<li class="inativo"><a href="#">Check-in</a></li>
						<li class="inativo"><a href="#">Check-out</a></li>
					</ul>
				</li>
				<li>Financeiro
					<ul>
						<li class="inativo"><a href="#">Demonstrativo de movimentação</a></li>
						<li class="inativo"><a href="#">Impressão de NF</a></li>
						<li class="inativo"><a href="#">Impressão NF avulsa</a></li>
						<li class="inativo"><a href="#">Etiqueta envelope cobrança</a></li>
						<li class="inativo"><a href="#">Relação de clientes para reajuste contratual</a></li>
					</ul>
				</li>
				<li>Gerenciamento de galpão
					<ul>
						<li class="inativo"><a href="#">Definição de endereços</a></li>
						<li class="inativo"><a href="#">Alteração de particularidades de endereços</a></li>
						<li class="inativo"><a href="#">Troca de endereços</a></li>
						<li class="inativo"><a href="#">Total de vagas disponíveis no galpão</a></li>
						<li class="inativo"><a href="#">Total de buracos</a></li>
					</ul>
				</li>
				<li>Importação
<!--					<ul>
						<li><a href="{{ URL::to('isometrico') }}">Controle de IsomÃ©tricos</a></li>
					</ul>-->
				</li>
				<li>Selecionar cliente</li>
				<li>Sistema</li>
				<li><a href="{{ URL::to('auth/logout') }}">Sair</a></li>
			</ul>
		</div>
		@endif
	</div>
	<div id="conteudo" style="margin-bottom:100px;">
		@yield('conteudo')
        </div>

	<div style="position: fixed; bottom: 0; font-size:10px; color: #666; margin: none; padding:10px; padding-bottom: 0px; width: 100%;background-color: #fff;">
		<div style="float:right"><img src="../img/logo.png" style="height: 20px;margin: none;margin-right: 10px;"/></div>
	&copy; 2008-2013, MEMODOC. Todos os direitos reservados.
	</div>
        <script src="/js/bootstrap.min.js"></script>
</body>
</html>
