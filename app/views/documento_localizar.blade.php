@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Documentos do cliente</h1>
            
        </div>

        <!-- content breadcrumb -->
        <ul class="breadcrumb breadcrumb-inline clearfix">
            <li><a href="#">Documentos</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
            <li class="active">Pesquisa</li>
        </ul>
        
        <div class='content-action pull-right'>
            Cliente: {{$cliente['nomefantasia']}} <br />
            <a href="/documento/cliente" class="btn btn-small pull-right">Alterar cliente</a>
        </div>
        
    </header> <!--/ content header -->
    
    <!-- content page -->
    <article class="content-page clearfix">
        
        <!-- main page -->
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">
    
        <!-- search box -->
        <div class="search-box">
            <!--<form class="form-vertical" method='post' id="form-search"> -->
            {{Form::model($model,array('id'=>'frmdocumento'))}}
                <h4>{{$cliente['nomefantasia']}}</h4>
                <div class="control-group">
                    <div class="controls">
                       {{Form::text('titulo', null ,array('placeholder'=>"Pesquisar no título do documento...", 'class'=>'input-block-level'))}} 
                       {{Form::text('conteudo', null ,array('placeholder'=>"Pesquisar no conteúdo do documento...", 'class'=>'input-block-level'))}} 
                    </div>
                </div>
                    
            <div class="controls">
                
                <div class="controls pull-left">
                    <label>Intervalo cronológico</label>
                    <div class="input-append input-append-inline ">
                        {{Form::text('inicio', null ,array('placeholder'=>"Data início", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                        <span class="add-on"><i class="icomo-calendar"></i> </span>
                    </div>
                    <div class="input-append input-append-inline ">
                        {{Form::text('fim', null ,array('placeholder'=>"Data Final", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}} 
                        <span class="add-on"><i class="icomo-calendar"></i> </span>
                    </div>
                </div>
                
                <div class="controls pull-left">
                    <label>Intervalo numérico</label>
                    <div class="input-append input-append-inline">
                        {{Form::text('nume_inicial', null ,array('placeholder'=>"De", 'class'=>'number'))}} 
                    </div>
                    <div class="input-append input-append-inline">
                        {{Form::text('nume_final', null ,array('placeholder'=>"Até", 'class'=>'number'))}}
                    </div>
                </div>
                
                <div class="controls pull-left">
                    <label>Intervalo alfabético</label>
                    <div class="input-append input-append-inline">
                        {{Form::text('alfa_inicial', null ,array('placeholder'=>"De", 'class'=>'alfa'))}}
                    </div>
                    <div class="input-append input-append-inline">
                        {{Form::text('alfa_inicial', null ,array('placeholder'=>"Até", 'class'=>'alfa'))}}
                    </div>
                    
                </div>
                
                <div class="controls pull-left">
                    <label>Número da caixa</label>
                    <div class="input-append input-append-inline">
                        {{Form::text('id_caixa', null ,array('placeholder'=>"Caixa", 'class'=>'alfa'))}}
                    </div>
                </div>
                

                
                    <button class="btn bg-cyan pull-right" type="submit">
                                <i class="aweso-search"></i> Buscar
                    </button>
                    
                    <div class='clearfix'></div>

                            <div class="controls pull-left">
                                <label>Setores</label>
                                {{Form::select('id_setor', $setores, null, array('data-fx'=>'select2'))}}
 
                           </div>

                            <div class="controls pull-left">
                                <label>Departamentos</label>
                                 {{Form::select('id_departamento', $departamentos, null, array('data-fx'=>'select2'))}}    
                            </div>

                            <div class="controls pull-left">
                                <label>Centro de custos</label>
                                 {{Form::select('id_centro', $centrocustos, null, array('data-fx'=>'select2'))}} 
                             </div>
                            <div class="controls pull-left">

                                <div class="checkbox-rounded help-block">
                                    <input class="input-fx" type="radio" value="1" id="radio-rounded1" name="reservado" checked="true" />
                                    <label for="radio-rounded1">Caixas em consultas</label>
                                </div>
                                <div class="checkbox-rounded help-block">
                                    <input class="input-fx" type="radio" value="2" id="radio-rounded2" name="reservado" />
                                    <label for="radio-rounded2">Caixas reservadas</label>
                                </div>
                            </div> 
                        </div>

                    </form>
                    <div class='clearfix'></div>
                </div><!-- /search box -->
                
                @if(isset($documentos)) 
                
                <div class='pull-right'><a href="/documento/pdf/{{$cliente['id_cliente']}}" class='btn' id='pdf'><i class='icon-download'></i> PDF</a></div>
                <h5 class="pull-left">{{$documentos->count()}} documentos encontrados </h5>
                <div id='invisivel'>
                    <iframe name='frame' id='frame' src=''></iframe>
                </div>
                <div class="clearfix"></div>
                <table class='table table-striped table-hover table-bordered table-condensed'>
                    <thead>
                        <tr>
                            <th style="width:80px">Data inicio</th>
                            <th style="width:80px">Data final</th>
                            <th style="width:80px">Num caixa</th>
                            <th style="width:90px">Caixa padrão</th>
                            <th style="width:50px">Item</th>
                            <th style="width:350px">Título</th>
                            <th style="width:auto">Conteúdo</th>
                            <th>Setor</th>
                            <th>Departamento</th>
                            <th>Centro</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($documentos as $doc)
                            <tr>
                                <td>{{$doc->inicio}}</td>
                                <td>{{$doc->fim}}</td>
                                <td>{{$doc->id_caixa}}</td>
                                <td>{{$doc->id_caixapadrao}}</td>
                                <td>{{$doc->id_item}}</td>
                                <td>{{$doc->titulo}}</td>
                                <td>{{$doc->conteudo}}</td>
                                <td>{{isset($doc->setcliente->nome) ? $doc->setcliente->nome : '' }}</td>
                                <td>{{isset($doc->depcliente->nome) ? $doc->depcliente->nome : '' }}</td>
                                <td>{{isset($doc->centrodecusto->nome) ? $doc->centrodecusto->nome : '' }}</td>
                                
                            </tr>
                            @endforeach
                    </tbody>
                </table>
                @endif    



    

    

    <style>
        .boxsetores,.boxdepartamentos,.boxcentrodecustos{
            float:left;
            width:300px; 
            margin-top: 15px;
        }
        .token-input-list-facebook{
            width:100% !important;
        }
        .token-input-list-facebook input{
            clear:both;
            
        }
        
        li.token-input-token-facebook p {
            font:9px arial;
            color:#5670A6;
        }
        .select2-search-choice{
            font-size:10px !important
        }
        .select2-container-multi .select2-choices{
            max-height:100px;
            overflow-y: scroll;
           
        }
        .controls{
            margin-right: 20px;
        }

        .date,.number,.alfa{
            width:90px
        }
        .table{
            font-size:11px
        }
        #invisivel{
            display:none
        }
    </style>
            
            
            </div>
        </div>
    </article> <!-- /content page -->           
@stop