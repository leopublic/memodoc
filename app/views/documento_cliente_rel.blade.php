@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>{{ $tituloRelatorio}}</h1>
            
        </div>

        <!-- content breadcrumb -->
        <ul class="breadcrumb breadcrumb-inline clearfix">
            <li><a href="#">Documentos</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
            <li class="active">Pesquisa</li>
        </ul>
        
        <div class='content-action pull-right'>
            Cliente: <span class='nomecliente'>{{!empty($cliente) ? $cliente->razaosocial : 'Nenhum cliente selecionado'}}</span> <br />
           
        </div>
        
    </header> <!--/ content header -->
    
    <!-- content page -->
    <article class="content-page clearfix">
        
        <!-- main page -->
        <div class="main-page" id="">
            <div class="content-inner">
                
                @if(isset($documentos)) 
                <div class="widget border-cyan">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-reorder"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">{{$documentos->count()}} documentos encontrados </h4>
                        <div class="widget-action">
                            <a href="/documento/pdf/{{$cliente->id_cliente}}" class='btn' id='pdf'><i class='icon-download'></i> PDF</a>
                        </div>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content bg-silver">
                    <table class='table table-striped table-hover table-bordered table-condensed'>
                        <thead>
                            <tr>
                                <th style="width:60px;text-align:center;">Ref</th>
                                <th style="width:80px">Período</th>
                                <th style="width:50px; text-align: center;">Item</th>
                                <th style="width:350px">Título</th>
                                <th style="width:auto">Conteúdo</th>
                                <th>Setor</th>
                                <th>Departamento</th>
                                <th>Centro</th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach($documentos as $doc)
                                <tr>
                                    <td style="text-align: center;">{{ substr("000000".$doc->id_caixapadrao, -6) }}</td>
                                    <td>
                                        @if ($doc->inicio != '')
                                            de {{$doc->formatDate('inicio')}}
                                        @endif
                                        @if ($doc->fim != '')
                                            <br/>até {{$doc->formatDate('fim')}}
                                        @endif
                                    </td>
                                    <td style="text-align: center;">{{ substr("000".$doc->id_item, -3)}}</td>
                                    <td>{{$doc->titulo}}</td>
                                    <td>{{$doc->conteudo}}</td>
                                    <td>{{isset($doc->setcliente->nome) ? $doc->setcliente->nome : '' }}</td>
                                    <td>{{isset($doc->depcliente->nome) ? $doc->depcliente->nome : '' }}</td>
                                    <td>{{isset($doc->centrodecusto->nome) ? $doc->centrodecusto->nome : '' }}</td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                    
                </div>
                @endif    
            </div>
        </div>
    </article> <!-- /content page -->           
@stop

@section('scripts')
<script>
  $(function(){ 
    $departamento = $('#id_departamento');  
    $setor = $('#id_setor');  
    $centrocusto = $('#id_centro'); 
    $cliente.change(function(){
        
       $('.load').show(); 
       $('#titulo').focus();
       $('.documentos').hide();
       
       // limpando combos
       $departamento.html('');
       $setor.html('');
       $centrocusto.html('');
       
       // ajax departamento
       $.get('publico/documento/ajaxdepartamento/'+$(this).val(),function(combo){
           $departamento.html(combo);
       });
       // ajax centro custo
       $.get('publico/documento/ajaxcentrocusto/'+$(this).val(),function(combo){
           $centrocusto.html(combo);
       });
       // ajax setor
       $.get('publico/documento/ajaxsetor/'+$(this).val(),function(combo){
           $setor.html(combo);
       });
       
       // Remove o load após 2.5 segundos
       setInterval(function(){
           $('.load').hide(); 
       },2500);
       
    }).focus();
  });
</script>
@stop