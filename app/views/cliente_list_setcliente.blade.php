@extends('stilearn-metro')

@section('conteudo')

    @include('cliente_tab_header')

    <!-- #tab_setores - Begin Content  -->
    <div class="tab-pane" id="tab_SetCliente">
        <?php
        $hasmany = array('title' => 'Setores',
            'fields' => array(
                'nome' => 'Nome do setor',
                'qtd_documentos' => 'Qtd documentos',
            )
        );
        ?>
        @include('padrao/hasmany_list', $hasmany)
    </div>
    <!-- #tab_setores - End Content  -->   

    @include('cliente_tab_footer')
@stop
