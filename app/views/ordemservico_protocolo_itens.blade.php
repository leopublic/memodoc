<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <style>
        td{vertical-align: top}
        th{vertical-align: top; font-weight: bold; text-align:right;}
        table.itens tr td{border-bottom:solid 1px black; font-weight:normal;}
    </style>
</head>
<body>
        <div style="font-family:arial;width:100%;padding:10px;">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="font-size:10px;font-family: arial;" class="itens">
                <? $qtd = 1; ?>
                <?php $id_caixapadrao_ant = ''; ?>
    @if(count($itens) > 0)
        @foreach($itens as $item)
            @if($item->id_caixapadrao != $id_caixapadrao_ant)
                <tr>
                    <td colspan=5 style="font-weight: bold; font-size:14px;padding-top:15px;">Caixa {{substr('000000'.$item->id_caixapadrao, -6)}}</td>
                </tr>
                <tr>
                    <td width="20px;" style="text-align:center;">#</td>
                    <td width="20px;" style="text-align:center;"><strong>Item</strong></td>
                    <td width="40px;" style="text-align:center;"><strong>Prontuário</strong></td>
                    <td width="150px;"><strong>Convênio</strong></td>
                    <td width="auto;"><strong>Paciente</strong></td>
                </tr>
                <?php $id_caixapadrao_ant = $item->id_caixapadrao; ?>
            @endif
                <tr>
                    <td style="text-align:center;">{{$qtd}}</td>
                    <td style="text-align:center;">{{$item->id_item}}</td>
                    <td style="text-align:center;">{{$item->codigo}}</strong></td>
                    <td>{{$item->campo1}}</td>
                    <td>{{$item->campo2}}</td>
                </tr>
                <? $qtd++; ?>
        @endforeach
    @endif
            </table>
            <br />
            Total de itens relacionados: {{count($itens)}}
            <br />
            <br />

        </div>
        <div style="font-family:arial; width:100%;padding:10px;text-align:center;">
            <table style="font-family: arial;">
                <tr>
                    <td style="text-align:center;font-size:14px;"><br/>____________________________________<br/>Cliente</td>
                    <td style="width:50px;">&nbsp;</td>
                    <td style="text-align:center;font-size:14px;"><br/>____________________________________<br/>Ajudante MEMODOC</td>
                    <td style="width:50px;">&nbsp;</td>
                    <td style="text-align:center;font-size:14px;"><br/>____________________________<br/>Data</td>
                </tr>
            </table>
       </div>
    </body>
</html>