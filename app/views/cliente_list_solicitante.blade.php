@extends('stilearn-metro')

@section('conteudo') 

        @include('cliente_tab_header')
        
        <!-- #tab_solicitantes - Begin Content  -->
         <div class="tab-pane" id="tab_Solicitante">
            <?php
            
               $hasmany = array('title' => 'Solicitantes',
                                'fields' => array(
                                                    'nome' => 'Nome',
                                                    'consulta' => 'Consulta',
                                                 )          
                                ); 
            ?>
            @include('padrao/hasmany_list', $hasmany)
         </div>
        <!-- #tab_solicitantes - End Content  -->   
        
        @include('cliente_tab_footer')
@stop
