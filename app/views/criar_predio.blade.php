@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Criar prédio</h1>
            
        </div>
    </header> <!--/ content header -->
    
    @include('padrao.mensagens')
    
    <article class="content-page clearfix">
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">
        <div class="widget border-cyan" id="widget-horizontal">
            <!-- widget header -->
            <div class="widget-header bg-cyan">
                <div class="widget-icon"><i class="aweso-edit"></i></div>
                <h4 class="widget-title">Informe os dados do prédio</h4>
            </div><!-- /widget header -->
                            
            <!-- widget content -->
            <div class="widget-content">
                {{ Form::open(array('class'=>'form-horizontal')) }}
                 <div class='span5 pull-left '>   
                     <div class='control-group'>
                         {{Form::label('id_galpao','Galpão',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_galpao', Input::old('id_galpao'), array('placeholder'=>'Número do galpão', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_rua','Rua',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_rua',  Input::old('id_rua'), array('placeholder'=>'Número da rua', 'class'=>'input-xlarge','required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_predio','Número do prédio',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_predio',  Input::old('id_predio'), array('placeholder'=>'Número do prédio', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('andar_ini','Andar inicial',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('andar_ini',  Input::old('andar_ini'), array('placeholder'=>'(padrão 1)', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('andar_fim','Andar final',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('andar_fim',  Input::old('andar_fim'), array('placeholder'=>'', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('qtd_unidades','Qtde de apartamentos',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('qtd_unidades',  Input::old('qtd_unidades'), array('placeholder'=>'(padrão 30)', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_tipodocumento','Tipo de documento',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::select('id_tipodocumento', $tipodocumento)}} 
                         </div>
                     </div>
                     
                 </div>
                   <div class='clearfix'></div>
                   <div class="form-actions bg-silver">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button type="reset" class="btn">Cancelar</button>
                   </div>
                </div><!-- /widget content -->
            </div> <!-- /widget form horizontal -->
                 {{Form::close()}}
            </div>
        </div>
    </article> <!-- /content page -->           
@stop