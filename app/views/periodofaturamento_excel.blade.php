@if ($conferencia != '')
    <? $colspan = '19'; ?>
@else
    <? $colspan = '20'; ?>
@endif
<html>
    <body>
        <table>
            <tr>
                <th colspan="{{$colspan}}" style="font-weight: bold; font-size:200%; text-align:left;">
                    Faturamento {{$perfat->mes}}/{{$perfat->ano}}: de {{$perfat->dt_inicio_fmt}} a {{$perfat->dt_fim_fmt}} - Relatório Sintético
                </th>
            </tr>
            <tr>
                <th colspan="{{$colspan}}" style="font-weight: bold; font-size:100%; text-align:left;">
                    {{$titulo_tipo}}
                </th>
            </tr>
            <tr>
                <th colspan="{{$colspan}}" style="font-weight: normal; text-align:left;">Calculado em: <span style="font-weight: bold;">{{$perfat->formatDate('dt_recalculo', 'd/m/Y H:i:s')}}</span> por:
                <span style="font-weight: bold;">
                @if (is_object($perfat->calculadopor))
                {{$perfat->calculadopor->nomeusuario}}
                @else
                --
                @endif
                </span>
                </th>
            </tr>
            <tr>
                <th colspan="{{$colspan}}" style="font-weight: bold; font-size:200%; text-align:left;">
                    &nbsp;
                </th>
            </tr>
            <tr>
                @if($conferencia != '')
                <th style="width:50px; border:solid 1px #000;" rowspan="2">Conf.</th>
                @endif
                <th style="width:auto; border:solid 1px #000;" rowspan="2">Cliente</th>
                <th style="width:auto; text-align: center;border:solid 1px #000;" colspan="7">Custódia</th>
                <th style="width:auto; text-align: center; border:solid 1px #000;" colspan="6">Atendimento</th>
                <th style="width:auto; text-align: center; border:solid 1px #000;" colspan="5">Totais</th>
            </tr>
            <tr>
                <th style="text-align: right;border:solid 1px #000;">Qtd. Cx. mês ant.</th>
                <th style="text-align: right;border:solid 1px #000;">1&ordf; entr. Cx nova</th>
                <th style="text-align: right;border:solid 1px #000;">Qtd. expurgos</th>
                <th style="text-align: right;border:solid 1px #000;">Qtd. expurgos em uso</th>
                <th style="text-align: right;border:solid 1px #000;">Qtd. Total</th>
                <th style="text-align: right;border:solid 1px #000;">Carênc.</th>
                <th style="text-align: right;border:solid 1px #000;">Contrato Mínimo</th>
                <th style="text-align: right;border:solid 1px #000;">Qtd cobrada</th>
                <th style="text-align: right;border:solid 1px #000;">Valor total custódia</th>

                <th style="text-align: right;border:solid 1px #000;">Cartona. qtd</th>
                <th style="text-align: right;border:solid 1px #000;">Cartona. val</th>
                <th style="text-align: right;border:solid 1px #000;">Frete qtd caixas</th>
                <th style="text-align: right;border:solid 1px #000;">Valor frete</th>
                <th style="text-align: right;border:solid 1px #000;">Movim. Qtd cxs</th>
                <th style="text-align: right;border:solid 1px #000;">Movim. valor</th>

                <th style="text-align: right;border:solid 1px #000;">Base<br>ISS</th>
                <th style="text-align: right;border:solid 1px #000;">Alíquota</th>
                <th style="text-align: right;border:solid 1px #000;">ISS</th>
                <th style="text-align: right;border:solid 1px #000;">Dif.<br>mín.</th>
                <th style="text-align: right;border:solid 1px #000;">Faturado</th>
            </tr>
            @foreach($registros as $cliente)
            <tr>
                @if($conferencia != '')
                <td style="width:auto; border:solid 1px #000;">&nbsp;</td>
                @endif
                <td style="border:solid 1px #000;">{{ $cliente->razaosocial }} <?php chr(10); ?><span class="diferente" style="font-size:10px;">{{ $cliente->nomefantasia }} - {{ substr('000'.$cliente->id_cliente, -3) }}</span></td></td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_custodia_mes_anterior, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_custodia_novas, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_expurgos, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_expurgos_usadas, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_custodia_periodo, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_custodia_em_carencia, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->cliente->valorminimofaturamento, 2, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->qtd_custodia_periodo + $cliente->qtd_custodia_em_carencia, 0, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->val_custodia_periodo_cobrado, 2, ",", ".")}}
                </td>

                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->qtd_cartonagem_enviadas, 0, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->val_cartonagem_enviadas, 2, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->qtd_frete_dem, 0, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->val_frete + $cliente->val_frete_urgente, 2, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->qtd_movimentacao + $cliente->qtd_movimentacao_urgente, 0, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->val_movimentacao + $cliente->val_movimentacao_urgente, 2, ",", ".")}}
                </td>

                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->val_servicos_sem_iss , 2, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">
                    @if ($cliente->cliente->issporfora)
                    *0,065
                    @else
                    /0,95
                    @endif
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->val_iss, 2, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->val_dif_minimo, 2, ",", ".") }}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->val_servicos_sem_iss + $cliente->val_iss + $cliente->val_cartonagem_enviadas, 2, ",", ".")}}</td>
            </tr>
            @endforeach
        </table>
    </body>
</html>
