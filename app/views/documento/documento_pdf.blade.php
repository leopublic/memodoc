<html>
    <head>
        <style>
            table tr td{vertical-align: top;}
            h4{font-weight:bold;font-size:14px;}
        </style>

    </head>
    <body>
        @if(isset($documentos))
        <table class='table table-striped table-hover table-bordered table-condensed'>
            <thead>
                <tr>
                    <th style="width:60px;text-align:center;">Ref</th>
                    <th style="width:80px">Período</th>
                    <th style="width:50px; text-align: center;">Item</th>
                    <th style="width:350px">Título</th>
                    <th style="width:auto">Conteúdo</th>
                    <th>Setor</th>
                    <th>Departamento</th>
                    <th>Centro</th>
                </tr>
            </thead>
            <tbody>
                @foreach($documentos as $doc)
                <tr>
                    <td>{{$doc->id_item}}</td>
                    <td>{{$doc->titulo}}</td>
                    <td>{{nl2br($doc->conteudo)}}</td>
                    <td>{{$doc->formatDate('inicio')}} a {{$doc->formatDate('fim')}}</td>
                    <td>{{$doc->expurgarem}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endif
    </body>
</html>