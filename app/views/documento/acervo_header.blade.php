<table style="border: solid 1px #000;width:100%">
    <tr>
        <td><img src='/img/logo.png' /></td>
        <td><h2>RELATÓRIO DE CONTEÚDO DE CAIXA</h2></td>
        <td style="text-align: right;">{{date('d/m/Y H:i:s')}}</td>
    </tr>
    <tr>
        <td colspan="3">{{$qtdDocumentos}} documentos encontrados em {{$qtdCaixas}} referências</td>
    </tr>
</table>
