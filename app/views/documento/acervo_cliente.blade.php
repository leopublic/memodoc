@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>{{ $titulo }}</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget border-cyan">
                        <div class="widget-header bg-cyan">
                            <div class="widget-icon"><i class="aweso-archive"></i></div>
                            <h4 class="widget-title">Selecione o cliente</h4>
                        </div>
                        <div class="widget-content bg-silver">
                        {{Form::open(array('class' => 'form-horizontal'))}}
                            <div class='control-group'>
                                {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::select('id_cliente', $clientes, Input::old('id_cliente') , array('id'=>'id_cliente','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                                </div>
                            </div>
                            <div class='control-group'>
                                {{Form::label('ordenacao','Ordenar por',array('class'=>'control-label'))}}
                                <div class="controls">
                                    {{Form::select('ordenacao', array("default"=>'Centro de custo, Departamento, Referência', "c.nome" => "Centro de custo, Referência", "dep.nome" => "Departamento, Referência"), Input::old('id_cliente') , array('id'=>'ordenacao','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                                </div>
                            </div>
                            <div class="form-actions bg-silver">
                                <button id="btnGerar" class="btn btn-primary">Gerar</button>
                            </div>
                        {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop
@section('scripts')
<script>
    $(document).ready(function(){
        $('#btnGerar').click(function (event){
            if($('#id_cliente').val()!= ''){
                event.preventDefault();
                window.open('{{URL::to( $caminho )}}/'+$('#id_cliente').val()+'/'+$('#ordenacao').val());
            }
        });
    });
</script>
@stop