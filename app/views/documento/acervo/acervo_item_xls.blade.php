<tr>
    <td style="text-align:center;">{{$doc->id_caixapadrao}}</td>
    <td style="text-align:center;">{{$doc->id_item_fmt}}</td>
    <td >{{$doc->titulo}}</td>
    <td width="600">{{str_replace("\n", chr(10),$doc->conteudo)}}</td>
    <td style="text-align:center;">{{$doc->inicio_fmt}} a {{$doc->fim_fmt}}</td>
    <td style="text-align:center;">{{$doc->primeira_entrada_fmt}}</td>
    <td style="text-align:left;">
        @if ($doc->id_centro != '')
        {{$doc->nome_centrodecusto}}
        @else
        (não informado)
        @endif    
    </td>
    <td style="text-align:left;">
        @if ($doc->id_departamento != '')
        {{$doc->nome_depcliente}}
        @else
        (não informado)
        @endif
    </td>
    <td style="text-align:left;">
        @if ($doc->id_setor != '')
        {{$doc->nome_setcliente}}
        @else
        (não informado)
        @endif
    </td>
    <td style="text-align:left;">{{$doc->migrada_de}}</td>
    <td style="text-align:left;">{{$doc->request_recall}}</td>
    <td style="text-align:center;">
        @if ($doc->expurgarem_fmt == '' || $doc->expurgarem_fmt == '00/00/0000')
        PERMANENTE
        @else
            {{$doc->expurgarem_fmt}}
        @endif
    </td>
</tr>
