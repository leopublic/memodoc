<tr>
    <td style="text-align:center;">{{$doc->id_item_fmt}}</td>
    <td>{{$doc->titulo}}</td>
    <td>{{nl2br($doc->conteudo)}}</td>
    <td style="text-align:center;">{{$doc->inicio_fmt}} a {{$doc->fim_fmt}}</td>
    <td style="text-align:center;">
    @if ($doc->expurgarem_fmt == '' || $doc->expurgarem_fmt == '00/00/0000')
    PERMANENTE
    @else
        {{$doc->expurgarem_fmt}}
    @endif
    </td>
</tr>
