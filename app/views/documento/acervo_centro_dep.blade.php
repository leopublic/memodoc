<tr class="centro">
    <td colspan="2">Centro de custo:
        @if ($doc->id_centro != '')
        {{$doc->nome_centrodecusto}}
        @else
        (não informado)
        @endif
    </td>
    <td colspan="3">Departamento:
        @if ($doc->id_departamento != '')
        {{$doc->nome_depcliente}}
        @else
        (não informado)
        @endif
    </td>
</tr>