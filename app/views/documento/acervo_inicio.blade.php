<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MEMODOC</title>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Sistema de controle de documentos - MEMODOC" />
        <meta name="author" content="M2 Software" />
        <style>
            *{font-family:arial;}
            body{margin-top:none; margin-bottom:none;}
            table tr td{font-size:10px;vertical-align: top;}
            h4{font-weight:bold;font-size:14px;}
            table.docs tr.cabini td{font-size:11px; border-top:none; vertical-align: top; font-size:14px;}
            table.docs tr.cab td{font-size:11px; vertical-align: top;}
            table.docs tr.cabfim td{font-size:11px; border-bottom:solid 1px #000; border-top:none; vertical-align: top;font-size:14px;}
            table.docs tr td{font-size:10px; border-top:solid 1px #000; vertical-align: top;}
            table.docs tr th{font-size:10px; border-top:solid 2px #000; border-bottom:solid 2px #000; font-weight:bold; background-color: #cccccc; text-align:left;}
            table.docs tr.caixa td{background-color: #cccccc; font-weight:bold;}
            table.docs tr.centro td{background-color: #999999; color:#ffffff; font-weight:bold;}
            table.docs { page-break-inside:auto }
            table.docs tr { page-break-inside:avoid; page-break-after:auto }
        </style>

    </head>
    <body>
        <table class="docs" cellspacing="0" style="width:100%;border-collapse:collapse;">
            <colgroup>
                <col width="50px"/>
                <col width="200px"/>
                <col width="500px"/>
                <col width="auto"/>
                <col width="100px;"/>
            </colgroup>
            <thead>
                <tr class="cabini">
                    <td colspan="2"><img src='/img/logo.png' /></td>
                    <td colspan="2"><span style="font-size:18px; font-weight:bold;">RELATÓRIO DE CONTEÚDO DE CAIXA</span><br/>{{$cliente->razaosocial}} - {{$cliente->nomefantasia}}</td>
                    <td style="text-align: right;" colspan="2">{{date('d/m/Y H:i:s')}}</td>
                </tr>
                @if ($filtros != '')
                <tr>
                    <td colspan="5"  style="border:none; font-size:10px;">{{$filtros}}</td>
                </tr>
                @endif
                <tr class="cabfim">
                    <td colspan="5">{{$qtdDocumentos}} documentos encontrados em {{$qtdCaixas}} referências</td>
                </tr>
                <tr>
                    <th style="text-align: center;">Item</th>
                    <th style="">Título</th>
                    <th style="">Conteúdo</th>
                    <th style="text-align:center;">Período</th>
                    <th style="text-align:center;">Expurgar em</th>
                </tr>
            </thead>
            <tbody>
