<head>
<style>
    br {mso-data-placement:same-cell;}
    td{border-top:solid 1px #000; vertical-align: top; font-size: 10px;}
    tr.centro td{background-color: #999999; color:#ffffff; font-weight:bold;}
    tr.caixa td{background-color: #cccccc; font-weight:bold;}
</style>
</head>
<body>
<table class="docs" cellspacing="0" style="width:100%;border-collapse:collapse;">
            <colgroup>
                <col width="80px"/>
                <col width="auto"/>
                <col width="auto"/>
                <col width="600px"/>
                <col width="120px;"/>
                <col width="auto"/>
                <col width="auto"/>
                <col width="auto"/>
                <col width="auto"/>
                <col width="auto"/>
                <col width="auto"/>
            </colgroup>
            <thead>
                <tr class="cabini">
                    <td colspan="10"><span style="font-size:18px; font-weight:bold;">RELATÓRIO DE CONTEÚDO DE CAIXA</span><br/>{{$cliente->razaosocial}} - ({{$cliente->nomefantasia}})</td>
                    <td style="text-align: right;" colspan="1">{{date('d/m/Y H:i:s')}}</td>
                </tr>
                @if ($filtros != '')
                <tr>
                    <td colspan="11"  style="border:none; font-size:10px;">{{$filtros}}</td>
                </tr>
                @endif
                <tr class="cabfim">
                    <td colspan="11">{{$qtdDocumentos}} documentos encontrados em {{$qtdCaixas}} referências</td>
                </tr>
                <tr>
                    <th style="text-align: center;">Caixa</th>
                    <th style="text-align: center;">Item</th>
                    <th style="">Título</th>
                    <th style="">Conteúdo</th>
                    <th style="text-align:center;">Período</th>
                    <th style="text-align:center;">Primeira entrada</th>
                    <th style="text-align:center;">Centro de custo</th>
                    <th style="text-align:center;">Departamento</th>
                    <th style="text-align:center;">Setor</th>
                    <th style="text-align:center;">Origem 1</th>
                    <th style="text-align:center;">Origem 2</th>
                    <th style="text-align:center;">Expurgar em</th>
                </tr>
            </thead>
            <tbody>
