<html>
    <head>
        <style>
            td{vertical-align: top}
            th{vertical-align: top; font-weight: bold; text-align:right;}
        </style>

    </head>
<body>
@include('ordemservico_cabecalho_rel', array('os' => $os,'brtitulo' =>'<br/>', 'titulo'=> 'CAIXAS PARA CONSULTAS (OPERACIONAL)') )

@if(count($desmembrada) > 0)
<div style="width:100%;padding:10px;">
    <?php //pr($desmembrada); ?>
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="font-size:14px;font-family: arial;">
        <? $qtd = 1; ?>
        @foreach($desmembrada as $r)
        <tr>
            <td width="30px;" style="text-align:right;">{{$qtd;}}:</td>
            <td width="30px;" style="border:solid 1px black;">&nbsp;&nbsp;</td>
            <td width="120px;">Caixa: <strong>{{substr('000000'.$r->id_caixapadrao, -6)}}</strong></td>
            <td>Localização:<strong>{{$r->id_galpao}}.{{substr('00'.$r->id_rua, -2)}}.{{substr('00'.$r->id_predio, -2)}}.{{substr('00'.$r->id_andar, -2)}}.{{substr('0000'.$r->id_unidade, -4)}}</strong>
            </td>
        </tr>
        <? $qtd++; ?>
        @endforeach
    </table>
    <br />
    Total de caixas relacionadas: {{count($desmembrada)}}
    <br />
    <br />
</div>
@else
--
@endif


</body>
</html>
