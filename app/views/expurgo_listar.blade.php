@extends('layout')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Consultar expurgos</h1></div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <!-- search box -->
            <div class="search-box">
                {{Form::open()}}
                <div class='control-group'>
                    {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                    <div class="controls">
                        {{Form::select('id_cliente', $clientes, null , array('id'=>'id_cliente','data-fx'=>'select2','style'=>'width:600px;'))}}
                    </div>
                </div>

                <div class='controls pull-left'>
                    <label> &nbsp;</label>
                    <div class="controls">
                        <button class='btn btn-success' id="submit-pesquisa">Buscar</button>
                    </div>
                </div>
                <div class='clearfix'></div>

                {{Form::close()}}
            </div>
            @if(isset($expurgos))
            {{ $expurgos->links() }}
            <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                <thead>
                    <th style="width:auto; text-align:center;">ID</th>
                    <th style="width:auto; text-align:center;">Referência</th>
                    <th style="width:auto; text-align: center;">Data da solicitação</th>
                    <th style="width:auto; text-align: center;">Data do checkout</th>
                    <th style="width:auto;">&nbsp;</th>
                </thead>
                <tbody>

                    @foreach($expurgos as $expurgo)
                    <tr class="">
                        <td style="text-align: center;">{{substr('000000'.$expurgo->id_caixa, -6)}}</td>
                        <td style="text-align: center;">{{substr('000000'.$expurgo->id_caixapadrao, -6)}}</td>
                        <td style="text-align: center;">{{$expurgo->data_solicitacao}}</td>
                        <td style="text-align: center;">
                            @if ($expurgo->data_checkout != '')
                            {{$expurgo->data_checkout}}
                            @else
                            --
                            @endif
                        </td>
                        <td style="width:auto;">&nbsp;</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

            {{ $expurgos->links() }}
            @endif
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .table tbody{
        font:12px arial;
    }
</style>
@stop