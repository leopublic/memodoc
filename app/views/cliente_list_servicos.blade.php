@extends('stilearn-metro')

@section('conteudo') 

        @include('cliente_tab_header')
        
        <!-- #tab_servicos - Begin Content  -->
         <div class="tab-pane" id="tab_servicos">
             <div class="tab listbox">
                <h3 class="pull-left">Serviços <span class="badge badge-info">{{$servicos->count()}}</span></h3> 
                <a class='pull-right btn btn-info' href='/cliente/servicoseditar/{{$cliente->id_cliente}}#tab_servicos'>Editar serviços</a>
                <div class='clearfix'></div>
                <table class='table table-hover table-striped'>
                    <thead>
                        <tr>
                            <th style="width:auto;">Produto</th>
                            <th style="text-align:right; width: 150px;">Valor normal R$</th>
                            <th style="text-align:right; width: 150px;">Valor urgência R$</th>
                            <th style="text-align:center; width: 100px;">Mínimo</th>
                            <th style="text-align:center; with:auto;">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                           <td>Valor faturamento (mínimo em R$)</td> 
                           <td style="text-align: right;">--</td> 
                           <td style="text-align: right;">--</td> 
                           <td style="text-align: center;">{{number_format($cliente->valorminimofaturamento, 2, ',', '.')}}</td> 
                        </tr>
                        @foreach($servicos as $s)
                        <tr>
                           <td>{{$s->tipoproduto->descricao}}</td> 
                           <td style="text-align:right;">{{number_format($s->valor_contrato,2,',','.')}}</td> 
                           <td style="text-align:right;">{{number_format($s->valor_urgencia,2,',','.')}}</td> 
                           <td  style="text-align:center;">{{$s->minimo}}</td> 
                           <td style="text-align:center;">&nbsp;</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

             </div>
         </div>
        <!-- #tab_servicos - End Content  -->   
        
        @include('cliente_tab_footer')
@stop
