@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Ordem de serviço
            @if ($model->id_status_os == \StatusOs::stINCOMPLETA)
            {{ substr('000000'.$model->getProximoNumero($model->id_cliente), -6) }} (não confirmado)
            @else
            {{ substr('000000'.$model->id_os, -6) }}  - {{ $model->statusos->descricao }}</h1>
            @endif
    </div>

    @if (count($menus)> 0)
        @include ('padrao/acoes_cabecalho')
    @endif

    @if ($model->id_status_os > \StatusOs::stINCOMPLETA)
    <ul class="content-action pull-right">
        <li><a class="btn bg-lime " href="{{URL::to(\Memodoc\Servicos\Rotas::ordemservico().'/historico/'.$model->id_os_chave)}}" target="_blank"><i class="aweso-time"></i> Histórico</a></li>
    </ul>
    @endif

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">

            @include ('padrao/mensagens')

            <div class="row-fluid">
                <div class="span12 bg-silver" style="padding:10px;margin-bottom:10px;">{{$cliente->razaosocial}}<span style="font-size:8px;margin-left:15px;">{{$cliente->nomefantasia}}</span>
                </div>
            </div>
            @if ($readonly)
            <div class="row-fluid">
                <div class="span12 bg-orange" style="padding:10px;margin-bottom:10px;">ATENÇÃO: Essa ordem de serviço já foi faturada! Para alterar essa OS voce precisa abrir o faturamento.</div>
            </div>
            @endif
            @if ($model->fl_importada == 1)
            <div class="row-fluid">
                <div class="span12 bg-orange" style="padding:10px;margin-bottom:10px;">ATENÇÃO: Ordem de serviço importada do antigo E-doc.</div>
            </div>
            @endif
            @if ($cliente->fl_os_bloqueada == 1)
            <div class="row-fluid">
                <div class="span12 bg-red" style="padding:10px;margin-bottom:10px;">ATENÇÃO: Esse cliente está bloqueado para novas OS! Antes de executar esse serviço, consulte a gerência.</div>
            </div>
            @endif
            <!-- widget form horizontal -->
            <div class="row-fluid">
                <div class="widget border-cyan span12" id="widget-horizontal">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-truck"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Dados da ordem de serviço</h4>
                        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        <div class="widget-action">
                            <div class="btn-group">
                            </div>
                        </div>

                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        @if ($model->id_faturamento > 0)
                        <div class="row-fluid">
                            <div class="span12 bg-steel" style="padding:10px;margin-bottom:10px;">Faturada em "{{$model->faturamento->periodofaturamento->dt_inicio_fmt}} a {{$model->faturamento->periodofaturamento->dt_fim_fmt}}"</div>
                        </div>
                        @endif
                        {{ Form::model($model, array('class'=>'form-horizontal', 'id'=>'form_os')) }}
                        {{ Form::hidden('id_os_chave', null) }}
                        {{Form::hidden('fl_frete_manual', 0)}}
                        <div class="row-fluid">
                            <div class="span6">
                                <div class='control-group'>
                                    {{Form::label('responsavel','Solicitado por',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('responsavel', null, array('placeholder'=>'nome da pessoa responsável pela OS', 'class'=>'input-block-level','required', 'disabled'=> $readonly))}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    {{Form::label('solicitado_em','Solicitado em',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('solicitado_em', $model->solicitado_em, array('class'=>'input-block-level', 'disabled'=> 'true'))}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    {{Form::label('entregar_em','Executar em',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        <div class="input-append input-append-inline ">
                                            {{Form::text('entregar_em', null ,array('placeholder'=>"quando a os deve ser atendida", 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker",'required', 'disabled'=> $readonly))}}
                                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                                        </div>
                                        <div class="input-append">
                                            {{Form::select('entrega_pela', array("0"=>"Manhã", "1"=>"Tarde"), null ,array("class"=>"input-small", 'disabled'=> $readonly))}}
                                        </div>
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('id_departamento','Departamento',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('id_departamento', array(), null, array('id'=>'departamento', 'data-fx'=>'select2', 'class' => 'input-block-level', 'disabled'=> $readonly))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('tipodeemprestimo','Tipo de empréstimo',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('tipodeemprestimo', array('0'=> 'Entrega MEMODOC', '1'=> 'Consulta no local', '2'=> 'Retirado pelo cliente'), null , array('id'=>'tipodeemprestimo', 'data-fx'=>'select2', 'class'=>"input-block-level", 'disabled'=> $readonly))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('id_endentrega','Local entrega/retirada',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('id_endeentrega', $endeentrega, null, array( 'id'=>'endereco','data-fx'=>'select2', 'class' => 'input-block-level', 'disabled'=> $readonly))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('urgente', 'Urgente',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::checkbox('urgente', 1,null, array( 'disabled'=> $readonly))}}
                                    </div>
                                </div>

                                <div class='control-group'>
                                    {{Form::label('naoecartonagemcortesia','Cobrar cartonagem?',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::checkbox('naoecartonagemcortesia',1, null, array('disabled'=> $readonly))}}
                                    </div>
                                </div>

                                <div class='control-group'>
                                    {{Form::label('cobrafrete','Cobrar frete?',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::checkbox('cobrafrete',1, null, array('disabled'=> $readonly))}}
                                    </div>
                                </div>
                             @if ($model->id_status_os > 1)
                                <div class='control-group'>
                                    {{Form::label('id_status_os','Situação atual',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('id_status_os', $status_os, null, array('data-fx'=>'select2', 'class' => 'input-block-level', 'disabled'=> $readonly))}}
                                    </div>
                                </div>
                             @endif
                                <div class='control-group'>
                                    {{Form::label('observacao', 'Observações',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::textarea('observacao', null, array('style'=>'height:120px','class'=>'input-block-level', 'disabled'=> $readonly))}}

                                    </div>
                                </div>

                            </div>
                            <div class="span6">
                                <div class="widget border-amber" id="widget-horizontal">
                                    <div class="widget-header bg-amber">
                                        <div class="widget-icon"><i class="aweso-archive"></i></div>
                                        <h4 class="widget-title">Caixas novas</h4>
                                    </div><!-- /widget header -->

                                    <!-- widget content -->
                                    <div class="widget-content">
                                        <div class='row-fluid'>
                                            {{Form::label('qtdenovascaixas','Qtde de novas caixas com endereçamento',array('class'=>'span8', 'style'=>'padding-top:5px;text-align:right;padding-right:10px;'))}}
                                            {{Form::text('qtdenovascaixas', null ,array('class' =>'span4', 'style'=> 'text-align: right;', 'disabled'=> $readonly))}}
                                        </div>
                                        <div class='row-fluid'>
                                            {{Form::label('qtd_cartonagens_cliente','Qtde cartonagem adicional',array('class'=>'span8', 'style'=>'padding-top:5px;text-align:right;padding-right:10px;'))}}
                                            {{Form::text('qtd_cartonagens_cliente', null ,array('class'=>'span4', 'style'=> 'text-align: right;', 'disabled'=> $readonly))}}
                                        </div>
                                        <div class='row-fluid'>
                                            {{Form::label('qtdenovasetiquetas','Qtde de novos endereços sem cartonagem',array('class'=>'span8', 'style'=>'padding-top:5px;text-align:right;padding-right:10px;'))}}
                                            {{Form::text('qtdenovasetiquetas', null ,array('class'=>'span4', 'style'=> 'text-align: right;', 'disabled'=> $readonly))}}
                                        </div>
                                    </div>
                                </div>

                                <div class="widget border-lime" id="widget-horizontal">
                                    <div class="widget-header bg-lime">
                                        <div class="widget-icon"><i class="aweso-archive"></i></div>
                                        <h4 class="widget-title">Caixas movimentadas e frete</h4>
                                    </div><!-- /widget header -->

                                    <!-- widget content -->
                                    <div class="widget-content">
                                        <div class='row-fluid'>
                                            <p class="span8">&nbsp;</p>
                                            <p class="span2">Prev.</p>
                                            <p class="span2">Real</p>
                                        </div>
                                        <div class='row-fluid'>
                                            {{Form::label('qtd_caixas_novas_cliente','Qtde de caixas novas apanhadas',array('class'=>'span8', 'style'=>'padding-top:5px;text-align:right;padding-right:10px;'))}}
                                            {{Form::text('qtd_caixas_novas_cliente', null ,array('class'=>'span2', 'style'=> 'text-align: right;', 'disabled'=> $readonly))}}
                                            {{Form::text('apanhanovascaixasxx', intval($model->apanhanovascaixas) ,array('disabled'=>'disabled', 'class'=>'span2', 'style'=> 'text-align:right;'))}}
                                        </div>
                                        <div class='row-fluid'>
                                            {{Form::label('qtd_caixas_retiradas_cliente','Qtde de caixas devolução',array('class'=>'span8', 'style'=>'padding-top:5px;text-align:right;padding-right:10px;'))}}
                                            {{Form::text('qtd_caixas_retiradas_cliente', null ,array('class'=>'span2', 'style'=> 'text-align: right;', 'disabled'=> $readonly))}}
                                            {{Form::text('apanhacaixasemprestadasxx', intval($model->apanhacaixasemprestadas) ,array('disabled'=>'disabled', 'class'=>'span2', 'style'=> 'text-align:right;'))}}
                                        </div>
                                        <div class='row-fluid'>
                                            {{Form::label('totalenviadas','Qtde caixas enviadas',array('class'=>'span8', 'style'=>'padding-top:5px;text-align:right;padding-right:10px;'))}}
                                            {{Form::text('totalenviadasxx', intval($model->qtdcaixasconsulta()) ,array('disabled'=>'disabled', 'class'=>'span2', 'style'=> 'text-align:right'))}}
                                            {{Form::text('totalenviadas', intval($model->qtdcaixasconsulta()) ,array('disabled'=>'disabled', 'class'=>'span2', 'style'=> 'text-align:right'))}}
                                        </div>
                                        <div class='row-fluid'>
                                            {{Form::label('totalretiradas','Total de caixas movimentadas',array('class'=>'span8', 'style'=>'padding-top:5px;text-align:right;padding-right:10px;'))}}
                                            {{Form::text('totalretirada_prevsxx', intval($model->qtd_caixas_novas_cliente) + intval($model->qtd_caixas_retiradas_cliente) + intval($model->qtdcaixasconsulta()),array('disabled'=>'disabled', 'class'=>'span2', 'style'=> 'text-align:right'))}}
                                            {{Form::text('totalretiradasxx', intval($model->apanhanovascaixas) + intval($model->apanhacaixasemprestadas) + intval($model->qtdcaixasconsulta()) ,array('disabled'=>'disabled', 'class'=>'span2', 'style'=> 'text-align:right'))}}
                                        </div>
                                        <div class='row-fluid'>
                                            {{Form::label('valorfrete','Valor do frete',array('class'=>'span8', 'style'=>'padding-top:5px;text-align:right;padding-right:10px;'))}}
                                            {{Form::text('valorfreteprevistoxx', number_format($model->valorfreteprevisto, 2, ',', '.') ,array('disabled'=>'disabled', 'class'=>'span2', 'style'=> 'text-align:right'))}}
                                            {{Form::text('valorfretexx', number_format($model->valorfrete, 2, ',', '.') ,array('disabled'=>'disabled', 'class'=>'span2', 'style'=> 'text-align:right'))}}
                                        </div>
                                        @if ($model->cliente->fl_frete_manual == 1)
                                        <div class='row-fluid'>
                                            <label for="valorfrete" class="span8" style="padding-top:5px;text-align:right;padding-right:10px;">
                                                {{Form::checkbox('fl_frete_manual', 1, intval($model->fl_frete_manual) ,array( 'style'=> '', 'id'=> 'fl_frete_manual'))}} Frete fixo</label>
                                            {{Form::text('val_frete_manual_fmt', $model->val_frete_manual_fmt ,array('disabled'=>'disabled', 'class'=>'span2', 'style'=> 'text-align:right', 'id'=>'val_frete_manual_fmt'))}}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions bg-silver">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary">Salvar</button>
                            @endif
                        </div>

                        {{Form::close()}}
                    </div>
                </div>
            </div>

                @if($model->qtde_novas_caixas_total > 0)
                <div class="row-fluid">
                    <div class="widget border-amber span12" id="widget-horizontal">
                        <div class="widget-header bg-amber">
                            <div class="widget-icon"><i class="aweso-magic"></i><i class="aweso-archive"></i></div>
                            <h4 class="widget-title">Caixas novas (Reserva)</h4>
                        </div>
                        <!-- widget content -->
                        <div class="widget-content">
                            @if(intval($model->reservas_nao_canceladas) == 0)
                            <a href="/reserva/criar/{{ $model->id_cliente }}/{{$model->qtde_novas_caixas_total}}/{{$model->id_os_chave}}" class="btn btn-warning" target="_blank">Criar reserva</a>
                            @endif
                            <table  data-sorter="true" class='table table-striped table-condensed' id="reservas">
                                <tr>
                                    <th style="text-align:center; width:200px">Ações</th>
                                    <th style="text-align:center;">ID</th>
                                    <th style="text-align:right;">Qtd</th>
                                    <th style="text-align:center;">Tipo</th>
                                    <th style="text-align:center;">Caixa inicial</th>
                                    <th style="text-align:center;">Caixa final</th>
                                </tr>
                                @foreach($model->reservas as $reserva)
                                @if($reserva->cancelada==1)
                                <? $estilo = 'text-decoration: line-through;'; ?>
                                @else
                                <? $estilo = ''; ?>
                                @endif
                                <tr>
                                    <td style="text-align:center;">
                                        @if($reserva->cancelada==1)
                                        <i class="aweso-remove"></i>
                                        @else
                                        <a class="btn silver" href="/etiqueta/gere/{{ $reserva->id_reserva }}" target="_blank" title="Imprimir etiquetas"><i class="aweso-tags"></i></a>
                                        <a class="btn silver" href="/reserva/inventario/{{ $reserva->id_reserva }}" target="_blank" title="Imprimir planilha"><i class="aweso-edit"></i></a>
                                        <a class="btn silver" href="/reserva/relatorio/{{ $reserva->id_reserva }}" target="_blank" title="Imprimir relatório"><i class="aweso-print"></i></a>
                                        @include('padrao.link_exclusao', array('url'=> \Memodoc\Servicos\Rotas::ordemservico().'/cancelarreserva/'. $model->id_os_chave.'/'. $reserva->id_reserva , 'msg_confirmacao'=>'Clique aqui para confirmar o cancelamento dessa reserva'))
                                        @endif
                                    </td>
                                    <td style="text-align:center;{{$estilo}}">{{substr('000000'.$reserva->id_reserva, -6)}}</td>
                                    <td style="text-align:right;{{$estilo}}">{{$reserva->caixas}}</td>
                                    <td style="text-align:center;{{$estilo}}">{{$reserva->tipodocumento->descricao}}</td>
                                    <td style="text-align:center;{{$estilo}}">{{substr('000000'.$reserva->primeiraCaixa(), -6)}}</td>
                                    <td style="text-align:center;{{$estilo}}">{{substr('000000'.$reserva->ultimaCaixa(), -6)}}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                        <!-- /widget content -->
                    </div>
                </div>
                @endif

                <div class="row-fluid">
                    <!-- widget form horizontal -->
                    {{ Form::open(array('class'=>'form-horizontal', 'id' => 'form-checkin', 'action' => array('OrdemServicoController@postPrecheckin', $model->id_os_chave))) }}
                    <div class="widget border-lime" id="widget-horizontal">
                        @include('padrao/widget_header', array('cor' => 'lime',  'titulo' => 'Caixas enviadas para empréstimo', 'icone' => '<i class="aweso-truck"></i><i class="aweso-arrow-left"></i><i class="aweso-archive"></i>'))
                        <div class="widget-content">

                            <div class="control-group">
                                @if (!$readonly)
                                <div class='input-append'>
                                    {{Form::text('id_caixapadrao_emprestimo', null, array('placeholder'=>'de', 'class'=>'input-medium', 'id' => 'id_caixapadrao_emprestimo'))}} a
                                    {{Form::text('id_caixapadrao_emprestimo_fim', null, array('placeholder'=>'até', 'class'=>'input-medium', 'id' => 'id_caixapadrao_emprestimo_fim'))}}
                                    <button onclick="adicionaEmprestimo('{{$model->id_os_chave}}', $('#id_caixapadrao_emprestimo').val(), $('#id_caixapadrao_emprestimo_fim').val()); return false;" name="AdicionarEmprestimo" id="AdicionarEmprestimo" class="btn bg-lime">Adicionar</button>
                                </div>
                                @endif
                            </div>

                            <div id="emprestimos">
                                @if(is_object($model->referenciasConsulta()))
                                @include('os_emprestimos', array('emprestimos'=> $model->referenciasConsulta()))
                                @else
                                --
                                @endif
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>

                <div class="row-fluid">
                    <!-- widget form horizontal -->
                    {{ Form::open(array('class'=>'form-horizontal', 'id' => 'form-checkin', 'action' => array('OrdemServicoController@postPrecheckin', $model->id_os_chave))) }}
                    <div class="widget border-lime" id="widget-horizontal">
                        @include('padrao/widget_header', array('cor'=> 'lime', 'titulo' => 'Caixas retiradas do cliente (de acordo com o protocolo de retirada)', 'icone' => '<i class="aweso-archive"></i><i class="aweso-arrow-right"><i class="aweso-truck aweso-flip-horizontal"></i></i>'))
                        <div class="widget-content">
                        <div class="row-fluid">
                            <div class="span5">
                                    @if (!$readonly)
                                    <div class='input-append'>
                                        {{Form::text('id_caixapadrao_checkin', null, array('placeholder'=>'de', 'class'=>'input-small', 'id' => 'id_caixapadrao_checkin'))}}
                                        {{Form::text('id_caixapadrao_checkin_fim', null, array('placeholder'=>'até', 'class'=>'input-small', 'id' => 'id_caixapadrao_checkin_fim'))}}
                                        <button onclick="adicionaCheckin('{{$model->id_os_chave}}', $('#id_caixapadrao_checkin').val(), $('#id_caixapadrao_checkin_fim').val()); return false;" name="AdicionarCheckin" id="AdicionarCheckin" class="btn bg-lime">Adicionar</button>
                                    </div>
                                    @endif
                            </div>
                            <div class="span7">
                        @if($qtd_itens_retirados > 0)
                            Enviar protocolo para 
                                <div class='input-append'>
                                    {{Form::text('email', null, array('placeholder'=>'email', 'class'=>'input-large', 'id' => 'email'))}}
                                    <a href="javascript:enviarProtocolo('{{$model->id_os_chave}}'); " class="btn bg-lime">Enviar</a>
                                </div>
                        @endif
                            </div>
                        </div>
                            <div id="checkins">
                                @if(is_object($model->checkin))
                                @include('os_checkins', array('checkins'=> $model->checkin))
                                @else
                                --
                                @endif
                            </div>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>

                @if ($model->id_status_os > 1)
                    <!-- Expurgos -->
                    <div class="row-fluid">
                        <!-- widget form horizontal -->
                        <div class="widget border-red" id="widget-horizontal">
                            @include('padrao/widget_header', array('cor'=> 'red', 'titulo' => 'Expurgo', 'icone' => '<i class="aweso-trash"></i>'))
                            <div class="widget-content">

                                <div class="control-group">
                                @if (count($expurgo) > 0)
                                    <a href="/expurgo/caixasgravadas/{{$expurgo->id_expurgo_pai}}" name="AdicionarExpurgo" id="AdicionarExpurgo" class="btn bg-red">Ver expurgo</a>
                                @else
                                    @if (!$readonly)
                                    <div class='input-append'>
                                        <a href="/expurgo/novoos/{{$model->id_os_chave}}" name="AdicionarExpurgo" id="AdicionarExpurgo" class="btn bg-red">Adicionar expurgo à OS</a>
                                    </div>
                                    @endif
                                @endif
                                </div>

                                <div id="expurgos">
                                    @if(count($expurgo) > 0)
                                        <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                                            <thead>
                                                <th style="width:auto; text-align:center;">Id</th>
                                                <th style="width:auto; text-align:center;">Referência</th>
                                                <th style="width:auto; text-align:center;">Endereço</th>
                                                <th style="width:auto; text-align:center;">Checkout</th>
                                                <th style="width:auto; text-align: left;">Observação</th>
                                            </thead>
                                            <tbody>
                                                @foreach($caixas_expurgo as $caixa)
                                                <tr class="">
                                                    <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa, -6)}}</td>
                                                    <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao, -6)}}</td>
                                                    <td style="text-align: center;">{{$caixa->endereco->enderecoCompleto()}}</td>
                                                    <td style="text-align: center;">{{$caixa->data_checkout_fmt}}</td>
                                                    <td style="text-align: left;">{{$caixa->observacao}}</td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    @else
                                    --
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Transbordos -->
                    <div class="row-fluid">
                        <!-- widget form horizontal -->
                        {{ Form::open(array('class'=>'form-horizontal', 'id' => 'form-checkin', 'action' => array('OrdemServicoController@postPrecheckin', $model->id_os_chave))) }}
                        <div class="widget border-taupe" id="widget-horizontal">
                            @include('padrao/widget_header', array('cor'=> 'taupe', 'titulo' => 'Transbordos', 'icone' => '<i class="aweso-archive"></i><i class="aweso-random"><i class="aweso-archive"></i></i>'))
                            <div class="widget-content">

                                <div class="control-group">
                                    @if (count($transbordo) > 0)
                                        <a href="/transbordo/caixas/{{$transbordo->id_transbordo}}" name="Adicionartransbordo" id="Adicionartransbordo" class="btn bg-taupe">Ver transbordo</a>
                                    @else
                                        @if (!$readonly)
                                        <div class='input-append'>
                                            <a href="/transbordo/novoos/{{$model->id_os_chave}}" name="Adicionartransbordo" id="Adicionartransbordo" class="btn bg-taupe">Adicionar transbordo à OS</a>
                                        </div>
                                        @endif
                                    @endif
                                </div>

                                <div>
                                    @if(count($transbordo) > 0)
                                        <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                                            <thead>
                                                <tr>
                                                    <th colspan="3" style="width:auto; text-align:center;">DE</th>
                                                    <th style="width:auto; text-align:center; border-top: none; border-bottom: none;">&nbsp;</th>
                                                    <th colspan="3" style="width:auto; text-align:center;">PARA</th>                                
                                                </tr>
                                                <tr>
                                                    <th style="width:auto; text-align:center;">ID</th>
                                                    <th style="width:auto; text-align:center;">Referência</th>
                                                    <th style="width:auto; text-align:center;">Endereço</th>
                                                    <th style="width:auto; text-align:center; border-top: none; border-bottom: none;">&nbsp;</th>
                                                    <th style="width:auto; text-align:center;">ID</th>
                                                    <th style="width:auto; text-align:center;">Referência</th>
                                                    <th style="width:auto; text-align:center;">Endereço</th>                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($caixas_transbordo as $caixa)
                                                <tr class="">
                                                    <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa_antes, -6)}}</td>
                                                    <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao_antes, -6)}}</td>
                                                    <td style="text-align: center;">{{$caixa->enderecoantes->enderecoCompleto()}}</td>
                                                    <th style="width:auto; text-align:center; border-top: none; border-bottom: none;"><i class="aweso-arrow-right aweso-2x"></i></th>
                                                    <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa_depois, -6)}}</td>
                                                    <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao_depois, -6)}}</td>
                                                    <td style="text-align: center;">{{$caixa->enderecodepois->enderecoCompleto()}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                    --
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                @else
                    <!-- Expurgos -->
                    <div class="row-fluid">
                        <!-- widget form horizontal -->
                        <div class="widget border-steel bg-silver" id="widget-horizontal">
                            @include('padrao/widget_header', array('cor'=> 'steel', 'titulo' => 'Expurgo', 'icone' => '<i class="aweso-trash"></i>'))
                            <div class="widget-content">
                                <div id="expurgos">
                                    Salve a OS para poder adicionar o expurgo.
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Transbordos -->
                    <div class="row-fluid">
                        <!-- widget form horizontal -->
                        <div class="widget border-steel bg-silver" id="widget-horizontal">
                            @include('padrao/widget_header', array('cor'=> 'steel', 'titulo' => 'Transbordos', 'icone' => '<i class="aweso-archive"></i><i class="aweso-random"><i class="aweso-archive"></i></i>'))
                            <div class="widget-content">
                                <div>
                                    Salve a OS para poder adicionar o transbordo.
                                </div>
                            </div>
                        </div>
                    </div>

                @endif
            </div>
        </div>
</article> <!-- /content page -->

@stop

@section('scripts')
<style>
    .date{
        width:96px;
    }
    .form-horizontal .control-group{
        margin-bottom:5px !important;
    }
</style>

<script>

@if ($acao == 'print')
    window.open('{{URL::to(\Memodoc\Servicos\Rotas::ordemservico().'/relatoriocompleto/'.$model->id_os_chave)}}', 'relatoriogeral');
@endif

    $(document).ready(function(){
        if($('#fl_frete_manual')){
            if($('#fl_frete_manual').prop('checked')){
                $('#val_frete_manual_fmt').removeAttr('disabled');
            } else {
                $('#val_frete_manual_fmt').attr('disabled', 'disabled');
                $('#val_frete_manual_fmt').val('');
            }
        }
        $('#fl_frete_manual').change(function() {
            if($(this).is(":checked")) {
                $('#val_frete_manual_fmt').removeAttr('disabled');
            } else{
                $('#val_frete_manual_fmt').attr('disabled', 'disabled');
                $('#val_frete_manual_fmt').val('');
            }
        });

        $('#form_os').submit(function(e) {
            var currentForm = this;
            e.preventDefault();
            var cobrafrete = $('#cobrafrete').prop('checked');
            var qtd = parseInt($('#qtd_caixas_novas_cliente').val()) + parseInt($('#qtd_caixas_retiradas_cliente').val()) ;
            var linhas_emprestimos = $('#tabela-emprestimos > tbody > tr:not(.error)').length;
            var linhas_checkins = $('#tabela-checkins > tbody > tr:not(.error)').length;
            var total = qtd + linhas_emprestimos + linhas_checkins;
            var msg = '';
            var nome = '{{Auth::user()->nomeusuario}}'
            if (cobrafrete && total == 0){
                if (nome != ''){
                    msg = "<b>ATENÇÃO: FRETE SEM MOVIMENTAÇÃO!!</b><br/><span style='text-decoration:underline;'>"+nome+"</span>, essa ordem de serviço está cobrando frete sem nenhuma movimentação de caixas. Será cobrado o frete mínimo.<br/><br/><span style='font-weight:bold; font-style:italic;'>Você quer salvar a ordem de serviço assim mesmo?</span>";
                } else {
                    msg = "<b>ATENÇÃO: FRETE SEM MOVIMENTAÇÃO!!</b><br/>Essa ordem de serviço está cobrando frete sem nenhuma movimentação de caixas. Será cobrado o frete mínimo.<br/><br/><span style='font-weight:bold; font-style:italic;'>Você quer salvar a ordem de serviço assim mesmo?</span>";
                }
            }
            if (!cobrafrete && total > 0){
                if (nome != ''){
                    msg = "<b>ATENÇÃO: MOVIMENTAÇÃO SEM FRETE!!</b><br/><span style='text-decoration:underline;'>"+nome+"</span>, essa ordem de serviço não está cobrando frete mas apresenta movimentação de caixas.<br/><br/><span style='font-weight:bold; font-style:italic;'>Você quer salvar a ordem de serviço assim mesmo?</span>";
                } else {
                    msg = "<b>ATENÇÃO: MOVIMENTAÇÃO SEM FRETE!!</b><br/>Essa ordem de serviço não está cobrando frete mas apresenta movimentação de caixas.<br/><br/><span style='font-weight:bold; font-style:italic;'>Você quer salvar a ordem de serviço assim mesmo?</span>";
                }
            }
            if (msg != ''){
                bootbox.dialog(msg, [{
                    "label" : "Sim, pode gravar assim mesmo",
                    "class" : "btn-success",
                    "callback": function() {
                        currentForm.submit();
                    }
                }, {
                    "label" : "Não! Vou corrigir, obrigado!",
                    "class" : "btn-danger"
                }]);
            } else {
                currentForm.submit();
            }
        });
    });


    function enviarProtocolo(id_os_chave){
        var url = '{{\Memodoc\Servicos\Rotas::ordemservico()}}/enviarprotocolo/'+id_os_chave+'/'+$('#email').val();
        $.ajax({
            type: "GET",
                    url: url,
                    dataType: "json",
                    async:true
            })
        .done(function(data) {
            if (data.hasOwnProperty('msg')){
                $.pnotify({title: data.titulo, text: data.msg, type: data.status});
            }
        });
        return false;

    }

    function adicionaCheckin(id_os_chave, id_caixapadrao, id_caixapadrao_fim){
        $('#id_caixapadrao_checkin').val('');
        $('#id_caixapadrao_checkin_fim').val('');
        $('#id_caixapadrao_checkin').focus();
        $('#AdicionarCheckin').text('(aguarde...)');
        var url = '{{\Memodoc\Servicos\Rotas::ordemservico()}}/addcheckin/' + id_os_chave + '/' + id_caixapadrao + '/' + id_caixapadrao_fim;
        $.ajax({
            type: "GET",
                    url: url,
                    dataType: "json",
                    async:true
            })
        .done(function(data) {
            $('#tabela-checkins').prepend(data.conteudo);
            $('#AdicionarCheckin').text('Adicionar');
            if (data.hasOwnProperty('msg')){
                $.pnotify({title: data.titulo, text: data.msg, type: data.status});
            }
        });
        return false;
    }


    function removeCheckin(id_os_chave, id_caixa){
        var url = '{{\Memodoc\Servicos\Rotas::ordemservico()}}/delcheckin/' + id_os_chave + '/' + id_caixa;
        $('#checkins').html('<i class="aweso-spinner aweso-spin"></i>');
        $.ajax({
            type: "GET",
                    url: url,
                    dataType: "json"
            })
            .done(function(data) {
                if (data.status == 'success'){
                    $('#checkins').html(data.conteudo);
                }
                $.pnotify({title: data.titulo, text: data.msg, type: data.status});
            });
        return false;
    }

    function confirmaInclusaoCheckin(botao, id_os_chave, id_caixapadrao){
        $(botao).text('(aguarde...)');
        var tr = $(botao).closest('tr');
        var url = '{{\Memodoc\Servicos\Rotas::ordemservico()}}/addcheckinconfirmado/' + id_os_chave + '/' + id_caixapadrao;
        $.ajax({
            type: "GET",
                    url: url,
                    dataType: "json",
                    async:true
            })
        .done(function(data, status) {
            $('.checkin_'+id_caixapadrao).not(tr).remove();
            $(tr).before(data.conteudo);
            $(tr).remove();
            if (data.hasOwnProperty('msg')){
                $.pnotify({title: data.titulo, text: data.msg, type: data.status});
            }
        })
        .fail(function(jxqr, status, error){
        });
        return false;
    }


    function adicionaEmprestimo(id_os_chave, id_caixapadrao, id_caixapadrao_fim){
        $('#id_caixapadrao_emprestimo').val('');
        $('#id_caixapadrao_emprestimo_fim').val('');
        $('#id_caixapadrao_emprestimo').focus();
        $('#AdicionarEmprestimo').text('(aguarde...)');
        var url = '{{\Memodoc\Servicos\Rotas::ordemservico()}}/addemprestimo/' + id_os_chave + '/' + id_caixapadrao + '/' + id_caixapadrao_fim;
//        $('#checkins').html('<i class="aweso-spinner aweso-spin"></i>');
        var numero = '000000' + id_caixapadrao;
//                var novalinha = '<tr id="emprestimo' + id_caixapadrao + '"><td style="text-align:center;">-</td><td style="text-align:center;"><i class="aweso-spinner aweso-spin"></i></td><td style="text-align:center;">' + numero.substr( - 6) + '</td><td style="text-align:center;"><i class="aweso-question"></i></td><td>(analisando...)</td><td>&nbsp;    </td><td>&nbsp;</td></tr>';
//              $('#tabela-emprestimos').prepend(novalinha);
        $.ajax({
            type: "GET",
                    url: url,
                    dataType: "json",
                    async:true
            })
            .done(function(data) {
                $('#tabela-emprestimos').prepend(data.conteudo)
//                    $('#emprestimo' + id_caixapadrao).html(data.conteudo);
                $('#AdicionarEmprestimo').text('Adicionar');
                if (data.hasOwnProperty('msg')){
                    $.pnotify({title: data.titulo, text: data.msg, type: data.status});
                }
        });
        return false;
    }


    function removeEmprestimo(id_osdesmembrada){
        var url = '{{\Memodoc\Servicos\Rotas::ordemservico()}}/delemprestimo/' + id_osdesmembrada ;
        $('#emprestimos').html('<i class="aweso-spinner aweso-spin"></i>');
        $.ajax({
        type: "GET",
                url: url,
                dataType: "json"
        })
        .done(function(data) {
            if (data.status == 'success'){
                $('#emprestimos').html(data.conteudo);
            }
            $.pnotify({title: data.titulo, text: data.msg, type: data.status});
        });
        return false;
    }

</script>
@stop
