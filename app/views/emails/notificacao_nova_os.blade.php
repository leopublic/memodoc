<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
        <div><img src="http://local.memodoc.com.br/img/logo.png"/></div>
        <br/><b>Notificação de abertura de ordem de serviço</b>
        <br/>
        <br/><b>Data/Hora da Solicitação:</b> {{$os->solicitado_em}}
        <br/>
        <br/><b>Cliente:</b> {{$os->cliente->razaosocial}}
        <br/><b>Solicitante:</b> {{$os->responsavel}}
        <br/>
        <br/><b>Solicito em caráter:</b> {{$os->urgente == 1 ? 'EMERGENCIAL' : 'NÃO EMERGENCIAL'}}
        <br/>
        <br/><b>Para ser atendido no endereço:</b> {{$os->endeentrega->completo()}}
        <br/>
        <br/><b>Os seguintes serviços:</b>
        @if($os->qtd_caixas_retiradas_cliente > 0)
        <br/><b>Caixas para retirar:</b> {{$os->qtd_caixas_retiradas_cliente}}
        @endif
        @if($os->qtd_cartonagens_cliente > 0)
        <br/><b>Enviar cartonagens:</b> {{$os->qtd_cartonagens_cliente}}
        @endif
        @if($os->qtdenovascaixas > 0)
        <br/><b>Enviar novas etiquetas:</b> {{$os->qtdenovascaixas}}
        @endif
        <br/>
        <br/><b>Observação:</b>
        <br/>{{nl2br($os->observacao)}}
        <br/>
        <br/><b>Referências para consulta:</b>
        <br/>
        @if(count($os->referenciasConsulta()) > 0)
            <? $virg = '';?>
            @foreach($os->referenciasConsulta() as $osdesmembrada)
            {{$virg.substr('000000'.$osdesmembrada->id_caixapadrao, -6)}}
            <? $virg = ', ';?>
            @endforeach
        @else
        (nenhuma referência solicitada)
        @endif
        <br/>
        @if ($os->cadastrada_por->id_nivel > 10)
            <br/><b>Solicitação aberta pela Memodoc</b>
        @else
            <br/><b>Solicitação aberta pelo login: {{$os->cadastrada_por->username}}</b>
        @endif
        <br/>
        <br/><b>Número da solicitação: {{substr('0000000000'.$os->id_os, -10)}}</b>
    </body>
</html>