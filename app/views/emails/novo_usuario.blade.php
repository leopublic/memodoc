<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
    Olá {{$usuario->nomeusuario}}<br/>
    <br/>
    <br/>Você foi cadastrado por {{$usuario->cadastradopor->nomeusuario}} para utilizar o sistema EDoc.<br/>
    <br/>
    <br/>Utilize o link: http://memodoc.com.br/manualedoc/manualedoc.doc para baixar o manual do sistema.
    <br/>
    <br/>Seu login: {{$usuario->username}}
    <br/>Sua senha: {{$password}}
    <br/>
    <br/>Acesse o EDoc pelo link: http://local.memodoc.com.br
    <br/>
    <br/>Atenciosamente,
    <br/>MEMODOC Guarda de Documentos Ltda.
    <br/>Departamento de Informática
    </body>
</html>