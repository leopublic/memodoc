<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
    <br/>Solicitação eletrônica (E-Doc)
    <br/>
    <br/>Data/Hora da Solicitação: {{$os->solicitado_em}}
    <br/>
    <br/>Cliente: {{$os->cliente->razaosocial}}
    <br/>Solicitante: {{$os->responsavel}}
    <br/>
    <br/>Solicito em caráter: {{$os->urgente == 1 ? 'EMERGENCIAL' : 'NÃO EMERGENCIAL'}}
    <br/>
    <br/>Para ser atendido no endereço: {{$os->endeentrega->completo()}}
    <br/>
    <br/>Os seguintes serviços:
    @if($os->qtd_caixas_retiradas_cliente > 0)
    <br/>Caixas para retirar: {{$os->qtd_caixas_retiradas_cliente}}
    @endif
    @if($os->qtd_cartonagens_cliente > 0)
    <br/>Enviar cartonagens: {{$os->qtd_cartonagens_cliente}}
    @endif
    @if($os->qtdenovascaixas > 0)
    <br/>Enviar novas etiquetas: {{$os->qtdenovascaixas}}
    @endif
    <br/>
    <br/>Observação:
    <br/>{{nl2br($os->observacao)}}
    <br/>
    <br/>Referências para consulta:
    <br/>
    @if(count($os->referenciasConsulta()) > 0)
        <? $virg = '';?>
        @foreach($os->referenciasConsulta() as $osdesmembrada)
        {{$virg.substr('000000'.$osdesmembrada->id_caixapadrao, -6)}}
        <? $virg = ', ';?>
        @endforeach
    @else
    (nenhuma referência solicitada)
    @endif
    <br/>
    <br/>Solicitação realizada através do login: {{$os->cadastrada_por->username}}
    <br/>
    <br/>Número da solicitação: {{substr('0000000000'.$os->id_os, -10)}}
    </body>
</html>