<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
    
    Olá {{$usuario->nomeusuario}}<br/>
    <br/>
    <br/>Informamos que a sua senha de acesso ao E-Doc foi alterada com sucesso.<br/>
    <br/>
    <br/>Seu login: {{$usuario->username}}
    <br/>
    <br/>Atenciosamente,
    <br/>MEMODOC Guarda de Documentos Ltda.
    <br/>Departamento de Informática
    </body>
</html>