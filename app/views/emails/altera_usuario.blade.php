<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
    Olá {{$usuario->nomeusuario}}<br/>
    <br/>
    <br/>Este e-mail confirma a alteração do seu login e senha no nosso sistema EDoc.<br/>
    <br/>
    <br/>Seu login: {{$usuario->username}}
    <br/>Sua senha: {{$password}}
    <br/>
    <br/>Atenciosamente,
    <br/>MEMODOC Guarda de Documentos Ltda.
    <br/>Departamento de Informática
    </body>
</html>