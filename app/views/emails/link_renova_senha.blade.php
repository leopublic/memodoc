<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>

    Olá {{$usuario->nomeusuario}}<br/>
    <br/>
    <br/>Foi solicitada uma nova senha para o seu acesso ao E-Doc<br/>
    <br/>
    <br/>Para criar uma nova senha, acesse esse link (validade: 2 horas)
    <br>http://{{ $usuario->link_nova_senha() }}
    <br/>
    <br/>Atenciosamente,
    <br/>MEMODOC Guarda de Documentos Ltda.
    <br/>Departamento de Informática
    </body>
</html>