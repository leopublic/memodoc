<style>
    span.sep {
        border-left: solid 1px #000;margin:0; padding: 0;
    }
</style>
                @if(isset($documentos))

                    @foreach($documentos as $doc)
                    <table style="width:100%;border:1px solid #000; font-family:arial">
                        <tr>
                            <td style="width:350px;">
                                <h1>Planilha de inventário</h1>
                            </td>
                            <td  style="text-align: right">
                                <img src="/img/logo_subtitulo.jpg" style="margin:0;"/><br />
                                Central de Atendimento: 0xx 21 2269-8040
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border-top:1px solid #000">
                                <table style="width:900px">
                                    <tr>
                                        <td style="width:150px; vertical-align: top;" rowspan="2">
                                            Dados da reserva:
                                        </td>
                                        <td style="border:2px dashed #000;padding:10px;line-height:25px" colspan="2">
                                            Cliente: {{$doc->cliente->razaosocial}} <br />
                                            <table>
                                                <tr>
                                                    <td width="600px">
                                                        Inventário caixa: <span style="font-weight:bold;font-size:16px;">{{substr('000000'.$doc->id_caixapadrao, -6)}}</span> <br />
                                                        Reserva Nº: {{substr('000000'.$doc->reserva->id_reserva, -6)}}  - {{$doc->reserva->formatDate("data_reserva")}}
                                                    </td>
                                                    <td style="text-align:right;"><barcode code="{{ $doc->id_caixa }}" type="C39" class="barcode" size="1" height="1"/></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">

                                    Data de destruição da CAIXA (Mês/Ano): ____/_______ (deixar em branco caso o documento seja permanente)
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="line-height:50px">

                                 Departamento: __________________________________
                               <!-- <div style="border:1px solid #000;background:#fff;width:50px;height:30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div> -->
                                Setor: __________________________________


                                Centro de Custo:__________________________________

                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="line-height:40px">
                              [ &nbsp;&nbsp;&nbsp; ] Título do Documento
                              [ &nbsp;&nbsp;&nbsp; ] Período dos Documentos
                              [ &nbsp;&nbsp;&nbsp; ] Descrição dos documentos
                            </td>
                        </tr>
                    </table>
                    <? $i = array("1", "2", "3"); ?>
                    @foreach($i as $item)
                    <table width="100%" style="background:#f0f0f0;padding:3px;font-size:16px;width:100%; border: solid 1px #000">
                        <tr>
                            <td rowspan="3" style="font:bolder 20px arial;text-align: center;background-color:#fff;width:30px;padding:6px;vertical-align: top"  >
                                    I<br />
                                    T<br />
                                    E<br />
                                    M<br />
                                    <br />
                                    {{$item}}
                            </td>
                            <td style="text-align:center;vertical-align: top;font:bolder 14px arial;width:20px;">1</td>
                            <td style="text-align:left;width:700px;">
                                <table style="border-collapse: collapse;">
                                    <tr><td style="border: solid 1px #000;width:700px; background-color: #fff;">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;vertical-align: top;font:bolder 14px arial;">2</td>
                            <td style="text-align:left;width:700px">
                                <table style="border-collapse: collapse;margin-top:2px;">
                                    <tr>
                                        <td style="width:100px;padding: 0; margin: 0;font:bolder 14px arial;">Cronológico</td>
                                        <td style="border: solid 1px #000;background-color: #fff;padding: 0; margin: 0;">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;/&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;/&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">
                                        </td>
                                        <td style="padding: 0; margin: 0;font:bolder 14px arial;">&nbsp;A&nbsp;</td>
                                        <td style="border: solid 1px #000;background-color: #fff;padding: 0; margin: 0;">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;/&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;/&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">
                                        </td>
                                    </tr>
                                </table>
                                <table style="border-collapse: collapse;margin-top:2px;">
                                    <tr>
                                        <td style="width:100px;padding: 0; margin: 0;font:bolder 14px arial;">Numérico</td>
                                        <td style="border: solid 1px #000;background-color: #fff;padding: 0; margin: 0;">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        </td>
                                        <td style="padding: 0; margin: 0;font:bolder 14px arial;">&nbsp;A&nbsp;</td>
                                        <td style="border: solid 1px #000;background-color: #fff;padding: 0; margin: 0;">
                                            &nbsp;&nbsp;&nbsp;&nbsp;<span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="sep">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        </td>
                                    </tr>
                                </table>
                                <table style="border-collapse: collapse;margin-top:2px;">
                                    <tr>
                                        <td style="width:100px;padding: 0; margin: 0;font:bolder 14px arial;">Alfabético</td>
                                        <td style="border: solid 1px #000; background-color: #fff;padding: 0; margin: 0;">&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep"></td>
                                        <td style="padding: 0; margin: 0;font:bolder 14px arial;">&nbsp;A&nbsp;</td>
                                        <td style="border: solid 1px #000; background-color: #fff;padding: 0; margin: 0;">&nbsp;&nbsp;&nbsp;&nbsp;<span class="sep"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;vertical-align: top;font:bolder 14px arial;">3</td>
                            <td style="text-align:left;width:700px;">
                                <table style="border-collapse: collapse;">
                                    <tr><td style="border: solid 1px #000;width:700px; background-color: #fff;font:bolder 13px arial;">&nbsp;</td></tr>
                                    <tr><td style="border: solid 1px #000;width:700px; background-color: #fff;font:bolder 13px arial;">&nbsp;</td></tr>
                                    <tr><td style="border: solid 1px #000;width:700px; background-color: #fff;font:bolder 13px arial;">&nbsp;</td></tr>
                                    <tr><td style="border: solid 1px #000;width:700px; background-color: #fff;font:bolder 13px arial;">&nbsp;</td></tr>
                                    <tr><td style="border: solid 1px #000;width:700px; background-color: #fff;font:bolder 13px arial;">&nbsp;</td></tr>
                                    <tr><td style="border: solid 1px #000;width:700px; background-color: #fff;font:bolder 13px arial;">&nbsp;</td></tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    @endforeach
                    <table style="width:100%;font:normal 12px arial;border:1px dashed #000;margin-top:3px;">
                        <tr>
                            <td style="width:200px;" style="vertical-align: top">
                                <div style="">
                                    Uso interno (Memodoc )<br/><br/><br/><br/><br/><br/>
                                </div>
                            </td>
                            <td style="text-align: center; vertical-align: bottom;text-align: center;">
                               Data:  ____/____/______
                            </td>
                            <td style=" vertical-align: bottom;text-align: center;">
                                ________________________________________________ <br />
                                Nome do Inventariante (LEGÍVEL)
                            </td>

                        </tr>
                    </table>
                    <pagebreak />

                    @endforeach

                @endif