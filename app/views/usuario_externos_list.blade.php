@extends('stilearn-metro')

@section('conteudo')

    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>{{$titulo}}</h1></div>


        <div class='content-action pull-right'>
           @if(isset($linkCriacao))
            <a href='/{{$linkCriacao}}' class='btn'>Novo</a>
            @endif
        </div>
    </header> <!--/ content header -->

    <!-- content page -->
    <article class="content-page clearfix">

        <!-- main page -->
        <div class="main-page">
            <div class="content-inner">
                @include ('padrao/mensagens')
                <!-- search box -->
                {{Form::open(array('url'=>'/usuario/externos', 'method' => 'get', 'class' => 'form-horizontal'))}}
                <div class="search-box">
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <div class="span6">
                                <div class='control-group'>
                                    {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('id_cliente', $clientes, Input::get('id_cliente', Input::old('id_cliente')), array('class' => 'input-block-level', 'data-fx'=>'select2'))}}
                                    </div>
                                </div>                                
                            </div>
                            <div class="span6">
                                <div class='control-group'>
                                    {{Form::label('nomeusuario','Nome ou e-mail',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('nomeusuario', Input::old("nomeusuario", Session::get('nomeusuario')), array('class' => 'input-block-level'))}}
                                    </div>
                                </div>                                
                            </div>
                        </div>
                        <div class='control-group'>
                            <label class="control-label">&nbsp;</label>
                            <div class="controls">
                                <button class='btn btn-success' id="submit-pesquisa" ><i class="aweso-search"></i> Buscar</button>
                            </div>
                        </div>
                    </div>
                </div><!-- /search box -->
                {{Form::close()}}

                <!-- List -->
                @if (isset($instancias))
                {{$instancias->appends(Input::except('page'))->links()}}
                <table class="listagem table table-hover table-striped table-condensed" data-sorter="true" style="width:100%">
                        <thead>
                                <tr>
                                        @if(isset($linkEdicao))
                                            <th class="c" style="width:100px;">Ações</th>
                                        @endif
                                        <th style="width:auto;">Cliente</th>
                                        <th>Nome</th>
                                        <th>Login</th>
                                        <th style="text-align: center;">Nível</th>
                                        <th style="text-align: right;">Qtd acessos</th>
                                        <th style="text-align: center;">Cadastrado em</th>
                                        <th style="text-align: center;">Últ. acesso</th>
                                </tr>
                        </thead>
                        <tbody>
                                @foreach ($instancias as $r)
                                <tr>
                                        @if(isset($linkEdicao) or isset($linkRemocao))
                                                <td class="c">
                                                    @if(isset($linkEdicao))
                                                        <a class="btn btn-primary" title="Editar" href="/{{ $linkEdicao }}/{{ $r->getKey() }}"><i class="aweso-pencil"></i></a>
                                                    @endif
                                                    @include('padrao.link_exclusao', array('url'=> '/usuario/enviarnovasenhaexterno/'. $r->getKey()  , 'title'=>'Enviar uma nova senha aleatória para esse usuário' ,'icon'=>'aweso-envelope', 'cor'=>'bg-orange', 'msg_confirmacao'=>'Clique para confirmar o envio de uma nova senha para o usuário "'.$r->nomeusuario.'"') )
                                                    @if(isset($linkRemocao))
                                                        @include('padrao.link_exclusao', array('url'=> $linkRemocao .'/'. $r->getKey()  , 'msg_confirmacao'=>'Clique para confirmar a exclusão do usuário "'.$r->nomeusuario.'"') )
                                                    @endif
                                                </td>
                                        @endif
                                        <td>
                                            @if ($r->razaosocial != '')
                                            {{$r->razaosocial. ' ('.$r->id_cliente_principal.')' }}
                                            @else
                                            --
                                            @endif
                                        </td>
                                        <td>{{$r->nomeusuario}}</td>
                                        <td>{{$r->username}}</td>
                                        <td style="text-align: center;">{{$r->id_nivel }}</td>
                                        <td style="text-align: right;">{{number_format($r->acessonumero, 0, ",", ".") }}</td>
                                        <td style="text-align: center;">@if($r->datacadastro != '')
                                            {{substr($r->datacadastro, 8,2).'/'.substr($r->datacadastro, 5,2).'/'.substr($r->datacadastro, 0,4)}}
                                            @else
                                            --
                                            @endif
                                        </td>
                                        <td style="text-align: center;">@if($r->ultimoacesso != '')
                                            {{substr($r->ultimoacesso, 8,2).'/'.substr($r->ultimoacesso, 5,2).'/'.substr($r->ultimoacesso, 0,4)}}
                                            @else
                                            --
                                            @endif
                                        </td>
                                </tr>
                                @endforeach
                        </tbody>
                </table>
                @endif
            </div>
        </div>
    </article> <!-- /content page -->
    <style>
        .c{
            width:200px;
            text-align:center;
        }
    </style>
@stop