@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Cadastrar documento</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
@include('_layout.msg')
@if ($disabled != '')
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-block alert-danger ">
            <h4 class="alert-heading">Edição bloqueada</h4>
            <p>O conteúdo desse item não pode ser alterado pois já foi revisado pela Memodoc</p>
        </div>        
    </div>
</div>
@endif
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <!-- widget title -->
                    @if (intval($documento->id_item) == 0)
                    <i class="fa fa-plus"></i>
                    Referência {{$documento->id_caixapadrao}} - Novo item
                    @else
                    <i class="fa fa-edit"></i>
                    Referência {{$documento->id_caixapadrao}} - Item {{$documento->id_item}}
                    @endif
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form ">
                {{ Form::model($documento, array('url' => '/publico/documento/editar/'.$documento->id_caixapadrao.'/'.$documento->id_item, 'class'=>'form-horizontal')) }}
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 style="font-weight: bold;">Classificação do item</h4>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Inicio primeira coluna-->                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Título</label>
                                <div class="col-md-7">
                                    {{Form::text('titulo', null, array('class'=>'form-control', $disabled) )}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Centro de custo</label>
                                <div class="col-md-7">
                                    {{Form::select('id_centro', FormSelect(Auth::user()->cliente->centrocusto,'nome'), null, array('class'=>'form-control select2me', $disabled))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Departamento</label>
                                <div class="col-md-7">
                                    {{Form::select('id_departamento', FormSelect(Auth::user()->cliente->departamento,'nome'), null, array('class'=>'form-control select2me', $disabled))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Setor</label>
                                <div class="col-md-7">
                                    {{Form::select('id_setor', FormSelect(Auth::user()->cliente->setor,'nome'), null, array('class'=>'form-control select2me', $disabled))}}
                                </div>
                            </div>
                            @if (isset($propriedade))
                            <div class="form-group">
                                <label class="control-label col-md-5">{{$propriedade->nome}}</label>
                                <div class="col-md-7">
                                    {{Form::select('id_valor', $valores, null, array('class'=>'form-control select2me', $disabled))}}
                                </div>
                            </div>
                            @endif
                        </div>
                        <!-- fim primeira coluna -->
                        <!-- Inicio segunda coluna -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Intervalo cronológico</label>
                                <div class="col-md-7">
                                    <div class="input-group input-large">
                                        <span class="input-group-addon">de</span>
                                        {{Form::text('inicio_fmt', null ,array('class'=>'form-control  date-picker', 'data-date-format' => 'dd/mm/yyyy', $disabled))}}
                                        <span class="input-group-addon">até</span>
                                        {{Form::text('fim_fmt', null ,array('class'=>'form-control  date-picker', 'data-date-format' => 'dd/mm/yyyy', $disabled))}}
                                    </div>
                                </div>                                
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-5">Intervalo numérico</label>
                                <div class="col-md-7">
                                    <div class="input-group input-large">
                                        <span class="input-group-addon">de</span>
                                        {{Form::text('nume_inicial', null ,array('class'=>'form-control number ', $disabled))}}
                                        <span class="input-group-addon">até</span>
                                        {{Form::text('nume_final', null ,array('class'=>'form-control number ', $disabled))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Intervalo alfabético</label>
                                <div class="col-md-7">
                                    <div class="input-group input-large">
                                        <span class="input-group-addon">de</span>
                                        {{Form::text('alfa_inicial', null ,array('class'=>'form-control alfa ', $disabled))}}
                                        <span class="input-group-addon">até</span>
                                        {{Form::text('alfa_final', null ,array('class'=>'form-control alfa ', $disabled))}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Ref. de migração 1</label>
                                <div class="col-md-7">
                                    {{Form::text('migrada_de', null, array('class'=> 'form-control', $disabled))}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5">Ref. de migração 2</label>
                                <div class="col-md-7">
                                    {{Form::text('request_recall', null, array('class'=> 'form-control', $disabled))}}
                                </div>
                            </div>
                        </div>
                        <!-- Fim segunda coluna -->
                    </div>
                    <!-- fim primeira linha -->
                    <!-- inicio segunda linha -->
                    <div class="row">
                        <div class="col-md-12">
                            <h4 style="font-weight: bold;">Controle de validade e expurgo</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-5">Informe a partir de que ano o item poderá ser expurgado</label>
                                <div class="col-md-7" >
                                    <div class="input-group input-large">
                                        <span class="input-group-addon">mês</span>
                                        {{Form::text('expurgo_programado_mes', null ,array('class'=>'form-control input-small mask_mes', $disabled))}}
                                        <span class="input-group-addon">ano</span>
                                        {{Form::text('expurgo_programado', null ,array('class'=>'form-control input-small number mask_ano', $disabled))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 style="font-weight: bold;">Conteúdo do item</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    @if (isset($documento))
                                    {{Form::textarea('conteudo', str_replace('<br/>',"\n" , $documento->conteudo) , array('class'=>'form-control', $disabled))}}
                                    @else
                                    {{Form::textarea('conteudo', null , array('class'=>'form-control', $disabled))}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                    <!-- fim segunda linha -->
                </div>
                <!-- Fim form-body -->
                <!-- INICIO FORM-ACTIONS -->
                @if (!$disabled)
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-offset-5 col-md-7">
                                <button type="submit" class="btn btn-primary" id="submit-pesquisa">Salvar</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <!-- FIM FORM-ACTIONS -->
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@stop
@section('scripts')
@include('_layout.scripts-forms')
@stop
