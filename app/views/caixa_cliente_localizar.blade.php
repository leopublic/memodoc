@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">{{$titulo}}</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-archive"></i>{{number_format($quantos, 0, ",", ".")}} documentos encontrados </div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form grey-steel filtro">
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->                        
                        <div class="col-md-12">
                            @include('documento_cliente_tabela', array('documentos', $documentos))
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop