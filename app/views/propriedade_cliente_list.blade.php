@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">{{$propriedade->nome}}</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-building"></i>Adicionar novo</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form grey-steel filtro">
                {{Form::open(array('url' => '/publico/propriedade/adicionar', 'class' => 'form-horizontal'))}}
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group input-large">
                                        {{Form::text('nome', Input::old('nome'), array('class'=>'form-control') )}}
                                        <div class="input-group-btn">
                                            <input type="submit" class="btn yellow-gold" class="button" id="adicionaCaixa" value="Adicionar"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-list"></i>Valores ja cadastrados</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body grey-steel">
                <div class="row">
                    <!-- inicio da coluna unica -->
                    <div class="col-md-12">
                        <table class="table table-hover table-striped table-condensed" data-sorter="true" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="c" style="width:120px;">Ações</th>
                                    <th style="width:auto;">Departamento</th>
                                    <th style="width:100px;">Documentos</th>
                                    <th style="width:auto;">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($valores as $r)
                                <tr>
                                    <td class="c">
                                        @if(Auth::user()->id_nivel<=2)
                                        <a class="btn btn-primary" title="Editar" href="#" onclick="$('#form{{ $r->getKey() }}').show();$('#conteudo{{ $r->getKey() }}').hide();"><i class="fa fa-edit"></i></a>
                                        <div class="btn-group">
                                            <button class="btn dropdown-toggle btn-danger" data-toggle="dropdown" title="Excluir" type="button">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            <ul class="dropdown-menu red" role="menu">
                                                <li><a href="/publico/propriedade/excluir/{{ $r->getKey() }}">Clique aqui para confirmar a exclusão do valor<br>(Os documentos serão mantidos, mas sem esse atributo)</a></li>
                                            </ul>
                                        </div>
                                        @endif
                                    </td>
                                    <td>
                                        {{Form::open(array('url' => '/publico/propriedade/alterar', 'class' => 'form-horizontal', 'style' =>'margin-bottom:0;display:none; ','id' => 'form'.$r->getKey() ))}}
                                        <div class="input-group">
                                            {{Form::hidden('id_valor', $r->getKey())}}
                                            {{Form::text('nome', $r->nome, array('class'=>'form-control input-sm input-large '))}}
                                            <input type="submit" class="btn btn-primary btn-sm" class="button" id="adiciona" value="Salvar"/>
                                        </div>
                                        {{ Form::close() }}
                                        <span id="conteudo{{$r->getKey()}}">{{$r->nome}}</span>
                                    </td>
                                    <td style="text-align:right;">{{number_format($r->qtd, 0, ",", ".")}}</td>
                                    <td>&nbsp;</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
@include('_layout.scripts-forms')
@stop
