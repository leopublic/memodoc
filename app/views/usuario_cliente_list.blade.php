@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Usuários</h3>
        <div class='content-action pull-right'>
        @if( Auth::user()->id_nivel==1 )    
            <a href='/publico/usuario/novo' class='btn btn-primary'>Cadastrar novo</a>
        @endif
        </div>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio conteúdo -->
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-list"></i>Usuários cadastrados</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body ">
                <div class="row">
                    <!-- Inicio primeira coluna-->                        
                    <div class="col-md-12">
                        <table class="listagem table table-hover table-striped table-condensed" data-sorter="true" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:160px;">Ações</th>
                                    <th style="width:auto;">Nome</th>
                                    <th style="width:auto;">Login</th>
                                    <th style="width:auto; text-align: center;">Nível</th>
                                    <th style="width:100px;">Criado em</th>
                                    <th style="width:100px;">Validade</th>
                                    <th style="text-align: center;">Qtd acessos</th>
                                    <th style="width:180px;">Último acesso</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($instancias as $r)
                                <tr>
                                    <td class="c">
                                        @if( Auth::user()->id_nivel==1 )
                                        <a class="btn btn-primary" title="Editar" href="/publico/usuario/editar/{{ $r->getKey() }}"><i class="fa fa-edit"></i></a>
                                        <div class="btn-group">
                                            <button class="btn dropdown-toggle yellow-gold" data-toggle="dropdown" title="Enviar nova senha"><i class="fa fa-lock"></i></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="/publico/usuario/renovarsenha/{{ $r->getKey() }}">Clique para enviar uma nova senha para o usuário</a></li>
                                            </ul>
                                        </div>
                                        @include('_layout.link-exclusao', array('msg'=> 'Clique para confirmar a exclusão do usuário', 'link'=>'/publico/usuario/excluir/'.$r->getKey()))
                                        @endif
                                    </td>
                                    <td>{{$r->nomeusuario}}</td>
                                    <td>{{$r->username}}</td>
                                    <td style="text-align: center;">{{$r->id_nivel }}</td>
                                    <td>
                                        @if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$r->datacadastro))
                                        {{substr($r->datacadastro, 8,2).'/'.substr($r->datacadastro, 5,2).'/'.substr($r->datacadastro, 0,4) }}
                                        @else
                                        (indeterminada)
                                        @endif
                                    </td>
                                    <td>
                                        @if(intval($r->validadelogin) > 0)
                                        {{$r->validadelogin}} dias
                                        @else
                                        (indeterminada)
                                        @endif
                                    </td>
                                    <td style="text-align: center;">{{number_format($r->acessonumero, 0, ",", ".") }}</td>
                                    <td>
                                        @if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) ([0-9][0-9]):([0-9][0-9]):([0-9][0-9])$/",$r->ultimoacesso) && $r->ultimoacesso != '0000-00-00 00:00:00')
                                        {{substr($r->ultimoacesso, 8,2).'/'.substr($r->ultimoacesso, 5,2).'/'.substr($r->ultimoacesso, 0,4).' '.substr($r->ultimoacesso, 11) }}
                                        @else
                                        (nunca acessou)
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
