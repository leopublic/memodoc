@extends('stilearn-metro')

@section('conteudo')

@include('cliente_tab_header')

<?php
$atendimento = array(
    '-1' => '(n/a)',
    '0' => '12 horas',
    '1' => '24 horas',
);

?>

<!-- Aba identificação -->
<div class="tab-pane" id="tab_Propriedade">
    @if ($prop->id_propriedade > 0)
        {{Form::model($prop) }}
        {{Form::hidden('id_cliente', $cliente->id_cliente)}}
        {{Form::hidden('id_propriedade', $prop->id_propriedade)}}
        <div class="box">
            <ul class="form">
                <li class='subtitle'>
                    <h3 CLASS='pull-left'>Campo personalizado: "{{$prop->nome}}"</h3>
                </li>
            </ul>
        </div>
        {{ Form::close() }}
        <div class="listbox">
            <div class="box">
                <div class='form clearfix'>
                    <div class='subtitle' style="text-align: left;">
                        <h3 class="pull-left"> Valores possíveis </h3> 
                        <a href="/cliente/valoredit/{{$cliente->id_cliente}}/0#tab_Propriedade" class="btn pull-right" style="margin-left:5px;"><i class="icon-plus-sign"></i> Novo valor</a>
                    </div>
                </div>
                <table class='table table-striped table-hover'>
                    <thead>
                    <th style="width:20px;text-align:center">#</th>
                    <th style="width:80px;text-align:center">Ações</th>
                    <th>Valor</th>
                    <th style="width:200px;text-align:center">Quantidade de documentos com esse valor</th>
                    </thead>
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($valores as $valor)
                        <? $i++; ?>
                        <tr>
                            <td style="text-align:center;"> {{$i}}</td>
                            <td style="text-align:center;"> 
                                <a title='Editar' href="/cliente/valoredit/{{$cliente->id_cliente}}/{{$valor->id_valor}}#tab_Propriedade" class="btn btn-mini btn-primary" title="Editar"><i class="icon-white icon-edit"></i></a>
                                <div class="btn-group">
                                    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-danger btn-mini" title="Excluir esse item"><i class="icon-white icon-remove-sign"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a title='Deletar' href="/cliente/valordel/{{$valor->id_valor}}" class="btn btn-danger" title="Confirmação">Clique aqui para confirmar a exclusão</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td> {{$valor->nome}}</td>
                            <td style="text-align: center;"> {{number_format($valor->qtdDocs, 0, ',', '.')}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    @else
        <div class="box">
            <ul class="form">
                <li class='subtitle'>
                    <h3 CLASS='pull-left'>Esse cliente ainda não possui o campo personalizado.</h3>
                    <a href="/cliente/propriedadeedit/{{$cliente->id_cliente}}#tab_Propriedade" class="btn btn-primary pull-right"><i class="icon-white icon-plus"></i> Adicionar</a>
                </li>
            </ul>
        </div>
    @endif
</div><!-- Fim Aba identificação -->

@include('cliente_tab_footer')
@stop