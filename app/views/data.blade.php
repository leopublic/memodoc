@extends('stilearn-metro')

@section('conteudo')

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Bem-vindo!</h1></div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">
            @include('padrao.mensagens')
            <div class="row-fluid">
                {{ date('d/m/Y')}}<br/>{{date('H:i:s')}}<br>
                {{ $data}}<br/>{{$hora}}
            </div>    
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .c{
        width:200px;
        text-align:center;
    }
</style>
@stop