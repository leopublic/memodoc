@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Cadastrar documentos</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-archive"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Selecione a referência</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('url' => '/documento/carregacaixa', 'class' => 'form-horizontal'))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente', $clientes, $id_cliente , array('id'=>'id_cliente','data-fx'=>'select2', 'required', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        {{Form::label('id_caixapadrao','Referência',array('class'=>'control-label'))}}
                        <div class='controls'>
                            <div class="input-append">
                                {{Form::text('id_caixapadrao', Input::old('id_caixapadrao', $id_caixapadrao))}}
                                <input type="submit" class="btn bg-cyan" class="button" id="localizar" value="Recuperar conteúdo atual"/>
                            </div>

                        </div>
                    </div>
                    @if ($id_caixapadrao > 0)
                    <div class="form-actions bg-silver">
                        <input type="submit" name="adicionar" class="btn btn-primary" value="Incluir novo item">
                    </div>
                    @endif
                    {{ Form::close() }}
                </div>
            </div>

            @include('padrao.mensagens')

            @if(isset($documentos))

            <div class="widget border-cyan" id="itens">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">{{$documentos->count()}} documentos encontrados </h4>
                    <div class="widget-action color-cyan">
                    </div>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    <table class='table table-striped table-hover table-bordered table-condensed'>
                        <thead>
                            <tr>
                                <th style="width:50px; text-align: center;">Ação</th>
                                <th style="width:50px; text-align: center;">Item</th>
                                <th style="width:auto">Título/Conteúdo</th>
                                <th style="width:auto">Digitalizações</th>
                                <th style="width:210px">Área</th>
                                <th style="width:180px">Classificação</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($documentos as $doc)
                            <tr>
                                <td style="text-align: center;">
                                    @if (Auth::user()->privilegio() >=4  && $doc->id_expurgo == 0)
                                    <a href="/documento/editar/{{ $doc->id_cliente}}/{{ $doc->id_caixapadrao}}/{{ $doc->id_item}}" class="btn btn-primary btn-sm" style="margin-bottom:5px;" title="Clique para editar esse item"><i class="aweso-edit"></i></a>
                                    @include('padrao.link_exclusao', array('url' =>'/documento/exclua/'.$doc->id_cliente.'/'.$doc->id_caixapadrao.'/'.$doc->id_documento, 'msg_confirmacao' =>'Clique para confirmar a exclusão desse item'))
                                    @endif
                                </td>
                                <td style="text-align: center;">{{ substr("000".$doc->id_item, -3)}}</td>
                                <td><b>{{$doc->titulo}}</b><br/>{{$doc->conteudo}}
                                @if ($cliente->fl_controle_revisao)
                                <br/><span style="font-style: italic;font-size: 11px;">{{$doc->situacao_revisao}}</span>
                                @endif
                                </td>
                                <td>
                                    @foreach ($doc->anexos as $anexo)
                                    <p><a href="/anexo/download/{{$anexo->id_anexo}}" class="btn btn-primary"><i class="aweso-cloud-download"></i></a> {{$anexo->descricao}}</p>
                                    @endforeach
                                </td>
                                <td style="padding:0;">
                                    <table >
                                        <tr><td><b>Setor:</b></td><td>{{isset($doc->setcliente->nome) ? $doc->setcliente->nome : '--' }}</td></tr>
                                        <tr><td><b>Departamento:</b></td><td>{{isset($doc->depcliente->nome) ? $doc->depcliente->nome : '--' }}</td></tr>
                                        <tr><td><b>Centro de custos:</b></td><td>{{isset($doc->centrodecusto->nome) ? $doc->centrodecusto->nome : '--' }}</td></tr>
                                        @if (isset($propriedade))
                                        <tr><td><b>{{$propriedade->nome}}:</b></td><td>{{count($doc->valor) >0 ? $doc->valor->nome : '--' }}</td></tr>
                                        @endif
                                        <tr><td style="border-right: none;border-left: none;">&nbsp;</td><td style="border-right: none;border-left: none;">&nbsp;</td></tr>
                                    </table>
                                </td>
                                <td style="padding:0;">
                                    <table style="width:100%;">
                                        <tr><td>Tipo</td><td>{{$doc->tipodocumento->descricao}}</td></tr>
                                        <tr><td>Origem 1</td><td>{{$doc->migrada_de}}</td></tr>
                                        <tr><td>Origem 2</td><td>{{$doc->request_recall}}</td></tr>
                                        <tr><td>Início</td><td>
                                                @if ($doc->inicio != '')
                                                {{$doc->formatDate('inicio')}}
                                                @endif
                                            </td></tr>
                                        <tr><td>Fim</td><td>
                                                @if ($doc->fim != '')
                                                {{$doc->formatDate('fim')}}
                                                @endif
                                            </td></tr>
                                        <tr><td>N&ordm; inicial</td><td>{{$doc->nume_inicial}}</td></tr>
                                        <tr><td>N&ordm; final</td><td>{{$doc->nume_final}}</td></tr>
                                        <tr><td>Alfa inicial</td><td>{{$doc->alfa_inicial}}</td></tr>
                                        <tr><td>Alfa final</td><td>{{$doc->alfa_final}}</td></tr>
                                    </table>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                @endif
            </div>
        </div>
</article> <!-- /content page -->
@stop

@section('scripts')
<script>
    $(function() {
        $departamento = $('#id_departamento');
        $setor = $('#id_setor');
        $centrocusto = $('#id_centro');
        $cliente.change(function() {

            $('.load').show();
            $('#titulo').focus();
            $('.documentos').hide();

            // limpando combos
            $departamento.html('');
            $setor.html('');
            $centrocusto.html('');

            // ajax departamento
            $.get('publico/documento/ajaxdepartamento/' + $(this).val(), function(combo) {
                $departamento.html(combo);
            });
            // ajax centro custo
            $.get('publico/documento/ajaxcentrocusto/' + $(this).val(), function(combo) {
                $centrocusto.html(combo);
            });
            // ajax setor
            $.get('publico/documento/ajaxsetor/' + $(this).val(), function(combo) {
                $setor.html(combo);
            });

            // Remove o load após 2.5 segundos
            setInterval(function() {
                $('.load').hide();
            }, 2500);

        }).focus();
    });
</script>
@stop