<html>
    <meta charset="UTF-8" />
    <head>
        <style>
            td{vertical-align: top}
            th{vertical-align: top; font-weight: bold; text-align:right;}
            table.detalhe th {text-align:left; background-color: #DDDDDD;}
            table.detalhe { page-break-inside:auto }
            table.detalhe tr { page-break-inside:avoid; page-break-after:auto; page-break-inside: avoid;}
            table.detalhe tbody tr td{border-top:solid 1px #000; page-break-after:auto;page-break-inside: avoid; overflow: ;}
            table, tr, td, th, tbody, thead, tfoot {
                   page-break-inside: avoid !important;
            }

        </style>

    </head>
    <body>

        @if(count($expurgos) > 0)
        <?php $i = 0; 
        $id_caixapadrao_ant = '';
        $find =  '/.,./';
        $replace = " , ";
        ?>
        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="font-size:11px;font-family: arial;" class="detalhe">
            <thead>
                <tr>
                    <th width="3%" style="text-align:center;">#</th>
                    <th width="3%" style="text-align:center;">Caixa</th>
                    <th width="3%" style="text-align:center;">Item</th>
                    <th width="20%">Título</th>
                    <th width="auto">Conteúdo</th>
                    <th width="15%" style="text-align:center;">Período</th>
                </tr>
            </thead>
            <tbody>
            @foreach($expurgos as $r)
            <tr>
                <td style="text-align:center; font-weight:bold;">
                @if($id_caixapadrao_ant == '' || $id_caixapadrao_ant != $r->id_caixapadrao)
                    <?php $i++; ?>
                    {{ $i }}
                    <?php $id_caixapadrao_ant = $r->id_caixapadrao; ?>
                @else
                &nbsp;
                @endif
                </td>
                <td style="text-align:center;border-top:solid 1px #000;">{{$r->id_caixapadrao}}</td>
                <td style="text-align:center;">{{$r->id_item}}</td>
                <td>{{$r->titulo}}</td>
                <td>{{ nl2br(preg_replace('/\S,\S/', ', ', $r->conteudo)) }}</td>
                <td style="text-align:center;">
                    @if ($r->inicio_fmt != '')
                    {{$r->inicio_fmt}}
                    @endif
                    a  @if ($r->fim_fmt != '')
                    {{$r->fim_fmt}}
                    @endif
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        <br />
        Total de documentos relacionados: {{count($expurgos)}}
        <br />
        <div style="width:100%;padding:10px; text-align: right">
            <table>
                <tr>
                    <td width="300px" style="border-bottom:solid 1px black;">&nbsp;</td>
                    <td width="10px" >&nbsp;</td>
                    <td width="120px" style="border-bottom:solid 1px black;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:center;">Assinatura do cliente</td>
                    <td width="10px" >&nbsp;</td>
                    <td style="text-align:center;">Data</td>
                </tr>
            </table>
        </div>
        @else
        --
        @endif
    </body>
</html>
