@extends('layout')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Selecione o cliente do novo usuário</h1></div>

    <!-- content breadcrumb -->
    <ul class="breadcrumb breadcrumb-inline clearfix">
        <li><a href="#">Usuário</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
        <li class="active">Cliente</li>
    </ul>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">
            @include('padrao.mensagens');
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-archive"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Informe o cliente</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente', $clientes, Input::old('id_cliente', Session::get('id_cliente')) , array('id'=>'id_cliente','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <input type="submit" name="adicionar" class="btn btn-primary" value="Criar">
                    </div>
                    {{ Form::close() }}
                </div>
            </div>


        </div>
    </div>
</article> <!-- /content page -->
@stop