@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Criar transbordo para o(s) galpõe(s) {{$galpoes}}</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-share-alt"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Primeiro informe o cliente de origem, o cliente de destino. </h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                   <div class='control-group'>
                        {{Form::label('id_cliente','Cliente origem',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente_antes', $clientes, Input::old('id_cliente_antes') , array('id'=>'id_cliente_antes','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente destino',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente_depois', $clientes, Input::old('id_cliente_depois') , array('id'=>'id_cliente_depois','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" name="adicionar" class="btn btn-primary" id="adicionar" ><i class="aweso-plus"></i> Adicionar caixas</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop
