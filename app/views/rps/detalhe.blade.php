@extends('stilearn-metro')

@section('styles')
@stop

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header">
        <h1>Remessas de notas fiscais </h1>
        Lote {{$lote_rps->id_lote_rps}}<br/>
        @if(isset($perfat))
            Do faturamento de {{$perfat->mes}}/{{$perfat->ano}}</br>
        @endif
        Emissão {{$lote_rps->lrps_dt_emissao_fmt}}
    </div>

    <ul class="content-action pull-right">
        <li>
            <div class="btn-group">
                <a class="btn silver dropdown-toggle " data-toggle="dropdown" href="#">
                    <i class="aweso-cogs"></i>Ações </a>
                <ul class="dropdown-menu pull-right cyan">
                    <li><a href="/loterps/gerararquivo/{{$lote_rps->id_lote_rps}}"  title="">Gerar arquivo</a></li>
                    <li><a href="/notacarioca/retorno"  title="">Carregar notas emitidas</a></li>
                    <li><a href="/sicoobremessa/gerar/{{$lote_rps->id_lote_rps}}" target="_blank" title="Gerar os boletos" class="btn btn-sm btn-success"><i class="aweso-barcode"></i></a></li>
                </ul>
            </div>
        </li>
    </ul>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            <!-- search box -->
            @include('padrao.mensagens')
            
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Notas incluídas na remessa</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                    @if(isset($notas_incluidas))
                    <table class='table table-striped table-hover table-bordered table-condensed table-fixed-header table-menor'>
                        <thead class="header">
                            <tr>
                                <th style="width:50px; text-align:center;" >Linha</th>
                                <th style="width:150px; text-align:center;" >Ação</th>
                                <th style="width:auto; text-align:center;" >Número</th>
                                <th style="width:auto; text-align:left;" >Cliente</th>
                                <th style="width:auto; text-align:center;" >Emissão de nota</th>
                                <th style="width:auto; text-align:center;" >Tipo faturamento</th>
                                <th style="width:auto; text-align:center;" >Dia venc.</th>
                                <th style="width:120px; text-align:right;" >Valor</th>
                                <th style="width:120px; text-align:right;" >Iss</th>                                
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $i = 1;
                            $total_iss = 0;
                            $total = 0;
                        ?>
                        @foreach($notas_incluidas as $nota)
                        <?php
                            $i++;
                            $val_servico = $nota->valor_total_emissao;
                            $val_iss = $nota->valor_iss_emissao;
                            
                            $total = $total + round($val_servico, 2); 
                            $total_iss =  $total_iss + round($val_iss, 2); 
                            
                            ?>


                            <tr id="tr{{$nota->id_nota_fiscal}}"  class="nota" data-valor="{{$nota->val_servico}}" >
                                <td style="text-align: center;">{{ $i }}</td>
                                <td style="text-align: center;">
                                    <a href="javascript:excluir_nota('{{$nota->id_nota_fiscal}}');"  id="btnExcluir{{$nota->id_nota_fiscal}}" title="Retirar nota do lote" class="btn btn-small btn-danger"><i class="aweso-remove"></i></a>
                                    <a href="#" title="Aguarde..." class="btn btn-small btn-danger" id="btnSpin{{$nota->id_nota_fiscal}}" style="display:none;"><i class="aweso-spin aweso-spinner"></i></a>
                                    <a href="/cliente/contrato/{{$nota->id_cliente}}#tab_contrato" title="Editar cliente" class="btn btn-small btn-warning" target="_blank"><i class="aweso-edit"></i></a>
                                    @if($nota->codigo_verificacao != '')
                                        <a href="https://notacarioca.rio.gov.br/contribuinte/notaprint.aspx?ccm=3130509&nf={{$nota->nfis_numero}}&cod={{$nota->codigo_verificacao}}" title="Abrir nota" class="btn btn-small btn-success" target="_blank"><i class="aweso-file-text"></i></a>
                                    @endif
                                </td>
                                <td style="text-align: center;">{{ $nota->nfis_numero }}</td>
                                <td style="text-align: left;">{{ $nota->razaosocial }} ({{ $nota->id_cliente }} - {{ $nota->nomefantasia }})</td>
                                <td style="text-align: center;">
                                @if($nota->fl_emissaonf_suspensa == 1)
                                    suspensa
                                @else
                                    habilitada
                                @endif
                                </td>
                                <td style="text-align: center;">{{ $nota->descricao }}</td>
                                <td style="text-align: center;">{{ $nota->dia_vencimento }}</td>
                                <td style="text-align: right;">R$ {{ number_format($val_servico, 2, ",", ".") }}</td>
                                <td style="text-align: right;">R$ {{ number_format($val_iss, 2, ",", ".") }}</td>
                            </tr>
                        @endforeach
                            <tr>
                                <td style="text-align: right;" style="font-weight:bold;" colspan="7">TOTAL</td>
                                <td style="text-align: right;" style="font-weight:bold;" id="valor_total">R$ {{ number_format($total, 2, ",", ".") }}</td>
                                <td style="text-align: right;" style="font-weight:bold;">R$ {{ number_format($total_iss, 2, ",", ".") }}</td>
                            </tr>
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>

            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Notas do mesmo faturamento fora da remessa</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                    @if(isset($notas_fora))
                    <table class='table table-striped table-hover table-bordered table-condensed table-fixed-header table-menor'>
                        <thead class="header">
                            <tr>
                                <th style="width:100px; text-align:center;" >Ação</th>
                                <th style="width:auto; text-align:left;" >Cliente</th>
                                <th style="width:auto; text-align:center;" >Emissão de nota</th>
                                <th style="width:auto; text-align:center;" >Tipo faturamento</th>
                                <th style="width:auto; text-align:center;" >Dia venc.</th>
                                <th style="width:120px; text-align:right;" >Valor</th>
                                <th style="width:120px  ; text-align:right;" >Iss</th>                                
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $total_iss = 0;
                            $total = 0;
                        ?>
                            @foreach($notas_fora as $nota)
                        <?php
                            $val_servico = $nota->valor_total_emissao;
                            $val_iss = $nota->valor_iss_emissao;
                            
                            $total = $total + round($val_servico, 2); 
                            $total_iss =  $total_iss + round($val_iss, 2); 
                            
                            ?>


                            <tr id="{{$nota->id_nota_fiscal}}" >
                                <td style="text-align: center;">
                                    <a href="/loterps/adicionarnota/{{$lote_rps->id_lote_rps}}/{{$nota->id_nota_fiscal}}" title="Incluir nota no lote" class="btn btn-small btn-success"><i class="aweso-plus"></i></a>
                                    <a href="/cliente/contrato/{{$nota->id_cliente}}#tab_contrato" title="Editar cliente" class="btn btn-small btn-warning" target="_blank"><i class="aweso-edit"></i></a>
                                </td>
                                <td style="text-align: left;">{{ $nota->razaosocial }} ({{ $nota->id_cliente }} - {{ $nota->nomefantasia }})</td>
                                <td style="text-align: center;">
                                @if($nota->fl_emissaonf_suspensa == 1)
                                suspensa
                                @else
                                habilitada
                                @endif
                                </td>
                                <td style="text-align: center;">{{ $nota->descricao }}</td>
                                <td style="text-align: center;">{{ $nota->dia_vencimento }}</td>
                                <td style="text-align: right;">R$ {{ number_format($val_servico, 2, ",", ".") }}</td>
                                <td style="text-align: right;">R$ {{ number_format($val_iss, 2, ",", ".") }}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td style="text-align: right;" style="font-weight:bold;" colspan="5">TOTAL</td>
                                <td style="text-align: right;" style="font-weight:bold;">R$ {{ number_format($total, 2, ",", ".") }}</td>
                                <td style="text-align: right;" style="font-weight:bold;">R$ {{ number_format($total_iss, 2, ",", ".") }}</td>
                            </tr>
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>


            <div class="widget border-red">
                <!-- widget header -->
                <div class="widget-header bg-red">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Notas de clientes com emissão de nota suspensa</h4>
                </div><!-- /widget header -->


                <!-- widget content -->
                <div class="widget-content">
                    @if(isset($notas_suspensas))
                    <table class='table table-striped table-hover table-bordered table-condensed table-fixed-header table-menor'>
                        <thead class="header">
                            <tr>
                                <th style="width:100px; text-align:center;" >Ação</th>
                                <th style="width:auto; text-align:left;" >Cliente</th>
                                <th style="width:auto; text-align:center;" >Emissão de nota</th>
                                <th style="width:auto; text-align:center;" >Tipo faturamento</th>
                                <th style="width:auto; text-align:center;" >Dia venc.</th>
                                <th style="width:120px; text-align:right;" >Valor</th>
                                <th style="width:120px  ; text-align:right;" >Iss</th>                                
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $total_iss = 0;
                            $total = 0;
                        ?>
                            @foreach($notas_suspensas as $nota)
                        <?php
                            $val_servico = $nota->valor_total_emissao;
                            $val_iss = $nota->valor_iss_emissao;
                            
                            $total = $total + round($val_servico, 2); 
                            $total_iss =  $total_iss + round($val_iss, 2); 
                            
                            ?>


                            <tr id="{{$nota->id_nota_fiscal}}">
                                <td style="text-align: center;">
                                    <a href="/loterps/adicionarnota/{{$lote_rps->id_lote_rps}}/{{$nota->id_nota_fiscal}}" title="Incluir nota no lote" class="btn btn-small btn-success"><i class="aweso-plus"></i></a>
                                    <a href="/cliente/contrato/{{$nota->id_cliente}}#tab_contrato" title="Editar cliente" class="btn btn-small btn-warning" target="_blank"><i class="aweso-edit"></i></a>
                                </td>
                                <td style="text-align: left;">{{ $nota->razaosocial }} ({{ $nota->id_cliente }} - {{ $nota->nomefantasia }})</td>
                                <td style="text-align: center;">
                                @if($nota->fl_emissaonf_suspensa == 1)
                                suspensa
                                @else
                                habilitada
                                @endif
                                </td>
                                <td style="text-align: center;">{{ $nota->descricao }}</td>
                                <td style="text-align: center;">{{ $nota->dia_vencimento }}</td>
                                <td style="text-align: right;">R$ {{ number_format($val_servico, 2, ",", ".") }}</td>
                                <td style="text-align: right;">R$ {{ number_format($val_iss, 2, ",", ".") }}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td style="text-align: right;" style="font-weight:bold;" colspan="5">TOTAL</td>
                                <td style="text-align: right;" style="font-weight:bold;">R$ {{ number_format($total, 2, ",", ".") }}</td>
                                <td style="text-align: right;" style="font-weight:bold;">R$ {{ number_format($total_iss, 2, ",", ".") }}</td>
                            </tr>
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>


        </div>
</article> <!-- /content page -->
@stop
@section('scripts')
<script>
function excluir_nota(id_nota_fiscal){
    $("#btnSpin"+id_nota_fiscal).show();
    $("#btnExcluir"+id_nota_fiscal).hide();
    $.getJSON("/loterps/removernota/"+id_nota_fiscal)
        .done(function(data){
            if(data.retcode == 0){
                $("#tr"+id_nota_fiscal).remove();
                recalcula_total();
            }
        });
}
function recalcula_total(){
    let total = 0;
    $(".nota").each(function(index){
        total = total + $(this).attr('data-valor');
    });
    $("#valor_total").html(total);
}
</script>
@stop
