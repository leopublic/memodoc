@extends('stilearn-metro')

@section('styles')

.control-group > .control-label{
    width: 280px;
}
.control-group > .controls{
    margin-left: 300px;
}

@stop

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Nota Carioca - Emitir notas fiscais</h1></div>

    <ul class="content-action pull-right">
        <a href="/notacarioca/retorno" target="_blank" class="btn btn-success">Carregar notas emitidas</a>
    </ul>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            <!-- search box -->
            @include('padrao.mensagens')
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-plus"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title" id="status">Gerar uma nova remessa de notas fiscais</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                    {{Form::open(array('class' => 'form-horizontal', 'url'=>"/loterps/novodefaturamento"))}}
                    <div class='control-group'>
                        {{Form::label('id_periodo_faturamento','Faturamento',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_periodo_faturamento', $perfats, null, array('id'=>'id_periodo_faturamento','data-fx'=>'select2', 'required', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('dia_vencimento','Filtrar pelo vencimento',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('dia_vencimento', $dias_vencimento, null, array('id'=>'id_periodo_faturamento','data-fx'=>'select2', 'class' => 'input-large'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('id_tipo_faturamento','Tipo de faturamento',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_tipo_faturamento', $tipos_faturamento, null, array('id'=>'id_periodo_faturamento','data-fx'=>'select2', 'class' => 'input-large'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('lrps_dt_emissao','Data de emissão das notas',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('lrps_dt_emissao',  null, array('id'=>'lrps_dt_emissao', 'required', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <input type="submit" class="btn btn-primary" value="Criar novo">
                    </div>
                    {{ Form::close() }}
                </div>
            </div>

            
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-reorder"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Remessa geradas anteriormente</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                    @if(isset($lotes_rps))
                    <table class='table table-striped table-hover table-bordered table-condensed table-fixed-header'>
                        <thead class="header">
                            <tr>
                                <th style="width:130px; text-align:center;" >Ação</th>
                                <th style="width:auto; text-align:center;" >ID</th>
                                <th style="width:auto; text-align:center;" >Faturamento</th>
                                <th style="width:auto; text-align:center;" >Gerado em</th>
                                <th style="width:auto; text-align:center;" >Data de emissão das notas</th>
                                <th style="width:auto; text-align:center;" >Notas</th>
<!--                                <th style="width:auto; text-align:center;" >Status</th>                                -->
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($lotes_rps as $lote_rps)
                            <tr id="lote{{$lote_rps->id_lote_rps}}" data-lrps-dt-geracao="{{$lote_rps->lrps_dt_geracao_fmt}}">
                                <td style="text-align: center;">
                                    <a href="/loterps/editar/{{$lote_rps->id_lote_rps}}" target="_blank" title="Listar notas do lote" class="btn btn-sm btn-primary"><i class="aweso-list"></i></a>
                                    <a href="/sicoobremessa/gerarlote/{{$lote_rps->id_lote_rps}}" target="_blank" title="Gerar os boletos" class="btn btn-sm btn-success"><i class="aweso-barcode"></i></a>
                                    <a href="javascript:confirmar_exclusao('{{$lote_rps->id_lote_rps}}');" title="Excluir lote" class="btn btn-sm btn-danger"><i class="aweso-remove"></i></a>
                                </td>
                                <td style="text-align: center;">{{ $lote_rps->id_lote_rps }}</td>
                                <td style="text-align: center;">{{ $lote_rps->descricao_perfat }}</td>
                                <td style="text-align: center;">{{ $lote_rps->lrps_dt_geracao_fmt }}</td>
                                <td style="text-align: center;">{{ $lote_rps->lrps_dt_emissao_fmt }}</td>
                                <td style="text-align: center;">{{ $lote_rps->qtd_notas }}</td>
                                <!-- <td style="text-align: center;">{{ $lote_rps->lrps_status_extenso }}</td> -->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
</article> <!-- /content page -->
@stop
@section('scripts')
<script>
function confirmar_exclusao(id_lote_rps){
    bootbox.dialog("Confirma a exclusão desse lote? As notas serão mantidas, mas serão marcadas como não emitidas.", [{
        "label" : "Sim, pode excluir",
        "class" : "btn-success",
        "callback": function() {
            excluir(id_lote_rps);        
        }
    }, {
        "label" : "Não! Mantenha o lote",
        "class" : "btn-danger"
    }]);
    
}

function excluir(id_lote_rps){
    let url = "/loterps/excluir";

    $.post(url, {id_lote_rps: id_lote_rps})
        .done(function (data){
            console.log(data);
            let tipo = "";
            if(data.retcode == '0'){
                tipo = "success";
            } else {
                tipo = "error";
            }
            $("#lote"+id_lote_rps).remove();
            $.pnotify({title: "Ok!", text: data.msg, type: tipo});
        });
}



</script>
@stop
