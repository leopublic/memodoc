@extends('stilearn-metro')

@section('conteudo')

@include('cliente_tab_header')

<?php
$atendimento = array(
    '-1' => '(n/a)',
    '0' => '12 horas',
    '1' => '24 horas',
);

?>

<!-- Aba identificação -->
<div class="tab-pane" id="tab_Propriedade">
    {{Form::model($prop) }}
    {{Form::hidden('id_cliente', $cliente->id_cliente)}}
    {{Form::hidden('id_propriedade', $prop->id_propriedade)}}
    <div class="box">
        <ul class="form">
            <li class='subtitle'>
                <h3 CLASS='pull-left'>{{$titulo}}</h3>
                <button class="btn btn-primary pull-right"><i class="icon-white icon-ok-sign"></i> Salvar</button>
            </li>

            <li>
                {{ Form::label('nome','Nome') }}
                {{ Form::text('nome') }}
            </li>

        </ul>

    </div>
    {{ Form::close() }}
</div><!-- Fim Aba identificação -->
@include('cliente_tab_footer')
@stop