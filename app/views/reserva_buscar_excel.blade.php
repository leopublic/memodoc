<head>
<style>
    br {mso-data-placement:same-cell;}
    td{border:solid thin #000; vertical-align: top;}
</style>
</head>
<body>
    <table >
        <thead>
            <tr>
                <th style='width:80px;text-align: center;'>ID</th>
                <th style='width:100px;text-align: center;'>Criada em</th>
                <th style='width:auto;'>Criada por</th>
                <th style='width:120px'>Documento</th>
                <th style='width:50px;text-align: right;'>Qtd</th>
                <th style='width:50px;text-align: center;'>Caixa inicial</th>
                <th style='width:50px;text-align: center;'>Caixa final</th>
                <th style='width:100px;text-align: center;'>Ordem de serviço</th>
                <th style='width:150px;text-align: center;'>Cancelada em</th>
                <th style='width:auto;'>Cancelada por</th>
                <th style='width:auto;'>Observações</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($reservas as $r)
            <tr>
                <td style="text-align: center;">{{substr('000000'.$r->id_reserva, -6)}}</td>
                <td style="text-align: center;">{{$r->formatDate('data_reserva')}}</td>
                <td>
                    @if ($r->nomeusuario!= '')
                    {{$r->nomeusuario}}
                    @else
                    --
                    @endif
                </td>
                <td>{{isset($r->descricao) ? $r->descricao : '' }}</td>
                <td style="text-align: right;">{{$r->caixas}}</td>
                <td style="text-align: center;">{{$r->caixa_inicial}}</td>
                <td style="text-align: center;">{{$r->caixa_final}}</td>
                <td style="text-align: center;">
                    @if ($r->id_os != '')
                    {{substr('000000'.$r->id_os, -6)}}
                    @else
                    --
                    @endif
                </td>
                <td style="text-align: center;">{{$r->data_cancelamento}}</td>
                <td>
                    @if ($r->nomeusuario_cancelamento != '')
                    {{$r->nomeusuario_cancelamento}}
                    @else
                    --
                    @endif
                </td>
                <td style="text-align: left;">{{$r->observacao}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>