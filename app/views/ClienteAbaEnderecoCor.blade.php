@extends('stilearn-metro')

@section('conteudo')

@include('cliente_tab_header')

<?php

$estados = array("AC" => "Acre", "AL" => "Alagoas", "AM" => "Amazonas", "AP" => "Amapá", "BA" => "Bahia", "CE" => "Ceará", "DF" => "Distrito Federal", "ES" => "Espírito Santo", "GO" => "Goiás", "MA" => "Maranhão", "MT" => "Mato Grosso", "MS" => "Mato Grosso do Sul", "MG" => "Minas Gerais", "PA" => "Pará", "PB" => "Paraíba", "PR" => "Paraná", "PE" => "Pernambuco", "PI" => "Piauí", "RJ" => "Rio de Janeiro", "RN" => "Rio Grande do Norte", "RO" => "Rondônia", "RS" => "Rio Grande do Sul", "RR" => "Roraima", "SC" => "Santa Catarina", "SE" => "Sergipe", "SP" => "São Paulo", "TO" => "Tocantins");
?>

<!-- Aba identificação -->
<div class="tab-pane" id="tab_enderecocor">
            {{ Form::model($cliente) }}
            <div class="box">
                <ul class="form">
                    <li class='subtitle'>
                        <h3 CLASS='pull-left'>Endereço de correspondência (cobrança)</h3>
                        <button class="btn btn-primary pull-right"><i class="icon-white icon-ok-sign"></i> Salvar</button>
                    </li>

                    <li>
                        {{ Form::label('logradourocor','Logradouro') }}
                        {{ Form::text('logradourocor') }}
                    </li>
                    <li>
                        {{ Form::label('enderecocor','Endereço') }}
                        {{ Form::text('enderecocor') }}
                    </li>
                    <li>
                        {{ Form::label('numerocor','Número') }}
                        {{ Form::text('numerocor') }}
                    </li>
                    <li>
                        {{ Form::label('complementocor','Complemento') }}
                        {{ Form::text('complementocor') }}
                    </li>
                    <li>
                        {{ Form::label('bairrocor','Bairro') }}
                        {{ Form::text('bairrocor') }}
                    </li>
                    <li>
                        {{ Form::label('cidadecor','Cidade') }}
                        {{ Form::text('cidadecor') }}
                    </li>
                    <li>
                        {{ Form::label('estadocor','Estado') }}
                        {{ Form::select('estadocor', $estados, null, array('data-fx'=>'select2')) }}
                    </li>
                    <li>
                        {{ Form::label('cepcor','CEP') }}
                        {{ Form::text('cepcor') }}
                    </li>

                </ul>

                <div style="width:100%; text-align: center;clear:both">
                    <button type="submit" class="btn btn-primary "><i class="icon-white icon-ok-sign"></i> Salvar</button>
                    <input type="submit" class="btn btn-primary " name="Clonar" value="Copiar do principal" />
                </div>
            </div>
            {{ Form::close() }}

</div><!-- Fim Aba identificação -->
@include('cliente_tab_footer')
@stop
