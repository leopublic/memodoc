<html>
    <body>
        <table>
            <tr>
                <th colspan="19" style="font-weight: bold; font-size:200%; text-align:left;">
                    {{$titulo_tipo}}
                </th>
            </tr>
            <tr>
                <th colspan="19" style="font-weight: bold; font-size:100%; text-align:left;">
                    &nbsp;
                </th>
            </tr>
            <tr>
                <th style="width:auto; border:solid 1px #000;" rowspan="2">Período</th>
                <th style="width:auto; text-align: center;border:solid 1px #000;" colspan="7">Custódia</th>
                <th style="width:auto; text-align: center; border:solid 1px #000;" colspan="6">Atendimento</th>
                <th style="width:auto; text-align: center; border:solid 1px #000;" colspan="6">Totais</th>
            </tr>
            <tr>
                <th style="text-align: right;border:solid 1px #000;">Qtd. Cx. mês ant.</th>
                <th style="text-align: right;border:solid 1px #000;">1&ordf; entr. Cx nova</th>
                <th style="text-align: right;border:solid 1px #000;">Qtd. expurgos<br/>TOTAL</th>
                <th style="text-align: right;border:solid 1px #000;">Qtd. expurgos<br/>que deram entrada no galpão</th>
                <th style="text-align: right;border:solid 1px #000;">Qtd. Total</th>
                <th style="text-align: right;border:solid 1px #000;">Carênc.</th>
                <th style="text-align: right;border:solid 1px #000;">Contrato Mínimo</th>
                <th style="text-align: right;border:solid 1px #000;">Qtd cobrada</th>
                <th style="text-align: right;border:solid 1px #000;">Valor total custódia</th>

                <th style="text-align: right;border:solid 1px #000;">Cartona. qtd</th>
                <th style="text-align: right;border:solid 1px #000;">Cartona. val</th>
                <th style="text-align: right;border:solid 1px #000;">Frete qtd caixas</th>
                <th style="text-align: right;border:solid 1px #000;">Valor frete</th>
                <th style="text-align: right;border:solid 1px #000;">Movim. Qtd cxs</th>
                <th style="text-align: right;border:solid 1px #000;">Movim. valor</th>

                <th style="text-align: right;border:solid 1px #000;">Base<br>ISS</th>
                <th style="text-align: right;border:solid 1px #000;">ISS</th>
                <th style="text-align: right;border:solid 1px #000;">Dif.<br>mín.</th>
                <th style="text-align: right;border:solid 1px #000;">Faturado</th>
            </tr>
            <?php $ano_ant = "" ?>
@foreach($registros as $cliente)
    @if($ano_ant != $cliente->ano)
        @if ($ano_ant != "")
                <tr>
                    <th style="text-align: left; border:solid 1px #000; background-color: #FFFF00;" >TOTALIZADOR {{$cliente->ano}}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($valorminimofaturamento, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_custodia_periodo_cobrado, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($qtd_cartonagem_enviadas, 0, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_cartonagem_enviadas, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($qtd_frete_dem, 0, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_frete, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($qtd_movimentacao, 0, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_movimentacao, 2, ",", ".") }}</th>

                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_iss, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_dif_minimo, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_faturado, 2, ",", ".") }}</th>
                </tr>
                <tr>
                    <th style="text-align: left;width:auto; border:solid 1px #000; background-color: #A6A6A6;" >&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>

                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>

                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                </tr>
        @endif
                    <?php $ano_ant = $cliente->ano; ?>
                    <?php
                        $valorminimofaturamento = 0;
                        $val_custodia_periodo_cobrado = 0;
                        $qtd_cartonagem_enviadas = 0;
                        $val_cartonagem_enviadas = 0;
                        $qtd_frete_dem = 0;
                        $val_frete = 0;
                        $qtd_frete_dem = 0;
                        $qtd_movimentacao = 0;
                        $val_movimentacao = 0;
                        $val_iss = 0;
                        $val_dif_minimo = 0;
                        $val_faturado = 0;
                    ?>
    @endif
                <?php
                    $valorminimofaturamento += $cliente->valorminimofaturamento;
                    $val_custodia_periodo_cobrado += $cliente->val_custodia_periodo_cobrado;
                    $qtd_cartonagem_enviadas += $cliente->qtd_cartonagem_enviadas;
                    $val_cartonagem_enviadas += $cliente->val_cartonagem_enviadas;
                    $qtd_frete_dem += $cliente->qtd_frete_dem;
                    $val_frete += $cliente->val_frete;
                    $qtd_movimentacao += $cliente->qtd_movimentacao;
                    $val_movimentacao += $cliente->val_movimentacao;
                    $val_iss += $cliente->val_iss;
                    $val_dif_minimo += $cliente->val_dif_minimo;
                    $val_faturado += $cliente->val_faturado;
                ?>
            <tr>
                <td style="border:solid 1px #000;">{{ $cliente->periodo }}</td></td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_custodia_mes_anterior, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_custodia_novas, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_expurgos, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_expurgos_usadas, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_custodia_periodo, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->qtd_custodia_em_carencia, 0, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->valorminimofaturamento, 2, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->qtd_custodia_periodo + $cliente->qtd_custodia_em_carencia, 0, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->val_custodia_periodo_cobrado, 2, ",", ".")}}
                </td>

                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->qtd_cartonagem_enviadas, 0, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->val_cartonagem_enviadas, 2, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->qtd_frete_dem, 0, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->val_frete, 2, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->qtd_movimentacao, 0, ",", ".")}}
                </td>
                <td style="text-align: right;border:solid 1px #000;">
                    {{ number_format($cliente->val_movimentacao, 2, ",", ".")}}
                </td>

                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->val_servicos_sem_iss , 2, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->val_iss, 2, ",", ".")}}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->val_dif_minimo, 2, ",", ".") }}</td>
                <td style="text-align: right;border:solid 1px #000;">{{ number_format($cliente->val_faturado, 2, ",", ".")}}</td>
            </tr>
@endforeach
                <tr>
                    <th style="text-align: left; border:solid 1px #000; background-color: #FFFF00;" >TOTALIZADOR {{$cliente->ano}}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($valorminimofaturamento, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_custodia_periodo_cobrado, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($qtd_cartonagem_enviadas, 0, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_cartonagem_enviadas, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($qtd_frete_dem, 0, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_frete, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($qtd_movimentacao, 0, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_movimentacao, 2, ",", ".") }}</th>

                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_iss, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_dif_minimo, 2, ",", ".") }}</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #FFFF00;">{{ number_format($val_faturado, 2, ",", ".") }}</th>
                </tr>
                <tr>
                    <th style="text-align: left;width:auto; border:solid 1px #000; background-color: #A6A6A6;" >&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>

                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>

                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                    <th style="text-align: right;border:solid 1px #000; background-color: #A6A6A6;">&nbsp;</th>
                </tr>
        </table>
    </body>
</html>
