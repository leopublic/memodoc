@extends('stilearn-metro')

@section('conteudo')

@include('cliente_tab_header')

<!-- Aba identificação -->
<div class="tab-pane" id="tab_pagadores">
    {{Form::model($pagador) }}
    {{Form::hidden('id_cliente', $cliente->id_cliente)}}
    {{Form::hidden('id_pagador', $pagador->id_pagador)}}
    <?php
        $estados = array("" => '(não informado)', "AC"=>"Acre", "AL"=>"Alagoas", "AM"=>"Amazonas", "AP"=>"Amapá","BA"=>"Bahia","CE"=>"Ceará","DF"=>"Distrito Federal","ES"=>"Espírito Santo","GO"=>"Goiás","MA"=>"Maranhão","MT"=>"Mato Grosso","MS"=>"Mato Grosso do Sul","MG"=>"Minas Gerais","PA"=>"Pará","PB"=>"Paraíba","PR"=>"Paraná","PE"=>"Pernambuco","PI"=>"Piauí","RJ"=>"Rio de Janeiro","RN"=>"Rio Grande do Norte","RO"=>"Rondônia","RS"=>"Rio Grande do Sul","RR"=>"Roraima","SC"=>"Santa Catarina","SE"=>"Sergipe","SP"=>"São Paulo","TO"=>"Tocantins");
    ?>
    <div class="box">
        <ul class="form">
            <li class='subtitle'>
                <h3 CLASS='pull-left'>{{$titulo}}</h3>
                <button class="btn btn-primary pull-right"><i class="icon-white icon-ok-sign"></i> Salvar</button>
            </li>

            <li>
                {{ Form::label('razaosocial','Razão social') }}
                {{ Form::text('razaosocial') }}
            </li>

            <li>
                {{ Form::label('cnpj','CNPJ') }}
                {{ Form::text('cnpj', null, array( 'data-fx'=>"masked" , 'data-inputmask'=>"'mask': '99.999.999/9999-99'")) }}
            </li>

            <li>
                {{ Form::label('logradouro','Logradouro') }}
                {{ Form::text('logradouro') }}
            </li>

            <li>
                {{ Form::label('numero','Número') }}
                {{ Form::text('numero') }}
            </li>

            <li>
                {{ Form::label('complemento','Complemento') }}
                {{ Form::text('complemento') }}
            </li>

            <li>
                {{ Form::label('bairro','Bairo') }}
                {{ Form::text('bairro') }}
            </li>

            <li>
                {{ Form::label('cidade','Cidade') }}
                {{ Form::text('cidade') }}
            </li>

            <li>
                {{ Form::label('estado','Estado') }}
                {{ Form::select('estado', $estados, null, array('data-fx'=>'select2')) }}
            </li>
            <li>
                {{ Form::label('cep','CEP') }}
                {{ Form::text('cep') }}
            </li>
            <li>
                {{ Form::label('insc_estadual','Inscrição estadual') }}
                {{ Form::text('insc_estadual') }}
            </li>
            <li>
                {{ Form::label('insc_municipal','Inscrição municipal') }}
                {{ Form::text('insc_municipal') }}
            </li>
            <li>
                {{ Form::label('observacao','Observações') }}
                {{ Form::textarea('observacao') }}
            </li>

        </ul>

    </div>
    {{ Form::close() }}
</div><!-- Fim Aba identificação -->
@include('cliente_tab_footer')
@stop
