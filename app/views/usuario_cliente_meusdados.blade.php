@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Atualizar meus dados</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio conteúdo -->
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-user"></i>Complemente os seus dados</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form">
                {{Form::model($model, array('class' => 'form-horizontal'))}}
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->
                        <div class="col-md-12">
                            <div class='form-group'>
                                {{Form::label('username','E-mail (login)',array('class'=>'control-label col-md-3'))}}
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                        {{ $model->username }}                                        
                                    </p>
                                </div>
                            </div>
                            <div class='form-group'>
                                {{Form::label('nomeusuario','Nome',array('class'=>'control-label col-md-3'))}} 
                                <div class="col-md-9">
                                    {{Form::text('nomeusuario', null, array('placeholder'=>'nome do usuário', 'class'=>'form-control fluid','required'))}} 
                                </div>
                            </div>
                            <div class='form-group'>
                                {{Form::label('aniversario','Data de nascimento',array('class'=>'control-label col-md-3'))}} 
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <span class="input-group-addon">dia</span>
                                        {{Form::text('aniversario_dia', null, array('class'=>'form-control input-small'))}} 
                                        <span class="input-group-addon">mês</span>
                                        {{Form::select('aniversario_mes', array('1'=>'Janeiro','2'=>'Fevereiro','3'=>'Março','4'=>'Abril','5'=>'Maio','6'=>'Junho','7'=>'Julho','8'=>'Agosto','9'=>'Setembro','10'=>'Outubro','11'=>'Novembro','12'=>'Dezembro') , null, array('mes'=>'descricao', 'class'=>'form-control input-medium'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- FIM linha CARTONAGEM ADICIONAL -->
                </div>
                <!-- Fim form-body -->
                <!-- INICIO FORM-ACTIONS -->
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-offset-3 col-md-9">
                                <input type="submit" class="btn btn-primary" id="submit-pesquisa" value="Salvar"></>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- FIM FORM-ACTIONS -->
                {{ Form::close() }}
            </div>
            <!-- Fim portlet-body -->
        </div>
        <!-- Fim portlet -->
    </div>
    <!-- fim  da coluna unica -->    
</div>

@stop

@section('scripts')
    @include('_layout.scripts-forms')
@stop