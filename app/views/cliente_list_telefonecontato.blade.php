@extends('stilearn-metro')

@section('conteudo') 

        @include('cliente_tab_header')

        <!-- #tab_contatos - Begin Content  -->
         <div class="tab-pane" id="tab_TelefoneContato">
            <?php
            
               $hasmany = array('title' => 'Contatos',
                                'fields' => array('ddd' => 'DDD',
                                                  'numero'=>'Numero',
                                                  'ramal'=>'Ramal'
                                                 )          
                                ); 
            ?>
            @include('padrao/hasmany_list', $hasmany)
         </div>
        <!-- #tab_contatos - End Content  -->  
        
        @include('cliente_tab_footer')
@stop

