@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>{{$cliente->nomefantasia}}</h1>
            
        </div>

        <!-- content breadcrumb -->
        <ul class="breadcrumb breadcrumb-inline clearfix">
            <li><a href="#">Cadastro</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
            <li class="active">Novo documento</li>
        </ul>
        
        
    </header> <!--/ content header -->
    
    <!-- content page -->
    <article class="content-page clearfix">
        
        <!-- main page -->
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">
    

        <!-- widget form horizontal -->
        <div class="widget border-cyan" id="widget-horizontal">
            <!-- widget header -->
            <div class="widget-header bg-cyan">
                <!-- widget icon -->
                <div class="widget-icon"><i class="aweso-edit"></i></div>
                <!-- widget title -->
                <h4 class="widget-title">Novo Documento</h4>
                <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                <div class="widget-action color-cyan">
                    <button data-toggle="collapse" data-collapse="#widget-horizontal" class="btn">
                        <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                    </button>
                </div>
            </div><!-- /widget header -->
            
                @if (Session::has('msg'))
                <div class="alert alert-info"><h4>Mensagem</h4>{{ Session::get('msg') }}</div>
                @endif
                
                @if (Session::has('success'))
                <div class="alert alert-success"><h4>Sucesso!</h4>{{ Session::get('success') }}</div>
                @endif
                
                @if (Session::has('error'))
                <div class="alert alert-error"><h4>Erro!</h4>{{ Session::get('error') }}</div>
                @endif
                
                
            <!-- widget content -->
            <div class="widget-content">
                {{ Form::model($model, array('class'=>'')) }}
                 <div class='span5 pull-left form-horizontal'>   
                     <div class='control-group'>
                         {{Form::label('id_caixapadrao','Caixa padrão',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_caixapadrao', null, array('placeholder'=>'Caixa padrão', 'class'=>'input-xlarge', 'required'))}} 
                         {{Form::hidden('id_cliente', $cliente->id_cliente)}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_box','Caixa box',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_box', '1', array('placeholder'=>'Caixa box', 'class'=>'input-xlarge','required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_item','Item',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('id_item', null, array('placeholder'=>'Item', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('titulo','Título',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('titulo', null, array('placeholder'=>'Título', 'class'=>'input-xlarge', 'required'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('migrada_de','Migrada de',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::text('migrada_de', null, array('placeholder'=>'Migrada de', 'class'=>'input-xlarge'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_centro','Centro de custo',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::select('id_centro', FormSelect($cliente->centrocusto,'nome'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_departamento','Departamento',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::select('id_departamento', FormSelect($cliente->departamento,'nome'))}} 
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('id_tipodocumento','Tipo de documento',array('class'=>'control-label'))}} 
                         <div class="controls">
                         {{Form::select('id_tipodocumento', $tipodocumento)}} 
                         </div>
                     </div>
                     
                 </div>
               
                <div class='pull-right span7'>
                    <h4>Período que abrange o Item</h4>     
                    <div class="controls">
                        <label>Intervalo cronológico</label>
                        <div class="input-append input-append-inline ">
                            {{Form::text('inicio', null ,array('placeholder'=>"Data início", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                        </div>
                        <div class="input-append input-append-inline ">
                            {{Form::text('fim', null ,array('placeholder'=>"Data Final", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}} 
                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                        </div>
                    </div>
                    
                    <div class="controls">
                        <label>Intervalo numérico</label>
                        <div class="input-append input-append-inline">
                            {{Form::text('nume_inicial', null ,array('placeholder'=>"De", 'class'=>'number'))}} 
                        </div>
                        <div class="input-append input-append-inline">
                            {{Form::text('nume_final', null ,array('placeholder'=>"Até", 'class'=>'number'))}}
                        </div>
                    </div>
                    
                    <div class="controls">
                        <label>Intervalo alfabético</label>
                        <div class="input-append input-append-inline">
                            {{Form::text('alfa_inicial', null ,array('placeholder'=>"De", 'class'=>'alfa'))}}
                        </div>
                        <div class="input-append input-append-inline">
                            {{Form::text('alfa_inicial', null ,array('placeholder'=>"Até", 'class'=>'alfa'))}}
                        </div>
                    </div>
                    <h4>Prazo para expurgo </h4> 
                     <div class='control-group'>
                        
                         <div class="controls">
                             <select name='expurgo_programado' id='expurgo_programado'>
                                 <option value='0'>Sem expurgo</option>
                                 <option value='1'>Expurgo programado</option>
                             </select>
                            <div id='expurgo_programado_data' class="input-append input-append-inline" style='display:none'>
                                {{Form::text('expurgo_programado_data', null ,array('placeholder'=>"Data do expurgo programado", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}} 
                                <span class="add-on"><i class="icomo-calendar"></i> </span>
                            </div>
                             
                         </div>
                         
                         
                     </div>
                    
                </div>
               
                   <div class='clearfix'></div>
                   <div class="form-actions bg-silver">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <button type="reset" class="btn">Cancelar</button>
                   </div>
                </div><!-- /widget content -->
            </div> <!-- /widget form horizontal -->

            {{Form::close()}}



            </div>
        </div>
    </article> <!-- /content page -->  
    
@stop

@section('scripts')
<script>
    $(function(){
       $('#expurgo_programado').change(function(){
           if($(this).val() == '1'){
               $('#expurgo_programado_data').show();
               $('#expurgo_programado_data').find('input').focus();
           }else{
               $('#expurgo_programado_data').hide(); 
               $('#expurgo_programado_data').find('input').val('');
           }
       }); 
    });
</script>
@stop