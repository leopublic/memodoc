@extends('stilearn-metro')

@section('conteudo')

@include('cliente_tab_header')

<?php
$atendimento = array(
    '-1' => '(n/a)',
    '0' => '12 horas',
    '1' => '24 horas',
);

?>

<!-- Aba identificação -->
<div class="tab-pane" id="tab_EnderecoEntrega">
            {{ Form::model($endeentrega) }}
            {{Form::hidden('id_cliente', $cliente->id_cliente)}}
            {{Form::hidden('id_endeentrega', $endeentrega->id_endeentrega)}}
            <div class="box">
                <ul class="form">
                    <li class='subtitle'>
                        <h3 CLASS='pull-left'>{{$titulo}}</h3>
                        <button class="btn btn-primary pull-right"><i class="icon-white icon-ok-sign"></i> Salvar</button>
                    </li>

                    <li>
                        {{ Form::label('logradouro','Logradouro') }}
                        {{ Form::text('logradouro') }}
                    </li>
                    <li>
                        {{ Form::label('endereco','Endereço') }}
                        {{ Form::text('endereco') }}
                    </li>
                    <li>
                        {{ Form::label('numero','Número') }}
                        {{ Form::text('numero') }}
                    </li>
                    <li>
                        {{ Form::label('complemento','Complemento') }}
                        {{ Form::text('complemento') }}
                    </li>
                    <li>
                        {{ Form::label('bairro','Bairro') }}
                        {{ Form::text('bairro') }}
                    </li>
                    <li>
                        {{ Form::label('cidade','Cidade') }}
                        {{ Form::text('cidade') }}
                    </li>
                    <li>
                        {{ Form::label('estado','Estado') }}
                        {{ Form::select('estado', $estados, null, array('data-fx'=>'select2')) }}
                    </li>
                    <li>
                        {{ Form::label('cep','CEP') }}
                        {{ Form::text('cep') }}
                    </li>

                </ul>

                <div style="width:100%; text-align: center;clear:both">
                    {{ Form::submit('Salvar') }}
                </div>
            </div>
            {{ Form::close() }}

</div><!-- Fim Aba identificação -->
@include('cliente_tab_footer')
@stop
