@extends('stilearn-metro')

@section('conteudo')

        @include('cliente_tab_header')

        <!-- #tab_servicos - Begin Content  -->
         <div class="tab-pane" id="tab_pagadores">
             <div class="tab listbox">
                <h3 class="pull-left">Pagadores</h3>
                <a class='pull-right btn btn-info' href='/cliente/pagadoreseditar/{{$cliente->id_cliente}}/0#tab_pagadores'><i class="icon-plus-sign"></i> Novo pagador</a>
                <div class='clearfix'></div>
                <table class='table table-hover table-striped'>
                    <thead>
                        <tr>
                            <th style="width:20px;text-align:center">#</th>
                            <th style="width:80px;text-align:center">Ações</th>
                            <th style="width:auto;">Razão social</th>
                            <th style="text-align:center; width: 150px;">CNPJ</th>
                            <th style="text-align:left; width: 300px;">Logradouro</th>
                            <th style="text-align:center; width: 100px;">Número</th>
                            <th style="text-align:center; with:auto;">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($pagadores as $pag)
                        <tr>
                            <td style="text-align:center;"> {{$i}}</td>
                            <td style="text-align:center;">
                                <a title='Editar' href="/cliente/pagadoresedit/{{$cliente->id_cliente}}/{{$pag->id_pagador}}#tab_pagador" class="btn btn-mini btn-primary" title="Editar"><i class="icon-white icon-edit"></i></a>
                                <div class="btn-group">
                                    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-danger btn-mini" title="Excluir esse item"><i class="icon-white icon-remove-sign"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a title='Deletar' href="/cliente/pagadordel/{{$cliente->id_cliente}}/{{$pag->id_pagador}}" class="btn btn-danger" title="Confirmação">Clique aqui para confirmar a exclusão</a></li>
                                    </ul>
                                </div>
                            </td>
                           <td>{{$pag->razaosocial}}</td>
                           <td style="text-align:center;">{{$pag->cnpj_formatado}}</td>
                           <td style="text-align:left;">{{$pag->logradouro}}</td>
                           <td  style="text-align:center;">{{$pag->numero}}</td>
                           <td style="text-align:center;">&nbsp;</td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                    </tbody>
                </table>

             </div>
         </div>
        <!-- #tab_servicos - End Content  -->

        @include('cliente_tab_footer')
@stop
