@extends('stilearn-metro')

@section('conteudo') 
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Total de buracos disponíveis: {{ $total }}</h1></div>
    <ul class="content-action pull-right">
        <li><a class="btn bg-lime " href="{{URL::to('/galpao/buracosdisponiveisexcel/')}}" target="_blank"><i class="aweso-table"></i> Excel</a></li>
    </ul>
</header> <!--/ content header -->

<!-- start section content-->
<article class="content-page clearfix">
    <!-- main page, you're application here -->
    <div class="main-page">
        <div class="content-inner">
            <!-- row #1 -->
            <div id="row1" class="row-fluid">
                <!-- span6 -->
                <div class="span12">
                    <!-- widget tdefault -->
                    <div class="widget border-cyan" id="widget-tdefault">
                        <!-- widget header -->
                        <div class="widget-header bg-cyan">
                            <!-- widget icon -->
                            <div class="widget-icon"><i class="aweso-table"></i></div>
                            <!-- widget title -->
                            <h4 class="widget-title">Buracos por cliente</h4>
                            <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        </div><!-- /widget header -->

                        <!-- widget content -->
                        <div class="widget-content">
                            @include ('buracos_disponivel_tabela')
                        </div><!-- /widget content -->
                    </div> <!-- /widget tdefault -->
                </div><!-- span6 -->

                <!-- /out of widget demo -->

            </div><!-- /content-inner-->
        </div><!-- /main-page-->

</article> <!-- /content page -->

@stop