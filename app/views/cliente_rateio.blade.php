@extends('stilearn-metro')

@section("styles")
table tr.total td{
    font-weight:bold;
    padding-top:10px;
    border-top-width:2px;
}
@stop

@section('conteudo') 

    @include('cliente_tab_header')

    <!-- #tab_centrocusto - Begin Content  -->
    <div class="tab-pane" id="tab_Rateio">
        @include('caixa/tabela_rateio')
<!-- content page -->

		<div class="row-fluid">
			<div class="span12">
				<a href="/caixa/rateioxls/{{$cliente->id_cliente}}" target="_blank" class="btn btn-success"><i class="aweso-download-alt"></i> Excel</a>
			</div>
		</div>
    </div>

     
@stop