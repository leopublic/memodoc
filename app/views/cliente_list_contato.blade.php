@extends('stilearn-metro')

@section('conteudo') 

        @include('cliente_tab_header')

        <!-- #tab_contatos - Begin Content  -->
         <div class="tab-pane" id="tab_Contato">
            <?php
            
               $hasmany = array('title' => 'Contatos',
                                'fields' => array('nome' => 'Nome',
                                                  'cargo'=>'Cargo',
                                                  
                                                 )          
                                ); 
            ?>
            @include('padrao/hasmany_list', $hasmany)
         </div>
        <!-- #tab_contatos - End Content  -->  
        
        @include('cliente_tab_footer')
@stop

