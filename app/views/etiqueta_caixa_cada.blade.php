<?php 
$border_int = 'white';
if(array_key_exists('border', $especs) && $especs['border'] == 1){
	$border_int = "green";
}
?>
<tr style="border: solid 1px black;">
	<td style="padding: 0px; margin: 0px; text-align: center; width:{{ $especs['largura_td_barras'] }}px; border: solid 1px {{ $border_int }};">
		<barcode code="{{ $xetiqueta['id'] }}" type="C39" class="barcode"  size="{{ $especs['barcode_size'] }}" height="{{ $especs['barcode_height'] }}" pr="{{ $especs['barcode_pr'] }}"/>
		<br/><span style="font-weight: bold; font-family:arial; font-size: {{ $especs['fontsize_id'] }};">ID: {{ $xetiqueta['id'] }}</span>
	</td>
	<td style="font-weight:bold; padding:0; margin:0; vertical-align: top;  border: solid 1px {{ $border_int }};">
		<table style="border-spacing: 0px; font-size:{{ $especs['fontsize_titulo'] }}px; font-weight:bold; padding:0;  border: solid 1px {{ $border_int }};">
			<tr>
				<td style="padding:0; margin:0; line-height:88%; letter-spacing: {{$especs['letter_spacing_titulo'] }}">{{ $xetiqueta['numero'] }}</td>
			</tr>
		</table>
		<table style="border-spacing: 0px; padding: 0px; margin:0px; font-size:{{ $especs['fontsize_texto']}}px; font-weight: bold ;font-family: arial; margin-top: -5px; width: 100%;  border: solid 1px {{$border_int}};">
			<tr>
				<td style="padding: 0px; margin:0px;">{{ $xetiqueta['tipo'] }}&nbsp;&nbsp;&nbsp;&nbsp;Reserva: {{ $xetiqueta['reserva'] }}&nbsp;&nbsp;Migr.:{{$xetiqueta['migrada_de']}}</td>
			</tr>
			<tr>
				<td style="padding: 0px; margin:0px;">{{ $xetiqueta['cliente'] }}</td>
			</tr>
			<tr>
				<td style="text-align: right; padding: 0px; margin:0px;"><span style="font-size:{{ $especs['fontsize_id'] }}px;">ID:{{ $xetiqueta['id'] }}</span></td>
			</tr>
			<tr>
				<td><span style="padding: 0px; margin:0px; text-align: right; font-size:{{ $especs['fontsize_endereco'] }}px;">{{ $xetiqueta['endereco'] }}</span></td>
			</tr>
		</table>
	</td>
</tr>
