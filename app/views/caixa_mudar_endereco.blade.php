@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Mover caixas para um novo endereço</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-share-alt"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Informe o ID das caixas a serem movidas ({{$qtd_disp}} vagas disponíveis para movimentação no(s) galpõe(s) {{$galpoes}})</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                     @if (isset($id_caixas))
                        @foreach($id_caixas as $i => $id_caixa)
                        <input type="hidden" name="id_caixas[]" value="{{$id_caixa}}"/>
                        @endforeach
                    @endif
                    <div class='control-group'>
                        {{Form::label('fl_cancelar_endereco','Cancelar endereço original?',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::checkbox('fl_cancelar_endereco', 1, Input::old('fl_cancelar_endereco'), array('class' => 'input-block-level'))}}
                                <span class="help-block">Com essa opção marcada, o endereço atual da(s) caixa(s) será inutilizado</span>
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('observacao','Observações',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::textarea('observacao', Input::old('observacao'), array('rows' => 4, 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        {{Form::label('id_caixa','ID',array('class'=>'control-label'))}}
                        <div class="controls">
                                {{Form::text('id_caixa', '' ,array('class'=>'input-block-level'))}}
                                <span class="help-block">Se precisar, informe vários IDs separados por vírgula</span>
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button name="adicionar" class="btn btn-primary" id="adicionar" ><i class="aweso-plus"></i> Adicionar caixas</button>
                        @if (isset($caixas))
                        <div class="btn-group">
                            <a href="#" id="buttonSubmit" data-toggle="dropdown" class="btn dropdown-toggle bg-orange" title="Realiza a movimentação"><i class="aweso-share-alt"></i> Efetivar</a>
                            <ul class="dropdown-menu">
                                <li><input type="submit" name="efetivar" class="btn bg-orange" value="Clique aqui para efetivar a movimentação. ATENÇÃO ESSA OPERAÇÃO NÃO PODE SER DESFEITA!!" /></li>
                            </ul>
                        </div>
                        @endif
                    </div>
                    {{ Form::close() }}
                    @if (isset($caixas))
                    <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                        <thead>
                            <th style="width:50px; text-align:center;">#</th>
                            <th style="width:80px; text-align:center;">Ação</th>
                            <th style="width:auto; text-align:left;">Cliente</th>
                            <th style="width:auto; text-align:center;">ID</th>
                            <th style="width:auto; text-align:center;">Referência</th>
                            <th style="width:auto; text-align:center;">Endereço</th>
                            <th style="width:auto;">&nbsp;</th>
                        </thead>
                            <? $i = 1;?>
                        <tbody>
                            @foreach($caixas as $caixa)
                            <tr class="" id="tr{{$caixa->id_caixa}}">
                                <td style="text-align: center;">{{$i}}</td>
                                <td style="text-align: center;"><button onClick="removeCaixa('{{$caixa->id_caixa}}');" title="Cancelar movimentação dessa caixa" class="btn btn-danger"><i class="icon-trash"></i></button></td>
                                <td style="text-align: left;">{{$caixa->cliente->razaosocial.' ('.$caixa->id_cliente.')'}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa, -6)}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao, -6)}}</td>
                                <td style="text-align: center;">{{$caixa->enderecoCompleto()}}</td>
                                <td style="width:auto;">&nbsp;</td>
                            </tr>
                            <? $i++;?>
                            @endforeach

                        </tbody>
                    </table>
                    @endif

                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop

@section('scripts')
<script>
function removeCaixa(id_caixa){
    $('input[name="id_caixas[]"]').each(function() {
        if ($(this).val() == id_caixa){
            $(this).remove();
            $('#tr'+id_caixa).remove();
        }
    });

}

</script>
@stop
