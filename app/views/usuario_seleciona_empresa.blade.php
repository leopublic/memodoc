@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Selecionar o cliente</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-user"></i>Indique o cliente</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form">
                {{Form::open(array('id'=>'frmdocumento', 'class' => 'form-horizontal'))}}
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->
                        <div class="col-md-12">
                            <div class='form-group'>
                                {{Form::label('id_cliente','Cliente',array('class'=>'control-label col-md-3'))}}
                                <div class="col-md-9">
                                    {{Form::select('id_cliente', $clientes, Auth::user()->id_cliente, array( 'class' => 'form-control select2me', 'id'=>'id_cliente','required'))}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-offset-3 col-md-9">
                                <input type="submit" class="btn btn-primary" value="Entrar no cliente selecionado"></>
                            </div>

                        </div>
                    </div>
                </div>
        {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
    @include('_layout.scripts-forms')
@stop
