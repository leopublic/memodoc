<!-- Un-collapse nav -->
<div class="nav-uncollapse menu">
    <!-- pull left menu-->
    <ul class="nav pull-left ">
        @if (Auth::user()->nivel->minimoAtendimentoNivel2())
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="aweso-gears aweso-large"></i> &nbsp;Cadastros <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
            @if (Auth::user()->nivel->minimoAdministrador())
            	<li><a href="{{ URL::to('aviso/home/1') }}">Aviso da Home-page</a></li>
            @endif
            	<li><a href="{{ URL::to('cliente') }}">Clientes</a></li>
                <li><a href="{{ URL::to('diavencimento') }}">Dias de faturamento</a></li>
                <li><a href="{{ URL::to('nivel') }}">Níveis de acesso</a></li>
                <li><a href="{{ URL::to('tipodocumento') }}">Tipos de documento</a></li>
                <li><a href="{{ URL::to('tipoemail') }}">Tipos de email</a></li>
                <li><a href="{{ URL::to('tipoproduto') }}">Tipos de produto</a></li>
                <li><a href="{{ URL::to('tipotelefone') }}">Tipos de telefone</a></li>
                <li><a href="{{ URL::to('usuario/externos') }}">Usuários externos</a></li>
                <li><a href="{{ URL::to('usuario/internos') }}">Usuários internos</a></li>
            </ul>
        </li>
        @endif
        @if (Auth::user()->nivel->minimoAtendimentoNivel2())
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="aweso-laptop aweso-large"></i> &nbsp;Atendimento <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                <li ><a href="{{ URL::to('documento/cadastrar') }}">Inventário</a></li>
                <li class="inativo"><a href="{{ URL::to('documento/buscar') }}">Localizar documentos</a></li>
                <li class="inativo"><a href="{{ URL::to('documento/buscarinativos') }}">Localizar documentos (inativos)</a></li>
                <li class="inativo"><a href="{{ URL::to('documento/acervocliente/pdf') }}">Gerar acervo para impressão</a></li>
                <li class="inativo"><a href="{{ URL::to('documento/acervocliente/pdf') }}">Gerar acervo em PDF</a></li>
            @if (Auth::user()->nivel->minimoAdministrador())
                <li class="inativo"><a href="{{ URL::to('documento/acervocliente/xls') }}">Gerar acervo em Excel</a></li>
            @endif
                <li class="divider"></li>
                    <li class="inativo"><a href="{{ URL::to(\Memodoc\Servicos\Rotas::ordemservico().'/listar') }}">Listar OS</a></li>
                    <li class="inativo"><a href="{{ URL::to(\Memodoc\Servicos\Rotas::ordemservico().'/listarinativos') }}">Listar OS (inativos)</a></li>
                    <li class="inativo"><a href="{{ URL::to(\Memodoc\Servicos\Rotas::ordemservico().'/cliente') }}">Abrir nova solicitação</a></li>
                    <li class="inativo"><a href="{{ URL::to(\Memodoc\Servicos\Rotas::ordemservico().'/localizarpornumero') }}">Localizar por número</a></li>
                    <li class="inativo"><a href="{{ URL::to('caixa/historico') }}">Consultar movimentação de documento por ordem de serviço</a></li>
                <li class="divider"></li>
<!--                    <li class="inativo"><a href="/expurgo/cliente">Expurgar caixas</a></li>-->
                    <li class="inativo"><a href="/expurgo/caixasexpurgadas">Consultar caixas expurgadas</a></li>
                    <li class="inativo"><a href="/expurgo/caixasexpurgadas/1">Consultar caixas expurgadas (inativos)</a></li>
                    <li class="inativo"><a href="/expurgo/pai">Consultar expurgos</a></li>
                    <li class="inativo"><a href="/expurgo/paiinativo">Consultar expurgos (inativos)</a></li>
                <li class="divider"></li>
                <li ><a href="{{ URL::to('etiqueta/reimprimir') }}">Imprimir etiquetas</a></li>
                <li ><a href="{{ URL::to('etiqueta/cancelar') }}">Cancelar etiquetas</a></li>
                <li class="divider"></li>
                <li ><a href="{{ URL::to('reserva/buscar') }}">Listar reservas </a>
                <li ><a href="{{ URL::to('reserva/inventarioavulso') }}">Reimprimir planilhas</a>
                </li>
            </ul>
        </li>
        @endif

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="aweso-truck aweso-large"></i> &nbsp;Operacional <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                <li class="inativo"><a href="/checkinxx/listar/{{str_random(40)}}">Check-in</a></li>
                <li class="inativo"><a href="/checkout">Check-out</a></li>
                <li class="inativo"><a href="/checkout/expurgos">Check-out de expurgos</a></li>
                <li class="inativo"><a href="/transbordo">Transbordo</a></li>
                <li class="inativo"><a href="/caixa/listarmovimentacoes">Mudar caixa de endereço</a></li>
                <li class="divider"></li>
                <li class="inativo"><a href="/remessa">Remessas importadas</a></li>
            </ul>
        </li>

        @if (Auth::user()->nivel->minimoAtendimentoNivel2())
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="aweso-usd aweso-large"></i> &nbsp;Financeiro <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                <li class="inativo"><a href="{{ URL::to('/periodofat') }}">Faturamentos</a></li>
                <li class="inativo"><a href="{{ URL::to('/faturamento/porcliente') }}">Faturamentos por cliente</a></li>
                <li class="inativo"><a href="{{ URL::to('/faturamento/previa') }}">Previsão de Faturamento</a></li>
                <li class="inativo"><a href="{{ URL::to('/periodofat/sinteticogeralexcel') }}">Totalizador Geral</a></li>
                <li class="divider"></li>
                <li class="inativo"><a href="/envelopecobr">Etiqueta envelope cobrança</a></li>
                <li class="inativo"><a href="/tipoproduto/periodo">Relação de clientes para reajuste contratual</a></li>
                <li class="divider"></li>
                <li class="inativo"><a href="{{ URL::to('cliente/excel') }}">Conferência de clientes</a></li>
                <li class="inativo"><a href="/cliente/conferencia/franquiacustodia">Conf. Franquia custódia</a></li>
                <li class="inativo"><a href="/cliente/conferencia/franquiamovimentacao">Conf. Franquia movimentação %</a></li>
                <li class="inativo"><a href="/cliente/conferencia/descontocustodiapercentual">Conf. Desconto custódia %</a></li>
                <li class="inativo"><a href="/cliente/conferencia/descontocustodialimite">Conf. Limite de caixas para desconto de custódia</a></li>
                <li class="divider"></li>
                <li class="inativo"><a href="/loterps">Nota Carioca - Emitir</a></li>
                <li class="inativo"><a href="/notacarioca/retorno">Nota carioca - Carregar emitidas</a></li>
                <li class="inativo"><a href="/sicoobretorno/">SICOOB - Carregar retorno de boletos</a></li>
                <li class="divider"></li>
                <li class="inativo"><a href="/cliente/comparavalores/1">Comparativo FRETE</a></li>
                <li class="inativo"><a href="/cliente/comparavalores/2">Comparativo CARTONAGEM</a></li>
                <li class="inativo"><a href="/cliente/comparavalores/3">Comparativo ARMAZENAMENTO</a></li>
                <li class="inativo"><a href="/cliente/comparavalores/4">Comparativo MOVIMENTAÇÃO</a></li>
                <li class="inativo"><a href="/cliente/comparavalores/5">Comparativo EXPURGO</a></li>
            </ul>
        </li>
        @endif

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="aweso-building aweso-large"></i> &nbsp;Galpão <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                <li class="inativo"><a href="{{ URL::to('galpao') }}">Galpões</a></li>
                <li class="inativo"><a href="{{ URL::to('endereco/conteudoandar') }}">Lista o conteúdo de um andar</a></li>
                <li class="inativo"><a href="{{ URL::to('endereco/conteudo') }}">Lista o conteúdo de um endereço</a></li>
            @if (Auth::user()->nivel->minimoAdministrador())
                <li class="inativo"><a href="{{ URL::to('endereco/novo') }}">Definição de endereços</a></li>
                <li class="inativo"><a href="#">Alteração de particularidades de endereços</a></li>
                <li class="inativo"><a href="{{ URL::to('caixa/movimentar') }}">Troca de endereços</a></li>
            @endif
                <li class="inativo"><a href="{{ URL::to('galpao/vagasdisponiveis') }}">Total de vagas disponíveis no galpão</a></li>
                <li class="inativo"><a href="{{ URL::to('galpao/buracosdisponiveis') }}">Total de buracos</a></li>
            </ul>
        </li>
        @if (Auth::user()->nivel->minimoAdministrador())
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="aweso-bar-chart aweso-large"></i> &nbsp;Gerencial <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                <li class="inativo"><a href="{{ URL::to('cliente/excel') }}">Conferência de clientes</a></li>
                <li class="divider"></li>
                <li class="inativo"><a href="{{ URL::to('gerencial/situacaocaixas') }}">Caixas por situação</a></li>
                <li class="inativo"><a href="{{ URL::to('gerencial/situacaoinadimplentes') }}">Situação inadimplentes</a></li>
                <li class="inativo"><a href="{{ URL::to('gerencial/franquia') }}">Monitoramento da franquia</a></li>
                <li class="divider"></li>
                <li class="inativo"><a href="/retorno/novo">Adicionar arquivo de retorno</a></li>
                <li class="inativo"><a href="/retorno/pesquisar">Consultar cobrança</a></li>
                <li class="inativo"><a href="/retorno/">Consultar arquivos de retorno</a></li>
            </ul>
        </li>
        @endif

    </ul><!--/pull left menu-->

    <!-- pull right menu-->
    <ul class="nav pull-right">

        <!-- account -->
        @if (Auth::user()->nivel->minimoAtendimentoNivel2())
        <li class="dropdown bg-orange bold">
            <a href="{{URL::to(\Memodoc\Servicos\Rotas::ordemservico().'/listar/2')}}" class="dropdown-toggle">
                <span style="font-size:14px;font-weight:bold;">{{ \Memodoc\Repositorios\RepositorioOsEloquent::qtdeOsAbertas() }}</span> OS novas
                <i class="aweso-exclamation"></i>
            </a>
        </li><!-- /account -->
        @endif
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                {{Auth::user()->nomeusuario}} <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                <li><a tabindex="-1" href="/usuario/alterarsenha"><i class="aweso-lock"></i> Alterar senha</a></li>
                <li class="divider"></li>
                <li><a tabindex="-1" href="/auth/logout"><i class="aweso-power-off"></i> Sair</a></li>
            </ul>
        </li><!-- /account -->
    </ul><!--/pull right menu-->
</div><!-- /uncollapse nav -->
