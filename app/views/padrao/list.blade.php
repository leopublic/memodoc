<div class="row tab">
    <div class="slot-0-1-2-3-4-5">
        <div class="box">
            <div class='form'>
                <div class='subtitle' style="text-align: left;">
                   {{$title}} <span class="badge badge-info">{{$model->count()}}</span>
                    <a href="/{{$route_add}}" class="btn btn-small pull-right"><i class="icon-plus-sign"></i> Novo </a>
                </div>
            </div>
           <table class='table table-striped table-hover'>
               <thead>
                   @foreach($fields as $col => $fieldname)
                     <th> {{$fieldname}}</th>
                   @endforeach
                  <th style="width:80px;text-align:center">#</th>
               </thead>
               <tbody>
                   @foreach($model as $data)
                      <tr>
                           @foreach($fields as $col => $field)
                                <td> {{$data[$col]}}</td>
                           @endforeach
                          <td> 
                              <a title='Editar' href="/{{$route_edit}}/{{$data->getKey()}}" class="btn btn-mini ajaxForm"><i class="icon-white icon-edit"></i></a>
                              <a title='Deletar' href="/{{$route_delete}}/{{$data->getKey()}}" class="btn btn-mini"><i class="icon-white icon-remove-sign"></i></a>
                          </td>
                      </tr>
                   @endforeach
               </tbody>
           </table>

        </div>
    </div>
</div>       