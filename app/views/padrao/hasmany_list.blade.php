<div class="tab">
    <div class="listbox">
        <div class="box">
            <div class='form clearfix'>
                <div class='subtitle' style="text-align: left;">
                    <h3 class="pull-left"> {{$title}} <span class="badge badge-info">{{$model->count()}}</span></h3> 
                    <a href="/{{$route_add}}" class="btn pull-right ajaxForm"><i class="icon-plus-sign"></i> Novo </a>
                </div>
            </div>
           <table class='table table-striped table-hover'>
               <thead>
                  <th style="width:20px;text-align:center">#</th>
                  <th style="width:80px;text-align:center">Ações</th>
                   @foreach($fields as $col => $fieldname)
                     <th> {{$fieldname}}</th>
                   @endforeach
               </thead>
               <tbody>
                   <? $i = 0;?>
                   @foreach($model as $data)
                    <? $i++; ?>
                      <tr>
                          <td style="text-align:center;"> {{$i}}</td>
                          <td style="text-align:center;"> 
                              <a title='Editar' href="/{{$route_edit}}/{{$data->getKey()}}" class="btn btn-mini btn-primary ajaxForm" title="Editar"><i class="icon-white icon-edit"></i></a>
                              <div class="btn-group">
                                  <a href="#" data-toggle="dropdown" class="btn dropdown-toggle btn-danger btn-mini" title="Excluir esse item"><i class="icon-white icon-remove-sign"></i></a>
                                  <ul class="dropdown-menu">
                                      <li><a title='Deletar' href="/{{$route_delete}}/{{$data->getKey()}}" class="btn btn-danger" title="Confirmação">Clique aqui para confirmar a exclusão</a></li>
                                  </ul>
                              </div>
                          </td>
                           @foreach($fields as $col => $field)
                                <td> {{$data[$col]}}</td>
                           @endforeach
                      </tr>
                   @endforeach
               </tbody>
           </table>

        </div>
    </div>
</div>       