<!-- mensagens -->
@if (Session::has('msg') || Session::has('success') || Session::has('errors'))
<div class="row-fluid">
    @if (Session::has('msg'))
    <div class="alert alert-info"><h4>Mensagem</h4>{{ Session::get('msg') }}</div>
        <? Session::remove('msg'); ?>
    @endif

    @if (Session::has('success'))
    <div class="alert alert-success"><h4>Sucesso!</h4>{{ Session::get('success') }}</div>
        <? Session::remove('success'); ?>
    @endif

    @if (Session::has('errors'))
    <div class="alert alert-error">
    	<h4>Erro!</h4>
    	@if(is_object(Session::get('errors')) && get_class(Session::get('errors')) == 'Illuminate\Support\MessageBag')
            <? $br = ''; ?>
            @foreach(Session::get('errors')->all() as $error)
                    {{ $br.$error }}
            <? $br = '<br/>'; ?>
            @endforeach
    	@elseif(is_array(Session::get('errors')))
            <? $br = ''; ?>
            @foreach(Session::get('errors') as $error)
                    {{ $br.$error }}
            <? $br = '<br/>'; ?>
            @endforeach
        @else
                {{ Session::get('errors') }}
        @endif
    </div>
        <? Session::remove('errors'); ?>
    @endif
</div>
@endif
<!-- /mensagens -->