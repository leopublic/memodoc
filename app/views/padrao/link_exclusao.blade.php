<? isset($title) ? null : $title=''; ?>
<? isset($class) ? null : $class='btn-sm'; ?>
<? isset($icon) ? null : $icon='aweso-trash'; ?>
<? isset($cor) ? null : $cor='btn-danger'; ?>
<div class="btn-group">
    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle {{$cor}} {{$class}}" title="{{$title}}"><i class="{{$icon}}"></i></a>
    <ul class="dropdown-menu">
        @if (isset($url))
        <li><a href="{{ URL::to($url) }}">{{ $msg_confirmacao }}</a></li>
        @elseif (isset($onclick))
        <li><a href="#" title="{{$title}}" onclick="{{$onclick}}">{{ $msg_confirmacao }}</a></li>
        @endif
    </ul>
</div>

