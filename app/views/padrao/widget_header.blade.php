<!-- widget header -->
    <div class="widget-header bg-{{{ isset($cor) ? $cor : 'cyan' }}}">
        <div class="widget-icon">{{ $icone }}</div>
        <h4 class="widget-title">{{ $titulo }}</h4>
        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
        <div class="widget-action color-cyan">
        @if(isset($collapse))
            <button data-toggle="collapse" data-collapse="#widget-horizontal" class="btn">
                <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
            </button>
        @endif
        @if(isset($botoes))
            @foreach($botoes as $botao)
                {{ $botao }}
            @endforeach
        @endif
        </div>
    </div>
<!-- /widget header -->
