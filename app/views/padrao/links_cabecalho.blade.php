<!-- links_cabecalho -->
<ul class="content-action pull-right">
    @foreach($menus as $menu))
    <li>
        <div class="btn-group">
            <a class="btn btn-link dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="{{ $menu->icon }}"></i> {{ $menu->nome }} <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu cyan pull-right">
        @foreach($menu->links as $link)
                <li><a href="{{ $link->url }}" title="{{ $link->title }}" target="{{ $link->target }}">{{ $link->nome }}</a></li>
        @endforeach
            </ul>
        </div>
    </li>
    @endforeach
    <li>
        <div class="btn-group">
            <a class="btn btn-link dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="aweso-print"></i> Relatórios <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu cyan pull-right" role="menu" aria-labelledby="dropdownMenu">
                <li><a href="/ordemservico/relatorio/{{ $model->id_os_chave }}" target="_blank">Capa</a></li>
                <li><a href="/ordemservico/relacaodecaixas/{{ $model->id_os_chave }}" target="_blank" >Caixas para consulta</a></li>
                <li><a href="/ordemservico/relacaodecaixasoperacional/{{ $model->id_os_chave }}" target="_blank">Caixas para operacional</a></li>
            </ul>
        </div>
    </li>
</ul>
<!-- /links_cabecalho -->