             <div class="row tab">
                 <div class="slot-0-1-2-3-4-5">
                     {{ Form::model($many) }}
                     <div class="box">
                         <ul class="form"> 
                             <li class='subtitle'>
                                {{$title}}
                                 <button class="btn btn-primary btn-mini pull-right"><i class="icon-white icon-ok-sign"></i> Salvar</button>
                             </li>

                             @foreach($many->getOriginal() as $col => $field)
                             
                             <li>
                                 
                                 {{Form::label($col) }}
                                 {{Form::text($col) }}
                             </li>
                             @endforeach


                         </ul>    

                         <div style="width:100%; text-align: center;clear:both">
                                 {{ Form::submit('Salvar') }}								
                         </div>
                     </div>
                     {{ Form::close() }}
                 </div>
             </div>