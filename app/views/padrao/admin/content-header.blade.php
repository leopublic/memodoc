<!-- content header -->
<header class="content-header">
    <div class="page-header">
        <h1>{{ $titulo }}</h1>
    </div>
    @if(isset($breadcrumb))
	    <!-- content breadcrumb -->
	    <ul class="breadcrumb breadcrumb-inline clearfix">
    	@if(is_array($breadcrumb))
    		@foreach($breacrumb as $link)
	        	<li>{{ $link }}<span class="divider"><i class="aweso-angle-right"></i></span></li>
	        @endforeach
	    @else
	        <li>{{ $breadcrumb }}<span class="divider"><i class="aweso-angle-right"></i></span></li>
	    @endif
		</ul>
	    <!-- /content breadcrumb -->
	@endif
</header>
<!--/ content header -->
