@if(isset($menus))
<ul class="content-action pull-right">
    @foreach($menus as $menu)
    <li>
        <div class="btn-group">
            <a class="btn silver dropdown-toggle {{ $menu->cor() }}" data-toggle="dropdown" href="#">
                    @if($menu->icon() != '')
                        <i class="{{ $menu->icon() }}"></i>
                    @endif
                    {{ $menu->nome() }}</a>
            <ul class="dropdown-menu pull-right cyan">
            @foreach($menu->links() as $link)
                <li><a href="{{ $link->url() }}" target="{{ $link->target() }}" title="{{ $link->title() }}">{{ $link->nome() }}</a></li>
            @endforeach
            </ul>
        </div>
    </li>
    @endforeach
</ul>
@endif