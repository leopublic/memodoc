<!-- Un-collapse nav -->
<div class="nav-uncollapse menu">
    <!-- pull left menu-->
    <ul class="nav pull-left">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="aweso-archive aweso-large"></i>&nbsp;Documentos <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                <li class="inativo"><a href="{{ URL::to('publico/documento') }}">Pesquisar conteúdo</a></li>
                @if(Auth::user()->id_nivel <= 2)
                <li><a href="{{ URL::to('publico/documento/cadastrar') }}">Cadastrar documentos</a></li>
                @endif
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="aweso-gears aweso-large"></i> &nbsp;Cadastros <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                <li><a href="{{ URL::to('publico/centrodecusto') }}">Centros de custo</a></li>
                <li><a href="{{ URL::to('publico/departamento') }}">Departamentos</a></li>
                <li><a href="{{ URL::to('publico/setor') }}">Setores</a></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="aweso-truck aweso-large"></i> &nbsp;Solicitações <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                @if(Auth::user()->id_nivel <= 2)
                <li class="inativo"><a href="{{ URL::to('publico/os/encerrarpendente') }}">Encerrar solicitação em aberto</a></li>
                <li class="inativo"><a href="{{ URL::to('publico/os/criar') }}">Abrir solicitação</a></li>
                @endif
                <li class="inativo"><a href="{{ URL::to('publico/os') }}">Consultar solicitações</a></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="aweso-key aweso-large"></i> &nbsp;Senha/Usuários <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                <li class="inativo"><a href="{{ URL::to('publico/usuario/meusdados') }}">Alterar meus dados</a></li>
                @if(Auth::user()->id_nivel==1)
                <li class="inativo"><a href="{{ URL::to('publico/usuario/novo') }}">Cadastrar usuário</a></li>
                @endif
                <li class="inativo"><a href="{{ URL::to('publico/usuario') }}">Listar usuários</a></li>
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="aweso-laptop aweso-large"></i> &nbsp;Consultas diversas <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu">
                <li class="inativo"><a href="{{ URL::to('publico/caixa/emconsulta') }}">Listar referências em consulta</a></li>
                <li class="inativo"><a href="{{ URL::to('publico/caixa/naoinventariadas') }}">Listar referências não enviadas para custódia e não inventariadas</a></li>
                <li class="inativo"><a href="{{ URL::to('publico/caixa/inventariadas') }}">xListar referências não enviadas para custódia e inventariadas</a></li>
                @if (\Auth::user()->cliente->franquiacustodia > 0)
                <li class="inativo"><a href="{{ URL::to('publico/caixa/emcarencia') }}">Listar referências não enviadas para custódia mas sendo faturadas (em carência)</a></li>
                @endif
                <li class="inativo"><a href="{{ URL::to('publico/caixa/expurgadas') }}">Listar referências expurgadas</a></li>
                <li class="inativo"><a href="{{ URL::to('publico/reserva') }}">Reservas realizadas</a></li>
            </ul>
        </li>



    </ul><!--/pull left menu-->

    <!-- pull right menu-->
    <ul class="nav pull-right">

        <!-- account -->
        <li class="dropdown">
            <a href="#" class="dropdown-toggle btn-sm" data-toggle="dropdown">
                {{Auth::user()->nomeusuario}} <i class="aweso-angle-down"></i>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
                <li><a href="/publico/usuario/meusdados">Meus dados</a></li>
                <li><a href="/publico/usuario/alterarsenha">Alterar senha</a></li>
            @if (Auth::user()->id_nivel < 10 && (Auth::user()->adm == 1 || Auth::user()->qtdClientes() > 0))
                <li><a href="/publico/usuario/escolherempresa">Alterar o cliente</a></li>
            @endif
                <li><a href="/auth/logout">Sair</a></li>
            </ul>
        </li><!-- /account -->
    </ul><!--/pull right menu-->
</div><!-- /uncollapse nav -->
