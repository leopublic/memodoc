@extends('base')

@section('estilos')
    @include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">{{ $titulo }}</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-archive"></i>{{number_format($quantos, 0, ",", ".")}} referências encontradas </div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body">
                <div class="form-body">
                <?php $i = 0;?>
                @foreach($expurgos as $expurgo)
                    @if ($i==12)
                        </div>
                        <div class="row" style="margin-bottom:5px;">
                        <?php $i = 0;?>
                    @elseif($i==0)
                        <div class="row" style="margin-bottom:5px;">
                    @endif
                    <div class="col-md-1"><a href="#" class="btn btn-primary">{{ substr('000000'.$expurgo->id_caixapadrao, -6) }}</a></div>
                    <?php $i++; ?>
                @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@stop