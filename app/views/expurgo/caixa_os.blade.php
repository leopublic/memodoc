@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Expurgar caixas</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-remove"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Informe o cliente e o número inicial e final das caixas (referências) a serem expurgadas</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal', 'id'=>'formexpurgo'))}}
                    {{Form::hidden('acao', '', array('id' => 'acao'))}}
                    {{Form::hidden('id_os_chave', $os->id_os_chave)}}
                    @if (isset($id_caixas))
                        @foreach($id_caixas as $i => $id_caixa)
                        <input type="hidden" name="id_caixas[]" id="id_caixa_{{$id_caixa}}" value="{{$id_caixa}}"/>
                        @endforeach
                    @endif
                    <div class='control-group'>
                        {{Form::label('os','OS',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('os', substr('000000'.$os->id_os, -6),  array('class' => 'input-block-level', 'disabled' => 'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('razaosocial', $razaosocial,  array('class' => 'input-block-level', 'disabled' => 'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('cortesia','Cortesia expurgo?',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::checkbox('cortesia', 1, $cortesia)}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('cortesia_movimentacao','Cortesia movimentação?',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::checkbox('cortesia_movimentacao', 1, $cortesia_movimentacao)}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('fl_bloqueia_liberados','Bloqueia endereços liberados?',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::checkbox('fl_bloqueia_liberados', 1, $fl_bloqueia_liberados)}}
                        </div>
                    </div>
                    <div class="control-group">
                        {{Form::label('id_caixapadrao','Referências ',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('id_caixapadrao_ini', null ,array('class'=>'input-small'))}}
                            </div>
                            A
                            <div class="input-append">
                                {{Form::text('id_caixapadrao_fim', null,array('class'=>'input-small'))}}
                            </div>
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button name="adicionar" class="btn btn-primary" id="adicionar" ><i class="aweso-plus"></i> Adicionar caixas</button>
                        @if (isset($caixas))
                        <div class="btn-group">
                            <a href="#" id="buttonSubmit" data-toggle="dropdown" class="btn dropdown-toggle bg-orange" title="Realiza o expurgo"><i class="aweso-exclamation"></i> Expurgar</a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:$('#acao').val('expurgar');$('#formexpurgo').submit();" class="btn bg-orange">Clique aqui para confirmar o expurgo. ATENÇÃO ESSA OPERAÇÃO NÃO PODE SER DESFEITA!!</a></li>
                            </ul>
                        </div>
                        @endif
                    </div>
                    {{ Form::close() }}
                    @if (isset($caixas))
                    <?php $i= 0; ?>
                    <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                        <thead>
                            <th style="width:auto; text-align:center;">#</th>
                            <th style="width:auto; text-align:center;">Ação</th>
                            <th style="width:auto; text-align:center;">ID</th>
                            <th style="width:auto; text-align:center;">Referência</th>
                            <th style="width:auto; text-align:center;">Situação</th>
                            <th style="width:auto; text-align:center;">Primeira entrada</th>
                            <th style="width:auto; text-align:center;">Guarda em dias</th>
                            <th style="width:auto; text-align:center;">Guarda em anos</th>
                            <th style="width:auto;">&nbsp;</th>
                        </thead>
                        <tbody>

                            @foreach($caixas as $caixa)
                            <?php $i++; ?>
                            <tr 
                                @if ($caixa->emcasa == 1)
                                    
                                @else
                                    class="warning"
                                @endif


                             id="linha_{{$caixa->id_caixapadrao}}">
                                <td style="text-align: center;">{{ $i }}
                                <td style="text-align: center;"><a href="javascript:retira_caixa('{{$caixa->id_caixapadrao}}');" class="btn btn-danger"><i class="aweso-trash"></i></a></td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa, -6)}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao, -6)}}</td>
                                <td style="text-align: center;">
                                @if ($caixa->emcasa == 1)
                                    No galpão
                                @else
                                    Emprestada
                                @endif
                                </td>
                                <td style="text-align: center;">{{$caixa->primeira_entrada}}</td>
                                <td style="text-align: center;">{{number_format($caixa->guardaDias, 0, ",", "." )}}</td>
                                <td style="text-align: center;">{{number_format($caixa->guardaAnos, 1, ",", "." )}}</td>
                                <td style="width:auto;">&nbsp;</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    @endif
                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop
@section('scripts')
<script>
    $( "#formexpurgo" ).submit(function( event ) {
        $('#buttonSubmit').prop('disabled', true);
        $('#buttonSubmit').addClass('disabled');
        $('#buttonSubmit').html('<i class="aweso-spinner aweso-spin"></i> Aguarde...');
        return true;
    });

    function retira_caixa(xid){
        $('#linha_'+xid).remove();
        $('#id_caixa_'+xid).remove();
    }
</script>

@stop