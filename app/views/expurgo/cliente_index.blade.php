@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Referências expurgadas</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-archive"></i>{{number_format($quantos, 0, ",", ".")}} referências encontradas </div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body ">
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->
                        <div class="col-md-12">
                            {{$expurgos->links()}}
                            <table class='table table-hover table-bordered table-condensed'>
                                <thead>
                                <tr>
                                    <th style="width:120px;text-align:center;">Ação</th>
                                    <th style="width:120px;text-align:center;">Expurgo</th>
                                    <th style="width:120px;text-align:center;">Data</th>
                                    <th style="width:120px;text-align:center;">Qtd caixas</th>
                                </tr>
                            </thead>
                                @foreach($expurgos as $expurgo)
                                <tr>
                                    <td style="text-align:center;"><a href="/publico/caixa/expurgadas/{{ $expurgo->id_expurgo_pai }}" class="btn btn-primary">Ver caixas</a></td>
                                    <td style="text-align:center;">{{ substr('000000'.$expurgo->id_expurgo_pai, -6) }}</td>
                                    <td style="text-align:center;">{{ $expurgo->data_solicitacao }}</td>
                                    <td style="text-align:center;">{{ number_format($expurgo->qtd_caixas(), 0, ",", ".") }}</td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop