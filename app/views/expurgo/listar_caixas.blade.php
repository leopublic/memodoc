@extends('layout')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>{{$titulo}}</h1></div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <!-- search box -->
            <div class="search-box form-horizontal">
                {{Form::open(array('id' => 'form-pesquisa'))}}
                {{Form::hidden('orderby', $orderby, array('id' => 'orderby'))}}
                {{Form::hidden('page', Input::get('page'))}}
                <div class='control-group'>
                    {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                    <div class="controls">
                        {{Form::select('id_cliente', $clientes, $id_cliente , array('id'=>'id_cliente','data-fx'=>'select2','class'=> 'input-block-level'))}}
                    </div>
                </div>
                <div class='control-group'>
                    {{Form::label('id_caixapadrao','Referência',array('class'=>'control-label'))}}
                    <div class="controls">
                        {{Form::text('id_caixapadrao', $id_caixapadrao , array('id'=>'id_caixapadrao','style'=>'width:200px;'))}}
                    </div>
                </div>

                <div class='controls pull-left'>
                    <label> &nbsp;</label>
                    <div class="controls">
                        <button class='btn btn-success' id="submit-pesquisa">Buscar</button>
                    </div>
                </div>
                <div class='clearfix'></div>

                {{Form::close()}}
            </div>
            @if(isset($expurgos))
            <?php 
            if (intval(Input::get('page')) == 0){
                $page = 1;
            } else {
                $page = intval(Input::get('page'));
            }
            $i = ($page  - 1) * 20 ; ?>
            {{ $expurgos->appends(Input::except('page'))->links() }}
            <table class='table table-hover table-striped table-condensed table-bordered'>
                <thead>
                    <th style="width:auto; text-align: center;">#</th>
                    <th style="width:auto; text-align: center;" onclick="ordene_por('id_caixapadrao');">Caixa</th>
                    <th style="width:auto; text-align: center;" onclick="ordene_por('id_expurgo_pai');">Expurgo</th>
                    <th style="width:auto; text-align: center;">OS</th>
                    <th style="width:auto; text-align: center;">Transbordo</th>
                    <th style="width:auto; text-align: center;">Cortesia exp.?</th>
                    <th style="width:auto; text-align: center;">Cortesia mov.?</th>
                    <th style="width:auto; text-align: center;">Data da solicitação</th>
                    <th style="width:auto; text-align: center;">Cadastrado por</th>
                </thead>
                <tbody>

                    @foreach($expurgos as $expurgo)
                    <tr class="">
                        <td style="text-align:center;">
                                <?php $i++?>
                            {{$i}}
                        </td>
                        <td style="text-align: center;"><a href="/caixa/historico/{{$expurgo->id_cliente}}/{{$expurgo->id_caixapadrao}}" target="_blank" class="btn btn-primary btn-sm">{{substr('000000'.$expurgo->id_caixapadrao, -6)}}</a></td>
                        <td style="text-align: center;"><a href="/expurgo/caixasgravadas/{{$expurgo->id_expurgo_pai}}" class="btn bg-orange btn-sm" title="Clique para ver as todas as caixas desse expurgo" target="_blank">{{substr('000000'.$expurgo->id_expurgo_pai, -6)}}</a></td>
                        <td style="text-align: center;">
                        @if($expurgo->id_os_chave > 0)
                            <a href="/osok/visualizar/{{$expurgo->id_os_chave}}" class="btn bg-green btn-sm" target="_blank">{{substr('000000'.$expurgo->id_os, -6)}}</a>
                        @else
                            {{$expurgo->id_os}}
                        @endif
                        </td>
                        <td style="text-align: center;">
                            @if ($expurgo->id_transbordo > 0)
                                <a class="btn" href="/transbordo/caixas/{{$expurgo->id_transbordo}}" target="_blank">{{substr('000000'.$expurgo->id_transbordo, -6)}}
                            @else
                                --
                            @endif
                        </td>
                        <td style="text-align: center;">
                            @if ($expurgo->cortesia == 1)
                            <i class="aweso-check"></i>
                            @else 
                            <i class="aweso-check-empty"></i>
                            @endif
                        </td>
                        <td style="text-align: center;">
                            @if ($expurgo->cortesia_movimentacao == 1)
                            <i class="aweso-check"></i>
                            @else 
                            <i class="aweso-check-empty"></i>
                            @endif
                        </td>
                        <td style="text-align: center;">{{$expurgo->data_solicitacao}}</td>
                        <td style="text-align: center;">
                            {{$expurgo->nomeusuario}}
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

            {{ $expurgos->appends(Input::except('page'))->links() }}
            @endif
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .table tbody{
        font:12px arial;
    }
</style>
@stop

@section('scripts')
<script>
    function ordene_por(coluna){
        $('#orderby').val(coluna); $('#form-pesquisa').submit();
    }
</script>
@stop
