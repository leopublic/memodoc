@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Log de alterações de documento</h1>

    </div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">

            @include ('padrao/mensagens')
            <div class="widget border-cyan" id="widget-horizontal">
                <div class="widget-header bg-cyan">
                    <div class="widget-icon"><i class="aweso-magic"></i><i class="aweso-archive"></i></div>
                    <h4 class="widget-title">Histórico </h4>
                </div>
                <!-- widget content -->
                <div class="widget-content">
                    <table  data-sorter="true" class='table table-striped table-condensed'>
                        <tr>
                            <th style="text-align:center; width:200px">Data</th>
                            <th style="text-align:center;">Responsável</th>
                            <th style="">Evento</th>
                            <th style="">Descrição</th>
                        </tr>
                        @foreach($historico as $h)
                        <tr>
                            <td style="text-align:center;">{{$h->created_at}}</td>
                            <td style="text-align:center;">{{$h->usuario->nomeusuario}}</td>
                            <td style="">{{$h->evento}}</td>
                            <td style="">{{$h->descricao}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <!-- /widget content -->
            </div>
        </div>
    </div>
</article> <!-- /content page -->

@stop

@section('scripts')
<style>
    .date{
        width:96px;
    }
    .form-horizontal .control-group{
        margin-bottom:5px !important;
    }
</style>

<script>


</script>
@stop