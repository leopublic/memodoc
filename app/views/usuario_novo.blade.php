@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>{{$cliente->nomefantasia}}</h1>

        </div>


        <!-- content breadcrumb -->
        <ul class="breadcrumb breadcrumb-inline clearfix">
            <li><a href="#">Cadastro</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
            <li class="active">Usuário</li>
        </ul>


    </header> <!--/ content header -->

    <!-- content page -->
    <article class="content-page clearfix">

        <!-- main page -->
        <div class="main-page documento_localizar" id="">
            <div class="content-inner">

            @if (Session::has('msg'))
            <div class="alert alert-info"><h4>Mensagem</h4>{{ Session::get('msg') }}</div>
            @endif

            @if (Session::has('success'))
            <div class="alert alert-success"><h4>Sucesso!</h4>{{ Session::get('success') }}</div>
            @endif

            @if (Session::has('error'))
            <div class="alert alert-error"><h4>Erro!</h4>{{ Session::get('error') }}</div>
            @endif




         {{ Form::model($model, array('id'=>'frmusuario')) }}
            <!-- widget form horizontal -->
            <div class="widget border-cyan span6" id="widget-horizontal">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-user"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Usuário</h4>
                    <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                    <div class="widget-action color-cyan">
                        <button data-toggle="collapse" data-collapse="#widget-horizontal" class="btn">
                            <i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
                        </button>
                    </div>
                </div><!-- /widget header -->




                <!-- widget content -->
                <div class="widget-content">


                 <div class='span5 pull-left form-horizontal'>


                     <div class='control-group'>
                         {{Form::label('nomeusuario','Nome usuário',array('class'=>'control-label'))}}
                         <div class="controls">
                         {{Form::text('nomeusuario', null, array('placeholder'=>'nome do pessoa usuário', 'class'=>'input-xlarge','required'))}}
                         {{Form::hidden('id_cliente', $cliente->id_cliente)}}
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('username','Login',array('class'=>'control-label'))}}
                         <div class="controls">
                         {{Form::text('username', null, array('placeholder'=>'Login', 'class'=>'input-xlarge','required'))}}
                         </div>
                     </div>
                     <div class='control-group'>
                         {{Form::label('password','Senha',array('class'=>'control-label'))}}
                         <div class="controls">
                         {{Form::password('password', array('id'=>'senha','placeholder'=>'Senha', 'class'=>'input-xlarge','required'))}}
                         </div>
                     </div>

                     <div class='control-group'>
                         {{Form::label('confirmacao','Confirmação',array('class'=>'control-label'))}}
                         <div class="controls">
                         {{Form::password('confirmacao', array('id'=>'confirmacao', 'placeholder'=>'Confirmação da senha', 'class'=>'input-xlarge','required'))}}
                         </div>
                     </div>

                     <div class='control-group'>
                         {{Form::label('validadelogin','Validade do login',array('class'=>'control-label'))}}
                         <div class="controls">
                         {{Form::text('validadelogin', null, array('placeholder'=>'Validade do login', 'class'=>'input-xlarge'))}}
                         </div>
                     </div>

                    <div class="control-group">
                       {{Form::label('aniversario','Aniversário',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('aniversario', null ,array('placeholder'=>"Aniversário",  'data-date-format'=>'dd/mm','data-fx' =>"datepicker"))}}
                                <span class="add-on"><i class="icomo-calendar"></i> </span>
                            </div>
                        </div>
                    </div>



                    </div>


                    <div class='clearfix'></div>
                 </div><!-- /widget content -->
             </div> <!-- /widget form horizontal -->






                <!-- Observações -->
                <div class="span6">
                    <!-- widget Caixas emprestimo -->
                    <div class="widget border-cyan" id="widget-obs">
                        <!-- widget header -->
                        <div class="widget-header bg-cyan">
                            <!-- widget icon -->
                            <div class="widget-icon"><i class="aweso-location-arrow"></i></div>
                            <!-- widget title -->
                            <h4 class="widget-title">Ambiente de acesso</h4>
                        </div><!-- /widget header -->
                        <!-- widget content -->
                        <div class="widget-content bg-silver">

                                <div class='control-group'>
                                    {{Form::label('id_departamento[]','Departamento',array('class'=>'control-label'))}}
                                    <div class="controls">
                                    {{Form::select('id_departamento[]', FormSelect($cliente->departamento,'nome'), FormSelectValue($model->departamentos), array('data-fx'=>'select2', 'multiple'=>'true'))}}
                                    </div>
                                </div>

                                <div class='control-group'>
                                    {{Form::label('id_setor[]','Setores',array('class'=>'control-label'))}}
                                    <div class="controls">
                                    {{Form::select('id_setor[]', FormSelect($cliente->setor,'nome'), FormSelectValue($model->setores), array('data-fx'=>'select2', 'multiple'=>'true'))}}
                                    </div>
                                </div>

                                <div class='control-group'>
                                    {{Form::label('id_centrodecusto[]','Centro de custos',array('class'=>'control-label'))}}
                                    <div class="controls">
                                    {{Form::select('id_centrodecusto[]', FormSelect($cliente->centrocusto,'nome'), FormSelectValue($model->centrocustos), array('data-fx'=>'select2', 'multiple'=>'true'))}}
                                    </div>
                                </div>
                                <div class='clearfix'></div>
                        </div><!-- /widget content -->
                    </div><!-- /widget Observações -->
                </div><!-- /Observações -->




                <div class='span6'>
                   <div class="form-actions bg-silver">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                   </div>
                </div>

                 {{Form::close()}}
            </div>
        </div>
    </article> <!-- /content page -->

@stop

@section('scripts')
<script>


  $(function(){


    $('#frmusuario').submit(function(){
       if($.trim($('#senha').val())!= '' && $.trim($('#confirmacao').val()) !=''){
            if($('#senha').val() !== $('#confirmacao').val()){
                alert('A senha e a confirmação da senha não são iguais, verifique!');
                return false;
            }
       }else{
          alert('Você precisa preencher corretamente a senha e a confirmação da senha!');
          return false;
       }
    });

    $('.add-caixa').click(function(){
        dados = {
                id_caixapadrao:$('#id_caixapadrao').val(),
                id_item:$('#id_item').val(),
                id_box:$('#id_box').val(),
                id_cliente:$('#id_cliente').val()
        };
        $(this).html('...');
        $caixa = $('.caixa-content');

        $.ajax({
            type: "POST",
            url: '/ordemservico/addcaixa',
            data: dados,
            dataType: 'json',
            success: function(data){
                $('.add-caixa').html('add');
                $('.add-to-chat').hide(200);
                if(data.length === 0){
                    alert('Nenhum registro foi encontrado para este cliente');
                }
                $('#id_caixapadrao').focus();
                $.each(data, function(i, item) {

                     $caixa.append('<tr>'+
                                         '<input type="hidden" name="id_documento['+item.id_documento+']" value="'+item.id_caixapadrao+'" />'+
                                         '<td>'+item.id_caixapadrao+'</td>'+
                                         '<td>'+item.titulo+'</td>'+
                                         '<td>'+item.id_box+'</td>'+
                                         '<td>'+item.id_item+'</td>'+
                                   '</tr>');


                })

            }
        });

    });

    $('[data-toggle="add-others"]').click(function(){
        $(".add-to-chat").slideToggle("fast","easeOutBack");
    });

  });


</script>
<style>
    .date{
        width:96px;
    }
    .form-horizontal .control-group{
        margin-bottom:5px !important;
    }
</style>
@stop