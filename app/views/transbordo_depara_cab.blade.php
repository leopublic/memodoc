<table border='0' cellspacing='0' width="100%" style='border:1px solid #000;font:12px arial;mar'>
    <tr>
        <td width="50%" style="font-size:20px; font-weight: bold; vertical-align: top; padding:0;">
            <img src='../public/img/logo_subtitulo.jpg' style="float:left; vertical-align: text-top; " />
        </td>
        <td width="50%" style="vertical-align: top; width:220px; text-align: right;">
            {{date('d/m/Y')}}<br/>{{date('H:i:s')}}
        </td>
    </tr>
    <tr>
        <td colspan=2 style="font-size:20px; font-weight: bold; vertical-align: top; padding:0; text-align: center;">
            TRANSBORDO DE CAIXAS N&ordm; {{substr('000000'.$transbordo->id_transbordo, -6)}}
        </td>
    </tr>
    <tr>
        <td style="font-size:10px; font-weight: normal; vertical-align: top; padding:5px;">
            de<br/><span style="font-size:14px;font-weight:bold;">{{$transbordo->cliente_antes->razaosocial.' ('.$transbordo->id_cliente_antes.')'}}</span>
        </td>
        <td style="font-size:10px; font-weight: normal; vertical-align: top; padding:5px;">
            para<br/><span style="font-size:14px;font-weight:bold;">{{$transbordo->cliente_depois->razaosocial.' ('.$transbordo->id_cliente_depois.')'}}</span>
        </td>
    </tr>
</table>
