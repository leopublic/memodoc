<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="/stilearn-metro/css/font-awesome.css" rel="stylesheet" />
    <title>:: MEMODOC ::</title>
    <style>
		body, html{
			height: 100%;
		}
        body {
			font-family: 'Verdana', Arial, sans-serif;
            margin:0;
			padding:0;
        }

		div#barra{
			background-color: transparent;
			background-color: #00b0ca;
			border-top: solid 3px #777;
		}

		h1{
			margin: 0;
			padding: 0;
			font-family: inherit;
			font-size:24px;
		}

		div#barra h1{
			margin-left: 10px;
			display: inline;
		}
		div#interno{
			border-radius: 10px;
			-webkit-border-radius: 10px;
			box-shadow: 0px 0px 15px -2px #c7c7c7;
			width:820px;
			margin:50px auto;
		}
		div#header{
			border-top-left-radius: 10px;
			border-top-right-radius: 10px;

		}
		div#header2{
			border-top-left-radius: 10px;
			border-top-right-radius: 10px;
			background-color: #00b0ca;
			border-top:solid 1px #00E2C0;
			border-left:solid 1px #00E2C0;
			border-right:solid 1px #00E2C0;
			padding:10px;
		}
		div#conteudo h1{
			font-weight: normal;
			margin-bottom:10px;
			margin-top: 15px;
		}
		div#conteudo input{
			padding: 10px 15px;
			margin: 5px 0 20px 0;
			border: 1px solid #e8e8e8;
			font: 1.2em Arial, sans-serif;
			color: #7a7a7a;
			background: #f5f5f5;
			font-size: 15px;
			width: 89%;
			border-radius: 5px;
		}
		div#conteudo p{
			margin:0;
		}
		div#conteudo p.submit{
			display:inline;
		}
		div#conteudo p.submit input{
			width:auto;
		}

		div#interno div#conteudo{
			padding:10px;
		}
		div#conteudo span.error{
			color:red;
		}
		div.interno h1{
			font-family: Verdana, Arial, sans-serif;
			color: #555;
			font-weight: normal;
		}
        a, a:visited {
            color: #006E7C;
            text-decoration:none;
        }

        a:hover {
            text-decoration:underline;
        }

		table{
			margin: auto;
			width: 100%;
			border-collapse: collapse;
			font-family: inherit;
			color: #555;
			border-bottom: solid 1px #ccc;
		}

    </style>

</head>
<body>
	<div id="interno">
		<div id="conteudo">
            <img src="/img/home_head.jpg"/>
            <table style="width:500px; margin-top:10px;">
                <tr>
                    <td style="width:200px;text-align: center; color:blue;" ><i class="aweso-lock aweso-2x"></i><br/>Criar nova senha</td>
                    <td style="font-size:11px;">Informe a nova senha.</td>
                </tr>
            </table>
			{{ Form::open() }}
			{{ Form::hidden('token', $token)}}
            <table style="width:500px;">
                <tr>
                    <td style="width:100px; vertical-align: middle ;">Nova senha</td>
                    <td>{{ Form::password('senha') }}</td>
                </tr>
                <tr>
                    <td style="width:100px; vertical-align: middle ;">(repita)</td>
                    <td>{{ Form::password('senha_rep') }}</td>
                </tr>
            </table>
            <div style="margin:auto; text-align: center;">
                <p class="submit">{{ Form::submit('Enviar') }}</p>
            </div>
			{{ Form::close() }}
            <!-- submit button -->
            @if (Session::has('msg'))
            <div style="margin:auto; text-align: center; color:red; ">
                <span class="error">{{ Session::get('msg') }}</span>
            </div>
            @endif

		</div>
	</div>
	<?php echo HTML::script('js/jquery-1.9.1.js'); ?>
	<?php echo HTML::script('js/script.js'); ?>
</body>
</html>