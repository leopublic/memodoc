@extends('layout')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    {{Form::open()}}
    <div class="page-header"><h1>Listar o conteúdo do andar {{Form::text('id_galpao', $id_galpao, array('style'=>'width:20px; text-align:center;'))}}.{{Form::text('id_rua', $id_rua, array('style'=>'width:20px; text-align:center;'))}}.{{Form::text('id_predio', $id_predio, array('style'=>'width:20px; text-align:center;'))}}.{{Form::text('id_andar', $id_andar, array('style'=>'width:20px; text-align:center;'))}}
            <button type="submit" title="Atualizar" class="btn" style="margin-bottom:10px;"><i class="icon-refresh"></i></button>
        </h1>
    </div>
    {{Form::close()}}

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

        @if (isset($enderecos))
            <table class='table table-hover'>
                <thead>
                    <th style="text-align:center; width:50px">#</th>
                    <th style="width:280px;text-align:center; width:50px">Unidade</th>
                    <th style="text-align:center; width:80px;">ID</th>
                    <th style="text-align:center; width:120px">Endereço</th>
                    <th style="text-align:center; width:80px">Referência</th>
                    <th>Cliente</th>
                    <th style="text-align:center;">Histórico</th>
                    <th style="text-align:center;">Reserva</th>
                    <th style="text-align:center;">Criado em</th>
                    <th style="text-align:center;">Atualizado em</th>
                    <th style="">Observação</th>
                </thead>
                <tbody>
            <?php $i = 0; ?>
            @foreach($enderecos as $end)
                @if ($end->endcancelado == 1 && $end->id_caixapadraofmt != '--')
                @else
                    <?php $i++;; ?>
                    @if($end->endcancelado == 1)
                        <tr class="alert">
                    @else
                        @if($end->id_cliente == '')
                            <tr class="success">
                        @else
                            <tr>
                        @endif
                    @endif
                            <td style="text-align:center; font-style: italic; color:#999;">{{$i}}</td>
                            <td style="text-align:center;">{{substr('0000'.$end->id_unidade, -4)}}</td>
                            <td style="text-align:center;"> {{$end->id_caixa}}</td>
                            <td style="text-align:center;"> {{$end->enderecoCompleto()}}</td>
                            @if ($end->endcancelado == 1)

                                @if($end->id_cliente > 0)
                                    <td style="text-align:center; font-weight: bold; ">{{ $end->id_caixapadraofmt}}</td>
                                    <td>{{$end->cliente->razaosocial.' ('.$end->id_cliente.')'}}</td>
                                    <td style="text-align:center;">
                                        @if(intval($end->id_caixapadrao) > 0)
                                            <a href="/caixa/historico/{{$end->id_cliente}}/{{$end->id_caixapadrao}}" target="_blank" class="btn btn-primary">Histórico</a>
                                        @else 
                                        --
                                        @endif
                                    </td>

                                    <td style="text-align:center;">
                                        @if (intval($end->id_reserva) > 0)
                                            <a href="/reserva/visualizar/{{$end->id_reserva}}" target="_blank" class="btn btn-primary">{{$end->id_reserva}}</a>
                                        @else 
                                            {{$end->id_reserva}}
                                        @endif
                                    </td>
                                @else
                                    <td style="text-align:center; font-weight: bold; ">{{ $end->id_caixapadraofmt}}</td>
                                    <td>(cancelado/bloqueado)</td>
                                    <td style="text-align:center;">--</td>
                                    <td style="text-align:center;">--</td>
                                @endif
                            @else
                                @if($end->id_cliente > 0)
                                    <td style="text-align:center; font-weight: bold; ">{{ $end->id_caixapadraofmt}}</td>
                                    <td>{{$end->cliente->razaosocial.' ('.$end->id_cliente.')'}}</td>
                                    <td style="text-align:center;">
                                        @if(intval($end->id_caixapadrao) > 0)
                                            <a href="/caixa/historico/{{$end->id_cliente}}/{{$end->id_caixapadrao}}" target="_blank" class="btn btn-primary">Histórico</a>
                                        @else 
                                        --
                                        @endif
                                    </td>
                                    <td style="text-align:center;">
                                        @if (intval($end->id_reserva) > 0)
                                            <a href="/reserva/visualizar/{{$end->id_reserva}}" target="_blank" class="btn btn-primary">{{$end->id_reserva}}</a>
                                        @else 
                                            {{$end->id_reserva}}
                                        @endif
                                    </td>
                                @else
                                    <td style="text-align:center; font-weight: bold; ">{{ $end->id_caixapadraofmt}}</td>
                                    <td>(vago)</td>
                                    <td style="text-align:center;">--</td>
                                    <td style="text-align:center;">--</td>
                                @endif
                            @endif
                            <td style="text-align:center;">{{$end->formatDate('created_at')}}</td>
                            <td style="text-align:center;">{{$end->formatDate('updated_at')}}</td>
                            <td>{{$end->observacao}}</td>
                        </tr>
                @endif
            @endforeach
                </tbody>
            </table>

        @endif
        </div>
    </div>
</article> <!-- /content page -->
@stop