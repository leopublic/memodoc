@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Criar transbordo</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-share-alt"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Adicione as caixas a serem transbordadas ({{$qtd_disp}} vagas disponíveis para movimentação)</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal'))}}
                    {{Form::hidden('id_cliente_antes', Input::old('id_cliente_antes')) }}
                    {{Form::hidden('id_cliente_depois', Input::old('id_cliente_depois')) }}
                     @if (isset($id_caixas))
                        @foreach($id_caixas as $i => $id_caixa)
                        <input type="hidden" name="id_caixas[]" value="{{$id_caixa}}"/>
                        @endforeach
                    @endif
                   <div class='control-group'>
                        {{Form::label('id_cliente','Cliente origem',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('cliente_antes', $cliente_antes->razaosocial.' ('.$cliente_antes->id_cliente.')' , array('disabled' => 'disabled',   'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente destino',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('cliente_depois', $cliente_depois->razaosocial.' ('.$cliente_depois->id_cliente.')' , array('disabled' => 'disabled',   'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('observacao','Observações',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::textarea('observacao', Input::old('observacao'), array('rows' => 4, 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class="control-group">
                        {{Form::label('id_caixapadrao','Referências ',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('id_caixapadrao_ini', null ,array('class'=>'input-small'))}}
                            </div>
                            A
                            <div class="input-append">
                                {{Form::text('id_caixapadrao_fim', null,array('class'=>'input-small'))}}
                            </div>
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button name="adicionar" class="btn btn-primary" id="adicionar" ><i class="aweso-plus"></i> Adicionar caixas</button>
                        @if (isset($caixas))
                        <div class="btn-group">
                            <a href="#" id="buttonSubmit" data-toggle="dropdown" class="btn dropdown-toggle bg-orange" title="Realiza o transbordo"><i class="aweso-share-alt"></i> Efetivar</a>
                            <ul class="dropdown-menu">
                                <li><input type="submit" name="efetivar" class="btn bg-orange" value="Clique aqui para efetivar o transbordo. ATENÇÃO ESSA OPERAÇÃO NÃO PODE SER DESFEITA!!" /></li>
                            </ul>
                        </div>
                        @endif
                    </div>
                    {{ Form::close() }}
                    @if (isset($caixas))
                    <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                        <thead>
                            <th style="width:80px; text-align:center;">#</th>
                            <th style="width:auto; text-align:center;">ID</th>
                            <th style="width:auto; text-align:center;">Referência</th>
                            <th style="width:auto;">&nbsp;</th>
                        </thead>
                            <? $i = 1;?>
                        <tbody>
                            @foreach($caixas as $caixa)
                            <tr class="">
                                <td style="text-align: center;">{{$i}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa, -6)}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao, -6)}}</td>
                                <td style="width:auto;">&nbsp;</td>
                            </tr>
                            <? $i++;?>
                            @endforeach

                        </tbody>
                    </table>
                    @endif

                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop