@extends('stilearn-metro')

@section('conteudo')

@include('cliente_tab_header')

<?php
$atendimento = array(
    '-1' => '(n/a)',
    '0' => '12 horas',
    '1' => '24 horas',
);

$estados = array("AC" => "Acre", "AL" => "Alagoas", "AM" => "Amazonas", "AP" => "Amapá", "BA" => "Bahia", "CE" => "Ceará", "DF" => "Distrito Federal", "ES" => "Espírito Santo", "GO" => "Goiás", "MA" => "Maranhão", "MT" => "Mato Grosso", "MS" => "Mato Grosso do Sul", "MG" => "Minas Gerais", "PA" => "Pará", "PB" => "Paraíba", "PR" => "Paraná", "PE" => "Pernambuco", "PI" => "Piauí", "RJ" => "Rio de Janeiro", "RN" => "Rio Grande do Norte", "RO" => "Rondônia", "RS" => "Rio Grande do Sul", "RR" => "Roraima", "SC" => "Santa Catarina", "SE" => "Sergipe", "SP" => "São Paulo", "TO" => "Tocantins");
?>

<!-- Aba identificação -->
<div class="tab-pane" id="tab_contrato">
    {{ Form::model($cliente, array('class'=>'form-horizontal')) }}
    <h3>Situação</h3>
    <div class="row-fluid">
        <div class="span6">
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('fl_ativo','Cliente ativo ?',array('class'=>'control-label span8')) }}
                    {{ Form::hidden('fl_ativo', 0)}}
                    <div class="controls span4">
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_ativo', 0, !$cliente->fl_ativo)}} Não
                        </label>
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_ativo', 1, $cliente->fl_ativo)}} Sim (padrão para novos)
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('obs','Observação',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::textarea('obs', null, array('class'=> 'input-block-level')) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h3>Contrato</h3>
    <div class="row-fluid">
        <div class="span6">
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('valorminimofaturamento','Valor mínimo faturamento',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::text('valorminimofaturamento', null, array('class'=> 'input-block-level', 'data-fx'=>"masked" , 'data-inputmask'=>"'mask': '999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true", 'style'=>'text-align: right;')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('atendimento','Tipo de atendimento',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::select('atendimento', $atendimento, null, array('class'=> 'input-block-level')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('movimentacaocortesia','Movimentação de cortesia',array('class'=>'control-label span8')) }}
                    {{ Form::hidden('movimentacaocortesia', 0)}}
                    <div class="controls span4">
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('movimentacaocortesia', 0, !$cliente->movimentacaocortesia)}} Não (padrão Memodoc)
                        </label>
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('movimentacaocortesia', 1, $cliente->movimentacaocortesia)}} Sim
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('iniciocontrato','Início do contrato',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::text('iniciocontrato',$cliente->formatDate('iniciocontrato'), array('class'=> 'input-block-level date', 'placeholder'=>"Início do contrato", 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker")) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('descontocustodiapercentual','Desconto custódia percentual',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::text('descontocustodiapercentual', null, array('class'=> 'input-block-level', 'data-fx'=>"masked" , 'data-inputmask'=>"'mask': '999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true", 'style'=>'text-align: right;')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('descontocustodialimite','Limite de caixas para o desconto de custódia',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::text('descontocustodialimite', null, array('class'=> 'input-block-level')) }}
                    </div>
                </div>
            </div>
            @if (Auth::user()->nivel->minimoAdministrador())
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('fl_frete_manual','Permite valor fixo do frete nas OS?',array('class'=>'control-label span8')) }}
                    {{ Form::hidden('fl_frete_manual', 0)}}
                    <div class="controls span4">
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_frete_manual', 0, !$cliente->fl_frete_manual)}} Não (padrão Memodoc)
                        </label>
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_frete_manual', 1, $cliente->fl_frete_manual)}} Sim
                        </label>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    <h3>Franquias</h3>
    <div class="row-fluid">
        <div class="span6">
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('franquiacustodia','Franquia de custódia deve ser cobrada após (dias)',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::text('franquiacustodia', null, array('class'=> 'input-block-level')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('franquiamovimentacao','Franquia movimentação sobre o total de caixas (%)',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::text('franquiamovimentacao', null, array('class'=> 'input-block-level', 'data-fx'=>"masked" , 'data-inputmask'=>"'mask': '999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true", 'style'=>'text-align: right;' )) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h3>Cobrança</h3>
    <div class="row-fluid">
        <div class="span6">
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('responsavelcobranca','Responsável da cobrança',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::text('responsavelcobranca', null, array('class'=>'input-block-level') ) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('id_indice','Cobrar de ',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::select('id_cliente_cobranca', $pagadores, null, array('class' => 'input-block-level', 'data-fx' => 'select2')) }}
                    </div>
                </div>
                <div class='control-group'>
                    {{ Form::label('issporfora','ISS não incluído',array('class'=>'control-label span8')) }}
                    {{ Form::hidden('issporfora', 0)}}
                    <div class="controls span4">
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('issporfora', 0, !$cliente->issporfora)}} Não (padrão Memodoc)
                        </label>
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('issporfora', 1, $cliente->issporfora)}} Sim
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('id_indice','Índice de reajuste',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::select('id_indice', $indice, null, array('class' => 'input-block-level')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('id_tipo_faturamento','Tipo de faturamento',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::select('id_tipo_faturamento', $tipo_faturamento,null, array('class'=> 'input-block-level')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('obs','Observação cobrança (a/c)',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::text('destinatario_cobranca', null, array('class'=> 'input-block-level')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('obs','Descrição complementar para nota fiscal',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::text('descricao_servicos', null, array('class'=> 'input-block-level')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('obs','Dia para faturamento',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::select('dia_vencimento', $dias_vencimento, null, array('class'=> 'input-block-level')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('obs','Dia vencimento boleto',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::text('dia_vencimento_boleto',  null, array('class'=> 'input-block-level')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('imprimenf','Emissão NF suspensa?',array('class'=>'control-label span8')) }}
                    {{ Form::hidden('fl_emissaonf_suspensa', 0)}}
                    <div class="controls span4">
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_emissaonf_suspensa', 0, !$cliente->fl_emissaonf_suspensa)}} Não (padrão Memodoc)
                        </label>
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_emissaonf_suspensa', 1, $cliente->fl_emissaonf_suspensa)}} Sim
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('obsdesconto','Observação para o desconto',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::textarea('obsdesconto', null, array('class'=> 'input-block-level')) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h3>Digitalização e controle de revisão</h3>
    <div class="row-fluid">
        <div class="span6">
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('fl_digitalizacao','Exibir digitalizações?',array('class'=>'control-label span8')) }}
                    {{ Form::hidden('fl_digitalizacao', 0)}}
                    <div class="controls span4">
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_digitalizacao', 0, !$cliente->fl_digitalizacao)}} Não (padrão para novos)
                        </label>
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_digitalizacao', 1, $cliente->fl_digitalizacao)}} Sim
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('fl_controle_revisao','Controle de revisão de conteúdo?',array('class'=>'control-label span8')) }}
                    {{ Form::hidden('fl_controle_revisao', 0)}}
                    <div class="controls span4">
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_controle_revisao', 0, !$cliente->fl_controle_revisao)}} Não (padrão para novos)
                        </label>
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_controle_revisao', 1, $cliente->fl_controle_revisao)}} Sim
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h3>Inadimplência e bloqueios</h3>
    <div class="row-fluid">
        <div class="span6">
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('em_atraso_desde','Em atraso desde',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::text('em_atraso_desde',$cliente->formatDate('em_atraso_desde'), array('placeholder'=>"Data de início do atraso para fins de bloqueio", 'class'=>'date input-block-level', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker")) }}
                    </div>
                </div>
            </div>

            @if (Auth::user()->nivel->minimoAdministrador())
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('em_atraso_desde','Nivel de inadimplência',array('class'=>'control-label span8')) }}
                    <div class="controls span4">
                        {{ Form::select('nivel_inadimplencia',array(""=>"(em dia)", "1"=>"Notificação", "2"=>"Bloqueio"), $cliente->nivel_inadimplencia, array('class'=>'input-block-level')) }}
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="span6">
            @if (Auth::user()->nivel->minimoAdministrador())
            <div class="row">
                <div class='control-group'>
                    {{ Form::label('fl_os_bloqueada','Abertura de OS restrita ao ADM?',array('class'=>'control-label span8')) }}
                    {{ Form::hidden('fl_os_bloqueada', 0)}}
                    <div class="controls span4">
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_os_bloqueada', 0, !$cliente->fl_os_bloqueada)}} Não (padrão Memodoc)
                        </label>
                        <label class="radio-inline" style="margin-bottom:0px;">
                            {{ Form::radio('fl_os_bloqueada', 1, $cliente->fl_os_bloqueada)}} Sim
                        </label>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="row-fluid">
        <div style="width:100%; text-align: center;clear:both">
            {{ Form::submit('Salvar') }}
        </div>
    </div>
</div>

{{ Form::close() }}
</div><!-- Fim Aba identificação -->
    

@include('cliente_tab_footer')
@stop
