@extends('stilearn-metro')

@section('links')
	<link href="/stilearn-metro/css/wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" />
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	    	<script src="/stilearn-metro/js/html5shiv.js"></script>
	    	<script src="/stilearn-metro/js/lte-ie7.js"></script>
	<![endif]-->
@stop @section('conteudo')
<!-- content header -->
<header class="content-header">
	<!-- content title-->
	<div class="page-header">
		<h1>Aviso da home</h1>
	</div>

	<!-- content breadcrumb -->

</header>
<!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
	<!-- main page -->
	<div class="main-page">
		<div class="content-inner">
			@include('padrao.mensagens')
			<div class="widget border-cyan" id="widget-wysihtml5">
				<div class="widget-header bg-cyan">
					<!-- widget icon -->
					<div class="widget-icon">
						<i class="aweso-edit"></i>
					</div>
					<!-- widget title -->
					<h4 class="widget-title">Alter aviso</h4>
					<!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
					<div class="widget-action color-cyan">
						<button data-toggle="collapse" data-collapse="#widget-wysihtml5"
							class="btn">
							<i class="aweso-minus" data-toggle-icon="aweso-minus aweso-plus"></i>
						</button>
					</div>
				</div>
				<!-- /widget header -->

				<!-- widget content -->
				<div class="widget-content">
					<div class="row-fluid">
						{{ Form::model($aviso, array('class'=>'form-horizontal')) }} {{
						Form::hidden('id_aviso', null) }}
						<div class="row-fluid">
							<div class="span12">
								<div class="control-group">
									{{Form::label('dt_inicio','Exibir a partir de',array('class'=>'control-label'))}}
									<div class="controls">
										<div class="input-append input-append-inline ">
											{{Form::text('dt_inicio', null ,array('placeholder'=>"(opcional)",
											'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
											<span class="add-on"><i class="icomo-calendar"></i> </span>
										</div>
									</div>
								</div>
								<div class="control-group">
									{{Form::label('dt_fim','Exibir até',array('class'=>'control-label'))}}
									<div class="controls">
										<div class="input-append input-append-inline ">
											{{Form::text('dt_fim', null ,array('placeholder'=>"(opcional)",
												'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
											<span class="add-on"><i class="icomo-calendar"></i> </span>
										</div>
									</div>
								</div>
								<div class='control-group'>
									{{Form::label('fl_ativo', 'Está ativo?',array('class'=>'control-label'))}}
									<div class="controls">
										<label class="radio-inline" style="margin-bottom: 0px;">
											{{Form::radio('fl_ativo', 0, !$aviso->fl_ativo)}} Não </label>
										<label class="radio-inline" style="margin-bottom: 0px;">
											{{Form::radio('fl_ativo', 1, $aviso->fl_ativo)}} Sim (padrão para novos) </label>
									</div>
								</div>
								<div class='control-group'>
									{{Form::label('fl_padrao', 'É o aviso padrão?',array('class'=>'control-label'))}}
									<div class="controls">
										<label class="radio-inline" style="margin-bottom: 0px;">
											{{ Form::radio('fl_padrao', 0, !$aviso->fl_padrao)}} Não (padrão para novos) </label>
										<label class="radio-inline" style="margin-bottom: 0px;">
											{{ Form::radio('fl_padrao', 1, $aviso->fl_padrao)}} Sim </label>
									</div>
								</div>

								<div class='control-group'>
									{{Form::label('observacao',
									'Observações',array('class'=>'control-label'))}}
									<div class="controls">
										{{Form::textarea('observacao', null, array('style'=>'height:120px','class'=>'input-block-level'))}}
									</div>
								</div>
							</div>
						</div>
						<br/>
						Aviso padrão
						<div class="row-fluid">
							<textarea id="texto" name="texto" class="span12" rows="20"  style="font-size:12px;" placeholder="Digite o texto ...">{{$aviso->texto}}</textarea>
						</div>
						<br/>
						Aviso Popup
						<div class="row-fluid">
							<textarea id="texto_popup" name="texto_popup" class="span12" rows="20"  style="font-size:12px;" placeholder="Digite o texto ...">{{$aviso->texto_popup}}</textarea>
						</div>
    	            	Atenciosamente,<br/><img src="{{asset('img/logo.jpg')}}"/>
						</div>
					<input type="submit" class="btn" value="Salvar"/>
					</form>
					<!-- /widget content -->
				</div>
			</div>
		</div>
	</div>
</article>
<!-- /content page -->
@stop @section('scripts')
<script type="text/javascript" src="/stilearn-metro/js/wysihtml5/wysihtml5-0.3.0.min.js"></script>
<script type="text/javascript" src="/stilearn-metro/js/wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript">
		$("#texto").wysihtml5({"color": true});
		$("#texto_popup").wysihtml5({"color": true});
	</script>
@stop
