@extends('layout')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Avisos</h1></div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            @if(isset($avisos))
            <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                <thead>
                    <th style="width:auto; text-align:center;">ID</th>
                    <th style="width:auto; text-align:center;">Desde</th>
                    <th style="width:auto; text-align: center;">Até</th>
                    <th style="width:auto; text-align: center;">Padrão</th>
                    <th style="width:auto; text-align: center;">Ativo</th>
					<th style="width:auto; text-align: center;">Texto</th>
                </thead>
                <tbody>

                    @foreach($avisos as $aviso)
                    <tr class="">
                        <td style="text-align: center;">{{substr('000000'.$expurgo->id_caixa, -6)}}</td>
                        <td style="text-align: center;">{{substr('000000'.$expurgo->id_caixapadrao, -6)}}</td>
                        <td style="text-align: center;">{{$expurgo->data_solicitacao}}</td>
                        <td style="text-align: center;">
                            @if ($expurgo->data_checkout != '')
                            {{$expurgo->data_checkout}}
                            @else
                            --
                            @endif
                        </td>
                        <td style="width:auto;">&nbsp;</td>
                    </tr>
                    @endforeach

                </tbody>
            </table>

            {{ $expurgos->links() }}
            @endif
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .table tbody{
        font:12px arial;
    }
</style>
@stop