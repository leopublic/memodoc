@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header">
        <h1>Detalhes do expurgo</h1>
    </div>
    <div class='content-action pull-right'>
        <a href="/expurgo/relacaooperacional/{{$exppai->id_expurgo_pai}}" class="btn btn-warning btn-sm" title="Clique para gerar a realação de caixas para o operacional" target="_blank"><i class="aweso-print"></i> Relação para operacional</a>
        <a href="/expurgo/relacaodocumentos/{{$exppai->id_expurgo_pai}}" class="btn bg-lime btn-sm" title="Clique para gerar a realação de documentos para o cliente" target="_blank"><i class="aweso-print"></i> Relação para o cliente</a>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-remove"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Detalhes do expurgo {{substr('000000'.$exppai->id_expurgo_pai, -6)}}</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal', 'url'=>'expurgo/caixasconfirmado'))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('razaosocial', $razaosocial, array('class' => 'input-block-level', 'disabled' => 'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('os','OS',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('os', $id_os,  array('class' => 'input-block-level', 'disabled' => 'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('cortesia','Cortesia expurgo?',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::checkbox('cortesia', 1, $exppai->cortesia, array('disabled'=>'true'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('cortesia_movimentacao','Cortesia movimentação?',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::checkbox('cortesia_movimentacao', 1, $exppai->cortesia_movimentacao, array('disabled'=>'true'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('data_solicitacao','Data solicitação',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('data_solicitacao', $exppai->data_solicitacao, array('class' => 'input-block-level', 'disabled' => 'disabled'))}}
                        </div>
                    </div>
                    @if ($exppai->id_transbordo > 0)
                    <div class='control-group'>
                        {{Form::label('transbordo','Transbordo',array('class'=>'control-label'))}}
                        <div class="controls">
                            <a class="btn" href="/transbordo/caixas/{{$exppai->id_transbordo}}" target="_blank">{{substr('000000'.$exppai->id_transbordo, -6)}}</a>
                        </div>
                    </div>
                    @endif
                    {{ Form::close() }}

                    <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                        <thead>
                            <th style="width:auto; text-align:center;">Status</th>
                            <th style="width:auto; text-align:center;">Id</th>
                            <th style="width:auto; text-align:center;">Referência</th>
                            <th style="width:auto; text-align:center;">Endereço</th>
                            <th style="width:auto; text-align:center;">Checkout</th>
                            <th style="width:auto; text-align: left;">Observação</th>
                        </thead>
                        <tbody>

                            @foreach($caixas as $caixa)
                            <tr class="">
                                <td style="text-align: center;" title="{{$caixa->msg_processamento}}">
                                @if($caixa->fl_processado == 1)
                                    @if($caixa->codret_processamento == 1)
                                        <i class="aweso-ok"></i>
                                    @elseif($caixa->codret_processamento == -1)
                                        <i class="aweso-remove"></i>
                                    @else
                                        <i class="aweso-spinner aweso-spin"></i>
                                    @endif
                                @else
                                    <i class="aweso-spinner aweso-spin"></i>
                                @endif
                                </td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa, -6)}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao, -6)}}</td>
                                <td style="text-align: center;">{{$caixa->endereco->enderecoCompleto()}}</td>
                                <td style="text-align: center;">{{$caixa->data_checkout_fmt}}</td>
                                <td style="text-align: left;">{{$caixa->observacao}}</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop