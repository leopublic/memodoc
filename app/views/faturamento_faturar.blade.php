@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Faturar</h1></div>
    </header> <!--/ content header -->

    <!-- content page -->
    <article class="content-page clearfix">
        <!-- main page -->
        <div class="main-page" id="">
            <div class="content-inner">
                <!-- search box -->
                {{Form::open(array('class' => 'form-horizontal'))}}
                <div class="widget border-cyan">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-archive"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Informe o período de faturamento</h4>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content bg-silver">
                        <div class="control-group">
                            <div class='input-append'>
                                {{Form::select('mes', array('1'=>'Janeiro','2'=>'Fevereiro','3'=>'Março','4'=>'Abril','5'=>'Maio','6'=>'Junho','7'=>'Julho','8'=>'Agosto','9'=>'Setembro','10'=>'Outubro','11'=>'Novembro','12'=>'Dezembro') , $mes, array('mes'=>'descricao'))}}
                                {{Form::text('ano', $ano ) }}
                               <input type="submit" class="btn bg-cyan" class="button" id="adicionaCaixa" value="Recuperar"/>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}

        @if (Session::has('msg'))
        <div class="alert alert-info"><h4>Mensagem</h4>{{ Session::get('msg') }}</div>
        @endif

        @if (Session::has('success'))
        <div class="alert alert-success"><h4>Sucesso!</h4>{{ Session::get('success') }}</div>
        @endif

        @if (Session::has('error'))
        <div class="alert alert-error"><h4>Erro!</h4>{{ Session::get('error') }}</div>
        @endif

        @if(isset($clientes))

                <div class="widget border-cyan">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-reorder"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Clientes ativos</h4>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content bg-silver">
                    <table class='table table-striped table-hover table-bordered table-condensed'>
                        <thead>
                            <tr>
                                <th style="width:50px; text-align: center;">Sel</th>
                                <th>Ação</th>
                                <th style="width:auto; ">Cliente</th>
                                <th style="width:80px;text-align: center;">Início do contrato</th>
                                <th style="width:80px;text-align: center;">Último reajuste</th>
                                <th>Valor total (R$)</th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach($clientes as $cliente)
                                <tr>
                                    <td style="text-align: center;">{{Form::checkbox("selecionado", $cliente->id_cliente) }}</td>
                                    <td style="text-align: center;">
                                        <a href="/faturamento/faturar/{{ $cliente->id_cliente}}/{{ $mes }}/{{ $ano }}" title="Gerar faturamento" target="_blank"><i class="aweso-gear"></i></a>
                                    <td style="">{{ $cliente->razaosocial }} <br/><span style="font-size:10px;">{{ $cliente->nomefantasia }}</span></td></td>
                                    <td style="text-align: center;">{{ $cliente->iniciocontrato}}</td>
                                    <td style="text-align: center;">{{ $cliente->ultimoreajuste() }}</td>
                                    <td style="text-align: right">@if (is_object($cliente->faturamento($mes, $ano)))
                                        {{ number_format($cliente->faturamento($mes, $ano)->val_totalNota(), 2, ',', '.')}}
                                        @else
                                        --
                                        @endif
                                    </td>
                                    </tr>
                                @endforeach
                        </tbody>
                    </table>

                </div>
                @endif
            </div>
        </div>
    </article> <!-- /content page -->
@stop

@section('scripts')
<script>
  $(function(){
    $departamento = $('#id_departamento');
    $setor = $('#id_setor');
    $centrocusto = $('#id_centro');
    $cliente.change(function(){

       $('.load').show();
       $('#titulo').focus();
       $('.documentos').hide();

       // limpando combos
       $departamento.html('');
       $setor.html('');
       $centrocusto.html('');

       // ajax departamento
       $.get('publico/documento/ajaxdepartamento/'+$(this).val(),function(combo){
           $departamento.html(combo);
       });
       // ajax centro custo
       $.get('publico/documento/ajaxcentrocusto/'+$(this).val(),function(combo){
           $centrocusto.html(combo);
       });
       // ajax setor
       $.get('publico/documento/ajaxsetor/'+$(this).val(),function(combo){
           $setor.html(combo);
       });

       // Remove o load após 2.5 segundos
       setInterval(function(){
           $('.load').hide();
       },2500);

    }).focus();
  });
</script>
@stop