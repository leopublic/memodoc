<table border='0' cellspacing='0' width="100%" style='border:1px solid #000;font:12px arial; font-family:arial;'>
     <tr>
         <td width="80%" style="font-size:20px; font-weight: bold; vertical-align: top; padding:0;">
             <img src='../public/img/logo_subtitulo.jpg' style="float:left; vertical-align: text-top; " />
             @if(isset($brtitulo))
             {{$brtitulo}}
             @else
             &nbsp;&nbsp;&nbsp;
             @endif
             {{$titulo}}
             <br />

             <table width='100%' style="margin:0; font-size:14px;">
                 <tr>
                     <th align='right' style='width:120px !important'>
                        <b>Razão Social:</b>
                     </th>
                     <td>
                         {{$os->cliente->razaosocial}}
                     </td>
                 </tr>
                 <tr>
                     <th align='right'>
                        <b> <b>Nome fantasia:</b></b>
                     </th>
                     <td>
                         {{$os->cliente->nomefantasia}}
                     </td>
                 </tr>
                 <tr>
                     <th>Endereço:</th>
                     <td>{{$os->enderecoEntregaRelatorios()}}</td>
                 </tr>
                 <tr>
                     <th align='right'>
                        <b>Contato:</b>
                     </th>
                     <td>
                        {{$os->responsavel}}
                     </td>
                 </tr>
                 <tr>
                     <th align='right'>
                       <b>Departamento:</b>
                     </th>
                     <td>
                        {{isset($os->departamento->nome) ? $os->departamento->nome : '' }}
                     </td>
                 </tr>
                 <tr>
                     <th align='right'>
                       <b>Telefones:</b>
                     </th>
                     <td><? $sep = ''; ?>
                           @foreach($os->cliente->telefonecliente as $tel)
                                {{$sep.$tel->numero}}
                                <? $sep = ' / '; ?>
                           @endforeach
                     </td>
                 </tr>
             </table>
         </td>
         <td style="vertical-align: top; width:220px;">
             <table>
                 <tr>
                     <th align='right'><b>Ordem de serviço: </b></th>
                     <td> {{$os->getNumeroFormatado()}}</td>
                 </tr>
                 <tr>
                     <th align='right'><b>Solicitado em:</b></th>
                     <td> {{substr($os->solicitado_em, 0, 10)}}</td>
                 </tr>
                 <tr>
                     <th align='right'><b>Entrega em:</b></th>
                     <td> {{$os->entregar_em}}</td>
                 </tr>
                 <tr>
                     <th align='right'><b>Período:</b> </th>
                     <td>{{($os->entrega_pela) ? 'Tarde' : 'Manhã' }} </td>
                 </tr>
                 <tr>
                     <th align='right'><b>Urgente:</b> </th>
                     <td>{{($os->urgente) ? 'SIM' : 'NÃO' }} </td>
                 </tr>
                 <tr>
                     <th colspan="2">
                         @if ($os->tipodeemprestimo == 0)
                         Entrega MEMODOC
                         @elseif ($os->tipodeemprestimo == 1)
                         Consulta no local
                         @else
                         Retirado pelo cliente
                         @endif
                     </th>
                 </tr>
             </table>
         </td>
     </tr>
 </table>
