<table  data-sorter="true" class='table table-striped table-condensed' id="tabela-emprestimos">
    <thead>
         <th style="width:30px;text-align:center">#</th>
         <th style="width:60px;text-align:center">Ações</th>
         <th style="width:80px;text-align: center;">Referência</th>
         <th style="width:50px;text-align: center;">ID</th>
         <th style="width:120px;text-align: center;">Endereço</th>
         <th style="width:auto;">Comentário</th>
         <th style="width:200px;text-align: center;">Data checkout</th>
         <th style='width:auto'>Por</th>
</thead>
    <tbody>
        <? $i =1; ?>
        @foreach($emprestimos as $osdesmembrada)
            @include('os_emprestimo', array('osdesmembrada', $osdesmembrada   ))
           <? $i++; ?>
        @endforeach
    </tbody>
</table>
