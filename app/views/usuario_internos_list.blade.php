@extends('stilearn-metro')

@section('conteudo')

    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>{{$titulo}}</h1></div>

        <div class='content-action pull-right'>
           @if(isset($linkCriacao))
            <a href='/{{$linkCriacao}}' class='btn'>Novo</a>
            @endif
        </div>
    </header> <!--/ content header -->

    <!-- content page -->
    <article class="content-page clearfix">

        <!-- main page -->
        <div class="main-page">
            <div class="content-inner">

                @if (Session::has('msg'))
                <div class="alert alert-info"><h4>Mensagem</h4>{{ Session::get('msg') }}</div>
                @endif

                @if (Session::has('success'))
                <div class="alert alert-success"><h4>Sucesso!</h4>{{ Session::get('success') }}</div>
                @endif

                @if (Session::has('error'))
                <div class="alert alert-error"><h4>Erro!</h4>{{ Session::get('error') }}</div>
                @endif

                <!-- search box -->
                <div class="search-box control-rows row-fluid">
                    <form class="form-vertical" id="form-search">
                        <div class="span2 control-label">Nome</div>
                        <div class="span3">
                                <div class="controls">
                                    <input class="input" name="search" placeholder="Buscar..." value="{{Input::old('search')}}" type="text" />
                                </div>
                        </div>
                        <div class="span2">
                            <button class="btn bg-cyan">
                                <i class="aweso-search"></i>
                            </button>
                        </div>
                    </form>
                </div><!-- /search box -->

                <!-- List -->
                {{$instancias->links()}}
                <table class="listagem table table-hover table-striped table-condensed" data-sorter="true" style="width:100%">
                        <thead>
                                <tr>
                                        @if(isset($linkEdicao))
                                                <th class="c">Ações</th>
                                        @endif
                                        <th>Nome</th>
                                        <th>Login</th>
                                        <th>Nível</th>
                                        <th>Admin ?</th>

                                </tr>
                        </thead>
                        <tbody>
                                @foreach ($instancias as $r)
                                <tr>

                                        @if(isset($linkEdicao) or isset($linkRemocao))
                                                <td class="c">
                                                    @if(isset($linkEdicao))
                                                        <a class="btn btn-primary" title="Editar" href="/{{ $linkEdicao }}/{{ $r->getKey() }}"><i class="aweso-pencil"></i></a>
                                                    @endif
                                                    @if(isset($linkRemocao))
                                                        @include('padrao.link_exclusao', array('url'=> $linkRemocao .'/'. $r->getKey()  , 'msg_confirmacao'=>'Clique para confirmar a exclusão do usuário "'.$r->nomeusuario.'"') )
                                                    @endif
                                                </td>
                                        @endif
                                        <td>{{$r->nomeusuario}}</td>
                                        <td>{{$r->username}}</td>
                                        <td>{{is_object($r->nivel) ? $r->nivel->descricao : '' }}</td>
                                       <td>{{($r->adm) ? '<input type="checkbox" checked="true" disabled="true" />' : '<input type="checkbox" disabled="true" />'}}</td>

                                </tr>
                                @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </article> <!-- /content page -->
    <style>
        .c{
            width:200px;
            text-align:center;
        }
    </style>
@stop