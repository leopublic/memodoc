@extends('stilearn-metro')

@section('conteudo')
<style>
    .controls{ margin-right: 20px;}
    .date,.number,.alfa{width:90px}
    .table{font-size:11px}
    #invisivel{display:none}
    .select2-container .select2-choice{margin-bottom: 10px;}
</style>

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Reservas</h1></div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">

            @include('padrao.mensagens')
            <!-- search box -->
        {{Form::model($model,array('id'=>'frmreservas', 'class' => 'form-horizontal'))}}
            {{Form::hidden('gerar_excel', "", array('id' => 'gerar_excel'))}}
            <div class="search-box">
                <div class="row-fluid">
                    <div class="span6">
                        <div class='control-group'>
                            {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                            <div class="controls">
                                {{Form::select('id_cliente', $clientes, null, array('data-fx'=>'select2', 'style'=>'width:600px;','id'=>'id_cliente'))}}
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class='control-group'>
                            {{Form::label('inicio','Criadas entre',array('class'=>'control-label'))}}
                            <div class="controls">
                                <div class="input-append input-append-inline ">
                                    {{Form::text('inicio', null ,array('placeholder'=>"Data início", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                    <span class="add-on"><i class="icomo-calendar"></i> </span>
                                </div>
                                <div class="input-append input-append-inline ">
                                    {{Form::text('fim', null ,array('placeholder'=>"Data Final", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                    <span class="add-on"><i class="icomo-calendar"></i> </span>
                                </div>
                            </div>
                        </div>
                        <div class='control-group'>
                            {{Form::label('cancelada','Exibir canceladas',array('class'=>'control-label'))}}
                            <div class="controls">
                                <div class="checkbox-rounded help-block">
                                    {{Form::checkbox('cancelada', '1')}} 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class='control-group'>
                            <div class="controls">
                                <button class="btn bg-cyan submit-spinner" type="button" onClick="javascript:$('#gerar_excel').val('0');$('#frmreservas').attr('target', '_self');$('#frmreservas').submit();"><i class="aweso-search"></i> Buscar</button>
                                <button class="btn bg-green" type="button" onClick="javascript:$('#gerar_excel').val('1');$('#frmreservas').attr('target', '_blank');$('#frmreservas').submit();" target="_blank"><i class="aweso-table"></i> Gerar excel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{Form::close()}}
            @if (is_object($reservas))
            <div class="row-fluid">
                {{$reservas->links()}}
                <table class="table table-hover  table-striped table-condensed pull-left"  data-sorter="true"  style='width:100%'>
                    <thead>
                        <tr>
                            <th style='width:250px;text-align:center;'>Ações</th>
                            <th style='width:10px;text-align: center;'>ID</th>
                            <th style='width:100px;text-align: center;'>Criada em</th>
                            <th style='width:auto;'>Criada por</th>
                            <th style='width:120px'>Documento</th>
                            <th style='width:50px;text-align: right;'>Qtd</th>
                            <th style='width:50px;text-align: center;'>Caixa inicial</th>
                            <th style='width:50px;text-align: center;'>Caixa final</th>
                            <th style='width:100px;text-align: center;'>Ordem de serviço</th>
                            <th style='width:150px;text-align: center;'>Cancelada em</th>
                            <th style='width:auto;'>Cancelada por</th>
                            <th style='width:auto;'>Observações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($reservas as $r)
                        <tr class="{{($r->cancelada) ? 'error' : ''}}">
                            <td style="text-align:center;">
                            @if( $r->cancelada)
                                <i class="aweso-remove"></i>
                                @else
                                <a class="btn silver" href="/reserva/visualizar/{{ $r->id_reserva }}" target="_blank" title="Ver detalhes"><i class="aweso-edit"></i></a>
                                <a class="btn silver" href="/etiqueta/gere/{{ $r->id_reserva }}" target="_blank" title="Imprimir etiquetas"><i class="aweso-tags"></i></a>
                                <a class="btn silver" href="/reserva/inventario/{{ $r->id_reserva }}" target="_blank" title="Imprimir planilha"><i class="aweso-edit"></i></a>
                                <a class="btn silver" href="/reserva/relatorio/{{ $r->id_reserva }}" target="_blank" title="Imprimir relatório"><i class="aweso-print"></i></a>
                                @include('padrao.link_exclusao', array('url'=> '/reserva/cancelar/'. $r->id_reserva , 'msg_confirmacao'=>'Clique aqui para confirmar o cancelamento dessa reserva'))
                                @if (Auth::user()->nivel->minimoAdministrador())
                                    @include('padrao.link_exclusao', array('url'=> '/reserva/deletar/'. $r->id_reserva , 'cor' => 'btn-primary', 'msg_confirmacao'=>'Clique aqui para confirmar a exclusão sem bloqueio de etiquetas dessa reserva'))
                                @endif
                            @endif
                            </td>
                            <td style="text-align: center;">{{substr('000000'.$r->id_reserva, -6)}}</td>
                            <td style="text-align: center;">{{$r->formatDate('data_reserva')}}</td>
                            <td>
                                @if ($r->nomeusuario!= '')
                                {{$r->nomeusuario}}
                                @else
                                --
                                @endif
                            </td>
                            <td>{{isset($r->descricao) ? $r->descricao : '' }}</td>
                            <td style="text-align: right;">{{$r->caixas}}</td>
                            <td style="text-align: center;">{{$r->caixa_inicial}}</td>
                            <td style="text-align: center;">{{$r->caixa_final}}</td>
                            <td style="text-align: center;">
                                @if ($r->id_os != '')
                                <a href="{{URL::to('/ordemservico/visualizar/'.$r->id_os_chave)}}" target="_blank" title="ver a os">{{substr('000000'.$r->id_os, -6)}}</a>
                                @else
                                --
                                @endif
                            </td>
                            <td style="text-align: center;">{{$r->data_cancelamento}}</td>
                            <td>
                                @if ($r->nomeusuario_cancelamento != '')
                                {{$r->nomeusuario_cancelamento}}
                                @else
                                --
                                @endif
                            </td>
                            <td style="text-align: left;">{{$r->observacao}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$reservas->links()}}
            </div>
            @endif
        </div>
    </div>
</article> <!-- /content page -->
@stop

@section('scripts')
<script>
    $(function () {
        $cliente = $('#id_cliente');
        $cliente.change(function () {
            $('.load').show();
            // Remove o load após 2.5 segundos
            setInterval(function () {
                $('.load').hide();
            }, 500);
        }).focus();
    });
</script>

@stop