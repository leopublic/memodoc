<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link href="/stilearn-metro/css/bootstrap.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/bootstrap-responsive.css" rel="stylesheet" />
        <!-- default theme -->
        <link href="/stilearn-metro/css/metro-bootstrap.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/metro.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/metro-responsive.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/font-awesome.css" rel="stylesheet" />
        <title>:: MEMODOC ::</title>
        <style>
            body, html{
                height: 100%;
            }
            body {
                font-family: 'Verdana', Arial, sans-serif;
                margin:0;
                padding:0;
            }

            div#barra{
                background-color: transparent;
                background-color: #00b0ca;
                border-top: solid 3px #777;
            }

            h1{
                margin: 0;
                padding: 0;
                font-family: inherit;
                font-size:24px;
            }

            div#barra h1{
                margin-left: 10px;
                display: inline;
            }
            div#interno{
                border-radius: 10px;
                -webkit-border-radius: 10px;
                box-shadow: 0px 0px 15px -2px #c7c7c7;
                width:820px;
                margin:50px auto;
            }
            div#header{
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;

            }
            div#header2{
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                background-color: #00b0ca;
                border-top:solid 1px #00E2C0;
                border-left:solid 1px #00E2C0;
                border-right:solid 1px #00E2C0;
                padding:10px;
            }
            div#conteudo h1{
                font-weight: normal;
                margin-bottom:10px;
                margin-top: 15px;
            }
            div#conteudo input{
                padding: 10px 15px;
                margin: 5px 0 20px 0;
                border: 1px solid #e8e8e8;
                font: 1.2em Arial, sans-serif;
                color: #7a7a7a;
                background: #f5f5f5;
                font-size: 15px;
                width: 89%;
                border-radius: 5px;
            }
            div#conteudo p{
                margin:0;
            }
            div#conteudo p.submit{
                display:inline;
            }
            div#conteudo p.submit input{
                width:auto;
            }

            div#interno div#conteudo{
                padding:10px;
            }
            div#conteudo span.error{
                color:red;
            }
            div.interno h1{
                font-family: Verdana, Arial, sans-serif;
                color: #555;
                font-weight: normal;
            }
            a, a:visited {
                color: #006E7C;
                text-decoration:none;
            }

            a:hover {
                text-decoration:underline;
            }

            table{
                margin: auto;
                width: 100%;
                border-collapse: collapse;
                font-family: inherit;
                color: #555;
                border-bottom: solid 1px #ccc;
            }

        </style>

    </head>
    <body style="background-image:none; background-color:#fff;">
        <div id="interno" style="margin-bottom:0 !important; margin-top:10px !important;">
            <div id="conteudo">
                <img src="../img/home_head.jpg"/>
                <table style="width:800px; margin-top:10px;">
                    <tr>
                        <td style="width:40px;text-align: center; color:blue; "><i class="aweso-lock aweso-2x"></i></td>
                    	<td style="width:80px;text-align: left; color:blue; font-weight:bold; font-size:14px;">Acesso restrito</td>
                        <td style="font-size:11px;">Este conteúdo é reservado a clientes MEMODOC cadastrados, caso você esteja cadastro efetue o login informando seus dados abaixo. No caso de não possuir cadastro, entre em contato com o setor de suporte.</td>
                    </tr>
                </table>
                {{ Form::open() }}
                <table style="width:500px; ">
                    <tr>
                        <td style="width:100px; vertical-align: middle ;">E-mail</td>
                        <td>{{ Form::text('username') }}</td>
                    </tr>
                    <tr>
                        <td style="width:100px;vertical-align: middle;">Senha</td>
                        <td>{{ Form::password('password') }}</td>
                    </tr>
                </table>
                <div style="margin:auto; text-align: center;">
                    <p class="submit">{{ Form::submit('Entrar') }}</p>&nbsp;&nbsp;<p class="submit"><input type="button" onClick="window.location ='{{ URL::to('/auth/reenviarsenha') }}';" value='Esqueci a senha'/></p>
                </div>
                {{ Form::close() }}
                <!-- submit button -->
                @if (Session::has('msg'))
                <div style="margin:auto; text-align: center; color:red; ">
                    <span class="error">{{ Session::get('msg') }}</span>
                </div>
                @endif
                <div style="margin:auto; text-align: center;color: #999; font-size:11px;">
                    Para uma melhor experiência, recomendamos acessar o site usando o <a href="https://www.google.com/intl/pt-BR/chrome/browser/" target="_blank">Chrome Browser</a>
                </div>

            </div>
        </div>
            <div style="width:800px; margin:auto; margin-top:5px; text-align: left; font-size:12px; font-weight: normal; ">
            	@if (isset($aviso))
	            	{{ $aviso->texto }}
    	            <br/><br/>Atenciosamente,<br/>
                    @if((date('m') == 11 && date('d') > 27) || date('m') == 12)
                    <img src="{{asset('img/logo_natal.jpg')}}"/>
                    @else
                    <img src="{{asset('img/logo.jpg')}}"/>
                    @endif
                @endif
            </div>
            <div id="myModal" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"  style="width:700px; margin-left:-320px;">
              <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Atenção</h4>
                  </div>
                  <div class="modal-body">
            	@if (isset($aviso))
                    {{$aviso->texto_popup}}
				@endif
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                  </div>
                </div>

              </div>
            </div>
        <?php echo HTML::script('js/jquery-1.9.1.js'); ?>
        <?php echo HTML::script('js/script.js'); ?>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('/stilearn-metro/js/bootbox/bootbox.min.js')}}"></script>
        <script>
    @if (isset($aviso) && $aviso->texto_popup != '')
    $(document).ready(function() {
        $('#myModal').modal('show');
    });
    @endif
        </script>
    </body>
</html>
