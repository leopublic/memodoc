@extends('stilearn-metro')

@section('conteudo')

    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Configurações de etiqueta</h1></div>


        <div class='content-action pull-right'>
            <a href='/configetiqueta/edit/0' class='btn'>Novo</a>
        </div>
    </header> <!--/ content header -->

    <!-- content page -->
    <article class="content-page clearfix">

        <!-- main page -->
        <div class="main-page">
            <div class="content-inner">
                @include ('padrao/mensagens')
                <!-- List -->
                <table class="listagem table table-hover table-striped table-condensed" data-sorter="true" style="width:100%">
                    <thead>
                        <tr>
                                <th class="c" style="width:100px;">Ações</th>
                            <th style="text-align: center;">id_config</th>
                            <th style="text-align: center;">fl_default</th>
                            <th style="text-align: center;">largura</th>
                            <th style="text-align: center;">altura</th>
                            <th style="text-align: center;">fontsize_titulo</th>
                            <th style="text-align: center;">fontsize_texto</th>
                            <th style="text-align: center;">fontsize_endereco</th>
                            <th style="text-align: center;">fontsize_id</th>
                            <th style="text-align: center;">barcode_size</th>
                            <th style="text-align: center;">barcode_height</th>
                            <th style="text-align: center;">barcode_pr</th>
                            <th style="text-align: center;">doc_largura</th>
                            <th style="text-align: center;">doc_altura</th>
                            <th style="text-align: center;">doc_margem_left</th>
                            <th style="text-align: center;">doc_margem_right</th>
                            <th style="text-align: center;">doc_margem_top</th>
                            <th style="text-align: center;">doc_margem_bottom</th>
                            <th style="text-align: center;">doc_margem_header</th>
                            <th style="text-align: center;">doc_margem_footer</th>
                            <th style="text-align: center;">largura_td_barras</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($configs as $config)
                        <tr>
                            <td class="c">
                                <a class="btn btn-primary" title="Editar" href="/configetiqueta/edit/{{ $config->id_config }}"><i class="aweso-pencil"></i></a>
                                    @include('padrao.link_exclusao', array('url'=> '/configetiqueta/del/'. $config->id_config  , 'msg_confirmacao'=>'Clique para confirmar a exclusão da configuracão') )
                            </td>
                            <td style="text-align: center;">{{$config->id_config}}</td>
                            <td style="text-align: center;">{{$config->fl_default}}</td>
                            <td style="text-align: center;">{{$config->largura}}</td>
                            <td style="text-align: center;">{{$config->altura}}</td>
                            <td style="text-align: center;">{{$config->fontsize_titulo}}</td>
                            <td style="text-align: center;">{{$config->fontsize_texto}}</td>
                            <td style="text-align: center;">{{$config->fontsize_endereco}}</td>
                            <td style="text-align: center;">{{$config->fontsize_id}}</td>
                            <td style="text-align: center;">{{$config->barcode_size}}</td>
                            <td style="text-align: center;">{{$config->barcode_height}}</td>
                            <td style="text-align: center;">{{$config->barcode_pr}}</td>
                            <td style="text-align: center;">{{$config->doc_largura}}</td>
                            <td style="text-align: center;">{{$config->doc_altura}}</td>
                            <td style="text-align: center;">{{$config->doc_margem_left}}</td>
                            <td style="text-align: center;">{{$config->doc_margem_right}}</td>
                            <td style="text-align: center;">{{$config->doc_margem_top}}</td>
                            <td style="text-align: center;">{{$config->doc_margem_bottom}}</td>
                            <td style="text-align: center;">{{$config->doc_margem_header}}</td>
                            <td style="text-align: center;">{{$config->doc_margem_footer}}</td>
                            <td style="text-align: center;">{{$config->largura_td_barras}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </article> <!-- /content page -->
    <style>
        .c{
            width:200px;
            text-align:center;
        }
    </style>
@stop