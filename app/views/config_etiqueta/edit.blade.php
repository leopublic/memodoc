@extends('base')

@section('estilos')
    @include('_layout.estilos-forms')
@stop

@section('conteudo')
    @include('_layout.page-header', array('title'=>'Editar usuário'))
    @include('_layout.msg')
<!-- inicio conteúdo -->
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-user"></i>Informe as características do usuário</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form">
                {{Form::model($objeto, array('class' => 'form-horizontal'))}}
                {{Form::hidden('id_config', $objeto->id_config)}}
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'altura'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'largura'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'fontsize_titulo'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'fontsize_texto'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'fontsize_endereco'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'fontsize_id'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'barcode_height'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'barcode_size'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'barcode_pr'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'doc_largura'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'doc_altura'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'doc_margem_left'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'doc_margem_right'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'doc_margem_top'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'doc_margem_bottom'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'doc_margem_header'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'doc_margem_footer'))
                            @include('padrao.text', array('objeto' => $objeto, 'nome' => 'largura_td_barras'))
                        </div>
                    </div>
                </div>
                <!-- Fim form-body -->
                <!-- INICIO FORM-ACTIONS -->
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-offset-3 col-md-9">
                                <input type="submit" class="btn btn-primary" id="submit-pesquisa" value="Salvar"></>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- FIM FORM-ACTIONS -->
                {{ Form::close() }}
            </div>
            <!-- Fim portlet-body -->
        </div>
        <!-- Fim portlet -->
    </div>
    <!-- fim  da coluna unica -->    
</div>
                        
@stop

@section('scripts')
    @include('_layout.scripts-forms')
@stop