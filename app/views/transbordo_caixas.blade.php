@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Transbordo {{substr('000000'.$transbordo->id_transbordo, -6)}}</h1></div>
    <div class='content-action pull-right'>
        <a class="btn btn-sm btn-warning" href="/etiqueta/gere/{{ $transbordo->id_reserva }}" target="_blank" title="Imprimir etiquetas"><i class="aweso-tags"></i> Etiquetas novas</a>
        <a href='/reserva/visualizar/{{ $transbordo->id_reserva }}' class='btn btn-warning' target="_blank"><i class="aweso-magic"></i><i class="aweso-archive"></i> Ver reserva</a>
        <a href='/expurgo/caixasgravadas/{{ $transbordo->id_expurgo_pai }}' class='btn btn-danger' target="_blank"><i class="aweso-remove"></i> Ver expurgo</a>
        <a href='/transbordo/depara/{{$transbordo->id_transbordo}}' class='btn bg-lime' target="_blank"><i class="aweso-print"></i> De->Para</a>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-share-alt"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Dados do transbordo</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                   {{Form::open(array('class' => 'form-horizontal'))}}

                   <div class='control-group'>
                        {{Form::label('id_cliente','Cliente origem',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('cliente_antes', $transbordo->clienteantes->razaosocial.' ('.$transbordo->clienteantes->id_cliente.')' , array('disabled' => 'disabled',   'class' => 'input-block-level'))}}
                        </div>
                    </div>
                   <div class='control-group'>
                        {{Form::label('id_os','OS',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('id_os', substr('000000'.$os->id_os, -6) , array('class' => 'input-medium', 'disabled'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente destino',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('cliente_depois', $transbordo->clientedepois->razaosocial.' ('.$transbordo->clientedepois->id_cliente.')' , array('disabled' => 'disabled',   'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('cortesia_movimentacao','Cortesia movimentação?',array('class'=>'control-label'))}}
                        {{Form::hidden('cortesia_movimentacao', '0') }}
                        <div class="controls">
                            {{Form::checkbox('cortesia_movimentacao', 1, $transbordo->cortesia_movimentacao, array('disabled' => 'disabled')) }}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('observacao','Observações',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::textarea('observacao', Input::old('observacao'), array('rows' => 4, 'class' => 'input-block-level', 'disabled'=>'disabled'))}}
                        </div>
                    </div>
                    {{Form::close()}}
                    @if (isset($caixas))
                    <table  data-sorter="true" class='table table-hover table-striped table-condensed table-bordered'>
                        <thead>
                            <tr>
                                <th colspan="3" style="width:auto; text-align:center;">DE</th>
                                <th style="width:auto; text-align:center; border-top: none; border-bottom: none;">&nbsp;</th>
                                <th colspan="3" style="width:auto; text-align:center;">PARA</th>                                
                            </tr>
                            <tr>
                                <th style="width:auto; text-align:center;">ID</th>
                                <th style="width:auto; text-align:center;">Referência</th>
                                <th style="width:auto; text-align:center;">Endereço</th>
                                <th style="width:auto; text-align:center; border-top: none; border-bottom: none;">&nbsp;</th>
                                <th style="width:auto; text-align:center;">ID</th>
                                <th style="width:auto; text-align:center;">Referência</th>
                                <th style="width:auto; text-align:center;">Endereço</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($caixas as $caixa)
                            <tr class="">
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa_antes, -6)}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao_antes, -6)}}</td>
                                <td style="text-align: center;">{{$caixa->enderecoantes->enderecoCompleto()}}</td>
                                <th style="width:auto; text-align:center; border-top: none; border-bottom: none;"><i class="aweso-arrow-right aweso-2x"></i></th>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixa_depois, -6)}}</td>
                                <td style="text-align: center;">{{substr('000000'.$caixa->id_caixapadrao_depois, -6)}}</td>
                                <td style="text-align: center;">{{$caixa->enderecodepois->enderecoCompleto()}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop