@extends('stilearn-metro')

@section('conteudo')

<!-- content header -->
<header class="content-header"><h1>SICOOB - Gerar boletos</h1></div>

    <!-- content breadcrumb -->
    <div class='content-action pull-right'>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">

            @include('padrao.mensagens')

            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-search"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Gerar uma nova remessa</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content">
                    {{ Form::open(array('class'=> 'form-horizontal', 'files'=> true, 'url'=> '/sicoobretorno/novaconsulta')) }}
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label">Lote de notas</label>
                                <div class="controls">
                                    {{Form::select('id_lote_rps', $lotes_rps, null, array('id'=>'id_lote_rps','data-fx'=>'select2', 'required', 'class' => 'input-block-level'))}}
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" class="btn bg-orange">Enviar</button>
                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .c{
        width:200px;
        text-align:center;
    }
</style>
@stop