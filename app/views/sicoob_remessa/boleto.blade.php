<html>
    <head>
        <style>
            table{
                border-collapse:collapse;
            }
            td{
                border: solid 1px #000000;
                font-family: arial;
                font-size:11px;
            }
            tr.def td{
                border: none;
            }
            td.cheia{
                border-top:none;
                border-bottom:none;
            }
            tr.topo td{
                border-bottom: none;
                font-weight: bold;
                padding-bottom: 4px;
            }
            tr.base td{
                border-top: none;
                font-size:12px;
            }
            td.fundo-cinza{
                background-color: #cccccc;
            }
        </style>
    </head>
    <body>
        <table>
        <!-- Define colunas -->
            <tr class="def">
                <td width="200px">&nbsp;</td>
                <td width="80px">&nbsp;</td>
                <td width="80px">&nbsp;</td>
                <td width="80px">&nbsp;</td>
                <td width="80px">&nbsp;</td>
                <td width="150px">&nbsp;</td>
                <td width="250px">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7"><img src="/img/logo.jpg"/></td>
            </tr>
            <tr>
                <td colspan="7" class="cheia">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" class="cheia">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" class="cheia">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" class="cheia">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" class="cheia">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" class="cheia">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" class="cheia">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" class="cheia">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" class="cheia">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" class="cheia">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="7" class="cheia">&nbsp;</td>
            </tr>

        <!-- Linha A - Banco e linha digitável -->
            <tr class="base">
                <td colspan="2"><img src=""/></td> <!-- Logo -->
                <td style="font-size:22px; font-weight:bold">756-0</td> <!-- Banco -->
                <td colspan="4">{{ $boleto->linha_digitavel }}</td> <!-- Linha digitavel -->
            </tr>
        <!-- Linha B título -->
            <tr class="topo">
                <td colspan="6">Local de pagamento</td> <!-- Local de pagamento -->
                <td class="fundo-cinza">Vencimento</td> <!-- Vencimento -->
            </tr>
        <!-- Linha B conteúdo -->
            <tr class="base">
                <td colspan="6">Pagável em qualquer banco até a data do vencimento</td> <!-- Local de pagamento -->
                <td class="fundo-cinza">{{ $boleto->dt_vencimento }}</td> <!-- Vencimento -->
            </tr>
        <!-- Linha C título -->
            <tr class="topo">
                <td colspan="6">Beneficiário</td>
                <td class="fundo-cinza">Agência/Código Beneficiário</td>
            </tr>
        <!-- Linha C conteúdo -->
            <tr class="base">
                <td colspan="6">{{ $boleto->beneficiario_nome }}</td>
                <td class="fundo-cinza">{{ $boleto->beneficiario_codigo }}</td>
            </tr>
        <!-- Linha D título -->
            <tr class="topo">
                <td >Data do documento</td>
                <td colspan="2">N. documento</td>
                <td >Espécie</td>
                <td >Aceite</td>
                <td >Dt processamento</td>
                <td >Nosso número</td>
            </tr>
        <!-- Linha D conteudo -->
            <tr class="base">
                <td >{{ $boleto->dt_documento }}</td>
                <td colspan="2">{{ $boleto->num_documento }}</td>
                <td >{{ $boleto->especie }}</td>
                <td >{{ $boleto->aceite }}</td>
                <td >{{ $boleto->dt_processamento }}</td>
                <td >{{ $boleto->nosso_numero }}</td>
            </tr>
        <!-- Linha E título -->
            <tr class="topo">
                <td class="fundo-cinza">Uso do banco</td>
                <td>Carteira</td>
                <td >Espécie</td>
                <td colspan="2" >Quantidade</td>
                <td >Valor</td>
                <td >Valor Documento</td>
            </tr>
        <!-- Linha E título -->
            <tr class="base">
                <td class="fundo-cinza">&nbsp;</td>
                <td>{{ $boleto->carteira }}</td>
                <td>{{ $boleto->especie }}</td>
                <td colspan="2" >{{ $boleto->quantidade }}</td>
                <td >{{ $boleto->valor }}</td>
                <td >{{ $boleto->valor_documento }}</td>
            </tr>
        <!-- Linha F título -->
            <tr class="topo">
                <td colspan="6">Instruções</td>
                <td>(-)Desconto/abatimento</td>
            </tr>
        <!-- Linha F conteudo -->
            <tr class="base">
                <td colspan="6" rowspan="5">{{ $boleto->instrucoes }}</td>
                <td>{{ $boleto->desconto }}</td>
            </tr>
        <!-- Linha H título -->
            <tr class="topo">
                <td>(-)Outras deduções</td>
            </tr>
        <!-- Linha H conteudo -->
            <tr class="base">
                <td>{{ $boleto->outras_deducoes }}</td>
            </tr>
        <!-- Linha I título -->
            <tr class="topo">
                <td>(+)Mora/multa</td>
            </tr>
        <!-- Linha i conteudo -->
            <tr class="base">
                <td>{{ $boleto->mora_multa }}</td>
            </tr>
        <!-- Linha j título -->
            <tr class="topo">
                <td colspan="6">Pagador</td>
                <td>(+)Outros acréscimos</td>
            </tr>
        <!-- Linha j conteudo -->
            <tr class="base">
                <td colspan="6" rowspan="3">{{ $boleto->pagador }}</td>
                <td>{{ $boleto->outros_acrescimos }}</td>
            </tr>
        <!-- Linha H título -->
            <tr class="topo">
                <td>Valor cobrado</td>
            </tr>
        <!-- Linha H conteudo -->
            <tr class="base">
                <td>{{ $boleto->valor_cobrado }}</td>
            </tr>
        </table>
    </body>
</html>