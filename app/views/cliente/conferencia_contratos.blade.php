@extends('stilearn-metro')

@section('conteudo')

<header class="content-header">
    <!-- Título -->
    <div class="page-header"><h1>{{ $titulo }}</div>
</header>

<article class="content-page clearfix">

    <div class="main-page">
        <div class="content-inner">

			@include ('padrao/mensagens')

			<div class="row-fluid">
				<div class="widget border-amber span12" id="widget-horizontal">
					<!-- widget header -->
					<div class="widget-header bg-amber">
						<div class="widget-icon">
							<i class="aweso-archive"></i>
						</div>
						<h4 class="widget-title">Registros encontrados</h4>
					</div>
					<!-- widget content -->
					<?php $i = 0; ?>
					<div class="widget-content">
						<table data-sorter="true" class='table table-striped table-condensed'>
							<tr>
								<th style="text-align: center; ">#</th>
								<th style="text-align: center; width: 80p;x">Ações</th>
								<th style="text-align: center;">ID</th>
								<th style="text-align:center;">Início contrato</th>
								<th style="">Cliente</th>
								<th style="text-align: center;">{{ $titulo_campo }}</th>
							</tr>
                                @foreach($registros as $cliente)
                                <?php $i++; ?>
                                <tr>
								<td style="text-align: center;">{{ $i }}</td>
								<td style="text-align: center;">
									<a class="btn silver" href="/cliente/contrato/{{ $cliente->id_cliente }}#tab_contrato" target="_blank" title="Editar"><i
										class="aweso-edit"></i></a>
								</td>
								<td style="text-align: center;">{{ $cliente->id_cliente }}</td>
								<td style="text-align: center;">{{ $cliente->iniciocontrato}}</td>
								<td style="">{{ $cliente->nomefantasia }}</td>
								<td style="text-align: center;">{{ number_format($cliente->{$nomecampo},2,",", ".") }}</td>
							</tr>
							@endforeach
						</table>
					</div>
					<!-- /widget content -->
				</div>
			</div>
        </div>
</article> <!-- /content page -->

@stop

@section('scripts')
<style>
    .date{
        width:96px;
    }
    .form-horizontal .control-group{
        margin-bottom:5px !important;
    }
</style>

<script>

</script>
@stop
