<head>
<style>
    br {mso-data-placement:same-cell;}
    td{border:solid thin #000; vertical-align: top; font-size: 10px;}
</style>
</head>
<body>
<table cellspacing="0" style="width:100%;border-collapse:collapse;">
            <colgroup>
                <col width="80px"/>
                <col width="auto"/>
                <col width="auto"/>
                <col width="600px"/>
                <col width="120px;"/>
                <col width="auto"/>
                <col width="auto"/>
                <col width="auto"/>
                <col width="auto"/>
                <col width="auto"/>
                <col width="auto"/>
            </colgroup>
            <thead>
                <tr>
                    <th style="text-align: center;">ID</th>
                    <th style="">Tipo pessoa</th>
                    <th style="">Razão Social</th>
                    <th style="">Fantasia</th>
                    <th style="">CNPJ/CPF</th>
                    <th style="">Início<br/>do contrato</th>
                    <th style="">Ativo</th>
                    <th style="text-align:center;">Tipo de<br/>atendimento</th>
                    <th style="text-align:center;">Movimentação<br/>de cortesia</th>
                    <th style="text-align:center;">Desconto<br/>custódia %</th>
                    <th style="text-align:center;">Limite de caixas<br/>para o desconto<br/>de custódia</th>
                    <th style="text-align:center;">Permite valor<br/>fixo nas OS</th>
                    <th style="text-align:center;">Franquia de custódia<br/>deve ser<br/>cobrada após</th>
                    <th style="text-align:center;">Franquia movimentação<br/>sobre o total<br/>de caixas (%)</th>
                    <th style="text-align:center;">ISS não<br/>incluído</th>
                    <th style="text-align:center;">Índice de<br/>reajuste</th>
                    <th style="text-align:center;">Tipo de<br/>faturamento</th>
                    <th style="text-align:center;">Exibir<br/>digitalizações</th>
                    <th style="text-align:center;">Controle de<br/>revisão de<br/>conteúdo</th>
                    <th style="text-align:center;">Em atraso<br/>desde</th>
                    <th style="text-align:center;">Nível de<br/>inadimplencia</th>
                    <th style="text-align:center;">Abertura<br/>de OS<br/>restrita<br/>ao ADM</th>
                    <th style="text-align:center;">Emissão<br/>de NF<br/>suspensa</th>
                    <th style="text-align:center;">Dia<br/>de faturamento</th>
                    <th style="text-align:center;">Complemento<br/>descrição<br/>serviços</th>
                </tr>
            </thead>
            <tbody>
			@foreach($clientes as $cliente)
                <tr>
                    <td style="text-align: center;">{{ substr('000'.$cliente->id_cliente, -3) }}</td>
                    <td style="text-align:center;">
            	        @if ($cliente->fl_pessoa_fisica== 1)
    		                Física
                    	@else 
                    		Jurídica
                    	@endif
                    </td>
                    <td style="">{{ $cliente->razaosocial }}</td>
                    <td style="">{{ $cliente->nomefantasia }}</td>
                    <td style="">{{ $cliente->cgc_formatado }}</td>
                    <td style="">{{ $cliente->iniciocontrato }}</td>
                    <td style="text-align:center;">
            	        @if ($cliente->fl_ativo== 1)
    		                Sim
                    	@else 
                    		Não
                    	@endif
                    </td>
                    <td style="text-align:center;">
            	        @if ($cliente->atendimento == 1)
    		                24 horas
	                    @elseif($cliente->atendimento == 0)
                    		12 horas
                    	@else 
                    		(não informado)
                    	@endif
                    </td>
                    <td style="text-align:center;">
            	        @if ($cliente->movimentacaocortesia == 1)
    		                Sim
                    	@else 
                    		Não
                    	@endif
                    </td>
                    <td style="text-align:center;">{{ number_format($cliente->descontocustodiapercentual, 2, ",", ".") }}</td>
                    <td style="text-align:center;">{{ number_format($cliente->descontocustodialimite, 2, ",", ".") }}</td>
                    <td style="text-align:center;">
            	        @if ($cliente->fl_frete_manual == 1)
    		                Sim
                    	@else 
                    		Não
                    	@endif
                    </td>
                    <td style="text-align:center;">{{ number_format($cliente->franquiacustodia, 2, ",", ".") }}</td>
                    <td style="text-align:center;">{{ number_format($cliente->franquiamovimentacao, 2, ",", ".") }}</td>
                    <td style="text-align:center;">
            	        @if ($cliente->issporfora == 1)
    		                Sim
                    	@else 
                    		Não
                    	@endif
                    </td>
                    <td style="text-align:center;">{{ $cliente->descricao_indice() }}</td>
                    <td style="text-align:center;">
            	        @if ($cliente->atendimento == 1)
    		                Correio
	                    @elseif($cliente->atendimento == 2)
                    		Email
                    	@else 
                    		(não informado)
                    	@endif
                    </td>
                    <td style="text-align:center;">
            	        @if ($cliente->fl_digitalizacao == 1)
    		                Sim
                    	@else 
                    		Não
                    	@endif
                    </td>
                    <td style="text-align:center;">
            	        @if ($cliente->fl_controle_revisao == 1)
    		                Sim
                    	@else 
                    		Não
                    	@endif
                    </td>
                    <td style="text-align:center;">
                	    @if($cliente->em_atraso_desde != '0000-00-00')
            	        	{{ $cliente->em_atraso_desde }}
        	            @else
    	                	--
	                    @endif
                    </td>
                    <td style="text-align:center;">
            	        @if ($cliente->atendimento == 1)
    		                Notificação
	                    @elseif($cliente->atendimento == 2)
                    		Bloqueio
                    	@else 
                    		(em dia)
                    	@endif
                    </td>
                    <td style="text-align:center;">
            	        @if ($cliente->fl_os_bloqueada == 1)
    		                Sim
                    	@else 
                    		Não
                    	@endif
                    </td>
                    <td style="text-align:center;">
            	        @if ($cliente->fl_emissaonf_suspensa == 1)
    		                Sim
                    	@else 
                    		Não
                    	@endif
                    </td>
                    <td style="text-align:center;">
            	        {{$cliente->dia_vencimento}}
                    </td>
                    <td style="text-align:left;">
            	        {{$cliente->descricao_servicos}}
                    </td>


                </tr>					
			@endforeach