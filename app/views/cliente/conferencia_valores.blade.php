<head>
<style>
br {
	mso-data-placement: same-cell;
}

td {
	border: solid thin #000;
	vertical-align: top;
	font-size: 12px;
}
.num{mso-number-format:"0.00"}
</style>
</head>
<body>
	<table cellspacing="0" style="width: 100%; border-collapse: collapse;">
			<tr><td colspan="8" style="font-size: 20px;font-weight: bold;">Comparativo de valores {{ $titulo }}</td></tr>
			<tr>
				<th style="text-align: center;">ID</th>
				<th style="">Razão Social</th>
				<th style="">Fantasia</th>
				<th style="">Início<br />do contrato</th>
				<th style="text-align: center;">Valor normal</th>
				<th style="text-align: center;">Valor urgência</th>
			@if($id_tipoproduto == 1)
				<th style="text-align: center;">Frete mínimo</th>
			@endif
			@if($id_tipoproduto == 3)
				<th style="text-align: center;">Faturamento mínimo</th>
			@endif
				<th style="text-align: center;">Qtd caixas cobradas</th>
				<th style="text-align: center;">Qtd caixas em carência (cobradas)</th>
			</tr>
			@foreach($clientes as $cliente)
			<tr>
				<td style="text-align: center;">{{ substr('000'.$cliente->id_cliente, -3) }}</td>
				<td style="">{{ $cliente->razaosocial }}</td>
				<td style="">{{ $cliente->nomefantasia }}</td>
				<td style="text-align: center;">{{ $cliente->iniciocontrato_fmt }}</td>
				<td class="num" style="text-align: center;">{{ number_format($cliente->valor_contrato, 2, ",", ".") }}</td>
				<td class="num" style="text-align: center;">{{ number_format($cliente->valor_urgencia, 2, ",", ".") }}</td>
			@if($id_tipoproduto == 1)
				<td class="num" style="text-align: center;">{{ number_format($cliente->minimo, 2, ",", ".") }}</td>
			@endif
			@if($id_tipoproduto == 3)
				<td class="num" style="text-align: center;">{{ number_format($cliente->valorminimofaturamento, 2, ",", ".") }}</td>
			@endif
				<td class="num" style="text-align: center;">{{ $cliente->qtd_caixas }}</td>
				<td class="num" style="text-align: center;">{{ $cliente->qtd_carencia }}</td>
			</tr>
			@endforeach
	</table>