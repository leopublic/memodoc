<div class="boxbform">
    @if(isset($many))
      {{ Form::model($many) }}  
    @else
      {{ Form::open(array('url'=>$_SERVER['REQUEST_URI'].'?tab=tab_emails')) }}    
    @endif
    <div class="box">
        <ul class="form"> 
            <li class='subtitle'>
                @if(isset($many))
                    Editar E-mail do cliente
                @else
                    Adicionar novo e-mail do cliente
                @endif
                {{ Form::submit('Salvar',array('class'=>'btn btn-primary pull-right')) }}
                {{ Form::hidden('tab','tab_emails')}}
            </li>
            <li>
                {{ Form::label('correioeletronico','Correio eletrônico') }} 
                {{ Form::text('correioeletronico') }}
            </li>
            <li>
                {{ Form::label('id_tipoemail','Tipo Email') }} 
                {{ Form::select('id_tipoemail', FormSelect($select['TipoEmail'],'descricao')) }}
            </li>
        </ul>    
    </div>
    {{ Form::close() }}  
</div>