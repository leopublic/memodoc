    <!-- Caixas emprestimo -->
<table class='table table-striped  table-condensed'>
    <thead>
        <tr>
            <th style='width:100px'>Caixa padrão</th>
            <th style="width:50px">Item</th>
            <th style='width:auto'>Título</th>
        </tr>
    </thead>
    <tbody class='caixa-content'>
    @if(count($osdesmembradas) > 0)
    @foreach($osdesmembradas as $osdesmembrada)
    <tr>
        <td>{{$osdesmembrada->id_caixapadrao}}</td>
        <td>{{$osdesmembrada->documento->id_item}}</td>
        <td>{{$osdesmembrada->documento->titulo}}</td>
    </tr>
    @endforeach
    @endif
    </tbody>
</table>
