@extends('base')

@section('estilos')
@include('_layout.estilos-forms')
@stop

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-10">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">{{$cliente->nomefantasia}}</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
    @if($qtd_itens_retirados > 0)
    <div class="col-md-2">
        <a href="/publico/os/itensretirados/{{$model->id_os_chave}}" target="_blank" class="btn btn-primary">Protocolo de itens retirados</a>
    </div>
    @endif
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio conteúdo -->
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-truck"></i>Ordem de serviço <strong>{{$model->getNumeroFormatado()}}</strong></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form grey-steel">
                {{Form::model($model, array('class' => 'form-horizontal'))}}
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->
                        <div class="col-md-6">
                            <div class="form-group">
                                {{Form::label('responsavel','Solicitado por:',array('class'=>'control-label  col-md-3 '))}}
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                        {{$model->responsavel}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('id_departamento','Departamento:',array('class'=>'control-label  col-md-3 '))}}
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                        @if (is_object($model->departamento))
                                        {{$model->departamento->nome}}
                                        @else
                                        --
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('id_endeentrega','Local entrega/retirada:',array('class'=>'control-label  col-md-3 '))}}
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                        @if (is_object($model->endeentrega))
                                        {{$model->endeentrega->completo()}}
                                        @else
                                        --
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('observacoes','Observações:',array('class'=>'control-label  col-md-3 '))}}
                                <div class="col-md-9">
                                    <p class="form-control-static">
                                        {{nl2br($model->observacao)}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- Fim primeira coluna-->
                        <!-- Inicio segunda coluna-->
                        <div class="col-md-6">
                            <div class="form-group">
                                {{Form::label('urgente', 'Urgente:',array('class'=>'control-label  col-md-6 '))}}
                                <div class="col-md-6">
                                    <p class="form-control-static">
                                        @if ($model->urgente == 1)
                                        Sim
                                        @else
                                        Não
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('solicitado_em','Solicitado em:',array('class'=>'control-label  col-md-6 '))}}
                                <div class="col-md-6">
                                    <p class="form-control-static">
                                        {{$model->solicitado_em}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('entregar_em','Executar em:',array('class'=>'control-label  col-md-6 '))}}
                                <div class="col-md-6">
                                    <p class="form-control-static">
                                        {{$model->entregar_em}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('tipoemprestimo','Tipo de empréstimo:',array('class'=>'control-label  col-md-6 '))}}
                                <div class="col-md-6">
                                    <p class="form-control-static">
                                        @if (intval($model->tipoemprestimo) == 0)
                                        Entrega MEMODOC
                                        @elseif ($model->tipoemprestimo == 1)
                                        Consulta no local
                                        @else
                                        Retirado pelo cliente
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('qtdenovascaixas','Qtde de novas caixas com endereçamento:',array('class'=>'control-label  col-md-6 '))}}
                                <div class="col-md-6">
                                    <p class="form-control-static">
                                        {{number_format($model->qtdenovascaixas, 0, ",", ".")}}
                                    </p>
                                </div>
                            </div>

                            <div class="form-group">
                                {{Form::label('qtd_cartonagens_cliente)','Qtde de novas caixas sem endereçamento:',array('class'=>'control-label  col-md-6 '))}}
                                <div class="col-md-6">
                                    <p class="form-control-static">
                                        {{number_format($model->qtd_cartonagens_cliente, 0, ",", ".")}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('qtdenovasetiquetas)','Qtde de novos endereços sem cartonagem:',array('class'=>'control-label  col-md-6 '))}}
                                <div class="col-md-6">
                                    <p class="form-control-static">
                                        {{number_format($model->qtdenovasetiquetas, 0, ",", ".")}}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('qtd_caixas_retiradas_cliente','Qtde caixas a retirar:',array('class'=>'control-label  col-md-6 '))}}
                                <div class="col-md-6">
                                    <p class="form-control-static">
                                        {{number_format($model->qtd_caixas_retiradas_cliente, 0, ",", ".")}}
                                    </p>
                                </div>
                            </div>


                        </div>
                        <!-- Fim segunda coluna-->
                    </div>
                </div>
                <!-- Fim form-body -->
                {{ Form::close() }}
            </div>
            <!-- Fim portlet-body -->
        </div>
        <!-- Fim portlet -->
    </div>
    <!-- fim  da coluna unica -->    
</div>
<!-- fim linha do painel de busca -->

<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-archive"></i>Caixas solicitadas</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body">
                @foreach($documentos as $doc)
                <a href="{{URL::to('publico/documento/cadastrar/'.$doc->id_caixapadrao)}}" class="btn btn-primary" title="Ver caixa" style="margin-bottom:5px;" target="_blank">{{substr('000000'.$doc->id_caixapadrao, -6)}}</a>
                @endforeach                
            </div>
        </div>
    </div>
    <!-- fim  da coluna unica -->    
</div>

@if (count($checkins) > 0)
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-archive"></i>Caixas enviadas</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body">
                @foreach($checkins as $checkin)
                    @if(count($checkin->getDocumento()) > 0)
                        <a href="{{URL::to('publico/documento/cadastrar/'.$checkin->getDocumento()->id_caixapadrao)}}" class="btn btn-primary" title="Ver caixa" style="margin-bottom:5px;" target="_blank">{{substr('000000'.$checkin->getDocumento()->id_caixapadrao, -6)}}</a>
                    @endif
                @endforeach                
            </div>
        </div>
    </div>
    <!-- fim  da coluna unica -->    
</div>
@endif

{{Form::close()}}

@stop

@section('scripts')
@include('_layout.scripts-forms')
@stop
