@extends('stilearn-metro')

@section('conteudo')

<header class="content-header">

    <!-- content title-->
    <div class="page-header"><h1>Galpões</h1></div>
    <ul class="content-action pull-right">
        <li><a class="btn btn-primary" href="/galpao/indisponibilizartodos" title="Tornar todos os galpões indisponíveis">Indisponibilizar todos</a></li>
        <li><a class="btn bg-orange" href="/galpao/disponibilizartodos" title="Tornar todos os galpões disponíveis">Disponibilizar todos</a></li>
    </ul>

</header> <!--/ content header -->


<!-- content page -->
<article class="content-page">
    <!-- main page, you're application here -->
    <div class="main-page">
        <div class="content-inner">
            <!-- row #1 -->

            @include('padrao.mensagens')
            <div class="row-fluid">
                <!-- span6 -->
                <div class="span12">
                    <!-- widget tdefault -->
                    <div class="widget border-cyan" id="widget-tdefault">
                        <!-- widget header -->
                        <div class="widget-header bg-cyan">
                            <!-- widget icon -->
                            <div class="widget-icon"><i class="aweso-table"></i></div>
                            <!-- widget title -->
                            <h4 class="widget-title">Situação</h4>
                            <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        </div><!-- /widget header -->

                        <!-- widget content -->
                        <div class="widget-content">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="text-align:center;">Ação</th>
                                        <th style="text-align:center;">Galpão</th>
                                        <th style="text-align:center;">Endereçamento disponível?</th>
                                        <th style="text-align:right;">Caixas disponíveis</th>
                                        <td style="text-align:right; font-weight: bold;">&nbsp;</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? $total = 0;?>
                                    @foreach ($galpoes as $galpao)
                                    @if ($galpao->fl_disponivel_enderecamento == 1)
                                    <tr>
                                    @else
                                    <tr class="bg-steel">
                                    @endif
                                        <td style="text-align:center;">
                                            @if ($galpao->fl_disponivel_enderecamento == 1)
                                            <a href="/galpao/indisponibilizar/{{$galpao->id_galpao}}" class="btn btn-primary">Indisponibilizar</a>
                                            @else
                                            <a href="/galpao/disponibilizar/{{$galpao->id_galpao}}" class="btn bg-orange">Disponibilizar</a>
                                            @endif
                                        </td>
                                        <td style="text-align:center;">{{ $galpao->id_galpao }}</td>
                                        <td style="text-align:center;">
                                            @if ($galpao->fl_disponivel_enderecamento == 1)
                                            <i class="aweso-check"><i/>
                                            @else
                                            <i class="aweso-check-empty"><i/>
                                            @endif
                                        </td>
                                        <td style="text-align:right;">{{ number_format($galpao->qtd, 0, ',', '.') }}</td>
                                        <? $total += $galpao->qtd; ?>
                                        <td style="text-align:right; font-weight: bold;">&nbsp;</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td style="font-weight: bold;">Total</td>
                                        <td style="text-align:right; font-weight: bold;">{{ number_format($total, 0, ',', '.') }}</td>
                                        <td style="text-align:right; font-weight: bold;">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- /widget content -->
                    </div> <!-- /widget tdefault -->
                </div><!-- span6 -->

                <!-- /out of widget demo -->

            </div><!-- /content-inner-->
        </div><!-- /main-page-->

</article> <!-- /content page -->

@stop
