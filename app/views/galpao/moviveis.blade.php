@extends('stilearn-metro')

@section('conteudo')

<header class="content-header">

    <!-- content title-->
    <div class="page-header"><h1>Buracos aproveitáveis</h1></div>
    Espaços não usados que passaram da franquia. Somente de clientes que tem franquia.
</header> <!--/ content header -->


<!-- content page -->
<article class="content-page">
    <!-- main page, you're application here -->
    <div class="main-page">
        <div class="content-inner">
            <!-- row #1 -->

            @include('padrao.mensagens')
            <div class="row-fluid">
                <!-- span6 -->
                <div class="span12">
                    <!-- widget tdefault -->
                    <div class="widget border-cyan" id="widget-tdefault">
                        <!-- widget header -->
                        <div class="widget-header bg-cyan">
                            <!-- widget icon -->
                            <div class="widget-icon"><i class="aweso-table"></i></div>
                            <!-- widget title -->
                            <h4 class="widget-title">Situação</h4>
                            <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                        </div><!-- /widget header -->

                        <!-- widget content -->
                        <div class="widget-content">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="text-align:center;">Ação</th>
                                        <th style="text-align:left;">Cliente</th>
                                        <th style="text-align:right;">Qtd caixas</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? $total = 0;?>
                                    @foreach ($regs as $reg)
                                    <tr>
                                        <td style="text-align:center;">
                                            <a href="/caixa/moviveiscliente/{{$reg->id_cliente}}" class="btn btn-primary"><i class="aweso-edit"></i></a>
                                        </td>
                                        <td style="text-align:left;">{{ $reg->razaosocial }}<br/><span style="color:#999; font-style:italic;">{{ $reg->nomefantasia }} ({{ $reg->id_cliente }}) </span></td>
                                        <td style="text-align:right;">{{$reg->qtd}}</td>
                                        <?php $total += $reg->qtd ; ?>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td style="text-align:right; font-weight: bold;">&nbsp;</td>
                                        <td style="font-weight: bold;">Total</td>
                                        <td style="text-align:right; font-weight: bold;">{{ number_format($total, 0, ',', '.') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- /widget content -->
                    </div> <!-- /widget tdefault -->
                </div><!-- span6 -->

                <!-- /out of widget demo -->

            </div><!-- /content-inner-->
        </div><!-- /main-page-->

</article> <!-- /content page -->

@stop
