@extends('base')

@section('conteudo')
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Bem-vindo!</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
@include('_layout.msg')
<!-- inicio linha principal-->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- INICIO DIV TILES -->
        <div class="tiles" style="margin-left:50px; margin-right:50px;">
            <!-- INICIO LADRILHO PESQUISA DOCUMENTOS -->
            <div class="tile double-down bg-blue-hoki" onclick="javascript:window.location='/publico/documento/';">
                <div class="tile-body">
                    <i class="fa fa-search"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Pesquisar no acervo
                    </div>
                </div>
            </div>            
            <!-- FIM LADRILHO PESQUISA DOCUMENTOS -->
            <!-- INICIO LADRILHO PESQUISA DOCUMENTOS -->
            @if(Auth::user()->id_nivel <= 2)
            <div class="tile bg-red-sunglo" onclick="javascript:window.location='/publico/documento/cadastrar';">
                <div class="tile-body">
                    <i class="fa fa-archive"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Inventariar
                    </div>
                </div>
            </div>            
            @endif
            <!-- FIM LADRILHO PESQUISA DOCUMENTOS -->
            <!-- INICIO LADRILHO PESQUISA DOCUMENTOS -->
            @if(Auth::user()->id_nivel==1)
            <div class="tile double bg-green-turquoise" onclick="javascript:window.location='/publico/usuario/novo';">
                <div class="tile-body">
                        <i class="fa fa-plus"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Adicionar usuário
                    </div>
                </div>
            </div>            
            @endif
            <!-- FIM LADRILHO PESQUISA DOCUMENTOS -->
            <!-- INICIO LADRILHO PESQUISA DOCUMENTOS -->
            <div class="tile bg-yellow-saffron" onclick="javascript:window.location='/publico/usuario';">
                <div class="tile-body">
                    <i class="fa fa-users"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Listar usuários
                    </div>
                </div>
            </div>            
            <!-- FIM LADRILHO PESQUISA DOCUMENTOS -->
            <!-- INICIO LADRILHO PESQUISA DOCUMENTOS -->
            <div class="tile bg-blue-madison" onclick="javascript:window.location='/publico/usuario/alterarsenha';">
                <div class="tile-body">
                    <i class="fa fa-lock"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Alterar minha senha
                    </div>
                </div>
            </div>            
            <!-- FIM LADRILHO PESQUISA DOCUMENTOS -->
            <!-- INICIO LADRILHO PESQUISA DOCUMENTOS -->
            @if(Auth::user()->id_nivel <= 2)
            <div class="tile double bg-purple-studio" onclick="javascript:window.location='/publico/os/criar';">
                <div class="tile-body">
                    <i class="fa fa-truck"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Criar ordem de serviço
                    </div>
                </div>
            </div>            
            @endif
            <!-- FIM LADRILHO PESQUISA DOCUMENTOS -->
            <!-- INICIO LADRILHO PESQUISA DOCUMENTOS -->
            <div class="tile bg-green-meadow" onclick="javascript:window.location='/publico/os/';">
                <div class="tile-body">
                    <i class="fa fa-history"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Histórico
                    </div>
                </div>
            </div>            
            <!-- FIM LADRILHO PESQUISA DOCUMENTOS -->
            <!-- INICIO LADRILHO PESQUISA DOCUMENTOS -->
            <div class="tile bg-grey-cascade" onclick="javascript:window.location='/auth/logout';">
                <div class="tile-body">
                    <i class="fa fa-power-off"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Sair
                    </div>
                </div>
            </div>            
            <!-- FIM LADRILHO PESQUISA DOCUMENTOS -->
        </div>
        <!-- FIM DIV TILES -->
    </div>
    <!-- fim da coluna unica -->
</div>
<!-- fim linha principal-->
@if (Auth::user()->exibeNotificacao() )
    <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                    <div class="modal-content">
                            <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Atenção</h4>
                            </div>
                            <div class="modal-body" style="color:red; font-weight:bold; font-size: 14px;">
                                     Existem pendências financeiras. Por favor, entre em contato com a Memodoc para regularizar sua situação. <br/>
                                     <div class="center">(21) 2269-8040 ou (21) 9 9883-8493</div>
                            </div>
                            <div class="modal-footer">
                                    <button type="button" class="btn default" data-dismiss="modal">Fechar</button>
                            </div>
                    </div>
                    <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
    </div>

@endif
@stop
@section('scripts')
@if (Auth::user()->exibeNotificacao() )
    <script>
        $(document).ready(function(){
            $('#basic').modal();
        });
    </script>
@endif
@stop
