@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Ordem de serviço {{substr('000000'.$model->id_os, -6)}}</h1>

    </div>


    <!-- content breadcrumb -->
    <ul class="breadcrumb breadcrumb-inline clearfix">
        <li><a href="#">Ordem de serviço</a> <span class="divider"><i class="aweso-angle-right"></i></span></li>
        <li class="active">Criar OS</li>
    </ul>


</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">
            @include('/padrao/mensagens')
            <!-- widget form horizontal -->
            <div class="row-fluid">
                <div class="widget border-cyan span12" id="widget-horizontal">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-edit"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">{{$model->cliente->razaosocial}}</h4>
                        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        {{ Form::model($model, array('class'=>'form-horizontal')) }}
                        {{ Form::hidden('id_cliente_js', $model->id_cliente, array('id' =>'id_cliente_js'))}}
                        {{ Form::hidden('id_os_chave', $model->id_os_chave, array('id' =>'id_os_chave'))}}
                        {{ Form::hidden('id_os', $model->id_os, array('id' =>'id_os'))}}
                        <div class="row-fluid">
                            <div class="span6">
                                <div class='control-group'>
                                    {{Form::label('responsavel','Solicitado por',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('responsavel', null, array('placeholder'=>'nome da pessoa responsável pela OS', 'class'=>'input-block-level'))}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    {{Form::label('solicitado_em_data','Solicitado em',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        <div class="input-append input-append-inline ">
                                            {{Form::text('solicitado_em_data', null ,array('data-fx' =>"datepicker", "class"=>"input-small"))}}
                                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                                        </div>
                                        <div class="input-append bootstrap-timepicker">
                                            {{Form::text('solicitado_em_hora', null ,array("data-fx"=>"timepicker", "data-template"=>"false", "data-show-inputs"=>"false", "data-show-seconds"=>"true", "data-show-meridian"=>"false",  "class"=>"input-small"))}}
                                        </div>
                                    </div>
                                </div>

                                <div class="control-group">
                                    {{Form::label('entregar_em','Executar em',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        <div class="input-append input-append-inline ">
                                            {{Form::text('entregar_em', null ,array('data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker", "class"=>"input-small"))}}
                                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                                        </div>
                                        <div class="input-append">
                                            {{Form::select('entrega_pela', array("0"=>"Manhã", "1"=>"Tarde"), null ,array("class"=>"input-small"))}}
                                        </div>
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('id_departamento','Departamento',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('id_departamento', array('' => '(não especificado)') + DepCliente::where('id_cliente', '=', $model->id_cliente)->orderBy('nome')->lists('nome', 'id_departamento'), null, array('id'=>'departamento','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('tipodeemprestimo','Tipo de empréstimo',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('tipodeemprestimo', array('0' => 'Entrega MEMODOC', '1' => 'Consulta no local', '2' => 'Retirado pelo cliente') , null, array('id'=>'tipodeemprestimo','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('observacao', 'Observações',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::textarea('observacao', null, array('style'=>'height:100px','class'=>'input-block-level'))}}

                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class='control-group'>
                                    {{Form::label('qtdenovascaixas','Qtde de novas caixas',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('qtdenovascaixas', null ,array('placeholder'=>"reserva"))}}
                                    </div>
                                </div>

                                <div class='control-group'>
                                    {{Form::label('apanhanovascaixas','Qtde caixas cheias novas',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('apanhanovascaixas', null ,array('placeholder'=>"caixas vindo pela primeira vez"))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('apanhacaixasemprestadas','Qtde caixas de devolução',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('apanhacaixasemprestadas',null ,array('placeholder'=>"caixas emprestadas devolvidas"))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('id_endeentrega','Local entrega/retirada',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('id_endeentrega', $endeentrega, null, array( 'id'=>'endereco','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('urgente', 'Urgente',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::checkbox('urgente','1',null)}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('cobrafrete', 'Cobrar frete?',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::checkbox('cobrafrete','1',null)}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('naoecartonagemcortesia', 'Cobrar cartonagem?',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::checkbox('naoecartonagemcortesia','1',null)}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <!-- widget Caixas emprestimo -->
                                <div class="widget border-cyan" id="widget-chat">
                                    <!-- widget header -->
                                    <div class="widget-header bg-cyan">
                                        <!-- widget icon -->
                                        <div class="widget-icon"><i class="aweso-archive"></i></div>
                                        <!-- widget title -->
                                        <h4 class="widget-title">Caixas para o empréstimo</h4>
                                    </div><!-- /widget header -->

                                    <!-- widget content -->
                                    <div class="widget-content bg-silver">
                                        <div class="_add-to-chat bg-silver">
                                            <div class='input-append'>
                                                <input type='text' id='id_caixapadrao' placeholder='Caixa padrão' style='width:140px' />
                                                <a href='#' class="add-caixa btn bg-cyan">Adicionar</a>
                                            </div>
                                        </div>
                                        <div id="caixasenviadas">
                                            @include('ordemservico_caixasenviadas', array('osdesmembradas' => OsDesmembrada::daOs($model)->get()))
                                        </div>
                                    </div><!-- /widget content -->
                                </div><!-- /widget Caixas emprestimo -->
                            </div><!-- /Caixas emprestimo -->
                        </div>
                        <div class="form-actions bg-silver">
                            <input type="submit" name="Salvar" value="Salvar"   class="btn btn-primary" />
                            <input type="submit" name="Concluir" value="Concluir" class="btn btn-warning"/>
                            <input type="submit" name="Cancelar" value="Cancelar" class="btn" title="Cancela essa Ordem de serviço"/>
                        </div>

                        {{Form::close()}}
                    </div>



                </div>


            </div>
            </article> <!-- /content page -->

            @stop

            @section('scripts')
            <script>


                $(function() {

                    $('.add-caixa').click(function() {

                        dados = {
                            id_caixapadrao: $('#id_caixapadrao').val(),
                            id_os_chave: $('#id_os_chave').val()
                        };
                        $(this).html('...');
                        $('#caixasenviadas').html('...');

                        $.ajax({
                            type: "POST",
                            url: '{{\Memodoc\Servicos\Rotas::ordemservico()}}/addcaixa',
                            data: dados,
                            dataType: 'html',
                            success: function(data) {
                                $('.add-caixa').html('Adicionar');
                                $('#caixasenviadas').html(data);
                                $('#id_caixapadrao').focus();
                            }
                        });
                    });

                });


            </script>
            @stop