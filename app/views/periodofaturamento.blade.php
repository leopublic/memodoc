@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Faturamentos</h1></div>
    <div class='content-action pull-right'>
        <a href='/periodofat/novo' class='btn btn-cyan'>Novo</a>
    </div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao.mensagens')
            <div class="row-fluid">
            @if(isset($instancias))
                <table class='table table-striped table-hover table-bordered table-condensed'>
                    <thead>
                        <tr>
                            <th style="width:40px; text-align: center;">ID</th>
                            <th style="width:220px; text-align: center;">Ação</th>
                            <th style="width:100px; text-align: center;">Status</th>
                            <th style="width:auto;  text-align: center;">Ano/Mês</th>
                            <th style="width:auto;  text-align: center;">Início</th>
                            <th style="width:auto;  text-align: center;">Fim</th>
                            <th style="width:auto;  text-align: center;">Carga de preços</th>
                            <th style="width:auto;  text-align: center;">Último cálculo</th>
                            <th style="width:auto;  text-align: center;">Criado por</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($instancias as $obj)
                        <tr>
                            <td style="text-align: center;">{{ $obj->sequencial }}</td>
                            <td style="text-align: center;">
                                <a href="/periodofat/detalhe/{{ $obj->id_periodo_faturamento}}" title="Listar faturamentos por cliente" class="btn btn-sm btn-primary"><i class="aweso-gear"></i></a>
                                <a href="/periodofat/edit/{{ $obj->id_periodo_faturamento}}" title="Editar faturamento" class="btn btn-sm btn-primary"><i class="aweso-edit"></i></a>
                                @if ($obj->status_conclusao == 1)
                                    <a href="/periodofat/fechar/{{ $obj->id_periodo_faturamento}}" title="Fechar o faturamento" class="btn btn-sm btn-primary "><i class="aweso-lock"></i></a>
                                @endif
                                @if ($obj->status_conclusao == 2)
                                    <a href="/periodofat/abrir/{{ $obj->id_periodo_faturamento}}" title="Abrir o faturamento" class="btn btn-sm btn-primary bg-amber"><i class="aweso-unlock"></i></a>
                                @endif
                                <a class="btn btn-sm bg-lime " href="{{URL::to('/faturamento/conferencia/'.$obj->id_periodo_faturamento)}}" target="_blank"><i class="aweso aweso-check"></i></a>
                                @include('padrao.link_exclusao', array('title'=> 'Clique para excluir esse faturamento', 'url' => '/periodofat/excluir/'.$obj->id_periodo_faturamento, 'msg_confirmacao'=> 'Clique para confirmar a exclusão desse faturamento'))
                            </td>
                            <td style="text-align: center;" class="@if($obj->status_conclusao==1)bg-amber@endif">{{ $obj->status_texto }}</td>
                            <td style="text-align: center;">{{ $obj->ano."/".$obj->mes }}</td>
                            <td style="text-align: center;">{{ $obj->dt_inicio_fmt }}</td>
                            <td style="text-align: center;">{{ $obj->dt_fim_fmt }}</td>
                            <td style="text-align: center;">
                                @if (is_object($obj->precificadopor))
                                {{ $obj->precificadopor->nomeusuario }}
                                @else
                                --
                                @endif
                                <br>{{ $obj->formatDate('dt_carga_precos', 'd/m/Y H:i:s') }}</td>
                            <td style="text-align: center;">
                                @if (is_object($obj->calculadopor))
                                {{ $obj->calculadopor->nomeusuario }}
                                @else
                                --
                                @endif
                                <br>{{ $obj->formatDate('dt_recalculo', 'd/m/Y H:i:s') }}
                                @if ($obj->temRecalculo())
                                <br><span style="color:red;">(existem clientes recalculados posteriormente)</span>
                                @endif
                            </td>
                            <td style="text-align: center;">
                                @if (is_object($obj->cadastradopor))
                                {{ $obj->cadastradopor->nomeusuario }}
                                @else
                                --
                                @endif
                                <br>{{ $obj->formatDate('created_at', 'd/m/Y H:i:s') }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif

            </div>
        </div>
    </div>
</article> <!-- /content page -->
@stop

@section('scripts')
<script>
    $(function() {
        $departamento = $('#id_departamento');
        $setor = $('#id_setor');
        $centrocusto = $('#id_centro');
        $cliente.change(function() {

            $('.load').show();
            $('#titulo').focus();
            $('.documentos').hide();

            // limpando combos
            $departamento.html('');
            $setor.html('');
            $centrocusto.html('');

            // ajax departamento
            $.get('publico/documento/ajaxdepartamento/' + $(this).val(), function(combo) {
                $departamento.html(combo);
            });
            // ajax centro custo
            $.get('publico/documento/ajaxcentrocusto/' + $(this).val(), function(combo) {
                $centrocusto.html(combo);
            });
            // ajax setor
            $.get('publico/documento/ajaxsetor/' + $(this).val(), function(combo) {
                $setor.html(combo);
            });

            // Remove o load após 2.5 segundos
            setInterval(function() {
                $('.load').hide();
            }, 2500);

        }).focus();
    });
</script>
@stop
