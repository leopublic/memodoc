@extends('stilearn-metro')

@section('estilos')
<style>
tr.in{
    background-color: #fcd1d1 !important;
}
tr.out{
    background-color: #d8ffdf !;
}
    
</style>
@stop


@section('conteudo')

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Histórico de movimentação da referência {{$id_caixapadrao}}</h1></div>

    <!-- content breadcrumb -->

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page">
        <div class="content-inner">
            @include('padrao.mensagens')
            <div class="widget border-cyan" id="widget-horizontal">
                @include('padrao/widget_header', array('titulo' => 'Informe o ID', 'icone' => '<i class="aweso-archive"></i>'))
                <!-- widget content -->
                <div class="widget-content">
                {{ Form::open(array('class'=>'form-horizontal')) }}

                     <div class='control-group'>
                         {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                         <div class="controls">
                             {{Form::select('id_cliente', $clientes, $id_cliente , array('id'=>'id_cliente','data-fx'=>'select2', 'required', 'class' => 'input-block-level'))}}
                         </div>
                     </div>

                    <div class="control-group">
                         {{Form::label('id_caixapadrao','Referência',array('class'=>'control-label'))}}
                        <div class='controls'>
                             {{Form::text('id_caixapadrao', $id_caixapadrao, array('placeholder'=>'referência', 'class'=>'input-small', 'id' => 'id_caixapadrao'))}}
                             @if ($caixa->id_documento > 0)
                                 &nbsp;{{$caixa->endereco_completo()}}
                             @endif
                         </div>
                    </div>
                    @if (is_object($caixa))
                    <div class="control-group">
                         {{Form::label('situacao', 'Situação', array('class'=>'control-label'))}}
                        <div class='controls'>
                            <span class="input-block-level uneditable-input">{{ $situacao }}</span>
                         </div>
                    </div>
                    @endif
                  <div class="form-actions bg-silver">
                        <button type="submit" class="btn btn-primary">Recuperar histórico</button>
                   </div>
                {{Form::close()}}
                </div>
            </div>

            <div class="widget border-cyan" id="widget-horizontal">
                @include('padrao/widget_header', array('titulo' => 'Movimentação', 'icone' => '<i class="aweso-time"></i>'))
                <!-- widget content -->
                <div class="widget-content">
                    @if(is_array($instancias))
                    <table class="listagem table table-hover table-condensed" data-sorter="true" style="width:100%">
                        <thead>
                            <tr>
                                <th style="text-align: center; width:auto">Direção</th>
                                <th style="text-align: center; width:auto">OS</th>
                                <th style="text-align: center; width:auto">Expurgo</th>
                                <th style="text-align: center; width:auto;">Solicitante</th>
                                <th style="text-align: center; width:auto;">Solicitada em</th>
                                <th style="text-align: center; width:auto;">Entregue em</th>
                                <th style="text-align: center; width:auto;">Tipo da solicitação</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($instancias as $r)
                                    @if ($r->tipo == 'in')
                            <tr style="background-color: #d8ffdf ;">
                                <td style="text-align: left;">
                                    <i class="aweso-arrow-right"></i><i class="aweso-building"></i> Entrada
                                    @elseif($r->tipo == 'out')
                            <tr style="background-color: #fcd1d1;">
                                <td style="text-align: left;">
                                    <i class="aweso-arrow-left"></i><i class="aweso-building"></i> Saída
                                    @elseif($r->tipo == 'exp')
                            <tr style="background-color: #E3C800;">
                                <td style="text-align: left;">
                                    <i class="aweso-trash"></i> Expurgo
                                    @endif
                                </td>
                                <td style="text-align: center;">
                            @if($r->id_os_chave > 0)
                                    <a href="/ordemservico/visualizar/{{$r->id_os_chave}}" class="btn btn-small" target="_blank">{{substr('000000'.$r->id_os, -6)}}</a>
                            @else 
                                (--)
                            @endif                                
                                </td>
                                <td style="text-align: center;">
                            @if($r->id_expurgo_pai > 0)                                    
                                    <a href="/expurgo/caixasgravadas/{{$r->id_expurgo_pai}}" class="btn btn-small" target="_blank">{{substr('000000'.$r->id_expurgo_pai, -6)}}</a>
                            @else 
                                (--)
                            @endif                                
                                </td>

                                <td style="text-align: center;">{{$r->responsavel}}</td>
                                <td style="text-align: center;">{{$r->solicitado_em_fmt}}</td>
                                <td style="text-align: center;">{{$r->entregar_em_fmt}}</td>
                                <td style="text-align: center;">
                                      @if($r->tipodeemprestimo == 0)
                                      Entregue pela Memodoc
                                      @elseif($r->tipodeemprestimo == 1)
                                      Consulta na Memodoc
                                      @else
                                      Retirada sem frete (portador próprio)
                                      @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    (informe o cliente e o número da referência Memodoc)
                    @endif
                </div>
            </div>

            <!-- List -->
        </div>
    </div>
</article> <!-- /content page -->
<style>
    .c{
        width:200px;
        text-align:center;
    }
</style>
@stop