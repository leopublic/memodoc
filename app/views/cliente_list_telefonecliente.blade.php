@extends('stilearn-metro')


@section('conteudo') 
 
         @include('cliente_tab_header')
         
        <!-- #tab_telefones - Begin Content  -->
         <div class="tab-pane" id="tab_TelefoneCliente">
            <?php
            
               $hasmany = array('title' => 'Telefones',
                                'fields' => array('nome' => 'Nome',
                                                  'descricaotipo'=>'Tipo',
                                                  'ddd' => 'DDD',
                                                  'numero'=>'Numero',
                                                  'ramal'=>'Ramal'
                                                 )          
                                ); 
            ?>
            @include('padrao/hasmany_list', $hasmany)
         </div>
        <!-- #tab_telefones - End Content  -->  
        
         @include('cliente_tab_footer')
@stop

