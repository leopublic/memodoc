@extends('stilearn-metro')

@section('conteudo')
    <!-- content header -->
    <header class="content-header">
        <!-- content title-->
        <div class="page-header"><h1>Cadastrar documentos</h1></div>
    </header> <!--/ content header -->
    
    <!-- content page -->
    <article class="content-page clearfix">        
        <!-- main page -->
        <div class="main-page" id="">
            <div class="content-inner">
                <!-- search box -->
                {{Form::open(array('url' => '/publico/caixa/selecionar', 'class' => 'form-horizontal'))}}
                <div class="widget border-cyan">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-archive"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Selecione a referência</h4>
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content bg-silver">
                        <div class="control-group">
                            <div class='input-append'>
                                {{Form::text('id_caixapadrao', Input::old('id_caixapadrao'))}}
                               <input type="submit" class="btn bg-cyan" class="button" id="adicionaCaixa" value="Localizar"/>
                            </div>
                        </div>
                        @include('padrao.mensagens')
                    </div>
                </div>
                {{ Form::close() }}


            </div>
        </div>
    </article> <!-- /content page -->           
@stop

@section('scripts')
<script>
  $(function(){ 
    $departamento = $('#id_departamento');  
    $setor = $('#id_setor');  
    $centrocusto = $('#id_centro'); 
    $cliente.change(function(){
        
       $('.load').show(); 
       $('#titulo').focus();
       $('.documentos').hide();
       
       // limpando combos
       $departamento.html('');
       $setor.html('');
       $centrocusto.html('');
       
       // ajax departamento
       $.get('publico/documento/ajaxdepartamento/'+$(this).val(),function(combo){
           $departamento.html(combo);
       });
       // ajax centro custo
       $.get('publico/documento/ajaxcentrocusto/'+$(this).val(),function(combo){
           $centrocusto.html(combo);
       });
       // ajax setor
       $.get('publico/documento/ajaxsetor/'+$(this).val(),function(combo){
           $setor.html(combo);
       });
       
       // Remove o load após 2.5 segundos
       setInterval(function(){
           $('.load').hide(); 
       },2500);
       
    }).focus();
  });
</script>
@stop