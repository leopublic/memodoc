<!DOCTYPE html>
<html lang="en">
    <head>
        <title>MEMODOC</title>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Sistema de controle de documentos - MEMODOC" />
        <meta name="author" content="M2 Software" />

        <!-- styles -->
        <link href="/stilearn-metro/css/bootstrap.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/bootstrap-responsive.css" rel="stylesheet" />
        <!-- default theme -->
        <link href="/stilearn-metro/css/metro-bootstrap.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/metro.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/metro-responsive.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/metro-helper.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/font-awesome.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/bootstrap-rowlink.min.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/morrisjs/morris.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/m-scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" />

        <!-- other -->
        <link href="/stilearn-metro/css/select2/select2-metro.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/datepicker/datepicker.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/timepicker/bootstrap-timepicker.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/datetimepicker/datetimepicker.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/daterangepicker/daterangepicker.css" rel="stylesheet" />
        <link href="/stilearn-metro/css/icomoon.css" rel="stylesheet" />

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="/stilearn-metro/js/html5shiv.js"></script>
          <script src="/stilearn-metro/js/lte-ie7.js"></script>
        <!--[endif]-->

        <!-- fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/stilearn-metro/ico/apple-touch-icon-144-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/stilearn-metro/ico/apple-touch-icon-114-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/stilearn-metro/ico/apple-touch-icon-72-precomposed.png" />
        <link rel="apple-touch-icon-precomposed" href="/stilearn-metro/ico/apple-touch-icon-57-precomposed.png" />
        <link rel="shortcut icon" href="/stilearn-metro/ico/favicon.png" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>
        <!-- start header-->
        <header class="header">
            <!-- start navbar, this navbar on top -->
            <div id="navbar-top" class="navbar navbar-cyan "> <!-- fixed: navbar-fixed-top -->
                <!-- navbar inner-->
                <div class="navbar-inner">
                    <!-- container-->
                    <div class="container">
                        <!-- Your brand here, images or text -->
                        <a class="brand" href="/">
                            <img src='/img/logo.png' style='width:150px' />
                            <!-- just a sample brand, replace with your own -->
                            <!--<i class="aweso-th-large"></i> Memodoc -->
                        </a>

                        @if (Auth::check())
                            @if (Auth::user()->id_nivel >= 10)
                                @include('padrao/menu')
                            @else
                                @include('padrao/menu_cliente')
                            @endif
                        @endif
                    </div><!--/container-->
                </div><!--/navbar-inner-->

            </div> <!--/ navbar-->
        @if ((Auth::user()->adm == 1 || Auth::user()->qtdClientes() > 0) && Auth::user()->id_cliente > 0)
            <div id="navbar-top" class="navbar navbar-cobalt"> <!-- fixed: navbar-fixed-top -->
                <div class="navbar-inner bg-steel color-silver" style="height: auto !important; min-height: 0px;">
                    <div class="container">
                        Você está na empresa: "{{Auth::user()->cliente->razaosocial}}"
                    </div><!--/container-->
                </div><!--/navbar-inner-->
            </div> <!--/ navbar-->
        @endif
        </header> <!--/ end header-->

        <!-- start section content-->
        <section class="section-content">

            <!-- start content -->
            <div class="content_">


                <!-- write your app here -->
                @yield('conteudo')



            </div><!--/ end content -->
        </section> <!-- /end section content-->


        <!-- footer, I place the footer on here. -->
        <footer class="footer">
            <p>Copyright &copy; Memodoc 2013. Todos os direitos reservados.</p>
        </footer><!--/ footer -->




        <!-- javascript
        ================================================== -->
        <!-- required js -->
        <script type="text/javascript" src="/stilearn-metro/js/jquery.min.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/jquery.ui.touch-punch.min.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/bootstrap.min.js"></script>

        <script type="text/javascript" src="/stilearn-metro/js/holder/holder.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/metro-base.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/bootstrap-rowlink.min.js"></script>
        <script type="text/javascript" src="/js/jquery.tokeninput.js"></script>

        <!-- apps component js, optional -->
        <script type="text/javascript" src="/stilearn-metro/js/select2/select2.min.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/bootstrap-fileupload/bootstrap-fileupload.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/inputmask/jquery.inputmask.bundle.min.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/datepicker/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/timepicker/bootstrap-timepicker.min.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/daterangepicker/moment.min.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/colorpicker/bootstrap-colorpicker.js"></script>


        <script type="text/javascript" src="/stilearn-metro/js/jquery.tablesorter.min.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/m-scrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/morrisjs/raphael-min.js"></script>
        <script type="text/javascript" src="/stilearn-metro/js/morrisjs/morris.min.js"></script>
        <!-- metro js, required! -->
        <script type="text/javascript" src="/stilearn-metro/js/metro-base.js"></script>

        <!-- demo js -->
        <script type="text/javascript" src="/stilearn-metro/js/holder/holder.js"></script>
		<script type="text/javascript" src="/stilearn-metro/js/demo/form-elements.js"></script>

        <script>

            $(document).ready(function(){
                $('table[data-sorter="true"]').tablesorter();

                $('[data-fx="select2"]').each(function(){
                    var e=$(this),t=e.attr("data-min-input")==undefined?0:parseInt(e.attr("data-min-input"));
                    e.select2({minimumInputLength:t})
                }), $('[data-fx="masked"]').inputmask(),
                    $('[data-fx="datepicker"]').datepicker({language:'pt-BR'});

                // ========= Start Globals ============
                //
                //
                    // Habilita a ABA de acordo com a ancora.
                    var url = document.location.toString();
                    if (url.match('#')) {
                        $anchor = '#'+url.split('#')[1];
                        $.each($('.nav-tabs a'), function(i, val) {
                            $item = $('.nav-tabs a:eq('+i+')');
                            $data_anchor = $item.attr('data-anchor');
                            if ($anchor === $data_anchor){
                                $item.tab('show');
                            }
                         });
                    }else{
                        $('.nav-tabs a:first').tab('show');
                    }
                   // ajaxForm: Abre formulário ajax dentro do .box
                    $('.ajaxForm').on('click',function(){
                        $content = $(this).closest('.box');
                        $.get($(this).attr('href'),function(data){
                            $content.html(data);
                        })
                        return false;
                    });
                //
                // =========== End Globals =========



            $('#pdf').click(function(){

                $form = $('#frmdocumento').clone().attr('id','newid').appendTo('#invisivel');
                $form.attr('target','frame');
                $form.attr('method','post');
                $form.attr('action',$(this).attr('href'));
                $form.submit();

                return false;
            });

           });


            $(document).ready(function(){
                $('#submit-pesquisa').click(function(event){
                    event.preventDefault();
                    $(this).html('<i class="aweso-spinner aweso-spin"></i> Aguarde...');
                    $(this).attr('disabled', 'disabled');
                    $(this).parents('form:first').submit();
                });
            });



	</script>

        @section('scripts')

        @show
        <style>
            ul.form{
                  list-style: none outside none;
            }
            ul.form li.subtitle{
                clear:both;
                width:100%;

            }
            ul.form li input[type="text"],ul.form li textarea{
                width:95%;
            }
            ul.form li textarea{
                height:60px;
            }
            ul.form li{
                width: 47%;
                float: left;
               font-size: 1.1em;
               margin-right: 3%;
               padding: 5px 0;
               position: relative;
            }
            .tabs-right > .nav-tabs .active > a, .tabs-right > .nav-tabs .active > a:hover, .tabs-right > .nav-tabs .active > a:focus{
                border-color: #efefef #efefef #efefef transparent;
                border: none;
                border-bottom:1px solid #efefef;
                border-top:1px solid #efefef
            }
            .tabs-right > .nav-tabs {
                border:1px solid #efefef;
                border-top:0px solid #efefef;
            }
            .tabs-right > .nav-tabs > li >a:hover, .tabs-right>.navtabs > li > a:focus{
                border:none;
            }
            .tabs-right > .nav-tabs > li >a:hover, .tabs-right>.navtabs > li > a{
                border:2px solid transparent;
            }
            .listbox{
               margin:20px;
            }
            body{
                background: #F3F3F3;
            }
            .pagination{
                margin: 5px 0px !important;
            }

            .select2-container .select2-choice span {
                font-size: 14px !important
            }
            .select2-container .select2-choice {

            }
            
            div.menu ul li a{
                font-size: 11px !important;
            }
            
            .header > .navbar .brand {
                width: auto;
            }
        </style>
    </body>
</html>
