@extends('stilearn-metro')

@section('conteudo')
<header class="content-header">
    <div class="page-header"><h1>Criar reserva no(s) galpõe(s) {{$galpoes}}</h1></div>
</header>
<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">
            @include('/padrao/mensagens')
            <!-- widget form horizontal -->
            @if ($pode_criar)
            <div class="row-fluid">
                <div class="widget border-cyan span12" id="widget-horizontal">
                    <!-- widget header -->
                    <div class="widget-header bg-cyan">
                        <!-- widget icon -->
                        <div class="widget-icon"><i class="aweso-edit"></i></div>
                        <!-- widget title -->
                        <h4 class="widget-title">Informe os dados da reserva</h4>
                        <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                    </div><!-- /widget header -->

                    <!-- widget content -->
                    <div class="widget-content">
                        {{ Form::model($model, array('class'=>'form-horizontal')) }}
                        {{ Form::hidden('id_os_chave', null) }}
                        <div class="row-fluid">
                            <div class="span6">
                                <div class='control-group'>
                                    {{Form::label('id_cliente','Cliente',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('id_cliente', $clientes, null , array('id'=>'id_cliente','data-fx'=>'select2', 'class' => 'input-block-level'))}}
                                    </div>
                                </div>
                                @if (isset($os) )
                                <div class='control-group'>
                                    {{Form::label('os','Ordem de serviรงo',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('id_os', substr('000000'.$os->id_os, -6), array('disabled' => 'disabled', 'class'=>'input-block-level'))}}
                                    </div>
                                </div>
                                @endif
                                <div class='control-group'>
                                    {{Form::label('caixas','Quantidade',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::text('caixas', null, array('placeholder'=>'quantos endereรงos', 'class'=>'input-block-level'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('id_tipodocumento','Tipo',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::select('id_tipodocumento', TipoDocumento::lists('descricao', 'id_tipodocumento'), null , array('data-fx'=>'select2', 'class' => 'input-block-level'))}}
                                    </div>
                                </div>
                                <div class='control-group'>
                                    {{Form::label('inventario_impresso', 'Gerar planilha?',array('class'=>'control-label'))}}
                                    <div class="controls">
                                        {{Form::checkbox('inventario_impresso','1',null)}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions bg-silver">
                            <input type="submit" name="Salvar" value="Salvar"   class="btn btn-primary" />
                            <input type="submit" name="Cancelar" value="Cancelar" class="btn" title="Cancela essa Ordem de serviรงo"/>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</article> <!-- /content page -->
@stop

@section('scripts')
<script>


</script>
@stop
