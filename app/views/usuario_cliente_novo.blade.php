@extends('base')

@section('estilos')
    @include('_layout.estilos-forms')
@stop

@section('conteudo')
    @include('_layout.page-header', array('title'=>'Cadastrar usuário'))
    @include('_layout.msg')
<!-- inicio conteúdo -->
<!-- inicio linha do painel de busca -->
<div class="row">
    <!-- inicio da coluna unica -->
    <div class="col-md-12">
        <!-- Inicio portlet -->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-user"></i>Informe as características do usuário</div>
                <div class="tools">
                </div>
            </div>
            <!-- INICIO PORTLET-BODY -->
            <div class="portlet-body form">
                {{Form::model($model, array('class' => 'form-horizontal'))}}
                {{Form::hidden('id_usuario', $model->id_usuario)}}
                <div class="form-body">
                    <div class="row">
                        <!-- Inicio primeira coluna-->
                        <div class="col-md-6">
                            <div class='form-group'>
                                {{Form::label('id_cliente','Cliente',array('class'=>'control-label col-md-3'))}}
                                <div class="col-md-9">
                                    {{Form::select('id_cliente', $clientes, Input::old('id_cliente', $id_cliente) , array('id'=>'id_cliente','class' => 'form-control select2me fluid'))}}
                                </div>
                            </div>
                            <div class='form-group'>
                                 {{Form::label('username','E-mail',array('class'=>'control-label col-md-3'))}}
                                 <div class="col-md-9">
                                 {{Form::text('username', null, array('class'=>'form-control fluid'))}}
                                 </div>
                            </div>
                            <div class='form-group'>
                                 {{Form::label('nomeusuario','Nome',array('class'=>'control-label col-md-3'))}} 
                                <div class="col-md-9">
                                 {{Form::text('nomeusuario', null, array('class'=>'form-control','required'))}} 
                                 </div>
                            </div>
                            <div class='form-group'>
                                 {{Form::label('validadelogin','Validade do login',array('class'=>'control-label col-md-3'))}} 
                                <div class="col-md-9">
                                 {{Form::text('validadelogin', null, array('class'=>'form-control'))}}
                                 <span class="help-block">Informe a quantidade de dias que o login permanecerá válido, ou deixe em branco para validade indeterminada</span>
                                 </div>
                            </div>
                            <div class="form-group">
                                {{Form::label('nivel','Tipo de usuário',array('class'=>'control-label col-md-3'))}}
                                <div class="col-md-9">
                                    <div class="radio-list">
                                        <label>
                                            {{ Form::radio('id_nivel', '1', (Input::old('id_nivel') == '1' ) ? true : false, array('id'=>'1')) }}
                                            Usuário pode cadastrar novos usuários e utilizar todas as opções do sistema
                                        </label>
                                        <label>
                                            {{ Form::radio('id_nivel', '2', (Input::old('id_nivel') == '2' ) ? true : false, array('id'=>'2')) }}
                                            Usuário sem permissão para criar novas senhas. Entretanto pode utilizar toda a funcionalidade do sistema
                                        </label>
                                        <label>
                                            {{ Form::radio('id_nivel', '3', (Input::old('id_nivel') == '3' ) ? true : false, array('id'=>'3')) }}
                                            Usuário somente pode efetuar consultas
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIM PRIMEIRA coluna-->
                        <!-- Inicio segunda coluna-->
                        <div class="col-md-6">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption"><i class="fa fa-lock"></i>Áreas disponíveis para esse usuário:</div>
                                    <div class="tools">
                                    </div>
                                </div>
                                <!-- INICIO PORTLET-BODY -->
                                <div class="portlet-body form">
                                    {{Form::model($model, array('class' => 'form-horizontal'))}}
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class='control-group'>
                                                    {{Form::label('id_departamento[]','Departamentos',array('class'=>'control-label'))}} 
                                                    <div class="controls">
                                                    {{Form::select('id_departamento[]', Auth::user()->cliente->departamento->lists('nome', 'id_departamento'), $model->departamentos->lists('id_departamento'), array('class' => 'form-control select2me fluid', 'multiple'=>'multiple'))}} 
                                                    </div>
                                                </div>

                                                <div class='control-group'>
                                                    {{Form::label('id_setor[]','Setores',array('class'=>'control-label'))}} 
                                                    <div class="controls">
                                                    {{Form::select('id_setor[]', Auth::user()->cliente->setor->lists('nome','id_setor'), $model->setores->lists('id_setor'), array('class' => 'form-control select2me fluid', 'multiple'=>'multiple'))}}
                                                    </div>
                                                </div> 

                                                <div class='control-group'>
                                                    {{Form::label('id_centrodecusto[]','Centros de custos',array('class'=>'control-label'))}} 
                                                    <div class="controls">
                                                    {{Form::select('id_centrodecusto[]', Auth::user()->cliente->centrocusto->lists('nome', 'id_centrodecusto'), $model->centrocustos->lists('id_centrodecusto'), array('class' => 'form-control select2me fluid', 'multiple'=>'multiple'))}}
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                            
                        </div>
                        <!-- FIM segunda coluna-->
                    </div>
                </div>
                <!-- Fim form-body -->
                <!-- INICIO FORM-ACTIONS -->
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-offset-3 col-md-9">
                                <input type="submit" class="btn btn-primary" id="submit-pesquisa" value="Salvar"></>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- FIM FORM-ACTIONS -->
                {{ Form::close() }}
            </div>
            <!-- Fim portlet-body -->
        </div>
        <!-- Fim portlet -->
    </div>
    <!-- fim  da coluna unica -->    
</div>
                        
@stop

@section('scripts')
    @include('_layout.scripts-forms')
@stop