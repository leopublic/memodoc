@extends('stilearn-metro')

@section('conteudo')
<style>
    .controls{ margin-right: 20px;}
    .date,.number,.alfa{width:90px}
    .table{font-size:11px}
    #invisivel{display:none}
    .select2-container .select2-open{ottomarray('url'=>'/ordemservico/listar', 'method' => 'get', 'class' => 'form-horizontal'))}}
</style>

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Pesquisar lançamentos</h1></div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">

            @include('padrao.mensagens')
            <!-- search box -->
            <div class="widget" id="default-widget">
                <!-- widget header -->
                <div class="widget-header">
                    <!-- widget title -->
                    <h4 class="widget-title"><i class="aweso-filter"></i> Filtrar</h4>
                    <!-- widget action, you can also use btn, btn-group, nav-tabs or nav-pills (also support dropdown). enjoy! -->
                </div><!--'mask': '[999999]9,99' -->

                <div class="widget-content" style="display: block;">
                    {{Form::open(array("id" => "form_pesquisa", 'class' => 'form-horizontal'))}}
                    {{Form::hidden('ordenacao', "", array("id"=>"ordenacao"))}}
                    <div class="row-fluid">
                        <div class="span6">
                            <div class='control-group'>
                                {{Form::label('id_cliente_filtro','Cliente',array('class'=>'control-label'))}}
                                <div class="controls">
                                {{Form::select('id_cliente_filtro', $clientes, Input::old('id_cliente_filtro') , array('id'=>'id_cliente_filtro','data-fx'=>'select2','class'=>'input-block-level'))}}
                                </div>
                            </div>
                            <div class="control-group">
                                {{Form::label('numero','Número documento', array('class'=>'control-label'))}}
                                <div class="controls">
                                   {{Form::text('numero', Input::old('numero') ,array('placeholder'=>"Número do documento",'class'=>'input-block-level','style'=>''))}}
                                </div>
                            </div>
                            <div class="control-group">
                                {{Form::label('nosso_numero','Nosso número', array('class'=>'control-label'))}}
                                <div class="controls">
                                   {{Form::text('nosso_numero', Input::old('nosso_numero') ,array('placeholder'=>"Nosso número",'class'=>'input-block-level','style'=>''))}}
                                </div>
                            </div>
                            <div class="control-group">
                                {{Form::label('valor','Valor ', array('class'=>'control-label'))}}
                                <div class="controls">
                                   {{Form::text('valor', Input::old('valor')  ,array('class'=>'input-block-level',"data-fx"=>"masked","data-inputmask"=>"'mask': '[999999]9,99'"))}}
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class='control-group'>
                                <label class="control-label">Vencimento</label>
                                <div class="controls">
                                    <div class="input-append input-append-inline ">
                                        {{Form::text('data_vencimento_ini', Input::old('data_vencimento_ini')  ,array('placeholder'=>"desde", 'class'=>'input-small', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                        <span class="add-on"><i class="icomo-calendar"></i> </span>
                                    </div>
                                    <div class="input-append input-append-inline ">
                                        {{Form::text('data_vencimento_fim', Input::old('data_vencimento_fim') ,array('placeholder'=>"até", 'class'=>'input-small', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                        <span class="add-on"><i class="icomo-calendar"></i> </span>
                                    </div>
                                </div>
                            </div>
                            <div class='control-group'>
                                <label class="control-label">Emissão</label>
                                <div class="controls">
                                    <div class="input-append input-append-inline ">
                                        {{Form::text('data_emissao_ini', Input::old('data_emissao_ini')  ,array('placeholder'=>"desde", 'class'=>'input-small', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                        <span class="add-on"><i class="icomo-calendar"></i> </span>
                                    </div>
                                    <div class="input-append input-append-inline ">
                                        {{Form::text('data_emissao_fim', Input::old('data_emissao_fim')  ,array('placeholder'=>"até", 'class'=>'input-small', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                        <span class="add-on"><i class="icomo-calendar"></i> </span>
                                    </div>
                                </div>
                            </div>
                            <div class='control-group'>
                                <label class="control-label">Pagamento</label>
                                <div class="controls">
                                    <div class="input-append input-append-inline ">
                                        {{Form::text('data_pagamento_ini', Input::old('data_pagamento_ini') ,array('placeholder'=>"desde", 'class'=>'input-small', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                        <span class="add-on"><i class="icomo-calendar"></i> </span>
                                    </div>
                                    <div class="input-append input-append-inline ">
                                        {{Form::text('data_pagamento_fim', Input::old('data_pagamento_fim')  ,array('placeholder'=>"até", 'class'=>'input-small', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                        <span class="add-on"><i class="icomo-calendar"></i> </span>
                                    </div>
                                </div>
                            </div>
                            <div class='control-group'>
                                <label class="control-label">Status pagamento</label>
                                <div class="controls">
                                    {{Form::select('status', array("todos" => "(todos)", "pendentes" => "Somente pendentes", "quitados" => "Somente quitados"), Input::old("status"))}}
                                </div>
                            </div>
                            <div class='control-group'>
                                <label class="control-label">Status processamento</label>
                                <div class="controls">
                                    {{Form::select('status_processamento', array("todos" => "(todos)", "duplicados" => "Somente com duplicidade de CNPJ", "sem_cliente" => "Somente sem cliente"), Input::old("status_processamento"))}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='control-group'>
                        <label class="control-label">&nbsp;</label>
                        <div class="controls">
                            <button class='btn btn-success' id="submit-pesquisa">Buscar</button>
                            <input type="submit" name="xls"  value="XLS" class="btn bg-lime"/>
                        </div>
                    </div>
                    {{Form::close()}}
                </div><!-- /widget content -->
            </div>

            @if (isset($pagamentos))
            <div class="row">
                <div class="span12">
                    <p>{{number_format($qtd, 0, ",", ".")}} registros encontrados</p>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    {{$pagamentos->appends(Input::except('page'))->links()}}
                </div>
            </div>
            <div class="row-fluid">
                <table class="table table-hover table-striped table-condensed "  style='width:100%'>
                    <thead>
                        <tr>
                            <th style='width:50px;text-align:center;'>Ações</th>
                            <th style='width:10px;text-align: center;'>ID{{$ordenacoes['id_pagamento']}}</th>
                            <th style='width:150px;text-align: center;'>Número{{$ordenacoes['numero']}}</th>
                            <th style='width:150px;text-align: center;'>Nosso número{{$ordenacoes['nosso_numero']}}</th>
                            <th style='width:150px;text-align: center;'>CNPJ{{$ordenacoes['cnpj']}}</th>
                            <th style='width:auto;text-align: left;'>Cliente associado</th>
                            <th style='width:120px;text-align: right;'>Valor cobrado{{$ordenacoes['valor']}}</th>
                            <th style='width:120px;text-align: right;'>Valor pago{{$ordenacoes['valor_pago']}}</th>
                            <th style='width:120px;text-align: center;'>Emitido em{{$ordenacoes['data_emissao']}}</th>
                            <th style='width:120px;text-align: center;'>Vencimento{{$ordenacoes['data_vencimento']}}</th>
                            <th style='width:120px;text-align: center;'>Pagamento{{$ordenacoes['data_pagamento']}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pagamentos as $pag)
                        <tr>
                            <td style="text-align:center;">
                                <a href="/retorno/revisarpagamento/{{$pag->id_pagamento}}" class="btn btn-primary"><i class="aweso-edit"></i></a>
                            </td>
                            <td style="text-align: center;">{{substr('000000'.$pag->id_pagamento, -6)}}</td>
                            <td style="text-align: center;">{{$pag->numero}}</td>
                            <td style="text-align: center;">{{$pag->nosso_numero}}</td>
                            <td style="text-align: center;">{{$pag->cnpj_formatado}}</td>
                            <td>
                            @if($pag->fl_duplicidade_cnpj)
                                <a id="btn_associar_cliente_{{$pag->id_pagamento}}" href="javascript:associar_cliente('{{$pag->id_pagamento}}');" class="btn btn-warning"><i class="aweso-question"></i></a>
                                <a class="btn btn-warning" href="#" id="btn_associar_cliente_spin_{{$pag->id_pagamento}}" style="display:none;"><i class="aweso-spinner aweso-spin"></i></a>
                            @else
                                @if($pag->id_cliente == '')

                                @else
                                    {{$pag->nome_cliente_associado}}
                                @endif
                            @endif
                            </td>
                            <td style="text-align: right;">{{number_format($pag->valor, 2, ",", ".")}}</td>
                            <td style="text-align: right;">{{number_format($pag->valor_pago, 2, ",", ".")}}</td>
                            <td style="text-align: center;">{{$pag->formatDate('data_emissao')}}</td>
                            <td style="text-align: center;">{{$pag->formatDate('data_vencimento')}}</td>
                            <td style="text-align: center;">{{$pag->formatDate('data_pagamento')}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$pagamentos->appends(Input::except('page'))->links()}}
            </div>
            @endif
        </div>
    </div>
</article> <!-- /content page -->
<div class="modal fade modal-lg" role="dialog" id="escolhe_cliente" style="width:700px; margin-left:-320px;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Defina o cliente correto</h4>
      </div>
      <div class="modal-body" id="escolhe_cliente_body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="javascript:atualiza_pagamento();">Salvar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop
@section('scripts')
<script>
    function ordena(campo){
        $('#ordenacao').val(campo);
        $('#form_pesquisa').submit();
    }

    function associar_cliente(id_pagamento){
        $('#btn_associar_cliente_'+id_pagamento).hide();
        $('#btn_associar_cliente_spin_'+id_pagamento).show();
        var url = '/retorno/associarcliente/'+id_pagamento;
        $('#escolhe_cliente_body').load(url, function(){
            $('#escolhe_cliente').modal();
            $('#btn_associar_cliente_spin_'+id_pagamento).hide();
            $('#btn_associar_cliente_'+id_pagamento).show();
            $('#escolhe_cliente_body').find('[data-fx="select2"]').each(function() {
                var e = $(this), t = e.attr("data-min-input") == undefined ? 0 : parseInt(e.attr("data-min-input"));
                e.select2({minimumInputLength: t})
            });
            $('#escolhe_cliente_body').find('[data-fx="masked"]').inputmask();
            $('#escolhe_cliente_body').find('[data-fx="datepicker"]').datepicker({format: 'dd/mm/yyyy', language: 'pt-BR', autoclose: true});
            $('#escolhe_cliente_body').find('#id_cliente').change(function(){
                atualiza_pagadores();
            })
        });
    }

    function atualiza_pagadores(){
        $("#id_pagador").find('option')
                        .remove()
                        .end()
                        .append('<option value="carregando">(carregando...)</option>')
                        .val('carregando');
        var url = '/cliente/pagadoresjson/'+$('#id_cliente').val();
        $.get(url, function(data){
            $("#id_pagador").find('option')
                            .remove();
            $.each(data, function(val, text) {
                $('#id_pagador').append( new Option(text,val) );
            });
            $("#id_pagador").val('');
        });
    }

    function atualiza_pagamento(){
        var url = "/retorno/atualizapagamentoduplicidade";
        var data = $('#frm_escolhe_cliente').serialize();
        $.post(url, data, function(data){
            if(data.status == 1){
                $('#escolhe_cliente').modal('hide');
                $.pnotify({title: 'Sucesso', text: 'Pagamento atualizado com sucesso', type: 'success'});
            }
        });
    }
</script>

@stop
