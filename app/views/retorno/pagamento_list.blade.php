@extends('stilearn-metro')

@section('conteudo')
<style>
    .controls{ margin-right: 20px;}
    .date,.number,.alfa{width:90px}
    .table{font-size:11px}
    #invisivel{display:none}
    .select2-container .select2-choice{margin-bottom: 10px;}
</style>

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Pagamentos do arquivo "{{$arquivo->nome_arquivo}}" (de {{$arquivo->formatDate('created_at')}})</h1></div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">

            @include('padrao.mensagens')
            <!-- search box -->
            @if ($pagamentos)
            <div class="row-fluid">
                {{$pagamentos->links()}}
                <table class="table table-hover table-striped table-condensed "  data-sorter="true"  style='width:100%'>
                    <thead>
                        <tr>
                            <th style='width:50px;text-align:center;'>Ações</th>
                            <th style='width:10px;text-align: center;'>ID</th>
                            <th style='width:150px;text-align: center;'>CNPJ</th>
                            <th style='width:auto;text-align: left;'>Nome</th>
                            <th style='width:auto;text-align: left;'>Cliente associado</th>
                            <th style='width:auto;text-align: left;'>Pagador associado</th>
                            <th style='width:120px;text-align: right;'>Valor cobrado</th>
                            <th style='width:120px;text-align: right;'>Valor pago</th>
                            <th style='width:120pxtext-align: center;'>Emitido em</th>
                            <th style='width:120px;text-align: center;'>Vencimento</th>
                            <th style='width:120px;text-align: center;'>Pagamento</th>
                            <th style='width:120px;text-align: center;'>Faturamento</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pagamentos as $pag)
                        <tr>
                            <td style="text-align:center;">
                                <a href="/retorno/revisarpagamento/{{$pag->id_pagamento}}" class="btn btn-primary"><i class="aweso-edit"></i></a>
                            </td>
                            <td style="text-align: center;">{{substr('000000'.$pag->id_pagamento, -6)}}</td>
                            <td style="text-align: center;">{{$pag->cnpj_formatado}}</td>
                            <td>{{$pag->nome_cliente}}</td>
                            <td>
                            @if($pag->fl_duplicidade_cnpj)
                                <a href="javascript:associar_cliente('{{$pag->id_pagamento}}');" class="btn btn-warning"><i class="aweso-question"></i></a>
                            @else
                                {{$pag->nome_cliente_associado}}
                            @endif
                            </td>
                            <td>{{$pag->nome_pagador_associado}}
                            <td style="text-align: right;">{{number_format($pag->valor, 2, ",", ".")}}</td>
                            <td style="text-align: right;">{{number_format($pag->valor_pago, 2, ",", ".")}}</td>
                            <td style="text-align: center;">{{$pag->formatDate('data_emissao')}}</td>
                            <td style="text-align: center;">{{$pag->formatDate('data_vencimento')}}</td>
                            <td style="text-align: center;">{{$pag->formatDate('data_pagamento')}}</td>
                            <td style="text-align: center;">{{$pag->faturamento_anomesvalor}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$pagamentos->links()}}
            </div>
            @endif
        </div>
    </div>
</article> <!-- /content page -->
<div class="modal fade modal-lg" role="dialog" id="escolhe_cliente" style="width:700px; margin-left:-320px;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Defina o cliente correto</h4>
      </div>
      <div class="modal-body" id="escolhe_cliente_body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="javascript:atualiza_pagamento();">Salvar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop

@section("scripts")
<script>
    function associar_cliente(id_pagamento){
        var url = '/retorno/associarcliente/'+id_pagamento;
        $('#escolhe_cliente_body').load(url, function(){
            $('#escolhe_cliente').modal();
            $('#escolhe_cliente_body').find('[data-fx="select2"]').each(function() {
                var e = $(this), t = e.attr("data-min-input") == undefined ? 0 : parseInt(e.attr("data-min-input"));
                e.select2({minimumInputLength: t})
            });
            $('#escolhe_cliente_body').find('[data-fx="masked"]').inputmask();
            $('#escolhe_cliente_body').find('[data-fx="datepicker"]').datepicker({format: 'dd/mm/yyyy', language: 'pt-BR', autoclose: true});
            $('#id_cliente').change(function(){
                atualiza_pagadores();
            })
        });
    }

    function atualiza_pagadores(){
        $("#id_pagador").find('option')
                        .remove()
                        .end()
                        .append('<option value="carregando">(carregando...)</option>')
                        .val('carregando');
        var url = '/cliente/pagadoresjson/'+$('#id_cliente').val();
        $.get(url, function(data){
            $("#id_pagador").find('option')
                            .remove();
            $.each(data, function(val, text) {
                $('#id_pagador').append( new Option(text,val) );
            });
            $("#id_pagador").val('');
        });
    }

    function atualiza_pagamento(){
        var url = "/retorno/atualizapagamentoduplicidade";
        var data = $('#frm_escolhe_cliente').serialize();
        $.post(url, data, function(data){
            if(data.status == 1){
                $('#escolhe_cliente').modal('hide');
                $.pnotify({title: 'Sucesso', text: 'Pagamento atualizado com sucesso'});
            }
        });
    }
</script>

@stop
