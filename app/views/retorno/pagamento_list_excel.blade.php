@foreach ($pagamentos as $pag)
<tr>
    <td style="text-align: center;">{{substr('000000'.$pag->id_pagamento, -6)}}</td>
    <td style="text-align: center;">{{$pag->numero}}</td>
    <td class="text" style="text-align: center;">{{$pag->nosso_numero}}</td>
    <td style="text-align: center;">{{$pag->cnpj_formatado}}</td>
    <td>{{$pag->nome_cliente_associado}}</td>
    <td style="text-align: right;">{{number_format($pag->valor, 2, ",", ".")}}</td>
    <td style="text-align: right;">{{number_format($pag->valor_pago, 2, ",", ".")}}</td>
    <td style="text-align: center;">{{$pag->formatDate('data_emissao')}}</td>
    <td style="text-align: center;">{{$pag->formatDate('data_vencimento')}}</td>
    <td style="text-align: center;">{{$pag->formatDate('data_pagamento')}}</td>
</tr>
@endforeach
