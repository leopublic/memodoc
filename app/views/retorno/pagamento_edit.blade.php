@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Revisar pagamento</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-remove"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Altere as informações e clique em salvar</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal', "files"=> true))}}
                    {{Form::hidden('id_pagamento', $pagamento->id_pagamento, array('id'=> 'id_pagamento'))}}
                    <div class='control-group'>
                        {{Form::label('valor','Data emissão',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('data_emissao',  $pagamento->data_emissao_fmt, array( 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                <span class="add-on"><i class="icomo-calendar"></i> </span>
                            </div>
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('valor','Data vencimento',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('data_vencimento',  $pagamento->data_vencimento_fmt, array( 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                <span class="add-on"><i class="icomo-calendar"></i> </span>
                            </div>
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('valor','Data pagamento',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append input-append-inline ">
                                {{Form::text('data_pagamento',  $pagamento->data_pagamento_fmt, array( 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                                <span class="add-on"><i class="icomo-calendar"></i> </span>
                            </div>
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('valor','Valor',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('valor',  'R$ '.number_format($pagamento->valor, 2, ",", "."), array('data-fx'=>"masked" , 'data-inputmask'=>"'mask': '999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true", 'style'=>'text-align: right;'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('valor','Valor pago',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::text('valor_pago',  'R$ '.number_format($pagamento->valor_pago, 2, ",", "."), array('data-fx'=>"masked" , 'data-inputmask'=>"'mask': '999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true", 'style'=>'text-align: right;'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('nome_cliente','Cliente no título',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append">
                                {{Form::text('cnpj',  $pagamento->cnpj_formatado, array('class'=>'input-medium','disabled'))}}
                                {{Form::text('nome_cliente',  $pagamento->nome_cliente, array('class'=>'input-xxlarge', 'disabled'))}}
                                <button type="button" class="btn btn-primary" onclick="javascript:adicionar_pagador();" id="btnAdicionarpagador">Adicionar como pagador</button>
                            </div>
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('id_cliente','Cliente do sistema',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_cliente', $clientes, $pagamento->id_cliente, array('id'=>'id_cliente', 'class' => 'input-block-level', 'data-fx'=>'select2'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('id_pagador','Pagador',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="input-append">
                            {{Form::select('id_pagador', $pagadores, $pagamento->id_pagador, array('id'=>'id_pagador', 'class'=> 'input-xxlarge'))}}
                             <button type="button" class="btn btn-warning" onclick="javascript:atualizar_pagador();" id="btnAtualizar"><i class="aweso-refresh"></i> Atualizar</button>
                         </div>
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('id_faturamento','Faturamento',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::select('id_faturamento', $faturamentos,$pagamento->id_faturamento, array('id'=>'id_faturamento', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class='control-group'>
                        {{Form::label('observacao','Observações',array('class'=>'control-label'))}}
                        <div class="controls">
                            {{Form::textarea('observacao', $pagamento->observacao, array('id'=>'observacao', 'class' => 'input-block-level'))}}
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" class="btn btn-primary"><i class="aweso-save"></i> Salvar</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>

        </div>
</article> <!-- /content page -->

@stop
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $( "#id_cliente" ).change(function() {
            atualiza_faturamentos();
        });
    });
    function atualiza_faturamentos(){
        var url = '/faturamento/listadocliente/'+$('#id_cliente').val();
        var faturamentos = $('#id_faturamento');
        faturamentos.empty();
        faturamentos.append($("<option />").val('').text('(carregando...)'));
        $.getJSON(url, { }, function(data) {
            faturamentos.empty();
            $.each(data, function(index, element) {
                faturamentos.append($("<option />").val(element.id_faturamento).text(element.anomesvalor));
            });
        });
    }

    function adicionar_pagador(){
        if ($('#id_cliente').val() == ''){
            bootbox.alert('Primeiro você precisa selecionar o cliente a quem o pagador será associado.');
        } else {
            window.open('/cliente/pagadoreseditar/'+$('#id_cliente').val()+'/0/'+$('#id_pagamento').val()+'#tab_pagadores');
        }
        $('#adicionar_pagador').modal();
    }

    function atualizar_pagador(){

    }
</script>
@stop
