@extends('stilearn-metro')

@section('conteudo')
<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Arquivo de retorno de pagamento</h1></div>
</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">
    <!-- main page -->
    <div class="main-page" id="">
        <div class="content-inner">
            @include('padrao/mensagens')
            <!-- search box -->
            <div class="widget border-cyan">
                <!-- widget header -->
                <div class="widget-header bg-cyan">
                    <!-- widget icon -->
                    <div class="widget-icon"><i class="aweso-remove"></i></div>
                    <!-- widget title -->
                    <h4 class="widget-title">Adicione um novo arquivo de retorno de pagamento do banco</h4>
                </div><!-- /widget header -->

                <!-- widget content -->
                <div class="widget-content bg-silver">
                    {{Form::open(array('class' => 'form-horizontal', "files"=> true))}}
                    <div class='control-group'>
                        {{Form::label('id_cliente','Arquivo',array('class'=>'control-label'))}}
                        <div class="controls">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input input-large">
                                        <i class="icon-file fileupload-exists"></i> 
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-primary btn-file">
                                        <span class="fileupload-new">Selecione um arquivo</span>
                                        <span class="fileupload-exists">Alterar</span>
                                        <input name="fileupload" type="file" id="file-upload" class="input-block-level"/>
                                    </span>
                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Cancelar</a>
                                </div>
                            </div><!-- /fileupload -->
                        </div>
                    </div>
                    <div class="form-actions bg-silver">
                        <button type="submit" name="adicionar" class="btn btn-primary" id="adicionar" ><i class="aweso-plus"></i> Enviar</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>

        </div>
</article> <!-- /content page -->
@stop
@section('scripts')

@stop