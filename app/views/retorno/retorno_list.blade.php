@extends('stilearn-metro')

@section('conteudo')
<style>
    .controls{ margin-right: 20px;}
    .date,.number,.alfa{width:90px}
    .table{font-size:11px}
    #invisivel{display:none}
    .select2-container .select2-choice{margin-bottom: 10px;}
</style>

<!-- content header -->
<header class="content-header">
    <!-- content title-->
    <div class="page-header"><h1>Arquivos de retorno importados</h1></div>

    <div class='content-action pull-right'>
        <a href='/retorno/novo' class='btn btn-primary'><i class="aweso-plus"></i> Adicionar um novo</a>
    </div>

</header> <!--/ content header -->

<!-- content page -->
<article class="content-page clearfix">

    <!-- main page -->
    <div class="main-page documento_localizar" id="">
        <div class="content-inner">

            @include('padrao.mensagens')
            <!-- search box -->
            <div class="search-box">
                {{Form::open()}}
                <div class="controls">
                    <div class="controls pull-left">
                        <div class="input-append input-append-inline ">
                            {{Form::text('inicio', Input::old("inicio") ,array('placeholder'=>"Data início", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                        </div>
                        <div class="input-append input-append-inline ">
                            {{Form::text('fim', Input::old('fim') ,array('placeholder'=>"Data Final", 'class'=>'date', 'data-date-format'=>'dd/mm/yyyy','data-fx' =>"datepicker"))}}
                            <span class="add-on"><i class="icomo-calendar"></i> </span>
                        </div>
                    </div>
                </div>
                <div class='controls pull-left'>
                    <div class="controls">
                        <button class="btn bg-cyan " type="submit" id="submit-pesquisa"><i class="aweso-search"></i> Buscar</button>
                    </div>
                </div>
                {{Form::close()}}
                <div class='clearfix'></div>
            </div>
            @if ($retornos)
            <div class="row-fluid">
                {{$retornos->links()}}
                <table class="table table-hover table-striped table-condensed "  data-sorter="true"  style='width:100%'>
                    <thead>
                        <tr>
                            <th style='width:250px;text-align:center;'>Ações</th>
                            <th style='width:10px;text-align: center;'>ID</th>
                            <th style='width:100px;text-align: center;'>Importado em</th>
                            <th style='width:auto;'>Nome do arquivo</th>
                            <th style='width:120px'>Qtd registros</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($retornos as $ret)
                        <tr>
                            <td style="text-align:center;">
                                <a class="btn btn-primary" href="/retorno/pagamentos/{{ $ret->id_arquivo_retorno }}" target="_blank" title="Ver detalhes"><i class="aweso-th-list"></i></a>
                                <a class="btn btn-primary" href="/retorno/processar/{{ $ret->id_arquivo_retorno }}" target="_blank" title="Reprocessar"><i class="aweso-refresh"></i></a>
                                @include('padrao.link_exclusao', array('url'=> '/retorno/excluir/'. $ret->id_arquivo_retorno , 'msg_confirmacao'=>'Clique aqui para confirmar a exclusão desse arquivo de retorno'))
                            </td>
                            <td style="text-align: center;">{{substr('000000'.$ret->id_arquivo_retorno, -6)}}</td>
                            <td style="text-align: center;">{{$ret->formatDate('created_at')}}</td>
                            <td>{{$ret->nome_arquivo}}</td>
                            <td style="text-align: center;">{{$ret->qtd_registros}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$retornos->links()}}
            </div>
            @endif
        </div>
    </div>
</article> <!-- /content page -->
@stop