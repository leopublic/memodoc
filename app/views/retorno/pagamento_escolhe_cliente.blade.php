{{Form::open(array('class' => 'form-horizontal', 'id' => 'frm_escolhe_cliente'))}}
{{Form::hidden('id_pagamento', $pagamento->id_pagamento, array('id'=> 'id_pagamento'))}}
<div class='control-group'>
    {{Form::label('cnpj','CNPJ no boleto',array('class'=>'control-label'))}}
    <div class="controls">
        {{Form::text('cnpj',  $pagamento->cnpj_formatado, array('class'=>'input-medium','disabled'))}}
    </div>
</div>
<div class='control-group'>
    {{Form::label('nome_cliente','Cliente no boleto',array('class'=>'control-label'))}}
    <div class="controls">
        {{Form::text('nome_cliente',  $pagamento->nome_cliente, array('class'=>'input-block-level', 'disabled'))}}
    </div>
</div>
<div class='control-group'>
    {{Form::label('valor','Valor',array('class'=>'control-label'))}}
    <div class="controls">
        {{Form::text('valor',  'R$ '.number_format($pagamento->valor, 2, ",", "."), array('data-fx'=>"masked" , 'data-inputmask'=>"'mask': '999,99', 'greedy': false, 'numericInput': true, 'placeholder':' ', 'clearMaskOnLostFocus': true", 'style'=>'text-align: right;', 'disabled'))}}
    </div>
</div>
<div class='control-group'>
    {{Form::label('id_cliente','Cliente do sistema',array('class'=>'control-label'))}}
    <div class="controls">
        {{Form::select('id_cliente', $clientes, $pagamento->id_cliente, array('id'=>'id_cliente', 'class' => 'input-block-level'))}}
    </div>
</div>
<div class='control-group'>
    {{Form::label('id_pagador','Pagador',array('class'=>'control-label'))}}
    <div class="controls" id="container_pagador">
        {{Form::select('id_pagador', $pagadores, $pagamento->id_pagador, array('id'=>'id_pagador', 'class'=> 'input-block-level'))}}
    </div>
</div>
{{ Form::close() }}
