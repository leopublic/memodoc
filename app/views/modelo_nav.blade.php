<!-- nav -->
<ul class="nav nav-list">
	<li class="dropdown-list active">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Dashboard</a>
		<ul class="dropdown-menu">
			<li class="active"><a href="index.html">Dashboard #1</a></li>
			<li><a href="index2.html">Dashboard #2</a></li>
			<li><a href="index3.html">Dashboard #3</a></li>
			<li><a href="index4.html">Dashboard #4</a></li>
		</ul>
	</li>
	<li class="dropdown-list">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Layouts</a>
		<ul class="dropdown-menu">
			<li><a href="layout-blank.html">Blank</a></li>
			<li><a href="layout-full-width.html">Full Width</a></li>
			<li><a href="layout-top-menus.html">Top Menus</a></li>
		</ul>
	</li>
	<li><a href="widgets.html">Widgets</a></li>
	<li class="dropdown-list">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Components</a>
		<ul class="dropdown-menu">
			<li><a href="ui-general.html">General</a></li>
			<li><a href="ui-typography.html">Typography</a></li>
			<li><a href="ui-buttons.html">Buttons</a></li>
			<li><a href="ui-alert-notify.html">Alert & Notify</a></li>
			<li><a href="ui-gauge.html">Gauge</a></li>
			<li><a href="ui-calendar.html">Calendar</a></li>
			<li><a href="ui-scrollbar.html">Scrollbar</a></li>
			<li><a href="ui-tabs-collapse.html">Tabs & Collapses</a></li>
			<li><a href="ui-sliders-bars.html">Sliders & Bars</a></li>
			<li><a href="ui-tiles.html">Tiles</a></li>
			<li><a href="ui-appbar.html">Appbar</a></li>
			<li><a href="ui-splash-page.html">Splash Page</a></li>
			<li><a href="ui-media-object.html">Media Object</a></li>
		</ul>
	</li>
	<li class="dropdown-list">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Form</a>
		<ul class="dropdown-menu">
			<li><a href="form-widget.html">Form Widget</a></li>
			<li><a href="form-elements.html">Elements</a></li>
			<li><a href="form-validation.html">Validation</a></li>
			<li><a href="form-wizard.html">Wizard</a></li>
			<li><a href="form-wysiwyg.html">Wysiwyg</a></li>
			<li><a href="form-code-editor.html">Code editor</a></li>
		</ul>
	</li>
	<li class="dropdown-list">
		<a href="charts.html" class="dropdown-toggle" data-toggle="dropdown-list">Charts</a>
		<ul class="dropdown-menu">
			<li><a href="chart-line.html">Lines Charts</a></li>
			<li><a href="chart-bar.html">Bar Charts</a></li>
			<li><a href="chart-pie.html">Pie Charts</a></li>
			<li><a href="chart-others.html">Other Charts</a></li>
		</ul>
	</li>
	<li><a href="grids.html">Grids</a></li>
	<li class="dropdown-list">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Tables</a>
		<ul class="dropdown-menu">
			<li><a href="table-basic.html">Basic</a></li>
			<li><a href="table-datatables.html">DataTables</a></li>
		</ul>
	</li>
	<li>
		<a href="icons.html"><span class="label">3</span> Icons</a>
	</li>
	<li class="dropdown-list">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown-list">Pages</a>
		<ul class="dropdown-menu">
			<li><a href="page-login.html">Login</a></li>
			<li class="divider"></li>
			<li><a href="page-search.html">Search</a></li>
			<li><a href="page-invoices.html">Invoices</a></li>
			<li><a href="page-inbox.html">Inbox</a></li>
			<li><a href="page-gallery.html">Gallery</a></li>
			<li class="divider"></li>
			<li><a href="page-error-404.html">Error 404</a></li>
			<li><a href="page-error-500.html">Error 500</a></li>
			<li><a href="page-coming-soon.html">Coming Soon</a></li>
		</ul>
	</li>
</ul>
<!-- /nav -->
