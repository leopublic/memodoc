<?php

App::before(function($request) {
    date_default_timezone_set('America/Sao_Paulo');
    App::singleton('ProtocoloOs', function() {
        return new ProtocoloOs;
    });

    App::singleton('PreencheCombo', function() {
        return new PreencheCombo;
    });
});


App::after(function($request, $response) {
    //
});

/*
  |--------------------------------------------------------------------------
  | Authentication Filters
  |--------------------------------------------------------------------------
  |
  | The following filters are used to verify that the user of the current
  | session is logged into this application. The "basic" filter easily
  | integrates HTTP Basic authentication for quick, simple checking.
  |
 */

Route::filter('autenticado', function() {
    if (Auth::guest()) {
        return Redirect::to('auth/login');
    }
});

Route::filter('backoffice', function() {
    if (Auth::guest()) {
        return Redirect::to('auth/login')->with('msg', 'Autenticação expirada. Favor informar suas credenciais novamente.');
    } else {
        if (Auth::user()->id_nivel < 10) {
            return Redirect::to('/home/bemvindocliente')->with('login-error', true)->with('msg', 'Função não autorizada para o seu perfil.');
        }
    }
});

Route::filter('auth.basic', function() {
    return Auth::basic();
});

/*
  |--------------------------------------------------------------------------
  | Guest Filter
  |--------------------------------------------------------------------------
  |
  | The "guest" filter is the counterpart of the authentication filters as
  | it simply checks that the current user is not logged in. A redirect
  | response will be issued if they are, which you may freely change.
  |
 */

//Route::filter('guest', function()
//{
//	if (Auth::check()) return Redirect::to('/');
//});

/*
  |--------------------------------------------------------------------------
  | CSRF Protection Filter
  |--------------------------------------------------------------------------
  |
  | The CSRF filter is responsible for protecting your application against
  | cross-site request forgery attacks. If this special token in a user
  | session does not match the one given in this request, we'll bail.
  |
 */

Route::filter('csrf', function() {
    if (Session::token() != Input::get('_token')) {
        throw new Illuminate\Session\TokenMismatchException;
    }
});
