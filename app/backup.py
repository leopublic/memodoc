#!/usr/bin/python
###########################################################
#
# This python script is used for mysql database backup
# using mysqldump utility.
#
# Written by : Rahul Kumar
# Website: https://tecadmin.net
# Created date: Dec 03, 2013
# Last modified: Dec 03, 2013
# Tested with : Python 2.6.6
# Script Revision: 1.1
#
##########################################################

# Import required python libraries
import os
import time
import datetime

# MySQL database details to which backup to be done. Make sure below user having enough privileges to take databases backup.
# To take multiple databases backup, create any file like /backup/dbnames.txt and put databses names one on each line and assignd to DB_NAME variable.

DB_HOST = 'localhost'
DB_USER = 'root'
DB_USER_PASSWORD = 'mysql'
#DB_NAME = '/backup/dbnames.txt'
DB_NAME = 'buscapele'
BACKUP_PATH = '/home/vagrant/backups'

# Getting current datetime to create seprate backup folder like "12012013-071334".
DATETIME = time.strftime('%Y%m%d')

# Faz o rodízio dos arquivos
for fileName in os.listdir(BACKUP_PATH):
    os.rename(fileName, fileName.replace("d9", "DEL"))
    os.rename(fileName, fileName.replace("d8", "d9"))
    os.rename(fileName, fileName.replace("d7", "d8"))
    os.rename(fileName, fileName.replace("d6", "d7"))
    os.rename(fileName, fileName.replace("d5", "d6"))
    os.rename(fileName, fileName.replace("d4", "d5"))
    os.rename(fileName, fileName.replace("d3", "d4"))
    os.rename(fileName, fileName.replace("d2", "d3"))
    os.rename(fileName, fileName.replace("d1", "d2"))
    os.rename(fileName, fileName.replace("d0", "d1"))

TODAYBACKUPPATH = BACKUP_PATH + DB_NAME+ "-d0" + DATETIME

dumpcmd = "mysqldump -u " + DB_USER + " -p" + DB_USER_PASSWORD + " " + DB_NAME + " > " + TODAYBACKUPPATH + ".sql"
os.system(dumpcmd)

print "Script finalizado com sucesso"
print "Your backups has been created in '" + TODAYBACKUPPATH + "' directory"