<?php
namespace Memodoc\Validadores;
class UsuarioExternoExistente extends Validador {
    public $id_unique;
    public static $rules = array(
        'nomeusuario'       => 'required',
        'id_nivel'       => 'required',
        'username'       => array('required', 'regex:/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,3})$/'),
    );
    public static $messages = array(
        'nomeusuario.required'       => 'O nome do usuário é obrigatório',
        'id_nivel.required'       => 'O nível do usuário é obrigatório',
        'username.required'       => 'O login é obrigatório',
        'username.regex'            => 'O login deve ser um e-mail válido',
        'username.unique'       => 'Já existe outro usuário com o login escolhido',
    );

    public function getRules(){
        $x = array_merge(static::$rules , array('username' => 'unique:usuario,username,'.$this->id_unique.',id_usuario'));
        return $x;
    }

}
