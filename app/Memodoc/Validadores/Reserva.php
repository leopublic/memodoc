<?php
namespace Memodoc\Validadores;
class Reserva extends Validador {
    public static $rules = array(
        'id_cliente'            => 'required|numeric|min:1',
        'id_tipodocumento'      => 'required|numeric|min:1',
    );
    public static $messages = array(
        'id_cliente.required'   => 'O cliente é obrigatório',
        'id_cliente.numeric'    => 'O cliente é obrigatório',
        'id_cliente.min'        => 'O cliente é obrigatório',
        'caixas.required'       => 'Informe a quantidade de endereços a serem reservados',
        'caixas.numeric'        => 'A quantidade de caixas deve ser um número maior que 0',
        'caixas.min'            => 'A quantidade de caixas deve ser maior que 0',
        'caixas.max'            => 'Não existem endereços disponíveis para a quantidade solicitada',
        'id_tipodocumento.required'   => 'O tipo do documento é obrigatório',
        'id_tipodocumento.numeric'    => 'O tipo do documento é obrigatório',
        'id_tipodocumento.min'        => 'O tipo do documento é obrigatório',
    );

    public function qtdCaixas($qtd){
        static::$rules['caixas'] = 'required|numeric|min:1|max:'.$qtd;
    }

    public function getRules() {
        if (!array_key_exists('caixas', static::$rules)){
            static::$rules['caixas'] = 'required|numeric|min:1|max:0';
        }
        return static::$rules;
    }

}
