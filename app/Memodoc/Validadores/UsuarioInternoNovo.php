<?php
namespace Memodoc\Validadores;
class UsuarioInternoNovo extends Validador {
    public static $rules = array(
        'nomeusuario'       => 'required',
        'id_nivel'       => 'required',
        'username'       => 'required',
        'password'    => 'required|confirmed',
        'username'      => 'unique:usuario,username',
        'password'      => 'between:8,20'
    );
    public static $messages = array(
        'nomeusuario.required'       => 'O nome do usuário é obrigatório',
        'id_nivel.required'       => 'O nível do usuário é obrigatório',
        'username.required'       => 'O login é obrigatório',
        'password.required'       => 'A senha é obrigatória',
        'password.between'       => 'A senha deve ter entre 8 e 20 caracteres',
        'password.confirmed'       => 'A confirmação da senha não bate com a senha',
        'username.unique'       => 'Já existe outro usuário com o login escolhido',
    );
}
