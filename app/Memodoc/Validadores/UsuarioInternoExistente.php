<?php
namespace Memodoc\Validadores;
class UsuarioInternoExistente extends Validador {
    public $id_unique;
    public static $rules = array(
        'nomeusuario'       => 'required',
        'id_nivel'       => 'required',
        'username'       => 'required',
    );
    public static $messages = array(
        'nomeusuario.required'       => 'O nome do usuário é obrigatório',
        'id_nivel.required'       => 'O nível do usuário é obrigatório',
        'username.required'       => 'O login é obrigatório',
        'username.unique'       => 'Já existe outro usuário com o login escolhido',
    );

    public function getRules(){
        $x = array_merge(static::$rules , array('username' => 'unique:usuario,username,'.$this->id_unique.',id_usuario'));
        return $x;
    }

}
