<?php
namespace Memodoc\Validadores;
class UsuarioExternoNovo extends Validador {
    public static $rules = array(
        'id_cliente'                => 'required|numeric',
        'nomeusuario'               => 'required',
        'id_nivel'                  => 'required',
        'username'                  => array('required', 'unique:usuario,username', 'regex:/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,3})$/'),
    );
    public static $messages = array(
        'id_cliente.numeric'        => 'O cliente é obrigatório',
        'id_cliente.required'       => 'O cliente é obrigatório',
        'id_cliente.min'            => 'O cliente é obrigatório',
        'nomeusuario.required'      => 'O nome do usuário é obrigatório',
        'id_nivel.required'         => 'O nível do usuário é obrigatório',
        'username.required'         => 'O login é obrigatório',
        'username.regex'            => 'O login deve ser um e-mail válido',
        'username.unique'           => 'Já existe outro usuário com o login escolhido',
    );
}
