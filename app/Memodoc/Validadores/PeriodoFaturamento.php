<?php
namespace Memodoc\Validadores;
class PeriodoFaturamento extends Validador {
    public static $rules = array(
        'dt_inicio_fmt'       => 'required',
        'dt_fim_fmt'       => 'required'
    );
    public static $messages = array(
        'dt_inicio_fmt.required'       => 'A data de início é obrigatória',
        'dt_fim_fmt.required'       => 'A data de fim é obrigatória'
    );
}
