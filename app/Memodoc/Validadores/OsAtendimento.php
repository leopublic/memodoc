<?php
namespace Memodoc\Validadores;
class OsAtendimento extends Validador {
    public static $rules = array(
        'solicitado_em_data'       => 'required',
        'entregar_em'       => 'required',
        'responsavel'       => 'required',
        'id_endeentrega'    => 'required',
    );
    public static $messages = array(
        'solicitado_em_data.required'       => 'A data de solicitação da ordem de serviço é obrigatória',
        'entregar_em.required'       => 'A data de execução é obrigatória',
        'responsavel.required'       => 'O solicitante da ordem de serviço é obrigatório',
        'id_endeentrega.required'       => 'O endereço é obrigatório',
    );
}
