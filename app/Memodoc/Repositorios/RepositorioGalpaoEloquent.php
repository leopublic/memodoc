<?php

namespace Memodoc\Repositorios;

class RepositorioGalpaoEloquent extends Repositorio {
    /**
     * Indisponibiliza um galpao para endereçamento
     * @return [type] [description]
     */
    public function indisponibiliza($id_galpao){
        \Galpao::where('id_galpao', '=', $id_galpao)
               ->update(array('fl_disponivel_enderecamento'=> '0'));
    }
    /**
     * Torna todos os galpoes indisponiveis para enderecamento
     * @return [type] [description]
     */
    public function indisponibiliza_todos(){
        \DB::table('galpao')
            ->update(array('fl_disponivel_enderecamento'=> '0'));
    }

    /**
     * Disponibiliza um galpão para endereçamento
     * @return [type] [description]
     */
    public function disponibiliza($id_galpao){
        \Galpao::where('id_galpao', '=', $id_galpao)
               ->update(array('fl_disponivel_enderecamento'=> '1'));
    }
    /**
     * Torna todos os galpoes disponiveis para enderecamento
     * @return [type] [description]
     */
    public function disponibiliza_todos(){
        \DB::table('galpao')
        ->update(array('fl_disponivel_enderecamento'=> '1'));
    }

    public function lista_disponiveis(){
        $sql = "select group_concat(id_galpao ) galpoes from galpao where fl_disponivel_enderecamento = 1";
        $ret = \DB::select(\DB::raw($sql));
        return $ret[0]->galpoes;
    }

    /**
     * Executa a retirada de uma caixa para emprestimo
     */
    public function emprestaCaixa($id_caixa, $id_usuario) {
        // Confirma que a caixa pode ser recebida
        $doc = \Endereco::where('id_caixa', '=', $id_caixa)->first();
        if (!isset($doc)) {
            throw new \Exception("Não foi encontrada uma referência com o id informado (" . $id_caixa . ')');
        } else {
            $sql = "select id_osdesmembrada "
                    . "  from osdesmembrada"
                    . " left join os on os.id_os_chave = osdesmembrada.id_os_chave"
                    . " where id_caixa = " . $id_caixa
                    . "   and coalesce(id_status_os, 2) > 1 "
                    . "   and status = " . \OsDesmembrada::stPENDENTE
                    . " order by id_osdesmembrada ";
            $ids = \DB::select(\DB::raw($sql));
            if (count($ids) > 0) {
                $id_osdesmembrada = $ids[0]->id_osdesmembrada;
                $emprestimo = \OsDesmembrada::find($id_osdesmembrada);
                $os = $emprestimo->os;
                // Só aceita checkout de OS ainda não atendida...
                if (is_object($os) && $os->id_status_os > 1 && $os->id_status_os != 5) {
                    \DB::beginTransaction();
                    // Atualiza empréstimo
                    $emprestimo->status = \OsDesmembrada::stATENDIDA;
                    $emprestimo->data_checkout = date('Y-m-d H:i:s');
                    $emprestimo->id_usuario_checkout = $id_usuario;
                    $emprestimo->save();
                    // Atualiza status dos documentos
                    \Documento::where('id_caixa', '=', $id_caixa)
                            ->update(array('emcasa' => 0, 'id_usuario_alteracao' => \Auth::user()->id_usuario, 'evento' => __CLASS__ . '.' . __FUNCTION__));
                    \DB::commit();
                } else {
                    throw new \Exception("Essa referência está numa OS que foi cancelada e por isso não deve ser retirada do galpão.");
                }
            } else {
                throw new \Exception("Não foi encontrada nenhuma caixa com esse ID aguardando checkout.");
            }
        }
    }

    public function registraSaidaExpurgo($id_caixa, $id_usuario){
        $expurgo = \Expurgo::where('id_caixa', '=', $id_caixa)->whereNull('data_checkout')->first();
        if (isset($expurgo) && is_object($expurgo)) {
            // Confirma que existe o expurgo pendente
            $expurgo->data_checkout = date('Y-m-d H:i:s');
            $expurgo->id_usuario_checkout = $id_usuario;
            $expurgo->save();
        } else {
            throw new \Exception("Essa referência não pode ser retirada pois não existe uma solicitação de empréstimo nem expurgo pendente para a mesma");
        }

    }

    public function retiraCaixaSemOs($id_caixa, $id_usuario) {

    }

    /**
     * Recupera a quantidade de caixas de um determinado cliente
     * em custódia, para um determinado perídodo
     */
    public function qtdCustodiaPeriodo($cliente, $dt_inicio, $dt_fim) {

    }

    public function efetivaMovimentacao($id_caixas, $observacao, $fl_cancelar_endereco, $id_usuario) {
        // Verifica se a movimentação pode ser criada para as caixas informadas
        if (count($id_caixas) == 0) {
            $this->registraErro('Informe pelo menos uma caixa para ser movimentada');
            return false;
        }
        // Verifica se todas as caixas são do mesmo tipo
        $tipos = \Documento::whereIn('id_caixa', $id_caixas)
                ->select(array('id_tipodocumento'))
                ->distinct()
                ->get();
        if (intval(count($tipos)) > 1) {
            $this->registraErro('Somente documentos de um mesmo tipo podem ser migrados juntos');
            return false;
        }
        $doc = $tipos->first();
        $id_tipodocumento = $doc->id_tipodocumento;
        // Confirma se tem espaço para movimentar

        \DB::beginTransaction();

        $docs = \Documento::whereIn('id_caixa', $id_caixas)
                ->select(array('id_caixa'))
                ->distinct()
                ->get();
        $disp = \Endereco::get_qtdDisponivel($id_tipodocumento);
        if (count($docs) > $disp) {
            $this->registraErro('Não há espaço disponível para reservar ' . count($docs) . ' endereços. Não será possível fazer a movimentação.');
            \DB::rollBack();
            return false;
        }
        $movimentacao = $this->criaMovimentacao($id_tipodocumento, $observacao, $fl_cancelar_endereco, $id_usuario);
        $this->movimentaCaixas($movimentacao, $id_caixas);
        \DB::commit();
        return $movimentacao;
    }

    /**
     * Cria uma nova movimentação
     * @param array $id_caixas Array com os ID das caixas a serem movimentadas
     * @param string $observacao Comentario sobre a movimentacao
     * @param boolean $fl_cancelar_endereco Indica se os endereços originais devem ser cancelados ou não
     */
    public function criaMovimentacao($id_tipodocumento, $observacao, $fl_cancelar_endereco, $id_usuario) {
        $movimentacao = new \Movimentacao;
        $movimentacao->observacao = $observacao;
        $movimentacao->id_tipodocumento = $id_tipodocumento;
        $movimentacao->fl_cancelar_endereco = $fl_cancelar_endereco;
        $movimentacao->id_usuario = $id_usuario;
        $movimentacao->save();
        return $movimentacao;
    }

    /**
     * Movimenta as caixas de uma movimentação
     * @param \Movimentacao $movimentacao
     */
    public function movimentaCaixas(\Movimentacao $movimentacao, $id_caixas) {
        $qtd = count($id_caixas);
        $disponiveis = \Endereco::get_cursorDisponiveis($movimentacao->id_tipodocumento)->take($qtd)->get();
        $i = 0;
        foreach ($id_caixas as $id_caixa) {
            // Recupera objetos envolvidos
            $disponivel = $disponiveis[$i];
            $endereco = \Endereco::where('id_caixa', '=', $id_caixa)->first();
            $endereco_orig = clone $endereco;
            // Registra a caixa movimentada
            $caixa = new \MovimentacaoCaixa;
            $caixa->id_movimentacao = $movimentacao->id_movimentacao;
            $caixa->id_caixa = $id_caixa;
            $caixa->endereco_antes = $endereco->enderecoCompleto();
            $caixa->endereco_depois = $disponivel->enderecoCompleto();
            $caixa->id_caixa_destino = $disponivel->id_caixa;
            $caixa->save();
            // Troca os enderecos das caixas
            $endereco->id_galpao = $disponivel->id_galpao;
            $endereco->id_rua = $disponivel->id_rua;
            $endereco->id_predio = $disponivel->id_predio;
            $endereco->id_andar = $disponivel->id_andar;
            $endereco->id_unidade = $disponivel->id_unidade;
            $endereco->observacao .= "\nEndereço alterado de " . $endereco_orig->enderecoCompleto() . " para " . $disponivel->enderecoCompleto() . " na movimentação " . $movimentacao->id_movimentacao;
            $disponivel->id_galpao = $endereco_orig->id_galpao;
            $disponivel->id_rua = $endereco_orig->id_rua;
            $disponivel->id_predio = $endereco_orig->id_predio;
            $disponivel->id_andar = $endereco_orig->id_andar;
            $disponivel->id_unidade = $endereco_orig->id_unidade;
            if ($movimentacao->fl_cancelar_endereco) {
                $disponivel->endcancelado = '1';
                $disponivel->disponivel = '0';
                $disponivel->observacao .= "\nEndereco movido para o ID " . $endereco->id_caixa . " na movimentacao " . $movimentacao->id_movimentacao;
            } else {
                $disponivel->observacao .= "\nEndereco trocado com o ID " . $endereco->id_caixa . " na movimentacao " . $movimentacao->id_movimentacao;
            }
            $disponivel->save();
            $endereco->save();
            $i++;
        }
    }

    public function criarPredio($id_tipodocumento, $id_galpao, $id_rua, $id_predio, $andar_ini, $andar_fim, $qtd_unidades) {
        // Testar se o prédio já existe
        $enderecos = \Endereco::where('id_galpao', '=', $id_galpao)
                ->where('id_rua', '=', $id_rua)
                ->where('id_predio', '=', $id_predio)
                ->where('id_andar', '>=', $andar_ini)
                ->where('id_andar', '<=', $andar_fim)
                ->count();
        if ($enderecos > 0) {
            throw new \Exception('Não é possível criar o prédio ' . $id_predio . ' na rua ' . $id_rua . ' do galpão ' . $id_galpao . ' porque já existem endereços criados nesse prédio. Tente criar andares ou apartamentos.');
        } else {
            for ($i = $andar_ini; $i <= $andar_fim; $i++) {
                $this->criarAndar($id_tipodocumento, $id_galpao, $id_rua, $id_predio, $i, $qtd_unidades);
            }
        }
    }

    public function criarAndar($id_tipodocumento, $id_galpao, $id_rua, $id_predio, $id_andar, $qtd_unidades) {
        // Testar se o andar já existe
        $enderecos = \Endereco::where('id_galpao', '=', $id_galpao)
                ->where('id_rua', '=', $id_rua)
                ->where('id_predio', '=', $id_predio)
                ->where('id_andar', '=', $id_andar)
                ->count();
        if ($enderecos > 0) {
            throw new \Exception('Não é possível criar o andar ' . $id_andar . ' no prédio ' . $id_predio . ' na rua ' . $id_rua . ' do galpão ' . $id_galpao . ' porque já existem endereços criados nesse andar. Tente criar apartamentos.');
        } else {
            $this->criarApartamentos($id_tipodocumento, $id_galpao, $id_rua, $id_predio, $id_andar, 1, $qtd_unidades);
        }
    }

    public function criarApartamentos($id_tipodocumento, $id_galpao, $id_rua, $id_predio, $id_andar, $id_unidade_ini, $id_unidade_fim) {
        // Testar se o andar já existe
        $enderecos = \Endereco::where('id_galpao', '=', $id_galpao)
                ->where('id_rua', '=', $id_rua)
                ->where('id_predio', '=', $id_predio)
                ->where('id_andar', '=', $id_andar)
                ->where('id_unidade', '>=', $id_unidade_ini)
                ->where('id_unidade', '<=', $id_unidade_fim)
                ->count();
        if ($enderecos > 0) {
            throw new \Exception('Não é possível criar as unidades de ' . $id_unidade_ini . ' a ' . $id_unidade_fim . ' no andar ' . $id_andar . ' na rua ' . $id_rua . ' do galpão ' . $id_galpao . ' porque já existem endereços com essas unidades nesse andar.');
        } else {
            for ($i = $id_unidade_ini; $i <= $id_unidade_fim; $i++) {
                $endereco = new \Endereco();
                $endereco->id_galpao = $id_galpao;
                $endereco->id_rua = $id_rua;
                $endereco->id_predio = $id_predio;
                $endereco->id_andar = $id_andar;
                $endereco->id_unidade = $i;
                $endereco->id_tipodocumento = $id_tipodocumento;
                $endereco->disponivel = 1;
                $endereco->reservado = 0;
                $endereco->reserva = 0;
                $endereco->endcancelado = 0;
                $endereco->id_usuario_criacao = \Auth::user()->id_usuario;
                $endereco->save();
            }
        }
    }

    /**
     * Faz o checkout de todos os expurgos pendentes de uma vez só.
     */
    public function checkoutTodosExpurgosPendentes($id_usuario, $nome, $id_cliente = '') {
        $data = date('Y-m-d H:i:s');
        $obs = 'Checkout da caixa realizado em massa em ' . $data . " por " . $nome;
        if ($id_cliente > 0){
            return \Expurgo::whereNull('data_checkout')
            ->where('id_cliente', '=', $id_cliente)
            ->update(array('data_checkout' => $data
                , 'id_usuario_checkout' => $id_usuario
                , 'observacao' => $obs));
        } else {
            return \Expurgo::whereNull('data_checkout')
            ->update(array('data_checkout' => $data
                , 'id_usuario_checkout' => $id_usuario
                , 'observacao' => $obs));
        }
    }

}
