<?php
namespace Memodoc\Repositorios;

use \Carbon;

class RepositorioConsultaBoleto extends RepositorioImportadorCsv
{

    public $validacao;

    public function posicoes_header(){
        return [
            'tiporeg' => [0,1]
            ,'operacao' => [2,7]       
        ];

    }

    public function posicoes_detalhe(){
        return [
            'id_reg' => [0,1]
            ,'tipo_doc'=> [1,2]
            ,'nosso_numero'=> [62,11]
            ,'nosso_numero_dv'=> [73,1]
            ,'codigo_baixa_recusa'=> [80,2]
            ,'movimento'=> [108,2]
            ,'dt_entrada'=> [110,6]
            ,'seu_numero'=> [116,10]
            ,'dt_vencimento'=> [146,6]
            ,'valor'=> [152,13]
            ,'dt_credito'=> [175,6]
            ,'valor_tarifa'=> [181,7]
            ,'valor_recebido'=> [253,13]
            ,'valor_juros'=> [266,13]
            ,'cnpj'=> [342,14] 
        ];
    }

    public function processa_arquivo($id_consulta_boletos){
        $consulta = \ConsultaBoletos::find($id_consulta_boletos);
        $arq = fopen($consulta->caminho_com_arquivo(), 'r');

        \RegistroConsultaBoletos::where('id_consulta_boletos', '=', $id_consulta_boletos)->delete();

        $ordem = 0;
        $tipo = '';

        $i_razaosocial = 0;        
        $i_nosso_numero = 8;
        $i_seu_numero = 11;
        $i_dt_entrada = 17;
        $i_dt_vencimento = 18;
        $i_valor = 20;

        while ($linha = fgets($arq)) {
            $ordem++;
            $campos = explode(",", $linha);
            $razaosocial = trim($campos[0]);
            if ($razaosocial == 'Tipo Consulta:'){
                $status = $campos[4];
            }

            if($razaosocial == 'Sacado'){
                $i_nosso_numero = array_search('Nosso Número', $campos);
                $i_seu_numero = array_search('Seu Número', $campos);
                $i_dt_entrada = array_search('Entrada', $campos);
                $i_dt_vencimento = array_search('Vencimento', $campos);
                $i_valor = array_search('Valor', $campos);
            }

            if($razaosocial != '' && $razaosocial != 'Cedente:' && $razaosocial != 'Tipo Consulta:' && $razaosocial != 'Conta Corrente:' && $razaosocial != 'Sacado' && $razaosocial != 'Ordenado por:' && $razaosocial != 'Gerado em:'){
                
                $reg = new \RegistroConsultaBoletos;
                $reg->id_consulta_boletos = $id_consulta_boletos;
                $reg->registro = $linha;
                $reg->ordem = $ordem;
                $reg->status = $status;
                $reg->razaosocial = $campos[0];
                $reg->nosso_numero = $campos[$i_nosso_numero];
                $reg->seu_numero = $campos[$i_seu_numero];

                if(trim($campos[$i_dt_entrada]) != ''){
                    try{
                        $data = Carbon::createFromFormat('d/m/Y', $campos[$i_dt_entrada])->format('Y-m-d');
                        $reg->dt_entrada = $data;
                    } catch(\Exception $e){

                    }
                }

                if(trim($campos[$i_dt_vencimento]) != ''){
                    try{
                        $data = Carbon::createFromFormat('d/m/Y', $campos[$i_dt_vencimento])->format('Y-m-d');
                        $reg->dt_vencimento = $data;
                    } catch(\Exception $e){

                    }
                }

                $cliente = \Cliente::where('razaosocial', 'like', $razaosocial.'%')->get();
                if (count($cliente) == 1){
                    $reg->id_cliente = $cliente[0]->id_cliente;
                }
                //Valor
                $xvalor = str_replace(".", "", str_replace('"', "", $campos[$i_valor])).".".str_replace('"', "", $campos[$i_valor+1]);
                $reg->valor = $xvalor;
                $reg->save();
        
            }

        }
        fclose($arq);

    }
}
