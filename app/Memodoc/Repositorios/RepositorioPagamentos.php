<?php
namespace Memodoc\Repositorios;
/**
 * Processamento dos retornos do banco
 *
 * No retorno são enviados 2 tipos de registro.
 * - Registros "01" que são títulos emitidos.
 * - Registros "03" que são pagamentos recebidos de títulos emitidos.
 *
 * Existem também :
 * - Registro "00" que é o cabeçalho do arquivo.
 * - Registros "99" que é o rodapé do arquivo.
 */
class RepositorioPagamentos extends Repositorio
{
	/**
	 * Número da linha que está sendo processada
	 * @var integer
	 */
	public $numero_linha;

	/**
	 * Cria no sistema um novo arquivo enviado pelo usuário.
	 * @param  [type] $file       [description]
	 * @param  [type] $id_usuario [description]
	 * @return [type]             [description]
	 */
	public function cria_arquivo($file, $id_usuario){
		$arq = new \ArquivoRetorno;
		$arq->nome_arquivo = $file->getClientOriginalName();
		$arq->data_importacao = date('Y-m-d H:i:s');
		$arq->id_usuario= $id_usuario;
		$arq->mime_type = $file->getMimeType();
		$arq->save();
		$file->move($arq->caminho(), $arq->id_arquivo_retorno);
		$this->processa_arquivo($arq);
	}

	public function processa_arquivo($arq){
		set_time_limit(0);
		\DB::connection()->disableQueryLog();
		$handle = fopen($arq->caminhoComNome(), "r");
		\Pagamento::where('id_arquivo_retorno', '=', $arq->id_arquivo_retorno)->delete();
		if ($handle) {
			// Pula primeira linha
			$line = fgets($handle);
			// Processa arquivo
			$i = 0;
		    while (!feof($handle)) {
				$line = fgets($handle);
				$i++;
				$registro = $this->cria_registro($line, $i);
		    	if (isset($registro)){
					$registro->processa($arq->id_arquivo_retorno);
		    	}
		    }
		    fclose($handle);
			// Registrar a data de processamento do arquivo e indicar que ele foi processado com sucesso.
		} else {
		    // error opening the file.
		}
		\DB::connection()->enableQueryLog();
	}

	public function cria_registro($line, $i){
		switch(substr($line, 0, 2)){
			case '01':
				$registro = new RegistroTitulo($line, $i);
				break;
			case '03':
				$registro = new RegistroPagamento($line, $i);
				break;
			default:
				$registro = null;
				break;
		}
		return $registro;
	}
	/**
	 * Registra um problema encontrado no processamento de algum registro.
	 * @param  [type] $numero_linha [description]
	 * @param  [type] $registro     [description]
	 * @param  [type] $problema     [description]
	 * @return [type]               [description]
	 */
	public function registrar_problema($numero_linha, $registro, $problema){

	}

	public function associa_faturamento($pagamento){

	}
}
