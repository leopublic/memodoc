<?php

namespace Memodoc\Repositorios;

class RepositorioDocumentoEloquent extends Repositorio {

    /**
     * Atualiza um array de referências de caixas a partir de um intervalo informado.
     *
     * Verifica se as caixas existem ou se já estão no array
     *
     * @param type $id_caixapadrao_ini      Referência da caixa inicial a ser adicionada
     * @param type $id_caixapadrao_fim      Referência da caixa final a ser adicionada
     * @param type $id_cliente              Cliente das caixas
     */
    public function atualizaRelacaoDeCaixas($id_caixasatual, $id_caixapadrao_ini, $id_caixapadrao_fim, $id_cliente) {
        $id_caixaspadrao = array();
        if (intval($id_caixapadrao_ini) == 0) {
            $this->registraErro('Informe o número da caixa que você quer adicionar');
        } else {
            if ($id_caixapadrao_fim == '') {
                $id_caixapadrao_fim = $id_caixapadrao_ini;
            }
            for ($index = $id_caixapadrao_ini; $index <= $id_caixapadrao_fim; $index++) {
                if (in_array($index, $id_caixasatual)) {
                    $this->registraErro('Caixa número ' . $index . ' já foi adicionada anteriormente e foi ignorada.');
                } else {
                    $doc = \Documento::where('id_cliente', '=', $id_cliente)
                            ->where('id_caixapadrao', '=', $index)
                            ->first();
                    if (is_object($doc) && $doc->id_documento > 0) {
                        $id_caixaspadrao[] = $index;
                    } else {
                        $this->registraErro('Caixa número ' . $index . ' não existe no cliente.');
                    }
                }
            }
        }
        return $id_caixaspadrao;
    }

    /**
     * Atualiza um array de referências de caixas a partir de um intervalo informado.
     *
     * Verifica se as caixas existem ou se já estão no array
     *
     * @param type $id_caixapadrao_ini      Referência da caixa inicial a ser adicionada
     * @param type $id_caixapadrao_fim      Referência da caixa final a ser adicionada
     * @param type $id_cliente              Cliente das caixas
     */
    public function atualizaRelacaoDeCaixasPorId($id_caixasatual, $id_caixa_nova) {
        $id_caixaspadrao = array();
        if (intval($id_caixa_nova) == 0) {
            $this->registraErro('Informe o número da caixa que você quer adicionar');
        } else {
            if (in_array($id_caixa_nova, $id_caixasatual)) {
                $this->registraErro('Caixa número ' . $id_caixa_nova . ' já foi adicionada anteriormente e foi ignorada.');
            } else {
                $doc = \Documento::where('id_caixa', '=', $id_caixa_nova)
                        ->first();
                $end = \Endereco::where('id_caixa', '=', $id_caixa_nova)->first();
                if (is_object($end)) {
                    if ($end->id_cliente > 0) {
                        if (intval($end->endcancelado) == 0) {
                            $id_caixaspadrao[] = $id_caixa_nova;
                        } else {
                            $this->registraErro('Esse ID ' . $id_caixa_nova . ' foi cancelado e não pode ser movido. Verifique');
                        }
                    } else {
                        $this->registraErro('Esse ID ' . $id_caixa_nova . ' não foi usado ainda. Verifique');
                    }
                } else {
                    $this->registraErro('Não existe uma caixa com o ID ' . $id_caixa_nova . '.');
                }
            }
        }
        return $id_caixaspadrao;
    }

    public function excluaPeloEndereco(\Endereco $end, $id_usuario, $evento) {
        $docs = \Documento::where('id_caixa', '=', $end->id_caixa)->get();
        foreach ($docs as $doc) {
            $this->exclua($doc, $id_usuario, $evento);
        }
    }

    public function exclua(\Documento $doc, $id_usuario, $evento) {
        if (intval($doc->id_item) == 1) {
            throw new \Exception('O primeiro item da caixa não pode ser excluído pois isso vai expurgar a caixa');
        }

        $qtd = \Documento::where('id_caixa', '=', $doc->id_caixa)->count();
        if (intval($qtd) <= 1) {
            throw new \Exception('O único item da caixa não pode ser excluído pois isso vai expurgar a caixa.');
        }

        $this->expurgue($doc, $id_usuario, null, $evento);
    }

    /**
     * Deleta um documento, sem verificar se ele pode ser deletado ou não.
     * @param \Documento $doc
     * @param type $id_usuario
     * @param type $evento
     */
    public function expurgue(\Documento $doc, $id_usuario, $id_expurgo, $evento) {
        $canc = new \DocumentoCancelado;
        $canc->id_documento = $doc->id_documento;
        $canc->id_cliente = $doc->id_cliente;
        $canc->id_caixapadrao = $doc->id_caixapadrao;
        $canc->id_box = $doc->id_box;
        $canc->id_item = $doc->id_item;
        $canc->id_departamento = $doc->id_departamento;
        $canc->id_setor = $doc->id_setor;
        $canc->id_centro = $doc->id_centro;
        $canc->id_tipodocumento = $doc->id_tipodocumento;
        $canc->id_caixa = $doc->id_caixa;
        $canc->id_reserva = $doc->id_reserva;
        $canc->id_expurgo = $id_expurgo;
        $canc->titulo = $doc->titulo;
        $canc->migrada_de = $doc->migrada_de;
        $canc->inicio = $doc->inicio;
        $canc->fim = $doc->fim;
        $canc->expurgo_programado = $doc->expurgo_programado;
        $canc->expurgarem = $doc->expurgarem;
        $canc->conteudo = $doc->conteudo;
        $canc->usuario = $doc->usuario;
        $canc->emcasa = $doc->emcasa;
        $canc->primeira_entrada = $doc->primeira_entrada;
        $canc->data_digitacao = $doc->data_digitacao;
        $canc->expurgada_em = $doc->expurgada_em;
        $canc->itemalterado = $doc->itemalterado;
        $canc->planilha_eletronica = $doc->planilha_eletronica;
        $canc->request_recall = $doc->request_recall;
        $canc->nume_inicial = $doc->nume_inicial;
        $canc->nume_final = $doc->nume_final;
        $canc->alfa_inicial = $doc->alfa_inicial;
        $canc->alfa_final = $doc->alfa_final;
        $canc->created_at_origin = $doc->created_at;
        $canc->updated_at_origin = $doc->updated_at;
        $canc->id_usuario = $id_usuario;
        $canc->evento = $evento;

        $canc->save();
        $doc->delete();
    }

    /**
     * Deleta um documento, sem verificar se ele pode ser deletado ou não.
     * @param \Documento $doc
     * @param type $id_usuario
     * @param type $evento
     */
    public function expurgue_sql($id_documento, $id_usuario, $id_expurgo, $evento) {
        $sql = "insert into documento_cancelado(id_documento, id_cliente, id_caixapadrao, id_box, id_item, id_departamento, id_setor, id_centro, id_tipodocumento, id_caixa, id_reserva, id_expurgo, titulo, migrada_de, inicio, fim, expurgo_programado, expurgarem, conteudo, usuario, emcasa, primeira_entrada, data_digitacao, expurgada_em, itemalterado, planilha_eletronica, request_recall, nume_inicial, nume_final, alfa_inicial, alfa_final, created_at_origin, updated_at_origin, id_usuario, evento) select id_documento, id_cliente, id_caixapadrao, id_box, id_item, id_departamento, id_setor, id_centro, id_tipodocumento, id_caixa, id_reserva, ".$id_expurgo.", titulo, migrada_de, inicio, fim, expurgo_programado, expurgarem, conteudo, usuario, emcasa, primeira_entrada, data_digitacao, expurgada_em, itemalterado, planilha_eletronica, request_recall, nume_inicial, nume_final, alfa_inicial, alfa_final, created_at_origin, updated_at_origin, ".$id_usuario.", '".$evento."' from documento where id_documento = ".$id_documento;
        \DB::insert($sql);
        $sql = "delete from documento where id_documento = ".$id_documento;
        \DB::delete($sql);
    }

    /**
    /**
     * Cria um novo item em branco em uma caixa existente
     * @param type $id_cliente
     * @param type $id_caixapadrao
     * @return \Memodoc\Repositorios\Documento
     */

    public function novoItem($id_cliente, $id_caixapadrao, $id_usuario) {
        $xdoc = \Documento::where('id_cliente', '=', $id_cliente)
                ->where('id_caixapadrao', '=', $id_caixapadrao)
                ->first();

        $doc = new \Documento();
        $doc->id_caixapadrao = $id_caixapadrao;
        $doc->id_caixa = $xdoc->id_caixa;
        $doc->id_tipodocumento = $xdoc->id_tipodocumento;
        $doc->id_reserva = $xdoc->id_reserva;
        $doc->emcasa = $xdoc->emcasa;
        $doc->primeira_entrada = $xdoc->primeira_entrada;
        $doc->data_digitacao = date('Y-m-d');
        $doc->usuario = $id_usuario;
        $doc->id_cliente = $id_cliente;
        $doc->fl_revisado = 0;
        $id_item = \Documento::where('id_cliente', '=', $id_cliente)
                ->where('id_caixapadrao', '=', $id_caixapadrao)
                ->max('id_item');

        $doc->id_item = $id_item + 1;
        return $doc;
    }

    /**
     * Altera o status do item para revisado
     * @param type $id_documento
     * @param type $id_usuario
     */
    public function revisar($id_documento, $id_usuario){
        $doc = \Documento::findOrFail($id_documento);
        $doc->fl_revisado = 1;
        $doc->dt_revisao = date('Y-m-d H:i:s');
        $doc->id_usuario_revisao = $id_usuario;
        $doc->id_usuario_alteracao = $id_usuario;
        $doc->evento = __CLASS__.".".__FUNCTION__;
        $doc->save();
    }

    /**
     * Altera o status do item para liberado para alteração
     * @param type $id_documento
     * @param type $id_usuario
     */
    public function liberar($id_documento, $id_usuario){
        $doc = \Documento::findOrFail($id_documento);
        $doc->fl_revisado = 0;
        $doc->dt_revisao = null;
        $doc->id_usuario_revisao = null;
        $doc->dt_liberacao = date('Y-m-d H:i:s');
        $doc->id_usuario_liberacao = $id_usuario;
        $doc->evento = __CLASS__.".".__FUNCTION__;
        $doc->save();
    }

    public function caixasEmIntervalo($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim){
        $docs = array();
        if (intval($id_caixapadrao_fim) ==0 ){
            $doc = \Documento::where('id_cliente', '=', $id_cliente)
                    ->where('id_caixapadrao', '=', $id_caixapadrao_ini)
                    ->first();
            if (!is_object($doc) || intval($doc->id_caixa) == 0){
                $doc = new \Documento;
                $doc->id_caixa = 0;
                $doc->id_cliente = $id_cliente;
                $doc->id_caixapadrao = $id_caixapadrao_ini;
            }
            $docs[] = $doc;
        } else {
            for($i=$id_caixapadrao_ini; $i<=$id_caixapadrao_fim; $i++){
                $doc = \Documento::where('id_cliente', '=', $id_cliente)
                        ->where('id_caixapadrao', '=', $i)
                        ->first();
                if (!is_object($doc) || intval($doc->id_caixa) == 0){
                    $doc = new \Documento;
                    $doc->id_caixa = 0;
                    $doc->id_cliente = $id_cliente;
                    $doc->id_caixapadrao = $i;
                }
                $docs[] = $doc;
            }
        }
        return $docs;
    }

    public function adicionaStatusCaixas($caixas, $franquiacustodia){
        foreach($caixas as &$caixa){
            if (intval($caixa->emcasa) == 0){
                if ($caixa->primeira_entrada == '' || $caixa->primeira_entrada == '0000-00-00'){
                    if (intval($franquiacustodia) > 0){
                        if (count($caixa->reserva) > 0){
                            if ($caixa->reserva->idade_reserva > $franquiacustodia){
                                $caixa->status = 'emcarencia';
                                $caixa->title="Em carência";
                            } else {
                                $caixa->status = 'emfranquia';
                                $caixa->title="Em franquia";
                            }
                        } else {
                            if ($caixa->titulo == 'CAIXA NOVA ENVIADA PARA CLIENTE' && $caixa->conteudo == ''){
                                $caixa->status = 'reservadas';
                                $caixa->title="Não enviada e não inventariada";
                            } else {
                                $caixa->status = 'naoenviadas';
                                $caixa->title="Não enviada e inventariada";
                            }
                        }
                    } else {
                        if ($caixa->titulo == 'CAIXA NOVA ENVIADA PARA CLIENTE' && $caixa->conteudo == ''){
                            $caixa->status = 'reservadas';
                            $caixa->title="Não enviada e não inventariada";
                        } else {
                            $caixa->status = 'naoenviadas';
                            $caixa->title="Não enviada e inventariada";
                        }
                    }
                } else {
                    $caixa->status = 'emconsulta';
                    $caixa->title="Enviada para custódia mas depois emprestada";
                }
            } else {
                $caixa->status = 'nogalpao';
                $caixa->title="No galpão";
            }
        }
        return $caixas;
    }
    /**
     * Adiiciona itens em lote pela digitação
     * @param  [type] $post [description]
     * @return [type]       [description]
     */
    public function adiciona_itens_lote($os, $id_caixa, $ids_txt){
        $id_cliente = $os->id_cliente;
        $doc = \Documento::where('id_caixa', '=', $id_caixa)->first();
        $qtddocs = \Documento::where('id_caixa', '=', $id_caixa)->count();
        $id_caixapadrao = $doc->id_caixapadrao;
        $msg = '';
        $ignorados = 0;
        $ja_cadastrados = array();
        $nao_encontrados = array();
        $processados = array();
        $msg_adicionados = '';
        $msg_erro = "";
        $virg = '';
        $ids = explode('##', $ids_txt);
        foreach($ids as $id){
            // Verifica se tem item de remessa com o ID informado ainda pendente
            $item_remessa = \ItemRemessa::whereRaw('id_remessa in (select id_remessa from remessa where id_cliente='.$id_cliente.")")
                                    ->where('codigo', '=', $id)
                                    ->whereNull('id_documento')
                                    ->first();
            // Se Não encontrou
            if (count($item_remessa) ==0){
                $nao_encontrados[] = array('id' => $id);
                $msg_erro .= "<br/>Item ".$id." não foi encontrado na remessa. Verifique.";
            } else {
                // Não deu erro nenhum, então cria o doc
                
                //Testa a caixa só tem o doc inicial e se tiver usa
                if($qtddocs == 1 && $doc->titulo = "CAIXA NOVA ENVIADA PARA CLIENTE" && $doc->conteudo == ''){
                    $doc->nume_inicial = $id;
                    $doc->titulo = $item_remessa->titulo_montado();
                    $doc->conteudo = $item_remessa->conteudo_montado();
                } else {
                    // Cria novo
                    $doc = $this->novoItem($id_cliente, $id_caixapadrao, \Auth::user()->id_usuario);
                    $doc->nume_inicial = $id;
                    $doc->titulo = $item_remessa->titulo_montado();
                    $doc->conteudo = $item_remessa->conteudo_montado();
                }
                $doc->save();

                //Cria os_item_retirado
                $ositemr = new \OsItemRetirado;
                $ositemr->id_os_chave = $os->id_os_chave;
                $ositemr->id_documento = $doc->id_documento;
                $ositemr->id_item_remessa = $item_remessa->id_item_remessa;
                $ositemr->save();


                $item_remessa->id_documento = $doc->id_documento;
                $item_remessa->save();
                $processados[] = array('item' => $id, 'id_item'=>$doc->id_item);
                $msg_adicionados .= $virg.$id;
                $virg = ',';
            }
        }

        $msg = "Itens ".$msg_adicionados." adicionados com sucesso.";
        if ($msg_erro != '') {
            $msg .=  "<br/>Os seguintes itens apresentaram erros: ".$msg_erro;
        }
        $ret = array(
                'ja_cadastrados'    => $ja_cadastrados
                , 'nao_encontrados' => $nao_encontrados
                , 'processados'     => $processados
                , 'msg'             => $msg
            );
        return $ret;
    }


}