<?php
namespace Memodoc\Repositorios;

use \Carbon;

class RepositorioSicoobRetorno extends RepositorioImportadorCsv
{

    public $validacao;

    public function instancia_objeto_arquivo($id){
        $this->id_objeto = $id;
        $this->objeto_arquivo = \SicoobRetorno::find($id_sicoob_retorno);        
    }
    public function caminho_arquivo(){
        return $this->$objeto_arquivo->caminho_com_arquivo();
    }

    public function resseta_registros_importados(){
        \RegistroConsultaBoletos::where('id_consulta_boletos', '=', $this->id_objeto)->delete();
    }

    public function posicoes_header(){
        return [
            'tiporeg' => [0,1]
            ,'operacao' => [2,7]       
        ];

    }

    public function posicoes_detalhe(){
        return [
            'id_reg' => [0,1]
            ,'tipo_doc'=> [1,2]
            ,'nosso_numero'=> [62,11]
            ,'nosso_numero_dv'=> [73,1]
            ,'codigo_baixa_recusa'=> [80,2]
            ,'movimento'=> [108,2]
            ,'dt_entrada'=> [110,6]
            ,'seu_numero'=> [116,10]
            ,'dt_vencimento'=> [146,6]
            ,'valor'=> [152,13]
            ,'dt_credito'=> [175,6]
            ,'valor_tarifa'=> [181,7]
            ,'valor_recebido'=> [253,13]
            ,'valor_juros'=> [266,13]
            ,'cnpj'=> [342,14] 
        ];
    }

    public function processa_arquivo($id){
        $this->instancia_objeto_arquivo($id);
        $arq = fopen($this->caminho_arquivo(), 'r');

        \RegistroSicoobRetorno::where('id_sicoob_retorno', '=', $id_sicoob_retorno)->delete();

        $ordem = 0;
        $tipo = '';

        $pos_header = $this->posicoes_header();
        $pos_det = $this->posicoes_detalhe();

        while ($linha = fgets($arq)) {
            $ordem++;
            $tipo = substr($linha, 0, 1);
            if ($tipo == "0"){

            }elseif($tipo == "1"){
                    
                $reg = new \RegistroSicoobRetorno;
                $reg->ordem = $ordem;
                $reg->id_sicoob_retorno = $retorno->id_sicoob_retorno;
                $reg->tipo_doc = substr($linha, $pos_det["tipo_doc"][0], $pos_det["tipo_doc"][1]);
                $reg->nosso_numero = substr($linha, $pos_det["nosso_numero"][0], $pos_det["nosso_numero"][1]);
                $reg->nosso_numero_dv = substr($linha, $pos_det["nosso_numero_dv"][0], $pos_det["nosso_numero_dv"][1]);
                $reg->codigo_baixa_recusa = substr($linha, $pos_det["codigo_baixa_recusa"][0], $pos_det["codigo_baixa_recusa"][1]);
                $reg->movimento = substr($linha, $pos_det["movimento"][0], $pos_det["movimento"][1]);
                $reg->dt_entrada = $this->formata_data(substr($linha, $pos_det["dt_entrada"][0], $pos_det["dt_entrada"][1]));
                $reg->seu_numero = substr($linha, $pos_det["seu_numero"][0], $pos_det["seu_numero"][1]);
                $reg->dt_vencimento = $this->formata_data(substr($linha, $pos_det["dt_vencimento"][0], $pos_det["dt_vencimento"][1]));
                $reg->valor = substr($linha, $pos_det["valor"][0], $pos_det["valor"][1])/100;
                $reg->dt_credito = substr($linha, $pos_det["dt_credito"][0], $pos_det["dt_credito"][1]);
                $reg->valor_tarifa = substr($linha, $pos_det["valor_tarifa"][0], $pos_det["valor_tarifa"][1])/100;
                $reg->valor_recebido = substr($linha, $pos_det["valor_recebido"][0], $pos_det["valor_recebido"][1])/100;
                $reg->valor_juros = substr($linha, $pos_det["valor_juros"][0], $pos_det["valor_juros"][1])/100;
                $reg->cnpj = substr($linha, $pos_det["cnpj"][0], $pos_det["cnpj"][1]);


                $cliente = \Cliente::where('cgc', '=', $reg->cnpj)->get();
                if (count($cliente) == 1){
                    $reg->id_cliente = $cliente[0]->id_cliente;
                }
                //Valor
                $reg->save();
            }
        

        }
        fclose($arq);

    }

    public function formata_data($valor){
        if(trim($valor) != ''){
            try{
                return Carbon::createFromFormat('dmy', $valor)->format('Y-m-d');
            } catch(\Exception $e){
                return null;
            }
        }

    }
}
