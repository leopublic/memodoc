<?php
namespace Memodoc\Repositorios;

use \Carbon;

class RepositorioImportadorNotasEmitidas extends RepositorioImportadorCsv
{

    public function separador_campos(){
        return ";";
    }

    public function instancia_objeto_arquivo($id){
        $this->id_objeto = $id;
        $this->objeto_arquivo = \NotaCariocaRetorno::find($id);        
    }

    public function caminho_arquivo(){
        return $this->objeto_arquivo->caminho_com_arquivo();
    }

    public function resseta_registros_importados(){
        // TODO:: Implementar
    }

    public function carrega_valores_lidos($campos_lidos){
        $campos = [
             "nfis_numero" => [ "tipo" => "T" , "ordem" => 1, "valor" => ""]
            , "codigo_verificacao" => [ "tipo" => "T" , "ordem" => 3, "valor" => ""]
             , "dthr_emissao" => [ "tipo" => "D"  , "ordem" => 4, "formato_dt" => "d/m/Y H:i:s", "valor" => ""]
            , "serie_rps" => [ "tipo" => "T"  , "ordem" => 6, "valor" => "", "ignore" => true]
            , "numero_rps" => [ "tipo" => "T"  , "ordem" => 7, "valor" => "", "ignore" => true]
            , "fl_tipo_documento" => [ "tipo" => "T"  , "ordem" => 25, "valor" => ""]
            , "cnpj_cpf" => [ "tipo" => "T"  , "ordem" => 26, "valor" => ""]
            , "tipo_tributacao" => [ "tipo" => "T"  , "ordem" => 40, "valor" => ""]
            , "cod_ativ_federal" => [ "tipo" => "T"  , "ordem" => 46, "valor" => ""]
            , "cod_ativ_municipal" => [ "tipo" => "T"  , "ordem" => 48, "valor" => ""]
            , "vl_servicos" => [ "tipo" => "V_BR"  , "ordem" => 50, "valor" => ""]
            , "vl_cofins" => [ "tipo" => "V_BR"  , "ordem" => 54, "valor" => ""]
            , "vl_csll" => [ "tipo" => "V_BR"  , "ordem" => 55, "valor" => ""]
            , "vl_inss" => [ "tipo" => "V_BR"  , "ordem" => 56, "valor" => ""]
            , "vl_irpj" => [ "tipo" => "V_BR"  , "ordem" => 57, "valor" => ""]
            , "vl_pis" => [ "tipo" => "V_BR"  , "ordem" => 58, "valor" => ""]
            , "vl_outras" => [ "tipo" => "V_BR"  , "ordem" => 59, "valor" => ""]
            , "vl_iss" => [ "tipo" => "V_BR"  , "ordem" => 60, "valor" => ""]
            , "vl_credito" => [ "tipo" => "V_BR"  , "ordem" => 61, "valor" => ""]
            , "vl_iss_retido" => [ "tipo" => "V_BR"  , "ordem" => 62, "valor" => ""]
            , "dt_cancelamento" => [ "tipo" => "D" , "ordem" => 63, "formato_dt" => "d/m/Y", "valor" => ""]
            , "dt_competencia" => [ "tipo" => "T" , "ordem" => 64, "valor" => ""]
            , "numero_nf_substituta" => [ "tipo" => "T" , "ordem" => 70, "valor" => ""]
            , "numero_nf_substituida" => [ "tipo" => "T"  , "ordem" => 71, "valor" => ""]
            , 'nfis_descricao_servicos' => [ "tipo" => "T"  , "ordem" => 72, "valor" => ""]                        
        ];
        \log::info('Campos lidos', $campos_lidos);
        foreach($campos as $nome_campo => $specs){
            switch($specs['tipo']){
                case "T":
                    $campos[$nome_campo]['valor'] = trim($campos_lidos[$specs['ordem']]);
                    break;
                case "D":
                    $campos[$nome_campo]['valor'] = $this->formata_data($campos_lidos[$specs['ordem']], $specs['formato_dt']);
                    break;
                case "V_EN":
                    $campos[$nome_campo]['valor'] = str_replace(".", "", str_replace('"', "", $campos_lidos[$specs['ordem']]));
                    break;
                case "V_BR":
                    $campos[$nome_campo]['valor'] = str_replace(",", ".", str_replace(".", "", str_replace('"', "", $campos_lidos[$specs['ordem']])));
                    break;
            }
        }
        return $campos;
    }

    public function grava_no_banco($campos){
        $nota = \NotaFiscal::where('nfis_numero', '=', $campos['nfis_numero']['valor'])->first();
        if (count($nota) == 0){
            $nota = \NotaFiscal::where('id_nota_fiscal', '=', $campos['numero_rps']['valor'])->first();
        }
        if (count($nota) == 0){
            // Implementar carga da nota e associação posterior
        } else {
            foreach($campos as $nome_campo => $specs){
                if(array_key_exists("ignore", $specs)){
    
                } else {
                    $nota->$nome_campo = $specs['valor'];                
                }
            }
            $nota->save();
    
        }
    }

    public function tipo_reg($linha){

    }

    public function inicializa_objeto_detalhe($campos){

    }

    public function processa_arquivo($id){
        $this->instancia_objeto_arquivo($id);
        $arq = fopen($this->caminho_arquivo(), 'r');


        $ordem = 0;
        $tipo = '';
        
        // TODO: Adicionar os outros campos
        // TODO: Adicionar layout header

        while ($linha = fgets($arq)) {
            $ordem++;
            $tipo_reg = $this->tipo_reg($linha);
            $campos_lidos = explode($this->separador_campos(), $linha);
            if($campos_lidos[0] == '20'){
                $campos = $this->carrega_valores_lidos($campos_lidos);
                $this->grava_no_banco($campos);    
            }
                
        }
        fclose($arq);
    }
}
