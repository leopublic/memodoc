<?php

namespace Memodoc\Repositorios;

class RepositorioExpurgoEloquent extends Repositorio {

    public function expurgueIntervalo(\Cliente $cliente, $id_caixapadrao_ini, $id_caixapadrao_fim, $id_usuario, $data, $cortesia = 0){
        \DB::beginTransaction();
        $exppai = $this->criePai($cliente, $id_usuario, $data, $cortesia);
        for ($id_caixapadrao= $id_caixapadrao_ini; $id_caixapadrao <= $id_caixapadrao_fim; $id_caixapadrao++) {
            $this->crie($exppai, $cliente, $id_caixapadrao, $id_usuario, $data);
        }
        \DB::commit();
        return $exppai;
    }

    public function expurgueAvulsas(\Cliente $cliente, $id_caixaspadrao, $id_usuario, $data, $cortesia = 0, $com_transacao = true, $cortesia_movimentacao = 0, $id_os_chave = null, $fl_bloqueia_liberados = 0, $origem = "Expurgo"){
        if ($com_transacao){
            \DB::beginTransaction();
        }
        $exppai = $this->criePai($cliente, $id_usuario, $data, $cortesia, $cortesia_movimentacao, $id_os_chave, $fl_bloqueia_liberados);
        foreach($id_caixaspadrao as $id_caixapadrao){
            $this->crie($exppai, $cliente, $id_caixapadrao, $id_usuario, $data, $fl_bloqueia_liberados, $origem);
        }
        if ($com_transacao){
            \DB::commit();
        }
        return $exppai;
    }

    public function expurgueAvulsasComProcessamento(\Cliente $cliente, $id_caixaspadrao, $id_usuario, $data, $cortesia = 0, $com_transacao = true, $cortesia_movimentacao = 0, $id_os_chave = null, $fl_bloqueia_liberados = 0){
        \DB::beginTransaction();
        $agora = \Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s');
        // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Vai criar pai' );
        $exppai = $this->criePai($cliente, $id_usuario, $data, $cortesia, $cortesia_movimentacao, $id_os_chave, $fl_bloqueia_liberados);
        // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Criou. Vai entrar no loop de criação de expurgos' );

        foreach($id_caixaspadrao as $id_caixapadrao){
            // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Vai criar ' );
            $id_expurgo = \DB::table('expurgo')->insertGetId(array(
                     'id_expurgo_pai' => $exppai->id_expurgo_pai
                    ,'id_cliente' => $cliente->id_cliente
                    ,'id_usuario_cadastro' => $id_usuario
                    ,'data_solicitacao' => $data
                    ,'id_caixapadrao' => $id_caixapadrao
                    ,'created_at' =>  $agora
                    , 'fl_processado' => 0
                ));
            // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Criou ' );
        }
        // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Vai atualizar os expurgos ' );

        $sql = "update expurgo, documento
                  set expurgo.id_caixa = documento.id_caixa
                where documento.id_cliente = expurgo.id_cliente
                  and documento.id_caixapadrao = expurgo.id_caixapadrao
                  and expurgo.id_expurgo_pai = ".$exppai->id_expurgo_pai;
        \DB::update($sql);
        // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Atualizou. Vai criar os documentos cancelados' );

        $sql = "insert into documento_cancelado(id_documento, id_cliente, id_caixapadrao, id_box, id_item, id_departamento, id_setor, id_centro, id_tipodocumento, id_caixa, id_reserva, id_expurgo, titulo, migrada_de, inicio, fim, expurgo_programado, expurgarem, conteudo, usuario, emcasa, primeira_entrada, data_digitacao, expurgada_em, itemalterado, planilha_eletronica, request_recall, nume_inicial, nume_final, alfa_inicial, alfa_final, created_at_origin, updated_at_origin, id_usuario, evento, created_at)
            select documento.id_documento, documento.id_cliente, documento.id_caixapadrao, documento.id_box, documento.id_item, documento.id_departamento, documento.id_setor, documento.id_centro, documento.id_tipodocumento, documento.id_caixa, documento.id_reserva, expurgo.id_expurgo, documento.titulo, documento.migrada_de, documento.inicio, documento.fim, documento.expurgo_programado, documento.expurgarem, documento.conteudo, documento.usuario, documento.emcasa, documento.primeira_entrada, documento.data_digitacao, documento.expurgada_em, documento.itemalterado, documento.planilha_eletronica, documento.request_recall, documento.nume_inicial, documento.nume_final, documento.alfa_inicial, documento.alfa_final, documento.created_at, documento.updated_at, expurgo.id_usuario_cadastro, 'Expurgo', '".$agora."'
            from documento, expurgo
            where documento.id_caixa = expurgo.id_caixa
                and expurgo.id_expurgo_pai = ".$exppai->id_expurgo_pai;
        \DB::insert($sql);
        // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Criou. Vai deletar' );

        $sql = "delete from documento where id_caixa in (select id_caixa from expurgo where id_expurgo_pai = ".$exppai->id_expurgo_pai.")";
        \DB::delete($sql);
        // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Deletou' );

        \DB::commit();
        // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Comitou' );

        return $exppai;
    }


    public function criePai(\Cliente $cliente, $id_usuario, $data, $cortesia = 0, $cortesia_movimentacao = 0, $id_os_chave = null, $fl_bloqueia_liberados){
        $exp = new \ExpurgoPai;
        $exp->id_cliente = $cliente->id_cliente;
        $exp->id_usuario_cadastro = $id_usuario;
        $exp->data_solicitacao = $data;
        $exp->cortesia = $cortesia;
        $exp->cortesia_movimentacao = $cortesia_movimentacao;
        $exp->id_os_chave = $id_os_chave;
        $exp->fl_bloqueia_liberados = $fl_bloqueia_liberados;
        $exp->save();
        return $exp;
    }


    public function crie(\ExpurgoPai $exppai, \Cliente $cliente, $id_caixapadrao, $id_usuario, $data, $fl_bloqueia_liberados = 0, $origem = "Expurgo"){
        $caixa = \Documento::where('id_cliente', '=', $cliente->id_cliente)
                    ->where('id_caixapadrao', '=', $id_caixapadrao)
                    ->first();
        if (!is_object($caixa)){
            throw new \Exception('Nenhuma caixa encontrada com esse número ('.$id_caixapadrao.") para esse cliente");
        }
        // Cria o expurgo
        $id_expurgo = \DB::table('expurgo')->insertGetId(array(
                 'id_expurgo_pai' => $exppai->id_expurgo_pai
                ,'id_cliente' => $cliente->id_cliente
                ,'id_usuario_cadastro' => $id_usuario
                ,'data_solicitacao' => $data
                ,'id_caixapadrao' => $id_caixapadrao
                ,'id_caixa' => $caixa->id_caixa
        		,'fl_processado' => 1
        		,'codret_processamento' =>1  
                ,'created_at' =>  \Carbon::now('America/Sao_Paulo')
            ));

        $repDoc = new \Memodoc\Repositorios\RepositorioDocumentoEloquent;
        $repEnd = new \Memodoc\Repositorios\RepositorioEnderecoEloquent();
            // Cria o expurgo para poder usar o ID
            // Exclui os documentos
            $docs = \Documento::where('id_caixa', '=', $caixa->id_caixa)->get();

            foreach($docs as $doc){
                $sql = "insert into documento_cancelado(id_documento, id_cliente, id_caixapadrao, id_box, id_item, id_departamento, id_setor, id_centro, id_tipodocumento, id_caixa, id_reserva, id_expurgo, titulo, migrada_de, inicio, fim, expurgo_programado, expurgarem, conteudo, usuario, emcasa, primeira_entrada, data_digitacao, expurgada_em, itemalterado, planilha_eletronica, request_recall, nume_inicial, nume_final, alfa_inicial, alfa_final, created_at_origin, updated_at_origin, id_usuario, evento, created_at) 
                    select id_documento, id_cliente, id_caixapadrao, id_box, id_item, id_departamento, id_setor, id_centro, id_tipodocumento, id_caixa, id_reserva, ".$id_expurgo.", titulo, migrada_de, inicio, fim, expurgo_programado, expurgarem, conteudo, usuario, emcasa, primeira_entrada, data_digitacao, expurgada_em, itemalterado, planilha_eletronica, request_recall, nume_inicial, nume_final, alfa_inicial, alfa_final, created_at, updated_at, ".$id_usuario.", 'Expurgo', now() from documento where id_documento = ".$doc->id_documento;
                \DB::insert($sql);
                $sql = "delete from documento where id_documento = ".$doc->id_documento;
                \DB::delete($sql);
            }
            // Clona endereço para garantir que o ID não será reutilizado
            \DB::delete("delete from checkout where id_caixa = ".$caixa->id_caixa);
//            \Checkout::where('id_caixa', '=', $caixa->id_caixa)->delete();
            // Cancela os checkouts
//            \OsDesmembrada::where('id_caixa', '=', $caixa->id_caixa)->update(array('id_expurgo'=> $id_expurgo, 'id_caixapadrao' => $caixa->id_caixapadrao ));
            \DB::update("update osdesmembrada set id_expurgo = ".$id_expurgo.", id_caixapadrao = ".$caixa->id_caixapadrao." where id_caixa = ".$caixa->id_caixa);
            // Cancela os checkins
            \DB::update("update checkin set id_expurgo = ".$id_expurgo.", id_caixapadrao = ".$caixa->id_caixapadrao." where id_caixa = ".$caixa->id_caixa);
//            \Checkin::where('id_caixa', '=', $caixa->id_caixa)->update(array('id_expurgo'=> $id_expurgo, 'id_caixapadrao' => $caixa->id_caixapadrao  ));

            // Cancela as reservas
            \DB::update("update reservadesmembrada set id_expurgo = ".$id_expurgo." where id_caixa=".$caixa->id_caixa);
//            \ReservaDesmembrada::where('id_caixa', '=', $caixa->id_caixa)->update(array('id_expurgo'=> $id_expurgo ));
            //$end = \Endereco::where('id_caixa', '=', $caixa->id_caixa)->first();
            //$novo = $repEnd->canceleCriandoNovo($end, $id_usuario, 'Expurgo');
            //
            // Cria o novo endereço disponível no mesmo lugar e cancela o atual
            $disponivel = 1;
            if ($fl_bloqueia_liberados){
                $disponivel = 0;
            }

            $end = \DB::table('endereco')->where('id_caixa', '=', $caixa->id_caixa)->first();

            $id_caixa_nova = \DB::table('endereco')->insertGetId(array(
                'id_galpao' => $end->id_galpao
                , 'id_rua' => $end->id_rua
                , 'id_predio' => $end->id_predio
                , 'id_andar' => $end->id_andar
                , 'id_unidade' => $end->id_unidade
                , 'id_tipodocumento' => $end->id_tipodocumento
                , 'disponivel' => $disponivel
                , 'reservado' => 0
                , 'reserva' => 0
                , 'endcancelado' => 0
                , 'id_usuario_criacao' => $id_usuario
                , 'evento' => $origem
            	, 'id_caixa_original' => $end->id_caixa
                , 'created_at' => \Carbon::now('America/Sao_Paulo')
            ));

            \DB::update("update endereco set endcancelado = 1, dt_cancelamento = now(), id_usuario_cancelamento = ".$id_usuario.", observacao = 'Expurgo' where id_caixa = ".$caixa->id_caixa);

            // Atualiza o expurgo com o endereço novo criado.
            //$exp->id_caixa_nova = $novo->id_caixa;
            \DB::update("update expurgo set id_caixa_nova = ".$id_caixa_nova." where id_expurgo = ".$id_expurgo);
    }


    public function processa_pendentes(){
        $pendentes = Expurgo::where('fl_processado', '=', 0)->chunk(50, function($expurgos){
            foreach($expurgos as $expurgo){
                $this->processe_expurgo($expurgo);
            }
        });
    }

    public function processe_expurgo($expurgo){
        $agora = \Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s');
        try{
            \DB::beginTransaction();
                // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Vai excluir os checkouts' );
                \DB::delete("delete from checkout where id_caixa = ".$expurgo->id_caixa);
                // Cancela os checkouts
                // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Excluiu. Vai atualizar os empréstimos' );
                \DB::update("update osdesmembrada set id_expurgo = ".$expurgo->id_expurgo.", id_caixapadrao = ".$expurgo->id_caixapadrao.", updated_at = '".$agora."' where id_caixa = ".$expurgo->id_caixa);
                // Cancela os checkins
                // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Atualizou. Vai atualizar os checkins' );
                \DB::update("update checkin set id_expurgo = ".$expurgo->id_expurgo.", id_caixapadrao = ".$expurgo->id_caixapadrao.", updated_at = '".$agora."' where id_caixa = ".$expurgo->id_caixa);

                // Cancela as reservas
                // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Atualizou. Vai atualizar as reservas' );
                \DB::update("update reservadesmembrada set id_expurgo = ".$expurgo->id_expurgo.", updated_at = '".$agora."' where id_caixa=".$expurgo->id_caixa);

                $disponivel = 1;
                if ($expurgo->pai->fl_bloqueia_liberados){
                    $disponivel = 0;
                }

                // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Atualizou. Vai obter o endereço' );
                $end = \Endereco::where('id_caixa', '=', $expurgo->id_caixa)->first();
                // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Obteve. Vai criar o clone' );

                $id_caixa_nova = \DB::table('endereco')->insertGetId(array(
                    'id_galpao' => $end->id_galpao
                    , 'id_rua' => $end->id_rua
                    , 'id_predio' => $end->id_predio
                    , 'id_andar' => $end->id_andar
                    , 'id_unidade' => $end->id_unidade
                    , 'id_tipodocumento' => $end->id_tipodocumento
                    , 'disponivel' => $disponivel
                    , 'reservado' => 0
                    , 'reserva' => 0
                    , 'endcancelado' => 0
                    , 'id_usuario_criacao' => $expurgo->id_usuario_cadastro
                    , 'evento' => 'Expurgo'
                    , 'id_caixa_original' => $end->id_caixa
                    , 'created_at' => $agora
                ));
                // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Criou. Vai cancelar o atual' );

                $end->id_usuario_cancelamento = $expurgo->id_usuario_cadastro;
                $end->endcancelado = 1;
                $end->dt_cancelamento = $agora;
                $end->save();
                // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Atualizou. Vai atualizar o expurgo' );

                // Atualiza o expurgo com o endereço novo criado.
                $expurgo->id_caixa_nova = $id_caixa_nova;
                $expurgo->fl_processado = 1;
                $expurgo->codret_processamento = 1;
                $expurgo->save();
                // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Atualizou. Vai commitar' );

            \DB::commit();
            // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Commitou' );
        } catch(\Exception $e){
            \DB::rollback();
            // \log::info(\Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s u').' Deu erro..'.$e->getMessage() );
            $expurgo->codret_processamento = -1;
            $expurgo->fl_processado = -1;
            $expurgo->msg_processamento = 'Erro no processamento '.$e->getMessage()." Trace:".$e->getTraceAsString();
            $expurgo->save();
        }
    }


    public function lista_caixas_expurgadas($inativos = false, $id_cliente, $id_caixapadrao = '', $orderby = 'id_caixapadrao'){
        if($id_cliente != ''){
            $expurgos = \DB::table('caixas_expurgadas')->where('id_cliente', '=', $id_cliente);

            if ($id_caixapadrao != ''){
                $expurgos = $expurgos->where('id_caixapadrao', '=', $id_caixapadrao);
            }
            $expurgos = $expurgos->orderBy($orderby)->orderBy('id_caixapadrao')->paginate(20);
        } else {
            $expurgos = null;
        }
        if ($inativos){
            $titulo = "Consultar expurgos de clientes inativos";
            $clientes = FormSelect(\Cliente::getInativos(), 'razaosocial');
        } else {
            $titulo = "Consultar expurgos de clientes (somente clientes ativos)";
            $clientes = FormSelect(\Cliente::getAtivos(), 'razaosocial');
        }

        return \View::make('expurgo.listar_caixas')
                        ->with('expurgos', $expurgos)
                        ->with('id_cliente', $id_cliente)
                        ->with('id_caixapadrao', $id_caixapadrao)
                        ->with('clientes', $clientes)
                        ->with('orderby', $orderby)
                        ->with('titulo', $titulo)
            ;


    }
}