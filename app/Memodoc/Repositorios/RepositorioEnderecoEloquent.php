<?php

namespace Memodoc\Repositorios;

class RepositorioEnderecoEloquent extends Repositorio {
    /**
     * Cria um endereço com o mesmo local de outro e salva.
     * @param \Endereco $end
     * @param type $id_usuario
     * @param type $evento
     * @return \Endereco
     */
    public function crieClone(\Endereco $end, $id_usuario, $evento){
        $novo = new \Endereco;
        $novo->id_galpao        = $end->id_galpao;
        $novo->id_rua           = $end->id_rua;
        $novo->id_predio        = $end->id_predio;
        $novo->id_andar         = $end->id_andar;
        $novo->id_unidade       = $end->id_unidade;

        $novo->id_tipodocumento = $end->id_tipodocumento;
        $novo->disponivel       = 1;
        $novo->reservado        = 0;
        $novo->reserva          = 0;
        $novo->endcancelado     = 0;
        $novo->id_cliente       = null;
        $novo->id_usuario_criacao = $id_usuario;
        $novo->evento           = $evento;
        $novo->save();
        return $novo;
    }
    /**
     * Cria um endereço com o mesmo local de outro e salva.
     * @param \Endereco $end
     * @param type $id_usuario
     * @param type $evento
     * @return \Endereco
     */
    public function crieClone_sql($id_caixa, $id_usuario, $evento){
        $sql = "insert into endereco(id_galpao, id_rua, id_predio, id_andar, id_unidade, id_tipo_documento, disponivel, reservado, reserva, endcancelado, id_usuario_criacao, evento, created_at) 
                select id_galpao, id_rua, id_predio, id_andar, id_unidade, id_tipo_documento, disponivel, reservado, reserva, endcancelado, ".$id_usuario.", '".$evento."', now() from endereco where id_caixa = ".$id_caixa;
        $id = \DB::insert($sql);
        return $id;
    }

    public function canceleCriandoNovo(\Endereco $end, $id_usuario, $evento){
        $novo = $this->crieClone($end, $id_usuario, $evento);

        $this->cancele($end, $id_usuario, $evento);
        return $novo;
    }

    /**
     * Não permitir cancelar um endereço que tenha documentos
     * @param \Endereco $end
     * @param type $id_usuario
     * @param type $evento
     */
    public function cancele(\Endereco $end, $id_usuario, $evento){
        $end->endcancelado = 1;
        $end->dt_cancelamento = date('Y-m-d H:i:s');
        $end->id_usuario_cancelamento = $id_usuario;
        $end->observacao = $evento;
        $end->save();
        // Deleta os documentos do endereco
    }

    public function cancelaIntervaloPelaReferencia($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim, $id_usuario, $evento){
        \DB::beginTransaction();
            $caixas = $this->enderecosNumIntervalo($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim);
            foreach($caixas as $caixa){
                \Documento::where('id_caixa', '=', $caixa->id_caixa)->delete();
                $end = \Endereco::where('id_caixa', '=', $caixa->id_caixa)->first();
                $this->canceleCriandoNovo($end, $id_usuario, $evento);
            }
        \DB::commit();
    }

    public function enderecosDaReserva(\Reserva $reserva){
        $sql = "select distinct e.id_caixa, e.id_galpao, e.id_rua, e.id_predio, e.id_andar, e.id_unidade, e.endcancelado,date_format(e.dt_cancelamento, '%d/%m/%Y %H:%i:%s') dt_cancelamento"
                . " ,u.nomeusuario, td.descricao descricao_tipo"
                . " , (select count(id_documento) from documento where id_caixa = d.id_caixa and itemalterado = 1) qtdAlterada"
                . " , (select count(id_documento) from documento where id_caixa = d.id_caixa ) qtdDocs"
                . " , date_format(d.primeira_entrada, '%d/%m/%Y %H:%i:%s') primeira_entrada, d.id_caixapadrao"
                . " from documento d "
                . " join endereco e on d.id_caixa = e.id_caixa"
                . " left join usuario u on u.id_usuario = e.id_usuario_cancelamento"
                . " left join tipodocumento td on td.id_tipodocumento = e.id_tipodocumento"
                . " where d.id_reserva = :id_reserva";
        $res = \DB::select(\DB::raw($sql), array('id_reserva' => $reserva->id_reserva));
        return $res;
    }

    public function enderecosNumIntervalo($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim){
        $sql = "select distinct e.id_caixa, e.id_galpao, e.id_rua, e.id_predio, e.id_andar, e.id_unidade, e.endcancelado,date_format(e.dt_cancelamento, '%d/%m/%Y %H:%i:%s') dt_cancelamento, d.itemalterado"
                . " ,u.nomeusuario, td.descricao descricao_tipo"
                . " , (select count(id_documento) from documento where id_caixa = d.id_caixa and itemalterado = 1) qtdAlterada"
                . " , (select count(id_documento) from documento where id_caixa = d.id_caixa ) qtdDocs"
                . " , date_format(d.primeira_entrada, '%d/%m/%Y %H:%i:%s') primeira_entrada, d.id_caixapadrao"
                . " from documento d "
                . " join endereco e on d.id_caixa = e.id_caixa"
                . " left join usuario u on u.id_usuario = e.id_usuario_cancelamento"
                . " left join tipodocumento td on td.id_tipodocumento = e.id_tipodocumento"
                . " where d.id_cliente = :id_cliente"
                . "   and d.id_caixapadrao >= :id_caixapadrao_ini"
                . "   and d.id_caixapadrao <= :id_caixapadrao_fim";
        $res = \DB::select(\DB::raw($sql), array('id_cliente' => $id_cliente, 'id_caixapadrao_ini'=> $id_caixapadrao_ini, 'id_caixapadrao_fim'=> $id_caixapadrao_fim));
        return $res;
    }
}
