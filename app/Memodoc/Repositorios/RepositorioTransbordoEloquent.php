<?php

namespace Memodoc\Repositorios;

use Carbon\Carbon;

class RepositorioTransbordoEloquent extends Repositorio {

    public function novoTransbordo($post, $id_usuario) {
        $id_caixas = $post['id_caixas'];
        $docs = \Documento::where('id_cliente', '=', $post['id_cliente_antes'])
                ->whereIn('id_caixapadrao', $id_caixas)
                ->select(array('id_caixa', 'id_caixapadrao'))
                ->distinct()
                ->orderBy('id_caixapadrao')
                ->get();
        $tipos = \Documento::where('id_cliente', '=', $post['id_cliente_antes'])
                ->whereIn('id_caixapadrao', $id_caixas)
                ->select(array('id_tipodocumento'))
                ->distinct()
                ->get();
        if (intval(count($tipos)) > 1) {
            $this->registraErro('Somente documentos de um mesmo tipo podem ser migrados juntos');
            return false;
        }
        $doc = $tipos->first();
        $id_tipodocumento = $doc->id_tipodocumento;
        if (count($docs) > \Endereco::get_qtdDisponivel($id_tipodocumento)){
            $this->registraErro('Não há espaço disponível para reservar '.count($docs).' endereços. Não será possível fazer o transbordo.');
            return false;            
        }

        $transbordo = new \Transbordo;
        $transbordo->id_cliente_antes = $post['id_cliente_antes'];
        $transbordo->id_cliente_depois = $post['id_cliente_depois'];
        $transbordo->id_usuario_cadastro = $id_usuario;
        $transbordo->observacao = $post['observacao'];
        $transbordo->save();
        return $transbordo;
    }

    public function novoTransbordoOs($post, $id_usuario) {
        $id_caixas = $post['id_caixas'];
        $docs = \Documento::where('id_cliente', '=', $post['id_cliente_antes'])
                ->whereIn('id_caixapadrao', $id_caixas)
                ->select(array('id_caixa', 'id_caixapadrao'))
                ->distinct()
                ->orderBy('id_caixapadrao')
                ->get();
        $tipos = \Documento::where('id_cliente', '=', $post['id_cliente_antes'])
                ->whereIn('id_caixapadrao', $id_caixas)
                ->select(array('id_tipodocumento'))
                ->distinct()
                ->get();
        if (intval(count($tipos)) > 1) {
            $this->registraErro('Somente documentos de um mesmo tipo podem ser migrados juntos');
            return false;
        }
        $doc = $tipos->first();
        $id_tipodocumento = $doc->id_tipodocumento;
        if (count($docs) > \Endereco::get_qtdDisponivel($id_tipodocumento)){
            $this->registraErro('Não há espaço disponível para reservar '.count($docs).' endereços. Não será possível fazer o transbordo.');
            return false;
        }

        $os = \Os::find($post['id_os_chave']);

        $transbordo = new \Transbordo;
        $transbordo->id_cliente_antes = $post['id_cliente_antes'];
        $transbordo->id_cliente_depois = $os->id_cliente;
        $transbordo->id_usuario_cadastro = $id_usuario;
        $transbordo->id_os_chave = $post['id_os_chave'];
        $transbordo->observacao = $post['observacao'];
        $transbordo->cortesia_movimentacao = $post['cortesia_movimentacao'];
        $transbordo->save();
        return $transbordo;
    }

    public function adicionaCaixas(\Transbordo $transbordo, $id_caixas) {
        // Carrega
        $docs = \Documento::where('id_cliente', '=', $transbordo->id_cliente_antes)
                ->whereIn('id_caixapadrao', $id_caixas)
                ->select(array('id_caixa', 'id_caixapadrao'))
                ->distinct()
                ->orderBy('id_caixapadrao')
                ->get();
        $tipos = \Documento::where('id_cliente', '=', $transbordo->id_cliente_antes)
                ->whereIn('id_caixapadrao', $id_caixas)
                ->select(array('id_tipodocumento'))
                ->distinct()
                ->get();
        if (intval(count($tipos)) > 1) {
            $this->registraErro('Somente documentos de um mesmo tipo podem ser migrados juntos');
            return false;
        } else {
            foreach ($tipos as $tipo) {
                $transbordo->id_tipodocumento = $tipo->id_tipodocumento;
                $transbordo->save();
            }
        }
        if (intval(count($docs)) == 0) {
            $this->registraErro('Nenhuma caixa encontrada no intervalo');
            return false;
        }

        foreach ($docs as $doc) {
            $transbordo_caixa = new \TransbordoCaixa;
            $transbordo_caixa->id_transbordo = $transbordo->id_transbordo;
            $transbordo_caixa->id_caixa_antes = $doc->id_caixa;
            $transbordo_caixa->id_caixapadrao_antes = $doc->id_caixapadrao;
            $transbordo_caixa->save();
        }
        return true;
    }

    public function removeCaixa($id_transbordocaixa) {
        $transbordo_caixa = \TransbordoCaixa::find($id_transbordocaixa);
        $transbordo_caixa->delete();
    }

    public function processaTransbordo(\Transbordo $transbordo, $id_usuario) {
        if (!$transbordo->id_cliente_depois > 0) {
            $this->registraErro('Informe o cliente para onde as caixas serão transferidas');
            return false;
        }
        if ($transbordo->qtd_caixas == 0) {
            $this->registraErro('Informe pelo menos uma caixa para efetivar o transbordo');
            return false;
        }
        // 1 - Cria as novas caixas
        try {
            \DB::getPdo()->beginTransaction();
            if (\Endereco::get_qtdDisponivel($transbordo->id_tipodocumento) >= $transbordo->qtd_caixas) {
                $reserva = $this->criaReserva($transbordo, $id_usuario);
                if ($reserva) {
                    if ($this->copiaCaixas($transbordo, $id_usuario)) {
                        if ($this->expurgaOriginais($transbordo, $id_usuario)) {
                            \DB::getPdo()->commit();
                            return true;
                        }
                    } else {
                        \DB::getPdo()->rollBack();
                        return false;
                    }
                } else {
                    \DB::getPdo()->rollBack();
                    return false;
                }
            } else {
                $this->registraErro('Não existem endereços disponíveis suficientes');
                \DB::getPdo()->rollBack();
                return false;
            }
        } catch (Exception $ex) {
            $this->registraErro($ex->getMessage());
            \DB::getPdo()->rollBack();
            return false;
        }
    }

    public function criaReserva(\Transbordo $transbordo, $id_usuario) {
        $post = array(
            "id_cliente" => $transbordo->id_cliente_depois
            , "data_reserva" => date('d/m/Y')
            , "caixas" => $transbordo->qtd_caixas
            , "id_tipodocumento" => $transbordo->id_tipodocumento
            , "inventario_impresso" => 0
            , "observacao" => 'Transbordo'
        );
        $rep_reserva = new \Memodoc\Repositorios\RepositorioReservaEloquent;
        $reserva = $rep_reserva->criar($post, $id_usuario, false);
        if (!$reserva) {
            $this->errors = $rep_reserva->errors;
            return false;
        } else {
            $reserva->id_transbordo = $transbordo->id_transbordo;
            $reserva->observacao = "Reserva criada para atender a transbordo de caixas";
            $reserva->id_os_chave = $transbordo->id_os_chave;
            $reserva->save();

            $transbordo->id_reserva = $reserva->id_reserva;
            $transbordo->save();
            // Faz o de->para nas caixas do transbordo
            $i_caixa = $reserva->primeiraCaixa();
            $transbordo_caixas = \TransbordoCaixa::where('id_transbordo', '=', $transbordo->id_transbordo)
                    ->orderBy('id_caixapadrao_antes')
                    ->get();
            foreach ($transbordo_caixas as $transbordo_caixa) {
                $nova = \ReservaDesmembrada::where('id_reserva', '=', $reserva->id_reserva)->where('id_caixapadrao', '=', $i_caixa)->first();
                $transbordo_caixa->id_caixapadrao_depois = $nova->id_caixapadrao;
                $transbordo_caixa->id_caixa_depois = $nova->id_caixa;
                $transbordo_caixa->save();
                $i_caixa++;
            }
            return $reserva;
        }
    }

    // Copia o conteúdo das caixas antes para depois
    public function copiaCaixas(\Transbordo $transbordo, $id_usuario) {
        $trans_caixas = \TransbordoCaixa::where('id_transbordo', '=', $transbordo->id_transbordo)
                ->orderBy('id_caixapadrao_antes')
                ->get();
        $cliente_antes = \Cliente::find($transbordo->id_cliente_antes);
        foreach ($trans_caixas as $trans_caixa) {
            $docs_antes = \Documento::where('id_caixa', '=', $trans_caixa->id_caixa_antes)->orderBy('id_item')->get();
            $doc_depois = \Documento::where('id_caixa', '=', $trans_caixa->id_caixa_depois)->first();
            \Documento::where('id_caixa', '=', $trans_caixa->id_caixa_depois)->delete();
            foreach ($docs_antes as $doc_antes) {
                $doc = $doc_depois->replicate();
                $doc->id_item = $doc_antes->id_item;
                $doc->id_departamento = $this->migraDepartamento($transbordo, $doc_antes->id_departamento);
                $doc->id_setor = $this->migraSetor($transbordo, $doc_antes->id_setor);
                $doc->id_centro = $this->migraCentrodecustos($transbordo, $doc_antes->id_centro);
                $doc->id_tipodocumento = $doc_antes->id_tipodocumento;
                $doc->titulo = $doc_antes->titulo;
                $doc->inicio = $doc_antes->inicio;
                $doc->fim = $doc_antes->fim;
                $doc->expurgo_programado = $doc_antes->expurgo_programado;
                $doc->expurgarem = $doc_antes->expurgarem;
                $doc->conteudo = $doc_antes->conteudo . "\n" . "Atenção: Item transferido (transbordo) do item " . $doc_antes->id_item . " da caixa " . $doc_antes->id_caixapadrao . " do cliente (" . $transbordo->id_cliente_antes . ") " . $cliente_antes->razaosocial . " em " . date('d/m/Y H:i:s');
                $doc->migrada_de = 'ANTIGA ' . $doc_antes->id_caixapadrao;
                $doc->usuario = $doc_antes->usuario;
                $doc->emcasa = 0;
                $doc->primeira_entrada = date("Y-m-d H:i:s");
                $doc->data_digitacao = $doc_antes->data_digitacao;
                $doc->itemalterado = $doc_antes->itemalterado;
                $doc->planilha_eletronica = $doc_antes->planilha_eletronica;
                $doc->request_recall = $doc_antes->request_recall;
                $doc->nume_inicial = $doc_antes->nume_inicial;
                $doc->nume_final = $doc_antes->nume_final;
                $doc->alfa_inicial = $doc_antes->alfa_inicial;
                $doc->alfa_final = $doc_antes->alfa_final;
                $doc->usuario = $doc_antes->usuario;
                $doc->id_usuario_alteracao = $id_usuario;
                $doc->evento = "Transbordo";
                $doc->save();
            }
            $checkin = new \Checkin;
            $checkin->id_caixa = $trans_caixa->id_caixa_depois;
            $checkin->id_transbordo = $transbordo->id_transbordo;
            $checkin->id_cliente = $transbordo->id_cliente_depois;
            $checkin->fl_primeira_entrada = 0;
            $checkin->id_usuario = $id_usuario;
            $checkin->id_caixapadrao = $trans_caixa->id_caixapadrao_depois;
            $checkin->save();
        }
        return true;
    }

    public function expurgaOriginais(\Transbordo $transbordo, $id_usuario) {
        $rep_expurgo = new \Memodoc\Repositorios\RepositorioExpurgoEloquent;
        $cliente = \Cliente::find($transbordo->id_cliente_antes);
        $id_caixas = \TransbordoCaixa::where('id_transbordo', '=', $transbordo->id_transbordo)->lists('id_caixapadrao_antes');
        $exp = $rep_expurgo->expurgueAvulsas($cliente, $id_caixas, $id_usuario, date('d/m/Y'), 1, false, $transbordo->cortesia_movimentacao, null, 0, "Transbordo");
        if ($exp) {
            $exp->observacao = "Caixas migradas para o cliente " . $transbordo->id_cliente_depois.". Expurgo criado sem checkout pendente.";
            $exp->id_transbordo = $transbordo->id_transbordo;
            $exp->save();
            
            $exps = \Expurgo::where('id_expurgo_pai', '=', $exp->id_expurgo_pai)
                    ->update(
                          array('data_checkout' => date('Y-m-d H:i:s')
                        , 'id_usuario_checkout' => $id_usuario
                        , 'observacao' => 'Expurgo criado sem conferência por checkout (transbordo).')
                    );

            $transbordo->id_expurgo_pai = $exp->id_expurgo_pai;
            $transbordo->save();
            return true;
        } else {
            $this->errors = $rep_expurgo->errors;
            return false;
        }
    }

    public function migraDepartamento(\Transbordo $transbordo, $id_departamento) {
        if ($id_departamento != '') {
            $nome_antes = \DepCliente::find($id_departamento);
            $novo_dep = \DepCliente::where('id_cliente', '=', $transbordo->id_cliente_depois)
                    ->where('nome', '=', $nome_antes)
                    ->first()
            ;
            if (!$novo_dep) {
                $novo_dep = new \DepCliente;
                $novo_dep->id_cliente = $transbordo->id_cliente_depois;
                $novo_dep->nome = $nome_antes;
                $novo_dep->save();
            }
            return $novo_dep->id_departamento;
        } else {
            return null;
        }
    }

    public function migraCentrodecustos(\Transbordo $transbordo, $id) {
        if ($id != '') {
            $nome_antes = \CentroDeCusto::find($id);
            $novo = \CentroDeCusto::where('id_cliente', '=', $transbordo->id_cliente_depois)
                    ->where('nome', '=', $nome_antes)
                    ->first()
            ;
            if (!$novo) {
                $novo = new \CentroDeCusto;
                $novo->id_cliente = $transbordo->id_cliente_depois;
                $novo->nome = $nome_antes;
                $novo->save();
            }
            return $novo->id_centro;
        } else {
            return null;
        }
    }

    public function migraSetor(\Transbordo $transbordo, $id) {
        if ($id != '') {
            $nome_antes = \SetCliente::find($id);
            $novo = \SetCliente::where('id_cliente', '=', $transbordo->id_cliente_depois)
                    ->where('nome', '=', $nome_antes)
                    ->first()
            ;
            if (!$novo) {
                $novo = new \SetCliente;
                $novo->id_cliente = $transbordo->id_cliente_depois;
                $novo->nome = $nome_antes;
                $novo->save();
            }
            return $novo->id_setor;
        } else {
            return null;
        }
    }

    /**
     * Atualiza um array de referências de caixas a partir de um intervalo informado.
     *
     * Verifica se as caixas existem ou se já estão no array
     *
     * @param type $id_caixapadrao_ini      Referência da caixa inicial a ser adicionada
     * @param type $id_caixapadrao_fim      Referência da caixa final a ser adicionada
     * @param type $id_cliente              Cliente original das caixas
     */
    public function caixas_para_transbordo($id_caixasatual, $id_caixapadrao_ini, $id_caixapadrao_fim, $id_cliente) {
        $ret = array();
        $msg = array();
        $id_caixaspadrao = array();
        $id_caixaspadrao_emprestadas = array();
        if (intval($id_caixapadrao_ini) == 0) {
            $msg[] = 'Informe o número da caixa que você quer adicionar';
        } else {
            if ($id_caixapadrao_fim == '') {
                $id_caixapadrao_fim = $id_caixapadrao_ini;
            }
            for ($index = $id_caixapadrao_ini; $index <= $id_caixapadrao_fim; $index++) {
                if (in_array($index, $id_caixasatual)) {
                    $msg[] = 'Caixa número ' . $index . ' já foi adicionada anteriormente e foi ignorada.';
                } else {
                    $doc = \Documento::where('id_cliente', '=', $id_cliente)
                            ->where('id_caixapadrao', '=', $index)
                            ->first();
                    if (is_object($doc) && $doc->id_documento > 0) {
                        if($doc->emcasa == 1){
                            $id_caixaspadrao_emprestadas[] = $index;
                        } else {
                            $id_caixaspadrao[] = $index;                            
                        }
                    } else {
                        $msg[] = 'Caixa número ' . $index . ' não existe no cliente.';
                    }
                }
            }
        }
        if(count($msg) > 0){
            $ret['retcode'] = 500;
        } else {
            $msg[] = 'Caixas carregadas com sucesso';
        }
        $ret['msgs'] = $msg;
        $ret['id_caixaspadrao'] = $id_caixaspadrao;
        $ret['id_caixaspadrao_emprestadas'] = $id_caixaspadrao_emprestadas;
        return $ret;
    }

}