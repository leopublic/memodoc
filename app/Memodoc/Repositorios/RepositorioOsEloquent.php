<?php

namespace Memodoc\Repositorios;

class RepositorioOsEloquent extends Repositorio implements RepositorioOsInterface {
    const REDIS_QTDOSABERTAS = 'qtdeOsAbertas';
    /**
     * Exclui ou cancela uma ordem de servico
     * @param type $id_os_chave
     */
    public function excluirOs(\Os $os) {
        // Verifica se a OS pode ser excluida
        \OsDesmembrada::daOs($os)->delete();
        $this->registraEvento($os, 'Ordem de serviço excluída');
        $os->delete();
    }

    /**
     * getProximoNumero - Obtem o proximo numero da Os do cliente
     * @param type $id_cliente
     * @return type int
     */
    public function proximaOs($id_cliente) {
        return intval(\Os::where('id_cliente', '=', $id_cliente)->where('id_status_os', '>', 1)->max('id_os')) + 1;
    }

    public static function qtdeOsAbertas() {
        try{
            return self::quantas_abertas();
        }catch(Exception $e){
            \log::info($e->getMessage());
            return "??";
        }

    }

    public static function quantas_abertas(){
        $sql = "select count(*) from os where id_status_os = 2 and id_cliente in (select id_cliente from cliente where fl_ativo = 1)";
        try {
            $pdo = \DB::getPdo();
            $res = $pdo->query($sql);
            $rs = $res->fetch(\PDO::FETCH_NUM);
            return $rs[0];
        } catch (Exception $ex) {

        }

    }

    public function nova() {
        $os = new \Os;
        $os->id_status_os = 1;
        $os->solicitado_em = date('d/m/Y H:i:s');
        $os->responsavel = \Auth::user()->nomeusuario;
        $os->id_usuario_cadastro = \Auth::user()->id_usuario;
        $os->cancelado = 0;
        $os->tipodeemprestimo = 0;
        $os->cobrafrete = 1;
        $os->naoecartonagemcortesia = 1;
        $os->valorfrete = 0;
        $os->valorcartonagem = 0;
        $os->valormovimento = 0;
        $os->entrega_pela = 0;
        $os->id_usuario_alteracao = \Auth::user()->id_usuario;
        return $os;
    }

    public function novaAtendimento($id_cliente, $id_usuario_cadastro) {
        $os = \Os::where('id_usuario_cadastro', '=', $id_usuario_cadastro)
                    ->where('id_cliente', '=', $id_cliente)
                    ->where('id_status_os', '=', \StatusOs::stINCOMPLETA)
                    ->first();
        if (is_object($os)){
            return $os;
        } else {
            \DB::beginTransaction();
                $end = \Endeentrega::where('id_cliente', '=', $id_cliente)->first();
                $os = $this->nova();
                $os->id_endeentrega = $end->id_endeentrega;
                $os->solicitado_em = date('d/m/Y H:i:s');
                $os->id_cliente = $id_cliente;
                $os->turnoDefault();
                $os->id_usuario_cadastro = $id_usuario_cadastro;
                $os->responsavel = '';
                $os->id_status_os = \StatusOs::stINCOMPLETA;
                $os->id_usuario_alteracao = $id_usuario_cadastro;
                $os->evento = 'Nova ordem de serviço criada';
                $os->save();
            \DB::commit();
        }
        return $os;
    }

    public function osPendente($id_cliente, $id_usuario) {
        $os = \Os::where('id_cliente', '=', $id_cliente)
                ->where('id_status_os', '=', 1)
                ->where('id_usuario_cadastro', '=', $id_usuario)
                ->first()
        ;
        if (!is_object($os)) {
            $os = $this->nova();
            $os->id_cliente = $id_cliente;
            $os->id_usuario_cadastro = $id_usuario;
            $os->responsavel = \Auth::user()->nomeusuario;
            $os->id_usuario_alteracao = \Auth::user()->id_usuario;
            $os->evento = 'Nova ordem de serviço em aberto criada pelo sistema para o cliente';
            $os->save();
        }
        return $os;
    }

    /**
     * Adiciona caixa sem verificar o status
     * @param \Os $os
     * @param type $id_caixapadrao
     */
    public function adicionaCaixa(\Os $os, \Documento $doc) {
        return $this->adicionaCaixaComControle($os, $doc, false);
    }

    /**
     * Adiciona caixa para emprestimo dando opção de verificar se a caixa está em casa.
     * @param \Os $os
     * @param type $id_caixapadrao
     */
    public function adicionaCaixaComControle(\Os $os, \Documento $doc, $verificaStatus = false) {
        if ($verificaStatus){
            if ($doc->emcasa == 0){
                throw new \Exception("Não é possível solicitar a referência ". substr('000000'.$doc->id_caixapadrao, -6) ." pois a mesma se encontra emprestada");
            }
        }

        $pendentes = \DB::table('os')
        		->join('osdesmembrada', 'osdesmembrada.id_os_chave', '=', 'os.id_os_chave')
        		->where('osdesmembrada.id_caixa', '=', $doc->id_caixa)
        		->whereIn('os.id_status_os', ['2', '3'])
        		->select(['os.id_os', 'os.id_os_chave'])
        		->get();

        		
//        $pendente = \OsDesmembrada::where('id_caixa', '=', $doc->id_caixa)
//                   ->whereRaw('id_os_chave in (select id_os_chave from os where id_cliente = '.$os->id_cliente." and id_status_os in (2,3)  ) ")
//                   ->count();

//        if ($pendente > 0){
//            throw new \Exception("Não é possível solicitar a referência ". substr('000000'.$doc->id_caixapadrao, -6) ." pois a mesma já está solicitada em uma ordem de serviço em atendimento.");
//        }

        if (count($pendentes) > 0){
        	$oss = '';
        	$virg = '';
        	foreach($pendentes as $pendente){
        		$oss .= $virg.'<a href="/osok/visualizar/'.$pendente->id_os_chave.'" target="_blank" class="btn btn-small">'.substr('000000'.$pendente->id_os, -6).'</a>';
        		$virg = ',';
        	}
        	
        	throw new \Exception("Não é possível solicitar a referência ". substr('000000'.$doc->id_caixapadrao, -6) ." pois a mesma já está solicitada nas seguinte(s) ordem(ns) de serviço não finalizada(s): ".$oss.".");
        }

        $os_caixa = \OsDesmembrada::where('id_os_chave', '=', $os->id_os_chave)
                ->where('id_caixa', '=', $doc->id_caixa)
                ->first();

        if (!is_object($os_caixa)) {
            $os_caixa = new \OsDesmembrada;
            $os_caixa->id_os_chave = $os->id_os_chave;
            $os_caixa->id_caixa = $doc->id_caixa;
            $os_caixa->status = \OsDesmembrada::stPENDENTE;
            $os_caixa->save();
            $this->registraEvento($os, 'Incluído o empréstimo da caixa ' . substr('000000' . $doc->id_caixapadrao, -6),'Incluído o empréstimo da caixa ' . substr('000000' . $doc->id_caixapadrao, -6), $doc->id_caixa);
        }
        $this->atualizaValorFrete($os);
        $os->save();
        return $os_caixa;
    }

    public function registraEvento(\Os $os, $descricao, $evento = '', $id_caixa = null, $id_usuario = null) {
        if ($id_usuario == ''){
            $id_usuario = \Auth::user()->id_usuario;
        }
        $log = new \LogOs;
        $log->id_usuario = $id_usuario;
        $log->id_os_chave = $os->id_os_chave;
        $log->id_status_os = $os->id_status_os;
        $log->descricao = $descricao;
        $log->evento = $evento;
        $log->id_caixa = $id_caixa;
        $log->save();
    }

    public function osIncompleta($id_usuario) {
        $os = \Os::where('id_status_os', '=', 1)
                ->where('id_usuario_cadastro', '=', $id_usuario)
                ->first();
        if (is_object($os)) {
            return $os;
        } else {
            return false;
        }
    }

    public function atribuiDoInput(\Os $os, $post) {
        $os->fill($post);
        if (!isset($post['cobrafrete'])) {
            $os->cobrafrete = null;
        }
        if (!isset($post['naoecartonagemcortesia'])) {
            $os->naoecartonagemcortesia = null;
        }
        if (!isset($post['urgente'])) {
            $os->urgente = null;
        }
        if ($os->id_departamento == '') {
            $os->id_departamento = null;
        }
        $os = $this->atualizaValorFrete($os);
        $os = $this->atualizaValorFretePrevisto($os);
        return $os;
    }

    public function salvaDoCliente($post, $encerrar = true) {
        $os = \Os::find($post['id_os_chave']);
        $os->fill($post);
        $os->responsavel = ucwords(strtolower($os->responsavel));
        if ($encerrar) {
            $os->id_status_os = 2;
            $os->solicitado_em = \Carbon::now('America/Sao_Paulo')->format('d/m/Y H:i:s');
            $os->turnoDefault();
            $os->id_os = \Os::getProximoNumero($os->id_cliente);
            $this->notificar_abertura($os);
        }
        if ($os->tipodeemprestimo == 0) {
            $os->cobrafrete = 1;
        } else {
            $os->cobrafrete = 0;
        }
        $os = $this->atualizaValorFretePrevisto($os);
        $os = $this->atualizaValorCartonagem($os);
        $os->id_usuario_alteracao = \Auth::user()->id_usuario;
        $os->evento = 'Atualizada pelo cliente';
        $os->save();
        $data = array('os' => $os);
        if ($encerrar) {
            \Mail::queue('emails.nova_os', $data, function($message) use($os) {
                $message->to(\Config::get('mail.atendimento'))
                        ->to($os->cadastradaPor->username, $os->cadastradaPor->nomeusuario)
                        ->subject('Solicitação E-Doc');
                $message->replyTo(\Config::get('mail.atendimento'));
            });
        }
        return $os;
    }

    public function salvaDoOperacional($post) {
        $repPerFat = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $os = \Os::find($post['id_os_chave']);
        $id_status_os_atual = $os->id_status_os;
        if (!isset($post['id_departamento']) || intval($post['id_departamento']) == 0) {
            unset($post['id_departamento']);
        }

        // Verifica se está salvando dentro de um faturamento fechado
        $entregar_em = $post['entregar_em'];
        if ($entregar_em != ''){
            $entregar_em = \Carbon::createFromFormat('d/m/Y', $entregar_em)->format('Y-m-d');
            $fechados = $repPerFat->faturamentosFechadosNaData($entregar_em);
            if (count($fechados) > 0){
                $perfat = $fechados[0];
                throw new \Exception ('Na data de entrega informada, essa OS vai afetar o faturamento "de '.$perfat->formatDate('dt_inicio').' a '.$perfat->formatDate('dt_fim').'". Altere a data de entrega ou abra o faturamento para poder alterá-lo.');
            }
        }

        $os->fill($post);
        $os->responsavel = ucwords(strtolower($os->responsavel));
        if (!isset($post['urgente'])) {
            $os->urgente = 0;
        } else {
            $os->urgente = 1;
        }
        if (!isset($post['naoecartonagemcortesia'])) {
            $os->naoecartonagemcortesia = 0;
        } else {
            $os->naoecartonagemcortesia = 1;
        }

        if ($post['tipodeemprestimo'] == 0){
            if (!isset($post['cobrafrete'])) {
                $os->cobrafrete = 0;
            } else {
                $os->cobrafrete = 1;
            }
        } else {
            $os->cobrafrete = 0;
        }
        if (!isset($post['val_frete_manual_fmt'])){
            $os->val_frete_manual = null;
        }
        $os = $this->atualizaValorFrete($os);
        $os = $this->atualizaValorFretePrevisto($os);
        $os = $this->atualizaValorCartonagem($os);
        $os->atualizarTotaisRecebidos();
        $os->id_usuario_alteracao = \Auth::user()->id_usuario;
        $os->evento = 'Atualizada pelo operador (v)';
        \DB::beginTransaction();
        if ($id_status_os_atual == \StatusOs::stINCOMPLETA){
            $os->id_status_os = \StatusOs::stNOVA;
            $os->id_os = $os->getProximoNumero($os->id_cliente);
            $this->notificar_abertura($os);
            $os->solicitado_em = \Carbon::now('America/Sao_Paulo')->format('d/m/Y H:i:s');
        }
        $os->save();
        \DB::commit();
        return $os;
    }

    public function concluir(\Os $os) {
        $os->id_status_os = 4;
        $os->id_usuario_alteracao = \Auth::user()->id_usuario;
        $os->evento = 'Concluída pelo operador';
        $os = $this->atualizaValorFrete($os);
        $os = $this->atualizaValorFretePrevisto($os);
        $os = $this->atualizaValorCartonagem($os);
        $os->atualizarTotaisRecebidos();
        $os->save();
    }

    public function atualizaValorFrete(\Os $os) {
        if (intval($os->fl_frete_manual) == 1){
            $os->valorfrete = $os->val_frete_manual;
        } else {
            $preco = new \Memodoc\Servicos\Precos($os->cliente);
            $preco->CarregaPrecos();
            $calculadora = new \Memodoc\Servicos\CalculadoraOs($preco, $os);
            $os->valorfrete = $calculadora->valorFrete();
        }

        return $os;
    }

    public function atualizaValorFretePrevisto(\Os $os) {
        if (intval($os->fl_frete_manual) == 1){
            $os->valorfrete = $os->val_frete_manual;
        } else {
            $preco = new \Memodoc\Servicos\Precos($os->cliente);
            $preco->CarregaPrecos();
            $calculadora = new \Memodoc\Servicos\CalculadoraOs($preco, $os);
            $os->valorfreteprevisto = $calculadora->valorFretePrevisto();
        }
        return $os;
    }

    public function atualizaValorCartonagem(\Os $os) {
        if ($os->naoecartonagemcortesia){
            $preco = new \Memodoc\Servicos\Precos($os->cliente);
            $preco->CarregaPrecos();
            $calculadora = new \Memodoc\Servicos\CalculadoraOs($preco, $os);
            $os->valorcartonagem = $calculadora->valorCartonagem();
        } else {
            $os->valorcartonagem = 0;
        }
        return $os;
    }


    public function cancele(\Os $os, \Usuario $usuario) {
        $qtdReserva = \Reserva::where('id_os_chave', '=', $os->id_os_chave)->count();
        $qtdCheckout = \OsDesmembrada::where('id_os_chave', '=', $os->id_os_chave)->where('status', '=', 1)->count();
        $qtdCheckin = \Checkin::where('id_os_chave', '=', $os->id_os_chave)->whereNotNull('dt_checkin')->count();
        if ($qtdReserva > 0 || $qtdCheckout > 0 || $qtdCheckin > 0){
            throw new Exception ("Não foi possível cancelar essa ordem de serviço pois ela já foi parcialmente ou totalmente atendida.");
        }

        $os->cancelado = '1';
        $os->id_status_os = \StatusOs::OsCancelada();
        $os->id_usuario_alteracao = $usuario->id_usuario;
        $os->evento = 'Cancelada pelo cliente';
        $os->save();
    }

    public function canceleOperador(\Os $os) {
        \DB::beginTransaction();
            // Cancela reservas
            // ---Reservas realizadas não podem ser canceladas automaticamente.
            // Cancela Checkins
            \Checkin::where('id_os_chave', '=', $os->id_os_chave)
                    ->whereNull('dt_checkin')
                    ->delete();
            // Cancela Checkouts
            \OsDesmembrada::where('id_os_chave', '=', $os->id_os_chave)
                    ->whereNull('data_checkout')
                    ->delete();
            // Cancela OS
            $os->cancelado = '1';
            $os->id_status_os = \StatusOs::OsCancelada();
            $os->id_usuario_alteracao = \Auth::user()->id_usuario;
            $os->evento = 'Cancelada pelo operador';
            $os->save();
        \DB::commit();
    }

    public function retiraCheckin(\Os $os, $id_caixa) {
        $checkin = \Checkin::where('id_os_chave', '=', $os->id_os_chave)
                    ->where('id_caixa', '=', $id_caixa)
                    ->first();
        $checkin->delete();
        $caixa = \Documento::where('id_caixa', '=', $id_caixa)->first();
        $this->registraEvento($os, 'Excluído o recebimento da caixa ' . substr('000000' . $caixa->id_caixapadrao, -6), 'Excluído o recebimento da caixa ' . substr('000000' . $caixa->id_caixapadrao, -6), $id_caixa);
        $os->evento = "Total atualizado por retirada de checkin";
        $os->atualizarTotaisRecebidos();
        $os->id_usuario_alteracao = \Auth::user()->id_usuario;
        $os->save();
    }

    /**
     * Retira o emprestimo da OS
     * Somente emprestimos que ainda não foi feito o checkout podem ser retirados da OS.
     * @param type $id_osdesmembrada
     * @param type $id_usuario
     * @param type $evento
     */
    public function retiraCaixaEmprestimo($id_osdesmembrada, $id_usuario, $evento){
        $osdesmembrada = \OsDesmembrada::find($id_osdesmembrada);
        $doc = \Documento::where('id_caixa', '=', $osdesmembrada->id_caixa)->first();
        $os = $osdesmembrada->os;
        $checkout = \Checkout::where('id_osdesmembrada', '=', $osdesmembrada->id_osdesmembrada)->first();
        if ($osdesmembrada->id_checkout != '' || is_object($checkout)){
            throw new \Exception('Esse empréstimo não pode ser removido porque já foi feito o checkout');
        } else {
            $osdesmembrada->delete();
        }
        $msg = 'Excluído o empréstimo da caixa ' . substr('000000' . $doc->id_caixapadrao, -6);
        $this->registraEvento($os, $msg , $evento, null, $id_usuario);
        $os->id_usuario_alteracao = \Auth::user()->id_usuario;
        $os->save();
    }

    public function retiraDocumentoEmprestimo(\Os $os, $id_documento, $id_usuario, $evento){
        $caixa = \Documento::where('id_documento', '=', $id_documento)->first();
        $osd = \OsDesmembrada::where('id_os_chave', '=', $os->id_os_chave)
                    ->where('id_documento', '=', $caixa->id_documento)
                    ->get();
        foreach($osd as $osdesmembrada){
            $osdesmembrada->delete();
        }

        $msg = 'Excluído o recebimento do item ' . substr('000' . $caixa->id_item). ' da caixa ' . substr('000000' . $caixa->id_caixapadrao). ' Devido ao evento '.$evento;
        $this->registraEvento($os, $msg, $msg, null, $id_usuario);
    }

    public function notificar_abertura($os){
        $cliente = $os->cliente;
        $emails = $cliente->emails_notificacao_os;

        if ($cliente->tem_notificacao_os){

            $emails = $cliente->emails_notificacao_os;
            $data = array(
                'os' => $os
            );
            \Mail::send('emails.notificacao_nova_os', $data, function($message) use($emails, $os) {
                $message->to($emails)->subject('Memodoc - Nova OS aberta '.substr('000000'.$os->id_os, -6));
            });
        }
    }
}
