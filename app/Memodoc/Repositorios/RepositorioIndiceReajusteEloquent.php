<?php

namespace Memodoc\Repositorios;

class RepositorioIndiceReajusteEloquent extends Repositorio {
	/**
	 * Preenche o mes ano informados com valores zerados, caso não existam
	 * @param unknown $mes
	 * @param unknown $ano
	 */
    public function preencheMesAno($mes, $ano){
        $valores = \Indice::leftJoin('indicereajuste', function ($join) use ($mes, $ano){
                        $join->on('indicereajuste.id_indice', '=', 'indice.id_indice');
                        $join->on('mes', '=', \DB::raw($mes));
                        $join->on('ano', '=', \DB::raw($ano));
                    })
                ->select(array('indice.id_indice', 'indicereajuste.id_indicereajuste'))
                ->get()
                ;
        foreach($valores as $indice){
            if ($indice->id_indicereajuste == ''){
                $reajuste = new \IndiceReajuste();
                $reajuste->id_indice = $indice->id_indice;
                $reajuste->mes = $mes;
                $reajuste->ano = $ano;
                $reajuste->percentual = 0;
                $reajuste->save();
            }
        }
    }
    
    public function processaAtualizacao($post){
        foreach($post['id_indicereajuste'] as $i => $id_indicereajuste){
            $indice = \IndiceReajuste::find($id_indicereajuste);
            $indice->percentual = trim(str_replace(',','.',str_replace('.', '',$post['percentual'][$i])));
            $indice->save();
        }
    }
}
