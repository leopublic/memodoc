<?php
namespace Memodoc\Repositorios;
/**
 * Representa um registro de retorno do tipo "titulo" (01)
 */
class RegistroRetorno
{
    public $linha;
    public $numero;
    public $nosso_numero;
    public $nome_cliente;
    public $tipo ;
    public $fl_duplicidade_cnpj;

    public function __construct($registro, $linha) {
        $this->tipo = substr($registro, 0,2);
        $this->linha = $linha;
    }
    /**
     * Converte uma data em formato texto ddmmaaaa em aaaa-mm-dd
     * @param [type] $valor    [description]
     * @param [type] $atributo [description]
     */
    public function setDataTexto($valor, $com_seculo = false){
        if (trim($valor) != ''){
            if ($com_seculo){
                $valor = substr($valor, 4,4).'-'.substr($valor, 2,2).'-'.substr($valor, 0,2);
            } else {
                $valor = '20'.substr($valor, 4,2).'-'.substr($valor, 2,2).'-'.substr($valor, 0,2);
            }
            return  $valor;
        } else {
            return null;
        }
    }
    /**
     * Recupera o pagamento associado a esse registro, se houver'
     * @return [type] [description]
     */
    public function recupera_pagamento($id_arquivo_retorno){
        $pag = \Pagamento::where('numero', '=', $this->numero)
                ->where('id_arquivo_retorno', '=', $id_arquivo_retorno)
                ->first();
        if (count($pag) > 0){
            return $pag;
        } else {
            return false;
        }
    }
}
