<?php
namespace Memodoc\Repositorios;

class Repositorio{
    public $validacao;
    public $errors;

    public function registraErro($msg){
        $this->errors[] = $msg ;
    }

    public function getErrors(){
        if (isset($this->errors)){
            return $this->errors;
        } else {
            if (isset ($this->validacao)){
                return $this->validacao->getErrors();
            } else {
                return null;
            }
        }
    }
}