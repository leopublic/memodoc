<?php
/**
 * O objetivo dessa classe é concentar os métodos relacionados 'as caixas
 * evitando sobrecarregar o modelo Documento
 */
namespace Memodoc\Repositorios;

class RepositorioCaixaEloquent extends Repositorio {
    /**
     * Só verifica se a caixa existe e em caso positivo retorna o primeiro documeto.
     * Caso contrário retorna uma exceção
     * Esse metodo só garante que a caixa existe
     */
    public function primeiroDocumento($id_caixa) {
        $caixa = Documento::where('id_caixa', '=', $id_caixa)
                        ->first();
        if(isset($caixa)){
            return $caixa;
        } else {
            throw new \Illuminate\Database\Eloquent\ModelNotFoundException();
        }
    }

    /**
     * Recupera o número da caixa pelo cliente e número
     * @param type $id_cliente
     * @param type $id_caixapadrao
     */
    public function primeiroDocumentoPeloNumero($id_cliente, $id_caixapadrao) {
        $caixa = Documento::where('id_cliente', '=', $id_cliente)
                        ->where('id_caixapadrao', '=', $id_caixapadrao)
                        ->first();
        if(isset($caixa)){
            return $caixa;
        } else {
            throw new \Illuminate\Database\Eloquent\ModelNotFoundException();
        }
    }

    /**
     * Retorna os documentos de uma determinada caixa
     * @param type $id_caixa
     */
    public function documentos($id_caixa) {

    }

    public function historico($caixa){
        $sql = "select 'in' tipo, os.id_os_chave, 0 id_expurgo_pai, os.id_os, os.responsavel, os.solicitado_em, date_format(os.solicitado_em, '%d/%m/%Y') solicitado_em_fmt, date_format(os.entregar_em, '%d/%m/%Y') entregar_em_fmt, os.tipodeemprestimo
                from os, checkin
                where checkin.id_os_chave = os.id_os_chave
                and checkin.id_caixa = ?
        		and os.id_status_os > 1
                union all
                select 'out' tipo,os.id_os_chave , 0 id_expurgo_pai
                ,  os.id_os, os.responsavel,os.solicitado_em,  date_format(os.solicitado_em, '%d/%m/%Y') solicitado_em_fmt, date_format(os.entregar_em, '%d/%m/%Y') entregar_em_fmt, os.tipodeemprestimo
                from os, osdesmembrada
                where osdesmembrada.id_os_chave = os.id_os_chave
        		and os.id_status_os > 1
        		and osdesmembrada.id_caixa = ?
        		union all
                select 'exp' tipo
                    , expurgo_pai.id_os_chave
                    , expurgo_pai.id_expurgo_pai
                	, coalesce(os.id_os, '--'), coalesce(os.responsavel, '--')
                	, coalesce(os.solicitado_em, expurgo_pai.data_solicitacao)
                	, coalesce(date_format(os.solicitado_em, '%d/%m/%Y'), date_format(expurgo_pai.data_solicitacao, '%d/%m/%Y')) solicitado_em_fmt
                	, coalesce(date_format(os.entregar_em, '%d/%m/%Y'), date_format(expurgo_pai.data_solicitacao, '%d/%m/%Y')) entregar_em_fmt
                	, os.tipodeemprestimo
                from expurgo
                join expurgo_pai on expurgo_pai.id_expurgo_pai = expurgo.id_expurgo_pai
                left join os on os.id_os_chave = expurgo_pai.id_os_chave
                where expurgo.id_caixa = ?
                order by solicitado_em desc";
        $ret = \DB::select(\DB::raw($sql), array($caixa->id_caixa, $caixa->id_caixa, $caixa->id_caixa));
        return $ret;
    }

    public function dados_para_rateio($id_cliente){
        $cliente = \Cliente::find($id_cliente);
        $total_caixas = $cliente->qtdCaixas();

        $sql = "select departamentos, 0 valor, 0 valorcomiss,  count(*) qtd from caixa_departamento where id_cliente = ".$id_cliente." group by departamentos";
        $departamentos = \DB::select(\DB::raw($sql));
        $sql = "select count(distinct id_caixapadrao) qtd from documento where id_cliente = ".$id_cliente." and id_departamento is null";
        $sem_departamento = \DB::select(\DB::raw($sql));


        $sql = "select centros, 0 valor, 0 valorcomiss,  count(*) qtd from caixa_centro where id_cliente = ".$id_cliente." group by centros";
        $centros = \DB::select(\DB::raw($sql));
        $sql = "select count(distinct id_caixapadrao) qtd from documento where id_cliente = ".$id_cliente." and id_centro is null";
        $sem_centro = \DB::select(\DB::raw($sql));

        $sql = "select setores, 0 valor, 0 valorcomiss, count(*) qtd from caixa_setor where id_cliente = ".$id_cliente." group by setores";
        $setores = \DB::select(\DB::raw($sql));
        $sql = "select count(distinct id_caixapadrao) qtd from documento where id_cliente = ".$id_cliente." and id_setor is null";
        $sem_setor = \DB::select(\DB::raw($sql));

        $data = array(
                    'cliente' => $cliente,
                    'departamentos' => $departamentos,
                    'centros' => $centros,
                    'setores' => $setores,
                    'sem_departamento' => $sem_departamento,
                    'sem_centro' => $sem_centro,
                    'sem_setor' => $sem_setor,
                    'total_caixas' => $total_caixas
                );
        return $data;
    }

    public function dados_para_rateio_faturamento($id_faturamento){
        $fat = \Faturamento::find($id_faturamento);
        $perfat = $fat->periodofaturamento;
        $id_cliente = $fat->id_cliente;
        $cliente = \Cliente::find($id_cliente);
        $total_caixas = $cliente->qtdCaixas();

        $sql = "select departamentos, 0 valor, 0 valorcomiss,  count(*) qtd
                from caixa_departamento
                where id_cliente = ".$id_cliente."
                   and ( (datediff('" . $perfat->dt_fim_query . "', data_reserva) > ".intval($cliente->franquiacustodia)."    and primeira_entrada is null ) or primeira_entrada < '" . $perfat->dt_fim_query . "')
                group by departamentos";
        $departamentos = \DB::select(\DB::raw($sql));

        $sql = "select count(distinct id_caixapadrao) qtd
                  from documento join reserva on reserva.id_reserva = documento.id_reserva
                 where documento.id_cliente = ".$id_cliente."
                   and ( (datediff('" . $perfat->dt_fim_query . "', data_reserva) > ".intval($cliente->franquiacustodia)."    and primeira_entrada is null ) or primeira_entrada < '" . $perfat->dt_fim_query . "')
                   and id_departamento is null";
        $sem_departamento = \DB::select(\DB::raw($sql));


        $sql = "select centros, 0 valor, 0 valorcomiss,  count(*) qtd
                from caixa_centro
                where id_cliente = ".$id_cliente."
                   and ( (datediff('" . $perfat->dt_fim_query . "', data_reserva) > ".intval($cliente->franquiacustodia)."    and primeira_entrada is null ) or primeira_entrada < '" . $perfat->dt_fim_query . "')
                group by centros";
        $centros = \DB::select(\DB::raw($sql));

        $sql = "select count(distinct id_caixapadrao) qtd
                  from documento join reserva on reserva.id_reserva = documento.id_reserva
                 where documento.id_cliente = ".$id_cliente."
                   and ( (datediff('" . $perfat->dt_fim_query . "', data_reserva) > ".intval($cliente->franquiacustodia)."    and primeira_entrada is null ) or primeira_entrada < '" . $perfat->dt_fim_query . "')
                   and id_centro is null";
        $sem_centro = \DB::select(\DB::raw($sql));

        $sql = "select setores, 0 valor, 0 valorcomiss, count(*) qtd
                from caixa_setor
                where id_cliente = ".$id_cliente."
                   and ( (datediff('" . $perfat->dt_fim_query . "', data_reserva) > ".intval($cliente->franquiacustodia)."    and primeira_entrada is null ) or primeira_entrada < '" . $perfat->dt_fim_query . "')
                group by setores";
        $setores = \DB::select(\DB::raw($sql));

        $sql = "select count(distinct id_caixapadrao) qtd
                from documento
                join reserva on reserva.id_reserva = documento.id_reserva
                where documento.id_cliente = ".$id_cliente."
                   and ( (datediff('" . $perfat->dt_fim_query . "', data_reserva) > ".intval($cliente->franquiacustodia)."    and primeira_entrada is null ) or primeira_entrada < '" . $perfat->dt_fim_query . "')
                and id_setor is null";
        $sem_setor = \DB::select(\DB::raw($sql));

        $data = array(
                    'cliente' => $cliente,
                    'departamentos' => $departamentos,
                    'centros' => $centros,
                    'setores' => $setores,
                    'sem_departamento' => $sem_departamento,
                    'sem_centro' => $sem_centro,
                    'sem_setor' => $sem_setor,
                    'total_caixas' => $total_caixas,
                    'perfat' => $perfat
                );
        $preco = \DB::table('hist_tipoprodutocliente')
                ->where('id_periodo_faturamento', '=', $fat->id_periodo_faturamento)
                ->where('id_tipoproduto', '=', 3)
                ->where('id_cliente', '=', $fat->id_cliente)
                ->first();
        $valor = $preco->valor_contrato;

        $data['valor'] = $preco->valor_contrato;

        $x = new \stdClass();
        $x->departamentos = '(sem departamento definido)';
        $x->qtd = $data['sem_departamento'][0]->qtd;
        $data['departamentos'][] = $x;
        $x = new \stdClass();
        $x->centros = '(sem centro de custo definido)';
        $x->qtd = $data['sem_centro'][0]->qtd;
        $data['centros'][] = $x;
        $x = new \stdClass();
        $x->setores = '(sem setor definido)';
        $x->qtd = $data['sem_setor'][0]->qtd;
        $data['setores'][] = $x;

        $data['departamentos'] = $this->calcula_valor_rateio($data['departamentos'], $valor, $fat->cliente->issporfora);
        $data['centros'] = $this->calcula_valor_rateio($data['centros'], $valor, $fat->cliente->issporfora);
        $data['setores'] = $this->calcula_valor_rateio($data['setores'], $valor, $fat->cliente->issporfora);
        return $data;
    }

    public function calcula_valor_rateio($rateio, $valor_unitario, $porfora){
        foreach($rateio as &$item){
            $item->valor = $item->qtd * $valor_unitario ;
            if ($porfora == 1){
                $item->valorcomiss = $item->valor * 1.05;
            } else {
                $item->valorcomiss = $item->valor / 0.95;
            }
        }
        return $rateio;
    }

}
