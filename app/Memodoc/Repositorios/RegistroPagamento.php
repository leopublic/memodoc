<?php
namespace Memodoc\Repositorios;
/**
 * Representa um registro de retorno do tipo "titulo" (01)
 */
class RegistroPagamento extends RegistroRetorno
{
    public $data_pagamento;
    public $valor_pago;

    public function __construct($registro, $linha) {
        parent::__construct($registro, $linha);
		$this->numero = substr($registro, 2,10);
		$this->nosso_numero = substr($registro, 12,20);
        $this->data_pagamento = $this->setDataTexto(substr($registro, 147, 6));
        $this->valor_pago = substr($registro, 43,11).".".substr($registro, 54,2);;
        $this->fl_duplicidade_cnpj = 0;
    }

    public function processa($id_arquivo_retorno){
		// Verifica se já existe registro com o mesno número
        $pag = $this->recupera_pagamento($id_arquivo_retorno);
		if ($pag){
            // Adicionar log de alterção
        } else {
            // Se não existir, cria um novo
            $pag = new \Pagamento;
            $pag->numero = $this->numero;
            $pag->nosso_numero = $this->nosso_numero;
        }
        // Se existe atualiza, registrando a atualização
        $pag->id_arquivo_retorno = $id_arquivo_retorno;
        $pag->data_pagamento = $this->data_pagamento;
        $pag->valor_pago = $this->valor_pago;
        $pag->linha = $this->linha;
        $pag->fl_duplicidade_cnpj = 0;
        $pag->save();
    }

}
