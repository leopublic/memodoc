<?php
namespace Memodoc\Repositorios;
/**
 * Representa um registro de retorno do tipo "titulo" (01)
 */
class RegistroTitulo extends RegistroRetorno
{
    public $id_cliente;
    public $cnpj;
    public $sacado;
    public $data_emissao;
    public $data_vencimento;
    public $valor_cobrado;

    public function __construct($registro, $linha) {
        parent::__construct($registro, $linha);
        $this->tipo_doc = substr($registro, 304,2);
        $this->cnpj = trim(substr($registro, 306,14));
		$this->sacado = trim(substr($registro, 320,37));
		$this->numero = trim(substr($registro, 13,10));
		$this->nosso_numero = trim(substr($registro, 23,20));
		$this->data_emissao = $this->setDataTexto(substr($registro, 73, 8), true);
		$this->data_vencimento = $this->setDataTexto(substr($registro, 81, 8), true);
		$this->valor_cobrado = intval(substr($registro, 89,11)).".".intval(substr($registro, 100,2));
        $this->nome_cliente = substr($registro, 320,100);
        $this->fl_duplicidade_cnpj = 0;
        $this->recupera_cliente();
    }

    public function processa($id_arquivo_retorno){
        // Verifica se já existe registro com o mesno número
        $pag = $this->recupera_pagamento($id_arquivo_retorno);
		if ($pag){
            // Adicionar log de alterção
        } else {
            // Se não existir, cria um novo
            $pag = new \Pagamento;
            $pag->numero = $this->numero;
            $pag->nosso_numero = $this->nosso_numero;
        }
        // Se existe atualiza, registrando a atualização
        $pag->cnpj = $this->cnpj;
        $pag->id_arquivo_retorno = $id_arquivo_retorno;
        $pag->id_cliente = $this->id_cliente;
        $pag->data_vencimento = $this->data_vencimento;
        $pag->data_emissao = $this->data_emissao;
        $pag->valor = $this->valor_cobrado;
        $pag->linha = $this->linha;
        $pag->nome_cliente = $this->nome_cliente;
        $pag->fl_duplicidade_cnpj = $this->fl_duplicidade_cnpj;
        $pag->save();
    }
    /**
	 *
	 * @param  [type] $registro [description]
	 * @return [type]           [description]
	 */
	public function recupera_cliente(){
	    $clientes = \Cliente::where('cgc', '=', $this->cnpj)->get();
	    if (count($clientes) > 0 ){
            $this->fl_duplicidade_cnpj = false;
            if (count($clientes) == 1){
                $cliente = $clientes[0];
                $this->id_cliente = $cliente->id_cliente;
            } else {
                $this->fl_duplicidade_cnpj = true;
                $this->id_cliente = null;
            }
	    } else {
            $pagadores = \Pagador::where('cnpj', '=', $this->cnpj)->get();
            if (count($pagadores) > 0 ){
                if (count($pagadores) == 1){
                    $pagador = $pagadores[0];
                    $this->id_cliente = $pagador->id_cliente;
                    $this->id_pagador = $pagador->id_pagador;
                } else {
                    $this->fl_duplicidade_cnpj = true;
                }
            } else {
                $this->id_cliente = null;
            }
	    }
	}
}
