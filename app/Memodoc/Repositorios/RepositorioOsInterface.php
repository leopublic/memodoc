<?php
namespace Memodoc\Repositorios;

interface RepositorioOsInterface{
	public function nova();
	public function novaAtendimento($id_cliente, $id_usuario_cadastro);
	public function osPendente($id_cliente, $id_usuario);

}
