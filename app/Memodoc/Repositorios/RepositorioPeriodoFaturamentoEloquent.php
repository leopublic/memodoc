<?php

namespace Memodoc\Repositorios;

class RepositorioPeriodoFaturamentoEloquent extends Repositorio {

    public function criar($post, $id_usuario) {
        $per = new \PeriodoFaturamento;
        $this->validacao = new \Memodoc\Validadores\PeriodoFaturamento($post);
        if ($this->validacao->passes()) {
            // Verifica se existe outro faturamento no mesmo período
            $inicio = \DateTime::createFromFormat('d/m/Y', $post['dt_inicio_fmt'])->format('Y-m-d');
            $fim = \DateTime::createFromFormat('d/m/Y', $post['dt_fim_fmt'])->format('Y-m-d');
            $where = "((dt_inicio >= '" . $inicio . "' and dt_inicio <= '" . $fim . "') or (dt_fim >= '" . $inicio . "' and dt_fim <= '" . $fim . "')  and fl_temporario = 0)";
            $existentes = \PeriodoFaturamento::whereRaw($where)->count();
            if ($existentes > 0) {
                $this->registraErro('Já existe um faturamento englobando o período informado. Verifique.');
                return false;
            } else {
                $per->dt_inicio_fmt = $post['dt_inicio_fmt'];
                $per->dt_fim_fmt = $post['dt_fim_fmt'];
                $per->dt_faturamento_fmt = $post['dt_fim_fmt'];
                $per->mes = \DateTime::createFromFormat('Y-m-d', $per->dt_fim)->format('m');
                $per->ano = \DateTime::createFromFormat('Y-m-d', $per->dt_fim)->format('Y');
                $per->id_usuario = $id_usuario;
                $per->fl_temporario = 0;
                $per->finalizado = -1;
                $ultimo = \PeriodoFaturamento::where('fl_temporario', '=',0)->max('sequencial');
                $per->sequencial = intval($ultimo) + 1;
                $per->status_conclusao = 2; // Sempre criados bloqueados
                $per->save();
                $this->adicionarTodosClientesAtivos($per, $id_usuario);
                $this->salvaEstruturaPreco($per, $id_usuario);
                $this->recalcular($per, $id_usuario);
                return $per;
            }
        } else {
            return false;
        }
    }

    public function editar($post, $id_usuario) {
        $per = \PeriodoFaturamento::find($post['id_periodo_faturamento']);

        $per->dt_inicio_fmt = $post['dt_inicio_fmt'];
        $per->dt_fim_fmt = $post['dt_fim_fmt'];
        $per->mes = \DateTime::createFromFormat('Y-m-d', $per->dt_fim)->format('m');
        $per->ano = \DateTime::createFromFormat('Y-m-d', $per->dt_fim)->format('Y');
        $per->finalizado = -1;
        $per->save();
        $this->adicionarTodosClientesAtivos($per, $id_usuario);
        $this->recalcular($per, $id_usuario);
        return $per;
    }

    public function adicionarTodosClientesAtivos(\PeriodoFaturamento $perfat, $id_usuario) {
        $sql = "insert into faturamento (id_periodo_faturamento, id_cliente, id_usuario) "
                . " select " . $perfat->id_periodo_faturamento . ", id_cliente ," . $id_usuario
                . " from cliente "
                . " where fl_ativo =1 "
                . " and id_cliente not in (select id_cliente "
                . " from faturamento "
                . "where id_periodo_faturamento = " . $perfat->id_periodo_faturamento . ")";
        \DB::insert(\DB::raw($sql));
    }

    public function recalcular(\PeriodoFaturamento $perfat, $id_usuario, $id_faturamento = '') {
        $this->zeraTodosTemporarios();
        $this->recalcularSemZerar($perfat, $id_usuario, $id_faturamento);
    }

    public function recalcularSemZerar(\PeriodoFaturamento $perfat, $id_usuario, $id_faturamento = '') {
        $this->calculaQtdCustodia($perfat, $id_usuario, $id_faturamento);
        $this->calculaValorCustodia($perfat, $id_usuario, $id_faturamento);

        $this->faturaOrdensDeServico($perfat, $id_usuario, $id_faturamento);
        $this->calculaQtdOrdensServico($perfat, $id_usuario, $id_faturamento);
        $this->calculaQtdAtendimento($perfat, $id_usuario, $id_faturamento);
        $this->calculaValorAtendimentoOs($perfat, $id_usuario, $id_faturamento);
        $this->calculaValorAtendimento($perfat, $id_usuario, $id_faturamento);

        $this->calculaValoresTotais($perfat, $id_usuario, $id_faturamento);

        if ($id_faturamento == '') {
            $recalculo = date('Y-m-d H:i:s');
            \DB::table('faturamento')
                    ->where('id_periodo_faturamento', '=', $perfat->id_periodo_faturamento)
                    ->update(array('dt_recalculo' => $recalculo, 'id_usuario_recalculo' => $id_usuario));
            $perfat->dt_recalculo = $recalculo;
            $perfat->id_usuario_recalculo = $id_usuario;
            $perfat->save();
        } else {
            $fat = \Faturamento::find($id_faturamento);
            $fat->dt_recalculo = date('Y-m-d H:i:s');
            $fat->id_usuario_recalculo = $id_usuario;
            $fat->save();
        }
    }

    public function salvaEstruturaPreco(\PeriodoFaturamento $perfat, $id_usuario) {
        // Limpa estrutura anterior
        $sql = "delete from hist_tipoprodutocliente where id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        \DB::delete(\DB::raw($sql));
        // Salva estrutura atual
        $sql = "insert into hist_tipoprodutocliente (id_periodo_faturamento, data, id_usuario, id_cliente, id_tipoproduto, valor_contrato, valor_urgencia, minimo)"
                . " select " . $perfat->id_periodo_faturamento . ", now(), " . $id_usuario . ", id_cliente, id_tipoproduto, valor_contrato, valor_urgencia, minimo"
                . " from tipoprodcliente "
                . " where id_cliente in (select id_cliente from faturamento where id_periodo_faturamento = " . $perfat->id_periodo_faturamento . ")";
        \DB::insert(\DB::raw($sql));

        $perfat->dt_carga_precos = date('Y-m-d h:i:s');
        $perfat->id_usuario_preco = $id_usuario;
        $perfat->save();
    }

    public function salvaEstruturaPrecoCliente(\Faturamento $fat, $id_usuario) {
        // Limpa estrutura anterior
        $sql = "delete from hist_tipoprodutocliente "
                . " where id_periodo_faturamento = " . $fat->id_periodo_faturamento
                . " and id_cliente=" . $fat->id_cliente;
        \DB::delete(\DB::raw($sql));
        // Salva estrutura atual
        $sql = "insert into hist_tipoprodutocliente (id_periodo_faturamento, data, id_usuario,  id_cliente, id_tipoproduto, valor_contrato, valor_urgencia, minimo)"
                . " select " . $fat->id_periodo_faturamento . ", now()," . $id_usuario . ", " . $fat->id_cliente . ", id_tipoproduto, valor_contrato, valor_urgencia, minimo"
                . " from tipoprodcliente "
                . " where id_cliente = " . $fat->id_cliente;
        \DB::insert(\DB::raw($sql));
    }

    public function calculaQtdCustodia(\PeriodoFaturamento $perfat, $id_usuario, $id_faturamento = '') {
        /**
         * Inicializa quantidades com zeros
         */
        $sql = "update faturamento"
                . " set faturamento.qtd_custodia_novas = 0 "
                . "   , faturamento.qtd_custodia_periodo = 0 "
                . "   , faturamento.qtd_expurgos = 0 "
                . "   , faturamento.qtd_expurgos_usadas = 0 "
                . "   , faturamento.qtd_custodia_em_carencia = 0"
                . "   , faturamento.qtd_custodia_mes_anterior = 0"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));


        // Calcula a qtd_custodia_periodo
        // Total de caixas reservadas que deram entrada
        $sql = "update faturamento"
                . " set faturamento.qtd_custodia_periodo = (select count(distinct id_caixa) "
                . "  from documento "
                . " where id_cliente = faturamento.id_cliente "
                . "   and primeira_entrada < '" . $perfat->dt_fim_query . " 00:00:00')"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        $sql = "update faturamento"
                . " set faturamento.qtd_custodia_novas = (select ifnull(count(distinct id_caixa), 0) "
                . " from documento "
                . " where id_cliente = faturamento.id_cliente "
                . " and primeira_entrada > '" . $perfat->dt_inicio_query . " 23:59:59'"
                . " and primeira_entrada < '" . $perfat->dt_fim_query . " 00:00:00' ) "

                . " + (select ifnull(count(distinct documento_cancelado.id_caixa), 0) "
                . " from documento_cancelado , expurgo "
                . " where expurgo.id_cliente = faturamento.id_cliente "
                . " and expurgo.id_caixa = documento_cancelado.id_caixa "
                . " and expurgo.data_solicitacao > '" . $perfat->dt_inicio_query . "' "
                . " and expurgo.data_solicitacao < '" . $perfat->dt_fim_query . "' "
                . " and primeira_entrada > '" . $perfat->dt_inicio_query . " 23:59:59'"
                . " and primeira_entrada < '" . $perfat->dt_fim_query . " 00:00:00' ) "

                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        /**
         * Calcula a quantidade de caixas não usadas que começaram a ser cobradas
         * qtd_custodia_em_carencia
         */
        // Calcula a carencia
        \DB::update(\DB::raw($sql));
        $sql = "update faturamento"
                . " set faturamento.qtd_custodia_em_carencia = (select count(distinct d.id_caixa)"
                . " from documento d, cliente c, reserva r , reservadesmembrada rd"
                . " where c.id_cliente = faturamento.id_cliente"
                . " and d.id_cliente = faturamento.id_cliente"
                . " and r.id_cliente = faturamento.id_cliente"
                . " and rd.id_caixa = d.id_caixa"
                . " and r.id_reserva = rd.id_reserva"
                . " and (d.primeira_entrada is null or d.primeira_entrada >= '" . $perfat->dt_fim_query . " 00:00:00')"
                . " and r.data_reserva < '" . $perfat->dt_fim_query . " 23:59:59'"
                . " and datediff('" . $perfat->dt_fim_query . "', r.data_reserva) > coalesce(c.franquiacustodia, 0)"
                . " and coalesce(c.franquiacustodia, 0) > 0"
                . " )"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula as caixas não cobradas
        \DB::update(\DB::raw($sql));
        $sql = "update faturamento"
                . " set faturamento.qtd_custodia_nao_cobrada = (select count(distinct d.id_caixa)"
                . " from documento d, cliente c, reserva r , reservadesmembrada rd"
                . " where c.id_cliente = faturamento.id_cliente"
                . " and d.id_cliente = faturamento.id_cliente"
                . " and r.id_cliente = faturamento.id_cliente"
                . " and rd.id_caixa = d.id_caixa"
                . " and r.id_reserva = rd.id_reserva"
                . " and (d.primeira_entrada is null or d.primeira_entrada >= '" . $perfat->dt_fim_query . " 00:00:00')"
                . " and r.data_reserva < '" . $perfat->dt_fim_query . " 23:59:59'"
                . " and (
                            (datediff('" . $perfat->dt_fim_query . "', r.data_reserva) <= coalesce(c.franquiacustodia, 0)
                             and coalesce(c.franquiacustodia, 0) > 0 )
                         or ( coalesce(c.franquiacustodia, 0) = 0 )"
                . " )"
                . " )"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Salva as caixas que estavam em carência
        $sql = "delete from faturamento_emcarencia "
                ." where id_faturamento in ("
                    ." select id_faturamento from faturamento"
                    ." where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento
                . ")";
        \DB::update(\DB::raw($sql));
        $sql = "insert into faturamento_emcarencia(id_faturamento, id_caixa)
                    select distinct id_faturamento, documento.id_caixa
                    from faturamento
                    join periodo_faturamento on periodo_faturamento.id_periodo_faturamento = faturamento.id_periodo_faturamento
                    join documento on documento.id_cliente = faturamento.id_cliente
                    join reserva on reserva.id_reserva = documento.id_reserva
                    join cliente on cliente.id_cliente = faturamento.id_cliente
                    where (documento.primeira_entrada is null or documento.primeira_entrada > '" . $perfat->dt_fim_query . " 00:00:00')
                    and datediff('" . $perfat->dt_fim_query . "', reserva.data_reserva) > coalesce(cliente.franquiacustodia, 0)
                    and coalesce(cliente.franquiacustodia, 0) > 0"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        \DB::update(\DB::raw($sql));


        // Salva as caixas que estavam nao cobradas
        $sql = "delete from faturamento_naocobradas "
                ." where id_faturamento in ("
                    ." select id_faturamento from faturamento"
                    ." where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento
                . ")";
        \DB::update(\DB::raw($sql));
        $sql = "insert into faturamento_naocobradas(id_faturamento, id_caixa)
                    select distinct id_faturamento, documento.id_caixa
                    from faturamento
                    join periodo_faturamento on periodo_faturamento.id_periodo_faturamento = faturamento.id_periodo_faturamento
                    join documento on documento.id_cliente = faturamento.id_cliente
                    join reserva on reserva.id_reserva = documento.id_reserva
                    join cliente on cliente.id_cliente = faturamento.id_cliente
                    where (documento.primeira_entrada is null or documento.primeira_entrada > periodo_faturamento.dt_fim)
                    and datediff(periodo_faturamento.dt_fim, reserva.data_reserva) <= coalesce(cliente.franquiacustodia, 0)
                    and datediff(periodo_faturamento.dt_fim, reserva.data_reserva) > 0
                    and coalesce(cliente.franquiacustodia, 0) > 0 "
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        \DB::update(\DB::raw($sql));

        $sql = "insert into faturamento_naocobradas(id_faturamento, id_caixa)
                    select distinct id_faturamento, documento.id_caixa
                    from faturamento
                    join periodo_faturamento on periodo_faturamento.id_periodo_faturamento = faturamento.id_periodo_faturamento
                    join documento on documento.id_cliente = faturamento.id_cliente
                    join reserva on reserva.id_reserva = documento.id_reserva
                    join cliente on cliente.id_cliente = faturamento.id_cliente
                    where (documento.primeira_entrada is null or documento.primeira_entrada > periodo_faturamento.dt_fim)
                    and coalesce(cliente.franquiacustodia, 0) = 0 "
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        \DB::update(\DB::raw($sql));

        // Calcula expurgadas no periodo
        $sql = "update faturamento"
                . " set qtd_expurgos = (select count(*) "
                . " from expurgo e , expurgo_pai ep"
                . " where ep.data_solicitacao > '" . $perfat->dt_inicio_query . "'"
                . " and  ep.data_solicitacao < '" . $perfat->dt_fim_query . "'"
                . " and e.id_expurgo_pai = ep.id_expurgo_pai"
                . " and ep.id_cliente = faturamento.id_cliente"
                . ") "
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula expurgadas cobráveis no periodo
        $sql = "update faturamento"
                . " set qtd_expurgos_cobravel = (select count(*) "
                . " from expurgo e join expurgo_pai ep on ep.id_expurgo_pai = e.id_expurgo_pai"
                . " where ep.data_solicitacao > '" . $perfat->dt_inicio_query . "'"
                . " and  ep.data_solicitacao < '" . $perfat->dt_fim_query . "'"
                . " and coalesce(ep.cortesia, 0) = 0"
                . " and ep.id_cliente = faturamento.id_cliente"
                . ") "
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula expurgadas no periodo - SOMENTE AS CAIXAS QUE ESTAVAM EM USO
        $sql = "update faturamento"
                . " set qtd_expurgos_usadas = (select count(distinct e.id_caixapadrao) "
                . " from expurgo e , documento_cancelado d, reservadesmembrada rd, reserva r, cliente c"
                . " where e.data_solicitacao > '" . $perfat->dt_inicio_query . "'"
                . " and  e.data_solicitacao < '" . $perfat->dt_fim_query . "'"
                . " and e.id_cliente = faturamento.id_cliente"
                . " and d.id_caixa = e.id_caixa "
                . " and rd.id_caixa = d.id_caixa"
                . " and c.id_cliente = faturamento.id_cliente"
                . " and r.id_reserva = rd.id_reserva"
                . " and d.primeira_entrada is not null  "
                . ") "
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula expurgadas no periodo - SOMENTE AS CAIXAS QUE ESTAVAM EM CARENCIA
        $sql = "update faturamento"
                . " set qtd_expurgos_carencia = (select count(distinct e.id_caixapadrao) "
                . " from expurgo e , documento_cancelado d, reservadesmembrada rd, reserva r, cliente c"
                . " where e.data_solicitacao > '" . $perfat->dt_inicio_query . "'"
                . " and  e.data_solicitacao < '" . $perfat->dt_fim_query . "'"
                . " and e.id_cliente = faturamento.id_cliente"
                . " and d.id_caixa = e.id_caixa "
                . " and rd.id_caixa = d.id_caixa"
                . " and c.id_cliente = faturamento.id_cliente"
                . " and r.id_reserva = rd.id_reserva"
                . " and ( (d.primeira_entrada is null or d.primeira_entrada >= '" . $perfat->dt_fim_query . " 00:00:00')"
                . "            and datediff('" . $perfat->dt_fim_query . "', r.data_reserva) > coalesce(c.franquiacustodia, 0)"
                . "            and coalesce(c.franquiacustodia, 0) > 0"
                . "          )"
                . "     ) "
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula qtd custodia mes anterior
        $sql = "update faturamento"
                . " set qtd_custodia_mes_anterior = qtd_custodia_periodo - qtd_custodia_novas + qtd_expurgos_usadas"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula valor do desconto
        $sql = "update faturamento, cliente"
                . " set faturamento.qtd_desconto = cliente.descontocustodialimite "
                . " where cliente.id_cliente = faturamento.id_cliente"
                . " and cliente.descontocustodialimite > 0"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
    }

    public function calculaValorCustodia(\PeriodoFaturamento $perfat, $id_usuario, $id_faturamento = '') {

        // Calcula valor custodia mes anterior
        $sql = "update faturamento, hist_tipoprodutocliente"
                . " set faturamento.val_custodia_periodo = (ifnull(qtd_custodia_periodo, 0) + ifnull(qtd_custodia_em_carencia, 0)) * hist_tipoprodutocliente.valor_contrato"
                . " , val_custodia_periodo_cobrado = 0"
                . " , val_dif_minimo = 0"
                . " where hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpARMAZENAMENTO
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Atualiza o valor da custodia para o valor mínimo...
        $sql = "update faturamento, cliente"
                . " set faturamento.val_custodia_periodo_cobrado = val_custodia_periodo"
                . " , val_dif_minimo = 0"
                . " where cliente.id_cliente = faturamento.id_cliente"
                . " and val_custodia_periodo >= cliente.valorminimofaturamento"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        $sql = "update faturamento, cliente"
                . " set faturamento.val_custodia_periodo_cobrado = cliente.valorminimofaturamento"
                . "   , val_dif_minimo = cliente.valorminimofaturamento - val_custodia_periodo"
                . " where cliente.id_cliente = faturamento.id_cliente"
                . " and val_custodia_periodo < cliente.valorminimofaturamento"
                . " and (ifnull(qtd_custodia_periodo, 0) + ifnull(qtd_custodia_em_carencia, 0)) > 0"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));


        // Calcula valor expurgos por OS
        $sql = "update faturamento, hist_tipoprodutocliente"
                . " set faturamento.val_expurgos = qtd_expurgos_cobravel * hist_tipoprodutocliente.valor_contrato"
                . " where hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpEXPURGO
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        $sql = "update faturamento, hist_tipoprodutocliente, cliente"
                . " set faturamento.val_outros         = descontocustodialimite * ifnull(hist_tipoprodutocliente.valor_contrato, 0) * (descontocustodiapercentual/100) "
                . " where hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and cliente.id_cliente = faturamento.id_cliente"
                . " and cliente.descontocustodialimite is not null"
                . " and cliente.descontocustodialimite > 0"
                . " and cliente.descontocustodiapercentual is not null"
                . " and cliente.descontocustodiapercentual > 0"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpARMAZENAMENTO
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
    }

    /**
     * Associa as ordens de serviço ao faturamento pela Data de atendimento
     * @param  \PeriodoFaturamento $perfat         [description]
     * @param  [type]              $id_usuario     [description]
     * @param  string              $id_faturamento [description]
     * @return [type]                              [description]
     */
    public function faturaOrdensDeServico(\PeriodoFaturamento $perfat, $id_usuario, $id_faturamento = '') {
        $sql = "update os, faturamento"
                . " set os.id_faturamento = null "
                . " where os.id_faturamento = faturamento.id_faturamento "
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        $sql = "update os, faturamento"
                . " set os.id_faturamento = faturamento.id_faturamento"
                . "   , id_usuario_alteracao = " . $id_usuario
                . "   , evento ='Adicionada ao faturamento (periodo) " . $perfat->id_periodo_faturamento . "'"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento
                . " and os.id_cliente = faturamento.id_cliente"
                . " and os.entregar_em > '" . $perfat->dt_inicio_query . "'"
                . " and os.entregar_em < '" . $perfat->dt_fim_query . "'"
                . " and coalesce(os.id_status_os, 0) > 1 "
                . " and coalesce(os.id_status_os, 0) < 5 "
//                . " and coalesce(os.cancelado, 0) = 0 "
                ;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
    }

    /**
     * Atualiza as quantidades de cada OS que serão usadas para calcular os valores
     * @param \PeriodoFaturamento $perfat
     * @param type $id_usuario
     * @param type $id_faturamento
     */
    public function calculaQtdOrdensServico(\PeriodoFaturamento $perfat, $id_usuario, $id_faturamento = '') {
		$this->isenta_movimentacao_franquia($perfat, $id_usuario, $id_faturamento );
    	
		//
        // Atualiza a quantidade de movimentação de entrega das ordens de serviço
        $sql = "update os, faturamento set os.qtd_movimentacao = "
        		. "   (select count(distinct id_caixa) "
                . "      from osdesmembrada"
                . "     where osdesmembrada.id_os_chave = os.id_os_chave)  "
                . " + (select count(*) from transbordo, transbordo_caixa 
                		where transbordo_caixa.id_transbordo = transbordo.id_transbordo 
                		  and transbordo.id_os_chave = os.id_os_chave 
                		  and coalesce(transbordo.cortesia_movimentacao, 0) = 0 )"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        $sql = "update os, faturamento set os.qtd_movimentacao_isenta = "
        		. "   (select count(distinct id_caixa) "
        		. "      from osdesmembrada"
        		. "     where osdesmembrada.id_os_chave = os.id_os_chave
                		  and osdesmembrada.fl_movimentacao_isenta_percentual = 1)  "
        	. " where os.id_faturamento = faturamento.id_faturamento"
            . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
        	$sql.= " and faturamento.id_faturamento = " . $id_faturamento;
		}
        \DB::update(\DB::raw($sql));
        
        
        // Calcula zera o frete das OS onde ele não é cobrado
        $sql = "update os, faturamento "
                . " set os.qtd_frete = 0"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and coalesce(os.cobrafrete,0)= 0"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
        // Calcula a quantidade de frete de cada OS
        $sql = "update os, faturamento "
                . " set os.qtd_frete = ifnull(os.qtd_movimentacao, 0)
                                    + ifnull(os.apanhacaixasemprestadas, 0 )
                                    + ifnull(os.apanhanovascaixas, 0)"
                . " , os.qtd_frete_dem = ifnull(os.qtd_movimentacao, 0)
                                    + ifnull(os.apanhacaixasemprestadas, 0 )
                                    + ifnull(os.apanhanovascaixas, 0)"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and coalesce(os.cobrafrete,0)= 1"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));


        // EXPURGOS DA OS
        // *==========================================================================================================================================
        // Zera as quantidades de expurgo
        $sql = "update os, faturamento "
                . " set os.qtd_expurgos_cobraveis = 0"
                . "   , os.qtd_expurgos = 0"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
        //
        // Calcula quantidade de expurgos 
        $sql = "update os, faturamento, expurgo_pai "
                . " set os.qtd_expurgos = (select count(*) from expurgo where expurgo.id_expurgo_pai = expurgo_pai.id_expurgo_pai )"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and expurgo_pai.id_os_chave = os.id_os_chave"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
        //
        // Calcula quantidade de expurgos cobráveis
        $sql = "update os, faturamento, expurgo_pai "
                . " set os.qtd_expurgos_cobraveis = (select count(*) from expurgo where expurgo.id_expurgo_pai = expurgo_pai.id_expurgo_pai )"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and expurgo_pai.id_os_chave = os.id_os_chave"
                . " and coalesce(expurgo_pai.cortesia, 0) = 0"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
        //
        // Calcula quantidade de movimentaões de expurgos cobráveis
        $sql = "update os, faturamento, expurgo_pai "
                . " set os.qtd_mov_expurgos_cobraveis = (select count(*) from expurgo where expurgo.id_expurgo_pai = expurgo_pai.id_expurgo_pai )"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and expurgo_pai.id_os_chave = os.id_os_chave"
                . " and coalesce(expurgo_pai.cortesia_movimentacao, 0) = 0"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
        //*=======================================================================================================================================


        // Ajusta quantidade mínima
        $sql = "update os, faturamento, hist_tipoprodutocliente "
                . " set os.qtd_frete = hist_tipoprodutocliente.minimo"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpFRETE
                . " and coalesce(os.cobrafrete,0)= 1"
                . " and os.qtd_frete < hist_tipoprodutocliente.minimo"
//                . " and os.qtd_frete > 0"  Se estiver marcado para cobrar frete, cobra o mínimo mesmo que não tenha quantidade nenhuma. Pedro: 14/08/2014
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
    }

    public function isenta_movimentacao_franquia(\PeriodoFaturamento $perfat, $id_usuario, $id_faturamento = ""){
		$where = " faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $where.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        $where .= " and id_cliente in (select id_cliente from cliente where franquiamovimentacao > 0)";

        $faturamentos = \Faturamento::whereRaw($where)
        			->with('cliente')
        			->get();
   		
   		foreach($faturamentos as $fat){
   			$isentadas = 0;
   			$qtd_total = $fat->qtd_custodia_em_carencia + $fat->qtd_custodia_periodo;
   			$perc_isencao = $fat->cliente->franquiamovimentacao / 100;
   			$qtd_isenta = $qtd_total * $perc_isencao;

   			$sql = "update osdesmembrada, os set fl_movimentacao_isenta_percentual = 0 where os.id_os_chave = osdesmembrada.id_os_chave and os.id_faturamento = ".$fat->id_faturamento;
   			
   			$sql = "select * from osdesmembrada, os where os.id_os_chave = osdesmembrada.id_os_chave and os.id_faturamento = ".$fat->id_faturamento." order by os.entregar_em ";
   			$movimentacoes = \DB::select(\DB::raw($sql));
  
   			foreach($movimentacoes as $mov){
   				$sql = "update osdesmembrada set fl_movimentacao_isenta_percentual = 1 where id_osdesmembrada = ".$mov->id_osdesmembrada;
				\DB::update(\DB::raw($sql));   				
   				$isentadas++;
   				if ($isentadas >= $qtd_isenta ) {
   					break;
   				}
   			}
   		}
    }
    
    
    
    /**
     * Calcula as quantidades de atendimento e serviços de cada faturamento do período sendo faturado
     * @param  \PeriodoFaturamento $perfat         [description]
     * @param  [type]              $id_usuario     [description]
     * @param  string              $id_faturamento [description]
     * @return [type]                              [description]
     */
    public function calculaQtdAtendimento(\PeriodoFaturamento $perfat, $id_usuario, $id_faturamento = '') {
        // Calcula o total de retornos de consulta do periodo
        $sql = "update faturamento set qtd_retornos_de_consulta = (select sum(ifnull(apanhacaixasemprestadas, 0)) "
                . " from os"
                . " where os.id_faturamento = faturamento.id_faturamento) "
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula o somatório das quantidades movimentadas total
        $sql = "update faturamento set qtd_movimentacao = (select sum(ifnull(qtd_movimentacao, 0)) - sum(ifnull(qtd_movimentacao_isenta,0)) "
                . " from os"
                . " where coalesce(os.urgente, 0) = 0"
                . " and os.id_faturamento = faturamento.id_faturamento)"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Apura a quantidade de movimentação urgente
        $sql = "update faturamento set qtd_movimentacao_urgente = (select sum(ifnull(qtd_movimentacao, 0)) - sum(ifnull(qtd_movimentacao_isenta,0)) "
                . " from os"
                . " where os.urgente = 1"
                . " and os.id_faturamento = faturamento.id_faturamento)"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Apura a movimentação associada aos expurgos
        $sql = "update faturamento set qtd_movimentacao_expurgo = (select count(*)  "
                . " from os, expurgo_pai, expurgo"
                . " where expurgo_pai.id_os_chave = os.id_os_chave "
                . " and expurgo.id_expurgo_pai = expurgo_pai.id_expurgo_pai"
                . " and coalesce(expurgo_pai.cortesia_movimentacao, 0) = 0"
                . " and os.id_faturamento = faturamento.id_faturamento)"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }

        \DB::update(\DB::raw($sql));
        //
        // Calcula a quantidade de cartonagens enviadas
        // Primeiro a quantidade de etiquetas enviadas com cartonagem
        $sql = "update faturamento set qtd_cartonagem_enviadas = ifnull((select sum(ifnull(qtdenovascaixas, 0)) "
                . " from os"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and naoecartonagemcortesia = 1), 0)"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
        //Depois soma a quantidade de caixas avulsas enviadas
        $sql = "update faturamento set qtd_cartonagem_enviadas = ifnull(qtd_cartonagem_enviadas, 0) + ifnull((select sum(ifnull(qtd_cartonagens_cliente, 0)) "
                . " from os"
                . " where os.id_faturamento = faturamento.id_faturamento), 0)"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        $sql = "update faturamento
                    set faturamento.qtd_frete =
                              (select sum(ifnull(os.qtd_frete, 0))
                                 from os
                                where coalesce(os.urgente,0)= 0
                                  and coalesce(os.cobrafrete,0)= 1
                                  and os.id_faturamento = faturamento.id_faturamento)
                    where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        $sql = "update faturamento
                    set faturamento.qtd_frete_urgente =
                              (select sum(ifnull(os.qtd_frete, 0))
                                 from os
                                where coalesce(os.urgente,0)= 1
                                  and os.id_faturamento = faturamento.id_faturamento)
                    where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        $sql = "update faturamento
                    set faturamento.qtd_frete_dem =
                              (select sum(ifnull(os.qtd_frete_dem, 0))
                                 from os
                                where os.id_faturamento = faturamento.id_faturamento)
                    where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
    }
    /**
     * Calcula os valores dos atendimentos apurados em cada OS
     * @param  \PeriodoFaturamento $perfat         [description]
     * @param  [type]              $id_usuario     [description]
     * @param  string              $id_faturamento [description]
     * @return [type]                              [description]
     */
    public function calculaValorAtendimentoOs(\PeriodoFaturamento $perfat, $id_usuario, $id_faturamento = '') {
        // Calcula o valor da movimentacao normal
        $sql = "update os, faturamento, hist_tipoprodutocliente"
                . " set os.valormovimento = ( ifnull(os.qtd_movimentacao, 0) - ifnull(os.qtd_movimentacao_isenta, 0) ) * ifnull(hist_tipoprodutocliente.valor_contrato, 0)"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and coalesce(os.urgente,0)= 0"
                . " and hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpMOVIMENTACAO
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));


        // Calcula o valor da movimentacao urgente
        $sql = "update os, faturamento, hist_tipoprodutocliente"
                . " set os.valormovimento = ( ifnull(os.qtd_movimentacao,0) - ifnull(os.qtd_movimentacao_isenta, 0)) * ifnull(hist_tipoprodutocliente.valor_urgencia, 0)"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and coalesce(os.urgente,0)= 1"
                . " and hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpMOVIMENTACAO
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Zera os valores de cartonagem
        $sql = "update os, faturamento"
                . " set os.valorcartonagem = 0"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula o valor da cartonagem
        $sql = "update os, faturamento, hist_tipoprodutocliente"
                . " set os.valorcartonagem = (ifnull(os.qtdenovascaixas, 0) + ifnull(os.qtd_cartonagens_cliente, 0)  )* ifnull(hist_tipoprodutocliente.valor_contrato, 0)"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and coalesce(os.naoecartonagemcortesia,0)= 1"
                . " and hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpCARTONAGEM
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Zera os valores de frete
        $sql = "update os, faturamento"
                . " set os.valorfrete= 0"
                . " where os.id_faturamento = faturamento.id_faturamento"
  //              . " and coalesce(os.cobrafrete,0)= 0"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula o valor do frete fixo
        $sql = "update os, faturamento"
                . " set os.valorfrete= os.val_frete_manual"
                . " where os.id_faturamento = faturamento.id_faturamento"
//                . " and cliente.id_cliente = os.id_cliente"
  //              . " and coalesce(os.cobrafrete,0)= 0"
//                . " and coalesce(cliente.fl_frete_manual,0)= 1"
                . " and coalesce(os.fl_frete_manual,0)= 1"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));


        // Calcula o valor do frete normal
        $sql = "update os, faturamento, hist_tipoprodutocliente"
                . " set os.valorfrete = ifnull(os.qtd_frete, 0) * ifnull(hist_tipoprodutocliente.valor_contrato, 0)"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and coalesce(os.urgente,0)= 0"
                . " and coalesce(os.cobrafrete,0)= 1"
                . " and coalesce(os.fl_frete_manual,0)= 0"
                . " and hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpFRETE
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula o valor do frete urgente
        $sql = "update os, faturamento, hist_tipoprodutocliente"
                . " set os.valorfrete = ifnull(os.qtd_frete, 0)  * ifnull(hist_tipoprodutocliente.valor_urgencia, 0)"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and coalesce(os.urgente,0)= 1"
                . " and coalesce(os.cobrafrete,0)= 1"
                . " and coalesce(os.fl_frete_manual,0)= 0"
                . " and hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpFRETE
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // EXPURGO
        // *==================================================================================================================================
        // Zera os valores de expurgo
        $sql = "update os, faturamento"
                . " set os.val_expurgo= 0"
                . "   , val_mov_expurgo = 0"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula o valor do expurgo
        $sql = "update os, faturamento, hist_tipoprodutocliente"
                . " set os.val_expurgo= ifnull(os.qtd_expurgos_cobraveis, 0) * ifnull(hist_tipoprodutocliente.valor_contrato, 0)"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpEXPURGO
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula movimentação do expurgo
        $sql = "update os, faturamento, hist_tipoprodutocliente"
                . " set os.val_mov_expurgo = ifnull(os.qtd_mov_expurgos_cobraveis, 0) * ( ifnull(hist_tipoprodutocliente.valor_contrato, 0) / 2)"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpMOVIMENTACAO
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
        // *==================================================================================================================================



    }



    public function calculaValorAtendimento(\PeriodoFaturamento $perfat, $id_usuario, $id_faturamento = '') {
        // Apura os totais associados às OS
        $sql = "update faturamento "
                . " set val_frete         = (select sum(ifnull(valorfrete, 0)) from os where os.id_faturamento = faturamento.id_faturamento and coalesce(os.urgente, 0) = 0)"
                . "   , val_frete_urgente = (select sum(ifnull(valorfrete, 0)) from os where os.id_faturamento = faturamento.id_faturamento and os.urgente = 1)"
                . "   , val_movimentacao_expurgo = (select sum(ifnull(val_mov_expurgo, 0)) from os where os.id_faturamento = faturamento.id_faturamento)"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula outros valores
        $sql = "update faturamento, hist_tipoprodutocliente"
                . " set faturamento.val_movimentacao         = (ifnull(qtd_movimentacao, 0) * ifnull(hist_tipoprodutocliente.valor_contrato, 0)) "
                . "    ,faturamento.val_movimentacao_urgente = (ifnull(qtd_movimentacao_urgente, 0) * ifnull(hist_tipoprodutocliente.valor_urgencia, 0))"
                . " where hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpMOVIMENTACAO
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));


//        $sql = "update faturamento, hist_tipoprodutocliente"
//                . " set faturamento.val_cartonagem_enviadas = (ifnull(qtd_cartonagem_enviadas, 0) * hist_tipoprodutocliente.valor_contrato) "
//                . " where hist_tipoprodutocliente.id_cliente = faturamento.id_cliente"
//                . " and hist_tipoprodutocliente.id_periodo_faturamento = faturamento.id_periodo_faturamento"
//                . " and hist_tipoprodutocliente.id_tipoproduto = " . \TipoProduto::tpCARTONAGEM
//                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
//        if ($id_faturamento != ''){
//            $sql.= " and faturamento.id_faturamento = ".$id_faturamento;
//        }
//        \DB::update(\DB::raw($sql));

        $sql = "update faturamento "
                . " set val_cartonagem_enviadas = ifnull((select sum(valorcartonagem) from os where os.id_faturamento = faturamento.id_faturamento), 0)"
                . " where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
    }


    public function calculaValoresTotais(\PeriodoFaturamento $perfat, $id_usuario, $id_faturamento = '') {
        // Apura o valor da custódia
        $sql = "update faturamento
                set faturamento.val_servicos_sem_iss = ifnull(val_custodia_periodo_cobrado, 0)
                                                        + ifnull(val_expurgos, 0)
                                                        + ifnull(val_frete, 0)
                                                        + ifnull(val_frete_urgente, 0)
                                                        + ifnull(val_movimentacao, 0)
                                                        + ifnull(val_movimentacao_expurgo, 0)
                                                        + ifnull(val_movimentacao_urgente, 0)
                                                        - ifnull(val_outros, 0)
                where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        // Calcula o valor total apurado
        $sql = "update faturamento
                set faturamento.val_total = ifnull(val_cartonagem_enviadas,0) + ifnull(val_servicos_sem_iss, 0)
                where faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));


        // Calcula o ISS
        $sql = "update faturamento, cliente
                set faturamento.val_iss = round(ifnull(val_servicos_sem_iss, 0) * 0.05, 2)
                 , faturamento.val_iss_nota = truncate( (ifnull(val_servicos_sem_iss, 0) + ifnull(val_cartonagem_enviadas, 0) ) * 0.05, 2)
                , faturamento.val_outros_com_iss = round(ifnull(val_outros, 0) * 1.05, 2)
                where cliente.id_cliente = faturamento.id_cliente
                and cliente.issporfora = 1
                and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));

        $sql = "update faturamento, cliente
                set faturamento.val_iss = round( (ifnull(val_servicos_sem_iss, 0) / 0.95) - ifnull(val_servicos_sem_iss, 0), 2)
                , faturamento.val_iss_nota = truncate( (ifnull(val_servicos_sem_iss, 0) + ifnull(val_cartonagem_enviadas, 0) ) / 0.95, 2)
                , faturamento.val_outros_com_iss = round( ifnull(val_outros, 0) / 0.95 , 2)
                where cliente.id_cliente = faturamento.id_cliente
                and coalesce(cliente.issporfora, 0) = 0
                and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        if ($id_faturamento != '') {
            $sql.= " and faturamento.id_faturamento = " . $id_faturamento;
        }
        \DB::update(\DB::raw($sql));
    }

    /**
     * Retorna o faturamento imediatamente posterior ao informado
     * @param \Faturamento $fat
     * @return null
     */
    public function proximoFaturamento(\Faturamento $fat) {
        $sql = "select id_faturamento
                from faturamento, cliente
                where faturamento.id_periodo_faturamento = " . $fat->id_periodo_faturamento . "
                and cliente.id_cliente = faturamento.id_cliente
                and cliente.razaosocial > (select razaosocial from cliente where id_cliente = " . $fat->id_cliente . ")
                order by razaosocial";
        $res = \DB::select(\DB::raw($sql));
        if (count($res) > 0) {
            $id_fat = $res[0]->id_faturamento;
            if ($id_fat > 0) {
                $faturamento = \Faturamento::find($id_fat);
                return $faturamento;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Retorna o faturamento imediatamente anterior ao informado
     * @param \Faturamento $fat
     * @return null
     */
    public function anteriorFaturamento(\Faturamento $fat) {
        $sql = "select id_faturamento
                from faturamento, cliente
                where faturamento.id_periodo_faturamento = " . $fat->id_periodo_faturamento . "
                and cliente.id_cliente = faturamento.id_cliente
                and cliente.razaosocial < (select razaosocial from cliente where id_cliente = " . $fat->id_cliente . ")
                order by razaosocial desc";
        $res = \DB::select(\DB::raw($sql));
        if (count($res) > 0) {
            $id_fat = $res[0]->id_faturamento;
            if ($id_fat > 0) {
                $faturamento = \Faturamento::find($id_fat);
                return $faturamento;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Retorna o faturamento imediatamente anterior ao informado
     * @param \Faturamento $fat
     * @return null
     */
    public function anteriorFaturamentoCliente(\Faturamento $fat) {
        $perfat = \PeriodoFaturamento::find($fat->id_periodo_faturamento);
        $sql = "select id_faturamento
                from faturamento, periodo_faturamento
                where faturamento.id_cliente = " . $fat->id_cliente . "
                and periodo_faturamento.id_periodo_faturamento = faturamento.id_periodo_faturamento
                and periodo_faturamento.ano <= " . $perfat->ano . "
                and periodo_faturamento.mes <= " . $perfat->mes . "
                and faturamento.id_faturamento <> " . $fat->id_faturamento . "
                order by periodo_faturamento.ano desc, periodo_faturamento.mes desc";
        $res = \DB::select(\DB::raw($sql));
        if (count($res) > 0) {
            $id_fat = $res[0]->id_faturamento;
            if ($id_fat > 0) {
                $faturamento = \Faturamento::find($id_fat);
                return $faturamento;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Retorna o faturamento imediatamente anterior ao informado
     * @param \Faturamento $fat
     * @return null
     */
    public function proximoFaturamentoCliente(\Faturamento $fat) {
        $perfat = \PeriodoFaturamento::find($fat->id_periodo_faturamento);
        $sql = "select id_faturamento
                from faturamento, periodo_faturamento
                where faturamento.id_cliente = " . $fat->id_cliente . "
                and periodo_faturamento.id_periodo_faturamento = faturamento.id_periodo_faturamento
                and periodo_faturamento.ano >= " . $perfat->ano . "
                and periodo_faturamento.mes >= " . $perfat->mes . "
                and faturamento.id_faturamento <> " . $fat->id_faturamento . "
                order by periodo_faturamento.ano asc, periodo_faturamento.mes asc";
        $res = \DB::select(\DB::raw($sql));
        if (count($res) > 0) {
            $id_fat = $res[0]->id_faturamento;
            if ($id_fat > 0) {
                $faturamento = \Faturamento::find($id_fat);
                return $faturamento;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function criarDemonstrativosOffline($job, $data) {

        $this->criarDemonstrativos($data['id_periodo_faturamento'], $data['id_usuario']);
    }

    public function criarDemonstrativos($id_periodo_faturamento, $id_usuario, $id_tipo_faturamento= "1") {
        set_time_limit(0);
        session_write_close();
        $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
        try {
//            $faturas = \Faturamento::where('id_periodo_faturamento', '=', $perfat->id_periodo_faturamento)->sharedLock()->get();
            $sql = "update faturamento set status_demonstrativo = 0 where id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
            \DB::update(\DB::raw($sql));
            $sql = "select count(*) from faturamento where id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
            $res = \DB::connection('mysql_fat')->getPdo()->query($sql);
            $rs = $res->fetch(\PDO::FETCH_BOTH);
            $quantos = $rs[0];

            $rep = new \Memodoc\Repositorios\RepositorioFaturamentoEloquent;
            $perfat->finalizado = 0;
            $perfat->status = "Iniciando processamento";
            $perfat->progresso = 0;
            $perfat->save();
            $processados = 0;

            $sql = "select id_faturamento "
                    . " from faturamento "
                    . " join cliente on cliente.id_cliente = faturamento.id_cliente"
                    . " where id_periodo_faturamento = " . $perfat->id_periodo_faturamento
                    . " and status_demonstrativo = 0 ";
            $sql .= " order by faturamento.id_cliente "
                    . " limit 1";
            $res = \DB::connection('mysql_fat')->getPdo()->query($sql);
            $rs = $res->fetch(\PDO::FETCH_BOTH);

            while ($rs['id_faturamento'] > 0 && $perfat->finalizado == 0) {
                $fat = \Faturamento::find($rs['id_faturamento']);
                $sql = "select count(*) from faturamento"
                        . " join cliente on cliente.id_cliente = faturamento.id_cliente"
                        . " where id_periodo_faturamento = " . $perfat->id_periodo_faturamento ;
                $sql .= " and status_demonstrativo <> 0";
                $res = \DB::connection('mysql_fat')->getPdo()->query($sql);
                $rs = $res->fetch(\PDO::FETCH_BOTH);
                $processados = $rs[0];
                $cliente = \Cliente::find($fat->id_cliente);
                $perfat->status = "Gerando cliente " . $cliente->razaosocial . " (ID " . $fat->id_cliente . ") " . $processados . " de " . $quantos . " (" . intval(($processados / $quantos) * 100) . "%)";
                $perfat->progresso = intval(($processados / $quantos) * 100);
                $perfat->save();
                try {
                    $arq = $rep->demonstrativoAnaliticoEmDisco($fat, false);
                    $fat->status_demonstrativo = 1;
                    $fat->save();
                } catch (Exception $ex) {
                    $fat->status_demonstrativo = -1;
                    $fat->save();
                }
                $perfat->getFresh();
                $sql = "select id_faturamento "
                        . " from faturamento "
                        . " where id_periodo_faturamento = " . $perfat->id_periodo_faturamento
                        . " and status_demonstrativo = 0 "
                        . " order by id_cliente "
                        . " limit 1";
                $res = \DB::connection('mysql_fat')->getPdo()->query($sql);
                $rs = $res->fetch(\PDO::FETCH_BOTH);
            }
            $perfat->status = "Gerando arquivo completo... (aguarde)";
            $perfat->progresso = 100;
            $perfat->save();
            $this->criarDemonstrativoCompleto($perfat, $id_usuario);
            $this->criarDemonstrativoEmail($perfat, $id_usuario);
            $perfat->finalizado = 1;
            $perfat->status = "Faturamento processado com sucesso!";
            $perfat->progresso = 100;
            $perfat->save();
        } catch (Exception $ex) {
            $perfat->finalizado = -1;
            $perfat->status = "Não foi possível finalizar o processamento: " . $ex->getMessage();
            $perfat->progresso = 100;
            $perfat->save();
            \log::error("Não foi possível finalizar o processamento: " . $ex->getMessage()." stack ".$ex->getTraceAsString);
        }
    }

    public function raizPeriodo($perfat) {
        return \Memodoc\Repositorios\RepositorioFaturamentoEloquent::raiz() . $perfat->id_periodo_faturamento . '/';
    }

    public function raizAnalitico($perfat) {
        return $this->raizPeriodo($perfat) . \Memodoc\Repositorios\RepositorioFaturamentoEloquent::ANALITICO;
    }

    public function caminhoArquivoCompleto(\PeriodoFaturamento $perfat) {
        return $this->raizPeriodo($perfat) . 'completo.pdf';
    }

    public function caminhoArquivoZip(\PeriodoFaturamento $perfat) {
        return $this->raizPeriodo($perfat) . $perfat->id_periodo_faturamento.'.pdf';
    }

    public function diretorioOk(\Faturamento $fat) {
        $dir = self::RAIZ . $fat->id_periodo_faturamento . '/' . self::ANALITICO;
        if (!file_exists($dir)) {
            mkdir($dir, 755, true);
        }
    }

    /**
     * Concatena todos os demonstrativos de um faturamento para montar um arquivo só.
     * @param \PeriodoFaturamento $perfat
     * @param type $id_usuario
     */
    public function criarDemonstrativoCompleto(\PeriodoFaturamento $perfat, $id_usuario, $id_tipo_faturamento = "1") {
        $caminho = $this->caminhoArquivoCompleto($perfat);
        \log::info("Gerando no caminho ".$caminho);
        if (file_exists($caminho)) {
            unlink($caminho);
        }
        $sql = "select faturamento.id_cliente"
                . " from faturamento, cliente "
                . " where cliente.id_cliente = faturamento.id_cliente";
        if ($id_tipo_faturamento != ''){
            $sql.=" and cliente.id_tipo_faturamento = ".$id_tipo_faturamento;
        }
        $sql .=   " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento
                . " order by razaosocial";
        $res = \DB::select(\DB::raw($sql));
        $cmd = "pdfunite ";
        foreach ($res as $fat) {
            $cmd .= " " . $this->raizAnalitico($perfat) . $fat->id_cliente . '.pdf';
        }
        $cmd .= ' ' . $caminho;
        \log::info("executando comando  ".$caminho);

        $result = shell_exec($cmd);
        return $result;
    }
    public function criarDemonstrativoEmail(\PeriodoFaturamento $perfat, $id_usuario) {
        $caminho = $this->caminhoArquivoZip($perfat);
        if (file_exists($caminho)) {
            unlink($caminho);
        }
        $sql = "select faturamento.id_cliente"
                . " from faturamento, cliente "
                . " where cliente.id_cliente = faturamento.id_cliente"
                . " and cliente.id_tipo_faturamento = 2"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento
                . " order by razaosocial";
        $res = \DB::select(\DB::raw($sql));
        $cmd = "zip ".$caminho;
        foreach ($res as $fat) {
            $cmd .= " " . $this->raizAnalitico($perfat) . $fat->id_cliente . '.pdf';
        }

        $result = shell_exec($cmd);
        return $result;
    }

    public function excluir(\PeriodoFaturamento $perfat, $id_usuario) {
        \DB::beginTransaction();
        // Liberar as ordens de serviço
        $sql = "update os , faturamento"
                . " set os.id_faturamento = null "
                . "   , id_usuario_alteracao = " . $id_usuario
                . "   , evento = 'Retirado do faturamento " . $perfat->id_periodo_faturamento . "'"
                . " where os.id_faturamento = faturamento.id_faturamento"
                . " and faturamento.id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        \DB::update(\DB::raw($sql));
        // Retira o histórico de preços
        $sql = "delete from hist_tipoprodutocliente where id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        \DB::delete(\DB::raw($sql));
        // Elimina os faturamentos
        $sql = "delete from faturamento where id_periodo_faturamento = " . $perfat->id_periodo_faturamento;
        \DB::delete(\DB::raw($sql));
        $perfat->delete();
        \DB::commit();
    }

    /**
     * Gera o próximo sequencial do período faturamento
     */
    public function proximoSequencial() {
        $seq = \PeriodoFaturamento::where('fl_visivel', '=', 1)->max('sequencial');
        $seq++;
        return $seq;
    }

    /**
     * Limpa os dados do periodofaturamento temporário do usuário
     * @param type $id_usuario
     */
    public function zeraTemporario($id_usuario) {
        $sql = "update os set id_faturamento = null "
             . " where id_faturamento in (select id_faturamento "
                                        . " from faturamento "
                                       . " where id_periodo_faturamento in (select id_periodo_faturamento "
                                                                           . "from periodo_faturamento "
                                                                          . "where id_usuario = " . $id_usuario . " and fl_temporario = 1)"
                                       . ")";
        \DB::update(\DB::raw($sql));
        $sql = "delete from faturamento where id_periodo_faturamento in (select id_periodo_faturamento from periodo_faturamento where id_usuario = " . $id_usuario . " and fl_temporario = 1)";
        \DB::delete(\DB::raw($sql));
        $sql = "delete from periodo_faturamento where id_usuario = " . $id_usuario . " and fl_temporario = 1";
        \DB::delete(\DB::raw($sql));
    }


    /**
     * Limpa os dados do periodofaturamento temporário do usuário
     * @param type $id_usuario
     */
    public function zeraTodosTemporarios() {
        $sql = "update os set id_faturamento = null "
             . " where id_faturamento in (select id_faturamento "
                                        . " from faturamento "
                                       . " where id_periodo_faturamento in (select id_periodo_faturamento "
                                                                           . "from periodo_faturamento "
                                                                          . "where fl_temporario = 1)"
                                       . ")";
        \DB::update(\DB::raw($sql));
        $sql = "delete from faturamento where id_periodo_faturamento in (select id_periodo_faturamento from periodo_faturamento where  fl_temporario = 1)";
        \DB::delete(\DB::raw($sql));
        $sql = "delete from periodo_faturamento where fl_temporario = 1";
        \DB::delete(\DB::raw($sql));
    }

    public function criaTemporario($post, $id_usuario) {
        $per = new \PeriodoFaturamento;
        $this->validacao = new \Memodoc\Validadores\PeriodoFaturamento($post);
        $this->zeraTemporario($id_usuario);
        $id_cliente = $post['id_cliente'];
        if ($this->validacao->passes()) {
            // Verifica se existe outro faturamento no mesmo período
            $inicio = \DateTime::createFromFormat('d/m/Y', $post['dt_inicio_fmt'])->format('Y-m-d');
            $fim = \DateTime::createFromFormat('d/m/Y', $post['dt_fim_fmt'])->format('Y-m-d');
            $where = "((dt_inicio >= '" . $inicio . "' and dt_inicio <= '" . $fim . "') or (dt_fim >= '" . $inicio . "' and dt_fim <= '" . $fim . "')) and fl_temporario = 0";
            $existentes = \PeriodoFaturamento::whereRaw($where)->count();
            if ($existentes > 0) {
                $this->registraErro('Já existe um faturamento englobando o período informado. Verifique.');
                return false;
            } else {
                $per->dt_inicio_fmt = $post['dt_inicio_fmt'];
                $per->dt_fim_fmt = $post['dt_fim_fmt'];
                $per->mes = \DateTime::createFromFormat('Y-m-d', $per->dt_fim)->format('m');
                $per->ano = \DateTime::createFromFormat('Y-m-d', $per->dt_fim)->format('Y');
                $per->id_usuario = $id_usuario;
                $per->fl_temporario = 1;
                $per->finalizado = -1;
                $per->save();

                $this->adicionaCliente($per, $id_cliente, $id_usuario);

                $this->salvaEstruturaPreco($per, $id_usuario);
                $this->recalcularSemZerar($per, $id_usuario);

                $fat = \Faturamento::where('id_periodo_faturamento', '=', $per->id_periodo_faturamento)
                                    ->where('id_cliente', '=', $id_cliente)
                                    ->first();
                return $fat;
            }
        } else {
            return false;
        }
    }

    /**
     * Adiciona um cliente específico ao periodo faturamento
     */
    public function adicionaCliente(\PeriodoFaturamento $perfat, $id_cliente, $id_usuario) {
        $sql = "insert into faturamento (id_periodo_faturamento, id_cliente, id_usuario, created_at, updated_at) "
                . "values (" . $perfat->id_periodo_faturamento . ", ".$id_cliente.",".$id_usuario.", now(), now())";
        print $sql;

        \DB::insert(\DB::raw($sql));
    }

    public function proximaDtInicio(){
        $ult_dt_fim = \DB::table('periodo_faturamento')->where('fl_temporario', '=', 0)->max('dt_fim');
        $dt_inicio = \Carbon::createFromFormat('Y-m-d', $ult_dt_fim);
        $dt_inicio->addDay();
        return $dt_inicio->format('d/m/Y');
    }
    public function proximaDtFim($dt_inicio) {
        $dt_inicio = \Carbon::createFromFormat('d/m/Y', $dt_inicio);
        $dt_fim = $dt_inicio->addMonth();
        $dt_fim = \Carbon::createFromDate($dt_fim->year, $dt_fim->month, '20');
        return $dt_fim->format('d/m/Y');
    }

    /**
     * Lista todas as pendências que possam impedir o faturamento
     * @param  [type] $dt_inicio [description]
     * @param  [type] $dt_fim    [description]
     * @return [type]            [description]
     */
    public function pendencias($dt_inicio, $dt_fim){
        $ret = \DB::table('os')
                    ->where('entregar_em', '>=', $dt_inicio)
                    ->where('entregar_em', "<=", $dt_fim)
                    ->whereIn('os.id_status_os', array(2,3,4) )
                    ->whereRaw(" os.id_os_chave in (select id_os_chave from checkin where dt_checkin is null and id_os_chave = os.id_os_chave) ")
                    ->join('cliente', 'cliente.id_cliente', '=','os.id_cliente')
                    ->join('status_os', 'status_os.id_status_os', '=','os.id_status_os')
                    ->select("os.id_os_chave", "os.id_os", "os.id_cliente", \DB::raw("date_format(os.entregar_em, '%d/%m/%Y') entregar_em"), "cliente.nomefantasia", "cliente.razaosocial", "status_os.descricao")
                    ->orderBy('cliente.razaosocial')
                    ->orderBy('os.id_os')
                    ->get();
        return $ret;
    }
    /**
     * Indica se existem faturamentos pendentes
     */
    public function existemPendentes(){
        $qtd = \PeriodoFaturamento::where('status_conclusao', '=', 1)->count();
        if ($qtd > 0){
            return true;
        } else {
            return false;
        }
    }

    public function abrir($id_periodo_faturamento){
        $perfat = \PeriodoFaturamento::findOrFail($id_periodo_faturamento);
        $perfat->status_conclusao = 1;
        $perfat->save();
    }

    public function fechar($id_periodo_faturamento){
        $perfat = \PeriodoFaturamento::findOrFail($id_periodo_faturamento);
        $perfat->status_conclusao = 2;
        $perfat->save();
    }
    /**
     * Retorna os faturamentos que estão em aberto na data informada
     * @param $data Data no formato Y-m-d
     */
    public function faturamentosFechadosNaData($data){
        $ret = \PeriodoFaturamento::where('status_conclusao', '=', 2)
                    ->where('dt_inicio', '<=', $data)
                    ->where('dt_fim', '>=', $data)
                    ->get();
        return $ret;
    }
}
