<?php

namespace Memodoc\Repositorios;

class RepositorioClienteEloquent  extends Repositorio {

    public function inative(\Cliente $cliente){
        try{
            if(is_object($cliente)){
                $cliente->fl_ativo = 0;
                $cliente->save();
                \Cliente::getFresh();
                return true;
            } else {
                throw new Exception('Objeto recebido não é um cliente');
            }
        } catch (Exception $ex) {
            $this->errors[] = $ex->getMessage();
            return false;
        }
    }

    public function atualizeEstatisticas(){
        $sql = "update cliente set
                qtd_caixas_galpao =  (select count(distinct id_caixa) from documento where id_cliente = cliente.id_cliente and emcasa = 1 )
                , qtd_caixas_reservadas = (select count(distinct id_caixa) from documento where id_cliente = cliente.id_cliente and emcasa = 0 and primeira_entrada is null )
                , qtd_caixas_consulta= (select count(distinct id_caixa) from documento where id_cliente = cliente.id_cliente and emcasa = 0 and primeira_entrada is not null )
                , qtd_caixas_nunca_movimentadas= (select count(distinct id_caixa) from documento where id_cliente = cliente.id_cliente and emcasa = 1 and id_caixa not in (select id_caixa from osdesmembrada where id_cliente = cliente.id_cliente))
                , qtd_caixas_total = (select count(distinct id_caixa) from documento where id_cliente = cliente.id_cliente  )
                ;";
        \DB::update(\DB::raw($sql));
    }
    /**
     * Associa um novo pagador ao cliente, pelo CNPJ informado
     * @param  [type] $id_cliente [description]
     * @param  [type] $cgc        [description]
     * @return [type]             [description]
     */
    public function adiciona_pagador_pelo_cnpj($id_cliente, $cnpj){
        $cnpj = preg_replace('/\D/', '', $cnpj);
        $pagadores = \Pagador::where('id_cliente', '=', $id_cliente)
                        ->where('cnpj', '=', $cnpj)
                        ->get();
        if (count($pagadores)> 0){
            throw new Exception ("Já existe um pagador associado a esse cliente com esse CNPJ");
        } else {
            $pagador = new \Pagador;
            $pagador->id_cliente = $id_cliente;
            $pagador->cnpj = $cnpj;
            $pagador->save();
            return $pagador;
        }
    }
    
    public function atualize_data_ultimo_reajuste($id_cliente){
    	$cliente = \Cliente::find($id_cliente);
    	if (count($cliente) == 0){
    		throw new \ValidacaoException("Cliente não encontrado para o ID informado ".$id_cliente);
    	}
    	$cliente->dt_ultimo_reajuste = \Carbon::now('America/Sao_Paulo')->format("Y-m-d H:i:s");
    	$cliente->save();
    }
	/**
	 * Atualiza o valor mínimo de faturamento do cliente
	 * , armazenando o valor anterior caso seja a primeira vez no mês
	 * 
	 * @param int $mes
	 * @param int $ano
	 * @param int $id_cliente
	 * @param decimal $minimo
	 * @throws \ValidacaoException
	 */
    public function atualize_valor_minimo($mes, $ano, $id_cliente, $minimo){
    	$cliente = \Cliente::find($id_cliente);
    	if (count($cliente) == 0){
    		throw new \ValidacaoException("Cliente não encontrado para o ID informado ".$id_cliente);
    	}
    	if($cliente->mes_ultimo_reajuste() != $mes || $cliente->ano_ultimo_reajuste() != $ano){
    		$cliente->valorminimofaturamento_ant = $cliente->valorminimofaturamento;
    	}
    	$cliente->valorminimofaturamento = $minimo;
    	$cliente->save();
    }
 }