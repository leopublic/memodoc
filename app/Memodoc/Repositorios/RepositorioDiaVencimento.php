<?php
namespace Memodoc\Repositorios;

class RepositorioDiaVencimento extends Repositorio {
    public function lista($filtros = null){
        return \DiaVencimento::orderBy('dia')->get();
    }

}
