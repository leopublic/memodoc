<?php

namespace Memodoc\Repositorios;

use Carbon\Carbon;

class RepositorioUsuarioExternoEloquent extends Repositorio {

    public function edita($post) {
        $id_usuario = $post ['id_usuario'];
        // Inicializa senha 
        if (intval($id_usuario) == 0) {
            if ($post ['senha'] == '') {
                $post ['senha'] = \Usuario::rand_string(8);
                ;
            }
        }
        $this->validacao = $this->inicializaValidacao($post);
        $usuario = $this->instanciaUsuario($post);
        if ($this->validacao->passes()) {
            $usuario->fill($post);
            if ($post ['senha'] != '') {
                if (strlen($post ['senha']) < 8 || strlen($post ['senha']) > 20) {
                    $this->registraErro('A senha deve ter entre 8 e 20 caracteres');
                    return false;
                } else {
                    $usuario->password = \Hash::make($post ['senha']);
                }
            }
            $usuario->save();
            $this->registraErro('Usuário salvo com sucesso!');
            // Atualiza satelites
            if (array_key_exists('id_clientes', $post)) {
                $clientes = $post ['id_clientes'];
            } else {
                $clientes = array();
            }
            if (array_key_exists('id_departamento', $post)) {
                $departamentos = $post ['id_departamento'];
            } else {
                $departamentos = array();
            }
            if (array_key_exists('id_centrodecusto', $post)) {
                $centros = $post ['id_centrodecusto'];
            } else {
                $centros = array();
            }
            if (array_key_exists('id_setor', $post)) {
                $setores = $post ['id_setor'];
            } else {
                $setores = array();
            }
            if (count($clientes) > 0 && (count($departamentos) || count($setores) || count($centros))) {
                $this->registraErro("Quando o usuário tem acesso a mais de um cliente, não é possível especificar departamentos, centros de custo ou setores. Por favor revise as informações.");
            } else {
                $usuario->clientes()->sync($clientes);
                $usuario->departamentos()->sync($departamentos);
                $usuario->centrocustos()->sync($centros);
                $usuario->setores()->sync($setores);
            }
            // Envia notificação se necessario
            if (intval($post ['id_usuario']) == 0 && $usuario->adm == 0) {
                $this->enviarNotificacaoNovoLogin($usuario, $post ['senha']);
                $this->registraErro('Mensagem com senha enviada para ' . $usuario->username . '!');
            }
            return $usuario;
        } else {
            return false;
        }
    }

    public function inicializaValidacao($post) {
        if (intval($post ['id_usuario']) > 0) {
            $validacao = new \Memodoc\Validadores\UsuarioExternoExistente($post);
            $validacao->id_unique = $post ['id_usuario'];
        } else {
            $validacao = new \Memodoc\Validadores\UsuarioExternoNovo($post);
        }
        return $validacao;
    }

    /**
     * Cria usuário para atualização
     * 
     * @param unknown $post        	
     */
    public function instanciaUsuario($post) {
        if ($post ['id_usuario'] > 0) {
            $usuario = \Usuario::find($post ['id_usuario']);
        } else {
            $usuario = new \Usuario ();
            $usuario->id_usuario_cadastro = \Auth::user()->id_usuario;
            $usuario->datacadastro = date('Y-m-d');
            if ($post ['id_cliente'] != '-1') {
                $usuario->id_cliente_principal = $post ['id_cliente'];
                $usuario->id_cliente = $post ['id_cliente'];
                $usuario->adm = 0;
                $usuario->interno = 0;
            } else {
                $usuario->id_cliente_principal = null;
                $usuario->id_cliente = null;
                $usuario->adm = 1;
                $usuario->interno = 0;
            }
        }
        return $usuario;
    }

    public function salvaDoCliente($post) {
        $id_usuario = $post ['id_usuario'];
        if ($id_usuario > 0) {
            $this->validacao = new \Memodoc\Validadores\UsuarioExternoExistente($post);
            $this->validacao->id_unique = $id_usuario;
            $usuario = \Usuario::find($id_usuario);
        } else {
            $this->validacao = new \Memodoc\Validadores\UsuarioExternoNovo($post);
            $usuario = new \Usuario ();
            $usuario->id_usuario_cadastro = \Auth::user()->id_usuario;
            $usuario->datacadastro = date('Y-m-d');
            $usuario->id_cliente_principal = $post ['id_cliente'];
            $usuario->id_cliente = $post ['id_cliente'];
            $usuario->password = \Hash::make($post ['senha']);
            $usuario->adm = 0;
            $usuario->interno = 0;
        }
        if ($this->validacao->passes()) {
            $usuario->fill($post);
            $usuario->save();
            if (array_key_exists('id_departamento', $post)) {
                $departamentos = $post ['id_departamento'];
            } else {
                $departamentos = array();
            }
            if (array_key_exists('id_centrodecusto', $post)) {
                $centros = $post ['id_centrodecusto'];
            } else {
                $centros = array();
            }
            if (array_key_exists('id_setor', $post)) {
                $setores = $post ['id_setor'];
            } else {
                $setores = array();
            }
            $usuario->departamentos()->sync($departamentos);
            $usuario->centrocustos()->sync($centros);
            $usuario->setores()->sync($setores);
            return $usuario;
        } else {
            return false;
        }
    }

    public function exclui($id_usuario) {
        \Usuario::destroy($id_usuario);
    }

    public function enviarNovaSenha(\Usuario $usuario) {
        $this->gerarTokenNovaSenha($usuario);
        $data = array(
            'usuario' => $usuario,
        );

        \Mail::send('emails.link_renova_senha', $data, function ($message) use($usuario) {
            $message->to($usuario->username, $usuario->nomeusuario)->subject('E-Doc: Solicitação de nova senha acesso');
            $message->replyTo(\Config::get('memodoc.email_atendimento'));
        });
    }

    public function notificarNovaSenha(\Usuario $usuario) {
        $data = array(
            'usuario' => $usuario,
        );
        \log::info('email='.$usuario->username);
        \Mail::send('emails.nova_senha', $data, function ($message) use($usuario) {
            $message->to($usuario->username, $usuario->nomeusuario)->subject('E-Doc: Senha de acesso alterada');
            $message->replyTo(\Config::get('memodoc.email_atendimento'));
        });
    }

    public function enviarNotificacaoNovoLogin(\Usuario $usuario, $senha) {
        $data = array(
            'usuario' => $usuario,
            'password' => $senha
        );
        \Mail::send('emails.novo_usuario', $data, function ($message) use($usuario) {
            $message->to($usuario->username, $usuario->nomeusuario)->subject('E-Doc: Confirmação de cadastro de Login para utilização do E-Doc');
            $message->replyTo(\Config::get('memodoc.email_atendimento'));
        });
    }

    public function gerarTokenNovaSenha($usuario){
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%¨&*()"), 0, 30);
        $usuario->token_nova_senha = md5($randomString);
        $usuario->dt_emissao_token_senha = Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s');
        $usuario->save();
    }
}