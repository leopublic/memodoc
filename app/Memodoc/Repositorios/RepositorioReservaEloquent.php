<?php

namespace Memodoc\Repositorios;

class RepositorioReservaEloquent extends Repositorio {
    /**
     * Cancela a reserva bloqueando os números de caixa usados
     */
    public function cancelarReserva(\Reserva $reserva, $id_usuario) {
        \DB::connection()->getPdo()->beginTransaction();
        // Cancelando a reserva
        $reserva->cancelada = '1';
        $reserva->id_usuario_cancelamento = $id_usuario;
        $reserva->data_cancelamento = date('Y-m-d H:i:s');
        $reserva->save();

        // Busco os documentos vinculados
        $documentos = \Documento::where('id_reserva', '=', $reserva->id_reserva)
                ->orderby('id_caixapadrao')
                ->get();

        foreach ($documentos as $d) {
            // Criando novo endereco para disponibilizar
            $novoendereco = $d->endereco->toArray();
            $novoendereco['reservado'] = '';
            $novoendereco['reserva'] = '';
            $novoendereco['disponivel'] = '1';
            unset($novoendereco['id_caixa']);
            unset($novoendereco['created_at']);
            unset($novoendereco['updated_at']);
            \Endereco::create($novoendereco);

            // Cancelando o endereco antigo
            $antigoendereco = $d->endereco;
            $antigoendereco->endcancelado = '1';
            // Tem que registrar quem cancelou...
            $antigoendereco->save();

            $d->delete();
        }
        \DB::connection()->getPdo()->commit();
    }
    /**
     * Exclui a reserva, sem bloquear os números de caixa ou recriar os IDs
     *
     * @param \Reserva $reserva
     * @param [type] $id_usuario
     * @return void
     */
    public function delete_reserva(\Reserva $reserva, $id_usuario){
        \DB::connection()->getPdo()->beginTransaction();
        // Deleta os documentos
        \Documento::where('id_reserva', '=', $reserva->id_reserva)->delete();
        // Libera os endereços usados
        \Endereco::whereRaw('id_caixa in (select id_caixa from reservadesmembrada where id_reserva ='.$reserva->id_reserva.')')
                ->update([
                    'id_cliente' => null
                    , 'reservado' => 0
                    , 'reserva' => 0
                    , 'disponivel' => 1
                ]);
        // Deleta a reserva desmembrada
        \ReservaDesmembrada::where('id_reserva', '=', $reserva->id_reserva)->delete();
        // Deleta a reserva
        $reserva->delete();
        \DB::connection()->getPdo()->commit();
    }


    /**
     *
     * @param type $post
     * @param type $id_usuario
     * @return \Reserva
     */
    public function criar($post, $id_usuario, $com_transacao = true) {
        $this->validacao = new \Memodoc\Validadores\Reserva($post);
        $qtdDisp = \Endereco::get_qtdDisponivel($post['id_tipodocumento']);
        $this->validacao->qtdCaixas($qtdDisp);
        if ($this->validacao->passes()) {
            $reserva = $this->nova($id_usuario);
            $reserva->fill($post);

            $reserva->save();
            if ($com_transacao) {
                \DB::connection()->getPdo()->beginTransaction();
            }
            // Abre um cursor dos endereços disponiveis
            $enderecos = \Endereco::get_cursorDisponiveis($reserva->id_tipodocumento)
                    ->take($reserva->caixas)
                    ->get();
            foreach ($enderecos as $endereco) {
                // Atualiza status do endereco
                $endereco->ReservarPara($reserva);
                $endereco->save();
                // Cria o documento default
                $doc = \DocumentoDefault::Novo($endereco, $reserva);
                $doc->id_caixapadrao = $this->proximoNumeroCaixa($doc->id_cliente);
                $doc->id_usuario_alteracao = $id_usuario;
                $doc->evento = __CLASS__.'.'.__FUNCTION__;
                $doc->save();
                //
                $reservadesmembrada = new \ReservaDesmembrada();
                $reservadesmembrada->id_reserva = $reserva->id_reserva;
                $reservadesmembrada->id_caixa = $doc->id_caixa;
                $reservadesmembrada->id_caixapadrao = $doc->id_caixapadrao;
                $reservadesmembrada->save();
            }
            if ($com_transacao) {
                \DB::connection()->getPdo()->commit();
            }
            return $reserva;
        } else {
            return false;
        }
    }

    /**
     * Retorna o próximo número da próxima caixa que será reservada para um cliente
     * @param type $id_cliente
     * @return integer
     */
    public function proximoNumeroCaixa($id_cliente) {
        $doc = new \Documento;
        $reserva = new \Reserva;
        $numero_caixa = $doc->getMaiorCaixaNaoCancelada($id_cliente);
        $numero_reservado = $reserva->getUltimaCaixa($id_cliente);
        if ($numero_caixa > $numero_reservado) {
            $proxima = $numero_caixa;
        } else {
            $proxima = $numero_reservado;
        }
        $proxima = intval($proxima + 1);
        return $proxima;
    }

    public function gerarPlanilha(\Reserva $reserva) {
        
    }

    public function nova($id_usuario) {
        $reserva = new \Reserva();
        $reserva->data_reserva = date('Y-m-d');
        $reserva->cancelada = 0;
        $reserva->id_usuario_alteracao = $id_usuario;
        return $reserva;
    }

}
