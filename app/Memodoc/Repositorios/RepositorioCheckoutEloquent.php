<?php

namespace Memodoc\Repositorios;

class RepositorioCheckoutEloquent extends Repositorio{

    protected $repCaixa;
    protected $repOs;

    public $validacao;

    /**
     * Retorna os checkouts do dia
     * @param type $data
     */
    public function doDia($data){
        // Todos os checkouts de OS que estão associados a OS desmembrada
        $sql = "select 'os' as tipo"
                . ", c.razaosocial"
                . ", c.id_cliente"
                . ", c.nomefantasia"
                . ", os.id_os"
                . ", os.id_os_chave"
                . ", osd.id_osdesmembrada"
                . ", os.entregar_em"
                . ", date_format(os.entregar_em, '%d/%m/%Y') entregar_em_fmt"
                . ", osd.id_caixa, d.id_caixapadrao"
                . ", if(osd.data_checkout is null, 0, 1) data_checkout_ordem"
                . ",  data_checkout"
                . ", uck.nomeusuario"
                . ", if( date_format(entregar_em , '%Y%m%d') < date_format(now(), '%Y%m%d'), 0, 1) ordem"
                . " from osdesmembrada osd"
                . " join os on os.id_os_chave = osd.id_os_chave"
                . " join cliente c on c.id_cliente = os.id_cliente"
                . " join (select distinct id_caixa, id_caixapadrao from documento ) d on d.id_caixa = osd.id_caixa"
                . " left join usuario uck on uck.id_usuario = osd.id_usuario_checkout"
                . " where (osd.status < 1 "
                . "         or (osd.status = 1 and osd.data_checkout >= '".$data." 00:00:00' and osd.data_checkout <='".$data." 23:59:59') )"
                . "  and os.id_status_os > 1 "
                . "  and os.id_status_os <> 5 ";
        $sql .= " union all select 'exp' as tipo"
                . ", c.razaosocial"
                . ", '' id_os"
                . ", 'expurgo' id_os_chave"
                . ", null, e.id_expurgo"
                . ", date_format(e.data_solicitacao, '%d/%m/%Y') entregar_em_fmt"
                . ", e.id_caixa"
                . ", e.id_caixapadrao"
                . ", if(e.data_checkout is null, 0,1) data_checkout_ordem"
                . ", data_checkout"
                . ", uck.nomeusuario"
                . ", 0 ordem"
                . " from expurgo e"
                . " join cliente c on c.id_cliente = e.id_cliente"
                . " left join usuario uck on uck.id_usuario = e.id_usuario_checkout"
                . " where e.data_checkout is null or date_format(e.data_checkout, '%d/%m/%Y') = '".date('d/m/Y')."'"
                . " order by tipo desc, data_checkout_ordem , ordem, id_os_chave";
        $res = \DB::getPdo()->query($sql);
        $x = $res->fetchAll(\PDO::FETCH_BOTH);
        return $x;
    }
    /**
     * Retorna os checkouts do dia
     * @param type $data
     */
    public function doDiaEnviadas($data){
        // Todos os checkouts de OS que estão associados a OS desmembrada
        $sql = "select distinct "
                . " c.razaosocial"
                . ", os.id_os"
                . ", os.id_os_chave"
                . ", osd.id_osdesmembrada"
                . ", os.entregar_em"
                . ", date_format(os.entregar_em, '%d/%m/%Y') entregar_em_fmt"
                . ", osd.id_caixa"
                . ", d.id_caixapadrao"
                . ", if(osd.data_checkout is null, 0, 1) data_checkout_ordem"
                . ",  data_checkout"
                . ", uck.nomeusuario"
                . ", if( date_format(entregar_em , '%Y%m%d') < date_format(now(), '%Y%m%d'), 0, 1) ordem"
                . " from osdesmembrada osd"
                . " join os on os.id_os_chave = osd.id_os_chave"
                . " join cliente c on c.id_cliente = os.id_cliente"
                . " join documento d on d.id_caixa = osd.id_caixa"
                . " left join usuario uck on uck.id_usuario = osd.id_usuario_checkout"
                . " where (osd.status < 1 "
                . "         or (osd.status = 1 and osd.data_checkout >= '".$data." 00:00:00' and osd.data_checkout <='".$data." 23:59:59') )"
                . "  and os.id_status_os > 1 "
                . "  and os.id_status_os <> 5 "
                . " order by  data_checkout_ordem , ordem, id_os_chave , id_caixapadrao";
        $res = \DB::getPdo()->query($sql);
        $x = $res->fetchAll(\PDO::FETCH_BOTH);
        return $x;
    }
    /**
     * Retorna os checkouts do dia
     * @param type $data
     */
    public function doDiaExpurgadas($data){
        // Todos os checkouts de OS que estão associados a OS desmembrada
        $sql = " select c.razaosocial"
                . ", c.id_cliente"
                . ", c.nomefantasia"
                . ", e.id_expurgo"
                . ", e.id_expurgo_pai"
                . ", date_format(e.data_solicitacao, '%d/%m/%Y') entregar_em_fmt"
                . ", e.id_caixa"
                . ", e.id_caixapadrao"
                . ", if(e.data_checkout is null, 0,1) data_checkout_ordem"
                . ", data_checkout"
                . ", uck.nomeusuario"
                . ", 0 ordem"
                . " from expurgo e"
                . " join cliente c on c.id_cliente = e.id_cliente"
                . " join expurgo_pai ep on ep.id_expurgo_pai = e.id_expurgo_pai"
                . " left join usuario uck on uck.id_usuario = e.id_usuario_checkout"
                . " where (e.data_checkout is null or date_format(e.data_checkout, '%d/%m/%Y') = '".date('d/m/Y')."' )"
                . " and coalesce(ep.id_transbordo, 0) = 0"
                . " order by data_checkout_ordem , id_expurgo_pai desc, id_caixapadrao";
        $res = \DB::getPdo()->query($sql);
        $x = $res->fetchAll(\PDO::FETCH_BOTH);
        return $x;
    }
    public function qtdExpurgosPendentes(){
        // Todos os checkouts de OS que estão associados a OS desmembrada
        return \Expurgo::whereNull('data_checkout')->count();
    }

    public function cancela(\Checkout $checkout, $id_usuario, $observacao){
        $checkout->id_usuario_cancelamento = $id_usuario;
        $checkout->observacao_cancelamento = $observacao;
        $checkout->dt_cancelamento = date('Y-m-d H:i:s');
        $checkout->save();
        return true;
    }
}
