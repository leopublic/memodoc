<?php

namespace Memodoc\Repositorios;

interface RepositorioCheckinInterface {
    public function adicionaCaixa($id_caixapadrao, $id_os_chave);
    public function removeCaixa($id_caixa, $id_os_chave);
    public function recuperaOs($id_caixa);
}
