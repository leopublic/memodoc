<?php

namespace Memodoc\Repositorios;

class RepositorioFaturamentoEloquent extends Repositorio {

    const RAIZ = '/arquivos/faturamento/';
    const ANALITICO = 'analitico/';

    public function removeDemonstrativoExistente(\Faturamento $fat) {
        if (file_exists($this->caminhoArquivo($fat))) {
            unlink($this->caminhoArquivo($fat));
        }
    }

    public static function raiz(){
        return base_path().self::RAIZ;
    }

    public function caminhoArquivo(\Faturamento $fat) {
        return self::raiz() . $fat->id_periodo_faturamento . '/'. self::ANALITICO . $fat->id_cliente . '.pdf';
    }

    public function diretorioOk(\Faturamento $fat) {
        $dir = self::raiz() . $fat->id_periodo_faturamento . '/'. self::ANALITICO;
        if (!file_exists($dir)) {
            mkdir($dir, 755, true);
        }
    }

    public function demonstrativoAnaliticoEmDisco(\Faturamento $fat, $retorneExistente = false) {
        if ($retorneExistente && file_exists($this->caminhoArquivo($fat))) {
            return $this->caminhoArquivo($fat);
        } else {
            $this->diretorioOk($fat);
            $this->removeDemonstrativoExistente($fat);
            $mpdf = $this->demonstrativoAnalitico($fat);
            $mpdf->Output($this->caminhoArquivo($fat), 'F');
        }
        return $this->caminhoArquivo($fat);
    }

    /**
     * Monta o mpdf com o demonstrativo
     * @param \Faturamento $faturamento
     * @return mpdf
     */
    public function demonstrativoAnalitico(\Faturamento $faturamento) {

        $this->mpdf = new \mPDF('utf-8', 'A4-L', '11px', 'Arial', '10', '10', '30', '20', '10', '10');  // largura x altura
        $this->mpdf->allow_charset_conversion = false;
        $this->mpdf->debug = true;

        // Monta o cabeçalho
        $cliente = \Cliente::find($faturamento->id_cliente);

        $data = $faturamento->formatDate('dt_recalculo', 'd/m/Y');
        $hora = $faturamento->formatDate('dt_recalculo', 'H:i:s');

        $html = \View::make('faturamento.analitico_cabecalho')
                ->with('faturamento', $faturamento)
                ->with('data', $data)
                ->with('hora', $hora)
        ;
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $this->mpdf->setHTMLHeader($html);

        // Monta do rodapé
        $this->mpdf->setFooter("||Página {PAGENO}/{nb}");

        // Monta a primeira parte do relatório
        $html = \View::make('faturamento.analitico_inicio')
                ->with('cliente', $cliente)
                ->with('faturamento', $faturamento)
        ;
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $this->mpdf->WriteHTML($html);

        // Monta as OS
//        $oss = \Os::where('id_faturamento', '=', $faturamento->id_faturamento)->orderBy('id_os')->get();
        $sql = "select *, concat(date_format(entregar_em, '%Y%m'), substr(concat('000000', id_os), -6)) numeroFormatado"
                . " , (select count(*) from osdesmembrada where id_os_chave = os.id_os_chave) qtdcaixasconsulta "
                . " , date_format(entregar_em, '%d/%m/%Y') entregar_em_fmt"
                . " from os "
                . " where id_faturamento = ".$faturamento->id_faturamento. " order by solicitado_em";

        $res = \DB::connection('mysql_fat')->getPdo()->query($sql);
        while($os = $res->fetch(\PDO::FETCH_BOTH)){
            $html = \View::make('faturamento.analitico_os')
                    ->with('os', $os)
            ;
            $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
            $this->mpdf->WriteHTML($html);
        }

        $html = \View::make('faturamento.analitico_fim')
                ->with('cliente', $cliente)
                ->with('faturamento', $faturamento)
                ->with('obs_desconto', $cliente->obsdesconto)
        ;
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $this->mpdf->WriteHTML($html);
        return $this->mpdf;
    }

    public function excluir(\Faturamento $fat) {
        $this->desvinculaOrdensDeServico($fat);
        $this->removeDemonstrativoExistente($fat);
        $fat->delete();
    }

    public function desvinculaOrdensDeServico(\Faturamento $fat){
        \Os::where('id_faturamento', '=', $fat->id_faturamento)
                   ->update(array('id_faturamento' => null));
    }

    public function conferir($id_faturamento){
        $fat = \Faturamento::find($id_faturamento);
        $fat->conferido = 1;
        $fat->save();
    }

    public function desconferir($id_faturamento){
        $fat = \Faturamento::find($id_faturamento);
        $fat->conferido = 0;
        $fat->save();
    }
}
