<?php
namespace Memodoc\Repositorios;

use \Carbon;

class RepositorioImportadorCsv extends Repositorio
{

    public $validacao;

    public $objeto_arquivo;
    public $id_objeto;

    public function instancia_objeto_arquivo($id){
        throw new \Exception('Instancia objeto arquivo não implementado '.__CLASS__);
    }

    public function caminho_arquivo(){
        throw new \Exception('Caminho do arquivo não implementado '.__CLASS__);
    }

    public function resseta_registros_importados(){
        throw new \Exception('Resseta registros importados não implementado '.__CLASS__);
    }

    public function posicoes_header(){
        return [];
    }

    public function posicoes_detalhe(){
        throw new \Exception('Posicções detalhe não implementado '.__CLASS__);
    }

    public function separador_campos(){
        return ",";
    }

    public function processa_arquivo($id_consulta_boletos){
        $this->instancia_objeto_arquivo($id);
        $arq = fopen($this->caminho_arquivo(), 'r');

        $this->resseta_registros_importados();

        $ordem = 0;
        $tipo = '';
        
        $pos_header = $this->posicoes_header();
        $pos_det = $this->posicoes_detalhe();


        while ($linha = fgets($arq)) {
            $ordem++;
            $campos = explode(",", $linha);
            $razaosocial = trim($campos[0]);
            if ($razaosocial == 'Tipo Consulta:'){
                $status = $campos[4];
            }

            if($razaosocial == 'Sacado'){
                $i_nosso_numero = array_search('Nosso Número', $campos);
                $i_seu_numero = array_search('Seu Número', $campos);
                $i_dt_entrada = array_search('Entrada', $campos);
                $i_dt_vencimento = array_search('Vencimento', $campos);
                $i_valor = array_search('Valor', $campos);
            }

            if($razaosocial != '' && $razaosocial != 'Cedente:' && $razaosocial != 'Tipo Consulta:' && $razaosocial != 'Conta Corrente:' && $razaosocial != 'Sacado' && $razaosocial != 'Ordenado por:' && $razaosocial != 'Gerado em:'){
                
                $reg = new \RegistroConsultaBoletos;
                $reg->id_consulta_boletos = $id_consulta_boletos;
                $reg->registro = $linha;
                $reg->ordem = $ordem;
                $reg->status = $status;
                $reg->razaosocial = $campos[0];
                $reg->nosso_numero = $campos[$i_nosso_numero];
                $reg->seu_numero = $campos[$i_seu_numero];

                if(trim($campos[$i_dt_entrada]) != ''){
                    try{
                        $data = Carbon::createFromFormat('d/m/Y', $campos[$i_dt_entrada])->format('Y-m-d');
                        $reg->dt_entrada = $data;
                    } catch(\Exception $e){

                    }
                }

                if(trim($campos[$i_dt_vencimento]) != ''){
                    try{
                        $data = Carbon::createFromFormat('d/m/Y', $campos[$i_dt_vencimento])->format('Y-m-d');
                        $reg->dt_vencimento = $data;
                    } catch(\Exception $e){

                    }
                }

                $cliente = \Cliente::where('razaosocial', 'like', $razaosocial.'%')->get();
                if (count($cliente) == 1){
                    $reg->id_cliente = $cliente[0]->id_cliente;
                }
                //Valor
                $xvalor = str_replace(".", "", str_replace('"', "", $campos[$i_valor])).".".str_replace('"', "", $campos[$i_valor+1]);
                $reg->valor = $xvalor;
                $reg->save();
        
            }

        }
        fclose($arq);

    }
    /**
     * Carrega os campos conforme informacoes do array
     *
     * Array:
     *  nome_campo => ["tipo" => "T"/"D"/"V_EN"/"V_BR" , "ordem" => indice , "formato_dt" => formato da data dmy, d/m/Y, etc.]
     * @param [type] $obj
     * @param [type] $linha
     * @param [type] $campos
     * @return void
     */
    public function carrega_campos_array($obj, $linha, $campos){
        foreach($campos as $nome_campo => $specs){
            switch($specs['tipo']){
                case "T":
                    $obj->$nome_campo = $this->$linha[$specs['ordem']];
                    break;
                case "D":
                    $obj->$nome_campo = $this->formata_data($linha[$specs['ordem']], $specs['formato_dt']);
                    break;
                case "V_EN":
                    $obj->$nome_campo = str_replace(".", "", str_replace('"', "", $campos[$i_valor]));
                    break;
                case "V_BR":
                    $obj->$nome_campo = str_replace(",", ".", str_replace(".", "", str_replace('"', "", $campos[$i_valor])));
                    break;
            }
        }
        return $obj;
    }

    public function formata_data($valor, $fmt_in = 'dmy'){
        if(trim($valor) != ''){
            try{
                return Carbon::createFromFormat($fmt_in, $valor)->format('Y-m-d');
            } catch(\Exception $e){
                return null;
            }
        }

    }


}
