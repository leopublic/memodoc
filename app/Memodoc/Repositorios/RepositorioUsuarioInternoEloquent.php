<?php

namespace Memodoc\Repositorios;

class RepositorioUsuarioInternoEloquent extends Repositorio{

    public function cria($post){
        $this->validacao = new \Memodoc\Validadores\UsuarioInternoNovo($post);
        if ($this->validacao->passes()) {
            $usuario = new \Usuario;
            $usuario->fill($post);
            $usuario->fl_primeira_vez = 1;
            $usuario->datacadastro = date('Y-m-d');
            $usuario->save();
            
            return true;
        } else {
            return false;
        }
    }

    public function edita($post){
        $id_usuario = $post['id_usuario'];
        $usuario = \Usuario::find($id_usuario);
        $this->validacao = new \Memodoc\Validadores\UsuarioInternoExistente($post);
        $this->validacao->id_unique = $id_usuario;
        if ($this->validacao->passes()) {
            $usuario->fill($post);
            if (array_key_exists('senha', $post) && $post['senha'] != ''){
                $usuario->password = \Hash::make($post['senha']);
            }
            $usuario->save();
            return true;
        } else {
            return false;
        }
    }

    public function exclui($id_usuario){
        \Usuario::destroy($id_usuario);
    }
}

