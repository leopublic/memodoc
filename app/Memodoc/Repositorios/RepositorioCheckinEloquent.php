<?php
namespace Memodoc\Repositorios;

class RepositorioCheckinEloquent extends Repositorio
{

    protected $repCaixa;
    protected $repOs;

    public $validacao;

    /**
     * Adiciona uma caixa à uma OS
     * @param type $id_caixapadrao Número da caix
     * @param type $id_os_chave
     *
     * Exceções
     * - A caixa deve existir
     * - A caixa deve estar emprestada
     * - A OS não pode estar nem cancelada nem concluída
     */
    public function adicionaCaixa($id_os_chave, $id_caixapadrao, $id_caixapadrao_fim) {
        $repDoc = new \Memodoc\Repositorios\RepositorioDocumentoEloquent;
        $conteudo = '';
        $ret = array();
        try {
            //
            // Obtem a ordem de serviço
            $os = \Os::find($id_os_chave);
            //
            // Obtem as caixas que foram solicitadas
            $id_cliente = $os->id_cliente;
            $docs = $repDoc->caixasEmIntervalo($id_cliente, $id_caixapadrao, $id_caixapadrao_fim);
            foreach ($docs as $doc) {
                if (intval($doc->id_caixa > 0)) {
                    $checkin = \Checkin::where('id_os_chave', '=', $id_os_chave)
                                ->where('id_caixa', '=', $doc->id_caixa)
                                ->first();
                    if (is_object($checkin)) {
                        $checkin->msg = 'Essa referência já foi adicionada na OS anteriormente. Inclusão ignorada.';
                        $checkin->classe = 'error';
                        $checkin->exibirConfirmacao = false;
                    }
                    else {
                        if ($doc->emcasa == 1) {
                            $checkin = new \Checkin();
                            $checkin->classe = 'warning';
                            $checkin->msg = 'Caixa não adicionada pois consta no galpão. Para incluir clique nesse botão: ';
                            $checkin->exibirConfirmacao = true;
                        }
                        else {
                            $checkin = $this->criar($os, $doc);
                            $checkin->exibirConfirmacao = false;
                            if ($doc->primeira_entrada == '') {
                                $msg = 'caixa nova (primeira entrada)';
                            } else {
                                $msg = 'retorno de empréstimo';
                            }
                            $checkin->msg = $msg;
                            $checkin->classe = 'success';
                        }
                    }
                }
                else {
                    $checkin = new \Checkin;
                    $checkin->classe = 'error';
                    $checkin->msg = 'Referência não encontrada';
                }
                $data = array('os' => $os, 'checkin' => $checkin, 'doc' => $doc);
                $saida = \View::make('os.checkin_novo', $data);
                $conteudo.= $saida;
            }
        }
        catch(Exception $e) {
            $conteudo = '<tr><td colspan="7">Não foi possível executar a operação: '.$ex->getMessage().'</td></tr>';
        }

        //
        // Atualiza os totais recebidos
        $os->evento = 'Inclusão de recebimento de caixa';
        $os->id_usuario_alteracao = \Auth::user()->id_usuario;
        $os->atualizarTotaisRecebidos();
        $os->save();
        $ret['conteudo'] = $conteudo;
        return $ret;
    }

    /**
     * Confirma a inclusão de uma caixa mesmo que ela esteja no galpão
     * @return [type] [description]
     */
    public function adicionaCaixaConfirmada($id_os_chave, $id_caixapadrao) {
        $conteudo = '';
        $ret = array();
        try {
            $os = \Os::find($id_os_chave);
            $doc = \Documento::where('id_cliente', '=', $os->id_cliente)
                    ->where('id_caixapadrao', '=', $id_caixapadrao)
                    ->first();

            $checkin = \Checkin::where('id_os_chave', '=', $id_os_chave)
                        ->where('id_caixa', '=', $doc->id_caixa)
                        ->first();
            if (is_object($checkin)){
                $checkin->msg = 'Essa referência já foi adicionada na OS anteriormente. Inclusão ignorada.';
                $checkin->classe = 'error';
            } else {
                $checkin = $this->criar($os, $doc);
                $checkin->msg = 'Caixa adicionada, apesar de constar no galpão (inclusão confirmada pelo operador)';
                $checkin->classe = 'success';
                $checkin->exibirConfirmacao = false;

                $os->evento = 'Inclusão de recebimento de caixa';
                $os->id_usuario_alteracao = \Auth::user()->id_usuario;
                $os->atualizarTotaisRecebidos();
                $os->save();

            }

            $data = array(
                'os' => $os,
                'checkin' => $checkin,
                'doc' => $doc
            );
            $conteudo .= \View::make('os.checkin_novo', $data);
        }
        catch(Exception $e) {
            $conteudo = '<tr><td colspan="7">Não foi possível executar a operação: '.$ex->getMessage().'</td></tr>';
        }
        $ret['conteudo'] = $conteudo;
        return $ret;
    }

    /**
     * Cria checkin
     * @param  \Os $os  [description]
     * @param  \Documento $doc [description]
     * @return string      [description]
     */
    public function criar($os, $doc){
        $checkin = new \Checkin();
        $checkin->id_os_chave = $os->id_os_chave;
        $checkin->id_caixa = $doc->id_caixa;
        $checkin->id_cliente = $os->id_cliente;
        $checkin->id_usuario = \Auth::user()->id_usuario;
        $checkin->primeira_entrada = $doc->primeira_entrada;
        $checkin->emcasa = $doc->emcasa;
        if ($doc->primeira_entrada == '') {
            $checkin->fl_primeira_entrada = 1;
        }
        $checkin->save();
        $repOs = new \Memodoc\Repositorios\RepositorioOsEloquent;
        $repOs->registraEvento($os, 'Incluído o recebimento da caixa ' . $doc->id_caixapadrao_fmt, $doc->id_caixa_fmt);
        return $checkin;
    }


    /**
     * Remove uma caixa do checkin de uma OS
     * @param type $id_caixa
     * @param type $id_os_chave
     *
     * Exceções:
     * - A caixa não pode ter sido recebida ainda
     * - A caixa deve estar no checkin da OS
     * - A OS não pode estar nem cancelada nem concluída
     */
    public function removeCaixa($id_caixa, $id_os_chave) {
    }

    /**
     * Retorna o id_os_chave da OS que tem um checkin pendente para essa caixa
     * @param type $id_caixa
     * @return id_os_caixa quando houver uma OS ou nulo quando não houver os.
     *
     * Exceções: nenhuma.
     */
    public function recuperaOs($id_caixa) {
    }

    public function pendentes() {
        $caixas = \DB::table('checkin')
                    ->leftjoin('os', 'os.id_os_chave', '=', 'checkin.id_os_chave')
                    ->leftjoin('transbordo', 'transbordo.id_transbordo', '=', 'checkin.id_transbordo')
                    ->leftjoin('documento', 'documento.id_caixa', '=', 'checkin.id_caixa')
                    ->join('cliente', 'cliente.id_cliente', '=', 'checkin.id_cliente')
                    ->whereNull('dt_checkin')
                    ->select(
                        array(
                            'checkin.id_checkin'
                            , 'checkin.dt_checkin'
                            , 'checkin.id_cliente'
                            , 'checkin.id_caixa'
                            , 'checkin.id_os_chave'
                            , 'checkin.id_transbordo'
                            , "documento.id_caixapadrao"
                            , \DB::raw("substr(concat('000000',documento.id_caixapadrao), -6) id_caixapadrao_fmt ")
                            , 'cliente.razaosocial'
                            , 'cliente.nomefantasia'
                            , 'os.id_os'
                            , 'os.entregar_em'
                            , \DB::raw("date_format(entregar_em, '%d/%m/%Y') entregar_em_fmt")
                            , \DB::raw("date_format(entregar_em, '%Y-%m-%d') entregar_em_Ymd")
                        )
                    )
                    ->distinct()
                    ->orderBy('entregar_em')
                    ->get();
        return $caixas;
    }
}
