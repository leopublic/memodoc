<?php
namespace Memodoc\Repositorios;

interface RepositorioGalpaoInterface{
    public function getErrors();
    public function CheckinDisponivel($id_caixa);
    public function emprestaCaixa($id_caixa, $id_usuario);
    public function retiraCaixaSemOs($id_caixa, $id_usuario);
}