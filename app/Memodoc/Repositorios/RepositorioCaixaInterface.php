<?php
interface RepositorioCaixaInterface{
    public function primeiroDocumento($id_caixa);
    public function primeiroDocumentoPeloNumero($id_cliente, $id_caixapadrao);
    public function documentos();
}