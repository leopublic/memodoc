<?php

namespace Memodoc\Repositorios;

class RepositorioCustodiaEloquent extends Repositorio implements RepositorioCustodiaInterface {

    protected $cliente;

    public function __construct($cliente) {
        $this->cliente = $cliente;
    }

    public function getErrors() {
        return $this->errors;
    }

    /**
     * Recupera a quantidade de caixas contratadas de um determinado cliente
     * para um determinado perídodo
     */
    public function qtdCustodiaPeriodo($dt_inicio, $dt_fim) {

    }

    /**
     * Recupera a quantidade de caixas contratadas que podem ser cobradas em um
     * determinado período
     *
     * @param type $cliente
     * @param type $dt_inicio
     * @param type $dt_fim
     */
    public function qtdCustodiaFaturavelPeriodo($dt_inicio, $dt_fim) {

    }

    public function qtdExpurgoPeriodo($dt_inicio, $dt_fim) {

    }

    /**
     * Indica a quantidade de caixas que foram retornadas de consulta no período
     * @param type $dt_inicio
     * @param type $dt_fim
     */
    public function qtdRetornoConsulta($dt_inicio, $dt_fim) {

    }

}
