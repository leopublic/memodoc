<?php
namespace Memodoc\Repositorios;

class RepositorioRemessa extends Repositorio {

    public function cria_remessa($id_cliente, $dt_inicial = '', $dt_final = '', $qtd_itens = 10){
        $remessa = new \Remessa;
        $remessa->id_cliente = $id_cliente;
        if ($dt_inicial != ''){
            $remessa->dt_inicial = $dt_inicial;
        }
        if ($dt_final != ''){
            $remessa->dt_final = $dt_final;
        }
        $remessa->save();

        for($i=1;$i<=$qtd_itens;$i++){
            $item = new \ItemRemessa;
            $item->id_remessa = $remessa->id_remessa;
            $item->codigo = 11111+$i;
            $item->ordem = $i;
            $item->campo1 = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,10);
            $item->campo2 = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,10);
            $item->save();
        }
    }

}
