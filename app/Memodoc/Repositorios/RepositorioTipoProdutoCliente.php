<?php
namespace Memodoc\Repositorios;

class RepositorioTipoProdutoCliente extends Repositorio
{

    protected $repCaixa;
    protected $repOs;

    public $validacao;

    
    
    public function atualizar_preco($mes, $ano, $id_cliente, $id_tipoproduto, $valor_contrato, $valor_urgencia, $id_usuario_alteracao){
    	$tipoprodcliente = \TipoProdCliente::dotipo($id_tipoproduto)->docliente($id_cliente)->first();
    	$cliente = \Cliente::find($id_cliente);
    	if (count($tipoprodcliente) > 0){
    		$atualiza = array();
    		if($cliente->mes_ultimo_reajuste() != $mes || $cliente->ano_ultimo_reajuste() != $ano){
    			$tipoprodcliente->salva_valores();
    			$atualiza['valor_contrato_ant'] = $tipoprodcliente->valor_contrato;
    			$atualiza['valor_urgencia_ant'] = $tipoprodcliente->valor_urgencia;
    		}
    		$atualiza['valor_contrato'] = $valor_contrato;
    		$atualiza['valor_urgencia'] = $valor_urgencia;
    		$atualiza['id_usuario_alteracao'] = $id_usuario_alteracao;
    		\TipoProdCliente::dotipo($id_tipoproduto)->docliente($id_cliente)->update($atualiza);
    	}
    }
    
}
