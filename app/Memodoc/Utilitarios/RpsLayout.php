<?php
/**
 * Gera os arquivos para importação da nota carioca
 */

namespace Memodoc\Utilitarios;

class RpsLayout {
    public $tipo_registro;              
    public $campos;

    public function __construct(){
        $this->monta_campos();
        $this->carrega_valores_array($this->valores_default());        
    }

    public function monta_campos(){

    }
    public function valores_default(){
        return [];
    }

    public function CR_LF(){
        return "\r"."\n";
    }

    public function numerico($campo, $tamanho){
        return substr(str_repeat("0", $tamanho).preg_replace("/\D/", "", $campo), -$tamanho);
    }

    public function campo_formatado($chave){
        $campo = $this->campos[$chave];
        switch($campo['tipo']){
            case 'N':
                return substr(str_repeat("0", $campo['tamanho']).preg_replace("/\D/", "", $campo['valor']), - $campo['tamanho']);
                break;
            case 'V':
                $x = round($campo['valor'], 2);
                $x = number_format($x , 2);
                $x = preg_replace("/[^0-9]/", "", $x );
                $x = str_repeat("0", $campo['tamanho']).$x;
                $x = substr($x , - $campo['tamanho']);
                return $x;
                break;
            case 'D':
                return $campo['valor'];
                break;
            case 'T':
                $valor = $campo['valor'];                
                $valor = $valor;
                $valor = $valor.str_repeat(" ", $campo['tamanho']);
                return  substr($valor, 0, $campo['tamanho']);
                break;
        }
    }

    public function carrega_valores_array($campos){
        foreach($campos as $campo => $valor){
            $this->atualiza_campo($campo, $valor);
        }
        
    }

    public function atualiza_campo($nome_campo, $valor){
        if(array_key_exists($nome_campo, $this->campos)){
            $campo = $this->campos[$nome_campo];
            switch($campo['tipo']){
                case 'N':
                case 'V':
                    $this->campos[$nome_campo]['valor'] = $valor;
                    break;
                case 'D':
                    try{
                        $data = \Carbon::createFromFormat('Y-m-d', substr($valor, 0 , 10), 'America/Sao_Paulo');
                        $valor_ok = $data->format('Ymd');
                    } catch(\Exception $e){
                        $valor_ok = '        ';
                    }
                    $this->campos[$nome_campo]['valor'] = $valor_ok;
                    break;
                case 'T':
                    $this->campos[$nome_campo]['valor'] = substr(utf8_decode($valor).str_repeat(" ", $campo['tamanho']), 0, $campo['tamanho']);
                    break;
            }
        }
    }
 
    public function registro(){
        $reg = "";
        $campos = $this->campos;

        foreach($campos as $chave => $campo){
            $cmp = $this->campo_formatado($chave);
            $reg .= $cmp;
        }
        $reg .= "\r"."\n";
        return $reg;
        
    }   
}