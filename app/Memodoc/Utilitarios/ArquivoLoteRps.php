<?php

/**
 * Gera os arquivos para importação da nota carioca
 * 
 */

namespace Memodoc\Utilitarios;

class ArquivoLoteRps {

    public $lote_rps;

    public function __construct(\LoteRps $lote_rps = null){
        if (isset($lote_rps)){
            $this->lote_rps = $lote_rps;
        }
    }

    public function caminho(){
        // Gera o caminho do arquivo pelo id do lote.
        $ret = app_path()."/arquivos/rps/";
        if(!is_dir($ret)){
            mkdir($ret, 0775, true);
        }
        return $ret;
    }

    public function nome(){
        return substr('00000000'.$this->lote_rps->id_lote_rps, -8).".txt";
    }
    
    public function gerar(){
        $caminho = $this->caminho();
        // Obtem cliente prestador
        $cliente = \Cliente::memodoc()->first();
        // Abre cursor de notas
        $notas = \NotaFiscal::dolote($this->lote_rps->id_lote_rps)
        ->leftjoin('faturamento', 'faturamento.id_faturamento', '=', 'nota_fiscal.id_faturamento')
        ->leftjoin('cliente', 'cliente.id_cliente', '=', \DB::raw('coalesce(nota_fiscal.id_cliente, faturamento.id_cliente)'))
        ->leftjoin('tipo_faturamento', 'tipo_faturamento.id_tipo_faturamento', '=', 'cliente.id_tipo_faturamento')
        ->orderBy('cliente.razaosocial')
        ->get();

        $caminho = $this->caminho().$this->nome();
        // Abre stream
        try{
            unlink($caminho);
        } catch(\Exception $e){

        }
        // $myfile = fopen($caminho, "wb");
        // Grava cabecalho
        $arq = "";
        $reg = new RpsLayoutTipo10();
        $reg->carrega_valores_array($this->lote_rps->valores_em_array());
        $reg->carrega_valores_array($cliente->valores_em_array_para_nota());
        $registro = $reg->registro();
        // fwrite($myfile, $registro);
        $arq .= $registro;
        // Para cada nota
        $qtd_linhas = 0;
        $val_total_srv = 0;
        foreach($notas as $nota){
            // Gera registro
            $reg = new RpsLayoutTipo20();
            $reg->carrega_valores_array($this->lote_rps->valores_em_array());            
            $reg->carrega_valores_array($nota->valores_em_array_tipo_20());            
            // Grava registro
            $registro = $reg->registro();
            // fwrite($myfile, $registro);
            $arq .= $registro;
            $qtd_linhas++;
            $val_total_srv += round($nota->valor_total_emissao, 2);
            
        }
        // Gera rodape
        $reg = new RpsLayoutTipo90;
        $val_total_srv = substr(str_repeat("0", 15).preg_replace("/\D/", "", number_format($val_total_srv, 2)), - 15);
        
        $reg->atualiza_campo("qtd_linhas", $qtd_linhas);
        $reg->atualiza_campo("val_total_srv", $val_total_srv);
        $registro = $reg->registro();
        $arq .= $registro;
        // fwrite($myfile, $registro);
        file_put_contents($caminho, $arq);
        // file_put_contents($caminho, utf8_decode($arq));
        // file_put_contents($caminho, mb_convert_encoding($arq, "ISO-8859-2", "UTF-8"));
        // file_put_contents($caminho, iconv("UTF-8", "windows-1250//TRANSLIT", $arq));
        // file_put_contents($caminho, iconv("UTF-8", "ISO-8859-2//TRANSLIT", $arq));
        // Fechar arquivo
        // fclose($myfile);
        // Retorna caminho do arquivo
        return $caminho;
    }
}