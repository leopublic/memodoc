<?php

/**
 * Gera os arquivos para importação da nota carioca
 */

namespace Memodoc\Utilitarios;

class RpsLayoutTipo10 extends RpsLayout{

    public function valores_default(){
        return [
        ];
    }

    public function monta_campos(){
        $this->campos = [
            'tipo_reg' => ['tipo' => 'N', 'tamanho' => 2, 'valor' => '10']
            ,'versao' => ['tipo' => 'N', 'tamanho' => 3, 'valor' => '003']
            ,'contr_tipo_doc' => ['tipo' => 'N', 'tamanho' => 1, 'valor' => '2']
            ,'contr_cnpj' => ['tipo' => 'N', 'tamanho' => 14, 'valor' => '']
            ,'contr_insc_municipal' => ['tipo' => 'N', 'tamanho' => 15, 'valor' => '']
            ,'dt_inicio' => ['tipo' => 'D', 'tamanho' => 8, 'valor' => '']
            ,'dt_fim' => ['tipo' => 'D', 'tamanho' => 8, 'valor' => '']
        ];
    }
}