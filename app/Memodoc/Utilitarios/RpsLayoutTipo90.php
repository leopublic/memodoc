<?php

/**
 * Gera os arquivos para importação da nota carioca
 */

namespace Memodoc\Utilitarios;

class RpsLayoutTipo90 extends RpsLayout{

    public function valores_default(){
        return [
        ];
    }

    public function monta_campos(){
        $this->campos = [
            'tipo_reg' => ['tipo' => 'N', 'tamanho' => 2, 'valor' => '90']
            ,'qtd_linhas' => ['tipo' => 'N', 'tamanho' => 8, 'valor' => '0']
            ,'val_total_srv' => ['tipo' => 'N', 'tamanho' => 15, 'valor' => '0']
            ,'val_total_deducoes' => ['tipo' => 'N', 'tamanho' => 15, 'valor' => '0']
            ,'val_total_desconto' => ['tipo' => 'N', 'tamanho' => 15, 'valor' => '0']
            ,'val_total_desconto_inc' => ['tipo' => 'N', 'tamanho' => 15, 'valor' => '0']
        ];
    }
}