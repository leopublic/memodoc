<?php

/**
 * Gera os arquivos para importação da nota carioca
 * 
 */

namespace Memodoc\Utilitarios;

class ArquivoSicoobRemessa {

    public $sicoob_remessa;

    public function __construct($remessa){
        $this->sicoob_remessa = $remessa;
        $this->lote_rps = \LoteRps::find($remessa->id_lote_rps);
    }

    public function caminho(){
        // Gera o caminho do arquivo pelo id do lote.
        $ret = app_path()."/arquivos/sicoob/remessa/";
        if(!is_dir($ret)){
            mkdir($ret, 0775, true);
        }
        return $ret;
    }
    
    public function nome(){
        return substr('00000000'.$this->sicoob_remessa->id_sicoob_remessa, -8).".txt";
    }
    
    public function gerar(){
        $caminho = $this->caminho();
        // Obtem cliente prestador
        $cliente = \Cliente::memodoc()->first();
        // Abre cursor de notas
        $notas = \NotaFiscal::dolote($this->sicoob_remessa->id_lote_rps)
        ->leftjoin('faturamento', 'faturamento.id_faturamento', '=', 'nota_fiscal.id_faturamento')
        ->leftjoin('cliente', 'cliente.id_cliente', '=', \DB::raw('coalesce(nota_fiscal.id_cliente, faturamento.id_cliente)'))
        ->leftjoin('tipo_faturamento', 'tipo_faturamento.id_tipo_faturamento', '=', 'cliente.id_tipo_faturamento')
        ->orderBy('cliente.razaosocial')
        ->get();

        $caminho = $this->caminho().$this->nome();
        // Abre stream
        try{
            unlink($caminho);
        } catch(\Exception $e){

        }
        $myfile = fopen($caminho, "wb");
        // Grava cabecalho
        $reg = new BoletoLayoutTipo0();
        $reg->carrega_valores_array($this->sicoob_remessa->valores_em_array());
        $registro = $reg->registro();
        fwrite($myfile, $registro);
        // Para cada nota
        $qtd_linhas = 1;
        foreach($notas as $nota){
            if($nota->nfis_numero > 0){
                $qtd_linhas++;
                // Gera registro
                $reg = new BoletoLayoutTipo1();
                $reg->carrega_valores_array($nota->valores_em_array_boleto());
                $reg->atualiza_campo('sequencial', $qtd_linhas);
                // Grava registro
                $registro = $reg->registro();
    
                fwrite($myfile, $registro);            
    
            }
        }
        // Gera rodape
        $qtd_linhas++;
        $reg = new BoletoLayoutTipo9;
        $reg->atualiza_campo('sequencial', $qtd_linhas);
        
        $registro = $reg->registro();
        fwrite($myfile, $registro);
    
        // Fechar arquivo
        fclose($myfile);
        // Retorna caminho do arquivo
        return $caminho;
    }
}