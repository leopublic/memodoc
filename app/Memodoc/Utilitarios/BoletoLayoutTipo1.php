<?php

/**
 * Gera os arquivos para importação da nota carioca
 */

namespace Memodoc\Utilitarios;

class BoletoLayoutTipo1 extends RpsLayout{

    public function valores_default(){
        return [
        ];
    }

    public function monta_campos(){
        $this->campos = [
            'id_registro' => [ 'tipo'=> "N", 'tamanho' => 1, 'valor' => '1' ]
            ,'tipo_pessoa' => [ 'tipo'=> "N", 'tamanho' => 2, 'valor' => '' ]
            ,'cgc' => [ 'tipo'=> "N", 'tamanho' => 14, 'valor' => '04844937000102' ]
            ,'prefixo_cooperativa' => [ 'tipo'=> "N", 'tamanho' => 4, 'valor' => '4327' ]
            ,'digito_cooperativa' => [ 'tipo'=> "N", 'tamanho' => 1, 'valor' => '3' ]
            ,'conta_corrente' => [ 'tipo'=> "N", 'tamanho' => 8, 'valor' => '12266' ]
            ,'digito_conta' => [ 'tipo'=> "T", 'tamanho' => 1, 'valor' => '1' ]
            ,'numero_convenio' => [ 'tipo'=> "N", 'tamanho' => 6, 'valor' => '000000' ]
            ,'numero_controle' => [ 'tipo'=> "T", 'tamanho' => 25, 'valor' => '' ]
            ,'nosso_numero' => [ 'tipo'=> "N", 'tamanho' => 12, 'valor' => '000000000000' ]
            ,'num_parcela' => [ 'tipo'=> "N", 'tamanho' => 2, 'valor' => '01' ]
            ,'grupo_valor' => [ 'tipo'=> "N", 'tamanho' => 2, 'valor' => '00' ]
            ,'filler1' => [ 'tipo'=> "T", 'tamanho' => 3, 'valor' => '   ' ]
            ,'indicativo_mensagem' => [ 'tipo'=> "T", 'tamanho' => 1, 'valor' => ' ' ]
            ,'prefixo_titulo' => [ 'tipo'=> "T", 'tamanho' => 3, 'valor' => '   ' ]
            ,'variacao_carteira' => [ 'tipo'=> "N", 'tamanho' => 3, 'valor' => '000' ]
            ,'conta_caucao' => [ 'tipo'=> "N", 'tamanho' => 1, 'valor' => '0' ]
            ,'num_contrato_garantia' => [ 'tipo'=> "N", 'tamanho' => 5, 'valor' => '00000' ]
            ,'dv_contrato_garantia' => [ 'tipo'=> "T", 'tamanho' => 1, 'valor' => '0' ]
            ,'bordero' => [ 'tipo'=> "N", 'tamanho' => 6, 'valor' => '000000' ]
            ,'filler2' => [ 'tipo'=> "T", 'tamanho' => 4, 'valor' => '    ' ]
            ,'tipo_emissao' => [ 'tipo'=> "N", 'tamanho' => 1, 'valor' => '2' ]
            ,'carteira' => [ 'tipo'=> "N", 'tamanho' => 2, 'valor' => '1' ]
            ,'comando' => [ 'tipo'=> "N", 'tamanho' => 2, 'valor' => '01' ]
            ,'nfis_numero' => [ 'tipo'=> "T", 'tamanho' => 10, 'valor' => '' ]
            ,'dt_vencimento' => [ 'tipo'=> "T", 'tamanho' => 6, 'valor' => '' ]
            ,'vl_titulo' => [ 'tipo'=> "V", 'tamanho' => 13, 'valor' => '' ]
            ,'numero_banco' => [ 'tipo'=> "N", 'tamanho' => 3, 'valor' => '756' ]
            ,'prefixo_cooperativa2' => [ 'tipo'=> "N", 'tamanho' => 4, 'valor' => '4327' ]
            ,'digito_cooperativa2' => [ 'tipo'=> "N", 'tamanho' => 1, 'valor' => '3' ]
            ,'especie_titulo' => [ 'tipo'=> "N", 'tamanho' => 2, 'valor' => '01' ]
            ,'aceite' => [ 'tipo'=> "T", 'tamanho' => 1, 'valor' => '0' ]
            ,'dt_emissao' => [ 'tipo'=> "N", 'tamanho' => 6, 'valor' => '' ]
            ,'primeira_instrucao' => [ 'tipo'=> "N", 'tamanho' => 2, 'valor' => '01' ]
            ,'segunda_instrucao' => [ 'tipo'=> "N", 'tamanho' => 2, 'valor' => '01' ]
            ,'taxa_mora' => [ 'tipo'=> "T", 'tamanho' => 6, 'valor' => '020000' ]
            ,'taxa_multa' => [ 'tipo'=> "T", 'tamanho' => 6, 'valor' => '010000' ]
            ,'tipo_distribuicao' => [ 'tipo'=> "N", 'tamanho' => 1, 'valor' => '2' ]
            ,'dt_primeiro_desconto' => [ 'tipo'=> "N", 'tamanho' => 6, 'valor' => '' ]
            ,'vl_primeiro_desconto' => [ 'tipo'=> "V", 'tamanho' => 13, 'valor' => '' ]
            ,'moeda_iof' => [ 'tipo'=> "N", 'tamanho' => 13, 'valor' => '0000000000000' ]
            ,'vl_abatimento' => [ 'tipo'=> "V", 'tamanho' => 13, 'valor' => '' ]
            ,'tipo_pessoa2' => [ 'tipo'=> "N", 'tamanho' => 2, 'valor' => '' ]
            ,'cgc2' => [ 'tipo'=> "N", 'tamanho' => 14, 'valor' => '' ]
            ,'razaosocial' => [ 'tipo'=> "T", 'tamanho' => 40, 'valor' => '' ]
            ,'endereco' => [ 'tipo'=> "T", 'tamanho' => 37, 'valor' => '' ]
            ,'bairro' => [ 'tipo'=> "T", 'tamanho' => 15, 'valor' => '' ]
            ,'cep' => [ 'tipo'=> "N", 'tamanho' => 8, 'valor' => '' ]
            ,'cidade' => [ 'tipo'=> "T", 'tamanho' => 15, 'valor' => '' ]
            ,'uf' => [ 'tipo'=> "T", 'tamanho' => 2, 'valor' => '' ]
            ,'observacao' => [ 'tipo'=> "T", 'tamanho' => 40, 'valor' => '' ]
            ,'num_dias_protesto' => [ 'tipo'=> "T", 'tamanho' => 2, 'valor' => '0' ]
            ,'filler3' => [ 'tipo'=> "T", 'tamanho' => 1, 'valor' => ' ' ]
            ,'sequencial' => [ 'tipo'=> "N", 'tamanho' => 6, 'valor' => '' ]
        ];
    }
}