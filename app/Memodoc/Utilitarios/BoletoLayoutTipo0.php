<?php

/**
 * Gera os arquivos para importação da nota carioca
 */

namespace Memodoc\Utilitarios;

class BoletoLayoutTipo0 extends RpsLayout{

    public function valores_default(){
        return [
        ];
    }

    public function monta_campos(){
        $this->campos = [
            'id_registro' => [ 'tipo'=> "N", 'tamanho' => 1, 'valor' => '0' ]
            ,'tipo_operacao' => [ 'tipo'=> "N", 'tamanho' => 1, 'valor' => '1' ]
            ,'tipo_operacao_extenso' => [ 'tipo'=> "T", 'tamanho' => 7, 'valor' => 'REMESSA' ]
            ,'tipo_servico' => [ 'tipo'=> "N", 'tamanho' => 2, 'valor' => '01' ]
            ,'tipo_servico_extenso' => [ 'tipo'=> "T", 'tamanho' => 8, 'valor' => 'COBRANÇA' ]
            ,'filler1' => [ 'tipo'=> "T", 'tamanho' => 7, 'valor' => '       ' ]
            ,'prefixo_coop' => [ 'tipo'=> "N", 'tamanho' => 4, 'valor' => '4327' ]
            ,'digito_coop' => [ 'tipo'=> "T", 'tamanho' => 1, 'valor' => ' ' ]
            ,'codigo_cliente' => [ 'tipo'=> "N", 'tamanho' => 8, 'valor' => '00030103' ]
            ,'digito_codigo_cliente' => [ 'tipo'=> "T", 'tamanho' => 1, 'valor' => '4' ]
            ,'convenio' => [ 'tipo'=> "N", 'tamanho' => 6, 'valor' => '      ' ]
            ,'nome_beneficiario' => [ 'tipo'=> "T", 'tamanho' => 30, 'valor' => 'MEMODOC - GUARDA DE DOCUMENTOS' ]
            ,'id_banco' => [ 'tipo'=> "T", 'tamanho' => 18, 'valor' => '756BANCOOBCED' ]
            ,'data_carga' => [ 'tipo'=> "N", 'tamanho' => 6, 'valor' => '' ]
            ,'id_sicoob_remessa' => [ 'tipo'=> "N", 'tamanho' => 7, 'valor' => '' ]
            ,'filler2' => [ 'tipo'=> "T", 'tamanho' => 287, 'valor' => '' ]
            ,'sequencial' => [ 'tipo'=> "N", 'tamanho' => 6, 'valor' => '000001' ]
        ];
    }
}