<?php

/**
 * Gera os arquivos para importação da nota carioca
 */

namespace Memodoc\Utilitarios;

class BoletoLayoutTipo9 extends RpsLayout{

    public function valores_default(){
        return [
        ];
    }

    public function monta_campos(){
        $this->campos = [
            'id_registro' => [ 'tipo'=> "N", 'tamanho' => 1, 'valor' => '9' ]
            ,'filler' => [ 'tipo'=> "T", 'tamanho' => 193, 'valor' => '' ]
            ,'msg1' => [ 'tipo'=> "T", 'tamanho' => 40, 'valor' => utf8_decode('APOS O VENCIMENTO CONSIDERAR            ') ]
            ,'msg2' => [ 'tipo'=> "T", 'tamanho' => 40, 'valor' => 'MULTA DE 2,00% E' ]
            ,'msg3' => [ 'tipo'=> "T", 'tamanho' => 40, 'valor' => utf8_decode('JUROS DE 1,00% AO MES') ]
            ,'msg4' => [ 'tipo'=> "T", 'tamanho' => 40, 'valor' => 'PARA ATUALIZAR O VENCIMENTO:' ]
            ,'msg5' => [ 'tipo'=> "T", 'tamanho' => 40, 'valor' => 'www.sicoob.com.br/segunda-via-de-boleto' ]
            ,'sequencial' => [ 'tipo'=> "N", 'tamanho' => 6, 'valor' => '' ]
        ];
    }
}