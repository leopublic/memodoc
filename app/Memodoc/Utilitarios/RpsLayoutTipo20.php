<?php

/**
 * Gera os arquivos para importação da nota carioca
 */

namespace Memodoc\Utilitarios;

class RpsLayoutTipo20 extends RpsLayout{

    public function valores_default(){
        return [
            'trb_tipo' => '01'
            ,'rps_tipo' => '0'
            ,'tipo_reg'     => "20"
            ,'srv_cidade' => 'RIO DE JANEIRO'
            ,'srv_uf'       => 'RJ'
            ,'trb_regime_especial'       => '00'
            ,'trb_simples'       => '0'
            ,'trb_incentivo_cultural'   => '0'
            ,'trb_regime_especial'       => '00'
            ,'srv_cod_federal'       => '1104'
            ,'trb_regime_especial'       => '00'
            ,'srv_cod_municipal'       => '110401'
            ,'srv_aliquota'       => '5'
            ,'srv_iss_retido'       => '0'
        ];
    }

    public function monta_campos(){
        $this->campos = [
            'tipo_reg' => ['tipo' => 'N', 'tamanho' => 2, 'valor' => '20']
            ,'rps_tipo' => ['tipo' => 'N', 'tamanho' => 1, 'valor' => '0']
            ,'rps_serie' => ['tipo' => 'T', 'tamanho' => 5, 'valor' => '']
            ,'rps_numero' => ['tipo' => 'N', 'tamanho' => 15, 'valor' => '']
            ,'rps_dt_emissao' => ['tipo' => 'D', 'tamanho' => 8, 'valor' => '']
            ,'rps_status' => ['tipo' => 'N', 'tamanho' => 1, 'valor' => '1']

            ,'tom_tipo_doc' => ['tipo' => 'N', 'tamanho' => 1, 'valor' => '']
            ,'tom_documento' => ['tipo' => 'N', 'tamanho' => 14, 'valor' => '']
            ,'tom_insc_municipal' => ['tipo' => 'T', 'tamanho' => 15, 'valor' => '']
            ,'tom_insc_estadual' => ['tipo' => 'N', 'tamanho' => 15, 'valor' => '']
            ,'tom_razao_social' => ['tipo' => 'T', 'tamanho' => 115, 'valor' => '']
            ,'end_tipo' => ['tipo' => 'T', 'tamanho' => 3, 'valor' => '']
            ,'end_endereco' => ['tipo' => 'T', 'tamanho' => 125, 'valor' => '']
            ,'end_numero' => ['tipo' => 'T', 'tamanho' => 10, 'valor' => '']
            ,'end_compl' => ['tipo' => 'T', 'tamanho' => 60, 'valor' => '']
            ,'end_bairro' => ['tipo' => 'T', 'tamanho' => 72, 'valor' => '']
            ,'end_cidade' => ['tipo' => 'T', 'tamanho' => 50, 'valor' => '']
            ,'end_uf' => ['tipo' => 'T', 'tamanho' => 2, 'valor' => '']
            ,'end_cep' => ['tipo' => 'N', 'tamanho' => 8, 'valor' => '']
            ,'tom_telefone' => ['tipo' => 'T', 'tamanho' => 11, 'valor' => '']
            ,'tom_email' => ['tipo' => 'T', 'tamanho' => 80, 'valor' => '']
            ,'trb_tipo' => ['tipo' => 'N', 'tamanho' => 2, 'valor' => '1']

            ,'srv_cidade' => ['tipo' => 'T', 'tamanho' => 50, 'valor' => '']
            ,'srv_uf' => ['tipo' => 'T', 'tamanho' => 2, 'valor' => '']
            ,'trb_regime_especial' => ['tipo' => 'N', 'tamanho' => 2, 'valor' => '0']
            ,'trb_simples' => ['tipo' => 'N', 'tamanho' => 1, 'valor' => '0']
            ,'trb_incentivo_cultural' => ['tipo' => 'N', 'tamanho' => 1, 'valor' => '0']
            ,'srv_cod_federal' => ['tipo' => 'N', 'tamanho' => 4, 'valor' => '1104']
            ,'reservado_1' => ['tipo' => 'T', 'tamanho' => 6, 'valor' => '']
            ,'srv_pais_exportacao' => ['tipo' => 'T', 'tamanho' => 5, 'valor' => '']
            ,'trb_codigo_beneficio' => ['tipo' => 'N', 'tamanho' => 3, 'valor' => '']
            ,'srv_cod_municipal' => ['tipo' => 'T', 'tamanho' => 6, 'valor' => '']

            ,'srv_aliquota' => ['tipo' => 'V', 'tamanho' => 5, 'valor' => '']
            ,'srv_valor' => ['tipo' => 'V', 'tamanho' => 15, 'valor' => '']
            ,'srv_deducoes' => ['tipo' => 'V', 'tamanho' => 15, 'valor' => '']
            ,'srv_desc_condicionado' => ['tipo' => 'V', 'tamanho' => 15, 'valor' => '']
            ,'srv_desc_incondicionado' => ['tipo' => 'V', 'tamanho' => 15, 'valor' => '']
            ,'srv_cofins' => ['tipo' => 'V', 'tamanho' => 15, 'valor' => '']
            ,'srv_csll' => ['tipo' => 'V', 'tamanho' => 15, 'valor' => '']
            ,'srv_inss' => ['tipo' => 'V', 'tamanho' => 15, 'valor' => '']

            ,'srv_irpj' => ['tipo' => 'V', 'tamanho' => 15, 'valor' => '']
            ,'srv_pis' => ['tipo' => 'V', 'tamanho' => 15, 'valor' => '']
            ,'srv_outras_retencoes' => ['tipo' => 'V', 'tamanho' => 15, 'valor' => '']
            ,'srv_iss' => ['tipo' => 'V', 'tamanho' => 15, 'valor' => '']
            ,'srv_iss_retido' => ['tipo' => 'N', 'tamanho' => 1, 'valor' => '0']
            ,'srv_iss_dt_competencia' => ['tipo' => 'D', 'tamanho' => 8, 'valor' => '']
            ,'obr_codigo' => ['tipo' => 'T', 'tamanho' => 15, 'valor' => '']
            ,'art' => ['tipo' => 'T', 'tamanho' => 15, 'valor' => '']
            ,'rps_serie_subst' => ['tipo' => 'T', 'tamanho' => 5, 'valor' => '']
            ,'rps_numero_subst' => ['tipo' => 'N', 'tamanho' => 15, 'valor' => '']
            ,'reservado_2' => ['tipo' => 'T', 'tamanho' => 30, 'valor' => '']

            ,'srv_descricao' => ['tipo' => 'T', 'tamanho' => 500, 'valor' => '']
        ];
    }

}