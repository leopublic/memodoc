<?php
namespace Memodoc\Servicos;

class CalculadoraCustodia{
    protected $precos;
    protected $faturamento;

    public function __construct(\Memodoc\Servicos\PrecosInterface $precos, \Faturamento $faturamento){
        $this->precos           = $precos;
        $this->faturamento      = $faturamento;
    }

    public function qtdCaixasMesAnterior(){
        // Conta a quantidade de caixas que foram reservadas antes da dt_inicio do faturamento
        $sql = "select count(distinct id_caixa)
                  from documento
                  join reserva on reserva.id_reserva = documento.id_reserva
                  where documento.id_cliente = ".$this->faturamento->id_cliente."
                  and reserva.data_reserva < ".$this->faturamento->dt_inicio;

        return \DB::raw($sql)->first();
//        return \DB::table('documento')
//                    ->join('reserva', 'reserva.id_reserva', '=', 'documento.id_reserva')
//                    ->select(array('id_caixa'))->distinct()
//                    ->where('documento.id_cliente', '=', $this->faturamento->id_cliente)
//                    ->where('reserva.data_reserva', '<', $this->faturamento->dt_inicio)
//                    ->count();

    }

    public function qtdCaixasAtual(){
        //Quantidade de caixas atualmente
    }

    public function qtdExpurgos(){
        // Quantidade de caixas expurgadas no período
    }

    public function qtdEmCarencia(){
        // Quantidade de caixas em carencia:
    }

    public function qtdCaixasNovas(){
        // Quantidade de caixas que foram criadas
    }
}
