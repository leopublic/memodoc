<?php

/**
 * Gera os arquivos para importação da nota carioca
 */

namespace Memodoc\Servicos;

class ExportadorNotaCarioca {
    public $periodo_faturamento ;

    public function gera_arquivo(){
        $faturamentos = Faturamento::doperiodo($this->periodo_faturamento->id_periodo_faturamento)->cobraveis()->get();
        $this->gera_tipo10();
        
        foreach($faturamentos as $fat){
            $this->gera_tipo20();
            $this->gera_tipo21();
            $this->gera_tipo90();
        }
    }

    public function gera_tipo10(){

    }


    public function gera_tipo20(){
        
    }

    public function monta_tipo10($campos){
        
    }

    public function gera_tipo21(){
                
    }

    public function gera_tipo90(){

    }
        
}
