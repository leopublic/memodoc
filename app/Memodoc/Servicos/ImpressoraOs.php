<?php

namespace Memodoc\Servicos;

class ImpressoraOs extends Impressora{

    public function capa($mpdf, \Os $os, $novaPagina = true) {
        if($novaPagina){
            $mpdf->AddPage('P','', 1);
        }

        $html = \View::make('ordemservico_relatorio')->with('os', $os);
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $mpdf->setFooter("||Página {PAGENO}/{nbpg}");
        $mpdf->WriteHTML($html);
        return $mpdf;
    }

    public function relacaoCaixas($mpdf, \Os $os, $novaPagina = true) {
        if($novaPagina){
            $mpdf->AddPage('A4-L','', 1);
        }

        $desmembrada = $os->referenciasConsulta();
        $html = \View::make('ordemservico_relacaodecaixas', compact('os', 'desmembrada'));
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);

        $mpdf->setFooter("||Página {PAGENO}/{nbpg}");
        $mpdf->WriteHTML($html);
        return $mpdf;
    }

    public function relacaoCaixasOperacional($mpdf, \Os $os, $novaPagina = true) {
        if($novaPagina){
            $mpdf->AddPage('P','', 1, '', '', 10, 10, 10, 10, '', 10);
        }

        $desmembrada = $os->osdesmembradasPorEndereco()->get();
        $html = \View::make('ordemservico_relacaodecaixas_operacional', compact('os', 'desmembrada'));
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);

        $mpdf->setFooter("||Página {PAGENO}/{nbpg}");

        $mpdf->WriteHTML($html);
        return $mpdf;
    }

    public function protocoloRetirada($mpdf, \Os $os, $novaPagina = true) {
        if($novaPagina){
            $mpdf->AddPage('P','', 1, '', '', 10, 10, 10, 10, '', '');
        }
        $html = \View::make('ordemservico_protocolo_retirada', compact('os'));
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $mpdf->setFooter("||Página {PAGENO}/{nbpg}");

        $mpdf->WriteHTML($html);
        return $mpdf;
    }

    public function protocoloItens($mpdf, \Os $os, $itens) {
        $brtitulo = '<br/>RELAÇÃO DE ITENS RETIRADOS DO CLIENTE';
        $header = \View::make('ordemservico_cabecalho_rel', compact('os', 'brtitulo'));
        $header = iconv("UTF-8", "UTF-8//IGNORE", $header);
        $mpdf->SetHTMLHeader($header);

        $html = \View::make('ordemservico_protocolo_itens', compact('os', 'itens'));
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);

        $mpdf->WriteHTML($html);
        return $mpdf;
    }
}
