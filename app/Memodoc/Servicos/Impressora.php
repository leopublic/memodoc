<?php

namespace Memodoc\Servicos;

class Impressora{

    public function novoPdf($formato = 'A4' ){
        $mpdf = new \mPDF('utf-8', $formato, '', '', '10', '10', '10', '10', '', '');  // largura x altura
        $mpdf->allow_charset_conversion = false;
        return $mpdf;
    }


}
