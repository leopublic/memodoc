<?php

/**
 * Concentra todos os cálculos de valores relacionados a OS
 */

namespace Memodoc\Servicos;

class CalculadoraOs {

    protected $precos;
    protected $os;

    public function __construct(\Memodoc\Servicos\PrecosInterface $preco, $os) {
        $this->preco = $preco;
        $this->os = $os;
    }

    /**
     * Calcula o valor do frete de uma OS a partir da quantidade de caixas
     * @return decimal Valor total do frete
     */
    public function valorFrete() {
        if (!$this->os->cobrafrete ) {
            return 0;
        } else {
            $qtdMovimentada = $this->os->qtdCaixasMovimentadas();
            if ($qtdMovimentada < $this->preco->qtdMinimoFrete()) {
//            if ($qtdMovimentada > 0 && $qtdMovimentada < $this->preco->qtdMinimoFrete()) {
                $qtdMovimentada = $this->preco->qtdMinimoFrete();
            }
            return $qtdMovimentada * $this->preco->valorUnitarioFrete($this->os->urgente);
        }
    }
    /**
     * Calcula o valor do frete de uma OS a partir da quantidade de caixas
     * @return decimal Valor total do frete
     */
    public function valorFretePrevisto() {
        if (!$this->os->cobrafrete ) {
            return 0;
        } else {
            $qtdMovimentada = $this->os->qtdCaixasMovimentadasPrevista();
            if ($qtdMovimentada < $this->preco->qtdMinimoFrete()) {
//            if ($qtdMovimentada > 0 && $qtdMovimentada < $this->preco->qtdMinimoFrete()) {
                $qtdMovimentada = $this->preco->qtdMinimoFrete();
            }
            return $qtdMovimentada * $this->preco->valorUnitarioFrete($this->os->urgente);
        }
    }

    /**
     * Calcula o valor da cartonagem para uma OS
     * @return decimal
     */
    public function valorCartonagem() {
        if ($this->os->naoehcartonagemcortesia) {
            return $this->preco->valorUnitarioCartonagem() * (intval($this->os->qtdenovascaixas) + intval($this->os->qtd_cartonagens_cliente)) ;
        } else {
            return 0;
        }
    }

    public function valorMovimentacao() {
        return intval($this->os->qtdcaixasemprestadas) * $this->precos->valorUnitarioMovimentacao($this->os->urgencia);
    }

}
