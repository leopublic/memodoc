<?php

namespace Memodoc\Servicos;

interface GeradorAcervoInterface {
    public function gere(\Cliente $cliente);
    public function caminhoArquivo();
}