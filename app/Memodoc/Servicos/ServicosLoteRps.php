<?php

/**
 * Gera os arquivos para importação da nota carioca
 */

namespace Memodoc\Servicos;

class ServicosLoteRps {
    /**
     * 
     */
    public function cria_novo_de_faturamento($id_periodo_faturamento, $dt_emissao, $dt_geracao, $dia_vencimento, $id_tipo_faturamento, $id_usuario){
        $loterps = new \LoteRps;
        $loterps->id_periodo_faturamento = $id_periodo_faturamento;
        $loterps->lrps_status = 0;
        $loterps->lrps_dt_emissao_fmt = $dt_emissao;
        $loterps->lrps_dt_geracao = $dt_geracao;
        $loterps->id_usuario = $id_usuario;
        $loterps->save();
        // Carrega notas 
        $pendentes = $this->pendentes($id_periodo_faturamento, $dia_vencimento, $id_tipo_faturamento);
        $pendentes->update(['id_lote_rps' => $loterps->id_lote_rps]);
        return $loterps;
    }   

    public function pendentes($id_periodo_faturamento, $dia_vencimento, $id_tipo_faturamento){
        return \NotaFiscal::doperfat($id_periodo_faturamento)
        ->declientescomfaturamentohabilitado()
        ->declientescomvencimentoem($dia_vencimento)
        ->declientescomtipodefaturamento($id_tipo_faturamento)
        ->semlote()
        ->whereRaw("id_faturamento not in (select id_faturamento from faturamento where id_periodo_faturamento = ".$id_periodo_faturamento." and (val_enviadas_consulta + val_frete + val_frete_urgente + val_custodia_periodo_cobrado + val_expurgos + val_movimentacao_expurgo + val_movimentacao) = 0)");
        ;

    }

    /**
     * Exclui um lote de RPS e dessassocia todas as notas dele.
     */
    public function excluir($id_lote_rps){
        \DB::beginTransaction();
        \NotaFiscal::where('id_lote_rps', '=', $id_lote_rps)
                    ->update(["id_lote_rps" => null]);
        \LoteRps::where('id_lote_rps', '=', $id_lote_rps)
                ->delete();
        \DB::commit();
    }
}
