<?php

namespace Memodoc\Servicos;

interface PrecosInterface {

    public function __construct(\Cliente $cliente);

    public function SalvaPrecos(\Faturamento $faturamento);

    public function CarregaPrecos(\Faturamento $faturamento = null);

    public function valorUnitarioFrete($urgencia = false);

    public function valorUnitarioCartonagem($urgencia = false);

    public function valorUnitarioArmazenamento($urgencia = false);

    public function valorUnitarioMovimentacao($urgencia = false);

    public function valorUnitarioExpurgo($urgencia = false);

    public function qtdMinimoFrete();
}
