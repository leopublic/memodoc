<?php

namespace Memodoc\Servicos;

class Autenticacao {

    //Autentica o cliente e retorna a rota
    public function rotaHome(\Usuario $usuario) {
        if ($usuario->id_nivel < 10 && ( $usuario->adm == 1 || $usuario->qtdClientes() > 0)) {
            return '/publico/usuario/escolherempresa';
        } else {
            $caminho = \Session::get('caminho_pendente');
            if ($caminho != ''){
                return $caminho;
            } else {
                return '/';
            }
        }
    }

    public function temAcesso($post) {
        $username = $post['username'];
        $password = $post['password'];
        if (\Auth::attempt(array('username' => $username, 'password' => $password))) {
            $usuario = \Auth::user();
            $usuario->acessonumero = intval($usuario->acessonumero) + 1;
            $usuario->ultimoacesso = date('Y-m-d H:i:s');
            $usuario->save();
            if (intval($usuario->validadelogin) > 0) {
                $datavalidade = \Carbon::createFromFormat('Y-m-d', $usuario->datacadastro);
                $datavalidade->addDays($usuario->validadelogin);
                if ($datavalidade->isFuture()) {
                    return true;
                } else {
                    \log::info('Candidato expirado '.$usuario->id_usuario);
                    return false;
                }
            } elseif ($usuario->bloqueio != '' && $usuario->bloqueio != '30/12/1899') {
                $bloqueio = \Carbon::createFromFormat('d/m/Y', $usuario->bloqueio);
                if ($bloqueio->isFuture()) {
                    return true;
                } else {
                    \log::info('Candidato bloqueado '.$usuario->id_usuario);
                    return false;
                }
            } else {
                return true;
            }
        } else {
            \log::info('Candidato login/senha errado '.$username);
            return false;
        }
    }

}
