<?php

/**
 * Gera os arquivos para importação da nota carioca
 */

namespace Memodoc\Servicos;

class ServicosNotaFiscal {
    public function cria_notas_de_faturamento($id_periodo_faturamento, $id_usuario){
        $faturamentos = \Faturamento::doperiodo($id_periodo_faturamento)
                    ->whereNull("id_nota_fiscal")
                    ->get();

        foreach($faturamentos as $fat){
            $nota = new \NotaFiscal;
            $nota->id_faturamento = $fat->id_faturamento;
            $nota->nfis_fl_cancelada = 0;
            $nota->cd_usuario = $id_usuario;
            $nota->save();

            $fat->id_nota_fiscal = $nota->id_nota_fiscal;
            $fat->save();
        }
    }    
}
