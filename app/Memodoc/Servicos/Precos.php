<?php

namespace Memodoc\Servicos;

class Precos implements \Memodoc\Servicos\PrecosInterface {

    protected $cliente;
    protected $valores;
    protected $faturamento;

    const tpFRETE = 1;
    const tpCARTONAGEM = 2;
    const tpARMAZENAMENTO = 3;
    const tpMOVIMENTACAO = 4;
    const tpEXPURGO = 5;

    public function __construct(\Cliente $cliente) {
        $this->cliente = $cliente;
        $this->valores = array();
    }

    /**
     * Congela os valores atuais dos tipos de produtos e serviços no faturamento
     */
    public function SalvaPrecos(\Faturamento $faturamento) {
        // Para cada tipoprodcliente do cliente do faturamento
        // Salva o valor na tabela faturamento_servico
        $valores = $this->cliente->tipoprodcliente()->get();
        foreach($valores as $valor){
            $valorServico = \FaturamentoServico::where('id_faturamento', '=', $faturamento->id_faturamento)
                            ->where('id_tipoproduto', '=', $valor->id_tipoproduto)
                            ->first();
            if(!isset($valorServico)){
                $valorServico = new \FaturamentoServico();
                $valorServico->id_faturamento = $faturamento->id_faturamento;
                $valorServico->id_tipoproduto = $valor->id_tipoproduto;
            }
            $valorServico->valor_contrato = $valor->valor_contrato;
            $valorServico->valor_urgencia = $valor->valor_urgencia;
            $valorServico->minimo         = $valor->minimo;
            $valorServico->save();
        }
    }

    /**
     * Carrega os valores do cliente ou de um faturamento especifico se especificado
     */
    public function CarregaPrecos(\Faturamento $faturamento = null) {

        if (isset($faturamento)) {
            $this->faturamento = $faturamento;
            $sql = "select * from hist_tipoprodutocliente where id_periodo_faturamento = ".$faturamento->id_periodo_faturamento." and id_cliente = ".$faturamento->id_cliente;
            $precos = \DB::select(\DB::raw($sql));
        } else {
            $precos = $this->cliente->tipoprodcliente()->get();
        }
        foreach ($precos as $preco) {
            $this->valores[$preco->id_tipoproduto] = array($preco->valor_contrato, $preco->valor_urgencia, $preco->minimo);
        }
    }

    public function valorUnitarioFrete($urgencia = false) {
        return $this->valorUnitario(self::tpFRETE, $urgencia);
    }

    public function valorUnitarioCartonagem($urgencia = false) {
        return $this->valorUnitario(self::tpCARTONAGEM, $urgencia);
    }

    public function valorUnitarioArmazenamento($urgencia = false) {
        return $this->valorUnitario(self::tpARMAZENAMENTO, $urgencia);
    }

    public function valorUnitarioMovimentacao($urgencia = false) {
        return $this->valorUnitario(self::tpMOVIMENTACAO, $urgencia);
    }

    public function valorUnitarioExpurgo($urgencia = false) {
        return $this->valorUnitario(self::tpEXPURGO, $urgencia);
    }

    public function qtdMinimoFrete() {
        if (isset($this->valores[1][2])){
            return $this->valores[1][2];
        } else {
            return 0;
        }
    }

    protected function valorUnitario($indice, $urgencia) {
        if (key_exists($indice, $this->valores)){
            if ($urgencia) {
                $ret = $this->valores[$indice][1];
                if ($ret == 0) {
                    $ret = $this->valores[$indice][0];
                }
            } else {
                $ret = $this->valores[$indice][0];
            }
        } else {
            $ret = 0;
        }
        return $ret;
    }

}
