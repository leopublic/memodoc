<?php
namespace Memodoc\Servicos;

class GeradorAcervoMpdf implements GeradorAcervoInterface{
    public function gere(\Cliente $cliente){
        set_time_limit(0);
        session_write_close();

        $this->mpdf = new mPDF('utf-8', 'A4-L', '', '', '10', '10', '30', '20', '10', '10');  // largura x altura
        $this->mpdf->debug = true;
        $this->mpdf->useSubstitutions=false;
        $this->mpdf->simpleTables = true;
        $this->mpdf->allow_charset_conversion = false;

        $dir = "../public/".$id_cliente."/tmp";

        $head = View::make('documento.acervo_header')
                ->with('qtdDocumentos', $cliente->qtdDocs())
                ->with('qtdCaixas', $cliente->qtdCaixas())
                ;
//        print $head;
        $this->mpdf->setHTMLHeader($head);
        $this->mpdf->setFooter("||Página {PAGENO}/{nb}");

        $html = View::make('documento.acervo_inicio');
//        print $html;
//        $this->mpdf->WriteHTML(iconv("UTF-8", "UTF-8//IGNORE", $html) );
        $id_centroant = '';
        $id_depclienteant = '';
        $id_caixapadraoant = '';
        $i=0;
        $html = '';
        $arqs = 0;
        //
        // Monta array com ranges 


        $res = \Documento::cursorAcervo($cliente->id_cliente);
        while($doc = $res->fetch(PDO::FETCH_OBJ)){
            if($id_centroant != $doc->id_centro || $id_depclienteant != $doc->id_departamento){
                $this->mpdf->WriteHTML(iconv("UTF-8", "UTF-8//IGNORE", View::make('documento.acervo_centro_dep')->with('doc', $doc)));
                $id_centroant = $doc->id_centro;
                $id_depclienteant = $doc->id_departamento;
            }
            if($id_caixapadraoant != $doc->id_caixapadrao){
                $this->mpdf->WriteHTML(iconv("UTF-8", "UTF-8//IGNORE", View::make('documento.acervo_caixa')->with('doc', $doc)));
                $id_caixapadraoant = $doc->id_caixapadrao;
            }
            $html = View::make('documento.acervo_item')->with('doc', $doc);
            $this->mpdf->WriteHTML(iconv("UTF-8", "UTF-8//IGNORE", $html));
            $i++;
        }

        $html= View::make('documento.acervo_fim');
//        print $html;
        $this->mpdf->WriteHTML(iconv("UTF-8", "UTF-8//IGNORE", $html));
        $this->mpdf->Output("../public/".$id_cliente.".pdf");

    }
    public function caminhoArquivo(){

    }
}