<?php
class OsFaturada {
    protected $os;
    protected $precos;
    public function __construct(\Os $os, \Memodoc\Servicos\Precos $precos){
        $this->os = $os;
        $this->precos = $precos;
    }

    public function qtdCartonagemNova(){

    }
    public function valCartonagemNova(){
        if($this->os->naoecartonagemcortesia){
            return intval($this->os->qtdenovascaixas) * $this->precos->valorUnitarioCartonagem();
        } else {
            return 0;
        }
    }

    public function qtdMovEntregue(){
        return intval($this->os->osdesmembradas->count());
    }

    public function valMovEntregue(){
        return $this->qtdMovEntregue() * $this->precos->valorUnitarioMovimentacao($this->os->urgente);
    }

    public function qtdMovRecebida(){

    }
    public function valMovRecebida(){

    }

}