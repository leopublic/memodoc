<?php
namespace Memodoc\Servicos;

class FaturamentoEloquent{
    /**
     * Cria um novo período de faturamento
     * @param type $post
     */
    public function criaPeriodo($post, $id_usuario){
        $per = new \PeriodoFaturamento();
        $dt_inicio  = \DateTime::createFromFormat('d/m/Y', Input::get('dt_inicio'));
        $dt_fim     = \DateTime::createFromFormat('d/m/Y', Input::get('dt_fim'));
        $per->dt_inicio = $dt_inicio->format('Y-m-d');
        $per->dt_fim = $dt_fim->format('Y-m-d');
        $per->mes = $dt_fim->format('m');
        $per->ano = $dt_fim->format('Y');
        $per->id_usuario = $id_usuario;
        $per->dt_faturamento = date('Y-m-d');
        $per->save();
        return $per;
    }
    /**
     * Emfilera faturamentos de um array de clientes para serem processados posteriormente
     *
     * O processamento do faturamento pode ser demorado devido a quantidade de clientes
     * e OS a serem processadas, então essa fila permite que o processamento seja
     * feito assincrono e permite também verificar se todos os clientes foram faturados
     *
     * @param array $clientes Array com o id dos clientes
     * @param date $dt_inicio Data de início do período a ser faturado
     * @param date $dt_fim Data final do período a ser faturado
     */
    public function EnfileraFaturamentoParaClientes($clientes, $dt_inicio, $dt_fim, $mes = '' , $ano = ''){
        // Caso mes e ano não sejam informados, assumir mes e ano da data final.

        // Criar uma instancia do model Faturamento para cada cliente do array com o id_status_faturamento = 0

    }

    /**
     * Obtem próximo faturamento aguardando processamento
     *
     * @return type Retorna uma instancia de faturamento caso existe algum pendente
     *  ou FALSE caso não existe nenhum pendente
     */
    public function ProximoFaturamentoPendente(){

    }

    /**
     * Define o escopo do faturamento de um cliente: os valores que serão utilizados
     * as OS que serão faturadas
     * @param \Faturamento $faturamento
     */
    public function DefineEscopo(\Faturamento $faturamento){
        // Cria ou atualiza as ocorrencias do faturamento_servico com os valores
        // da tabela tipoprodcliente, para o cliente do faturamento
        $preco = new Precos($faturamento->cliente);
        $preco->SalvaPrecos($faturamento);

        // Indica as OS que serão consideradas nesse faturamento
        // (informa o id deste faturamento em todas as OS que foram solicitadas_em
        //  entre a dt_inicio e a dt_fim do faturamento
        $oss = \Os::query()->where('id_faturamento', '=', $faturamento->id_faturamento)
            ->update(array('id_faturamento' => null) )
        ;
        $oss = \Os::query()->where('id_cliente', '=', $faturamento->id_cliente)
                        ->whereNull('id_faturamento')
                        ->where('entregar_em', '>=', $faturamento->dt_inicio)
                        ->where('entregar_em', '<=', $faturamento->dt_fim)
                        ->where('cancelado', '=', 0)
                        ->update(array('id_faturamento' => $faturamento->id_faturamento) )
                        ;

    }

    /**
     * Fatura a custódia de caixas para o período faturado
     * @param \Faturamento $faturamento
     */
    public function FaturaCustodia(\Faturamento $faturamento, Precos $precos){
        // Calcula a qtd_custodia_periodo
        // Total de caixas reservadas que deram entrada
        $sql = "update faturamento, periodo_faturamento"
                . " set faturamento.qtd_custodia_periodo = (select count(distinct id_caixa) "
                                                         . "  from documento "
                                                         . " where id_cliente = faturamento.id_cliente "
                                                         . "   and primeira_entrada <= periodo_faturamento.dt_fim)"
                . " where faturamento.id_periodo_faturamento = periodo_faturamento.id_periodo_faturamento"
                . " and faturamento.id_faturamento = ".$faturamento->id_faturamento;
        \DB::update(\DB::raw($sql));

        /**
         * Calcula a quantidade de caixas que foram criadas no período faturado
         * qtd_custodia_novas
         */
        $sql = "update faturamento, periodo_faturamento"
                . " set faturamento.qtd_custodia_novas = (select ifnull(count(distinct id_caixa), 0) from documento where id_cliente = faturamento.id_cliente and primeira_entrada >= periodo_faturamento.dt_inicio"
                . " and primeira_entrada <= periodo_faturamento.dt_fim ) "
                . " where faturamento.id_periodo_faturamento = periodo_faturamento.id_periodo_faturamento"
                . " and faturamento.id_faturamento = ".$faturamento->id_faturamento;
        \DB::update(\DB::raw($sql));

        /**
         * Calcula a quantidade de caixas do mes anterior
         * qtd_custodia_mes_anterior
         */
        $sql = "update faturamento"
                . " set qtd_custodia_mes_anterior = qtd_custodia_periodo - qtd_custodia_novas + qtd_expurgos"
                . " where faturamento.id_faturamento = ".$faturamento->id_faturamento;
        \DB::update(\DB::raw($sql));

        /**
         * Calcula a quantidade de caixas não usadas que começaram a ser cobradas
         * qtd_custodia_em_carencia
         */
        $sql = "update faturamento, periodo_faturamento"
                . " set faturamento.qtd_custodia_em_carencia = (select count(distinct d.id_caixa)"
                . " from documento d, cliente c, reserva r , reservadesmembrada rd"
                . " where c.id_cliente = faturamento.id_cliente"
                . " and d.id_cliente = faturamento.id_cliente"
                . " and rd.id_caixa = d.id_caixa"
                . " and r.id_reserva = rd.id_reserva"
                . " and d.primeira_entrada is null "
                . " and datediff(periodo_faturamento.dt_fim, r.data_reserva) > c.franquiacustodia"
                . " )"
                . " where faturamento.id_periodo_faturamento = periodo_faturamento.id_periodo_faturamento"
                . " and faturamento.id_faturamento = ".$faturamento->id_faturamento;
        \DB::update(\DB::raw($sql));

        $faturamento->getFresh();

        // Calcula expurgadas no periodo
        $sql = "select count(distinct d.id_caixa) total
                  from cliente c
                  join documento d  on d.id_cliente = c.id_cliente
                  join reserva r on r.id_reserva = d.id_reserva
                  join expurgo e on e.id_expurgo = d.id_expurgo
                 where c.id_cliente = ".$faturamento->id_cliente."
                   and coalesce(r.cancelada, 0) = 0
                   and e.data_checkout >= '".$faturamento->dt_inicio.' 00:00:00'."'
                   and e.data_checkout <= '".$faturamento->dt_fim.' 23:59:59'."'
                   and d.primeira_entrada is not null ";
        $res = \DB::select($sql);
        $faturamento->getFresh();

        $faturamento->qtd_expurgos = $res[0]->total;
        $faturamento->val_expurgos = $faturamento->qtd_expurgos * $precos->valorUnitarioExpurgo();

        $faturamento->val_custodia_periodo = $faturamento->qtd_custodia_periodo * $precos->valorUnitarioArmazenamento();
        if ($faturamento->val_custodia_periodo < $faturamento->cliente->valorminimofaturamento){
            $faturamento->val_custodia_periodo = $faturamento->cliente->valorminimofaturamento;
        }

        // Atualiza faturamento
        $faturamento->save();
    }

    /**
     * Fatura os atendimentos (OS)
     * @param \Faturamento $faturamento
     */
    public function FaturaServicos(\Faturamento $faturamento, Precos $precos){


        // Calcula a movimentações
        $sql = "select coalesce(count(distinct os.id_os, osd.id_caixapadrao), 0) total
                  from os
                  join osdesmembrada osd on osd.id_os = os.id_os and osd.id_cliente = os.id_cliente
                 where os.id_faturamento = ".$faturamento->id_faturamento;
        $res = \DB::select($sql);
        $faturamento->qtd_movimentacao = $res[0]->total;
        $faturamento->val_movimentacao = $faturamento->qtd_movimentacao * $precos->valorUnitarioMovimentacao();

        // Calcula o valor total das cartonagens
        $faturamento->qtd_cartonagem_enviadas = $faturamento->ordensdeservico()->sum('qtdenovascaixas');
        $faturamento->val_cartonagem_enviadas = $faturamento->qtd_cartonagem_enviadas * $precos->valorUnitarioCartonagem();

        $faturamento->qtd_retornos_de_consulta = $faturamento->ordensdeservico()->sum('apanhacaixasemprestadas');

        /**
         * Calcula o frete total
         */
        $sql = "update faturamento
                    set faturamento.qtd_frete = (select count(id_caixa) from osdesmembrada, os where osdesmembrada.id_os_chave = os.id_os_chave and os.id_faturamento = faturamento.id_faturamento)
							+ (select sum(ifnull(os.apanhacaixasemprestadas, 0) + ifnull(os.apanhanovascaixas, 0)) from os where id_faturamento = faturamento.id_faturamento)
                      , faturamento.val_frete = (select sum(ifnull(valorfrete, 0)) from os where id_faturamento = faturamento.id_faturamento)
                where faturamento.id_faturamento = ".$faturamento->id_faturamento;
        \DB::update(\DB::raw($sql));

        $faturamento->sumariza();

        $faturamento->save();
    }

    /**
     * Processa o faturamento
     *
     */
    public function ProcessaFaturamento(\Faturamento $faturamento){
        //
        // Define valores e o que será faturado no período
        $this->DefineEscopo($faturamento);
        $precos = new Precos($faturamento->cliente);
        $precos->CarregaPrecos($faturamento);
        //
        // Calcula as quantidades em custódia para o período faturado
        $this->FaturaCustodia($faturamento, $precos);
        //
        // Calcula o faturamento dos serviços executados (OS)
        $this->FaturaServicos($faturamento, $precos);

        // Calcula o valor de custodia

        // Calcula o valor da OS

        // Altera o status do faturamento para 1 - FATURADO
        // Atualiza a dt_faturamento para now().

        // Retorna true
    }


}
