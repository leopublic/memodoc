<?php
namespace Memodoc\Servicos;

class GeradorAcervoArquivo implements GeradorAcervoInterface{
    public function gere(\Cliente $cliente){
        set_time_limit(0);
        session_write_close();

        $file = fopen("../public/".$cliente->id_cliente.".html","w");
        fwrite($file,"Hello World. Testing!");
        $head = View::make('documento.acervo_header')
                ->with('qtdDocumentos', $cliente->qtdDocs())
                ->with('qtdCaixas', $cliente->qtdCaixas())
                ;
        fwrite($file,$head);

        $html = View::make('documento.acervo_inicio');
//        print $html;
//        $this->mpdf->WriteHTML(iconv("UTF-8", "UTF-8//IGNORE", $html) );
        $id_centroant = '';
        $id_depclienteant = '';
        $id_caixapadraoant = '';
        $i=0;
        $html = '';
        $arqs = 0;
        $res = \Documento::cursorAcervo($cliente->id_cliente);
        while($doc = $res->fetch(PDO::FETCH_OBJ)){
            if($id_centroant != $doc->id_centro || $id_depclienteant != $doc->id_departamento){
                $html = View::make('documento.acervo_centro_dep')->with('doc', $doc);
                fwrite($file,$html);
                $id_centroant = $doc->id_centro;
                $id_depclienteant = $doc->id_departamento;
            }
            if($id_caixapadraoant != $doc->id_caixapadrao){
                $html = View::make('documento.acervo_caixa')->with('doc', $doc);
                $id_caixapadraoant = $doc->id_caixapadrao;
                fwrite($file,$html);
            }
            $html = View::make('documento.acervo_item')->with('doc', $doc);
            fwrite($file,$html);
            $i++;
        }

        $html= View::make('documento.acervo_fim');
        fwrite($file,$html);
        fclose($file);
    }
    public function caminhoArquivo(){

    }
}