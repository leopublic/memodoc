<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('NivelTableSeeder');
	//	$this->call('UsuarioTableSeeder');
		$this->call('StatusOsTableSeeder');
	}

}