<?php

class NivelTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('usuario')->truncate();
    	DB::table('nivel')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $nivel = array("id_nivel" => "1", "descricao" => "Administrador cliente", "previlegio" => "1");
        DB::table('nivel')->insert($nivel);
        $nivel = array("id_nivel" => "2", "descricao" => "Operacional cliente", "previlegio" => "2");
        DB::table('nivel')->insert($nivel);
        $nivel = array("id_nivel" => "3", "descricao" => "Consulta cliente", "previlegio" => "3");
        DB::table('nivel')->insert($nivel);

        $nivel = array("id_nivel" => "12", "descricao" => "Diretoria", "previlegio" => "5");
        DB::table('nivel')->insert($nivel);
        $nivel = array("id_nivel" => "13", "descricao" => "Administrador", "previlegio" => "6");
        DB::table('nivel')->insert($nivel);
        $nivel = array("id_nivel" => "14", "descricao" => "Atendimento Nivel 1", "previlegio" => "3");
        DB::table('nivel')->insert($nivel);
        $nivel = array("id_nivel" => "15", "descricao" => "Atendimento Nível 2", "previlegio" => "4");
        DB::table('nivel')->insert($nivel);
        $nivel = array("id_nivel" => "16", "descricao" => "Financeiro", "previlegio" => "4");
        DB::table('nivel')->insert($nivel);
        $nivel = array("id_nivel" => "17", "descricao" => "Operacional", "previlegio" => "1");
        DB::table('nivel')->insert($nivel);
        $nivel = array("id_nivel" => "18", "descricao" => "Visitante", "previlegio" => "0");
        DB::table('nivel')->insert($nivel);
    }

}