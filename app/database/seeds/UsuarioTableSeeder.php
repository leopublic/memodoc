<?php

class UsuarioTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
  		DB::table('usuariointerno')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $usuario = array("id_usuario" => "4", "usuario"=> "JUNIOR", "password"=>  Hash::make("Junior"), "id_nivel"=> "13", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "12", "usuario"=> "CRIS", "password"=>  Hash::make("danniel"), "id_nivel"=> "15", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "3", "usuario"=> "LAILTON", "password"=>  Hash::make("churrasco de gato"), "id_nivel"=> "15", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "11", "usuario"=> "TESTE", "password"=>  Hash::make("teste"), "id_nivel"=> "17", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "7", "usuario"=> "ALEXANDRE", "password"=>  Hash::make("3112"), "id_nivel"=> "15", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "8", "usuario"=> "THIAGO", "password"=>  Hash::make("040482"), "id_nivel"=> "17", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "9", "usuario"=> "REGINALDO", "password"=>  Hash::make("2305"), "id_nivel"=> "17", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "10", "usuario"=> "ANDRE", "password"=>  Hash::make("1924"), "id_nivel"=> "17", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "13", "usuario"=> "RENATA", "password"=>  Hash::make("a123"), "id_nivel"=> "15", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "14", "usuario"=> "ANA", "password"=>  Hash::make("a123"), "id_nivel"=> "15", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "15", "usuario"=> "FABIO", "password"=>  Hash::make("a123"), "id_nivel"=> "15", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "18", "usuario"=> "PC", "password"=>  Hash::make("2269"), "id_nivel"=> "12", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "17", "usuario"=> "PEDRO", "password"=>  Hash::make("22698040"), "id_nivel"=> "12", "bloqueio"=> null);
        DB::table('usuariointerno')->insert($usuario);
        $usuario = array("id_usuario" => "19", "usuario"=> "FERNANDA", "password"=>  Hash::make("230501"), "id_nivel"=> "15", "bloqueio"=> null);
		DB::table('usuariointerno')->insert($usuario);
    }

}