<?php

class StatusOsTableSeeder extends Seeder {

	public function run()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('status_os')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

		$statusos = array('descricao' => 'incompleta', 'descricao_cliente' => 'incompleta');
		DB::table('status_os')->insert($statusos);
		$statusos = array('descricao' => 'nova', 'descricao_cliente' => 'nova');
		DB::table('status_os')->insert($statusos);
		$statusos = array('descricao' => 'pendente', 'descricao_cliente' => 'pendente');
		DB::table('status_os')->insert($statusos);
		$statusos = array('descricao' => 'atendida', 'descricao_cliente' => 'atendida');
		DB::table('status_os')->insert($statusos);
		$statusos = array('descricao' => 'cancelada', 'descricao_cliente' => 'cancelada');
		DB::table('status_os')->insert($statusos);
	}

}
