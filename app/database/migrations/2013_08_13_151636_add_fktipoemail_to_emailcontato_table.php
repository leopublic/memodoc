<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFktipoemailToEmailcontatoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emailcontato', function(Blueprint $table) {
            $table->foreign('id_tipoemail')->references('id_tipoemail')->on('tipoemail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emailcontato', function(Blueprint $table) {
            $table->dropForeign('emailcontato_id_tipoemail_foreign');
        });
    }

}
