<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableUsuario extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::unprepared("CREATE TABLE usuario
					(
					  id_usuario integer unsigned NOT NULL auto_increment,
					  username varchar(50) NOT NULL,
					  password varchar(64) NOT NULL,
					  adm char(1) NOT NULL,
					  id_cliente integer unsigned null ,
					  id_nivel integer unsigned NULL,
					  nivel integer unsigned NULL,
					  nomeusuario varchar(50) NOT NULL,
					  acessonumero integer DEFAULT 0,
					  validadelogin integer DEFAULT NULL,
					  aniversario varchar(5) DEFAULT NULL,
					  datacadastro date not null,
					  ultimoacesso timestamp,
					  bloqueio date null,
					  CONSTRAINT pk_usuario PRIMARY KEY (id_usuario)
				) engine=InnoDb");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuario');
	}

}