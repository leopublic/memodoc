<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkclienteToDepclienteTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dep_cliente', function(Blueprint $table) {
            $table->foreign('id_cliente')->references('id_cliente')->on('cliente');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dep_cliente', function(Blueprint $table) {
            $table->dropForeign('dep_cliente_id_cliente_foreign');            
        });
    }

}
