<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableOs extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE os
			(
			  id_os_chave integer unsigned not null auto_increment,
			  id_os integer unsigned not null ,
			  id_cliente integer unsigned NOT NULL,
			  id_departamento integer unsigned null,
			  id_endeentrega integer unsigned null,
			  num_os integer unsigned  NOT NULL,
			  responsavel varchar(50) null,
			  solicitado_em datetime NOT NULL,
			  entregar_em date NOT NULL,
			  entrega_pela integer not null DEFAULT 0,
			  tipodeemprestimo integer DEFAULT 0 NOT NULL,
			  qtdenovascaixas integer null,
			  apanhanovascaixas integer null,
			  apanhacaixasemprestadas integer null,
			  cobrafrete integer DEFAULT 1 NOT NULL,
			  observacao text null,
			  urgente tinyint not null DEFAULT 0,
			  valorfrete decimal(15,2) not null  DEFAULT 0  ,
			  valormovimento decimal(15,2) not null  DEFAULT 0,
			  valorcartonagem decimal(15,2) not null  DEFAULT 0,
			  cancelado tinyint not null DEFAULT 0,
			  tipo_os integer DEFAULT 0 NOT NULL,
			  naoecartonagemcortesia tinyint  not null DEFAULT 1,
			  CONSTRAINT pk_os PRIMARY KEY (id_os_chave)
			) engine=InnoDb ;

		");
        Schema::table('os', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('os');
    }

}
