<?php

use Illuminate\Database\Migrations\Migration;

class CreateViewsCaixasPorDepartamentoSetorCentro extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
 	public function up()
    {
    	$sql = "create view caixa_departamento as 
				select documento.id_cliente, id_caixapadrao, group_concat(distinct nome) departamentos, group_concat(distinct dep_cliente.id_departamento) ids
				from documento
				join dep_cliente ON dep_cliente.id_departamento = documento.id_departamento
				group by documento.id_cliente, id_caixapadrao";
        DB::statement( $sql );
        $sql = "create view caixa_setor as
				select documento.id_cliente, id_caixapadrao, group_concat(distinct nome) setores, group_concat(distinct set_cliente.id_setor) ids
				from documento
				join set_cliente ON set_cliente.id_setor = documento.id_setor
				group by documento.id_cliente, id_caixapadrao
				";
        DB::statement( $sql );
        $sql = "create view caixa_centro as
				select documento.id_cliente, id_caixapadrao, group_concat(distinct nome) centros, group_concat(distinct centrodecusto.id_centrodecusto) ids
				from documento
				join centrodecusto ON centrodecusto.id_centrodecusto= documento.id_centro
				group by documento.id_cliente, id_caixapadrao
				";
        DB::statement( $sql );
    }

    public function down()
    {
        DB::statement( 'DROP VIEW caixa_departamento' );
        DB::statement( 'DROP VIEW caixa_setor' );
        DB::statement( 'DROP VIEW caixa_centro' );
    }

}