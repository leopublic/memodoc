<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCartonagemClienteToOsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('os', function(Blueprint $table) {
			$table->integer('qtd_cartonagens_cliente')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('os', function(Blueprint $table) {
            $table->dropColumn('qtd_cartonagens_cliente');
		});
	}

}
