<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkNivelToUsuariointernoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuariointerno', function(Blueprint $table) {
            $table->foreign('id_nivel')->references('id_nivel')->on('nivel');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usuariointerno', function(Blueprint $table) {
            $table->dropForeign('usuariointerno_id_nivel_foreign');
        });
    }

}
