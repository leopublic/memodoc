<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableClienteEmpresa extends Migration {

    public function up() {
        Schema::create('usuario_cliente', function(Blueprint $table) {
            $table->increments('id_usuario_cliente');
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_cliente')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
		Schema::drop('usuario_cliente');
    }

}
