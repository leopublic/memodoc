<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkcontatoToTelefonecontatoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telefonecontato', function(Blueprint $table) {
            $table->foreign('id_contato')->references('id_contato')->on('contato');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telefonecontato', function(Blueprint $table) {
            $table->dropForeign('telefonecontato_id_contato_foreign');
        });
    }

}
