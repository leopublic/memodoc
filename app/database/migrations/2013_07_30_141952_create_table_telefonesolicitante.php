<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableTelefonesolicitante extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE telefonesolicitante
			(
			  id_telefonesolicitante integer unsigned NOT NULL auto_increment,
			  id_tipotelefone integer unsigned  null,
			  id_solicitante integer unsigned NOT NULL,
			  numero varchar(20)  null,
			  ramal varchar(5)  null,
			  CONSTRAINT pk_telefonesolicitante PRIMARY KEY (id_telefonesolicitante)
			) engine=InnoDb;

		");
        Schema::table('telefonesolicitante', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telefonesolicitante');
    }

}
