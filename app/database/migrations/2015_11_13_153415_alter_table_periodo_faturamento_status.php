<?php

use Illuminate\Database\Migrations\Migration;

class AlterTablePeriodoFaturamentoStatus extends Migration {
    public function up() {
		if (!Schema::hasColumn('periodo_faturamento', 'status_conclusao')){
			Schema::table('periodo_faturamento', function($table) {
	            $table->integer('status_conclusao')->unsigned()->null();
	        });
		}

        $sql = "update periodo_faturamento set status_conclusao = 2";
        DB::statement( $sql );
    }

    public function down() {
    }
}
