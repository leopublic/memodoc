<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDataToFaturamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('faturamento', function(Blueprint $table) {
			$table->date('dt_inicio')->nullable();
			$table->date('dt_fim')->nullable();
            $table->text('observacao')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('faturamento', function(Blueprint $table) {
			$table->dropColumn('dt_inicio');
			$table->dropColumn('dt_fim');
            $table->dropColumn('observacao');
		});
	}

}
