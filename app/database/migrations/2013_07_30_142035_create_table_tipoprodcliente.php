<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableTipoprodcliente extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE tipoprodcliente
			(
			  id_cliente integer unsigned NOT NULL,
			  id_tipoproduto integer unsigned NOT NULL,
			  valor_contrato decimal(10,2) NOT NULL,
			  valor_urgencia decimal(10,2) null,
			  minimo integer NOT NULL,
			  CONSTRAINT pk_tipoprodcliente PRIMARY KEY (id_cliente, id_tipoproduto)
			) engine=InnoDb;

		");
        Schema::table('tipoprodcliente', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipoprodcliente');
    }

}
