<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableContaCobranca extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('conta_cobranca', function($table) {
            $table->increments('id_conta_cobranca');
            $table->string('banco', 500)->nullable();
            $table->string('agencia', 20)->nullable();
            $table->string('conta', 20)->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('conta_cobranca');
	}

}