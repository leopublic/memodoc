<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdUsuarioToCheckoutTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('checkout', function(Blueprint $table) {
            $table->integer('id_usuario_cadastro')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('checkout', function(Blueprint $table) {
            $table->drop('id_usuario_cadastro');
        });
    }

}
