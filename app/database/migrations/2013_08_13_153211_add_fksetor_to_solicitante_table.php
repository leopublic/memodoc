<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFksetorToSolicitanteTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitante', function(Blueprint $table) {
            $table->foreign('id_setor')->references('id_setor')->on('set_cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitante', function(Blueprint $table) {
            $table->dropForeign('solicitante_id_setor_foreign');
        });
    }

}
