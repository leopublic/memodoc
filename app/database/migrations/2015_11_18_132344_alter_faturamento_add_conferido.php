<?php

use Illuminate\Database\Migrations\Migration;

class AlterFaturamentoAddConferido extends Migration {
	public function up(){
		if (!Schema::hasColumn('faturamento', 'conferido')){
			Schema::table('faturamento', function($table) {
	            $table->integer('conferido')->unsigned()->null();
	        });
		}
	}

	public function down()
	{
	}

}
