<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFktipotelefoneToTelefonesolicitanteTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telefonesolicitante', function(Blueprint $table) {
            $table->foreign('id_tipotelefone')->references('id_tipotelefone')->on('tipotelefone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telefonesolicitante', function(Blueprint $table) {
            $table->dropForeign('telefonesolicitante_id_tipotelefone_foreign');
        });
    }

}
