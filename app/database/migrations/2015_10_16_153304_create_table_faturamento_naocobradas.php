<?php
use Illuminate\Database\Migrations\Migration;

class CreateTableFaturamentoNaocobradas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('faturamento_naocobradas', function ($table) {
            $table->increments('id_faturamento_naocobradas');
            $table->integer('id_faturamento')->unsigned();
            $table->integer('id_caixa')->unsigned();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('faturamento_naocobradas');
    }
}
