<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFktipoemailToEmailclienteTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('emailcliente')->where('id_tipoemail', 0)->update(array('id_tipoemail' => null));

        Schema::table('emailcliente', function(Blueprint $table) {
            $table->foreign('id_tipoemail')->references('id_tipoemail')->on('tipoemail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emailcliente', function(Blueprint $table) {
            $table->dropForeign('emailcliente_id_tipoemail_foreign');
        });
    }

}
