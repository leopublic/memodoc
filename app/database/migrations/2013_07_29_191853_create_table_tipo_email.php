<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableTipoEmail extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$sql = "CREATE TABLE tipoemail
					(
					  id_tipoemail integer unsigned NOT NULL auto_increment,
					  descricao varchar(100) NOT NULL ,
					  CONSTRAINT pk_tipoemail PRIMARY KEY (id_tipoemail)
					)engine=InnoDb ;";
		DB::unprepared($sql);
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipoemail');
	}

}