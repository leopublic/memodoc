<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdCheckoutToOsdesmembradaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->index('id_os_chave');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->dropIndex('osdesmembrada_id_os_chave');
        });
	}

}