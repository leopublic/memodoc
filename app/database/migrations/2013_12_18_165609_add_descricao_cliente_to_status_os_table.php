<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDescricaoClienteToStatusOsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('status_os', function(Blueprint $table) {
			$table->string('descricao_cliente', 100)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('status_os', function(Blueprint $table) {
            $table->dropColumn('descricao_cliente');
		});
	}

}
