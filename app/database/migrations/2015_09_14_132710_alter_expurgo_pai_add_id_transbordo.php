<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterExpurgoPaiAddIdTransbordo extends Migration {

    public function up() {
        Schema::table('expurgo_pai', function(Blueprint $table) {
            $table->integer('id_transbordo')->unsigned()->null();
        });

        $sql = "update expurgo_pai set id_transbordo = (select id_transbordo from transbordo where id_expurgo_pai = expurgo_pai.id_expurgo_pai)";
        DB::statement( $sql );
    }

    public function down() {
        Schema::table('expurgo_pai', function(Blueprint $table) {
            $table->dropColumn('id_transbordo');
        });
    }


}