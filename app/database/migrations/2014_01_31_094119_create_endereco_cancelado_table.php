<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEnderecoCanceladoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('endereco_cancelado', function(Blueprint $table) {
			$table->integer('id_caixa')->unsigned();
            $table->integer('id_galpao')->unsigned();
            $table->integer('id_rua')->unsigned();
            $table->integer('id_predio')->unsigned();
            $table->integer('id_andar')->unsigned();
            $table->integer('id_unidade')->unsigned();
            $table->integer('id_cliente')->unsigned();
            $table->integer('id_tipodocumento')->unsigned();
            $table->boolean('disponivel')->nullable();
            $table->boolean('reservado')->nullable();
            $table->boolean('reserva')->nullable();
            $table->boolean('endcancelado')->nullable();
            $table->string('observacao', 2000)->nullable();
            $table->integer('id_usuario')->unsigned();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('endereco_cancelado');
	}

}
