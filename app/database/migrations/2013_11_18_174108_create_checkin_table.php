<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCheckinTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('checkin', function(Blueprint $table) {
			$table->increments('id_checkin');
			$table->integer('id_os_chave')->unsigned();
			$table->integer('id_usuario')->unsigned();
			$table->integer('id_cliente')->unsigned();
			$table->integer('id_caixa')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('checkin');
	}

}
