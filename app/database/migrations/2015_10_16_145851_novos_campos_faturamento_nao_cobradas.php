<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class NovosCamposFaturamentoNaoCobradas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
        Schema::table('faturamento', function(Blueprint $table) {
            $table->integer('qtd_custodia_nao_cobrada')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		if (Schema::hasColumn('faturamento', 'qtd_custodia_nao_cobrada')){
	        Schema::table('faturamento', function(Blueprint $table) {
				$table->dropColumn('qtd_custodia_nao_cobrada');
			});
		}
	}

}