<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIndexIdCaixapadraoToOsDesemembradaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->index('id_cliente', 'id_caixapadrao');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('osdesmembrada', function(Blueprint $table) {

		});
	}

}
