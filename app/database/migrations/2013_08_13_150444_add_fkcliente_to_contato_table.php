<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkclienteToContatoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contato', function(Blueprint $table) {
            $table->foreign('id_cliente')->references('id_cliente')->on('cliente');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contato', function(Blueprint $table) {
            $table->dropForeign('contato_id_cliente_foreign');            
        });
    }

}
