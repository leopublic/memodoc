<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkSetclienteToDocumentoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documento', function(Blueprint $table) {
            $table->foreign('id_setor')->references('id_setor')->on('set_cliente');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documento', function(Blueprint $table) {
            $table->dropForeign('documento_id_setor_foreign');
        });
    }

}
