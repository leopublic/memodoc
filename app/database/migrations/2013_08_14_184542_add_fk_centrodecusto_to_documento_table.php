<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkCentrodecustoToDocumentoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documento', function(Blueprint $table) {
            $table->foreign('id_centro')->references('id_centrodecusto')->on('centrodecusto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documento', function(Blueprint $table) {
            $table->dropForeign('documento_id_centro_foreign');
        });
    }

}
