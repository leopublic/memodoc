<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDocumentofkToReservadesemembradaTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservadesmembrada', function(Blueprint $table) {
            $table->foreign('id_caixa')->references('id_caixa')->on('endereco');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservadesmembrada', function(Blueprint $table) {
            $table->dropForeign('reservadesmembrada_id_caixa_foreign');
        });
    }

}
