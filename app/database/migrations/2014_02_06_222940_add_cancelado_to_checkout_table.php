<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCanceladoToCheckoutTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('checkout', function(Blueprint $table) {
            $table->boolean('fl_cancelado')->nullable();
            $table->integer('id_usuario_cancelamento')->unsigned()->nullable();
            $table->dateTime('dt_cancelamento')->nullable();
            $table->string('observacao_cancelamento', 1000)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('checkout', function(Blueprint $table) {
			$table->dropColumn('fl_cancelado');
			$table->dropColumn('id_usuario_cancelamento');
			$table->dropColumn('dt_cancelamento');
			$table->dropColumn('observacao_cancelamento');

		});
	}

}
