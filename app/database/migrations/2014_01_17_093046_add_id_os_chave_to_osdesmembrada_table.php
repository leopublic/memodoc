<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdOsChaveToOsdesmembradaTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->integer('id_os_chave')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->dropColumn('id_os_chave');
        });
    }

}
