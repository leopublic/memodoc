<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddValorurgenciaToFaturamentoServicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('faturamento_servico', function(Blueprint $table) {
			if(Schema::hasColumn('faturamento_servico', 'valor')){
                $table->dropColumn('valor');
            }
            $table->decimal('valor_contrato', 10, 2)->nullable();
            $table->decimal('valor_urgencia', 10, 2)->nullable();
            $table->decimal('minimo', 10, 2)->nullable();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('faturamento_servico', function(Blueprint $table) {
			if(Schema::hasColumn('faturamento_servico', 'valor_contrato')){
                $table->dropColumn('valor_contrato');
            }
			if(Schema::hasColumn('faturamento_servico', 'valor_urgencia')){
                $table->dropColumn('valor_urgencia');
            }
			if(Schema::hasColumn('faturamento_servico', 'minimo')){
                $table->dropColumn('minimo');
            }
		});
	}

}
