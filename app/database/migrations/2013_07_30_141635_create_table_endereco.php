<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableEndereco extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE endereco
			(
			  id_caixa integer unsigned NOT NULL auto_increment,
			  id_galpao integer unsigned NOT NULL,
			  id_rua integer unsigned NOT NULL,
			  id_predio integer unsigned NOT NULL,
			  id_andar integer unsigned NOT NULL,
			  id_unidade integer unsigned NOT NULL,
			  id_cliente integer unsigned null,
			  id_tipodocumento integer unsigned NOT NULL,
			  disponivel tinyint  not null DEFAULT 1,
			  reservado tinyint  not null DEFAULT 0,
			  reserva tinyint not null DEFAULT 0,
			  endcancelado tinyint not null DEFAULT 0,
			  CONSTRAINT pk_endereco PRIMARY KEY (id_caixa)
			) engine=InnoDb;


		");
        Schema::table('endereco', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('endereco');
    }

}
