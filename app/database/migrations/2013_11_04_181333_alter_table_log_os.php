<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTableLogOs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('log_os', function(Blueprint $table) {
			$table->integer('id_status_os')->unsigned()->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasColumn('log_os', 'id_status_os')){
			Schema::table('log_os', function(Blueprint $table) {
				$table->dropColumn('id_status_os');
			});
		}
	}

}
