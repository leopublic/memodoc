<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableEmailcontato extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::unprepared("CREATE TABLE emailcontato
					(
					  id_emailcontato integer unsigned NOT NULL auto_increment,
					  id_tipoemail integer unsigned,
					  id_contato integer unsigned NOT NULL,
					  id_cliente integer unsigned NOT NULL,
					  correioeletronico varchar(200),
					  CONSTRAINT pk_emailcontato PRIMARY KEY (id_emailcontato)
					) engine=InnoDb;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emailcontato');
	}

}