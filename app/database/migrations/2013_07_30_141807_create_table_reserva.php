<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableReserva extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE reserva
			(
			  id_reserva integer unsigned NOT NULL auto_increment,
			  id_cliente integer unsigned NOT NULL,
			  data_reserva date NOT NULL,
			  caixas integer unsigned NOT NULL,
			  id_tipodocumento integer unsigned NOT NULL,
			  inventario_impresso tinyint DEFAULT 0 NOT NULL,
			  CONSTRAINT pk_reserva PRIMARY KEY (id_reserva)
			) engine=InnoDb;

		");
        Schema::table('reserva', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reserva');
    }

}
