<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableAddTimestamp extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cliente'			, function($table){ $table->timestamps();});
		Schema::table('dep_cliente'		, function($table){ $table->timestamps();});
		Schema::table('tipoemail'		, function($table){ $table->timestamps();});
		Schema::table('tipotelefone'	, function($table){ $table->timestamps();});
		Schema::table('usuario'			, function($table){ $table->timestamps();});
		Schema::table('nivel'			, function($table){ $table->timestamps();});
		Schema::table('centrodecusto'	, function($table){ $table->timestamps();});
		Schema::table('contato'			, function($table){ $table->timestamps();});
		Schema::table('emailcontato'	, function($table){ $table->timestamps();});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}