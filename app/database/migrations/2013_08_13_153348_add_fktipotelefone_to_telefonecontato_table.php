<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFktipotelefoneToTelefonecontatoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('telefonecontato')->where('id_tipotelefone', 0)->update(array('id_tipotelefone' => null));
        
        Schema::table('telefonecontato', function(Blueprint $table) {
            $table->foreign('id_tipotelefone')->references('id_tipotelefone')->on('tipotelefone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telefonecontato', function(Blueprint $table) {
            $table->dropForeign('telefonecontato_id_tipotelefone_foreign');
        });
    }

}
