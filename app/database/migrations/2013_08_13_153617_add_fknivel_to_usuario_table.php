<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFknivelToUsuarioTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('usuario')->where('id_nivel', 0)->update(array('id_nivel' => null));

        Schema::table('usuario', function(Blueprint $table) {
            $table->foreign('id_nivel')->references('id_nivel')->on('nivel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usuario', function(Blueprint $table) {
            $table->dropForeign('usuario_id_nivel_foreign');
        });
    }

}
