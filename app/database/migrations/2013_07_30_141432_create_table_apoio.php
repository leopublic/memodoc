<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableApoio extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE apoio
			(
			  id_galpao int unsigned null,
			  id_andar  int unsigned null,
			  id_tipodocumento  int unsigned null,
			  disponivel  int unsigned null
			) engine=InnoDb;
		");
        Schema::table('apoio', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apoio');
    }

}
