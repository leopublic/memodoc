<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableTipodocumento extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE tipodocumento
			(
			  id_tipodocumento integer unsigned NOT NULL auto_increment,
			  descricao varchar(20) NOT NULL,
			  CONSTRAINT pk_tipodocumento PRIMARY KEY (id_tipodocumento)
			) engine=InnoDb;

		");
        Schema::table('tipodocumento', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipodocumento');
    }

}
