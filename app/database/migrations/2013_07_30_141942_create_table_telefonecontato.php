<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableTelefonecontato extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE telefonecontato
			(
			  id_telefonecontato integer unsigned NOT NULL auto_increment,
			  id_tipotelefone integer unsigned  null,
			  id_contato integer  unsigned NOT NULL,
			  id_cliente integer  unsigned NOT NULL,
			  ddd varchar(5) null,
			  numero varchar(20)  null,
			  ramal varchar(5)  null,
			  CONSTRAINT pk_telefonecontato PRIMARY KEY (id_telefonecontato)
			) engine=InnoDb;

		");
        Schema::table('telefonecontato', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telefonecontato');
    }

}
