<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentoCanceladoTable extends Migration {

    public function up() {
        DB::unprepared("
			CREATE TABLE documento_cancelado
			(
			  id_documento int unsigned not null auto_increment,
			  id_cliente int unsigned not null,
			  id_caixapadrao int unsigned not null,
			  id_box int unsigned not null,
			  id_item int unsigned not null,
			  id_departamento int unsigned null,
			  id_setor int unsigned null,
			  id_centro int unsigned null,
			  id_tipodocumento int unsigned not null,
			  id_caixa int unsigned not null,
			  id_reserva int unsigned null,
			  id_expurgo int unsigned null,
			  titulo varchar(250)  NULL ,
			  migrada_de varchar(22) null,
			  inicio date null,
			  fim date null,
			  expurgo_programado tinyint null,
			  expurgarem date null,
			  conteudo text null,
			  usuario varchar(50)  null,
			  emcasa tinyint DEFAULT 0 NOT NULL,
			  primeira_entrada datetime NULL,
			  data_digitacao date NULL,
			  expurgada_em date null,
			  itemalterado tinyint not null DEFAULT 1,
			  planilha_eletronica integer DEFAULT 0 NOT NULL,
			  request_recall varchar(22) null,
			  nume_inicial integer null,
			  nume_final integer null,
			  alfa_inicial char(1) null,
			  alfa_final char(1) null,
              id_usuario int unsigned not null,
              evento varchar(1000) null,
              created_at_origin datetime null,
              updated_at_origin datetime null,
			  CONSTRAINT pk_documento PRIMARY KEY (id_documento)
			) engine=InnoDb;
		");
        Schema::table('documento_cancelado', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('documento_cancelado');
    }

}
