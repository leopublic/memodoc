<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTablePagamento extends Migration {


    public function up() {
        Schema::table('pagamento', function(Blueprint $table) {
            $table->string('nosso_numero', 100)->null();
            $table->integer('linha')->unsigned()->null();
        });
    }

    public function down() {
        if(Schema::hasColumn('pagamento', 'nosso_numero')){
            Schema::table('pagamento', function(Blueprint $table) {
                $table->dropColumn('nosso_numero');
            });
        }
        if(Schema::hasColumn('pagamento', 'linha')){
            Schema::table('pagamento', function(Blueprint $table) {
                $table->dropColumn('linha');
            });
        }
    }
}
