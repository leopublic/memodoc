<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkcentrodecustoToSolicitanteTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitante', function(Blueprint $table) {
            $table->foreign('id_centrocusto')->references('id_centrodecusto')->on('centrodecusto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitante', function(Blueprint $table) {
            $table->dropForeign('solicitante_id_centrocusto_foreign');
        });
    }

}
