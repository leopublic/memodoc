<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DropIdTelefoneFromTelefonecliente extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('telefonecliente', 'id_telefone')) {
            Schema::table('telefonecliente', function(Blueprint $table) {
                $table->dropColumn('id_telefone');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telefonecliente', function(Blueprint $table) {
            
        });
    }

}
