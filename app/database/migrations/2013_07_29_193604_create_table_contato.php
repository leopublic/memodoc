<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableContato extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::unprepared("CREATE TABLE contato
				(
				  id_contato integer unsigned NOT NULL auto_increment,
				  id_cliente integer unsigned NOT NULL,
				  nome varchar(200) ,
				  cargo varchar(200) ,
				  CONSTRAINT pk_contato PRIMARY KEY (id_contato)
				) engine=InnoDb;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contato');
	}

}