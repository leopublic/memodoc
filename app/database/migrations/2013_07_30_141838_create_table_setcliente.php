<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableSetcliente extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE set_cliente
			(
			  id_setor integer unsigned NOT NULL auto_increment,
			  id_cliente integer unsigned NOT NULL,
			  nome varchar(20) not null,
			  CONSTRAINT pk_set_cliente PRIMARY KEY (id_setor)
			) engine=InnoDb;

		");
        Schema::table('set_cliente', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('set_cliente');
    }

}
