<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableEmailsolicitante extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE emailsolicitante
			(
			  id_emailsolicitante integer unsigned NOT NULL auto_increment,
			  id_tipoemail integer unsigned null,
			  id_solicitante integer unsigned NOT NULL,
			  correioeletronico varchar(50)  null,
			  CONSTRAINT pk_emailsolicitante PRIMARY KEY (id_emailsolicitante)
			) engine=InnoDb;

		");
        Schema::table('emailsolicitante', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emailsolicitante');
    }

}
