<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFaturamentoServicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('faturamento_servico', function(Blueprint $table) {
			$table->increments('id_faturamento_servico');
			$table->integer('id_faturamento')->unsigned();
			$table->integer('id_cliente')->unsigned();
			$table->integer('id_tipoproduto')->unsigned();
			$table->decimal('qtd', 10,2)->unsigned()->nullable();

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('faturamento_servico');
	}

}
