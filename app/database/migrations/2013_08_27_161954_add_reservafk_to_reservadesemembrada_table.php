<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddReservafkToReservadesemembradaTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservadesmembrada', function(Blueprint $table) {
            $table->foreign('id_reserva')->references('id_reserva')->on('reserva');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservadesmembrada', function(Blueprint $table) {
            $table->dropForeign('reservadesmembrada_id_reserva_foreign');
        });
    }

}
