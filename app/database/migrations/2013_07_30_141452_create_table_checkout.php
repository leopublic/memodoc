<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableCheckout extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE checkout
			(
			  id_checkout int unsigned NOT NULL auto_increment,
			  id_cliente int unsigned NOT NULL ,
			  id_caixapadrao int unsigned NOT NULL ,
			  id_caixa int unsigned NOT NULL ,
			  id_os int unsigned NOT NULL ,
			  status integer NOT NULL,
			  data_inclusao_os datetime null,
			  data_checkout datetime  null,
			  CONSTRAINT pk_checkout PRIMARY KEY (id_checkout)
			) engine=InnoDb;
		");
        Schema::table('checkout', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('checkout');
    }

}
