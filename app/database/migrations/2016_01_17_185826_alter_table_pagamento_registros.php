<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTablePagamentoRegistros extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (! \Schema::hasColumn('pagamento', 'id_pagador')){
			\Schema::table('pagamento', function(Blueprint $table) {
	            $table->integer('id_pagador')->null();
	        });
		}
		if (! \Schema::hasColumn('pagamento', 'fl_corrigido')){
			\Schema::table('pagamento', function(Blueprint $table) {
	            $table->boolean('fl_corrigido')->null();
	        });
		}
		if (! \Schema::hasColumn('pagamento', 'fl_duplicidade_cnpj')){
			\Schema::table('pagamento', function(Blueprint $table) {
	            $table->boolean('fl_duplicidade_cnpj')->null();
	        });
		}
		if (! \Schema::hasColumn('pagamento', 'registro_boleto')){
			\Schema::table('pagamento', function(Blueprint $table) {
	            $table->text('registro_boleto')->null();
	        });
		}
		if (! \Schema::hasColumn('pagamento', 'registro_pagamento')){
			\Schema::table('pagamento', function(Blueprint $table) {
	            $table->text('registro_pagamento')->null();
	        });
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (\Schema::hasColumn('pagamento', 'id_pagador')){
			\Schema::table('pagamento', function(Blueprint $table) {
	            $table->dropColumn('id_pagador');
	        });
		}
		if (\Schema::hasColumn('pagamento', 'fl_corrigido')){
			\Schema::table('pagamento', function(Blueprint $table) {
	            $table->dropColumn('fl_corrigido');
	        });
		}
		if (\Schema::hasColumn('pagamento', 'fl_duplicidade_cnpj')){
			\Schema::table('pagamento', function(Blueprint $table) {
	            $table->dropColumn('fl_duplicidade_cnpj');
	        });
		}
		if (\Schema::hasColumn('pagamento', 'registro_boleto')){
			\Schema::table('pagamento', function(Blueprint $table) {
	            $table->dropColumn('registro_boleto');
	        });
		}
		if (! \Schema::hasColumn('pagamento', 'registro_pagamento')){
			\Schema::table('pagamento', function(Blueprint $table) {
	            $table->dropColumn('registro_pagamento');
	        });
		}
	}
}
