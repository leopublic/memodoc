<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHistoricoTipoprodutoclienteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('hist_tipoprodutocliente')){
			Schema::create('hist_tipoprodutocliente', function(Blueprint $table) {
				$table->increments('id_hist');
				$table->datetime('data');
				$table->integer('id_cliente')->unsigned()->null();
				$table->integer('id_tipoproduto')->unsigned()->null();
				$table->decimal('valor_contrato', 10, 2)->unsigned()->null();
				$table->decimal('valor_urgencia', 10, 2)->unsigned()->null();
				$table->integer('minimo')->unsigned()->null();
			
				$table->timestamps();
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('hist_tipoprodutocliente')){
			Schema::drop('hist_tipoprodutocliente');			
		}
	}

}
