<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDtFaturamentoToFaturamentoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('faturamento', function(Blueprint $table) {
            $table->dateTime('dt_faturamento')->nullable();
            $table->integer('qtd_custodia_mes_anterior')->nullable();
            $table->integer('qtd_custodia_periodo')->nullable();
            $table->integer('qtd_custodia_em_carencia')->nullable();
            $table->integer('qtd_custodia_novas')->nullable();
            $table->integer('qtd_cartonagem_enviadas')->nullable();
            $table->integer('qtd_retornos_de_consulta')->nullable();
            $table->integer('qtd_frete')->nullable();
            $table->integer('qtd_frete_urgente')->nullable();
            $table->integer('qtd_movimentacao')->nullable();
            $table->integer('qtd_movimentacao_urgente')->nullable();
            $table->integer('qtd_expurgos')->nullable();

            $table->decimal('val_custodia_periodo', 10, 2)->nullable();
            $table->decimal('val_cartonagem_enviadas', 10, 2)->nullable();
            $table->decimal('val_enviadas_consulta', 10, 2)->nullable();
            $table->decimal('val_frete', 10, 2)->nullable();
            $table->decimal('val_frete_urgente', 10, 2)->nullable();
            $table->decimal('val_movimentacao', 10, 2)->nullable();
            $table->decimal('val_movimentacao_urgente', 10, 2)->nullable();
            $table->decimal('val_expurgos', 10, 2)->nullable();
            $table->decimal('val_servicos_sem_iss', 10, 2)->nullable();
            $table->decimal('val_outros', 10, 2)->nullable();
            $table->decimal('val_iss', 10, 2)->nullable();
            $table->decimal('val_total', 10, 2)->nullable();
            $table->decimal('val_dif_minimo', 10, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('faturamento', function(Blueprint $table) {
            $table->dropColumn('dt_faturamento');
            $table->dropColumn('qtd_custodia_mes_anterior');
            $table->dropColumn('qtd_custodia_periodo');
            $table->dropColumn('qtd_custodia_em_carencia');
            $table->dropColumn('qtd_custodia_novas');
            $table->dropColumn('qtd_cartonagem_enviadas');
            $table->dropColumn('qtd_retornos_de_consulta');
            $table->dropColumn('qtd_frete');
            $table->dropColumn('qtd_frete_urgente');
            $table->dropColumn('qtd_movimentacao');
            $table->dropColumn('qtd_movimentacao_urgente');
            $table->dropColumn('qtd_expurgos');

            $table->dropColumn('val_custodia_periodo');
            $table->dropColumn('val_cartonagem_enviadas');
            $table->dropColumn('val_enviadas_consulta');
            $table->dropColumn('val_frete');
            $table->dropColumn('val_frete_urgente');
            $table->dropColumn('val_movimentacao');
            $table->dropColumn('val_movimentacao_urgente');
            $table->dropColumn('val_expurgos');
            $table->dropColumn('val_servicos_sem_iss');
            $table->dropColumn('val_outros');
            $table->dropColumn('val_iss');
            $table->dropColumn('val_total');
            $table->dropColumn('val_dif_minimo');
        });
    }

}
