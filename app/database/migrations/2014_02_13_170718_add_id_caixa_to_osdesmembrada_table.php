<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdCaixaToOsdesmembradaTable extends Migration {

    public function up() {
        Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->integer('id_caixa')->unsigned();
        });
    }

    public function down() {
        Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->dropColumn('id_caixa');
        });
    }

}
