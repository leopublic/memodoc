<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddEmAtrasoDesdeToClienteTable extends Migration {

    public function up() {
        Schema::table('cliente', function(Blueprint $table) {
            $table->date('em_atraso_desde')->nullable();
        });
    }

    public function down() {
        Schema::table('cliente', function(Blueprint $table) {
            $table->dropColumn('em_atraso_desde');
        });
    }

}
