<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCaixasToOsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('os', function(Blueprint $table) {
			$table->integer('qtd_caixas_novas_cliente')->unsigned()->nullable();
			$table->integer('qtd_caixas_retiradas_cliente')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('os', function(Blueprint $table) {
			$table->dropColumn('qtd_caixas_novas_cliente');
			$table->dropColumn('qtd_caixas_retiradas_cliente');
		});
	}

}
