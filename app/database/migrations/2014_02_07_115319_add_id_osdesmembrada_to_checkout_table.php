<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdOsdesmembradaToCheckoutTable extends Migration {

    public function up() {
        Schema::table('checkout', function(Blueprint $table) {
            $table->integer('id_osdesmembrada')->unsigned()->nullable();
        });
    }

    public function down() {
        Schema::table('checkout', function(Blueprint $table) {
            $table->dropColumn('id_osdesmembrada');
        });
    }

}
