<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFaturamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('faturamento', function(Blueprint $table) {
			$table->increments('id_faturamento');
			$table->integer('id_cliente')->unsigned();
			$table->integer('mes')->unsigned();
			$table->integer('ano')->unsigned();
			$table->integer('id_usuario')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('faturamento');
	}

}
