<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFktipodocumentoToEnderecoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('endereco', function(Blueprint $table) {
            $table->foreign('id_tipodocumento')->references('id_tipodocumento')->on('tipodocumento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('endereco', function(Blueprint $table) {
            $table->dropForeign('endereco_id_tipodocumento_foreign');
        });
    }

}
