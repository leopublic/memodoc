<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariocentrocustoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('usuariocentrocusto')){		
			Schema::create('usuariocentrocusto', function(Blueprint $table) {
				$table->increments('id_usuariocentrocusto');
	    		$table->integer('id_usuario')->unsigned()->nullable();
	    		$table->integer('id_cliente')->unsigned()->nullable();
	  			$table->integer('id_centrocusto')->unsigned()->nullable();
				$table->timestamps();
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(!Schema::hasTable('usuariocentrocusto')){
			Schema::drop('usuariocentrocusto');
		}
	}

}
