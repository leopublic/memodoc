<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdUsuarioCancelamentoToReservaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reserva', function(Blueprint $table) {
			$table->integer('id_usuario_cancelamento')->unsigned()->nullable();
            $table->dateTime('data_cancelamento')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reserva', function(Blueprint $table) {
            $table->dropColumn('id_usuario_cancelamento');
            $table->dropColumn('data_cancelamento');
		});
	}

}
