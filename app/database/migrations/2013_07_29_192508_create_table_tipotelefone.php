<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableTipotelefone extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::unprepared("CREATE TABLE tipotelefone
				(
				  id_tipotelefone integer unsigned NOT NULL auto_increment,
				  descricao varchar(100) NOT NULL ,
				  CONSTRAINT pk_tipotelefone PRIMARY KEY (id_tipotelefone)
				) engine=InnoDb ");		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tipotelefone');
	}

}