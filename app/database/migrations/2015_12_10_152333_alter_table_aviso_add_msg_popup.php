<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableAvisoAddMsgPopup extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasColumn('aviso', 'texto_popup')){
			Schema::table('aviso', function($table) {
				$table->text('texto_popup')->nullable();
			});
		}

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasColumn('aviso', 'texto_popup')){
	        Schema::table('aviso', function($table) {
				$table->dropColumn('texto_popup');
			});
		}
	}

}
