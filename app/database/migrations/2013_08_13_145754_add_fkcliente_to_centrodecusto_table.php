<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkclienteToCentrodecustoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('centrodecusto', function(Blueprint $table) {
            $table->foreign('id_cliente')->references('id_cliente')->on('cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('centrodecusto', function(Blueprint $table) {
            $table->dropForeign('centrodecusto_id_cliente_foreign');
        });
    }

}
