<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableGalpao extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('galpao');
		Schema::create('galpao', function($table) {
            $table->increments('id_galpao');
            $table->boolean('fl_disponivel_enderecamento')->nullable();
            $table->timestamps();
        });
		$sql = "insert into galpao(id_galpao, fl_disponivel_enderecamento) values (1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (6, 0)";
        DB::statement( $sql );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('galpao');
	}

}
