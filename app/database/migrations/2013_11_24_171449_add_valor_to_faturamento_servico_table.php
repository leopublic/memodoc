<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddValorToFaturamentoServicoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('faturamento_servico', function(Blueprint $table) {
            if(Schema::hasColumn('faturamento_servico', 'id_cliente')){
                $table->dropColumn('id_cliente');
            }
            $table->decimal('valor', 10, 2)->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('faturamento_servico', function(Blueprint $table) {
            if(Schema::hasColumn('faturamento_servico', 'valor')){
                $table->dropColumn('valor');
            }
		});
	}

}
