<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariodepartamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if(!Schema::hasTable('usuariodepartamento')){
			Schema::create('usuariodepartamento', function(Blueprint $table) {
	  			$table->increments('id_usuariodepartamento');
	  			$table->integer('id_usuario')->unsigned()->nullable();
	    		$table->integer('id_cliente')->unsigned()->nullable();
	  			$table->integer('id_departamento')->unsigned()->nullable();
				$table->timestamps();
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('usuariodepartamento')){
			Schema::drop('usuariodepartamento');
		}
	}

}
