<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableTipoproduto extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE tipoproduto
			(
			  id_tipoproduto integer unsigned NOT NULL auto_increment,
			  descricao varchar(50) NOT NULL ,
			  valor numeric(15,2) NOT NULL,
			  id_servico integer unsigned NOT NULL,
			  descricao_s varchar(30) NOT NULL,
			  CONSTRAINT pk_tipoproduto PRIMARY KEY (id_tipoproduto)
			) engine=InnoDb;

		");
        Schema::table('tipoproduto', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipoproduto');
    }

}
