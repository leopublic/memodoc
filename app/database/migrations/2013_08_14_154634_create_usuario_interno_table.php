<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuarioInternoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuariointerno', function(Blueprint $table) {
            $table->increments('id_usuario');
            $table->string('usuario', 30);
            $table->string('password', 64);
            $table->integer('id_nivel')->unsigned()->nullable();
            $table->dateTime('bloqueio')->nullable();
            $table->timestamp('ultimoacesso')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuariointerno');
    }

}
