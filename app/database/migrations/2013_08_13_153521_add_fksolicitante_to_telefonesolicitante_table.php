<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFksolicitanteToTelefonesolicitanteTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telefonesolicitante', function(Blueprint $table) {
            $table->foreign('id_solicitante')->references('id_solicitante')->on('solicitante');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telefonesolicitante', function(Blueprint $table) {
            $table->dropForeign('telefonesolicitante_id_solicitante_foreign');
        });
    }

}
