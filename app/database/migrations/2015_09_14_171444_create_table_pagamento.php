<?php

use Illuminate\Database\Migrations\Migration;

class CreateTablePagamento extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('pagamento', function($table) {
            $table->increments('id_pagamento');
            $table->integer('id_arquivo_retorno')->unsigned();
            $table->integer('id_cliente')->unsigned()->nullable();
            $table->integer('id_faturamento')->unsigned()->nullable();
            $table->string('cnpj', 30)->nullable();
            $table->string('sacado', 500)->nullable();
            $table->date('data_emissao')->nullable();
            $table->date('data_vencimento')->nullable();
            $table->date('data_pagamento')->nullable();
            $table->decimal('valor', 12,2)->nullable();
            $table->decimal('valor_pago', 12,2)->nullable();
            $table->string('situacao', 100)->nullable();
            $table->integer('titulo')->unsigned()->nullable();
            $table->string('nosso_numero', 30)->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pagamento');
	}

}