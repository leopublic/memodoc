<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkclienteToCheckoutTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checkout', function(Blueprint $table) {
            $table->foreign('id_cliente')->references('id_cliente')->on('cliente');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checkout', function(Blueprint $table) {
            $table->dropForeign('checkout_id_cliente_foreign');
        });
    }

}
