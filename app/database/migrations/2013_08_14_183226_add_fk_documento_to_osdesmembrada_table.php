<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkDocumentoToOsdesmembradaTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->foreign('id_documento')->references('id_documento')->on('documento');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->dropForeign('osdesmembrada_id_documento_foreign');
        });
    }

}
