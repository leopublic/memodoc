<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariosetorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuariosetor', function(Blueprint $table) {
			$table->increments('id_usuariosetor');
  			$table->integer('id_usuario')->unsigned()->nullable();
    		$table->integer('id_cliente')->unsigned()->nullable();
  			$table->integer('id_setor')->unsigned()->nullable();			
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasTable('usuariosetor')){
			Schema::drop('usuariosetor');			
		}
	}

}
