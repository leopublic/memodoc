<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdUsuarioToExpurgoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('expurgo', function(Blueprint $table) {
            $table->integer('id_usuario_cadastro')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('expurgo', function(Blueprint $table) {
            $table->dropColumn('id_usuario_cadastro');
		});
	}

}
