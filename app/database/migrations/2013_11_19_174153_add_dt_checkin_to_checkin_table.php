<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDtCheckinToCheckinTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('checkin', function(Blueprint $table) {
			$table->integer('id_usuario_checkin')->unsigned()->nullable();
			$table->dateTime('dt_checkin')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('checkin', function(Blueprint $table) {
            $table->dropColumn('id_usuario_checkin');
            $table->dropColumn('dt_checkin');
		});
	}

}
