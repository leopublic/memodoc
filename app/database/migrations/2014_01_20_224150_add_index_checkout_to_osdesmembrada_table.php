<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIndexCheckoutToOsdesmembradaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->index(array('id_checkout', 'id_caixapadrao'));
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->dropIndex('osdesmembrada_id_checkout_id_caixapadrao');
        });
	}

}