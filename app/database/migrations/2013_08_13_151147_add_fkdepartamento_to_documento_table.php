<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkdepartamentoToDocumentoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documento', function(Blueprint $table) {
            $table->foreign('id_departamento')->references('id_departamento')->on('dep_cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documento', function(Blueprint $table) {
            $table->dropForeign('documento_id_departamento_foreign');
        });
    }

}
