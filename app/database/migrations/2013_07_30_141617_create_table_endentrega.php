<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableEndentrega extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE endeentrega
			(
			  id_endeentrega integer unsigned NOT NULL auto_increment,
			  id_cliente integer unsigned NOT NULL,
			  logradouro varchar(10) NULL,
			  endereco varchar(200) NULL ,
			  numero varchar(50) NULL,
			  complemento varchar(50) NULL,
			  bairro varchar(200) NULL,
			  cidade varchar(200) NULL,
			  estado char(2) NULL,
			  cep char(8) NULL,
			  CONSTRAINT pk_endeentrega PRIMARY KEY (id_endeentrega)
			) engine=InnoDb;

		");
        Schema::table('endeentrega', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('endeentrega');
    }

}
