<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableEmailcliente extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE emailcliente
			(
			  id_emailcliente integer unsigned NOT NULL auto_increment,
			  id_tipoemail integer unsigned  null,
			  id_cliente integer unsigned NOT NULL,
			  correioeletronico varchar(50)  null,
			  CONSTRAINT pk_emailcliente PRIMARY KEY (id_emailcliente)
			) engine=InnoDb;

		");
        Schema::table('emailcliente', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emailcliente');
    }

}
