<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdUsuarioCriacaoToEnderecoTable extends Migration {
    public function up() {
        Schema::table('endereco', function(Blueprint $table) {
            $table->integer('id_usuario_criacao')->unsigned()->nullable();
            $table->string('evento', 1000)->nullable();
        });
    }

    public function down() {
        Schema::table('endereco', function(Blueprint $table) {
            $table->dropColumn('id_usuario_criacao');
            $table->dropColumn('evento');
        });
    }

}
