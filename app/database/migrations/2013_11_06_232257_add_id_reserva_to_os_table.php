<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdReservaToOsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('os', function(Blueprint $table) {
			$table->integer('id_reserva')->unsigned()->nullable();		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('os', function(Blueprint $table) {
			$table->dropColumn('id_reserva');			
		});
	}

}
