<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFlAtivoToClienteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cliente', function(Blueprint $table) {
			$table->boolean('fl_ativo')->nullable()->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if(Schema::hasColumn('cliente', 'fl_ativo')){
			Schema::table('cliente', function(Blueprint $table) {
				$table->dropColumn('fl_ativo');
			});
		}
	}

}
