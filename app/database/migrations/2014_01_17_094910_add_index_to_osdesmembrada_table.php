<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIndexToOsdesmembradaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->index(array('id_cliente', 'id_os'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->dropIndex('osdesmembrada_id_cliente_id_os_index');
		});
	}

}
