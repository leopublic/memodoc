<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableArquivoRetorno extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('arquivo_retorno', function($table) {
            $table->increments('id_arquivo_retorno');
            $table->integer('id_conta_cobranca')->unsigned();
            $table->string('nome_arquivo', 500)->nullable();
            $table->integer('id_status_importacao')->nullable();
            $table->integer('qtd_registros')->unsigned()->nullable();
            $table->integer('id_usuario')->unsigned()->nullable();
            $table->dateTime('data_importacao')->nullable();
            $table->string('mime_type', 500)->nullable();
            $table->integer('id_usuario_importacao')->unsigned()->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('arquivo_retorno');
	}

}