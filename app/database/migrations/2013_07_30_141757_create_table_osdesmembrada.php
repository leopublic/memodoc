<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableOsdesmembrada extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE osdesmembrada
			(
			  id_osdesmembrada integer unsigned not null auto_increment,
			  id_os integer unsigned not null ,
			  id_cliente integer unsigned not null ,
			  id_documento integer unsigned NOT NULL,
			  id_caixapadrao integer unsigned null,
			  CONSTRAINT pk_osdesmembrada PRIMARY KEY (id_osdesmembrada)
			) engine=InnoDb;
		");
        Schema::table('osdesmembrada', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('osdesmembrada');
    }

}
