<?php

use Illuminate\Database\Migrations\Migration;

class CreateTablePagador extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('pagador');
		Schema::create('pagador', function($table) {
            $table->increments('id_pagador');
			$table->integer('id_cliente');
			$table->string('cnpj')->nullable();
			$table->string('razaosocial', 500)->nullable();
			$table->string('logradouro', 500)->nullable();
			$table->string('numero', 500)->nullable();
			$table->string('complemento', 500)->nullable();
			$table->string('bairro', 500)->nullable();
			$table->string('cidade', 500)->nullable();
			$table->string('estado', 2)->nullable();
			$table->string('cep', 10)->nullable();
			$table->string('contato', 500)->nullable();
			$table->string('insc_estadual', 11)->nullable();
			$table->string('insc_municipal', 11)->nullable();
			$table->text('observacao')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pagador');
	}

}
