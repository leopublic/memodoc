<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFkenderecoToCheckoutTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checkout', function(Blueprint $table) {
            $table->foreign('id_caixa')->references('id_caixa')->on('endereco');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checkout', function(Blueprint $table) {
            $table->dropForeign('checkout_id_caixa_foreign');
        });
    }

}
