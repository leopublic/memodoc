<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableSolicitante extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE solicitante
			(
			  id_solicitante integer unsigned NOT NULL auto_increment,
			  id_cliente integer unsigned null,
			  id_departamento integer unsigned null,
			  id_setor integer unsigned null,
			  id_centrocusto integer unsigned null,
			  nome varchar(40) not null,
			  nomeconsulta varchar(40) null,
			  CONSTRAINT pk_solicitante PRIMARY KEY (id_solicitante)
			) engine=InnoDb;

		");
        Schema::table('solicitante', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('solicitante');
    }

}
