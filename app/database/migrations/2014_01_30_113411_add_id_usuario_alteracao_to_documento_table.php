<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdUsuarioAlteracaoToDocumentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('documento', function(Blueprint $table) {
			$table->integer('id_usuario_alteracao')->unsigned()->nullable();
			$table->string('evento', 1000)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('documento', function(Blueprint $table) {
			$table->dropColumn('id_usuario_alteracao');
            $table->dropColumn('evento');
		});
	}

}
