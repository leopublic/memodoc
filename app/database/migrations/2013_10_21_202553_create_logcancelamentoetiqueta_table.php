<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogcancelamentoetiquetaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logcancelamentoetiqueta', function(Blueprint $table) {
			$table->increments('id');
			$table->string('usuario', 30)->nullable();
			$table->dateTime('data_operacao')->nullable();
			$table->string('cliente',50)->nullable();
			$table->integer('id_cliente')->unsigned()->nullable();			
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('logcancelamentoetiqueta');
	}

}
