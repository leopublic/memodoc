<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddUltimoAcessoToUsuarioTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuario', function(Blueprint $table) {
            if (Schema::hasColumn('usuario', 'ultimoacesso')){
                $table->dropColumn('ultimoacesso');
            }
		});
		Schema::table('usuario', function(Blueprint $table) {
            $table->dateTime('ultimoacesso')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuario', function(Blueprint $table) {

		});
	}

}
