<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFksolicitanteToEmailsolicitanteTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emailsolicitante', function(Blueprint $table) {
            $table->foreign('id_solicitante')->references('id_solicitante')->on('solicitante');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emailsolicitante', function(Blueprint $table) {
            $table->dropForeign('emailsolicitante_id_solicitante_foreign');
        });
    }

}
