<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableTelefonecliente extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE telefonecliente
			(
			  id_telefonecliente integer unsigned NOT NULL auto_increment,
			  id_telefone integer unsigned  null,
			  id_cliente integer unsigned  NOT NULL,
			  numero varchar(20)  null,
			  ramal varchar(5)  null,
			  ddd varchar(3) null,
			  CONSTRAINT pk_telefonecliente PRIMARY KEY (id_telefonecliente)
			) engine=InnoDb;

		");
        Schema::table('telefonecliente', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('telefonecliente');
    }

}
