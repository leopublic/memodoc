<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableDepartamento extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$sql = "CREATE TABLE dep_cliente
				(
				  id_departamento integer unsigned NOT NULL auto_increment,
				  id_cliente integer unsigned NOT NULL,
				  nome varchar(100) ,
				  CONSTRAINT pk_dep_cliente PRIMARY KEY (id_departamento)
				) engine=InnoDb;";
		DB::unprepared($sql);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dep_cliente');
	}

}