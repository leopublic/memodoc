<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableCliente extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
//        Schema::create('cliente', function(Blueprint $table) {
//            $table->increments('id_cliente');
//			$table->string('razaosocial',200);
//			$table->string('nomefantasia', 200);
//			$table->string('logradouro', 5)->nullable();
//			$table->string('endereco', 200)->nullable();
//			$table->string('numero', 20)->nullable();
//			$table->string('complemento', 20)->nullable();
//			$table->string('bairro', 200)->nullable();
//			$table->string('cidade', 200)->nullable();
//			$table->string('estado', 2)->nullable();
//			$table->string('cep', 8)->nullable();
//			$table->string('cgc', 14)->nullable();
//			$table->string('ie', 15)->nullable();
//			$table->string('im', 15)->nullable();
//			$table->string('razaoconsulta', 200)->nullable();
//			$table->string('fantasiaconsulta', 200)->nullable();
//			$table->text('obs')->nullable();
//			$table->string('diretoriorecall', 450)->nullable();
//			$table->decimal('valorminimofaturamento', 9,2)->nullable();
//			$table->date('iniciocontrato')->nullable();
//			$table->integer('franquiacustodia')->default(0);
//			$table->integer('atendimento')->default(0);
//			$table->decimal('franquiamovimentacao', 11,2)->default(0);
//			$table->decimal('descontocustodiapercentual', 10,2)->default(0);
//			$table->integer('descontocustodialimite')->default(0);
//			$table->tinyInteger('issporfora')->nullable()->default(1);
//			$table->text('obsdesconto text')->nullable();
//			$table->tinyInteger('movimentacaocortesia')->default(0);
//			$table->string('responsavelcobranca', 50)->nullable();
//			$table->string('indicereajuste', 10)->nullable();
//			$table->string('logradourocor', 5)->nullable();
//			$table->string('enderecocor', 200)->nullable();
//			$table->string('numerocor', 5)->nullable();
//			$table->string('complementocor', 20)->nullable();
//			$table->string('bairrocor', 200)->nullable();
//			$table->string('cidadecor', 200)->nullable();
//			$table->string('estadocor', 2)->nullable();
//			$table->string('cepcor', 8)->nullable();
//			$table->tinyInteger('imprimenf')->default(1);
//            
//            $table->timestamps();
//		});
				$sql = "CREATE TABLE cliente(
				id_cliente integer unsigned auto_increment,
				razaosocial varchar(200) ,
				nomefantasia varchar(200) ,
				logradouro char(5) ,
				endereco varchar(200) ,
				numero varchar(20),
				complemento varchar(20),
				bairro varchar(200) ,
				cidade varchar(200) ,
				estado char(2) ,
				cep char(8) ,
				cgc char(14) ,
				ie char(15) ,
				im varchar(15) ,
				razaoconsulta varchar(200) ,
				fantasiaconsulta varchar(200) ,
				obs text ,
				diretoriorecall varchar(450),
				valorminimofaturamento decimal(9,2),
				iniciocontrato date NOT NULL,
				franquiacustodia integer DEFAULT 0,
				atendimento integer DEFAULT 0 NOT NULL,
				franquiamovimentacao decimal(11,2) DEFAULT 0,
				descontocustodiapercentual decimal(11,2) DEFAULT 0,
				descontocustodialimite integer DEFAULT 0,
				issporfora tinyint DEFAULT 1,
				obsdesconto text,
				movimentacaocortesia tinyint DEFAULT 0,
				responsavelcobranca varchar(50),
				indicereajuste varchar(10),
				logradourocor char(5) ,
				enderecocor varchar(200) ,
				numerocor varchar(5),
				complementocor varchar(20),
				bairrocor varchar(200) ,
				cidadecor varchar(200) ,
				estadocor char(2) ,
				cepcor char(8) ,
				imprimenf tinyint DEFAULT 1 NOT NULL,
				CONSTRAINT pk_cliente PRIMARY KEY (id_cliente)
			  ) engine=InnoDb
			  ";
		DB::unprepared($sql);

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("cliente");
	}

}