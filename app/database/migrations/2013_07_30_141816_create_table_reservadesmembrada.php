<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableReservadesmembrada extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE reservadesmembrada
			(
			  id_reservadesmembrada integer unsigned NOT NULL auto_increment,
			  id_reserva integer unsigned null,
			  id_caixapadrao integer unsigned null,
			  id_caixa integer unsigned null,
			  CONSTRAINT pk_reservadesmembrada PRIMARY KEY (id_reservadesmembrada)
			) engine=InnoDb;

		");
        Schema::table('reservadesmembrada', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservadesmembrada');
    }

}
