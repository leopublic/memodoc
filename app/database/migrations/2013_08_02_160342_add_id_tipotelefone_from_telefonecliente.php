<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdTipotelefoneFromTelefonecliente extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telefonecliente', function(Blueprint $table) {
            $table->integer('id_tipotelefone')->unsigned()->nullable()->after('id_cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telefonecliente', function(Blueprint $table) {
            $table->dropColumn('id_tipotelefone');            
        });
    }

}
