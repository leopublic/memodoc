<?php

use Illuminate\Database\Migrations\Migration;

class RecreateViewsDeRateio extends Migration {


 	public function up()
    {
        DB::statement( 'DROP VIEW if exists caixa_departamento' );
        DB::statement( 'DROP VIEW if exists caixa_setor' );
        DB::statement( 'DROP VIEW if exists caixa_centro' );
    	$sql = "create view caixa_departamento as 
				select documento.id_cliente, id_caixapadrao, reserva.data_reserva
					, group_concat(distinct nome) departamentos
					, group_concat(distinct dep_cliente.id_departamento) ids
					, (select max(dx.primeira_entrada) from documento dx where dx.id_caixa = documento.id_caixa) primeira_entrada
				from documento
				join dep_cliente ON dep_cliente.id_departamento = documento.id_departamento
				join reserva on reserva.id_reserva = documento.id_reserva
				group by documento.id_cliente, id_caixapadrao";
        DB::statement( $sql );
        $sql = "create view caixa_setor as
				select documento.id_cliente, id_caixapadrao, reserva.data_reserva
					, group_concat(distinct nome) setores
					, group_concat(distinct set_cliente.id_setor) ids
					, (select max(dx.primeira_entrada) from documento dx where dx.id_caixa = documento.id_caixa) primeira_entrada
				from documento
				join set_cliente ON set_cliente.id_setor = documento.id_setor
				join reserva on reserva.id_reserva = documento.id_reserva
				group by documento.id_cliente, id_caixapadrao
				";
        DB::statement( $sql );
        $sql = "create view caixa_centro as
				select documento.id_cliente, id_caixapadrao, reserva.data_reserva
					, group_concat(distinct nome) centros
					, group_concat(distinct centrodecusto.id_centrodecusto) ids
					, (select max(dx.primeira_entrada) from documento dx where dx.id_caixa = documento.id_caixa) primeira_entrada
				from documento
				join centrodecusto ON centrodecusto.id_centrodecusto= documento.id_centro
				join reserva on reserva.id_reserva = documento.id_reserva
				group by documento.id_cliente, id_caixapadrao
				";
        DB::statement( $sql );
    }

    public function down()
    {
        DB::statement( 'DROP VIEW if exists caixa_departamento' );
        DB::statement( 'DROP VIEW if exists caixa_setor' );
        DB::statement( 'DROP VIEW if exists caixa_centro' );
    }

}