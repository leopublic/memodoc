<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdStatusFaturamentoToFaturamentoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('faturamento', function(Blueprint $table) {
			$table->integer('id_status_faturamento')->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('faturamento', function(Blueprint $table) {
			if(Schema::hasColumn('faturamento', 'id_status_faturamento')){
                $table->dropColumn('id_status_faturamento');
            }
		});
	}

}
