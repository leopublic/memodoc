<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFlPrimeiraEntradaToCheckinTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('checkin', function(Blueprint $table) {
			$table->boolean('fl_primeira_entrada')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('checkin', function(Blueprint $table) {
            $table->dropColumns('fl_primeira_entrada');
		});
	}

}
