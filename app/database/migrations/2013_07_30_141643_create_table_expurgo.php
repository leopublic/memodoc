<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableExpurgo extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
				CREATE TABLE expurgo
				(
				  id_expurgo integer unsigned NOT NULL auto_increment,
				  id_cliente integer unsigned NOT NULL,
				  id_caixapadrao integer unsigned NULL,
				  id_caixa integer unsigned not NULL,
				  data_solicitacao date NOT NULL,
				  data_checkout datetime NOT NULL,
				  valor_expurgo decimal(12,2) DEFAULT 0,
				  cortesia tinyint DEFAULT 0,
				  CONSTRAINT pk_expurgo PRIMARY KEY (id_expurgo)
				) engine=InnoDb;

		");
        Schema::table('expurgo', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('expurgo');
    }

}
