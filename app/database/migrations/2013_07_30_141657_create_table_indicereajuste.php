<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTableIndicereajuste extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
			CREATE TABLE indicereajuste
			(
			  id_indicereajuste int unsigned not null auto_increment,
			  indice varchar(50) NOT NULL,
			  mes integer NOT NULL,
			  ano integer NOT NULL,
			  percentual decimal(12,2),
			  CONSTRAINT pk_indicereajuste PRIMARY KEY (id_indicereajuste)
			) engine=InnoDb
			;

		");
        Schema::table('indicereajuste', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('indicereajuste');
    }

}
