<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableCentrodecusto extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::unprepared("CREATE TABLE centrodecusto
					(
					  id_centrodecusto integer unsigned NOT NULL auto_increment,
					  id_cliente integer unsigned NOT NULL,
					  nome varchar(50) ,
					  nomeconsulta varchar(50) ,
					  CONSTRAINT pk_centrodecusto PRIMARY KEY (id_centrodecusto)
					) engine=InnoDb;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('centrodecusto');
	}

}