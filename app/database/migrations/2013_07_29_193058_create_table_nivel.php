<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableNivel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::unprepared("CREATE TABLE nivel
					(
					  id_nivel integer unsigned NOT NULL auto_increment,
					  descricao varchar(30) NOT NULL,
					  previlegio integer NOT NULL,
					  exclusivo_adm tinyint NOT NULL default 0,
					  CONSTRAINT pk_previlegio PRIMARY KEY (id_nivel)
					) engine=InnoDb;");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nivel');
	}

}