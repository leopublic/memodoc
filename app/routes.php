<?php

use Darsain\Console\Console;

// Rotas publicas
Route::get('login.aspx', function () {
    return Redirect::to('auth/login');
});
Route::controller('auth', 'AuthController');

// Rotas autorizadas somente a usuários logados
Route::group(array(
    'before' => 'autenticado'
        ), function () {

    Route::controller('home', 'HomeController');
    Route::get('/', function () {
        if (Auth::user()->id_nivel < 10) {
            if (Auth::user()->fl_primeira_vez) {
                return Redirect::to('publico/usuario/meusdados');
            } else {
                return Redirect::to('home/bemvindocliente');
            }
        } else {
            return Redirect::to('home/bemvindo');
        }
    });

    Route::group(array(
        'prefix' => 'publico'
            ), function () {
        Route::controller('anexo', 'AnexoClienteController');
        Route::controller('documento', 'DocumentoClienteController');
        Route::controller('usuario', 'UsuarioClienteController');
        Route::controller('os', 'OrdemServicoClienteController');
        Route::controller('caixa', 'CaixaClienteController');
        Route::controller('centrodecusto', 'CentrodecustoClienteController');
        Route::controller('departamento', 'DepartamentoClienteController');
        Route::controller('setor', 'SetorClienteController');
        Route::controller('reserva', 'ReservaClienteController');
        Route::controller('propriedade', 'PropriedadeClienteController');
        Route::controller('remessa', 'RemessaClienteController');
    });

    // Rotas autorizadas somente aos usuários nivel > 10 (internos)
    Route::group(array(
        'before' => 'backoffice'
            ), function () {

        View::composer('padrao/combos/clientes', 'PreencheComboComposer');
        View::composer('stilearn-metro', 'ProtocoloOsComposer');

        Route::controller('aviso', 'AvisoController');
        Route::controller('anexo', 'AnexoController');
        Route::controller('caixa', 'CaixaController');
        Route::controller('checkin', 'CheckinController');
        Route::controller('checkinok', 'CheckinController');
        Route::controller('checkinxx', 'CheckinController');
        Route::controller('checkout', 'CheckoutController');
        Route::controller('cliente', 'ClienteController');
        Route::controller('consultaboletos', 'ConsultaBoletosController');
        Route::controller('configetiqueta', 'ConfigEtiquetaController');
        Route::controller('documento', 'DocumentoController');
        Route::controller('diavencimento', 'DiaVencimentoController');
        Route::controller('digitacao', 'DigitacaoController');
        Route::controller('envelopecobr', 'EnvelopeCobrController');
        Route::controller('etiqueta', 'EtiquetaController');
        Route::controller('endereco', 'EnderecoController');
        Route::controller('expurgo', 'ExpurgoController');
        Route::controller('faturamento', 'FaturamentoController');
        Route::controller('galpao', 'GalpaoController');
        Route::controller('gerencial', 'GerencialController');
        Route::controller('importador', 'ImportadorController');
        Route::controller('modelo', 'ModeloController');
        Route::controller('nivel', 'NivelController');
        Route::controller('notacarioca', 'NotacariocaController');
        Route::controller('ordemservico', 'OrdemServicoController');
        Route::controller('osok', 'OrdemServicoController');
        Route::controller('periodofat', 'PeriodoFaturamentoController');
        Route::controller('sicoobremessa', 'SicoobRemessaController');
        Route::controller('sicoobretorno', 'SicoobRetornoController');
        Route::controller('loterps', 'LoteRpsController');
        Route::controller('tipodocumento', 'TipoDocumentoController');
        Route::controller('tipoemail', 'TipoEmailController');
        Route::controller('tipoproduto', 'TipoProdutoController');
        Route::controller('tipotelefone', 'TipoTelefoneController');
        Route::controller('reserva', 'ReservaController');
        Route::controller('transbordo', 'TransbordoController');
        Route::controller('retorno', 'RetornoController');
        Route::controller('remessa', 'RemessaController');
        Route::controller('usuario', 'UsuarioController');
    });
});

// Binds para interfaces definidas no sistema
App::bind('Memodoc\Repositorios\RepositorioOsInterface', 'Memodoc\Repositorios\RepositorioOsEloquent');

// Validadores
if (\Auth::user()) {
    if (\Auth::user()->id_nivel < 10) {
        $propriedade = \Propriedade::where('id_cliente', '=', \Auth::user()->id_cliente)->first();
        View::share('propriedade', $propriedade);
    }
}
