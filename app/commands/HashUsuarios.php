<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class HashUsuarios extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'HashUsuarios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Criptografa a senha dos usuários';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        $usuarios = Usuario::all();
        foreach ($usuarios as $usuario) {
            if (strlen($usuario->password) < 50) {
                $usuario->password = Hash::make($usuario->password);
                $usuario->save();
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
//            array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
//            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
