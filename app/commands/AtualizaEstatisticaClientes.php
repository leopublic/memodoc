<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AtualizaEstatisticaClientes extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'memodoc:atualiza-estatisticas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza estatísticas dos clientes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        $rep = new Memodoc\Repositorios\RepositorioClienteEloquent;
        $rep->atualizeEstatisticas();
        $this->info('Estatísticas atualizadas com sucesso!');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
        );
    }

}
