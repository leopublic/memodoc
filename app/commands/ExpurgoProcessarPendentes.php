<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ExpurgoProcessarPendentes extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'memodoc:expurgos-procesar-pendentes';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Processa os expurgos pendentes';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
        set_time_limit(0);
        $rep = new Memodoc\Repositorios\RepositorioExpurgoEloquent;
        $pendentes = Expurgo::where('fl_processado', '=', 0)
                    ->chunk(50, function($expurgos) use ($rep){
            foreach($expurgos as $expurgo){
                $rep->processe_expurgo($expurgo);
            }
        });
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		);
	}

}