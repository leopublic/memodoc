<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AdminTestaSenha extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'admin:testa-senha';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Testa a senha de um usuário';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
        $username = $this->argument('username');
        $password = $this->argument('password');
        if (\Auth::attempt(array('username' => $username, 'password' => $password))) {
        	$this->info('Senha confirmada para o usuario '.\Auth::user()->nomeusuario.'!');
        	$this->info('id_usuario='.\Auth::user()->id_usuario);
        	$this->info('id_cliente='.\Auth::user()->id_cliente);
        	$this->info('id_cliente_principal='.\Auth::user()->id_cliente_principal);
        	$this->info('id_nivel='.\Auth::user()->id_nivel);
        	$this->info('adm='.\Auth::user()->adm);
        } else {
        	$this->error('Senha não confere!');
        }
	}
	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('username', InputArgument::REQUIRED, 'Login do usuario'),
			array('password', InputArgument::REQUIRED, 'Senha a ser testada'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(

		);
	}

}