<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PedidosPendentes extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'Pedidospendentes:atualizar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        return "Comando suspenso";
        exit();
        $sql = "select count(*) from os where id_status_os = 2";
        try {
            $pdo = DB::getPdo();
            $res = $pdo->query($sql);
            $rs = $res->fetch(PDO::FETCH_NUM);
            $qtd = $rs[0];
            $redis = \Redis::connection();
            $chave = \Memodoc\Repositorios\RepositorioOsEloquent::REDIS_QTDOSABERTAS;
            $redis->set($chave, $qtd);
            // \Cache::put('osabertas', $qtd, 1);
        } catch (Exception $ex) {
            \log::info('Erro na atualizacao da quantidade '.$ex->getMessage());
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
//            array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
                //          array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
