<?php

return array(


    'pdf' => array(
        'enabled' => true,
        'binary' => '/home/vagrant/wkhtmltox/bin/wkhtmltopdf',
        'timeout' => false,
        'options' => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary' => '/home/vagrant/wkhtmltox/bin/wkhtmltoimage',
        'timeout' => false,
        'options' => array(),
    ),


);
