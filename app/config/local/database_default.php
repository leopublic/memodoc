<?php
/*
|--------------------------------------------------------------------------
| Configuraçao local do database
|--------------------------------------------------------------------------
|
| Altere a configuração local da sua conexao com o banco de dados, e salve 
| esse arquivo como database.php no diretório app/config/local.
|
| Os parametros informados aqui vão se sobrepor ao valores definidos em app/config/database.php
*/
return array(

	'fetch' => PDO::FETCH_CLASS,
	'default' => 'mysql',
	'connections' => array(

		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'memodoc',
			'username'  => 'root',
			'password'  => 'mysql',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

	),


);
