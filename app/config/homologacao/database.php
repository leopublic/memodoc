<?php

return array(

	'fetch' => PDO::FETCH_CLASS,


	'default' => 'mysql',

	'connections' => array(

		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => '127.0.0.1',
			'database'  => 'memodoc',
			'username'  => 'root',
			'password'  => 'gg0rezqn',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => ''
		),

		'mysql_fat' => array(
			'driver'    => 'mysql',
			'host'      => '127.0.0.1',
			'database'  => 'memodoc',
			'username'  => 'root',
			'password'  => 'gg0rezqn',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		)

	),

	'redis' => array(

		'cluster' => true,

		'default' => array(
			'host'     => '127.0.0.1',
			'port'     => 6379,
			'database' => 0,
		),

	),

	'migrations' => 'migrations',



);
