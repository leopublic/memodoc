<?php
/**
 * @property ddd string
 * @property ramal string
 * @property numero string
 * @property id_tipotelefone int
 */

class Telefone extends Modelo {
	public function tipotelefone(){
		return $this->belongsTo('TipoTelefone', 'id_tipotelefone');
	}
}