<?php

/**
 * @property int $id_status_os
 * @property int $descricao
 * @property int $descricao_cliente
 */
class StatusOs extends Modelo {

    protected $table = 'status_os';
    protected $primaryKey = 'id_status_os';

    const stINCOMPLETA = 1;
    const stNOVA = 2;
    const stPENDENTE = 3;
    const stATENDIDA = 4;
    const stCANCELADA = 5;

    public static function NovaOsAdmin() {
        return self::stNOVA;
    }

    public static function NovaOsCliente() {
        return self::stNOVA;
    }

    public static function OsNaoFinalizada() {
        return self::stINCOMPLETA;
    }

    public static function OsCancelada() {
        return self::stCANCELADA;
    }

    public static function OsAtendida() {
        return self::stATENDIDA;
    }

    public function scopeDisponiveis($query){
        return $query->where('id_status_os', '<>', 1)->orderBy('id_status_os');
    }
    
}
