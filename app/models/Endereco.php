<?php

/**
 * @property int $id_caixa
 * @property int $id_galpao
 * @property int $id_rua
 * @property int $id_predio
 * @property int $id_andar
 * @property int $id_unidade
 * @property int $id_cliente
 * @property int $id_tipodocumento
 * @property int $disponivel
 * @property int $reservado
 * @property int $reserva
 * @property int $endcancelado
 */
class Endereco extends Modelo {

    protected $table = 'endereco';
    protected $primaryKey = 'id_caixa';
    protected $fillable = array('id_galpao', 'id_rua', 'id_predio', 'id_andar', 'id_unidade', 'disponivel', 'id_tipodocumento');

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function scopeNaousados($query){
        return $query->where('endcancelado', '=', 0)
                     ->where('disponivel', '=', '1');

    }

    public function scopeBloqueados($query){
        return $query->where('endcancelado', '=', 1)
                     ->whereRaw("coalesce(disponivel, 0) = 1 or id_galpao in (select id_galpao from galpao where coalesce(fl_disponivel_enderecamento, 0) = 0) ");

    }

    public function scopeAlugados($query){
        return $query->where('endcancelado', '=', 0)
                     ->whereRaw(" coalesce(id_cliente, 0) <> 0")
                     ->where('disponivel', '=', '0');

    }
    /**
     * Adiciona os filtros que selecionam os enderecos disponiveis
     * @param type $result
     */
    public function scopeDisponiveis($query) {
        return $query->naousados()
                     ->whereRaw("id_galpao in (select id_galpao from galpao where fl_disponivel_enderecamento = 1)");
    }

    /**
     * Retorna o total de vagas disponíveis não utilizadas
     * @param type $result
     */
    public static function naousadosPorGalpaoTipo($pid_galpao, $pid_tipodocumento) {
        return Endereco::where('id_galpao', '=', $pid_galpao)
                        ->where('id_tipodocumento', '=', $pid_tipodocumento)
                        ->naousados()
                        ->count();
    }

    /**
     * Retorna o total de vagas disponíveis não utilizadas
     * @param type $result
     */
    public static function bloqueadosPorGalpaoTipo($pid_galpao, $pid_tipodocumento) {
        return Endereco::where('id_galpao', '=', $pid_galpao)
                        ->where('id_tipodocumento', '=', $pid_tipodocumento)
                        ->bloqueados()
                        ->count();
    }

    /**
     * Retorna o total de vagas disponíveis não utilizadas
     * @param type $result
     */
    public static function alugadosPorGalpaoTipo($pid_galpao, $pid_tipodocumento) {
        return Endereco::where('id_galpao', '=', $pid_galpao)
                        ->where('id_tipodocumento', '=', $pid_tipodocumento)
                        ->alugados()
                        ->count();
    }

    /**
     * Retorna o total de vagas disponíveis não utilizadas
     * @param type $result
     */
    public static function totalPorGalpaoTipo($pid_galpao, $pid_tipodocumento) {
        return Endereco::where('id_galpao', '=', $pid_galpao)
                        ->where('id_tipodocumento', '=', $pid_tipodocumento)
                        ->whereRaw("coalesce(endcancelado, 0) = 0")
                        ->count();
    }

    /**
     * Adiciona os filtros que selecionam os enderecos disponiveis
     * @param type $result
     */
    public static function galpoes() {
        return DB::table('endereco')->distinct()->get(array('id_galpao'));
    }

    /**
     * Adiciona os filtros que selecionam os enderecos disponiveis
     * @param type $result
     */

    /**
     * Retorna a quantidade de espacos disponíveis
     * @return int Quantidade de espacos disponíveis
     */
    public static function get_qtdDisponivel($id_tipodocumento) {
        $query = Endereco::disponiveis()
                ->where('id_tipodocumento', '=', $id_tipodocumento)
        ;
        return $query->count();

    }

    /**
     * Retorna a quantidade de caixas reservadas mas nunca ocupadas
     * @return int Quantidade
     */
    public static function get_qtdNuncaUtilizado() {
        return DB::table('endereco')
                    ->distinct()
                    ->where('endcancelado', '=', 0)
                    ->where('disponivel', '=', 0)
                    ->whereRaw('id_caixa in (select id_caixa from documento where primeira_entrada is null)')
                    ->whereRaw('id_caixa not in (select id_caixa from documento where primeira_entrada is not null)')
                    ->count();
    }

    /**
     * Retorna a quantidade de caixas reservadas mas nunca ocupadas
     * @return int Quantidade
     */
    public static function nuncaUtilizadoPorCliente() {
        return DB::table('endereco')
                    ->where('endcancelado', '=', 0)
                    ->where('disponivel', '=', 0)
                    ->whereRaw('id_caixa in (select id_caixa from documento where primeira_entrada is null) ')
                    ->whereRaw('id_caixa not in (select id_caixa from documento where primeira_entrada is not null) ')
                    ->groupBy('id_cliente')
                    ->orderBy(DB::raw('count(*) '), 'desc')
                    ->get(array('id_cliente', DB::raw('count(*) as qtd')));
    }

    /**
     * Obtem o primeiro endereço disponivel
     */
    public static function get_cursorDisponiveis($pid_tipodocumento) {
        $query = \Endereco::disponiveis();
        if ($pid_tipodocumento != '') {
            $query->where('id_tipodocumento', '=', $pid_tipodocumento);
        } else {
            throw new ExceptionParametroObrigatorio("tipo de documento a ser reservado");
        }
        $end = $query->orderBy('id_andar', 'asc')
                ->orderBy('id_galpao', 'asc')
                ->orderBy('id_rua', 'asc')
                ->orderBy('id_predio', 'asc')
        ;
        return $end;
    }

    /**
     * Associa endereços livres a uma determinada reserva
     * @param Reserva $preserva
     */
    public function ReservarPara(Reserva $preserva) {
        $this->id_cliente = $preserva->id_cliente;
        $this->IndicaQueEstaOcupado();
    }

    public function IndicaQueEstaOcupado() {
        $this->disponivel = 0;
        $this->reservado = 1;
        $this->reserva = 1;
    }

    /**
     *
     *  - Número do galpão
     *  - Número da rua
     *  - Quantidade de prédios por rua
     *  - Quantidade de andares dos prédios do lado par
     *  - Quantidade de andares dos prédios do lado ímpar
     *  - Quantidade de apartamentos por andar
     *
     * @param type $id_galpao
     * @param type $id_rua
     * @param type $qtd_predio
     * @param type $qtd_andares_par
     * @param type $qtd_andares_impar
     * @param type $qtd_apartamentos_andar
     * @param type $id_tipodocumento
     * @return array $tags
     */
    public static function Novo($id_galpao, $id_rua, $qtd_predio, $qtd_andares_par, $qtd_andares_impar, $qtd_apartamentos_andar, $id_tipodocumento) {

        // Percorrendo os prédios
        $predios = array();
        for ($i = 1; $i <= $qtd_predio; $i++) {

            // Qtde se o prédio for Impar ou Par
            $qtd_andar = !($i % 2) ? $qtd_andares_par : $qtd_andares_impar;

            // Percorrendo os andares dentro de cada prédio
            $andares = array();
            for ($n = 1; $n <= $qtd_andar; $n++) {

                // Percorrendo os apartamentos dentro de cada andar
                $apartamentos = array();
                for ($z = 1; $z <= $qtd_apartamentos_andar; $z++) {
                    $apartamentos[$z] = $z;
                }

                $andares[$n] = $apartamentos;
            }

            $predios[$i] = $andares;
        }

        DB::connection()->getPdo()->beginTransaction();
        $tags = array();
        foreach ($predios as $id_predio => $andares) {
            foreach ($andares as $id_andar => $apartamentos) {
                foreach ($apartamentos as $id_unidade) {

                    $tags[] = $id_galpao . '.' . $id_rua . '.' . $id_predio . '.' . $id_andar . '.' . $id_unidade;
                    Endereco::create(array(
                        'id_galpao' => $id_galpao,
                        'id_rua' => $id_rua,
                        'id_predio' => $id_predio,
                        'id_andar' => $id_andar,
                        'id_unidade' => $id_unidade,
                        'id_tipodocumento' => $id_tipodocumento,
                        'disponivel' => '1'
                    ));
                    //$endereco->save();
                }
            }
        }
        DB::connection()->getPdo()->commit();

        return $tags;
    }

    /**
     * alteracaoQtdPorAndar Faz a alteração (exclui e insere) a nova quantidade de apartamentos dentro de um Andar.
     * @param type $id_galpao
     * @param type $id_predio
     * @param type $id_rua
     * @param type $id_andar
     * @param type $id_tipodocumento
     * @param type $qtd_unidades
     * @return array
     */
    static function alteracaoQtdPorAndar($id_galpao, $id_predio, $id_rua, $id_andar, $id_tipodocumento, $qtd_unidades) {

        // Percorrendo os apartamentos dentro de cada andar
        $apartamentos = array();
        for ($z = 1; $z <= $qtd_unidades; $z++) {
            $apartamentos[$z] = $z;
        }
        DB::connection()->getPdo()->beginTransaction();
        Endereco::where('id_galpao', '=', $id_galpao)
                ->where('id_predio', '=', $id_predio)
                ->where('id_rua', '=', $id_rua)
                ->where('id_andar', '=', $id_andar)
                ->delete();
        foreach ($apartamentos as $id_unidade) {
            Endereco::create(array(
                'id_galpao' => $id_galpao,
                'id_rua' => $id_rua,
                'id_predio' => $id_predio,
                'id_andar' => $id_andar,
                'id_unidade' => $id_unidade,
                'id_tipodocumento' => $id_tipodocumento,
                'disponivel' => '1'
            ));
        }
        DB::connection()->getPdo()->commit();
        return $apartamentos;
    }

    public function enderecoCompleto() {
        return $this->id_galpao
                . "." . $this->filler($this->id_rua, 2)
                . "." . $this->filler($this->id_predio, 2)
                . "." . $this->filler($this->id_andar, 2)
                . "." . $this->filler($this->id_unidade, 4);
    }

    public function getIdCaixapadraoAttribute($valor) {
        return $this->recupera_id_caixapadrao();
    }
    public function getIdCaixapadraoFmtAttribute($valor){
        $x = $this->recupera_id_caixapadrao();
        if (is_numeric($x)){
            return substr('000000'.$x, -6);
        } else {
            return $x;
        }
    }

    public function recupera_id_caixapadrao(){
        $docs = \Documento::where('id_caixa', '=', $this->attributes['id_caixa'])->select('id_caixapadrao')->distinct()->get();
        if (count($docs) ==0) {
            $movimentacao = \MovimentacaoCaixa::where('id_caixa', '=', $this->attributes['id_caixa'])->count();
            if ($movimentacao > 0){
                return '(movimentada)';
            } else {
                $trans = \TransbordoCaixa::where('id_caixa_antes', '=', $this->attributes['id_caixa'])->count();
                if ($trans > 0){
                    return "(transbordada)";
                } else {
                    $expurgos = \Expurgo::where('id_caixa', '=', $this->attributes['id_caixa'])->count();
                    if ($expurgos > 0){
                        return "(expurgada)";
                    } else {
                        return '--';
                    }
                }
            }
        } elseif (count($docs) > 1) {
            return '???';
        } else {
            $doc = $docs->first();
            return $doc->id_caixapadrao;
        }

    }

    public function getIdReservaAttribute($valor) {
        $docs = \Documento::where('id_caixa', '=', $this->attributes['id_caixa'])->select('id_reserva')->distinct()->get();
        if (count($docs) > 1) {
            return '???';
        } else {
            if (count($docs) == 0){
                return '--';
            } else {
                $doc = $docs->first();
                return $doc->id_reserva;
            }
        }
    }

    public function filler($valor, $qtdZeros) {
        return substr(str_repeat('0', $qtdZeros) . $valor, $qtdZeros * -1);
    }

    /**
     * Gera um novo id para um endereço
     */
    public function renovarId($observacao) {
        $end = new Endereco;
        $end->id_galpao = $this->id_galpao;
        $end->id_predio = $this->id_predio;
        $end->id_rua = $this->id_rua;
        $end->id_unidade = $this->id_unidade;
        $end->id_cliente = $this->id_cliente;
        $end->id_tipodocumento = $this->id_tipodocumento;
        $end->disponivel = $this->disponivel;
        $end->endcancelado = $this->endcancelado;
        $end->reserva = $this->reserva;
        $end->reservado = $this->reservado;

        $end->save();

        $this->endcancelado = true;
        $this->observacao .= $observacao;
        $this->save();

        return $end;
    }

}
