<?php

class Usuariocentrocusto extends Modelo {
	protected $guarded = array();
	protected $table = 'usuariocentrocusto';
        protected $primaryKey = 'id_usuariocentrocusto';
        protected $fillable = array('id_cliente','id_centrocusto','id_usuario');

	public static $rules = array();
}
