<?php
use \Carbon;

class Boleto extends Modelo {

    protected $table = 'boleto';
    protected $primaryKey = 'id_boleto';
    protected $fillable = array();

    public function cliente(){
            return $this->belongsTo('Cliente', 'id_cliente', 'id_cliente');
    }

    public function getLinhaDigitavelAttribute(){
        return '&nbsp;';
    }

    public function getDtVencimentoAttribute(){
        return '&nbsp;';
    }
    public function getBeneficiarioNomeAttribute(){
        return '&nbsp;';
    }
    public function getBeneficiarioCodigoAttribute(){
        return '&nbsp;';
    }
    public function getDtDocumentoAttribute(){
        return '&nbsp;';
    }
    public function getNumDocumentoAttribute(){
        return '&nbsp;';
    }
    public function getEspecieAttribute(){
        return '&nbsp;';
    }
    public function getAceiteAttribute(){
        return '&nbsp;';
    }
    public function getDtProcessamentoAttribute(){
        return '&nbsp;';
    }
    public function getNossoNumeroAttribute(){
        return '&nbsp;';
    }
    public function getQuantidadeAttribute(){
        return '&nbsp;';
    }    
    public function getValorAttribute(){
        return '&nbsp;';
    }    
    public function getValorDocumentoAttribute(){
        return '&nbsp;';
    }    
    public function getCarteiraAttribute(){
        return '&nbsp;';
    }    
    public function getInstrucoesAttribute(){
        return '&nbsp;';
    }    
    public function getDescontoAttribute(){
        return '&nbsp;';
    }    
    public function getOutrasDeducoesAttribute(){
        return '&nbsp;';
    }    
    public function getMoraMultaAttribute(){
        return '&nbsp;';
    }    
    public function getPagadorAttribute(){
        return '&nbsp;';
    }    
    public function getOutrosAcrescimosAttribute(){
        return '&nbsp;';
    }    
    public function getValorCobradoAttribute(){
        return '&nbsp;';
    }    
}