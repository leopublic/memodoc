<?php
class TipoDocumento extends Modelo {
	protected $table = 'tipodocumento';
	protected $primaryKey = 'id_tipodocumento';
        protected $fillable = array('descricao');
        
        /**
         * verifica se permite deletar
         */
        static function getPermiteDeletar($id){
            
           $count = self::find($id)->first()->hasMany('Documento','id_tipodocumento')->count();
           return ($count > 0) ? false : true;
        }
}