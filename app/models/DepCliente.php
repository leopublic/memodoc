<?php

/**
 * @property int $id_departamento Chave
 * @property int $id_cliente Cliente
 * @property string $nome Nome do centro de custo
 */
class DepCliente extends Modelo {

    protected $table = 'dep_cliente';
    protected $primaryKey = 'id_departamento';

    public static function boot() {
        parent::boot();
        static::deleting(function($depcliente) {
            $user = Auth::user();
            \Documento::where('id_departamento', '=', $depcliente->id_departamento)->update(array('id_departamento' => null));
            \Os::where('id_departamento', '=', $depcliente->id_departamento)->update(array('id_departamento' => null, 'id_usuario_alteracao' => $user->id_usuario));
        });
    }

    public function campoOrdemPadrao() {
        return 'nome';
    }

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function usuarios() {
        return $this->belongsToMany('Usuario', 'usuariodepartamento', 'id_usuario', 'id_departamento');
    }

    public function getQtdDocumentosAttribute($valor) {
        $sql = "select count(*) qtd from documento where id_departamento =" . $this->attributes['id_departamento'];
        $res = \DB::select(\DB::raw($sql));

        return $res[0]->qtd;
    }

    public function scopeDropdown($query) {
        return $query->orderBy('nome')->lists('nome', 'id_departamento');
    }

    public function qtdDocumentos() {
        return Documento::where('id_departamento', '=', $this->id_departamento)->count();
    }

}
