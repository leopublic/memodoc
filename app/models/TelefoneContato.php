<?php
class TelefoneContato extends Telefone {
	protected $table = 'telefonecontato';
	protected $primaryKey = 'id_telefonecontato';

	public function cliente(){
		return $this->belongsTo('Cliente', 'id_cliente');
	}

	public function contato(){
		return $this->belongsTo('Contato', 'id_contato');
	}
/*
	public function CarregaDoRecordsetFirebird($pRs)
	{
		parent::CarregaDoRecordsetFirebird($pRs);
		if($this->id_tipotelefone == 0){
			$this->id_tipotelefone = null;
		}
	} */
}