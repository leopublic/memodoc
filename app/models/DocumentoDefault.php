<?php

/**
 * É o documento que é criado para reservar o endereço
 */
class DocumentoDefault extends Documento {

    public function set_tituloDefault() {
        $this->titulo = 'CAIXA NOVA ENVIADA PARA CLIENTE';
    }

    public static function Novo(Endereco $pendereco, $preserva) {
        $doc = new DocumentoDefault();
        $doc->set_tituloDefault();
        $doc->id_cliente = $pendereco->id_cliente;
        $doc->id_tipodocumento = $pendereco->id_tipodocumento;
        $doc->id_caixa = $pendereco->id_caixa;
        $doc->itemalterado = 0;
        $doc->emcasa = 0;
        $doc->id_box = 0;
        $doc->id_reserva = $preserva->id_reserva;
        $doc->id_item = 1;
        $doc->fl_revisado = 0;
        return $doc;
    }

}
