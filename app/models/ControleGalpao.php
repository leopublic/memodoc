<?php
class ControleGalpao{
	public static function ReservarEnderecos(Reserva $reserva)
	{
		if($reserva->caixas < Endereco::get_qtdDisponivel($reserva->id_tipodocumento)){
			DB::connection()->getPdo()->beginTransaction();
			// Abre um cursor dos endereços disponiveis
			$enderecos = Endereco::get_cursorDisponiveis($reserva->id_tipodocumento)
							->take($reserva->caixas)
							->get();
			foreach($enderecos as $endereco){
				// Atualiza status do endereco
				$endereco->ReservarPara($reserva);
				$endereco->save();
				// Cria o documento default
				$doc = DocumentoDefault::Novo($endereco, $reserva);
				$doc->AssumaProximoNumeroDisponivel();
				$doc->save();
				//
				$reservadesmembrada = new ReservaDesmembrada();
				$reservadesmembrada->id_reserva = $reserva->id_reserva;
				$reservadesmembrada->id_caixa = $doc->id_caixa;
				$reservadesmembrada->id_caixapadrao = $doc->id_caixapadrao;
				$reservadesmembrada->save();
			}
			DB::connection()->getPdo()->commit();
		}
		else{
			throw new ExceptionSemEnderecoDisponivel("Não existem endereços disponíveis para reserva");
		}
	}
}