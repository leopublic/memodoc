<?php
class TelefoneCliente extends Telefone {
	protected $table = 'telefonecliente';
	protected $primaryKey = 'id_telefonecliente';
	protected $fillable = array (
			'*' 
	);
	public function cliente() {
		return $this->belongsTo ( 'Cliente', 'id_cliente' );
	}
	function tipotelefone() {
		return $this->belongsTo ( 'TipoTelefone', 'id_tipotelefone' );
	}
	public function getDescricaoTipoAttribute($valor) {
		if (is_object ( $this->tipotelefone )) {
			return $this->tipotelefone->descricao;
		} else {
			return '--';
		}
	}
	public function CarregaDoRecordsetFirebird($pRs) {
		parent::CarregaDoRecordsetFirebird ( $pRs );
		if ($this->id_tipotelefone == 0) {
			$this->id_tipotelefone = null;
		}
	}
	public function campoOrdemPadrao() {
		return 'nome';
	}
}