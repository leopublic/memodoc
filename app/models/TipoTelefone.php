<?php
class TipoTelefone extends Modelo {
	protected $table = 'tipotelefone';
	protected $primaryKey = 'id_tipotelefone';
	protected $fillable = array (
			'descricao' 
	);
	function telefonecliente() {
		$this->hasMany ( 'telefonecliente', 'id_telefonecliente' );
	}
	/**
	 * verifica se permite deletar
	 */
	static function getPermiteDeletar($id) {
		$count = self::find ( $id )->first ()->hasMany ( 'TelefoneCliente', 'id_tipotelefone' )->count ();
		$count += self::find ( $id )->first ()->hasMany ( 'TelefoneContato', 'id_tipotelefone' )->count ();
		$count += self::find ( $id )->first ()->hasMany ( 'TelefoneSolicitante', 'id_tipotelefone' )->count ();
		return ($count > 0) ? false : true;
	}
	public function campoOrdemPadrao() {
		return 'descricao';
	}
	public static function ordemPadrao() {
		return 'descricao';
	}
}