<?php
use \Carbon;

class NotaFiscal extends Modelo {

        protected $table = 'nota_fiscal';
        protected $primaryKey = 'id_nota_fiscal';
        protected $fillable = array();

        public function cliente(){
                return $this->belongsTo('Cliente', 'id_cliente', 'id_cliente');
        }

        public function faturamento(){
                return $this->belongsTo('Faturamento', 'id_faturamento', 'id_faturamento');
        }

        public function getClienteEmissaoAttribute(){
                if ($this->id_cliente > 0){
                        return $this->cliente;
                } else {
                        return $this->faturamento->cliente;
                }
        }

        public function getValorTotalEmissaoAttribute(){
                if ($this->id_faturamento > 0){
                        return $this->faturamento->val_totalFatura();
                } else {
                        return $this->nfis_vl_total;
                }
        }

        public function getValorIssEmissaoAttribute(){
                if ($this->id_faturamento > 0){
                        return $this->faturamento->val_iss_fatura();
                } else {
                        return $this->nfis_vl_iss;
                }
        }

        public function getDescricaoServicoEmissaoAttribute(){
                if ($this->id_faturamento > 0){
                        return $this->faturamento->descricao_servicos;
                } else {
                        return $this->nfis_descricao_servicos;
                }
        }

        public function getDtCompetenciaEmissaoAttribute(){
                if ($this->id_faturamento > 0){
                        return $this->faturamento->dt_faturamento;
                } else {
                        return $this->nfis_dt_competencia;
                }
        }    

    public function scopeDoperfat($query, $id_periodo_faturamento){
        return $query->whereRaw("id_faturamento in (select id_faturamento from faturamento where id_periodo_faturamento = ".$id_periodo_faturamento.")");
    }

    public function scopeDeclientescomfaturamentohabilitado($query){
        return $query->whereRaw("id_faturamento in (select id_faturamento from faturamento where id_cliente in (select id_cliente from cliente where coalesce(fl_emissaonf_suspensa,0) = 0))");
    }

    public function scopeDeclientescomvencimentoem($query, $dia_vencimento){
            if ($dia_vencimento > 0){
                return $query->whereRaw("id_faturamento in (select id_faturamento from faturamento where id_cliente in (select id_cliente from cliente where dia_vencimento = ".$dia_vencimento."))");                
            } else {
                return $query;                    
            }
    }

    public function scopeDeclientescomtipodefaturamento($query, $id_tipo_faturamento){
        if ($id_tipo_faturamento > 0){
            return $query->whereRaw("id_faturamento in (select id_faturamento from faturamento where id_cliente in (select id_cliente from cliente where id_tipo_faturamento = ".$id_tipo_faturamento."))");                
        } else {
            return $query;                    
        }
}
    
    
        public function scopeSemlote($query){
                return $query->whereNull("id_lote_rps");
        }    

        public function scopeDolote($query, $id_lote_rps){
                return $query->where('id_lote_rps', '=', $id_lote_rps);
        }

        public function valores_em_array_tipo_20(){
                $ret = [];
        
                $ret['rps_numero'] = $this->id_nota_fiscal;
                // Carrega dados do tomador
                $cliente = $this->cliente_emissao;
                if ($cliente->fl_pessoa_fisica == ''){
                    $ret['tom_tipo_doc'] = '2';
                } else {
                    $ret['tom_tipo_doc'] = '1';
                }
                $ret['tom_documento'] = preg_replace("/\D/", "", $cliente->cgc);
//                $ret['tom_insc_municipal'] = preg_replace("/\D/", "", $cliente->im);
//                $ret['tom_insc_estadual'] = preg_replace("/\D/", "", $cliente->ie);
                $ret['tom_insc_estadual'] = "0";
                $ret['tom_razao_social'] = $cliente->razaosocial;
        
                // Carrega endereco do tomador
                $ret['end_tipo'] = $cliente->logradouro;
                $ret['end_endereco'] = $cliente->endereco;
                $ret['end_numero'] = $cliente->numero;
                $ret['end_compl'] = $cliente->complemento;
                $ret['end_bairro'] = $cliente->bairro;
                $ret['end_cidade'] = $cliente->cidade;
                $ret['end_uf'] = $cliente->estado;
                $ret['end_cep'] = preg_replace("/\D/", "", $cliente->cep);
        
                // $ret['tom_telefone'] = $cliente->telefone_nota();        
                // $ret['tom_email'] = $cliente->email_nota();
        
                $ret['srv_valor'] = $this->valor_total_emissao;
                // $ret['srv_iss'] = $this->valor_iss_emissao;
                $ret['srv_iss'] = 0;
                $ret['srv_iss_dt_competencia'] = $this->dt_competencia_emissao;
                $ret['srv_descricao'] = $this->descricao_servico_emissao." ".$cliente->descricao_servicos;
                
                return $ret;
        }

        public function valores_em_array_boleto(){
                $ret = [];
                
                $ret['nfis_numero'] = $this->nfis_numero;
                // Carrega dados do tomador
                $cliente = $this->cliente_emissao;
                if ($cliente->fl_pessoa_fisica == ''){
                        $ret['tipo_pessoa'] = '02';
                        $ret['tipo_pessoa2'] = '02';
                } else {
                        $ret['tipo_pessoa'] = '01';
                        $ret['tipo_pessoa2'] = '01';
                }

                $ret['vl_titulo'] = $this->valor_total_emissao;
                $emissao = Carbon::createFromFormat('Y-m-d H:i:s', $this->dthr_emissao);
                $emissao->addMonth();  
                $ret['dt_vencimento'] = Carbon::createFromDate($emissao->year, $emissao->month, $cliente->dia_vencimento_boleto, 'America/Sao_Paulo')->format('dmy');
                $ret['dt_emissao'] = Carbon::createFromFormat('Y-m-d H:i:s', $this->dthr_emissao)->format('dmy');
                
                $ret['cgc'] = preg_replace("/\D/", "", $cliente->cgc);
                $ret['razaosocial'] = $cliente->razaosocial;
                $ret['endereco'] = $cliente->logradouro . ' ' . $cliente->endereco. ' ' . $cliente->numero. ' '. $cliente->complemento;
                $ret['bairro'] = $cliente->bairro;
                $ret['cidade'] = $cliente->cidade;
                $ret['uf'] = $cliente->estado;
                $ret['cep'] = preg_replace("/\D/", "", $cliente->cep);
        
                // Carrega endereco do tomador
                return $ret;
                        
        }
}