<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * 
 * @author Leonardo
 * @property int 		$id_usuario		Chave
 * @property string 	$username		Login do usuário
 * @property string 	$password		Senha
 * @property char(1)	$adm			Indica se é administrador ou não
 * @property int		$id_cliente		Chave do cliente 
 * @property int		$id_nivel		Nível de acesso
 * @property string		$nomeusuario	Nome do usuário
 * @property int		$acessonumero	Quantidade de vezes que o usuário acessou
 * @property int		$validadelogin	Quantidade de dias que o login é valido a partir da data de cadastro
 * @property string		$aniversario	Dia/mes do aniversário do usuário
 * @property date		$datacadastro	Data em que o usuário foi cadastrado
 * @property date		$bloqueio		Data a partir de quando o acesso deve ser bloqueado
 * @property datetime	$created_at		Data e hora em que o registro foi inserido
 * @property datetime	$updated_at		Data e hora em que o registro foi atualizado
 * @property boolean	$interno		Indica se é um usuário interno ou externo (cliente)
 * @property int		$id_usuario_cadastro	Indica o usuário que cadastro esse usuário
 * @property datetime	$deleted_at		Indica quando o usuário foi excluído
 * @property datetime	$ultimo_acesso	Indica a data e hora da última vez que esse usuário acessou o sistema
 * @property int		$id_cliente_principal	Indica o cliente que está valendo para esse usuário
 * @property boolean	$fl_primeira_vez	Indica se é a primeira vez que o usuário está acessando o sistema
 * @property int		$aniversario_dia	Indica o dia do aniversário do usuário
 * @property int		$aniversario_mes	Indica o mês do aniversário do usuário
 * @property string 	$remember_token		Token de controle de acesso.
 * 	
 */
class Usuario extends Modelo implements UserInterface, RemindableInterface {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usuario';
    protected $primaryKey = 'id_usuario';
    protected $fillable = array(
        'username'
        , 'nomeusuario'
        , 'aniversario'
        , 'validadelogin'
        , 'id_nivel'
        , 'bloqueio'
        , 'aniversario_dia'
        , 'aniversario_mes'
        , 'token_nova_senha'
        , 'dt_emissao_token_senha'
    );
    protected $senha_original;
    protected $dates = array('dt_emissao_token_senha');
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword() {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail() {
        return $this->email;
    }

    public function CarregaDoRecordsetFirebird($pRs) {
        foreach ($pRs as $coluna => $valor) {
            $nivel = Nivel::find($pRs['NIVEL']);
            if (is_object($nivel)) {
                $this->nivel = $pRs['NIVEL'];
            } else {
                $nivel = new Nivel();
                $nivel->descricao = "usuário nível " . $pRs['NIVEL'];
                $nivel->save();
                $this->id_nivel = $nivel->id;
            }
            if ($coluna == 'NIVEL') {
                $this->id_nivel = $pRs['NIVEL'];
            } elseif ($coluna == 'USU') {
                $this->username = $pRs['USU'];
            } elseif ($coluna == 'SEN') {
                $this->password = Hash::make($pRs['SEN']);
            } elseif ($coluna == 'NOMEUSUARIO') {
                $this->nomeusuario = ucwords(strtolower(utf8_encode($pRs['NOMEUSUARIO'])));
            } else {
                $coluna = strtolower($coluna);
                $this->$coluna = utf8_encode($valor);
                if ($this->$coluna == '') {
                    $this->$coluna = null;
                }
            }
            $this->interno = false;
        }
        $this->datacadastro = date('Y-m-d');
    }

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function clienteprincipal() {
        return $this->belongsTo('Cliente', 'id_cliente_principal');
    }

    public function usuarionivel() {
        return $this->belongsTo('Nivel', 'nivel');
    }

    public function nivel() {
        return $this->belongsTo('Nivel', 'id_nivel');
    }

    public function privilegio() {
        if (is_object($this->nivel)) {
            return $this->nivel->previlegio;
        } else {
            return 0;
        }
    }

    public function departamentos() {
        return $this->belongsToMany('DepCliente', 'usuariodepartamento', 'id_usuario', 'id_departamento')->orderBy('nome');
    }

    public function centrocustos() {
        return $this->belongsToMany('CentroDeCusto', 'usuariocentrocusto', 'id_usuario', 'id_centrocusto')->orderBy('nome');
    }

    public function setores() {
        return $this->belongsToMany('SetCliente', 'usuariosetor', 'id_usuario', 'id_setor')->orderBy('nome');
    }

    public function clientes() {
        return $this->belongsToMany('Cliente', 'usuario_cliente', 'id_usuario', 'id_cliente')->orderBy('razaosocial');
    }

    public function cadastradopor() {
        return $this->belongsTo('Usuario', 'id_usuario_cadastro');
    }

    public function scopeFiltrarPor($query, $id_cliente = '', $nomeusuario = '') {
        $query->leftJoin('cliente', 'cliente.id_cliente', '=', 'usuario.id_cliente_principal');
        if ($id_cliente != '') {
            if ($id_cliente == '-1') {
                $query->where('adm', '=', 1);
            } else {
                $query->where('usuario.id_cliente_principal', '=', $id_cliente);
            }
        }
        if ($nomeusuario != '') {
            $query->where(function ($query) use ($nomeusuario) {
                $query->orWhere('nomeusuario', 'like', '%' . $nomeusuario . '%')
                        ->orWhere('username', 'like', '%' . $nomeusuario . '%');
            });
        }
        return $query;
    }

    public function scopeSearch($query, $term) {
        if (trim($term) <> '') {
            return $query->where('username', 'like', '%' . $term . '%')
                            ->where('nomeusuario', 'like', '%' . $term . '%')
            ;
        } else {
            return $query;
        }
    }

    public function scopeInternos($query) {
        return $query->where('id_nivel', '>', '10');
    }

    public function scopeExternos($query) {
        return $query->where(function ($query) {
                    $query->where('adm', '=', 1)->orWhere('id_nivel', '<', '10');
                });
    }

    public function scopeDoCliente($query, $id_cliente) {
        return $query->where('usuario.id_cliente_principal', '=', $id_cliente);
    }

    public static function rand_string($length) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }

    public function incrementaAcesso() {
        $this->acessonumero++;
        $this->ultimoacesso = date('Y-m-d H:i:s');
        $this->save();
    }

    public function getBloqueioAttribute($data) {
        if (isset($this->attributes['bloqueio'])) {
            return $this->dtModelParaView($this->attributes['bloqueio']);
        } else {
            return '';
        }
    }

    public function setBloqueioAttribute($data) {
        $this->attributes['bloqueio'] = $this->dtViewParaModel($data);
    }

    public function qtdClientes() {
        return $this->clientes()->count();
    }

    /**
     * Gera query de clientes que esse usuÃ¡rio tem acesso.
     */
    public function getListaClientes() {
        if ($this->adm == 1) {
            $clientes = \Cliente::ativos()->select(array('id_cliente', DB::raw("concat(razaosocial, ' (', substr(concat('000',id_cliente), -3), ' - ', nomefantasia, ')') as razaosocial")))
                    ->orderBy('razaosocial')
                    ->lists('razaosocial', 'id_cliente');
        } else {
            $clientes = \Cliente::select(array('id_cliente', DB::raw("concat(razaosocial, ' (', substr(concat('000',id_cliente), -3), ' - ', nomefantasia, ')') as razaosocial")))
                    ->whereIn('id_cliente', function($query) {
                        $query->select('id_cliente')
                        ->from('usuario_cliente')
                        ->where('id_usuario', '=', Auth::user()->id_usuario);
                    }
                    )
                    ->orWhere('id_cliente', '=', Auth::user()->id_cliente_principal)
                    ->orWhere('id_cliente', '=', Auth::user()->id_cliente)
                    ->orderBy('razaosocial')
                    ->lists('razaosocial', 'id_cliente');
        }
        return $clientes;
    }

    public function atualizaClientes() {
        
    }

    public function getRememberToken() {
        return $this->remember_token;
    }

    public function setRememberToken($value) {
        $this->remember_token = $value;
    }

    public function getRememberTokenName() {
        return 'remember_token';
    }

    public function recebeSenhaPost($post) {
        
    }

    public function exibeNotificacao() {
        if ($this->id_nivel < 10) {
            if ($this->adm) {
                if ($this->id_cliente > 0 && $this->cliente->nivel_inadimplencia > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                $pendentes = \DB::table('usuario_cliente')
                        ->join('cliente', 'cliente.id_cliente', '=', 'usuario_cliente.id_cliente')
                        ->where('usuario_cliente.id_usuario', '=', $this->id_usuario)
                        ->where('cliente.nivel_inadimplencia', '>', 0)
                        ->count();
                if ($pendentes > 0 || $this->cliente->nivel_inadimplencia > 0) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public function temClienteBloqueado() {
        if ($this->id_nivel < 10) {
            if (!$this->adm) {
                $pendentes = \DB::table('usuario_cliente')
                        ->join('cliente', 'cliente.id_cliente', '=', 'usuario_cliente.id_cliente')
                        ->where('usuario_cliente.id_usuario', '=', $this->id_usuario)
                        ->where('cliente.nivel_inadimplencia', '=', 2)
                        ->count();
                if ($pendentes > 0 || ($this->cliente->nivel_inadimplencia == 2)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getDatacadastroFmtAttribute(){
        return $this->formatDate('datacadastro');
    }

    public function link_nova_senha(){
        $ret = '';
        $ret =  url('auth/renovarsenha', $parameters = array('token' => $this->token_nova_senha), $secure = null);
        return $ret;

    }
}
