<?php

/**
 * @property int $id_departamento Chave
 * @property int $id_cliente Cliente
 * @property string $nome Nome do centro de custo
 */
class ConfigEtiqueta extends Modelo {

    protected $table = 'config_etiqueta';
    protected $primaryKey = 'id_config';



    public function scopePadrao($query){
        return $query->where('fl_default', '=', 1);
    }
}
