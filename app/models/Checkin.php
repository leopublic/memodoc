<?php

/**
 * @property int $id_os_chave
 * @property int $id_caixa
 * @property int $id_cliente
 * @property int $fl_primeira_entrada
 */
class Checkin extends Eloquent {

    protected $table = 'checkin';
    protected $primaryKey = 'id_checkin';
    protected $guarded = array();
    public static $rules = array();

    public function os() {
        return $this->belongsTo('Os', 'id_os_chave');
    }

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function documento() {
        return $this->belongsTo('Documento', 'id_caixa');
    }

    public function transbordo() {
        return $this->belongsTo('Transbordo', 'id_transbordo');
    }

    public function getDocumento(){
        return Documento::where('id_caixa', '=', $this->id_caixa)->first();
    }
    
    public function getId_caixapadrao(){
        $doc = Documento::where('id_caixa', '=', $this->id_caixa)->first();
        if (isset($doc)){
            return substr('000000'.$doc->id_caixapadrao, -6);
        } else {
            $doc = DocumentoCancelado::where('id_caixa', '=', $this->id_caixa)->first();
            if (isset($doc)){
                return substr('000000'.$doc->id_caixapadrao, -6);
            } else {
                return '(caixa não encontrada)';
            }
        }
    }
    
    public function scopeDaos($query, $id_os_chave){
    	return $query->where('id_os_chave', '=', $id_os_chave);
    }

    public function scopePendentesdaos($query, $id_os_chave){
    	return $query->where('id_os_chave', '=', $id_os_chave)->whereNull('dt_checkin');
    }
    
    public function usuario(){
        return $this->belongsTo('usuario', 'id_usuario_checkin');
    }
}
