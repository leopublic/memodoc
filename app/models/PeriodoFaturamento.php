<?php
/**
 * @property int $id_periodo_faturamento
 * @property int $mes
 * @property int $ano
 * @property int $id_usuario
 * @property mixed $dt_inicio
 * @property mixed $dt_fim
 * @property mixed $dt_inicio_query É a data de início menos um dia
 * @property mixed $dt_fim_query É a data de fim mais um dia
 * @property string $observacao
 * @property datetime $dt_faturamento Data da emissão da nota fiscal
 * @property int $progresso
 * @property string $status
 * @property boolean $finalizado Indica se o processo terminou. 1 indica que terminou com sucesso. -1 indica que terminou com problema. 0 Indica que está em andamento.
 * @property datetime $dt_carga_precos Indica a data da última carga de preços
 * @property datetime $dt_recalculo Indica a data do último recalculo
 * @property int $id_usuario_preco Usuário que fez a última atualização do preço
 * @property int $id_usuario_recalculo Usuário que fez o último recalculo
 * @property boolean $fl_temporario Indica se é um faturamento temporário ou um definitivo
 * @property int $status Indica o status do faturamento. 1-Em processamento, 2-Fechado
 */
class PeriodoFaturamento extends Modelo {

    protected $table = 'periodo_faturamento';
    protected $primaryKey = 'id_periodo_faturamento';
    protected $fillable = array('dt_inicio', 'dt_fim');
    protected $dates = array('dt_faturamento');

    public function faturamento() {
        return $this->hasMany('Faturamento', 'id_periodo_faturamento');
    }

    public static function combo() {
        return PeriodoFaturamento::select(array('id_periodo_faturamento', DB::raw("concat(mes, '/', ano, ' de ', date_format(dt_inicio, ''), ' até ', date_format(dt_fim, '')) descricao")))
                        ->lists('descricao', 'id_periodo_faturamento');
    }

    public function getDtInicioFmtAttribute($data) {
        return $this->dtModelParaView(substr($this->dt_inicio, 0, 10));
    }

    public function setDtInicioFmtAttribute($data) {
        $this->dt_inicio = $this->dtViewParaModel($data);
    }

    public function getDtFimFmtAttribute($data) {
        return $this->dtModelParaView(substr($this->dt_fim, 0, 10));
    }

    public function setDtFimFmtAttribute($data) {
        $this->dt_fim = $this->dtViewParaModel($data);
    }

    public function getDtfaturamentoFmtAttribute($data) {
        return $this->dtModelParaView(substr($this->dt_faturamento, 0, 10));
    }

    public function setDtfaturamentoFmtAttribute($data) {
        $this->dt_faturamento = $this->dtViewParaModel($data);
    }

    public function getDtInicioQueryAttribute() {
        $x = \Carbon::createFromFormat('Y-m-d', $this->dt_inicio);
        return $x->subDay()->format('Y-m-d');
    }

    public function getDtFimQueryAttribute() {
        $x = \Carbon::createFromFormat('Y-m-d', $this->dt_fim);
        return $x->addDay()->format('Y-m-d');
    }

    public function cadastradopor(){
        return $this->belongsTo('Usuario', 'id_usuario');
    }

    public function calculadopor(){
        return $this->belongsTo('Usuario', 'id_usuario_recalculo');
    }

    public function precificadopor(){
        return $this->belongsTo('Usuario', 'id_usuario_preco');
    }

    public function temRecalculo(){
        if ($this->dt_recalculo != ''){
            $qtd = \Faturamento::where('id_periodo_faturamento', '=', $this->id_periodo_faturamento)
                            ->where('dt_recalculo', '<>', $this->dt_recalculo)
                            ->count();
            if ($qtd > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getStatusTextoAttribute($valor){
        switch ($this->status_conclusao){
            case 1:
                return 'Aberto';
                break;
            case 2:
                return 'Fechado';
                break;
            default:
                return 'Aberto';
                break;
        }
    }
}
