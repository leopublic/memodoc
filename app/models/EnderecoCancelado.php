<?php

/**
 * @property int $id_caixa
 * @property int $id_galpao
 * @property int $id_rua
 * @property int $id_predio
 * @property int $id_andar
 * @property int $id_unidade
 * @property int $id_cliente
 * @property int $id_tipodocumento
 * @property int $disponivel
 * @property int $reservado
 * @property int $reserva
 * @property int $endcancelado
 */
class EnderecoCancelado extends Modelo {

    protected $table = 'endereco_cancelado';
    protected $primaryKey = 'id_caixa';

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }


}
