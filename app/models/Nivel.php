<?php
class Nivel extends Modelo {
    protected $table = 'nivel';
    protected $primaryKey = 'id_nivel';

    public function get_nomeTabelaFirebird() {
        return 'TBL_PREVILEGIO';
    }

    public function scopeAdministrativos($query) {
        return $query->where('id_nivel', '>', 10)->orderBy('id_nivel');
    }

    public function scopeClientes($query) {
        return $query->where('id_nivel', '<', 10)->orderBy('id_nivel');
    }

    public function minimoOperacional(){
        return $this->minimo(17);
    }

    public function minimoAtendimentoNivel2(){
        return $this->minimo(15);
    }

    public function minimoAtendimentoNivel1(){
        return $this->minimo(14);
    }

    public function minimoAdministrador(){
        return $this->minimo(13);
    }

    public function minimoDiretoria(){
        return $this->minimo(12);
    }

    public function minimo($id_nivel_minimo){
        if ($this->id_nivel <= $id_nivel_minimo ){
            return true;
        } else {
            return false;
        }
    }
}
