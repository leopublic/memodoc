<?php
/**
 * @property int $id_faturamento
 * @property int $id_cliente
 * @property int $mes
 * @property int $ano
 * @property int $id_usuario
 * @property int $id_periodo_faturamento
 * @property mixed $dt_inicio É a data de inicio do período faturamento associado
 * @property mixed $dt_fim É a data de fim do periodo faturamento associado
 * @property int $id_status_faturamento
 * @property string $observacao
 * @property datetime $dt_faturamento
 * @property int $qtd_custodia_mes_anterior
 * @property int $qtd_custodia_periodo
 * @property int $qtd_custodia_em_carencia
 * @property int $qtd_custodia_novas
 * @property int $qtd_cartonagem_enviadas
 * @property int $qtd_retornos_de_consulta
 * @property int $qtd_frete
 * @property int $qtd_frete_urgente
 * @property int $qtd_movimentacao
 * @property int $qtd_movimentacao_urgente
 * @property int $qtd_expurgos
 * @property float $val_custodia_periodo
 * @property float $val_cartonagem_enviadas
 * @property float $val_enviadas_consulta
 * @property float $val_frete
 * @property float $val_frete_urgente
 * @property float $val_movimentacao
 * @property float $val_movimentacao_urgente
 * @property float $val_expurgos
 * @property float $val_servicos_sem_iss
 * @property float $val_outros
 * @property float $val_iss
 * @property float $val_total
 * @property float $val_dif_minimo
 * @property datetime $dt_recalculo
 * @property int $id_usuario_recalculo
 * @property int $conferido Indica se o faturamento já foi conferido
 */
class Faturamento extends Modelo{
	protected $table = 'faturamento';
	protected $primaryKey = 'id_faturamento';

    public function ordensdeservico(){
        return $this->hasMany('Os', 'id_faturamento')->orderBy('solicitado_em');
    }

    public function cliente(){
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function periodofaturamento(){
        return $this->belongsTo('PeriodoFaturamento', 'id_periodo_faturamento');
    }

    public function excedenteFranquia(){
        $franquia = $this->cliente->franquiamovimentacao;
        if ($this->qtd_movimentacao > $franquia){
            return $this->qtd_movimentacao - $franquia;
        } else {
            return 0;
        }
    }

    public function getMesAttribute(){
        return DateTime::createFromFormat('Y-m-d', $this->periodofaturamento->dt_fim)->format('m');
    }

    public function getAnoAttribute(){
        return DateTime::createFromFormat('Y-m-d', $this->periodofaturamento->dt_fim)->format('Y');
    }

    public function getDtInicioAttribute() {
        return $this->periodofaturamento->dt_inicio;
    }

    public function getDtFimAttribute() {
        return $this->periodofaturamento->dt_fim;
    }

    public function servicos(){
        return $this->hasMany('FaturamentoServico', 'id_faturamento');
    }

    public function getDtInicioQueryAttribute(){
        return $this->periodofaturamento->dt_inicio_query;
    }

    public function getDtFimQueryAttribute(){
        return $this->periodofaturamento->dt_fim_query;
    }

    public function sumariza(){
        $this->val_servicos_sem_iss = $this->val_enviadas_consulta + $this->val_frete + $this->val_frete_urgente + $this->val_custodia_periodo_cobrado + $this->val_expurgos + $this->val_movimentacao_expurgo + $this->val_movimentacao;
        if($this->cliente->issporfora){
            $this->val_iss = round(($this->val_servicos_sem_iss * 0.05), 2);
        } else {
            $this->val_iss = round(($this->val_servicos_sem_iss / 0.95) - $this->val_servicos_sem_iss, 2);
        }
    }



    public function val_totalServico(){
        if($this->cliente->issporfora){
            return $this->val_servicos_sem_iss ;
        } else {
            return $this->val_servicos_sem_iss / 0.95;
        }
    }

    public function val_totalNota(){
        if($this->cliente->issporfora){
            return $this->val_servicos_sem_iss + $this->val_iss;
        } else {
            return $this->val_servicos_sem_iss / 0.95;
        }
    }

    public function val_totalFatura(){
        return $this->val_totalNota() + $this->val_cartonagem_enviadas ;
    }

    public function val_iss_fatura(){
        $this->sumariza();
        return $this->val_iss;
    }

    public function val_totalGeral(){
        return $this->val_cartonagem_enviadas + $this->val_servicos_sem_iss;
    }

	public function getDtRecalculoFmtAttribute(){
		if ($this->attributes['dt_recalculo'] != ''){
			try{
				$x = \Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['dt_recalculo']);
				return $x->format('d/m/Y H:i:s');
			} catch(\Exception $e){
				return '--';
			}
		} else {
			return '--';
		}
		return $this->periodofaturamento->dt_fim_query;
	}

	public function usuariorecalculo(){
		return $this->belongsTo('Usuario', 'id_usuario_recalculo');
	}

	public function getRecalculadoPorAttribute(){
		if ($this->attributes['id_usuario_recalculo'] != ''){
			return $this->usuariorecalculo->nomeusuario;
		} else {
			return '--';
		}
	}

    public function emcarencia(){
        $ret = \DB::table('faturamento_emcarencia')
                ->join('documento', 'documento.id_caixa', '=', 'faturamento_emcarencia.id_caixa')
                ->join('reserva', 'reserva.id_reserva', '=', 'documento.id_reserva')
                ->select(array('faturamento_emcarencia.id_caixa', \DB::raw("date_format(reserva.data_reserva, '%d/%m/%Y') data_reserva"), \DB::raw("substr(concat('000000', documento.id_caixapadrao), -6) id_caixapadrao"), 'documento.id_reserva', 'documento.id_item', 'documento.titulo', 'documento.conteudo', \DB::raw("date_format(documento.primeira_entrada, '%d/%m/%Y %H:%i:%s') primeira_entrada") ))
                ->distinct()
                ->where('id_faturamento', '=', $this->id_faturamento)
                ->orderBy('id_caixapadrao')->orderBy('id_item')
                ->get();
        return $ret;
    }

    public function naocobradas(){
        $ret = \DB::table('faturamento_naocobradas')
                ->join('documento', 'documento.id_caixa', '=', 'faturamento_naocobradas.id_caixa')
                ->join('reserva', 'reserva.id_reserva', '=', 'documento.id_reserva')
                ->select(array('faturamento_naocobradas.id_caixa', \DB::raw("date_format(reserva.data_reserva, '%d/%m/%Y') data_reserva"), \DB::raw("substr(concat('000000', documento.id_caixapadrao), -6) id_caixapadrao"), 'documento.id_reserva',  'documento.id_item','documento.titulo', 'documento.conteudo', \DB::raw("date_format(documento.primeira_entrada, '%d/%m/%Y %H:%i:%s') primeira_entrada") ))
                ->distinct()
                ->where('id_faturamento', '=', $this->id_faturamento)
                ->orderBy('id_caixapadrao')->orderBy('id_item')
                ->get();
        return $ret;
    }

	public function scopeDoperiodo($query, $id_periodo_faturamento){
		return $query->where('id_periodo_faturamento', '=', $id_periodo_faturamento);
	}

	public function scopeEnvioporemail($query){
		return $query->whereRaw("faturamento.id_cliente in (select id_cliente from cliente where id_tipo_faturamento = 2)");
	}

	public function scopeEnvioporcorreio($query){
		return $query->whereRaw("faturamento.id_cliente in (select id_cliente from cliente where id_tipo_faturamento = 1)");
	}
}