<?php

/**
 * Representa os documentos que estão armazenados nas caixas
 *
 * - Chave: id_documento
 * - Todo documento está obrigatoriamente associado a uma caixa (id_caixa).
 * - O id_caixapadrao indica o número da caixa que é entendido pelo cliente.
 * - Os documentos são sequenciados pelo id_item
 *
 *
 * @property int $id_documento              Chave
 * @property int $id_cliente                Cliente
 * @property int $id_caixapadrao            Número da caixa para o usuário
 * @property int $id_box
 * @property int $id_item                   Sequencial dos itens da caixa
 * @property int $id_departamento           Departamento do cliente associado ao documento
 * @property int $id_setor                  Setor do cliente associado ao documento
 * @property int $id_centro                 Centro de custos do cliente associado ao documento
 * @property int $id_tipodocumento          Tipo de documento
 * @property int $id_caixa                  Id do endereço ocupado pela caixa
 * @property int $id_reserva                Id da reserva que ocupou o endereço
 * @property int $id_expurgo                Id do expurgo que excluiu o documento
 * @property varchar $titulo                Titulo
 * @property varchar $migrada_de            Referência da caixa no serviço de guarda anterior
 * @property date $inicio                   Data de início do conteúdo
 * @property date $fim                      Data de fim do conteúdo
 * @property tinyint $expurgo_programado
 * @property date $expurgarem
 * @property text $conteudo                 Descrição do conteúdo
 * @property varchar $usuario
 * @property tinyint $emcasa                Indica se o documento está no depósito (1) ou emprestado com o cliente (0)
 * @property datetime $primeira_entrada     Indica a data/hora da primeira vez que a caixa entrou no depósito.
 * @property date $data_digitacao
 * @property date $expurgada_em
 * @property tinyint $itemalterado
 * @property int $planilha_eletronica
 * @property varchar $request_recall
 * @property int $nume_inicial              Número inicial do conteúdo
 * @property int $nume_final                Número final do conteúdo
 * @property char $alfa_inicial             Alfanumérico inicial do conteúdo
 * @property char $alfa_final               Alfanumérico final do conteúdo
 * @property int $id_usuario_alteracao
 * @property varchar $evento
 * @property timestamp $created_at
 * @property timestamp $updated_at
 * @property boolean $fl_revisado
 * @property int $id_usuario_revisao
 * @property datetime $dt_revisao
 * @property int $id_usuario_liberacao
 * @property datetime $dt_liberacao
 *  */
class Documento extends Modelo {

    protected $table = 'documento';
    protected $primaryKey = 'id_documento';
    protected $fillable = array(
        'id_cliente', 'id_caixapadrao',
        'id_box', 'id_item', 'id_departamento', 'id_setor',
        'id_centro', 'id_tipodocumento', 'id_caixa', 'id_reserva', 'id_expurgo', 'titulo',
        'migrada_de', 'inicio', 'fim', 'expurgarem', 'conteudo', 'usuario', 'emcasa',
        'primeira_entrada', 'data_digitacao', 'expurgada_em', 'itemalterado', 'planilha_eletronica',
        'request_recall', 'nume_inicial', 'nume_final', 'alfa_inicial', 'alfa_final', 'id_valor'
    );

    public function __construct() {
        ;
    }

    public function endereco() {
        return $this->belongsTo('Endereco', 'id_caixa');
    }

    public function tipodocumento() {
        return $this->belongsTo('TipoDocumento', 'id_tipodocumento');
    }

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function depcliente() {
        return $this->belongsTo('DepCliente', 'id_departamento');
    }

    public function setcliente() {
        return $this->belongsTo('SetCliente', 'id_setor');
    }

    public function centrodecusto() {
        return $this->belongsTo('CentroDeCusto', 'id_centro');
    }

    public function reserva() {
        return $this->belongsTo('Reserva', 'id_reserva');
    }

    public function valor(){
        return $this->belongsTo('Valor', 'id_valor', 'id_valor');
    }

    public static function getLocalizar($id_cliente, $titulo = '', $conteudo = '', $id_setor = '', $id_centro = '', $id_departamento = '', $inicio = '', $fim = '', $nume_inicial = '', $nume_final = '', $alfa_inicial = '', $alfa_final = '', $id_caixa = '', $id_caixapadrao = '', $referencia = '', $status = '', $id_valor = '') {

        $model = self::where('id_cliente', '=', $id_cliente);

        if ($titulo <> '') {
            $model->where(function ($query) use ($titulo) {
                $query->where('titulo', 'LIKE', "%$titulo%")
                        ->orWhere('conteudo', 'LIKE', "%$titulo%");
            });
        }
        if ($conteudo <> '') {
            $model->where('conteudo', 'LIKE', "%$conteudo%");
        }
        if ($id_setor <> '') {
            $model->where('id_setor', '=', $id_setor);
        }
        if ($id_centro <> '') {
            $model->where('id_centro', '=', $id_centro);
        }
        if ($id_departamento <> '') {
            $model->where('id_departamento', '=', $id_departamento);
        }

        // Intervalo cronológico
        if ($inicio <> '') {
            $model->where('inicio', '>=', Carbon::createFromFormat('d/m/Y', $inicio)->format('Y-m-d'));
        }
        if ($fim <> '') {
            $model->where('fim', '<=', Carbon::createFromFormat('d/m/Y', $fim)->format('Y-m-d'));
        }

        // Intervalo numérico
        if ($nume_inicial <> '') {
            $model->where('nume_inicial', '>=', $nume_inicial);
        }
        if ($nume_final <> '') {
            $model->where('nume_final', '<=', $nume_final);
        }

        // Intervalo alfabetico
        if ($alfa_inicial <> '') {
            $model->where('alfa_inicial', '>=', $alfa_inicial);
        }
        if ($alfa_final <> '') {
            $model->where('alfa_final', '<=', $alfa_final);
        }

        if ($id_caixa <> '') {
            $model->where('id_caixa', '=', $id_caixa);
        }

        if ($id_caixapadrao <> '') {
            $model->where('id_caixapadrao', '=', $id_caixapadrao);
        }

        if ($referencia <> '') {
            $model->where(function ($query) use ($referencia) {
                $query->where('migrada_de', 'LIKE', "%$referencia%")
                        ->orWhere('request_recall', 'LIKE', "%$referencia%");
            });
        }

        if ($status <> '') {
            if ($status == 'emconsulta') {
                $model->where('emcasa', '=', '0');
            } elseif ($status == 'reservadas') {
                $model->whereNull('primeira_entrada');
            }
        }

        if ($id_valor <> ''){
            $model->where('id_valor', '=', $id_valor);
        }

        //return $model->take(20)->get();
        $model->orderBy('id_caixapadrao')->orderBy('id_item');
        return $model->paginate(20);
    }

    public function scopeTituloConteudo($query, $titulo){
        if ($titulo <> '') {
            if (str_contains($titulo, '-')) {
                $where = '';
                $op = "";
                $termos = explode('-', $titulo);
                foreach ($termos as $termo) {
                    $termo = trim($termo);
                    $where .= $op." concat(titulo, conteudo) LIKE '%".$termo."%'";
                    $op = " and ";
                }
                $where = "(".$where.")";
                $query->whereRaw($where);
            } elseif (str_contains($titulo, '+')) {
                $whereTit = '';
                $whereCont = '';
                $op = "";
                $termos = explode('+', $titulo);
                foreach ($termos as $termo) {
                    $termo = trim($termo);
                    $whereTit .= $op." titulo LIKE '%".$termo."%'";
                    $whereCont .= $op." conteudo LIKE '%".$termo."%'";
                    $op = " or ";
                }
                $where = "(".$whereTit." or ".$whereCont.")";
                $query->whereRaw($where);
            } else {
                $query->where(function ($query) use ($titulo) {
                    $query->where('titulo', 'LIKE', "%$titulo%")
                            ->orWhere('conteudo', 'LIKE', "%$titulo%");
                });
            }
        }
    }

    public function scopeSetor($query, $id){
        if ($id <> '') {
            $query->where('documento.id_setor', '=', $id);
        }
    }

    public function scopeCentrodecustos($query, $id){
        if ($id <> '') {
            $query->where('documento.id_centro', '=', $id);
        }
    }

    public function scopeDepartamento($query, $id){
        if ($id <> '') {
            $query->where('documento.id_departamento', '=', $id);
        }
    }

    public function scopeDataInicio($query, $dt){
        if ($dt != '' && $dt != '__/__/____' && $dt != 'de' && $dt != 'até') {
            $query->where('inicio', '>=', Carbon::createFromFormat('d/m/Y', $dt)->format('Y-m-d'));
        }
    }

    public function scopeDataFim($query, $dt){
        if ($dt != '' && $dt != '__/__/____' && $dt != 'de' && $dt != 'até') {
            $query->where('fim', '<=', Carbon::createFromFormat('d/m/Y', $dt)->format('Y-m-d'));
        }
    }

    public function scopeprimeiraentradainicio($query, $dt){
        if ($dt != '' && $dt != '__/__/____' && $dt != 'de' && $dt != 'até') {
            $query->where('primeira_entrada', '>=', Carbon::createFromFormat('d/m/Y', $dt)->format('Y-m-d'));
        }
    }

    public function scopeprimeiraentradafim($query, $dt){
        if ($dt != '' && $dt != '__/__/____' && $dt != 'de' && $dt != 'até') {
            $query->where('primeira_entrada', '<=', Carbon::createFromFormat('d/m/Y', $dt)->format('Y-m-d'));
        }
    }

    public function scopeSolicitadoEmIni($query, $dt){
        if ($dt <> '') {
            $query->where('solicitado_em', '>=', Carbon::createFromFormat('d/m/Y', $dt)->format('Y-m-d').' 00:00:00');
        }
    }

    public function scopeSolicitadoEmFim($query, $dt){
        if ($dt <> '') {
            $query->where('solicitado_em', '<=', Carbon::createFromFormat('d/m/Y', $dt)->format('Y-m-d').' 23:59:59');
        }
    }

    public function scopeNumeroInicio($query, $val){
        if ($val <> '') {
            $query->where('nume_inicial', '>=', $val);
        }
    }

    public function scopeNumeroFim($query, $val){
        if ($val <> '') {
            $query->where('nume_final', '<=', $val);
        }
    }

    public function scopeAlfaInicio($query, $val){
        if ($val <> '') {
            $query->where('alfa_inicial', '>=', $val);
        }
    }

    public function scopeAlfaFim($query, $val){
        if ($val <> '') {
            $query->where('alfa_final', '<=', $val);
        }
    }

    public function scopeIdCaixaPadrao($query, $id_caixapadrao, $id_caixapadrao_fim = ''){
        if ($id_caixapadrao <> '') {
            if ($id_caixapadrao_fim == ''){
                $query->whereIn('documento.id_caixapadrao', explode(",", $id_caixapadrao));
            } else {
                $query->where('documento.id_caixapadrao', '>=', $id_caixapadrao)
                      ->where('documento.id_caixapadrao', '<=', $id_caixapadrao_fim);
            }
        }
    }

    public function scopeIdCaixa($query, $val){
        if ($val <> '') {
            $query->where('documento.id_caixa', '=', $val);
        }
    }

    public function scopeReferencia($query, $referencia) {
        if ($referencia <> '') {
            $query->where(function ($query) use ($referencia) {
                $query->where('migrada_de', 'LIKE', "%$referencia%")
                        ->orWhere('request_recall', 'LIKE', "%$referencia%");
            });
        }
    }
    public function scopeFiltre($query, $id_cliente, $titulo = '', $id_setor = '', $id_centro = '', $id_departamento = '', $inicio = '', $fim = '', $nume_inicial = '', $nume_final = '', $alfa_inicial = '', $alfa_final = '', $id_caixa = '', $id_caixapadrao = '', $referencia = '', $status = '', $primeira_entrada_inicio = '', $primeira_entrada_fim = '') {
        $query->filtrarPor($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $primeira_entrada_inicio = '', $primeira_entrada_fim = '');
        return $query;
    }
    public function scopeDocumentosSelecionados($query, $id_cliente, $titulo = '', $id_setor = '', $id_centro = '', $id_departamento = ''
                , $inicio = '', $fim = '', $nume_inicial = '', $nume_final = '', $alfa_inicial = '', $alfa_final = ''
                , $id_caixa = '', $id_caixapadrao = '', $referencia = ''
                , $status = '', $reserva_inicio = '', $reserva_fim = '', $id_caixapadrao_fim = ''
                , $migradas = '', $movimentacao = '', $id_valor = '', $primeira_entrada_inicio = '', $primeira_entrada_fim = '') {
        $query->filtrarPor($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim);
        $query->ordenacaoPadrao();
        return $query;
    }

    public function QtdDocumentosSelecionados2($query) {
        $x = clone $query;
        return $x->select('id_documento')->distinct()->count('id_documento');
    }
    public function QtdCaixasSelecionados2($query) {
        $x = clone $query;
        return $x->select('id_caixa')->distinct()->count('id_caixa');
    }

    public function qtdDocumentosSelecionados($id_cliente, $titulo = '', $id_setor = '', $id_centro = '', $id_departamento = '', $inicio = '', $fim = '', $nume_inicial = '', $nume_final = '', $alfa_inicial = '', $alfa_final = '', $id_caixa = '', $id_caixapadrao = '', $referencia = '', $status = '', $reserva_inicio = '', $reserva_fim = '', $id_caixapadrao_fim = '', $migradas = '', $movimentacao = '', $id_valor='', $primeira_entrada_inicio = '', $primeira_entrada_fim = '') {
        $query = Documento::filtrarPor($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim) ;
        return $query->select('id_documento')->distinct()->count('id_documento');
    }
    public function qtdCaixasSelecionados($id_cliente, $titulo = '', $id_setor = '', $id_centro = '', $id_departamento = '', $inicio = '', $fim = '', $nume_inicial = '', $nume_final = '', $alfa_inicial = '', $alfa_final = '', $id_caixa = '', $id_caixapadrao = '', $referencia = '', $status = '', $reserva_inicio = '', $reserva_fim = '', $id_caixapadrao_fim = '', $migradas = '', $movimentacao = '', $id_valor = '', $primeira_entrada_inicio = '', $primeira_entrada_fim = '') {
        $query = Documento::filtrarPor($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim);
        return $query->select('id_caixa')->distinct()->count('id_caixa');
    }

    public function scopeFiltrarPor($query, $id_cliente
            , $titulo = '', $id_setor = '', $id_centro = '', $id_departamento = ''
            , $inicio = '', $fim = ''
            , $nume_inicial = '', $nume_final = ''
            , $alfa_inicial = '', $alfa_final = ''
            , $id_caixa = '', $id_caixapadrao = ''
            , $referencia = '', $status = ''
            , $reserva_inicio = '', $reserva_fim = '', $id_caixapadrao_fim = ''
            , $migradas = '', $movimentacao = '', $id_valor= '', $primeira_entrada_inicio = '', $primeira_entrada_fim = ''
            , $exp_progr_mes_ini = '', $exp_progr_ano_ini = '', $exp_progr_mes_fim = '', $exp_progr_ano_fim = '') {

        $query->with('setcliente')->with('depcliente')->with('centrodecusto');
        $query->doCliente($id_cliente);
        $query->tituloConteudo($titulo);
        $query->setor($id_setor);
        $query->centrodecustos($id_centro);
        $query->departamento($id_departamento);
        $query->dataInicio($inicio);
        $query->dataFim($fim);
        $query->numeroInicio($nume_inicial);
        $query->numeroFim($nume_final);
        $query->alfaInicio($alfa_inicial);
        $query->alfaFim($alfa_final);
        $query->idCaixa($id_caixa);
        $query->idCaixaPadrao($id_caixapadrao, $id_caixapadrao_fim);
        $query->referencia($referencia);
        $query->status($status);
        $query->primeiraentradainicio($primeira_entrada_inicio);
        $query->primeiraentradafim($primeira_entrada_fim);

        if ($reserva_inicio != '' || $reserva_fim != ''){
            $query->whereIn('id_reserva', function($query) use($reserva_inicio, $reserva_fim)
                {
                    $query->select('id_reserva')
                          ->from('reserva');
                    if ($reserva_inicio != ''){
                        $inicio = \DateTime::createFromFormat('d/m/Y', $reserva_inicio)->format('Y-m-d');
                              $query->whereRaw("data_reserva >= '".$inicio."'");
                    }
                    if ($reserva_fim != ''){
                        $fim = \DateTime::createFromFormat('d/m/Y', $reserva_fim)->format('Y-m-d');
                        $query->whereRaw("data_reserva <= '".$fim."'");
                    }
                });
        }

        // Migradas
        if ($migradas != 'todas' && $migradas != ''){
            if ($migradas == 'sim'){
                $query->whereRaw("( coalesce(migrada_de, '')  <> '' or coalesce(request_recall, '') <> '') ");
            } else {
                $query->whereRaw("( coalesce(migrada_de, '')  = '' and coalesce(request_recall, '') = '')");
            }
        }
        // Movimentacao
        if ($movimentacao != 'todas' && $movimentacao != ''){
            if ($movimentacao == 'sim'){
                $query->whereRaw("documento.id_caixa in   (select id_caixa from osdesmembrada where id_cliente = documento.id_cliente and id_caixa = documento.id_caixa ) ");
            } else {
                $query->whereRaw("documento.id_caixa not in   (select id_caixa from osdesmembrada where id_cliente = documento.id_cliente and id_caixa = documento.id_caixa ) ");
            }
        }

        if ($id_valor != '' && $id_valor != 'todas'){
            $query->where('id_valor', '=', $id_valor);
        }

        $exp_progr_ini = '';
        if ($exp_progr_ano_ini != ''){
            if ($exp_progr_mes_ini != ''){
                $exp_progr_ini = $exp_progr_ano_ini . substr('00'.$exp_progr_mes_ini, -2);
            } else {
                $exp_progr_ini = $exp_progr_ano_ini . '00';
            }
        }

        $exp_progr_fim = '';
        if ($exp_progr_ano_fim != ''){
            if ($exp_progr_mes_fim != ''){
                $exp_progr_fim = $exp_progr_ano_fim . substr('00'.$exp_progr_mes_fim, -2);
            } else {
                $exp_progr_fim = $exp_progr_ano_fim . '99';
            }
        }

        if ($exp_progr_ini != ''){
            $query->whereNotNull('expurgo_programado');
            $raw = " concat(expurgo_programado, substr(concat('00',coalesce(expurgo_programado_mes, '1')), -2) ) >=".$exp_progr_ini;
            \Log::info('INICIO: '.$raw);
            $query->whereRaw($raw);
        }

        if ($exp_progr_fim != ''){
            $query->whereNotNull('expurgo_programado');
            $raw = " concat(expurgo_programado, substr(concat('00',coalesce(expurgo_programado_mes, '1')), -2) ) <=".$exp_progr_fim;
            \Log::info('FIM: '.$raw);
            $query->whereRaw($raw);
        }

        return $query;
    }

    public function scopeOrdenacaoPadrao($query){
        return $query->orderBy('id_caixapadrao')
                     ->orderBy('id_item');
    }

    public function scopeDisponiveisPara($query, Usuario $usuario) {
        $query->doCliente($usuario->id_cliente);
        // Departamentos
        $departamentos = $usuario->departamentos()->get();
        if (count($departamentos) > 0) {
            $query->whereIn('id_departamento', $departamentos->lists('id_departamento'));
        }
        // Setores
        $setores = $usuario->setores()->get();
        if (count($setores) > 0) {
            $query->whereIn('id_setor', $setores->lists('id_setor'));
        }
        // Centros de custo
        $centrosdecusto = $usuario->centrocustos()->get();
        if (count($centrosdecusto) > 0) {
            $query->whereIn('id_centro', $centrosdecusto->lists('id_centrodecusto'));
        }

        return $query;
    }

    public function scopeDoCliente($query, $id_cliente) {
        return $query->where('documento.id_cliente', '=', $id_cliente);
    }

    public function AssumaProximoNumeroDisponivel() {
        $this->id_caixapadrao = $this->getMaiorCaixaNaoCancelada($this->id_cliente);
        if (intval($this->id_caixapadrao) == 0) {
            $this->id_caixapadrao = 1;
        } else {
            $this->id_caixapadrao++;
        }
    }

    public function getMaiorCaixaNaoCancelada($id_cliente){
        return DB::table('documento')
                ->where('id_cliente', '=', $id_cliente)
                ->max('id_caixapadrao')
                ;

    }

    public function scopeStatus($query, $status){
        if ($status <> '') {
            if ($status == 'emconsulta') {
//                $query->whereRaw("emcasa = 0 and ((primeira_entrada is not null and primeira_entrada <> '0000-00-00') )");
                $query->caixasEmConsulta();
            } elseif ($status == 'reservadas') {
//                $query->whereRaw("emcasa = 0 and (primeira_entrada is null or primeira_entrada = '0000-00-00') and titulo = 'CAIXA NOVA ENVIADA PARA CLIENTE'");
                $query->CaixasNaoinventariadasNaoEnviadas();
            } elseif ($status == 'emcasa') {
                $query->whereRaw("emcasa = 1");
            } elseif ($status == 'naoenviadas') {
                $query->CaixasInventariadasNaoEnviadas();
//                $query->whereRaw("emcasa = 0 and (primeira_entrada is null or primeira_entrada = '0000-00-00') and titulo <> 'CAIXA NOVA ENVIADA PARA CLIENTE'");
            } elseif ($status == 'emcarencia') {
//                $query->whereRaw("emcasa = 0 and (primeira_entrada is null or primeira_entrada = '0000-00-00') and exists(select id_reserva from reserva where id_reserva = documento.id_reserva and datediff(now(), data_reserva) > (select coalesce(franquiacustodia, 0) from cliente where id_cliente = documento.id_cliente) ) ");
                $query->caixasEmCarencia();
            } elseif ($status == 'emfranquia') {
//                $query->whereRaw("emcasa = 0 and (primeira_entrada is null or primeira_entrada = '0000-00-00') and exists(select id_reserva from reserva where id_reserva = documento.id_reserva and datediff(now(), data_reserva) > (select coalesce(franquiacustodia, 0) from cliente where id_cliente = documento.id_cliente) ) ");
                $query->caixasEmFranquia();
            }
        }
        return $query;
    }

    public function scopeCaixasEmConsulta($query) {
        return $query->where('emcasa', '=', 0)
                     ->whereRaw("coalesce(primeira_entrada, '0000-00-00') <> '0000-00-00' ");
    }

    public function scopeCaixasNaoinventariadasNaoEnviadas($query) {
        return $query->where('emcasa', '=', 0)
                     ->whereRaw("coalesce(primeira_entrada, '0000-00-00') = '0000-00-00'")
                     ->where('titulo', '=', 'CAIXA NOVA ENVIADA PARA CLIENTE')
                     ->whereNull('conteudo');
    }

    public function scopeCaixasInventariadasNaoEnviadas($query) {
        return $query->where('emcasa', '=', 0)
                     ->whereRaw("coalesce(primeira_entrada, '0000-00-00') = '0000-00-00'")
                     ->whereRaw("(titulo <> 'CAIXA NOVA ENVIADA PARA CLIENTE' or conteudo is not null)");
    }

    public function scopeCaixasEmCarencia($query) {
        return $query->where('emcasa', '=', 0)
                     ->whereRaw("coalesce(primeira_entrada, '0000-00-00') = '0000-00-00'")
                     ->whereExists(function($query){
                            $query->select('id_reserva')
                                    ->from('reserva')
                                    ->whereRaw('id_reserva = documento.id_reserva')
                                    ->whereRaw('datediff(now(), data_reserva) > (select coalesce(franquiacustodia, 0) from cliente where id_cliente = documento.id_cliente)');
                        });
    }

    public function scopeCaixasEmFranquia($query) {
        return $query->where('emcasa', '=', 0)
                     ->whereRaw("coalesce(primeira_entrada, '0000-00-00') = '0000-00-00'")
                     ->whereExists(function($query){
                            $query->select('id_reserva')
                                    ->from('reserva')
                                    ->whereRaw('id_reserva = documento.id_reserva')
                                    ->whereRaw('datediff(now(), data_reserva) <= (select coalesce(franquiacustodia, 0) from cliente where id_cliente = documento.id_cliente)');
                        });
    }
    public function scopeCaixasExpurgadas($query) {
        return $query->where('id_expurgo', '=', 1);
    }

    public function get_data_checkout($id_os_chave) {
        $checkout = Checkout::where('id_os_chave', '=', $id_os_chave)
                ->where('id_caixapadrao', '=', $this->id_caixapadrao)
                ->first();
        if (isset($checkout)) {
            return $checkout->data_checkout;
        } else {
            return '(pendente)';
        }
    }

    public function scopePelaReferencia($query, $id_cliente, $id_caixapadrao) {
        return $query->where('id_cliente', '=', $id_cliente)
                        ->where('id_caixapadrao', '=', $id_caixapadrao)
        ;
    }

    public static function caixasPorLista($caixas) {
        return DB::table('documento')
                        ->whereIn('id_caixa', $caixas)
                        ->join('cliente', 'cliente.id_cliente', '=', 'documento.id_cliente')
                        ->select(array('documento.id_caixa', 'documento.id_caixapadrao', 'cliente.razaosocial'))
                        ->distinct()
                        ->orderBy('id_caixapadrao')
                        ->get();
    }

    public static function sqlAcervo($id_cliente, $id_caixapadrao_ini = '', $id_caixapadrao_fim = ''){
        $sql = "select c.nome nome_centrodecustos"
                . ", d.id_centro"
                . ", d.id_departamento"
                . ", dep.nome nome_depcliente"
                . ", d.id_caixapadrao"
                . ", substr(concat('000',d.id_item), -3) id_item_fmt"
                . ", d.titulo, d.conteudo"
                . ", date_format(inicio, '%d/%m/%Y') inicio_fmt "
                . ", date_format(fim, '%d/%m/%Y') fim_fmt "
                . ", expurgo_programado "
                . ", date_format(expurgarem, '%d/%m/%Y') expurgarem_fmt"
                . ", date_format(primeira_entrada, '%d/%m/%Y') primeira_entrada_fmt"
                . ", migrada_de "
                . ", request_recall "
                . " from documento d"
                . " left join dep_cliente dep on dep.id_departamento = d.id_departamento"
                . " left join centrodecusto c on c.id_centrodecusto = d.id_centro"
                . " where d.id_cliente = ".$id_cliente;
        if ($id_caixapadrao_ini != ''){
            $sql .= " and d.id_caixapadrao >= ".$id_caixapadrao_ini;
        }
        if ($id_caixapadrao_fim != ''){
            $sql .= " and d.id_caixapadrao <= ".$id_caixapadrao_fim;
        }
        $sql .= " order by c.nome, dep.nome, d.id_caixapadrao, d.id_item ";
        return $sql;
    }

    public static function cursorAcervo($id_cliente, $id_caixapadrao_ini = '', $id_caixapadrao_fim = ''){
        return \DB::getPdo()->query(self::sqlAcervo($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim));
    }

    public function ultimoCheckout(){
        $checkout = OsDesmembrada::where('id_caixapadrao', '=', $this->id_caixapadrao)
                ->where('osdesmembrada.id_cliente', '=', $this->id_cliente)
                ->join('os', 'os.id_os_chave', '=', 'osdesmembrada.id_os_chave')
                ->orderBy('solicitado_em', 'desc')
                ->select('os.id_os_chave','os.id_os', \DB::raw("date_format(solicitado_em, '%d/%m/%Y') solicitado_em"))
                ->first();
        if (isset($checkout)) {
            return 'OS <a href="'.\URL::to('/ordemservico/visualizar/'.$checkout->id_os_chave) .'" target="_blank" class="btn btn-mini">'. substr('000000'.$checkout->id_os, -6).'</a> de '.$checkout->solicitado_em;
        } else {
            return '--';
        }
    }

    public function scopeCaixapadraoEntre($query, $id_caixapadrao_ini, $id_caixapadrao_fim = ''){
        if ($id_caixapadrao_fim > 0){
            $query->where('id_caixapadrao', '>=', $id_caixapadrao_ini)
                  ->where('id_caixapadrao', '<=', $id_caixapadrao_fim);
        } else {
            $query->where('id_caixapadrao', '=', $id_caixapadrao_ini);
        }
        return $query;
    }
    /**
     * Undocumented function
     *
     * @param [type] $query
     * @param array $id_caixapadrao_lista
     * @return void
     */
    public function scopeCaixapadraoLista($query, $id_caixapadrao_array){
        $query->whereIn('id_caixapadrao', $id_caixapadrao_array);
        return $query;
    }

    public function getInicioFmtAttribute($valor){
        if (isset($this->attributes['inicio'])){
            return $this->dtModelParaView($this->attributes['inicio']);
        } else {
            return '';
        }
    }

    public function setInicioFmtAttribute($valor){
        $this->attributes['inicio'] = $this->dtViewParaModel($valor);
    }

    public function getFimFmtAttribute($valor){
        if (isset($this->attributes['fim'])){
            return $this->dtModelParaView($this->attributes['fim']);
        } else {
            return '';
        }
    }

    public function setFimFmtAttribute($valor){
        $this->attributes['fim'] = $this->dtViewParaModel($valor);
    }
    
    public function getExpurgaremFmtAttribute($valor){
        if (isset($this->attributes['expurgarem'])){
            return $this->dtModelParaView($this->attributes['expurgarem']);
        } else {
            return '';
        }
    }

    public function setExpurgaremFmtAttribute($valor){
        $this->attributes['expurgarem'] = $this->dtViewParaModel($valor);
    }

    public function anexos(){
        return $this->hasMany('Anexo', 'id_documento', 'id_documento');
    }

    public function getSituacaoRevisaoAttribute(){
        $ret = '';
        if ($this->fl_revisado){
            $usuario = \Usuario::find($this->id_usuario_revisao);
            if ($usuario->id_usuario > 0){
                $ret = 'Revisado por '.$usuario->nomeusuario;
                $data = $this->formatDateTime('dt_revisao');
                if ($data != ''){
                    $ret .= " em ".$data;
                }
            } else {
                $ret = 'Revisado por ??';
            }
        } else {
            $ret = "Aguardando revisão";
            if ($this->id_usuario_liberacao > 0){
                $usuario = \Usuario::find($this->id_usuario_liberacao);
                $ret .= " (liberado por ".$usuario->nomeusuario;
                $data = $this->formatDateTime('dt_liberacao');
                if ($data != ''){
                    $ret .= " em ".$data;
                }
                $ret .= ')';
            }
        }
        return $ret;
    }

    public function getSituacaoRevisaoClienteAttribute(){
        $ret = '';
        if ($this->fl_revisado){
            $ret = '(Revisado pela Memodoc';
            $data = $this->formatDateTime('dt_revisao');
            if ($data != ''){
                $ret .= " em ".$data;
            }
            $ret .= ")";
        } else {
            $ret = "(Aguardando revisão)";
        }
        return $ret;
    }

    public function getSituacaoGalpaoAttribute(){
        if ($this->emcasa == 1) {
            return 'No galpão';
        } else {
            if ($this->primeira_entrada != '') {
                return 'Emprestada';
            } else {
                return 'Caixa nova';
            }
        }
    }

    public function getSituacaoGalpaoClienteAttribute(){
        if ($this->emcasa == 1) {
            return 'No galpão da Memodoc';
        } else {
            if ($this->primeira_entrada != '') {
                return 'Com o cliente';
            } else {
                return 'Caixa nova nunca enviada à Memodoc';
            }
        }
    }

    public function getIdCaixapadraoFmtAttribute(){
        return substr("000000".$this->attributes['id_caixapadrao'], -6);
    }

    public function getIdCaixaFmtAttribute(){
        return substr("000000".$this->attributes['id_caixa'], -6);
    }

    public function getStatusAttribute(){
        if (array_key_exists('status', $this->attributes)){
            return $this->attributes['status'];
        } else {
            return '';
        }
    }
    public function setStatusAttibute($valor){
        $this->attributes['status'] = $valor;
    }

    public function getExpurgoProgramadoCompletoAttribute(){
        $ret = $this->attributes['expurgo_programado'];
        if ($this->attributes['expurgo_programado_mes'] != ''){
            $ret = $this->attributes['expurgo_programado_mes']."/".$ret;
        }
        return $ret;
    }

    public function endereco_completo(){
        if (count($this->endereco)> 0){
            return $this->endereco->enderecoCompleto();
        } else {
            return '(endereço não encontrado)';
        }
    }
}
