<?php
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
/**
 * Essa classe só foi criada para permitir a importação padronizada dos usuarios 
 * internos que ficavam numa tabela separada. Não faz parte do dominio do sistema
 * novo.
 */
class UsuarioInterno extends Modelo  implements UserInterface, RemindableInterface {
	protected $table = 'usuariointerno';
	protected $primaryKey = 'id_usuario';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public function CarregaDoRecordsetFirebird($pRs) 
	{
		foreach($pRs as $coluna => $valor){

			if($coluna == 'USUARIO'){
				$this->username = utf8_encode($valor);
				$this->nomeusuario = ucwords(strtolower(utf8_encode($valor)));
			}
			elseif($coluna == 'SENHA'){
				$this->password = Hash::make($valor);
			}
			elseif($coluna != 'BLOQUEIO' && $coluna != 'ID_USUARIO'){
				$coluna = strtolower($coluna);
				$this->$coluna = utf8_encode($valor);
			}
		}
		$this->interno = true;
		$this->datacadastro = date('Y-m-d');
	}
	
}