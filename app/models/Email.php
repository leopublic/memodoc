<?php
/**
 * @property correioeletronico string
 */
class Email extends Modelo {
	public function tipoemail(){
		return $this->belongsTo('TipoEmail', 'id_tipoemail');
	}
}