<?php
class TelefoneSolicitante extends Telefone {
	protected $table = 'telefonesolicitante';
	protected $primaryKey = 'id_telefonesolicitante';

	public function solicitante(){
		return $this->belongsTo('Solicitante', 'id_solicitante');
	}

	public function CarregaDoRecordsetFirebird($pRs)
	{
		parent::CarregaDoRecordsetFirebird($pRs);
		if($this->id_tipotelefone == 0){
			$this->id_tipotelefone = null;
		}
	}
}