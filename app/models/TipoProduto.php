<?php

class TipoProduto extends Modelo {

    protected $table = 'tipoproduto';
    protected $primaryKey = 'id_tipoproduto';
    protected $fillable = array('descricao', 'valor', 'descricao_s');

    const tpFRETE           = 1;
    const tpCARTONAGEM      = 2;
    const tpARMAZENAMENTO   = 3;
    const tpMOVIMENTACAO    = 4;
    const tpEXPURGO         = 5;
    /**
     * verifica se permite deletar
     */
    static function getPermiteDeletar($id) {

        $count = self::find($id)->first()->hasMany('TIpoProdCliente', 'id_tipoproduto')->count();

        return ($count > 0) ? false : true;
    }

}
