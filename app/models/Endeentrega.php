<?php
/*
 * @property int $id_endeentrega
 * @property int $id_cliente
 * @property varchar $logradouro
 * @property varchar $endereco
 * @property varchar $numero
 * @property varchar $complemento
 * @property varchar $bairro
 * @property varchar $cidade
 * @property char $estado
 * @property varchar $cep
 * @property timestamp $created_at
 * @property timestamp $updated_at
 */
class Endeentrega extends Eloquent {
    protected $guarded = array();
    protected $table = 'endeentrega';
    protected $primaryKey = 'id_endeentrega';
    public static $rules = array();

    public function cliente(){
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function scopeCombo($query, $id_cliente)
    {
    	$raw = "concat(coalesce(concat(logradouro, ' '), '')
	    			, coalesce(concat(endereco, ' '), '')
	    			, coalesce(concat(numero, ' '), '')
	    			, coalesce(concat(complemento, ' '), '')
	    			, coalesce(concat(bairro, ' '), '')
	    		) endereco";
		return $query->where('id_cliente','=', $id_cliente)
				->select(array(DB::raw($raw), 'id_endeentrega'))
				->get()
				->lists('endereco', 'id_endeentrega');
    }

    public function completo(){
        $completo = '';
        $completo = $this->concatenaNaoVazio($completo, $this->logradouro);
        $completo = $this->concatenaNaoVazio($completo, $this->endereco);
        $completo = $this->concatenaNaoVazio($completo, $this->numero, ' - ');
        $completo = $this->concatenaNaoVazio($completo, $this->complemento);
        $completo = $this->concatenaNaoVazio($completo, $this->bairro, ' - ');
        return $completo;
    }

    public function concatenaNaoVazio($concatenado, $string, $sep = ' '){
        if (trim($string) != ''){
            return $concatenado.$sep.trim($string);
        } else {
           return $concatenado;
        }
    }

    public function scopeComboCompletoDoCliente($query, $id_cliente){
        return $query->where('id_cliente', '=', $id_cliente)
                            ->select(array(DB::raw("concat(coalesce(concat(logradouro, ' '), ''), coalesce(concat(endereco, ' '), ''), coalesce(concat(numero, ' '), ''), coalesce(concat(complemento, ' '), ''), coalesce(concat(bairro, ' '), '') , coalesce(concat(cidade, ' '), ''), coalesce(concat(estado, ' '), '')  ) endereco"), 'id_endeentrega'))
                            ->get()->lists('endereco', 'id_endeentrega');

    }
}
