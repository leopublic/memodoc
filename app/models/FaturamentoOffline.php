<?php
class FaturamentoOffline{
    public function fire($job, $data){
        set_time_limit(0);
        try{
            $perfat = \PeriodoFaturamento::where('status', '=', 0);
            $sql = "update faturamento set status_demonstrativo = 0 where id_periodo_faturamento = ".$perfat->id_periodo_faturamento;
            \DB::update(\DB::raw($sql));
            $sql = "select count(*) from faturamento where id_periodo_faturamento = ".$perfat->id_periodo_faturamento;
            $res = \DB::connection('mysql_fat')->getPdo()->query($sql);
            $rs = $res->fetch(\PDO::FETCH_BOTH);
            $quantos = $rs[0];

            $rep = new \Memodoc\Repositorios\RepositorioFaturamentoEloquent;
            $perfat->finalizado = 0;
            $perfat->status="Iniciando processamento";
            $perfat->progresso = 0;
            $perfat->save();
            \DB::commit();
            $processados = 0;

            $sql = "select id_faturamento from faturamento where id_periodo_faturamento = ".$perfat->id_periodo_faturamento." and status_demonstrativo = 0 order by id_cliente limit 1";
            $res = \DB::connection('mysql_fat')->getPdo()->query($sql);
            $rs = $res->fetch(\PDO::FETCH_BOTH);

            while($rs['id_faturamento'] > 0){
                $processados++;
                $fat = \Faturamento::find($rs['id_faturamento']);
                $perfat->status= "Gerando cliente (ID ".$fat->id_cliente.") ".$processados." de ".$quantos. " (".intval( ($processados/$quantos) * 100). "%)";
                $perfat->progresso = intval( ($processados/$quantos) * 100);
                $perfat->save();
                \DB::commit();
                try{
                    $arq = $rep->demonstrativoAnaliticoEmDisco($fat, false);
                    $fat->status_demonstrativo = 1;
                    $fat->save();
                } catch (Exception $ex) {
                    $fat->status_demonstrativo = -1;
                    $fat->save();
                }
                \DB::commit();
                $sql = "select id_faturamento from faturamento where id_periodo_faturamento = ".$perfat->id_periodo_faturamento." and status_demonstrativo = 0 order by id_cliente limit 1";
                $res = \DB::connection('mysql_fat')->getPdo()->query($sql);
                $rs = $res->fetch(\PDO::FETCH_BOTH);
            }
            $perfat->status="Gerando arquivo completo... (aguarde)";
            $perfat->progresso = 100;
            $perfat->save();
            \DB::commit();
            $this->criarDemonstrativoCompleto($perfat, $id_usuario);
            $perfat->finalizado = 1;
            $perfat->status="Faturamento processado com sucesso!";
            $perfat->progresso = 100;
            $perfat->save();
        } catch (Exception $ex) {
            $perfat->finalizado = -1;
            $perfat->status="Não foi possível finalizar o processamento: ".$ex->getMessage();
            $perfat->progresso = 100;
            $perfat->save();
        }

    }
}