<?php

class LogDocumento extends Modelo {
	protected $table = 'log_documento';
	protected $primaryKey = 'id';
	protected $fillable = array('id_documento','id_usuario','evento','descricao');

	public static $rules = array();


    public function usuario(){
        return $this->belongsTo('Usuario', 'id_usuario');
    }


    public function scopeHistorico($query, $id_documento){
        return $query->where('id_documento', '=', $id_documento)
                    ->with('Usuario')
                    ->orderBy('created_at', 'desc')
                    ->get();
    }
}
