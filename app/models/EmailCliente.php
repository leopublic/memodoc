<?php
class EmailCliente extends Email {
	protected $table = 'emailcliente';
	protected $primaryKey = 'id_emailcliente';

	public function cliente(){
		return $this->belongsTo('Cliente', 'id_cliente');
	}

    public function tipoemail() {
        return $this->belongsTo('TipoEmail','id_tipoemail');
    }

    public function getDescricaoTipoEmailAttribute(){
    	$tipo = TipoEmail::find($this->attributes['id_tipoemail']);
    	if (count($tipo) > 0){
    		return $tipo->descricao;
    	} else {
    		return '--';
    	}
    }
}