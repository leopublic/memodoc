<?php
class EnderecoEntrega extends Modelo {
	protected $table = 'endeentrega';
	protected $primaryKey = 'id_endeentrega';

	public function cliente(){
		return $this->belongsTo('Cliente', 'id_cliente');
	}

    public function completo(){
        return $this->logradouro.' '.$this->endereco.' '.$this->numero.' '.$this->complemento.' '.$this->bairro;
    }
}