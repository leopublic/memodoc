<?php
/**
 * @property int $id_arquivo_retorno
 * @property date $data_arquivo
 * @property string $nome_arquivo
 * @property int $qtd_registros
 * @property datetime $data_importacao
 * @property int $id_usuario
 * @property int $id_status_importacao
 */
class ArquivoRetorno extends Modelo
{
    protected $table = 'arquivo_retorno';
    protected $primaryKey = 'id_arquivo_retorno';

    public static function raiz_caminho(){
    	$raiz = app_path();
    	$raiz = $raiz.DIRECTORY_SEPARATOR.'arquivos'.DIRECTORY_SEPARATOR.'pagamentos'.DIRECTORY_SEPARATOR;
    	return $raiz;
    }

    public function quebra_digitos($id){
    	$digitos = array();
    	$digitos[0] = floor($id / 1000);
    	$id = $id % 1000;
    	$digitos[1] = floor($id / 100);
    	$id = $id % 100;
    	return $digitos;
    }


    /**
     * Retorna o caminho do arquivo
     * @return [type] [description]
     */
	public function caminho(){
    	$digitos = $this->quebra_digitos($this->id_arquivo_retorno);
		$caminho = self::raiz_caminho().$digitos[0].DIRECTORY_SEPARATOR.$digitos[1];
		if (!is_dir($caminho)){
			mkdir($caminho, 0775, true);
		}
        return $caminho;
	}
    public function caminhoComNome(){
        return $this->caminho().DIRECTORY_SEPARATOR.$this->id_arquivo_retorno;
    }

    public function getQtdRegistrosAttribute(){
        return \Pagamento::where('id_arquivo_retorno', '=', $this->id_arquivo_retorno)->count();
    }
    /**
     * [scopeDesde description]
     * @param  date $inicio Data no formato dd/mm/yyyy
     * @return [type]         [description]
     */
    public function scopeDesde($query, $inicio){
        try{
            $inicio = \Carbon::createFromFormat('d/m/Y', $inicio);
            return $query->where('data_importacao', '>=', $inicio->format("Y-m-d"));
        } catch(Exception $e){
            return $query;
        }
    }
    /**
     * [scopeDesde description]
     * @param  date $inicio Data no formato dd/mm/yyyy
     * @return [type]         [description]
     */
    public function scopeAte($query, $inicio){
        try{
            $inicio = \Carbon::createFromFormat('d/m/Y', $inicio);
            return $query->where('data_importacao', '<=', $inicio->format("Y-m-d"));
        } catch(Exception $e){
            return $query;
        }
    }


}