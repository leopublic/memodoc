<?php

/**
 * @property int $id_os_chave
 * @property int $id_caixa
 * @property int $id_cliente
 * @property int $fl_primeira_entrada
 */
class RegistroSicoobRetorno extends Eloquent {

    protected $table = 'reg_sicoob_retorno';
    protected $primaryKey = 'id_reg_sicoob_retorno';
    protected $guarded = array();
    public static $rules = array();

    public function razaosocial(){
        if($this->id_cliente > 0){
            $cliente = Cliente::find($this->id_cliente);
            return $cliente->razaosocial;
        } else {
            return '(não encontrado)';
        }
    }

    public function getMovimentoExtensoAttribute(){
        switch($this->movimento){
            case '02':
                return "Confirmação Entrada Título";
                break;
            case '04':
                return "Transferência de carteira/Entrada";
                break;
            case '05':
                return "Liquidação Sem Registro";
                break;
            case '06':
                return "Liquidação Normal";
                break;
            case '09':
                return "Baixa de Titulo";
                break;
            case '10':
                return "Baixa Solicitada";
                break;
            case '11':
                return "Títulos em abarto";
                break;
            case '14':
                return "Alteração de Vencimento";
                break;
            case '15':
                return "Liquidação em Cartório";
                break;
            case '23':
                return "Encaminhado a Protesto";
                break;
            case '27':
                return "Confirmação Alteração Dados";
                break;
            case '48':
                return "Confirmação de instrução de transferência de carteira/modalidade de cobrança";
                break;
            }
        
    }

}
