<?php
class EmailContato extends Email {
	protected $table = 'emailcontato';
	protected $primaryKey = 'id_emailcontato';

	public function cliente(){
		return $this->belongsTo('Cliente', 'id_cliente');
	}

	public function contato(){
		return $this->belongsTo('Contato', 'id_contato');
	}
}