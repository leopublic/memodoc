<?php
/**
 * Classe para armazenar os dados de uma movimentação de caixas
 * @property int $id_movimentacao
 * @property int $id_usuario
 * @property boolean $fl_cancelar_endereco
 * @property int $id_tipodocumento
 * @property datetime $created_at
 * @property datetime $update_at
 */
class Movimentacao extends Modelo {
    protected $table = 'movimentacao';
    protected $primaryKey = 'id_movimentacao';
    protected $guarded = array();
    
    public function getQtdCaixasAttribute($valor){
        return \MovimentacaoCaixa::where('id_movimentacao', '=', $this->id_movimentacao)->count();
    }

    public function caixas(){
        return $this->hasMany('MovimentacaoCaixa', 'id_movimentacao')->orderBy('id_caixa');
    }
    
    public function documentos(){
        $id = $this->id_movimentacao;
        return \Documento::whereIn('id_caixa', function($query) use($id) {
                        $query->select(array('id_caixa'))
                              ->from('movimentacao_caixa')
                              ->where('id_movimentacao','=', $id);
                    })
                    ->get();
    }
    
    public function enderecos(){
        $id = $this->id_movimentacao;
        return \Endereco::whereIn('id_caixa', function($query) use($id){
                        $query->select(array('id_caixa'))
                              ->from('movimentacao_caixa')
                              ->where('id_movimentacao','=', $id);
                    })
                    ->get();
    }
    
    public function usuario(){
        return $this->belongsTo('Usuario', 'id_usuario');
    }
}
