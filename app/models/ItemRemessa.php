<?php

/**
 * @property  string
 */
class ItemRemessa extends Modelo {

    protected $table = 'item_remessa';
    protected $primaryKey = 'id_item_remessa';

    public function getTituloMontadoAttribute(){
        return '';
    }


    public function titulo_montado(){
        return $this->campo2;
    }
    public function conteudo_montado(){
        return $this->campo1."<br/>".$this->codigo;
    }
}
