<?php

/**
 * @property int $id_expurgo
 * @property int $id_cliente
 * @property int $id_caixa
 * @property int $id_caixapadrao
 * @property date $data_solicitacao
 * @property date $data_checkout
 * @property decimal $valor_expurgo
 * @property boolean $cortesia
 * @property int $id_usuario_cadastro
 * @property int $id_usuario_expurgo
 */
class Expurgo extends Modelo {

    protected $table = 'expurgo';
    protected $primaryKey = 'id_expurgo';

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function getDataSolicitacaoAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDataSolicitacaoAttribute($data) {
        $this->attributes['data_solicitacao'] = $this->dtViewParaModel($data);
    }


    public function getDataCheckoutFmtAttribute($data) {
        if ($this->attributes['data_checkout'] == ''){
            return '--';
        } else {
            return $this->dtHrModelParaView($this->attributes['data_checkout']);            
        }
    }

    public function pai(){
        return $this->belongsTo('ExpurgoPai', 'id_expurgo_pai', 'id_expurgo_pai');
    }

    public function endereco(){
        return $this->belongsTo('Endereco', 'id_caixa');
    }

    public function endereco_completo(){
        if (count($this->endereco) > 0){
            return $this->endereco->endereco_completo;
        } else {
            return '(caixa expurgada)';
        }
    }
    
    public function scopeDoCliente($query, $id_cliente){
        return $query->where('id_cliente', '=', $id_cliente)
                    ->orderBy('data_solicitacao', 'desc')
                    ->orderBy('id_caixapadrao', 'desc');
    }

}
