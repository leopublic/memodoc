<?php
/**
 * Classe para armazenar os dados de um transbordo
 * @property int $id_transbordocaixa
 * @property int $id_transbordo
 * @property int $id_caixapadrao
 * @property int $id_caixa
 * @property datetime $created_at
 * @property datetime $update_at
 */
class TransbordoCaixa extends Modelo {
    protected $table = 'transbordo_caixa';
    protected $primaryKey = 'id_transbordocaixa';
    protected $guarded = array();

    public function enderecoantes(){
        return $this->belongsTo('Endereco', 'id_caixa_antes');
    }

    public function enderecodepois(){
        return $this->belongsTo('Endereco', 'id_caixa_depois');
    }
}
