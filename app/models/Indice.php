<?php

/**
 */
class Indice extends Modelo {

    protected $table = 'indice';
    protected $primaryKey = 'id_indice';
    protected $fillable = array('*');

    public function clientes_reajustando_em($mes){
    	return \Cliente::where('id_indice', '=', $this->id_indice)
    	->ativos()
    	->whereRaw("month(iniciocontrato) = ".$mes)
    	->count();
    }
    
    public function clientes_reajustados($mes, $ano){
    	return \Cliente::where('id_indice', '=', $this->id_indice)
    	->ativos()
    	->whereRaw("month(iniciocontrato) = ".$mes)
    	->whereRaw("month(dt_ultimo_reajuste) = ".$mes." and year(dt_ultimo_reajuste) = ".$ano)
    	->count();
    }
    
    public function qtd_clientes_restantes($mes, $ano){
    	return $this->clientes_reajustando_em($mes) - $this->clientes_reajustados($mes, $ano);
    }
    
}
