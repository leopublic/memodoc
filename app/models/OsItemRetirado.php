<?php
/**
 */
class OsItemRetirado extends Modelo {

    protected $table = 'os_item_retirado';
    protected $primaryKey = 'id_os_item_retirado';

    public function documento() {
        return $this->belongsTo('Documento', 'id_documento');
    }

    public function scopeDaOs($query, \Os $os){
        return $query->where('id_os_chave', '=', $os->id_os_chave)
                     ->orderBy('id_caixapadrao')
                ;
    }

    public function scopePendentesdaos($query, \Os $os){
    	return $query->where('id_os_chave', '=', $os->id_os_chave)->whereNull('dt_checkin');
    }
    
    public function os(){
        return $this->belongsTo('Os', 'id_os_chave');
    }
}
