<?php

/**
 * @property  string
 */
class ReservaDesmembrada extends Modelo {

    protected $table = 'reservadesmembrada';
    protected $primaryKey = 'id_reservadesmembrada';

    public function endereco() {
        return $this->belongsTo('Endereco', 'id_caixa');
    }

    public function reserva() {
        return $this->belongsTo('Reserva', 'id_reserva');
    }

    public function scopeDocliente($query, $id_cliente) {
        return $query->whereIn('id_reserva', function($query) use($id_cliente) {
                    $query->select('id_reserva')
                            ->from('reserva')
                            ->where('id_cliente', '=', $id_cliente);
                });
    }

    public function scopeIdCaixaPadraointervalo($query, $id_caixapadrao, $id_caixapadrao_fim = '') {
        if ($id_caixapadrao <> '') {
            $query = $query->where('id_caixapadrao', '>=', $id_caixapadrao);
        }
        if ($id_caixapadrao_fim <> '') {
            $query = $query->where('id_caixapadrao', '<=', $id_caixapadrao_fim);
        }
        return $query;
    }

    public function scopeIdCaixaPadraoin($query, $id_caixapadrao) {
        if ($id_caixapadrao <> '') {
            $query = $query->whereIn('id_caixapadrao', explode(",", $id_caixapadrao));
        }
        return $query;
    }
}
