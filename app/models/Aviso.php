<?php
/**
 * @property int $id_aviso Chave
 * @property int $id_cliente Cliente
 * @property datetime $dt_inicio Data de início da validade do aviso
 * @property datetime $dt_fim Data de fim da validade do aviso
 * @property boolean $fl_padrao Indica se esse é o aviso padrão. Só um aviso pode ser o aviso padrão
 * @property boolean $fl_ativo Indica se o aviso está ativo ou não
 * @property text $texto
 * @property text $texto_popup
 */
class Aviso extends Modelo {

	protected $table = 'aviso';
	protected $primaryKey = 'id_aviso';
	protected $appends = array('dt_inicio_fmt', 'dt_fim_fmt');
	/**
	 * Retorna o aviso vigente.
	 * @param string $id_cliente
	 */
	public static function homeVigente(){

	}

	public function sejaPadrao(){
		$this->fl_padrao = true;
		$this->save();
	}

	public function getDtInicioFmtAttribute($data) {
		return $this->dtHrModelParaView($this->attributes['dt_inicio']);
	}

	public function setDtInicioFmtAttribute($data) {
		$this->attributes['dt_inicio'] = $this->dtHrViewParaModel($data);
	}

	public function getDtFimFmtAttribute($data) {
		return $this->dtHrModelParaView($this->attributes['dt_fim']);
	}

	public function setDtFimFmtAttribute($data) {
		$this->attributes['dt_fim'] = $this->dtHrViewParaModel($data);
	}

}
