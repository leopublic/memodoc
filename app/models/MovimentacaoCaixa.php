<?php
/**
 * Classe para armazenar os dados de um transbordo
 * @property int $id_movimentacaocaixa
 * @property int $id_movimentacao
 * @property int $id_caixa
 * @property string $endereco_antes
 * @property string $endereco_depois
 * @property datetime $created_at
 * @property datetime $update_at
 */
class MovimentacaoCaixa extends Modelo {
    protected $table = 'movimentacao_caixa';
    protected $primaryKey = 'id_movimentacao_caixa';
    protected $guarded = array();

    public function endereco(){
        return $this->belongsTo('Endereco', 'id_caixa');
    }
}
