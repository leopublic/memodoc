<?php

/**
 * @property int $id_os_chave
 * @property int $id_caixa
 * @property int $id_cliente
 * @property int $fl_primeira_entrada
 */
class ConsultaBoletos extends Eloquent {

    protected $table = 'consulta_boletos';
    protected $primaryKey = 'id_consulta_boletos';
    protected $guarded = array();
    public static $rules = array();


    public function raiz(){

        $caminho = storage_path().'/consulta_boletos/';
        if(!is_dir($caminho)){
            mkdir($caminho, 0775, true);
        }
        return $caminho;
    }

    public function caminho(){
        return $this->raiz();
    }
    

    public function caminho_com_arquivo(){
        return $this->raiz().$this->id_consulta_boletos;
    }    

}
