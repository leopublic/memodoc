<?php

class Modelo extends Eloquent implements \InterfaceImportavel {

    public $errors;

    public function get_nomeTabelaFirebird() {
        return strtoupper('TBL_' . $this->table);
    }

    public function CarregaDoRecordsetFirebird($pRs) {
        foreach ($pRs as $coluna => $valor) {
            $coluna = strtolower($coluna);
            $valor = utf8_encode($valor);
            if ($valor == '') {
                $valor = null;
            }
            $this->$coluna = $valor;
        }
    }

    function formatDate($name, $format = 'd/m/Y', $valorVazio = '') {
        if ($name != '') {
            if ($this->$name != '' && $this->$name != '00/00/0000' && $this->$name != '0000-00-00'){
                return date($format, strtotime($this->$name));
            } else {
                return $valorVazio;
            }
        } else {
            return $valorVazio;
        }
    }

    function formatDateTime($name, $format = 'd/m/Y H:i:s', $valorVazio = '') {
        if ($name != '') {
            if ($this->$name != '' && substr ($this->$name, 0,10) != '00/00/0000' && substr($this->$name, 0, 10) != '0000-00-00'){
                return date($format, strtotime($this->$name));
            } else {
                return $valorVazio;
            }
        } else {
            return $valorVazio;
        }
    }

    public function dtModelParaView($data) {
        if ($data != '' && $data != '00/00/0000' && $data != '0000-00-00') {
            return Carbon::createFromFormat('Y-m-d', $data)->format('d/m/Y');
        } else {
            return null;
        }
    }

    public function dtHrModelParaView($data) {
        if ($data != '') {
            return Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('d/m/Y H:i:s');
        } else {
            return $data;
        }
    }

    public function dtViewParaModel($data) {
        if ($data != '' && $data != '00/00/0000' && $data != '0000-00-00') {
            return Carbon::createFromFormat('d/m/Y', substr($data, 0, 10))->format('Y-m-d');
        } else {
            return null;
        }
    }

    public function dtHrViewParaModel($data) {
        if ($data != '') {
            return Carbon::createFromFormat('d/m/Y H:i:s', $data)->format('Y-m-d H:i:s');
        } else {
            return $data;
        }
    }

    public function scopeOrdemPadrao($query){
        if ($this->campoOrdemPadrao() != ''){
            return $query->orderBy($this->campoOrdemPadrao());
        } else {
            return $query;
        }
    }
    /**
     * Valida o model, a partir das regras e mensagens passadas
     * @param  [type] $regras    [description]
     * @param  [type] $mensagens [description]
     * @return [type]            [description]
     */
    public function valido($regras, $mensagens) {
        $validador = Validator::make($this->attributes, $regras, $mensagens);
        if ($validador->passes()) {
            return true;
        } else {
            $this->errors = $validador->messages();
            return false;
        }
    }

    
    function simpleList($manyModel, $pk, $selects = array()){
        $oneModel = $this->getTable();
        $one = $this->findOrFail($pk);
        $many = $one->$manyModel;
//        $many = $one->hasMany($manyModel, $this->getKeyName())->get();

        $attr['one'] = $one;
        $attr['many'] = $many;
        $attr['model'] = $many;
        $attr['route_add'] = $oneModel . '/auto/' . $manyModel . '/create/' . $pk;
        $attr['route_edit'] = $oneModel . '/auto/' . $manyModel . '/edit';
        $attr['route_delete'] = $oneModel . '/auto/' . $manyModel . '/delete';
        $attr['pk'] = $pk;
        return $attr;        
    }
    
    
    /**
     * Simple method based in REST
     *
     * Example list:
     * customer/auto/[contacts/list]
     *
     * Example create:
     * customer/auto/[contacts/create]
     *
     * Example edit:
     * customer/auto/[contacts/edit/522]
     *
     * Example delete:
     * customer/auto/[contacts/delete/522]
     *
     * @param type $action =  List |  Edit  |  Create  | Delete
     * @param type $manyModel = customer -> contacts (1:N) relations
     * @param type $pk  = customer_id
     * @return array
     */
    function simple($action, $manyModel, $pk, $selects = array()) {

        $oneModel = $this->getTable();
        $attr = array();

        // List
        
        
        if ($action == 'list') {

            $one = $this->findOrFail($pk);
            $many = $one->hasMany($manyModel, $this->getKeyName())->get();

            $attr['one'] = $one;
            $attr['many'] = $many;
            $attr['model'] = $many;
            $attr['route_add'] = $oneModel . '/auto/' . $manyModel . '/create/' . $pk;
            $attr['route_edit'] = $oneModel . '/auto/' . $manyModel . '/edit';
            $attr['route_delete'] = $oneModel . '/auto/' . $manyModel . '/delete';
            $attr['pk'] = $pk;
        }

        // Edit
        if ($action == 'edit') {

            $oneModel = $this;
            $objMany = new $manyModel;
            $many = $objMany->findOrFail($pk);
            $one = $oneModel->findOrFail($many[$oneModel->getKeyName()]);
            $attr['one'] = $one;
            $attr['many'] = $many;
            $attr['select'] = array();
            $id = $one->getKey();

            //pr($id);

            if (isset($selects[$manyModel]) and !empty($selects[$manyModel])) {
                foreach ($selects[$manyModel] as $selectmodelname => $selectmodelwhereraw) {
                    //pr($selectmodelwhereraw);
                    $find = null;
                    if (is_numeric($selectmodelname)) {
                        $selectmodelname = $selectmodelwhereraw;
                        $find = $selectmodelname::ordemPadrao();
                    } else {
                        $find = $selectmodelname::whereRaw($selectmodelwhereraw)->ordemPadrao()->get();
                    }
                    $attr['select'][$selectmodelname] = $find;
                    //pr($pk);
                    //pr(queryLog());
                }
                //pr($attr['select']);
            }
            if (!empty($_POST)) {
                foreach ($attr['many']->getOriginal() as $col => $value) {
                    if (isset($_POST[$col])) {
                        $attr['many']->$col = $_POST[$col];
                    }
                }
                $attr['many']->save();
            }
            return $attr;
        }

        // Create
        if ($action == 'create') {
            $objOneModel = new $oneModel;
            $data = $objOneModel->findOrFail($pk);
            if (!empty($data)) {
                $objManyModel = new $manyModel;

                if (!empty($_POST)) {
                    $table = \DB::getDoctrineSchemaManager()->listTableDetails($objManyModel->getTable());
                    $columns = $table->getColumns();
                    $_POST[$data->getKeyName()] = $data->getKey(); // relation one To many
                    foreach ($columns as $col => $value) {
                        if (isset($_POST[$col])) {
                            $objManyModel->$col = $_POST[$col];
                        }
                    }
                    $objManyModel->save();

                    $attr['many'] = $objManyModel;
                    $attr['one'] = $data;
                } else {
                    if (isset($selects[$manyModel]) and !empty($selects[$manyModel])) {
                        foreach ($selects[$manyModel] as $selectmodelname => $selectmodelwhereraw) {
                            $find = null;
                            if (is_numeric($selectmodelname)) {
                                $selectmodelname = $selectmodelwhereraw;
                                $find = $selectmodelname::all();
                            } else {
                                $find = $selectmodelname::whereRaw($selectmodelwhereraw)->get();
                            }
                            $attr['select'][$selectmodelname] = $find;
                            //pr($pk);
                            //pr(queryLog());
                        }
                    }
                }
                //pr(queryLog());
            }
        }

        // Delete
        if ($action == 'delete') {
            $objOneModel = new $oneModel;
            $objManyModel = new $manyModel;
            $many = $objManyModel->findOrFail($pk);
            $one = $objOneModel->findOrFail($many[$objOneModel->getKeyName()]);
            $attr['many'] = $many;
            $attr['one'] = $one;
            $many->delete();
        }

        return $attr;
    }

    function FormSelect($model, $name = 'nome') {
        $data = array();

        if ($model->count() > 0) {
            $data[''] = 'Selecione';
            foreach ($model as $fields) {
                foreach ($fields->getAttributes() as $fieldname => $field) {
                    if ($fieldname == $name) {
                        $data[$fields->getKey()] = $field;
                    }
                }
            }
        }
        return $data;
    }
    /**
     * Permite indicar se a instancia pode ser excluida ou não
     * O default é true
     * @return boolean Indicação se pode excluir ou não
     */
    public function pode_excluir(){
        return true;
    }
    /**
     * Permite indicar se a instancia pode ser alterada ou não
     * O default é true
     * @return boolean Indicação se pode alterar ou não
     */
    public function pode_alterar(){
        return true;
    }

}
