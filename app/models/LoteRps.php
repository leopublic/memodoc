<?php

/**
 * @property  string
 */
class LoteRps extends Modelo {

    protected $table = 'lote_rps';
    protected $primaryKey = 'id_lote_rps';
    protected $fillable = array('id_lote_rps', 'lrps_dt_geracao_fmt', 'lrps_dt_emissao_fmt', 'id_usuario', 'id_periodo_faturamento');
    
    public function getLrpsDtGeracaoFmtAttribute($data) {
        return $this->dtHrModelParaView($this->attributes['lrps_dt_geracao']);
    }

    public function setLrpsDtGeracaoFmtAttribute($data) {
        $this->attributes['lrps_dt_geracao'] = $this->dtHrViewParaModel($data);
    }
    
    
    public function getLrpsDtEmissaoFmtAttribute($data) {
        return $this->dtModelParaView($this->attributes['lrps_dt_emissao']);
    }

    public function setLrpsDtEmissaoFmtAttribute($data) {
        $this->attributes['lrps_dt_emissao'] = $this->dtViewParaModel($data);
    }
    
    public function getLrpsStatusExtensoAttribute($data) {
        switch($this->lrps_status){
            case 0: 
                return "não gerado";
                break;
            case 1: 
                return "enviado";
                break;
            case 2: 
                return "parcialmente numerado";
                break;
            case 3: 
                return "totalmente numerado";
                break;
            default: 
                return "(desconhecido - ".$this->lrps_status.")";
                break;
        }
    }

    public function getDescricaoPerfatAttribute(){
        if($this->attributes['id_periodo_faturamento'] > 0){
            $perfat = PeriodoFaturamento::find($this->attributes['id_periodo_faturamento']);
            return $perfat->mes.'/'.$perfat->ano;
        } else {
            return '--';
        }
    }

    public function getAnoAttribute(){
        return substr($this->lrps_dt_geracao, 0,4);
    }

    public function getQtdNotasAttribute(){
        return NotaFiscal::where('id_lote_rps', '=', $this->id_lote_rps)->count();
    }

    public function valores_em_array(){
        $ret = [];
        $ret['rps_serie'] = $this->ano;
        $ret['rps_numero'] = $this->id_lote_rps;
        $ret['rps_dt_geracao'] = $this->lrps_dt_geracao;
        $ret['dt_inicio'] = $this->lrps_dt_geracao;
        $ret['dt_fim'] = $this->lrps_dt_geracao;
        $ret['rps_dt_emissao'] = $this->lrps_dt_emissao;
        $ret['srv_iss_dt_competencia'] = $this->lrps_dt_emissao;
        return $ret;        
    }
}