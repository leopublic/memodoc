<?php

/**
 * @property int $id_pagador
 * @property int $id_cliente
 * @property varchar $cnpj
 * @property varchar $razaosocial
 * @property varchar $logradouro
 * @property varchar $numero
 * @property varchar $complemento
 * @property varchar $bairro
 * @property varchar $cidade
 * @property varchar $estado
 * @property varchar $cep
 * @property varchar $contato
 * @property varchar $insc_estadual
 * @property varchar $insc_municipal
 * @property text $observacao
 * @property timestamp $created_at
 * @property timestamp $updated_at
 */
class Pagador extends Modelo {

    protected $table = 'pagador';
    protected $primaryKey = 'id_pagador';

    public function cliente(){
        return $this->belongsTo('Cliente', 'id_cliente', 'id_cliente');
    }

    public function getCnpjFormatadoAttribute(){
        if(strlen(trim($this->attributes['cnpj'])) > 11){
            return substr($this->attributes['cnpj'], 0,2).'.'.substr($this->attributes['cnpj'], 2,3).'.'.substr($this->attributes['cnpj'], 5,3).'/'.substr($this->attributes['cnpj'], 8,4).'-'.substr($this->attributes['cnpj'], 12,2);
        } else {
            return substr($this->attributes['cnpj'], 0,3).'.'.substr($this->attributes['cnpj'], 3,3).'.'.substr($this->attributes['cnpj'], 6,3).'-'.substr($this->attributes['cnpj'], 9,2);
        }
    }


}
