<?php
class Solicitante extends Modelo {
	protected $table = 'solicitante';
	protected $primaryKey = 'id_solicitante';
	protected $fillable = array('id_cliente','id_departamento','id_setor','id_centrocusto','nome');


	public function emailsolicitante(){
		return $this->hasMany('emailsolicitante', 'id_cliente');
	}

	public function telefonesolicitante(){
		return $this->hasMany('telefonesolicitante', 'id_cliente');
	}

	public function cliente(){
		return $this->belongsTo('Cliente', 'id_cliente');
	}

	public function centrodecusto(){
		return $this->belongsTo('CentroDeCusto', 'id_centrodecusto');
	}

	public function depcliente(){
		return $this->belongsTo('DepCliente', 'id_departamento');
	}

	public function setcliente(){
		return $this->belongsTo('SetCliente', 'id_setor');
	}
}