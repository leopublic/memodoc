<?php

/**
 * @property  string
 */
class Remessa extends Modelo {

    protected $table = 'remessa';
    protected $primaryKey = 'id_remessa';
    protected $fillable = array('id_remessa', 'id_cliente');

    public function getNomeAnexoAttribute($valor){
        $anexo = Anexo::where('id_remessa', '=', $this->id_remessa)->first();
        if (count($anexo) > 0){
            return $anexo->nome_original;
        } else {
            return '(não informado)';
        }

    }

    public function getQtdRegistrosAttribute(){
        return ItemRemessa::where('id_remessa', '=', $this->id_remessa)->count();
    }
}
