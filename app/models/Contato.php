<?php
class Contato extends Modelo {
	protected $table = 'contato';
	protected $primaryKey = 'id_contato';

	public function cliente(){
		return $this->belongsTo('Cliente', 'id_cliente');
	}

	public function emailcontato(){
		return $this->hasMany('EmailContato', 'id_cliente');
	}

	public function telefonecontato(){
		return $this->hasMany('TelefoneContato', 'id_cliente');
	}
}