<?php
class TipoEmail extends Modelo {
    protected $table = 'tipoemail';
    protected $primaryKey = 'id_tipoemail';
    protected $fillable = array('descricao');

    const NOTIFICACAO_OS = '6';

    public function pode_excluir(){
        if ($this->fl_sistema == '1'){
            return false;
        } else {
            return self::getPermiteDeletar($this->id_tipoemail);
        }
    }

    public function pode_alterar(){
        if ($this->fl_sistema == '1'){
            return false;
        } else {
            return true;
        }
    }

    /**
     * verifica se permite deletar
     */
    static function getPermiteDeletar($id){
        
       $count = self::find($id)->first()->hasMany('EmailCliente','id_tipoemail')->count();
       $count += self::find($id)->first()->hasMany('EmailContato','id_tipoemail')->count();
       $count += self::find($id)->first()->hasMany('EmailSolicitante','id_tipoemail')->count();
       return ($count > 0) ? false : true;
    }
}