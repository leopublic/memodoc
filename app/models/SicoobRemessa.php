<?php
use Carbon;
/**
 * @property int $id_os_chave
 * @property int $id_caixa
 * @property int $id_cliente
 * @property int $fl_primeira_entrada
 */
class SicoobRemessa extends Eloquent {

    protected $table = 'sicoob_remessa';
    protected $primaryKey = 'id_sicoob_remessa';
    protected $guarded = array();
    public static $rules = array();


    public function raiz(){

        $caminho = storage_path().'/sicoob/remessa/';
        if(!is_dir($caminho)){
            mkdir($caminho, 0775, true);
        }
        return $caminho;
    }

    public function caminho(){
        return $this->raiz();
    }
    

    public function caminho_com_arquivo(){
        return $this->raiz().$this->id_sicoob_remessa;
    }    

    public function valores_em_array(){
        $ret = [];
        
        $ret['data_carga'] = Carbon::createFromFormat('Y-m-d',$this->data_carga)->format('dmy');
        $ret['id_sicoob_remessa'] = $this->id_sicoob_remessa;
        
        return $ret;
    }
}
