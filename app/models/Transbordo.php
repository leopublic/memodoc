<?php
/**
 * Classe para armazenar os dados de um transbordo
 * @property int $id_transbordo
 * @property int $id_usuario_cadastro
 * @property int $id_cliente_antes
 * @property int $id_cliente_depois
 * @property int $id_tipodocumento
 * @property int $id_reserva
 * @property int $id_expurgo_pai
 * @property datetime $created_at
 * @property datetime $update_at
 */
class Transbordo extends Modelo {
    protected $table = 'transbordo';
    protected $primaryKey = 'id_transbordo';
    protected $guarded = array();

    public function clienteantes(){
        return $this->belongsTo('Cliente', 'id_cliente_antes');
    }

    public function clientedepois(){
        return $this->belongsTo('Cliente', 'id_cliente_depois');
    }
    
    public function getQtdCaixasAttribute($valor){
        return \TransbordoCaixa::where('id_transbordo', '=', $this->id_transbordo)->count();
    }

    public function caixas(){
        return $this->hasMany('TransbordoCaixa', 'id_transbordocaixa')->orderBy('id_caixapadrao_ini');
    }
    
    public function documentos(){
        return \Documento::where('id_cliente', '=', $this->id_cliente_antes)
                    ->whereIn('id_caixapadrao', function($query){
                        $query->select(array('id_caixapadrao_antes'))
                              ->from('transbordo_caixa')
                              ->where('id_transbordo','=', $this->id_transbordo );
                    })
                    ->get();
    }
    
    public function reserva(){
        return $this->belongsTo('Reserva', 'id_reserva', 'id_reserva');
    }

    public function usuario(){
        return $this->belongsTo('Usuario', 'id_usuario_cadastro');        
    }
    public function getNumOsAttribute(){
        if ($this->id_os_chave > 0){
            $os = \Os::find($this->id_os_chave);
            return substr('000000'.$os->id_os, -6);
        } else {
            return '--';
        }
    }
}
