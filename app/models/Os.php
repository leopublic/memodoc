<?php

/**
 * @property int $id_os_chave
 * @property int $id_os
 * @property int $id_cliente
 * @property int $id_departamento
 * @property int $id_endeentrega
 * @property int $num_os
 * @property string $responsavel
 * @property mixed $solicitado_em
 * @property mixed $entregar_em
 * @property int $entrega_pela
 * @property int $tipodeemprestimo
 * @property int $cobrafrete
 * @property int $observacao
 * @property bool $urgente Indica se a OS é urgente
 * @property float $valorfrete
 * @property float $valorfreteprevisto
 * @property float $valor
 * @property float $valormovimento
 * @property float $valorcartonagem
 * @property bool $cancelado
 * @property int $tipo_os
 * @property int $tipodeemprestimo
 * @property bool $naoecartonagemcortesia
 * @property int $id_status_os
 * @property int $id_reserva
 * @property int $id_faturamento
 * @property boolean $fl_importada
 * @property float $fl_frete_manual
 * @property float $val_frete_manual
 * @property int $id_usuario_cadastro
 * @property int $id_usuario_alteracao
 *
 * Campos preenchidos pelo cliente na abertura da OS
 * =====================================================
 * @property int $qtd_caixas_retiradas_cliente  Quantidade de caixas que pretende retirar (estimativa)
 * @property int $qtd_cartonagens_cliente       Quantidade de cartonagens adicionais.
 *
 * Campo preenchidos pelo operacional e que são utilizados no cálculo do faturamento
 * =================================================================================
 * @property int $qtdenovascaixas               Quantidade de endereços reservados com cartonagem
 * @property int $apanhanovascaixas             (calculado) Das caixas retiradas, quantas eram novas (não cobrado)
 * @property int $apanhacaixasemprestadas       (calculado) Das caixas retiradas, quantas eram retorno
 */
class Os extends Modelo {

    protected $table = 'os';
    protected $primaryKey = 'id_os_chave';
    protected $fillable = array ('id_os',
        'id_cliente',
        'id_departamento',
        'id_endeentrega',
        'num_os',
        'responsavel',
        'solicitado_em',
        'solicitado_em_data',
        'solicitado_em_hora',
        'entregar_em',
        'entrega_pela',
        'tipodeemprestimo',
        'qtdenovascaixas',
        'apanhanovascaixas',
        'apanhacaixasemprestadas',
        'cobrafrete',
        'observacao',
        'urgente',
        'valormovimento',
        'valorcartonagem',
        'cancelado',
        'tipo_os',
        'naoecartonagemcortesia',
        'id_status_os',
        'qtd_caixas_novas_cliente',
        'qtd_caixas_retiradas_cliente',
        'qtd_cartonagens_cliente',
        'qtdeenderecosreservados',
        'fl_frete_manual',
        'val_frete_manual_fmt',
        'qtdenovasetiquetas'
        );

    public static function nova(){
        $os = new Os();

    }

    public function get_nomeTabelaFirebird() {
        return 'TBL_OS';
    }

    public function getEntregarEmAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setEntregarEmAttribute($data) {
        $this->attributes['entregar_em'] = $this->dtViewParaModel($data);
    }

    public function getSolicitadoEmAttribute($data) {
        return $this->dtHrModelParaView($data);
    }

    public function setSolicitadoEmAttribute($data) {
        $this->attributes['solicitado_em'] = $this->dtHrViewParaModel($data);
    }

    public function getSolicitadoEmDataAttribute($data) {
        return $this->dtModelParaView(substr($this->attributes['solicitado_em'], 0, 10));
    }

    public function setSolicitadoEmDataAttribute($data) {
        $valor = $data.' '.$this->solicitado_em_hora;
        $this->attributes['solicitado_em'] = $this->dtHrViewParaModel($valor);
    }

    public function getSolicitadoEmHoraAttribute($data) {
        if(isset($this->attributes['solicitado_em'])){
            return substr($this->attributes['solicitado_em'], 11);
        } else {
            return '00:00:00';
        }
    }

    public function setSolicitadoEmHoraAttribute($data) {
        $valor = $this->solicitado_em_data.' '.$data;
        $this->attributes['solicitado_em'] = $this->dtHrViewParaModel($valor);
    }

    public function getObservacaoAttribute() {
        if (isset($this->attributes['observacao'])) {
            return str_replace('<br/>', "\n", $this->attributes['observacao']);
        }
    }

    public function getNumeroFormatado() {
        $data = Carbon::createFromFormat('d/m/Y H:i:s', $this->solicitado_em);
        return $data->year . $data->month . substr('000000' . $this->id_os, -6);
    }

    function log_os() {
        return $this->hasMany('LogOs', 'id_os_chave');
    }

    public function reserva() {
        return $this->belongsTo('Reserva', 'id_reserva');
    }

    public function reservas() {
        return $this->hasMany('Reserva', 'id_os_chave');
    }

    public function getReservasNaoCanceladasAttribute() {
        return \Reserva::where('id_os_chave' , '=', $this->id_os_chave)->whereRaw('coalesce(cancelada,0) = 0 ')->count();
    }

    /**
     * getProximoNumero - Obtem o proximo numero da Os do cliente
     * @param type $id_cliente
     * @return type int
     */
    static public function getProximoNumero($id_cliente) {
        return intval(Os::where('id_cliente', '=', $id_cliente)->where('id_status_os', '>', 1)->max('id_os')) + 1;
    }

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function departamento() {
        return $this->belongsTo('DepCliente', 'id_departamento');
    }

    public function statusos() {
        return $this->belongsTo('StatusOs', 'id_status_os');
    }

    public function checkin() {
        return $this->hasMany('Checkin', 'id_os_chave');
    }

    public function checkout() {
        return $this->hasMany('Checkout', 'id_os_chave');
    }

    public function osdesmembradas(){
        return $this->hasMany('OsDesmembrada', 'id_os_chave')->orderBy('id_caixapadrao');
    }

    public function usuariocadastro(){
        return $this->belongsTo('Usuario', 'id_usuario_cadastro');
    }

    public function faturamento(){
        return $this->belongsTo('Faturamento', 'id_faturamento');
    }

    public function osdesmembradascomcheckout(){
        return $this->hasMany('OsDesmembrada', 'id_os_chave')
                        ->with('checkout')
                        ->orderBy('id_caixapadrao');
    }

    public function osdesmembradasPorEndereco(){
        return OsDesmembrada::where('id_os_chave', '=', $this->id_os_chave)
                        ->join('documento', 'documento.id_caixa', '=', 'osdesmembrada.id_caixa')
                        ->join('endereco', 'endereco.id_caixa', '=', 'documento.id_caixa')
                        ->select('documento.id_caixapadrao', 'endereco.id_galpao', 'endereco.id_rua', 'endereco.id_predio', 'endereco.id_andar', 'endereco.id_unidade')
                        ->distinct()
                        ->orderBy('id_galpao')
                        ->orderBy('id_rua')
                        ->orderBy('id_predio')
                        ->orderBy('id_andar')
                        ->orderBy('id_unidade')
                ;
    }


    public function scopeDeativos($query){
    	return $query->whereIn('id_cliente', function($query){
    		$query->select('id_cliente')->from('cliente')->where('fl_ativo', '=', '1');
    	});

    }

    public function scopeDeinativos($query){
    	return $query->whereIn('id_cliente', function($query){
    		$query->select('id_cliente')->from('cliente')->where('fl_ativo', '=', '0')->orWhereNull('fl_ativo');
    	});
    }

    public function scopeNovas($query) {
        return $query->where('id_status_os', '=', StatusOs::stNOVA);
    }

    public function totalcaixasparacheckin() {
        $total = Checkin::where('id_os_chave', '=', $this->id_os_chave)->count();
        if (intval($total) == 0) {
            $total = intval($this->apanhanovascaixas) + intval($this->apanhacaixasemprestadas);
        }
        return $total;
    }

    public function totalcheckin() {
        $res = Checkin::where('id_cliente', '=', $this->id_cliente)->where('id_os_chave', '=', $this->id_os_chave)->count();
        return $res;
    }

    public function totalcheckout() {
        $res = Checkout::where('id_cliente', '=', $this->id_cliente)->where('id_os', '=', $this->id_os)->count();
        return $res;
    }

    public function caixaconsulta() {
        $sql = "select distinct d.id_caixapadrao, e.id_galpao, e.id_rua, e.id_predio, e.id_andar, e.id_unidade
                from osdesmembrada osm
               join documento d on d.id_documento = osm.id_documento
               join endereco e on e.id_caixa = d.id_caixa
               where osm.id_os_chave = ".$this->id_os_chave."
               order by e.id_galpao, e.id_rua, e.id_predio, e.id_andar, e.id_unidade
                    ";
        $res = DB::select($sql);

//        $res = OsDesmembrada::where('id_cliente', '=', $this->id_cliente)
//                            ->where('id_os', '=', $this->id_os)
//                            ->with('Documento')
//                            ->with('Endereco')
//                            ->distinct(array('osdesmembrada.id_caixapadrao', 'endereco.id_galpao', 'endereco.id_rua', 'endereco.id_predio', 'endereco.id_andar', 'endereco.id_unidade'))
//                            ->get();
        //pr(queryLog());  // Debuger sql
        return $res;
    }

    public function referenciasConsulta(){
        return OsDesmembrada::where('id_os_chave', '=', $this->id_os_chave)
                ->leftJoin('documento', 'documento.id_caixa', '=', 'osdesmembrada.id_caixa')
                ->leftJoin('expurgo', 'expurgo.id_expurgo', '=', 'osdesmembrada.id_expurgo')
                ->leftJoin('usuario', 'usuario.id_usuario', '=', 'osdesmembrada.id_usuario_checkout')
                ->select(array('id_osdesmembrada', 'id_os_chave'
                            , DB::raw("coalesce(documento.id_cliente, expurgo.id_cliente) id_cliente")
                            , DB::raw('coalesce(osdesmembrada.id_caixa, documento.id_caixa, expurgo.id_caixa) id_caixa')
                            , DB::raw('coalesce(osdesmembrada.id_caixapadrao,documento.id_caixapadrao,expurgo.id_caixapadrao) id_caixapadrao')
                            , 'documento.emcasa'
                            , 'osdesmembrada.id_expurgo'
                            , DB::raw('expurgo.id_caixa id_caixa_cancelado')
                            , 'osdesmembrada.status'
                            , DB::raw("date_format(osdesmembrada.data_checkout, '%d/%m/%Y') data_checkout")
                            , DB::raw('usuario.nomeusuario as nomeusuario_checkout') )
                        )
                ->distinct()
                ->orderBy('documento.id_caixapadrao')
                ->get();
    }

    public function qtdcaixasconsulta(){
        return  count($this->referenciasConsulta());
    }

    public function getQtdCartonagemEnviada() {
        if ($this->naoecartonagemcortesia) {
            return $this->qtdecaixasnovas;
        } else {
            return 0;
        }
    }

    public function valortotal(){
        return $this->valorcartonagem + $this->valorfrete + $this->valormovimento + $this->val_mov_expurgo + $this->val_expurgo;
    }

    public function endeentrega(){
        return $this->belongsTo('EnderecoEntrega', 'id_endeentrega');
    }

    public function salvarCliente($encerrar = false){
    }

    public function atualizarTotaisRecebidos(){
        if ($this->fl_importada != 1){
            $this->apanhanovascaixas = Checkin::where('id_os_chave', '=', $this->id_os_chave)
                            ->where('fl_primeira_entrada', '=', 1)
                            ->select('id_caixa')->distinct()
                            ->count('id_caixa');
            $this->apanhacaixasemprestadas = Checkin::where('id_os_chave', '=', $this->id_os_chave)
                            ->whereNull('fl_primeira_entrada')
                            ->select('id_caixa')->distinct()
                            ->count('id_caixa');
        }
    }

    public function cadastradaPor(){
        return $this->belongsTo('Usuario', 'id_usuario_cadastro');
    }

    /**
     * Quantidade total de caixas movimentadas para efeito do cálculo do frete
     */
    public function qtdCaixasMovimentadas(){
        return intval($this->apanhacaixasemprestadas)
                + intval($this->apanhanovascaixas)
                + intval(count($this->osdesmembradas()->get()));
    }
    /**
     * Quantidade de caixas previstas
     */
    public function qtdCaixasMovimentadasPrevista(){
        return intval($this->qtd_caixas_retiradas_cliente)
                + intval($this->qtd_caixas_novas_cliente)
                + intval(count($this->osdesmembradas()->get()));
    }

    public function enderecoEntregaRelatorios(){
        if ($this->tipodeemprestimo == 0){
            return $this->endeentrega->completo();
        } elseif ($this->tipodeemprestimo == 1) {
            return 'CONSULTA LOCAL - MEMODOC';
        } else {
            return 'ENTREGA POR CONTA DO CLIENTE';
        }

    }
    /**
     * Retorna a quantidade total de novas etiquetas da OS
     * @return [type] [description]
     */
    public function getQtdeNovasCaixasTotalAttribute(){
        return intval($this->attributes['qtdenovascaixas']) + intval($this->attributes['qtdenovasetiquetas']);
    }

    /**
     * Calcula o turno default da OS
     * @param \Os $os
     */
    public function turnoDefault(){
        $hora = $this->solicitado_em_hora;
        if($this->cliente->atendimento <= 0){
            $this->entregar_em = $this->proximo_util(\Carbon::createFromFormat('d/m/Y', $this->solicitado_em_data))->format('d/m/Y');
            if($hora < '13:00:00'){
                $this->entrega_pela = 0;
            } else {
                $this->entrega_pela = 1;
            }
        } else {
            if($hora < '13:00:00'){
                $this->entrega_pela = 1;
                $this->entregar_em = $this->solicitado_em_data;
            } else {
                $this->entregar_em = $this->proximo_util(\Carbon::createFromFormat('d/m/Y', $this->solicitado_em_data))->format('d/m/Y');
                $this->entrega_pela = 0;
            }
        }
    }

    public function proximo_util($hoje){
        if ($hoje->dayOfWeek === \Carbon::FRIDAY || $hoje->dayOfWeek === \Carbon::SATURDAY || $hoje->dayOfWeek === \Carbon::SUNDAY){
            $hoje->next(\Carbon::MONDAY);
        } else {
            $hoje->addDay();
        }
        $dia = $hoje->day;
        $mes = $hoje->month;
        if ($this->eh_util($hoje)){
            return $hoje;
        } else {
            return $this->proximo_util($hoje);
        }
    }

    public function eh_util($data){
        $dia = $data->day;
        $mes = $data->month;
        if( ($dia == 1 && $mes == 1)
          ||($dia == 20 && $mes == 1    )
          ||($dia == 21 && $mes == 4)
          ||($dia == 23 && $mes == 4)
          ||($dia == 1 && $mes == 5)
          ||($dia == 7 && $mes == 9)
          ||($dia == 12 && $mes == 10)
          ||($dia == 17 && $mes == 10)
          ||($dia == 2 && $mes == 11)
          ||($dia == 15 && $mes == 11)
          ||($dia == 25 && $mes == 12)
        ){
            return false;
        } else {
            if ($data->dayOfWeek === \Carbon::SATURDAY || $data->dayOfWeek === \Carbon::SUNDAY){
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * Indica se a OS pode ser cancelada ou não
     */
    public function podeCancelar(){
        $pode = true;
        // Se tem reserva não pode...
        if ($this->id_reserva > 0){
            $pode = false;
            return;
        }
        // Se tem checkout não pode...
        if (count($this->checkout) > 0){
            $pode = false;
            return;
        }
        // Se tem checkin não pode...
        if (count($this->checkin) > 0){
            $pode = false;
            return;
        }
        return $pode;
    }

    public function getValFreteManualFmtAttribute($valor){
        return number_format($this->attributes['val_frete_manual'], 2, ",", ".");
    }

    public function setValFreteManualFmtAttribute($valor){
        $this->attributes['val_frete_manual'] = str_replace(",", ".", str_replace(".", "", str_replace("_", "", $valor)));
    }

    /**
     * Indica se existe algum checkin ou checkout pendente para a OS
     */
    public function temPendencia(){
		$checkins = \Checkin::pendentesdaos($this->id_os_chave)->count();
		$checkouts = \OsDesmembrada::pendentesdaos($this->id_os_chave)->count();
    }

    /**
     * Critério de seleção que implementa a regra 1.1.  OS que não permitem que o cliente seja faturado
     * @param  [type] $query     [description]
     * @param  [type] $dt_inicio [description]
     * @param  [type] $dt_fim    [description]
     * @return [type]            [description]
     */
    public function scopePendenciafaturamento($query, $dt_inicio, $dt_fim){
        return $query->where('entregar_em', '>=', $dt_inicio)
                     ->where('entregar_em', "<=", $dt_fim)
                     ->whereIn('id_status_os', array(2,3,4) )
                     ->whereRaw(" os.id_os_chave in (select id_os_chave from checkin where dt_checkin is null and id_os_chave = os.id_os_chave) ")
                     ;
    }

    public function expurgo_cortesia(){
        $expurgo = \ExpurgoPai::where('id_os_chave', '=', $this->id_os_chave)->first();
        if (count($expurgo) > 0){
            return $expurgo->cortesia;
        } else {
            return 0;
        }

    }
    public function expurgo_cortesia_movimentacao(){
        $expurgo = \ExpurgoPai::where('id_os_chave', '=', $this->id_os_chave)->first();
        if (count($expurgo) > 0){
            return $expurgo->cortesia_movimentacao;
        } else {
            return 0;
        }
    }
}
