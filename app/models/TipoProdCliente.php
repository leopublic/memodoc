<?php
/**
 * Relacionamento entre o cliente e os produtos oferecidos. 
 * 
 * Aqui ficam os preços cobrados pelos produtos e serviços oferecidos pela memodoc
 * 
 * @property int $id_cliente
 * @property int $id_tipoproduto
 * @property decimal $valor_contrato
 * @property decimal $valor_urgencia
 * @property int $minimo
 * @property decimal $valor_contrato_ant
 * @property decimal $valor_urgencia_ant
 * @property decimal $valor_minimo_ant
 * @property timestamp $created_at
 * @property timestamp $updated_at
 */
class TipoProdCliente extends Modelo {

    protected $table = 'tipoprodcliente';

    public function tipoproduto() {
        return $this->belongsTo('TipoProduto', 'id_tipoproduto');
    }

    static function salvar($dados) {
        return TipoProdCliente::where('id_tipoproduto', '=', $dados['id_tipoproduto'])
                        ->where('id_cliente', '=', $dados['id_cliente'])
                        ->update($dados);
    }

    public function cliente(){
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function scopeDotipo($query, $id_tipoproduto){
    	return $query->where('id_tipoproduto', '=', $id_tipoproduto);
    }

    public function scopeDocliente($query, $id_cliente){
    	return $query->where('id_cliente', '=', $id_cliente);
    }
    
	/**
	 * Salva os valores atuais nos campos de valor anterior
	 */
    public function salva_valores(){
    	$this->valor_contrato_ant = $this->valor_contrato;
    	$this->valor_urgencia_ant = $this->valor_urgencia;
    	$this->valor_minimo_ant = $this->minimo;
    }
}
