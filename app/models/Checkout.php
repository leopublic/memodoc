<?php
class Checkout extends Modelo {
	protected $table = 'checkout';
	protected $primaryKey = 'id_checkout';
	protected $fillable = array('id_cliente','id_caixapadrao','id_caixa','id_os', 'status', 'data_inclusao_os', 'data_checkout', 'id_os_chave', 'id_usuario_cadastro');

	public function cliente(){
		return $this->belongsTo('Cliente','id_cliente');
	}

    public function usuariocadastro(){
        return $this->belongsTo('Usuario', 'id_usuario_cadastro');
    }

    public function osdesmembrada(){
        return $this->belongsTo('OsDesmembrada', 'id_osdesmembrada');
    }
}