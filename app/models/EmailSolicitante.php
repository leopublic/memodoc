<?php
class EmailSolicitante extends Email {
	protected $table = 'emailsolicitante';
	protected $primaryKey = 'id_emailsolicitante';

	public function solicitante(){
		return $this->belongsTo('Solicitante', 'id_solicitante');
	}
}