<?php
/**
 * @property int $id_centrodecusto Chave
 * @property int $id_cliente Cliente
 * @property string $nome Nome do centro de custo
 */
class CentroDeCusto extends Modelo {

    protected $table = 'centrodecusto';
    protected $primaryKey = 'id_centrodecusto';

    public static function boot(){
        parent::boot();
        
        static::deleting(function($centro){
           \Documento::where('id_centro', '=', $centro->id_centrodecusto)->update(array('id_centro'=> null)); 
        });
    }

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function documento() {
        return $this->hasMany('Documento', 'id_centro');
    }

    public function usuarios() {
        return $this->belongsToMany('Usuario', 'usuariodepartamento', 'id_usuario', 'id_centrodecusto');
    }

    public function qtdDocumentos(){
        return Documento::where('id_centro', '=', $this->id_centrodecusto)->count();
    }

    public function getQtdDocumentosAttribute($valor) {
        $sql = "select count(*) qtd from documento where id_centro =" . $this->attributes['id_centrodecusto'];
        $res = \DB::select(\DB::raw($sql));

        return $res[0]->qtd;
    }
}
