<?php

class LogOs extends Modelo {
	protected $table = 'log_os';
	protected $primaryKey = 'id';
	protected $fillable = array('id_os_chave','id_usuario','evento','descricao', 'id_status_os', 'id_caixa');

	public static $rules = array();


    public function usuario(){
        return $this->belongsTo('Usuario', 'id_usuario');
    }

    public function statusos(){
        return $this->belongsTo('StatusOs', 'id_status_os');
    }

    public function scopeHistorico($query, $id_os_chave){
        return $query->where('id_os_chave', '=', $id_os_chave)->with('StatusOs')->with('Usuario')->orderBy('created_at', 'desc')->get();
    }
}
