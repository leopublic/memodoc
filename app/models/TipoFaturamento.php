<?php

/**
 */
class TipoFaturamento extends Modelo {
    protected $table = 'tipo_faturamento';
    protected $primaryKey = 'id_tipo_faturamento';
    protected $fillable = array('*');
    
    const tfCORREIO = 1;
    const tfEMAIL = 2;
    
}
