<?php

class Galpao extends Modelo {
    protected $table = 'galpao';
    protected $primaryKey = 'id_galpao';
    protected $protected = array();

    public static function Checkin($checkin) {
        $checkin->dt_checkin = date('Y-m-d H:i:s');
        $checkin->id_usuario_checkin = Auth::user()->id_usuario;
        $checkin->save();

        $docs = Documento::where('id_caixa', '=', $checkin->id_caixa)->get();
        foreach ($docs as $doc) {
            $doc->emcasa = 1;
            if ($doc->primeira_entrada == '') {
                $doc->primeira_entrada = date('Y-m-d H:i:s');
            }
            if($doc->itemalterado == "0"){
                $doc->titulo = "EFETUADO CHECK-IN E CAIXA NÃO INVENTARIADA";
            }
            $doc->id_usuario_alteracao = \Auth::user()->id_usuario;
            $doc->evento = __CLASS__ . "." . __FUNCTION__;
            $doc->save();
        }
        return true;
    }

    public static function PreCheckin($id_os_chave, $id_caixapadrao) {

        $os = Os::find($id_os_chave);
        $caixa = Documento::where('id_caixapadrao', '=', $id_caixapadrao)
                ->where('id_cliente', '=', $os->id_cliente)
                ->first();

        if (isset($os) and isset($caixa)) {

            Checkin::create(array(
                'id_os_chave' => $os->id_os_chave,
                'id_cliente' => $os->id_cliente,
                'id_usuario' => Auth::user()->id_usuario,
                'id_caixa' => $caixa->id_caixa,
            ));
            /*
              $docs = Documento::where('id_caixa', '=', $caixa->id_caixa)->get();
              foreach($docs as $doc){
              $doc->emcasa =1;
              if($doc->primeira_entrada == ''){
              $doc->primeira_entrada = date('Y-m-d H:i:s');
              }
              $doc->save();
              } */

            return true;
        } else {
            return false;
        }
    }

    /**
     * Verifica se a OS tem a quantidade disponivel para realização do checkin
     * @param Os $os
     * @param type $id_caixa
     * @return bool
     */
    public static function VerificaOsParaCheckin(Os $os, $id_caixa) {

        // se os existir
        if (isset($os->id_cliente)) {

            // Total de caixas possíveis para checkin
            $TotalDeCaixasParaCheckin = intval($os->apanhanovascaixas) + intval($os->apanhacaixasemprestadas);

            // Total de Checkins realizados
            $TotalDeCheckins = Checkin::where('id_caixa', '=', $id_caixa)
                    ->where('id_os_chave', '=', $os->id_chave_os)
                    ->count();

            // Se total o de caixas para disponivel checkin for maior que o total de checkins, então a OS está disponivel
            return ($TotalDeCaixasParaCheckin > $TotalDeCheckins) ? true : false;
        } else {
            return false;
        }
    }

    /**
     * Obtem uma OS disponivel para checkin
     * @param type $id_caixa
     * @return type Os
     */
    public static function GetOSDisponivelParaCheckin($id_caixa) {

        // Pega o documento da Caixa informada
        $doc = Documento::where('id_caixa', '=', $id_caixa)->first();

        if (isset($doc->id_cliente)) {

            // Pega a ultima OS do cliente (dono da caixa) onde não esteja cancelada
            $os = Os::where('id_cliente', '=', $doc->id_cliente)
                    ->where('id_status_os', '<>', StatusOs::OsCancelada())
                    ->orderby('id_os_chave', 'desc')
                    ->first();

            return $os;
        } else {
            return array();
        }
    }

    public static function Checkout($id_caixa) {
        $docs = Documento::where('id_caixa', '=', $id_caixa)
                ->get();
        foreach ($docs as $doc) {
            $doc->emcasa = 0;
            $doc->save();
            $os = OsDesmembrada::where('id_cliente', '=', $doc->id_cliente)
                    ->where('id_caixapadrao', '=', $doc->id_caixapadrao)
                    ->whereNull('id_checkout')
                    ->orderBy('id_osdesmembrada', 'desc')
                    ->first();

            $checkout = Checkout::create(array(
                        'id_cliente' => $doc->id_cliente,
                        'id_caixapadrao' => $doc->id_caixapadrao,
                        'id_caixa' => $doc->id_caixa,
                        'id_os' => $os->id_os,
                        'id_os_chave' => $os->id_os_chave,
                        'status' => 1,
                        'data_checkout' => date('Y-m-d H:i:s'),
                        'id_usuario_cadastro' => Auth::user()->id_usuario
            ));
            $os->save();
        }
    }

    public function NovaReservaParaOs($os) {

        $reserva = new Reserva();
        $reserva->id_cliente = $os->id_cliente;
        $reserva->id_tipodocumento = 1;
        $reserva->caixas = $os->qtdenovascaixas;
        $reserva->data_reserva = date('Y-m-d');
        $reserva->cancelada = 0;
        $reserva->save();
        return $reserva;
    }

}
