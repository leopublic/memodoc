<?php

class SetCliente extends Modelo {

    protected $table = 'set_cliente';
    protected $primaryKey = 'id_setor';
    
    public static function boot(){
        parent::boot();
        
        static::deleting(function($setor){
           \Documento::where('id_setor', '=', $setor->id_setor)->update(array('id_setor'=> null)); 
        });
    }

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function usuarios() {
        return $this->belongsToMany('Usuario', 'usuariodepartamento', 'id_usuario', 'id_setor');
    }

    public function qtdDocumentos() {
        return Documento::where('id_setor', '=', $this->id_setor)->count();
    }

    public function getQtdDocumentosAttribute($valor) {
        $sql = "select count(*) qtd from documento where id_setor =" . $this->attributes['id_setor'];
        $res = \DB::select(\DB::raw($sql));

        return $res[0]->qtd;
    }

}
