<?php

/**
 * @property  string
 */
class Reserva extends Modelo {

    protected $table = 'reserva';
    protected $primaryKey = 'id_reserva';
    protected $fillable = array('id_cliente', 'caixas', 'id_tipodocumento', 'inventario_impresso', 'id_os_chave');

    public static function boot() {
        parent::boot();
        Reserva::creating(function($reserva) {
            $reserva->data_reserva = date('Y-m-d');
        });
    }

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function os() {
        return $this->belongsTo('Os', 'id_os_chave');
    }

    public function reservadesmembradas(){
        return $this->hasMany('ReservaDesmembrada', 'id_reserva');
    }

    public function documentos(){
        return $this->hasMany('Documento', 'id_reserva');
    }

    public function tipodocumento() {
        return $this->belongsTo('TipoDocumento', 'id_tipodocumento');
    }

    /**
     * Reserva uma quantidade de endereços para um cliente
     * @param type $pid_cliente
     * @param type $pquantidade
     */
    public static function Nova($pid_cliente, $pquantidade, $pid_tipodocumento) {
        // Cria uma nova reserva
        $reserva = new Reserva();
        $reserva->id_cliente = $pid_cliente;
        $reserva->id_tipodocumento = $pid_tipodocumento;
        $reserva->caixas = $pquantidade;
        return $reserva;
    }

    static function getListagem($id_cliente = null, $inicio = null, $fim = null, $cancelada = 0) {

        // Select
        $model = Reserva::addSelect(DB::raw("(select min(id_caixapadrao) from reservadesmembrada rd where rd.id_reserva = reserva.id_reserva) as caixa_inicial"))
                ->addSelect(DB::raw("(select max(id_caixapadrao) from reservadesmembrada rd where rd.id_reserva = reserva.id_reserva) as caixa_final"))
                ->addSelect('reserva.id_reserva', 'reserva.observacao', 'cliente.razaosocial', 'data_reserva', 'caixas', 'tipodocumento.descricao', 'cancelada', 'id_os', 'usuario.nomeusuario', 'reserva.id_os_chave', DB::raw("coalesce(date_format(data_cancelamento, '%d/%m/%Y %H:%i:%s'), '--') as data_cancelamento"), 'usuario_cancelamento.nomeusuario as nomeusuario_cancelamento')
                ->join('cliente', 'cliente.id_cliente', '=', 'reserva.id_cliente')
                ->join('tipodocumento', 'tipodocumento.id_tipodocumento', '=', 'reserva.id_tipodocumento')
                ->leftJoin('os', 'os.id_os_chave', '=', 'reserva.id_os_chave')
                ->leftJoin('usuario', 'usuario.id_usuario', '=', 'reserva.id_usuario_alteracao')
                ->leftJoin('usuario as usuario_cancelamento', 'usuario_cancelamento.id_usuario', '=', 'reserva.id_usuario_cancelamento')
                ->orderBy('id_reserva', 'desc')
                ;

        // Filtro por Periodo
        if (isset($inicio) and isset($fim) and $inicio <> '' and $fim <> '') {
            $inicio = DateTime::createFromFormat('d/m/Y', $inicio)->format('Y-m-d');
            $fim = DateTime::createFromFormat('d/m/Y', $fim)->format('Y-m-d');
            $model->whereBetween('data_reserva', array($inicio, $fim));
        }

        // Filtro por Cliente
        if (isset($id_cliente) and $id_cliente <> '') {
            $model->where('reserva.id_cliente', '=', $id_cliente);
        }

        // Default: Não exibir também as canceladas
        if (!$cancelada) {
            //$model->where('cancelada','IS',DB::raw('null'));
            $model->where(function ($query) {
                $query->whereNull('cancelada')
                        ->orWhere('cancelada', '=', '0');
            });
        }

        // Ordenação pela data e limit 50
        $reservas = $model->orderby('data_reserva', 'desc');

        //pr(queryLog());

        return $reservas;
    }

    static function getHistoricoDeReservas($id_cliente) {

        return \DB::table('reserva')
                        ->select(DB::raw("DATE_FORMAT(reserva.data_reserva,'%Y-%m') as anomes, count(caixas) as countreserva, sum(caixas) as caixatotal"))
                        ->where('id_cliente', '=', $id_cliente)
                        ->groupBy('anomes')
                        ->get();
    }

    public function primeiraCaixa() {
        $doc = ReservaDesmembrada::where('id_reserva', '=', $this->id_reserva)
                ->orderBy('id_caixapadrao', 'asc')
                ->first()
        ;
        if(is_object($doc)){
            return $doc->id_caixapadrao;
        } else {
            return '0';
        }
    }

    public function ultimaCaixa() {
        $doc = ReservaDesmembrada::where('id_reserva', '=', $this->id_reserva)
                ->orderBy('id_caixapadrao', 'desc')
                ->first()
        ;
        if(is_object($doc)){
            return $doc->id_caixapadrao;
        } else {
            return '0';
        }
    }

    public function getUltimaCaixa($id_cliente){
        $x = DB::table('reserva')
                        ->join('reservadesmembrada', 'reservadesmembrada.id_reserva', '=', 'reserva.id_reserva')
                        ->where('reserva.id_cliente', '=', $id_cliente)
                        ->max('id_caixapadrao')
                ;
        return $x;
    }
    
    public function getIdadeReservaAttribute($valor){
        try{
            $data = \Carbon::createFromFormat('Y-m-d', $this->attributes['data_reserva']);
            $dias = $data->diffInDays(\Carbon::now());
        } catch(Exception $e){
            $dias = 0;
        }
        return $dias;

    }

    public function getDataReservaFmtAttribute(){
        try{
            return  \Carbon::createFromFormat('Y-m-d', $this->attributes['data_reserva'])->format('d/m/Y');
        } catch(Exception $e){
            return '--';
        }

    }
}
