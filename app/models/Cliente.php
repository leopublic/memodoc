<?php

/**
 * @property integer$id_cliente integer unsigned auto_increment,
 * @property string $razaosocial varchar(200) ,
 * @property string $nomefantasia varchar(200) ,
 * @property string $logradouro char(5) ,
 * @property string $endereco varchar(200) ,
 * @property string $numero varchar(20),
 * @property string $complemento varchar(20),
 * @property string $bairro varchar(200) ,
 * @property string $cidade varchar(200) ,
 * @property string $estado char(2) ,
 * @property string $cep char(8) ,
 * @property string $cgc char(14) ,
 * @property string $ie char(15) ,
 * @property string $im varchar(15) ,
 * @property string $razaoconsulta varchar(200) ,
 * @property string $fantasiaconsulta varchar(200) ,
 * @property string $obs text ,
 * @property string $diretoriorecall varchar(450),
 * @property decimal $valorminimofaturamento decimal(9,2),
 * @property date $iniciocontrato date NOT NULL,
 * @property int $franquiacustodia integer DEFAULT 0,
 * @property int $atendimento integer DEFAULT 0 NOT NULL,
 * @property decimal $franquiamovimentacao decimal(11,2) DEFAULT 0,
 * @property decimal $descontocustodiapercentual decimal(11,2) DEFAULT 0,
 * @property int $descontocustodialimite integer DEFAULT 0,
 * @property boolean $issporfora tinyint DEFAULT 1,
 * @property string $obsdesconto text,
 * @property boolean $movimentacaocortesia tinyint DEFAULT 0,
 * @property string $responsavelcobranca varchar(50),
 * @property string $indicereajuste varchar(10),
 * @property string $logradourocor char(5) ,
 * @property string $enderecocor varchar(200) ,
 * @property string $numerocor varchar(5),
 * @property string $complementocor varchar(20),
 * @property string $bairrocor varchar(200) ,
 * @property string $cidadecor varchar(200) ,
 * @property string $estadocor char(2) ,
 * @property string $cepcor char(8) ,
 * @property boolean $imprimenf tinyint DEFAULT 1 NOT NULL,
 * @property date $dt_ultimo_reajuste
 * @property boolean $fl_frete_manual       Indica se esse cliente permite frete informado manualmente
 * @property boolean $fl_digitalizacoes     Indica se esse cliente pode consultar digitalizações
 * @property boolean $fl_os_bloqueada
 * @property tinyiny $fl_memoodc            Flag para indicar quem é o cliente com os dados da Memodoc.
 * @property smallint $dia_faturamento      Indica o dia que o cliente deseja ser faturado. Nulo significa que ele segue o padrão
 * @property varchar $descricao_servicos    Complemento da descrição dos serviços prestados a ser adicionada na nota fiscal.
 * @property tinyint $fl_emissaonf_suspensa Indica que a emissão de notas fiscais está suspensa para o cliente.
 * 
 */
class Cliente extends Modelo {

    protected $table = 'cliente';
    protected $primaryKey = 'id_cliente';
    protected $fillable = array('razaosocial', 'nomefantasia', 'logradouro', 'endereco', 'numero', 'complemento', 'bairro', 'cidade', 'estado', 'cep', 'cgc', 'ie', 'im',
        'razaoconsulta', 'fantasiaconsulta', 'obs', 'diretoriorecall', 'valorminimofaturamento', 'iniciocontrato', 'franquiacustodia', 'atendimento', 'franquiamovimentacao',
        'descontocustodiapercentual', 'descontocustodialimite', 'issporfora', 'obsdesconto', 'movimentacaocortesia', 'responsavelcobranca', 'indicereajuste',
        'logradourocor', 'enderecocor', 'numerocor', 'complementocor', 'bairrocor', 'cidadecor', 'estadocor', 'cepcor', 'imprimenf', 'fl_ativo', 'em_atraso_desde', 'id_indice'
        , 'id_tipo_faturamento', 'destinatario_cobranca',  'nivel_inadimplencia', 'fl_os_bloqueada', 'dia_faturamento', 'descricao_servicos', 'fl_emissaonf_suspensa', 'fl_digitalizacao', 'fl_controle_revisao', 'dia_vencimento', "id_cliente_cobranca", "dia_vencimento_boleto", "fl_pessoa_fisica"
    );

    const scOK = 0;
    const scEMATRASO = 1;
    const scINADIMPLENTE = 2;

    /*     * ==================================================
     * Relacionamentos
     * ==================================================
     */

    public function centrocusto() {
        return $this->hasMany('CentroDeCusto', 'id_cliente')->orderBy('nome');
    }

    public function CentroDeCusto() {
        return $this->hasMany('CentroDeCusto', 'id_cliente')->orderBy('nome');
    }

    public function contato() {
        return $this->hasMany('Contato', 'id_cliente')->orderBy('nome');
    }

    public function emailcliente() {
        return $this->hasMany('EmailCliente', 'id_cliente')->orderBy('correioeletronico');
    }

    public function emailcontato() {
        return $this->hasMany('EmailContato', 'id_cliente');
    }

    public function enderecoentrega() {
        return $this->hasMany('Endeentrega', 'id_cliente')->orderBy('logradouro')->orderBy('endereco');
    }

    public function departamento() {
        return $this->hasMany('DepCliente', 'id_cliente')->orderBy('nome');
    }

    public function DepCliente() {
        return $this->hasMany('DepCliente', 'id_cliente')->orderBy('nome');
    }

    public function enderecos() {
        return $this->hasMany('Endereco', 'id_cliente');
    }

    public function faturamentos() {
        return $this->hasMany('Faturamento', 'id_cliente');
    }

    public function indice() {
        return $this->belongsTo('Indice', 'id_indice');
    }

    public function ordensdeservico() {
        return $this->hasMany('Os', 'id_cliente');
    }

    public function setor() {
        return $this->hasMany('SetCliente', 'id_cliente')->orderBy('nome');
    }

    public function SetCliente() {
        return $this->hasMany('SetCliente', 'id_cliente')->orderBy('nome');
    }

    public function solicitante() {
        return $this->hasMany('Solicitante', 'id_cliente')->orderBy('nome');
    }

    public function telefonecliente() {
        return $this->hasMany('TelefoneCliente', 'id_cliente')->orderBy('nome');
    }

    public function telefonecontato() {
        return $this->hasMany('TelefoneContato', 'id_cliente');
    }

    public function tipoprodcliente() {
        return $this->hasMany('TipoProdCliente', 'id_cliente');
    }

    /** ==================================================
     * Scopes e queries
     * ==================================================
     */

    public function search($term) {
        /*
          return $this->where('razaosocial', 'like', '%'.$term.'%')
          ->where('nomefantasia', 'like', '%'.$term.'%')
          //->where('razaoconsulta', 'like', '%'.$term.'%')
          //->where('fantasiaconsulta', 'like', '%'.$term.'%')
          ->orderby('razaosocial');
         */
        return $this->where(function($query) use ($term) {
                    $query->where('razaosocial', 'like', '%' . $term . '%')
                            ->orWhere('nomefantasia', 'like', '%' . $term . '%')
                            ->orWhere('razaoconsulta', 'like', '%' . $term . '%')
                            ->orWhere('fantasiaconsulta', 'like', '%' . $term . '%')
                            ->orWhere('id_cliente', 'like', '%' . $term . '%');
                })
        ;
    }

    public function scopeAtivos($query) {
        return $query->where('fl_ativo', '=', '1');
    }

    public function scopeRazaosocialCompleta($query) {
        return $query->select(array('cliente.*', DB::raw("concat(razaosocial, ' (', substr(concat('000',id_cliente), -3), ' - ', nomefantasia, ')') as razaosocialCompleta")));
    }

    public function scopeConferencia($query, $area){
    	switch($area){
    		case "franquiacustodia":
    			return $query->whereRaw("coalesce(franquiacustodia,0) < 90");
    			break;
    		case "descontocustodiapercentual":
    			return $query->whereRaw("coalesce(descontocustodiapercentual,0) > 0");
    			break;
    		case "franquiamovimentacao":
    			return $query->whereRaw("coalesce(franquiamovimentacao,0) > 0");
    			break;
    		case "descontocustodialimite":
    			return $query->whereRaw("coalesce(descontocustodialimite,0) > 0");
    			break;
    				 
    	}
    }
    
    public function scopeFaturaveis($query){
        return $query->whereRaw("coalesce(fl_emissaonf_suspensa, 0)=0");
    }
    
    public function scopeNaofaturaveis($query){
        return $query->where("fl_emissaonf_suspensa", "=", 1);
    }


    static public function getAtivos() {
        return Cliente::where('fl_ativo', '=', '1')
                        ->select(array('id_cliente', DB::raw("concat(razaosocial, ' (', substr(concat('000',id_cliente), -3), ' - ', nomefantasia, ')') as razaosocial")))
                        ->orderBy('razaosocial')
                        ->get();
    }

    static public function getInativos() {
        return Cliente::where('fl_ativo', '=', '0')->orWhereNull('fl_ativo')
                        ->select(array('id_cliente', DB::raw("concat(razaosocial, ' (', substr(concat('000',id_cliente), -3), ' - ', nomefantasia, ')') as razaosocial")))
                        ->orderBy('razaosocial')
                        ->get();
    }

    static public function getTodos() {
        return Cliente::select(array('id_cliente', DB::raw("concat(razaosocial, ' (', substr(concat('000',id_cliente), -3), ' - ', nomefantasia, ')') as razaosocial")))->orderBy('razaosocial')->get();
    }

    public function faturamento($mes, $ano) {
        return Faturamento::where('id_cliente', '=', $this->id_cliente)->where('mes', '=', $mes)->where('ano', '=', $ano)->first();
    }

    /** ==================================================
     * Métodos e funções
     * ==================================================
     */

    public function ultimoreajuste() {
        return $this->dt_ultimo_reajuste;
    }

    public function emCarencia($hoje = '') {
        $data = \Carbon::createFromFormat('Y-m-d', $this->iniciocontrato);
        if ($hoje == '') {
            $hoje = \Carbon::now();
        } else {
            $hoje = \Carbon::createFromFormat('Y-m-d', $hoje);
        }
        $dias = $data->diffInDays($hoje);
        if ($dias <= $this->franquiacustodia) {
            return true;
        } else {
            return false;
        }
    }

    public function qtdCaixas() {
        return DB::table('documento')->where('id_cliente', '=', $this->id_cliente)
                        ->select(array('id_caixapadrao'))
                        ->distinct()
                        ->count('id_caixapadrao');
    }

    public function qtdDocs() {
        return DB::table('documento')->where('id_cliente', '=', $this->id_cliente)
                        ->distinct()
                        ->count('id_documento');
    }

    /**
     * Indica o status de cobrança do cliente
     * 0 = ok
     * 1 = Em atraso
     * 2 = Inadimplente
     */
    public function statusComercial() {
        return 0;
    }

    public static function msgInadimplente() {
        return "Sua conta está bloqueada para essa operação";
    }

    public static function msgEmatraso() {
        return "Sua conta está em atraso";
    }

    public function cursorReajuste($mes, $ano) {
    	if($this->mes_ultimo_reajuste() == $mes && $this->ano_ultimo_reajuste() == $ano){
    		$sql = "select tp.id_tipoproduto, tp.descricao
    					, ir.percentual
    					, tpc.minimo
			        	, tpc.id_cliente
    					, tpc.valor_contrato_ant 
    					, tpc.valor_urgencia_ant  
    					, tpc.valor_contrato
    					, tpc.valor_urgencia  
    					, round(tpc.valor_contrato_ant * (1 + (ir.percentual/100)), 2) valor_contrato_reajustado
			            , round(tpc.valor_urgencia_ant * (1 + (ir.percentual/100)), 2) valor_urgencia_reajustado
			            , c.valorminimofaturamento
			            , c.valorminimofaturamento_ant
    					, round(c.valorminimofaturamento_ant * (1 + (ir.percentual/100)), 2) valorminimofaturamento_reajustado
			        	, fl_tem_urgencia
		            from tipoproduto tp
		            join cliente c
		            left join indicereajuste ir on ir.mes = " . $mes . " and ir.ano = " . $ano . " and ir.id_indice = c.id_indice
		            left join tipoprodcliente tpc on tpc.id_tipoproduto = tp.id_tipoproduto and tpc.id_cliente = c.id_cliente
		            where c.id_cliente = " . $this->id_cliente;
    	} else {
    		$sql = "select tp.id_tipoproduto, tp.descricao
    					, ir.percentual
    					, tpc.minimo
			        	, tpc.id_cliente
    					, tpc.valor_contrato valor_contrato_ant 
    					, tpc.valor_urgencia valor_urgencia_ant  
    					, tpc.valor_contrato
    					, tpc.valor_urgencia  
    					, round(tpc.valor_contrato * (1 + (ir.percentual/100)), 2) valor_contrato_reajustado
			            , round(tpc.valor_urgencia * (1 + (ir.percentual/100)), 2) valor_urgencia_reajustado
	    		        , c.valorminimofaturamento
			            , c.valorminimofaturamento valorminimofaturamento_ant
    					, round(c.valorminimofaturamento * (1 + (ir.percentual/100)), 2) valorminimofaturamento_reajustado
	        			, fl_tem_urgencia
		            from tipoproduto tp
		            join cliente c
		            left join indicereajuste ir on ir.mes = " . $mes . " and ir.ano = " . $ano . " and ir.id_indice = c.id_indice
		            left join tipoprodcliente tpc on tpc.id_tipoproduto = tp.id_tipoproduto and tpc.id_cliente = c.id_cliente
		            where c.id_cliente = " . $this->id_cliente;
    		
    	}
        $rows = \DB::select(\DB::raw($sql));
        return $rows;
    }

    public function getEnderecoCorCompletoAttribute($valor) {
        $ret = '';
        $sep = '';
        if (trim($this->logradourocor) != '') {
            $ret .= trim($this->logradourocor);
            $sep = ' ';
        }
        if (trim($this->enderecocor) != '') {
            $ret .= $sep . trim($this->enderecocor);
            $sep = ' ';
        }
        if (trim($this->numerocor) != '') {
            $ret .= $sep . trim($this->numerocor);
            $sep = ', ';
        }
        if (trim($this->complementocor) != '') {
            $ret .= $sep . trim($this->complementocor);
        }
        $sep = ', ';
        if (trim($this->bairrocor) != '') {
            $ret .= $sep . trim($this->bairrocor);
        }
        $sep = '<br/>';
        if (trim($this->cidadecor) != '') {
            $ret .= $sep . trim($this->cidadecor);
            $sep = ' - ';
        }
        if (trim($this->estadocor) != '') {
            $ret .= $sep . trim($this->estadocor);
            $sep = ' - ';
        }
        if (trim($this->cepcor) != '') {
            $ret .= $sep . 'CEP ' . trim($this->cepcor);
            $sep = ' - ';
        }
        return $ret;
    }

    public function clonaEndParaCorrespondencia($clonar = false) {
        if ($this->enderecocor == '' || $clonar) {
            $this->enderecocor = $this->endereco;
            $this->logradourocor = $this->logradouro;
            $this->bairrocor = $this->bairro;
            $this->cidadecor = $this->cidade;
            $this->complementocor = $this->complemento;
            $this->numerocor = $this->numero;
            $this->estadocor = $this->estado;
            $this->cepcor = $this->cep;
        }
    }

    public function clonaEndParaEntregaSeNaoHouver() {
        $end = \Endeentrega::where('id_cliente', '=', $this->id_cliente)->count();
        if ($end == 0) {
            $this->clonaEndParaEntrega();
        }
    }

    public function clonaEndParaEntrega() {
        $end = new \Endeentrega;
        $end->id_cliente = $this->id_cliente;
        $end->endereco = $this->endereco;
        $end->logradouro = $this->logradouro;
        $end->bairro = $this->bairro;
        $end->cidade = $this->cidade;
        $end->complemento = $this->complemento;
        $end->numero = $this->numero;
        $end->estado = $this->estado;
        $end->cep = $this->cep;
        $end->save();
    }

    public function clonaCorrespondenciaParaEntrega() {
        $end = new \Endeentrega;
        $end->id_cliente = $this->id_cliente;
        $end->endereco = $this->enderecocor;
        $end->logradouro = $this->logradourocor;
        $end->bairro = $this->bairrocor;
        $end->cidade = $this->cidadecor;
        $end->complemento = $this->complementocor;
        $end->numero = $this->numerocor;
        $end->estado = $this->estadocor;
        $end->cep = $this->cepcor;
        $end->save();
    }

    public function getCgcFormatadoAttribute($valor){
        if(strlen(trim($this->cgc)) > 11){
            return substr($this->cgc, 0,2).'.'.substr($this->cgc, 2,3).'.'.substr($this->cgc, 5,3).'/'.substr($this->cgc, 8,4).'-'.substr($this->cgc, 12,2);
        } else {
            return substr($this->cgc, 0,3).'.'.substr($this->cgc, 3,3).'.'.substr($this->cgc, 6,3).'-'.substr($this->cgc, 9,2);
        }
    }

    public function getTemNotificacaoOsAttribute(){
        $emails = EmailCliente::where('id_cliente', '=', $this->attributes['id_cliente'])
                        ->where('id_tipoemail', '=', TipoEmail::NOTIFICACAO_OS)
                        ->get()
                        ;
        if (count($emails) > 0){
            return true;
        } else {
            return false;
        }
    }

    public function getEmailsNotificacaoOsAttribute(){
        return EmailCliente::where('id_cliente', '=', $this->attributes['id_cliente'])
                        ->where('id_tipoemail', '=', TipoEmail::NOTIFICACAO_OS)
                        ->lists('correioeletronico')
                        ;
    }
    
    public function descricao_indice(){
    	if ($this->id_indice > 0){
 			return $this->indice->descricao;   		
    	} else {
    		return '(não informado)';
    	}
    }
    
    public function mes_ultimo_reajuste(){
    	if ($this->dt_ultimo_reajuste != ''){
    		try{
    			$dt = \Carbon::createFromFormat('Y-m-d', $this->dt_ultimo_reajuste);
    			return $dt->month;
    		} catch(\Exception $e){
				return 'Erro '.$e;		    			
    		}
    	}
    }


    public function ano_ultimo_reajuste(){
    	if ($this->dt_ultimo_reajuste != ''){
    		try{
    			$dt = \Carbon::createFromFormat('Y-m-d', $this->dt_ultimo_reajuste);
    			return $dt->year;
    		} catch(\Exception $e){
    			return '';
    		}
    	}
    }

    public function suspende_faturamento(){
        $this->fl_emissaonf_suspensa = 0;
    }
    public function habilita_faturamento(){
        $this->fl_emissaonf_suspensa = 1;        
    }

    public function email_nota(){
        $email = \EmailCliente::where('id_cliente', '=', $this->id_cliente)->where('id_tipoemail', '=', 2)->first();
        if(count($email)> 0){
            return $email->correioeletronico;
        } else {
            return '';
        }
    }

    public function telefone_nota(){
        $telefone = \TelefoneCliente::where('id_cliente', '=', $this->id_cliente)->where('id_tipotelefone', '=', 2)->first();
        if(count($telefone)> 0){
            return $telefone->numero;
        } else {
            return '';
        }       
    }
    
    public function valores_em_array_para_nota(){
        $ret = [];
        $ret['contr_cnpj'] = preg_replace("/\D/", "", $this->cgc);
        $ret['contr_insc_municipal'] = preg_replace("/\D/", "", $this->im);
        return $ret;
    }

    public function scopeMemodoc($query){
        return $query->where('fl_memodoc', '=', 1);
    }
}