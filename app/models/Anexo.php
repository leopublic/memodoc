<?php

/**
 * @property int $id_anexo 
 * @property varchar $descricao 
 * @property varchar $extensao 
 * @property varchar $mime
 * @property int $tamanho
 * @property varchar $nome_original 
 * @property timestamp $created_at 
 * @property timestamp $updated_at 
 */
class Anexo extends Modelo {

    protected $table = 'anexo';
    protected $primaryKey = 'id_anexo';

    public function raiz(){
        return storage_path().'/anexos/';
    }

    public function caminho(){
        return $this->raiz().$this->id_anexo;
    }

    public function salvaArquivo($nome_campo){
        if (Input::hasFile($nome_campo)){
            $arquivo = Input::file($nome_campo);
            if ($arquivo->isValid()){
                $this->save();
                $arquivo->move($this->raiz(), $this->id_anexo);
                chmod($this->caminho(), 0755);
                $this->nome_original = $arquivo->getClientOriginalName();
                $this->extensao = $arquivo->getClientOriginalExtension();
                $this->tamanho = $arquivo->getClientSize();
                $this->mime = $arquivo->getClientMimeType();
                $this->save();
            }
        }
    }

    public function deleta(){
        // Deleta arquivo físico
        if (File::exists($this->caminho())){
            File::delete($this->caminho());
        }
        $this->delete();
    }

}
