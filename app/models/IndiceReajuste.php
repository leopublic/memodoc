<?php

/**
 */
class IndiceReajuste extends Modelo {

    protected $table = 'indicereajuste';
    protected $primaryKey = 'id_indicereajuste';
    protected $fillable = array('*');

    protected $dates= array("created_at", "updated_at");
    
    public function indiceent(){
        return $this->belongsTo('Indice', 'id_indice');
    }
    
    public function clientes_reajustando_em($mes, $ano){
    	return \Cliente::where('id_indice', '=', $this->id_indice)
    				->ativos()
    				->whereRaw("month(iniciocontrato) = ".$mes)
					->whereRaw(" year(iniciocontrato) <>".$ano)
    				->count();
    }

    public function clientes_reajustados($mes, $ano){
    	return \Cliente::where('id_indice', '=', $this->id_indice)
    	->ativos()
    	->whereRaw("month(iniciocontrato) = ".$mes)
		->whereRaw(" year(iniciocontrato) <>".$ano)
    	->whereRaw("month(dt_ultimo_reajuste) >= ".$mes." and year(dt_ultimo_reajuste) >= ".$ano)
    	->count();
    }
    
    public function ultima_atualizacao(){
    	\log::info($this->created_at->format('d/m/Y'));
    	 if ($this->updated_at != ''){
    		try{
    			\log::info("Atualizado em ".$this->updated_at);
    			return \Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at)->format('d/m/Y H:i:s');
    		} catch(\Exception $e){
    			return '--';
    		}
    	}
    }
}
