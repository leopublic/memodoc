<?php
class DocumentoCancelado extends Modelo {

    protected $table = 'documento_cancelado';
    protected $primaryKey = 'id_documento';

    public function endereco_completo(){
        if (count($this->endereco)> 0){
            return $this->endereco->enderecoCompleto();
        } else {
            return '(endereço não encontrado)';
        }
    }

    public function endereco() {
        return $this->belongsTo('Endereco', 'id_caixa');
    }

}