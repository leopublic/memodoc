<?php

/**
 * @property int $id_expurgo_pai
 * @property int $id_cliente
 * @property date $data_solicitacao
 * @property int $id_usuario_cadastro
 */
class ExpurgoPai extends Modelo {

    protected $table = 'expurgo_pai';
    protected $primaryKey = 'id_expurgo_pai';

    public function cliente() {
        return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function getDataSolicitacaoAttribute($data) {
        return $this->dtModelParaView($data);
    }

    public function setDataSolicitacaoAttribute($data) {
        $this->attributes['data_solicitacao'] = $this->dtViewParaModel($data);
    }

    public function scopeDoCliente($query, $id_cliente){
        return $query->where('id_cliente', '=', $id_cliente)
                    ->orderBy('data_solicitacao', 'desc')
                    ->orderBy('id_expurgo_pai', 'desc')
                ;
    }
    
    public function cadastradopor(){
        return $this->belongsTo('usuario', 'id_usuario_cadastro');
    }
    public function getNumOsAttribute(){
        if ($this->id_os_chave > 0){
            $os = \Os::find($this->id_os_chave);
            return substr('000000'.$os->id_os, -6);
        } else {
            return '--';
        }
    }

    public function qtd_caixas(){
        return \Expurgo::where('id_expurgo_pai', '=', $this->id_expurgo_pai)->count();
    }
}
