<?php
/**
 * @property int $id_osdesmembrada Chave
 * @property int $id_caixa Referencia solicitada
 * @property int $id_os_chave Ordem de serviço que solicitou a referencia
 * @property int $status Situacao do emprestimo. 0=> Pendente, 1=> Atendido, 2=> Cancelado
 * @property datetime $data_checkout Data em que a caixa deixou o galpao
 * @property int $id_usuario_inclusao Usuario que adicionou o empréstimo na OS
 * @property int $id_usuario_checkout Usuário que retirou a caixa do galpao
 */
class OsDesmembrada extends Modelo {

    const stPENDENTE = 0;
    const stATENDIDA = 1;
    const stCANCELADA = 2;

    protected $table = 'osdesmembrada';
    protected $primaryKey = 'id_osdesmembrada';
    protected $fillable = array( 'id_caixa', 'status', 'data_checkout', 'id_usuario_checkout', 'id_usuario_inclusao', 'id_os_chave');

    public function get_nomeTabelaFirebird() {
        return 'TBL_OSDESMEMBRADA';
    }

    public function documento() {
        return $this->belongsTo('Documento', 'id_documento');
    }

    public function endereco() {
        return $this->belongsTo('Endereco', 'id_caixa');
    }

    public function endereco_completo(){
        if (count($this->endereco) > 0){
            return $this->endereco->enderecoCompleto();
        } else {
            return '(expurgada)';
        }
    }

    public function scopeDaOs($query, \Os $os){
        return $query->where('id_os_chave', '=', $os->id_os_chave)
                     ->orderBy('id_caixapadrao')
                ;
    }

    public function scopePendentesdaos($query, \Os $os){
    	return $query->where('id_os_chave', '=', $os->id_os_chave)->whereNull('id_checkout');
    }
    
    public function os(){
        return $this->belongsTo('Os', 'id_os_chave');
    }


    public function checkout(){
        return $this->belongsTo('Checkout', 'id_checkout');
    }
}
