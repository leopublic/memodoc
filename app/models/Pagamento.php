<?php
/**
 * Representa um registro de pagamento enviado pelo banco no arquivo de retorno
 * @property int $id_pagamento
 * @property int $id_arquivo_retorno
 * @property int $id_cliente
 * @property int $id_faturamento
 * @property int $cnpj
 * @property int $sacado
 * @property string $situacao
 * @property string $titulo
 * @property string $nosso_numero
 * @property date $data_emissao
 * @property date $data_vencimento
 * @property date $data_pagamento
 * @property decimal $valor
 * @property decimal $valor_pago
 */
class Pagamento extends Modelo
{
    protected $table = 'pagamento';
    protected $primaryKey = 'id_pagamento';

    public function cliente(){
    	return $this->belongsTo('Cliente', 'id_cliente');
    }

    public function faturamento(){
        return $this->belongsTo('Faturamento', 'id_faturamento');
    }

    public function pagador(){
    	return $this->belongsTo('Pagador', 'id_pagador');
    }

    public function getNomeClienteAssociadoAttribute($valor){
    	if (count($this->cliente) > 0){
    		return '('.substr('000'.$this->id_cliente, -3).') '.$this->cliente->nomefantasia.'<br/>'.$this->cliente->razaosocial;
    	}
    }

    public function getNomePagadorAssociadoAttribute($valor){
    	if (count($this->pagador) > 0){
    		return $this->pagador->razaosocial;
    	}
    }

    public function getCnpjFormatadoAttribute($valor){
    	$val = intval($this->cnpj);
        if($val > 99999999999){
        	$cnpj = $this->cnpj;
            return substr($cnpj, 0,2).'.'.substr($cnpj, 2,3).'.'.substr($cnpj, 5,3).'/'.substr($cnpj, 8,4).'-'.substr($cnpj, 12,2);
        } else {
        	$cpf = $this->cnpj;
            return substr($cpf, 0,3).'.'.substr($cpf, 3,3).'.'.substr($cpf, 6,3).'-'.substr($cpf, 9,2);
        }
    }

    public function getFaturamentoAnomesvalorAttribute($valor){
        if (count($this->faturamento) > 0){
            $perfat = $this->faturamento->periodofaturamento;

            return $perfat->ano.'/'.$perfat->mes.' R$ '.number_format($this->faturamento->val_servicos_sem_iss + $this->faturamento->val_iss + $this->faturamento->val_cartonagem_enviadas, 2, ',', '.');
            //return $perfat->ano.'/'.$perfat->mes;
        } else {
            '--';
        }

    }
    /**
     * Converte uma data em formato texto ddmmaaaa em aaaa-mm-dd
     * @param [type] $valor    [description]
     * @param [type] $atributo [description]
     */
    public function setDataTexto($valor){
        if (trim($valor) != ''){
            $valor = '20'.substr($valor, 4,2).'-'.substr($valor, 2,2).'-'.substr($valor, 0,2);
            return  $valor;
        } else {
            return null;
        }
    }

    public function scopeDocliente($query, $id_cliente = ''){
        if ($id_cliente > 0){
            $query->where('id_cliente', '=', $id_cliente);
        }
        return $query;
    }

    public function scopeNumero($query, $numero = ''){
        if ($numero > 0){
            $query->where('numero', '=', $numero);
        }
        return $query;
    }

    public function scopeVencimento($query, $venc_ini = '', $venc_fim = ''){
        if ($venc_ini != ''){
            $venc_ini = \Carbon::createFromFormat("d/m/Y", $venc_ini)->format("Y-m-d");
            $query->where('data_vencimento', '>=', $venc_ini);
        }
        if ($venc_fim != ''){
            $venc_fim = \Carbon::createFromFormat("d/m/Y",$venc_fim)->format("Y-m-d");
            $query->where('data_vencimento', '<=', $venc_fim);
        }
        return $query;
    }

    public function scopeEmissao($query, $venc_ini = '', $venc_fim = ''){
        if ($venc_ini != ''){
            $venc_ini = \Carbon::createFromFormat("d/m/Y",$venc_ini)->format("Y-m-d");
            $query->where('data_emissao', '>=', $venc_ini);
        }
        if ($venc_fim != ''){
            $venc_fim = \Carbon::createFromFormat("d/m/Y",$venc_fim)->format("Y-m-d");
            $query->where('data_emissao', '<=', $venc_fim);
        }
        return $query;
    }

    public function scopePagamento($query, $venc_ini = '', $venc_fim = ''){
        if ($venc_ini != ''){
            $venc_ini = \Carbon::createFromFormat("d/m/Y",$venc_ini)->format("Y-m-d");
            $query->where('data_pagamento', '>=', $venc_ini);
        }
        if ($venc_fim != ''){
            $venc_fim = \Carbon::createFromFormat("d/m/Y",$venc_fim)->format("Y-m-d");
            $query->where('data_pagamento', '<=', $venc_fim);
        }
        return $query;
    }

    public function scopeValor($query, $valor = ''){
        if ($valor != ''){
            $valor = str_replace(',', ".", $valor);
            $query->where('valor', '=', $valor);
        }
        return $query;
    }

    public function scopeStatus($query, $valor = ''){
        if ($valor != ''){
            switch($valor){
                case "todos":
                    break;
                case "pendentes":
                    $query->whereNull('data_pagamento');
                    break;
                case "quitados":
                    $query->whereNotNull('data_pagamento');
                    break;
            }
        }
        return $query;
    }

    public function scopeSemcliente($query, $semcliente){
        if ($semcliente){
            return $query->whereNull('id_cliente');
        } else {
            return $query;
        }
    }

    public function scopeDuplicidadecnpj($query, $duplicidade){
        if ($duplicidade){
            return $query->where('fl_duplicidade_cnpj', '=', '1');
        } else {
            return $query;
        }
    }

    public function setDataEmissaoFmtAttribute($data) {
        $this->attributes['data_emissao'] = $this->dtViewParaModel($data);
    }
    public function getDataEmissaoFmtAttribute($data) {
        return $this->dtModelParaView($this->attributes['data_emissao']);
    }

    public function setDataVencimentoFmtAttribute($data) {
        $this->attributes['data_vencimento'] = $this->dtViewParaModel($data);
    }
    public function getDataVencimentoFmtAttribute($data) {
        return $this->dtModelParaView($this->attributes['data_vencimento']);
    }

    public function setDataPagamentoFmtAttribute($data) {
        $this->attributes['data_pagamento'] = $this->dtViewParaModel($data);
    }
    public function getDataPagamentoFmtAttribute($data) {
        return $this->dtModelParaView($this->attributes['data_pagamento']);
    }


}
