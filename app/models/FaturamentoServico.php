<?php
/**
 * @property int $id_faturamento_servico
 * @property int $id_faturamento
 * @property int $id_tipoproduto
 * @property int $qtd
 * @property decimal $valor_contrato
 * @property decimal $valor_urgencia
 * @property decimal $minimo
 */
class FaturamentoServico extends Modelo{
	protected $table = 'faturamento_servico';
	protected $primaryKey = 'id_faturamento_servico';

    public function faturamento(){
        return $this->belongsTo('Faturamento', 'id_faturamento');
    }

    public function tipoproduto(){
        return $this->belongsTo('TipoProduto', 'id_tipoproduto');
    }
}