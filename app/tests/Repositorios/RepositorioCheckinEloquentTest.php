<?php

use \Memodoc\Repositorios;

class RepositorioCheckinEloquentTest extends \TestCase {
    protected $id_cliente;

    public function setUp() {
        parent::setUp();
        // Cria o cliente de teste
        $cliente = new Cliente;
        $cliente->razaosocial = 'Cliente teste';
        $cliente->save();
        $this->id_cliente = $cliente->id_cliente;

        // Cria as caixas do cliente
        $doc = new Documento;
        $doc->id_cliente = $cliente->id_cliente;
        $doc->id_caixapadrao = 1;
        $doc->emcasa = 1;
        $doc->save();
        $doc = new Documento;
        $doc->id_cliente = $cliente->id_cliente;
        $doc->id_caixapadrao = 2;
        $doc->emcasa = 1;
        $doc->save();
        $doc = new Documento;
        $doc->id_cliente = $cliente->id_cliente;
        $doc->id_caixapadrao = 3;
        $doc->emcasa = 1;
        $doc->save();
        $doc = new Documento;
        $doc->id_cliente = $cliente->id_cliente;
        $doc->id_caixapadrao = 4;
        $doc->emcasa = 0;
        $doc->save();

        // Cria as OS
        $os = new Os;
        $os->id_cliente = $cliente->id_cliente;
        $os->id_os = 1;
        $os->id_status_os = 1;
        $os->save();

        $os = new Os;
        $os->id_cliente = $cliente->id_cliente;
        $os->id_os = 2;
        $os->id_status_os = 1;
        $os->save();

        $os = new Os;
        $os->id_cliente = $cliente->id_cliente;
        $os->id_os = 3;
        $os->id_status_os = 4;
        $os->save();

        //Cria checkins
        

    }
    public function testAdicionaCaixa_SemOS() {
        $rep = new Repositorios\RepositorioCheckinEloquent;
    }

    public function testAdicionaCaixa_Inexistente() {

        $rep = new Repositorios\RepositorioCheckinEloquent;
    }

    public function testAdicionaCaixa_NaoEmprestada() {
        $rep = new Repositorios\RepositorioCheckinEloquent;
    }

    public function testAdicionarCaixa_OSCancelada() {
        $rep = new Repositorios\RepositorioCheckinEloquent;
    }

    public function testAdicionarCaixa_OSConcluida() {
        $rep = new Repositorios\RepositorioCheckinEloquent;
    }

    public function tearDown(){
        parent::tearDown();

    }
}
