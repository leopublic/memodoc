<?php

use \Memodoc\Repositorios;

class RepositorioReservaEloquentTest extends \TestCase {

    public function reservasComErros() {
        return array(
            array(0, 10, 1, 'O cliente é obrigatório'),
            array('', 10, 1, 'O cliente é obrigatório'),
            array('xx', 10, 1, 'O cliente é obrigatório'),
            array(37, '', 1, 'Informe a quantidade de endereços a serem reservados'),
            array(37, 0, 1, 'A quantidade de caixas deve ser maior que 0'),
            array(37, 'xx', 1, 'A quantidade de caixas deve ser um número maior que 0'),
            array(37, 1000000, 1, 'Não existem endereços disponíveis para a quantidade solicitada'),
            array(37, 10, '', 'O tipo do documento é obrigatório'),
            array(37, 10, 0, 'O tipo do documento é obrigatório'),
            array(37, 10, 'xx', 'O tipo do documento é obrigatório'),
        );
    }

    /**
     * @dataProvider reservasComErros
     */
    public function testCriar_erros($id_cliente, $caixas, $id_tipodocumento, $msg = '') {
        $post = array(
            'id_cliente' => $id_cliente,
            'caixas' => $caixas,
            'id_tipodocumento' => $id_tipodocumento,
        );
        $rep = new \Memodoc\Repositorios\RepositorioReservaEloquent;
        $reserva = $rep->criar($post, 1450);
        $errors = $rep->getErrors();
        $this->assertGreaterThan(0, count($errors));
        if ($msg != '' && count($errors) == 1) {
            $this->assertEquals($msg, $errors[0]);
        }
        if ($reserva) {
            $reserva->delete();
        }
    }

    public function testCriar_Enderecos() {
        $id_cliente = 37;
        $id_tipodocumento = 1;
        $caixas = 10;
        $post = array(
            'id_cliente' => $id_cliente,
            'caixas' => $caixas,
            'id_tipodocumento' => $id_tipodocumento,
        );
        $qtdDispAnt = \Endereco::get_qtdDisponivel($id_tipodocumento);
        $qtdEndClienteAnt = \Endereco::where('id_cliente', '=', $id_cliente)->count();
        $qtdItensClienteAnt = \Documento::where('id_cliente', '=', $id_cliente)->count();
        $rep = new \Memodoc\Repositorios\RepositorioReservaEloquent;
        $reserva = $rep->criar($post, 1450);
        $errors = $rep->getErrors();
        $this->assertNull($errors);

        // Verifica se foi tudo criado
        $qtdDispDepois = \Endereco::get_qtdDisponivel($id_tipodocumento );
        $qtdEndClienteDepois = \Endereco::where('id_cliente', '=', $id_cliente)->count();
        $qtdItensClienteDepois = \Documento::where('id_cliente', '=', $id_cliente)->count();
        $qtdReservaDesmembrada = \ReservaDesmembrada::where('id_reserva', '=', $reserva->id_reserva)->count();

        $this->assertEquals($qtdDispAnt - $caixas, $qtdDispDepois);
        $this->assertEquals($qtdEndClienteAnt + $caixas, $qtdEndClienteDepois);
        $this->assertEquals($qtdItensClienteAnt + $caixas, $qtdItensClienteDepois);
        $this->assertEquals($caixas, $qtdReservaDesmembrada);

        if ($reserva) {
            $rep->cancelarReserva($reserva, 1450);
            $reserva->documentos()->delete();
            $reserva->reservadesmembradas()->delete();
            $reserva->delete();
        }
    }

    public function testCriar_Log() {
        $id_cliente = 37;
        $id_tipodocumento = 1;
        $caixas = 10;
        $post = array(
            'id_cliente' => $id_cliente,
            'caixas' => $caixas,
            'id_tipodocumento' => $id_tipodocumento,
        );
        $rep = new \Memodoc\Repositorios\RepositorioReservaEloquent;
        $reserva = $rep->criar($post, 1450);
        $errors = $rep->getErrors();
        $this->assertNull($errors);

        // Verifica se foi tudo criado
        $x = \Reserva::find($reserva->id_reserva);
        $this->assertNotEmpty($x->id_usuario_alteracao);
        $this->assertNotEmpty($x->created_at);

        if ($reserva) {
            $rep->cancelarReserva($reserva, 1450);
            $reserva->documentos()->delete();
            $reserva->reservadesmembradas()->delete();
            $reserva->delete();
        }
    }


    public function testCancelar() {
        $rep = new \Memodoc\Repositorios\RepositorioReservaEloquent;
        $id_cliente = 37;
        $id_tipodocumento = 1;
        $caixas = 10;
        $post = array(
            'id_cliente' => $id_cliente,
            'caixas' => $caixas,
            'id_tipodocumento' => $id_tipodocumento,
        );
        $reserva = $rep->criar($post, 1450);
        $qtdDispAnt = \Endereco::get_qtdDisponivel($id_tipodocumento);
        $qtdEndClienteAnt = \Endereco::where('id_cliente', '=', $id_cliente)->count();
        $qtdEndClienteCanceAnt = \Endereco::where('id_cliente', '=', $id_cliente)
                                ->where('endcancelado', '=', 1)
                                ->count();
        $qtdItensClienteAnt = \Documento::where('id_cliente', '=', $id_cliente)->count();
        $horaAntes = date('Y-m-d H:i:s');
        $rep->cancelarReserva($reserva, 1400);
        $qtdDispDepois = \Endereco::get_qtdDisponivel($id_tipodocumento );
        $qtdEndClienteDepois = \Endereco::where('id_cliente', '=', $id_cliente)->count();
        $qtdEndClienteCanceDepois = \Endereco::where('id_cliente', '=', $id_cliente)
                                ->where('endcancelado', '=', 1)
                                ->count();
        $qtdItensClienteDepois = \Documento::where('id_cliente', '=', $id_cliente)->count();
        $qtdReservaDesmembrada = \ReservaDesmembrada::where('id_reserva', '=', $reserva->id_reserva)->count();

        $this->assertEquals($qtdDispAnt + $caixas, $qtdDispDepois, 'Quantidade de endereços disponíveis');
        $this->assertEquals($qtdEndClienteAnt , $qtdEndClienteDepois, 'Quantidade de endereços associados ao cliente');
        $this->assertEquals($qtdEndClienteCanceAnt + $caixas, $qtdEndClienteCanceDepois, 'Quantidade de endereços cancelados do cliente');
        $this->assertEquals($qtdItensClienteAnt - $caixas, $qtdItensClienteDepois, 'Quantidade de itens do cliente');
        $this->assertEquals($caixas, $qtdReservaDesmembrada, 'Quantidade de itens de reserva associado à reserva');

        $x = \Reserva::find($reserva->id_reserva);

        $this->assertEquals(1, $x->cancelada);
        $this->assertEquals(1400, $x->id_usuario_cancelamento);
        $this->assertGreaterThanOrEqual($x->data_cancelamento, $horaAntes);

        if ($reserva){
            $reserva->documentos()->delete();
            $reserva->reservadesmembradas()->delete();
            $reserva->delete();
        }
    }

    public function testproximoNumeroCaixa() {
        $rep = new \Memodoc\Repositorios\RepositorioReservaEloquent;
        $numero_ant = $rep->proximoNumeroCaixa(37);
        // Cria uma reserva de 10 caixas
        $post = array(
            'id_cliente' => 37,
            'caixas' => 10,
            'id_tipodocumento' => 1,
        );
        $reserva = $rep->criar($post, 1450);
        $numero_depois = $rep->proximoNumeroCaixa(37);
        $this->assertEquals($numero_ant + 10, $numero_depois);
        $rep->cancelarReserva($reserva, 1450);
        $numero_canc = $rep->proximoNumeroCaixa(37);
        // Garante que o próximo número não mudou mesmo depois da reserva
        $this->assertEquals($numero_ant + 10, $numero_canc);
        if ($reserva){
            $reserva->documentos()->delete();
            $reserva->reservadesmembradas()->delete();
            $reserva->delete();
        }
    }
}
