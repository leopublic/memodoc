<?php
use \Memodoc\Repositorios;

class RepositorioTransbordoEloquentTest extends \TestCase {
    const OBS_TEST = 'testes_m2_por_favor_delete';
    const CAIXA_INICIAL = 9110;
    const CAIXA_FINAL= 9119;
    public function testCriar(){
        $rep = new \Memodoc\Repositorios\RepositorioTransbordoEloquent;
        
        $post = array(
            "id_cliente_antes" => 32,
            "id_cliente_depois" => 242,
            "observacao" => self::OBS_TEST
        );
        $transbordo = $rep->novoTransbordo($post, 2022);
        $this->assertNotEmpty($transbordo->id_transbordo);
        $this->assertEquals(32, $transbordo->id_cliente_antes);
        $this->assertEquals(242, $transbordo->id_cliente_depois);
        
    }
    
    public function testAdicionaCaixaErrado(){
        $rep = new \Memodoc\Repositorios\RepositorioTransbordoEloquent;
        
        $post = array(
            "id_cliente_antes" => 32,
            "id_cliente_depois" => 242,
            "observacao" => self::OBS_TEST
        );
        $transbordo = $rep->novoTransbordo($post, 2022);

        $ret = $rep->adicionaCaixa($transbordo, '');
        $this->assertFalse($ret);
        $this->assertEquals('O número da caixa inicial é obrigatório', $rep->errors[0]);
        $rep->errors = array();
        $ret = $rep->adicionaCaixa($transbordo, 999999);
        $this->assertFalse($ret);
        $this->assertEquals('Nenhuma caixa encontrada no intervalo', $rep->errors[0]);
        $rep->errors = array();
        $ret = $rep->adicionaCaixa($transbordo, 999999, 99999);
        $this->assertFalse($ret);
        $this->assertEquals('O número da caixa final deve ser maior que o número da caixa inicial', $rep->errors[0]);
    }
    
    public function testAdicionaCaixaTiposDiferentes(){
        $rep = new \Memodoc\Repositorios\RepositorioTransbordoEloquent;
        
        $post = array(
            "id_cliente_antes" => 19,
            "id_cliente_depois" => 242,
            "observacao" => self::OBS_TEST
        );
        $transbordo = $rep->novoTransbordo($post, 2022);

        $ret = $rep->adicionaCaixa($transbordo, 580, 590);
        $this->assertFalse($ret);
        $this->assertEquals('Somente documentos de um mesmo tipo podem ser migrados juntos', $rep->errors[0]);
    }
    
    public function testAdicionaCaixa(){
        $rep = new \Memodoc\Repositorios\RepositorioTransbordoEloquent;
        
        $post = array(
            "id_cliente_antes" => 32,
            "id_cliente_depois" => 242,
            "observacao" => self::OBS_TEST
        );
        $transbordo = $rep->novoTransbordo($post, 2022);

        $ret = $rep->adicionaCaixa($transbordo, self::CAIXA_INICIAL, self::CAIXA_FINAL);
        $this->assertTrue($ret);
        
        $qtd = \TransbordoCaixa::where('id_transbordo', '=', $transbordo->id_transbordo)->count();
        $this->assertEquals(10, $qtd);

        $this->assertEquals(10, $transbordo->qtd_caixas);

        $caixas = \TransbordoCaixa::where('id_transbordo', '=', $transbordo->id_transbordo)->orderBy('id_caixapadrao_antes')->get();
        $i = self::CAIXA_INICIAL;
        foreach($caixas as $caixa){
            $this->assertEquals($i, $caixa->id_caixapadrao_antes, 'Caixa '.$i);
            $i++;
        }
    }

    public function testCriaReserva(){
        $rep = new \Memodoc\Repositorios\RepositorioTransbordoEloquent;
        
        $post = array(
            "id_cliente_antes" => 32,
            "id_cliente_depois" => 242,
            "observacao" => self::OBS_TEST
        );
        $transbordo = $rep->novoTransbordo($post, 2022);

        $ret = $rep->adicionaCaixa($transbordo, self::CAIXA_INICIAL, self::CAIXA_FINAL);
        $qtd_disp_antes = \Endereco::get_qtdDisponivel($transbordo->id_tipodocumento);
        $ret = $rep->criaReserva($transbordo, 2022);
        $qtd_disp_depois = \Endereco::get_qtdDisponivel($transbordo->id_tipodocumento);
        $this->assertEquals($qtd_disp_depois + 10, $qtd_disp_antes);
        
        $transbordox = \Transbordo::find($transbordo->id_transbordo);
        $this->assertNotEmpty($transbordox->id_reserva);
        $reserva = \Reserva::find($transbordox->id_reserva);
        $this->assertNotEmpty($reserva);
        $this->assertEquals(10, $reserva->reservadesmembradas()->count());
        
        $transbs = \TransbordoCaixa::where('id_transbordo', '=', $transbordo->id_transbordo)->get();
        foreach($transbs as $transb){
            $this->assertNotEmpty($transb->id_caixa_depois);
            $this->assertNotEmpty($transb->id_caixapadrao_depois);
            $reservaD = \ReservaDesmembrada::where('id_reserva', '=', $reserva->id_reserva)
                    ->where('id_caixapadrao', '=', $transb->id_caixapadrao_depois)
                    ->first();
            $this->assertEquals($reservaD->id_caixa, $transb->id_caixa_depois);
        }
    }
    
    public function testCopiaDocumentos(){
        $rep = new \Memodoc\Repositorios\RepositorioTransbordoEloquent;
        $id_cliente_antes = 32;
        $id_cliente_depois = 242;
        $post = array(
            "id_cliente_antes" => $id_cliente_antes,
            "id_cliente_depois" => $id_cliente_depois,
            "observacao" => self::OBS_TEST
        );
        
        $qtd =\DepCliente::where('id_cliente', '=', $id_cliente_antes)->count();
        if ($qtd < 10){
            for ($i=$qtd;$i<=10;$i++){
                $novo = new \DepCliente;
                $novo->id_cliente = $id_cliente_antes;
                $novo->nome = $this->generate_random_string(10);
                $novo->save();
            }
        }
        $deps =\DepCliente::where('id_cliente', '=', $id_cliente_antes)->get()->toArray();
        
        $qtd =\SetCliente::where('id_cliente', '=', $id_cliente_antes)->count();
        if ($qtd < 10){
            for ($i=$qtd;$i<=10;$i++){
                $novo = new \SetCliente;
                $novo->id_cliente = $id_cliente_antes;
                $novo->nome = $this->generate_random_string(10);
                $novo->save();
            }
        }
        $sets =\SetCliente::where('id_cliente', '=', $id_cliente_antes)->get()->toArray();
        
        $qtd =  \CentroDeCusto::where('id_cliente', '=', $id_cliente_antes)->count();
        if ($qtd < 10){
            for ($i=$qtd;$i<=10;$i++){
                $novo = new \CentroDeCusto;
                $novo->id_cliente = $id_cliente_antes;
                $novo->nome = $this->generate_random_string(10);
                $novo->save();
            }
        }
        $centros =  \CentroDeCusto::where('id_cliente', '=', $id_cliente_antes)->get()->toArray();
        
        
        $docs = \Documento::where('id_cliente', '=', $id_cliente_antes)
                ->where('id_caixapadrao', '>=', self::CAIXA_INICIAL)
                ->where('id_caixapadrao', '<=', self::CAIXA_FINAL)
                ->orderBy('id_caixapadrao')
                ->orderBy('id_item')
                ->get();
        $id_caixapadrao = '';
        $qtd_items = 0;
        foreach($docs as $doc){
            if ($id_caixapadrao == ''){
                $id_caixapadrao = $doc->id_caixapadrao;
            }
            if ($id_caixapadrao != $doc->id_caixapadrao){
                if ($qtd_items == 1){
                    $docx = $doc_ant->replicate();
                    $docx->id_item++;
                    $docx->id_caixa = $docx->id_caixa;
                    $docx->id_setor = $sets[rand(0,9)]['id_setor'];
                    $docx->id_centro = $centros[rand(0,9)]['id_centrodecusto'];
                    $docx->id_departamento = $deps[rand(0,9)]['id_departamento'];
                    $docx->titulo = $this->generate_random_string(100);
                    $docx->conteudo = $this->generate_random_string(100);
                    $docx->inicio = $this->gen_random_data();
                    $docx->fim = $this->gen_random_data();
                    $docx->alfa_inicial = $this->generate_random_string(20);
                    $docx->alfa_final = $this->generate_random_string(20);
                    $docx->nume_inicial = $this->gen_random_num();
                    $docx->nume_final = $this->gen_random_num();
                    $docx->expurgarem = $this->gen_random_data();
                    $docx->primeira_entrada = $this->gen_random_data();
                    $docx->save();
                }
                $id_caixapadrao = $doc->id_caixapadrao;
                $qtd_items = 0;
            }
            $doc->id_setor = $sets[rand(0,9)]['id_setor'];
            $doc->id_centro = $centros[rand(0,9)]['id_centrodecusto'];
            $doc->id_departamento = $deps[rand(0,9)]['id_departamento'];
            $doc->titulo = $this->generate_random_string(100);
            $doc->conteudo = $this->generate_random_string(100);
            $doc->inicio = $this->gen_random_data();
            $doc->fim = $this->gen_random_data();
            $doc->alfa_inicial = $this->generate_random_string(20);
            $doc->alfa_final = $this->generate_random_string(20);
            $doc->nume_inicial = $this->gen_random_num();
            $doc->nume_final = $this->gen_random_num();
            $doc->expurgarem = $this->gen_random_data();
            $doc->primeira_entrada = $this->gen_random_data();
            $doc->save();
            $doc_ant = $doc;
            $qtd_items++;
        }
        
        
        $transbordo = $rep->novoTransbordo($post, 2022);

        $ret = $rep->adicionaCaixa($transbordo, self::CAIXA_INICIAL, self::CAIXA_FINAL);
        $reserva = $rep->criaReserva($transbordo, 2022);
        $ret = $rep->copiaCaixas($transbordo, 2022);

        $transbs = \TransbordoCaixa::where('id_transbordo', '=', $transbordo->id_transbordo)->get();
        foreach($transbs as $transb){
            $docs_antes = \Documento::where('id_cliente', '=', $transbordo->id_cliente_antes)
                                    ->where('id_caixapadrao', '=', $transb->id_caixapadrao_antes)->count();
            $docs_depois = \Documento::where('id_cliente', '=', $transbordo->id_cliente_depois)
                                    ->where('id_caixapadrao', '=', $transb->id_caixapadrao_depois)->count();
            $this->assertEquals($docs_antes, $docs_depois, "A quantidade de documentos não bateu");
            $docs_antes = \Documento::where('id_cliente', '=', $transbordo->id_cliente_antes)
                                    ->where('id_caixapadrao', '=', $transb->id_caixapadrao_antes)->orderBy('id_caixapadrao')->orderBy('id_item')->get();
            $cliente_antes = \Cliente::find($transbordo->id_cliente_antes);
            foreach ($docs_antes as $doc_antes){
                $doc_depois = \Documento::where('id_cliente', '=', $transbordo->id_cliente_depois)
                                    ->where('id_caixapadrao', '=', $transb->id_caixapadrao_depois)->where('id_item', '=', $doc_antes->id_item)->first();
                $this->assertEquals($doc_depois->id_tipodocumento,  $doc_antes->id_tipodocumento);
                $this->assertEquals($doc_depois->titulo, $doc_antes->titulo);
                $this->assertEquals($doc_depois->migrada_de, $doc_antes->migrada_de);
                $this->assertEquals($doc_depois->inicio, $doc_antes->inicio);
                $this->assertEquals($doc_depois->fim, $doc_antes->fim);
                $this->assertEquals($doc_depois->expurgo_programado, $doc_antes->expurgo_programado);
                $this->assertEquals($doc_depois->expurgarem, $doc_antes->expurgarem);
                $this->assertEquals(substr($doc_depois->conteudo, 1, strlen($doc_depois->conteudo) -20 ) , substr($doc_antes->conteudo."\n"."Atenção: Item transferido do item ".$doc_antes->id_item." da caixa ".$doc_antes->id_caixapadrao." do cliente (".$transbordo->id_cliente_antes.") ".$cliente_antes->razaosocial." em ".date('d/m/Y H:i:s'),1, strlen($doc_depois->conteudo) -20 ));
                $this->assertEquals($doc_depois->usuario, $doc_antes->usuario);
                $this->assertEquals($doc_depois->emcasa, $doc_antes->emcasa);
                //$this->assertEquals($doc_depois->primeira_entrada, date('Y-m-d H:i:s'));
                $this->assertEquals($doc_depois->data_digitacao, $doc_antes->data_digitacao);
                $this->assertEquals($doc_depois->itemalterado, $doc_antes->itemalterado);
                $this->assertEquals($doc_depois->planilha_eletronica, $doc_antes->planilha_eletronica);
                $this->assertEquals($doc_depois->request_recall, $doc_antes->request_recall);
                $this->assertEquals($doc_depois->nume_inicial, $doc_antes->nume_inicial);
                $this->assertEquals($doc_depois->nume_final, $doc_antes->nume_final);
                $this->assertEquals($doc_depois->alfa_inicial, $doc_antes->alfa_inicial);
                $this->assertEquals($doc_depois->alfa_final, $doc_antes->alfa_final);
                $this->assertEquals($doc_depois->usuario,  $doc_antes->usuario);
                $this->assertEquals($doc_depois->id_usuario_alteracao, 2022);
                $this->assertEquals($doc_depois->evento, "Transbordo");
                
            }
        }
    }
    public function testListaCaixasParaExpurgo(){
        $rep = new \Memodoc\Repositorios\RepositorioTransbordoEloquent;
        
        $post = array(
            "id_cliente_antes" => 32,
            "id_cliente_depois" => 242,
            "observacao" => self::OBS_TEST
        );
        $transbordo = $rep->novoTransbordo($post, 2022);

        $ret = $rep->adicionaCaixa($transbordo, self::CAIXA_INICIAL, self::CAIXA_FINAL);
        
        $id_caixas = \TransbordoCaixa::where('id_transbordo', '=', $transbordo->id_transbordo)->lists('id_caixapadrao_antes');
        
        $this->assertTrue(is_array($id_caixas));
        $this->assertEquals(10, count($id_caixas));       
    }
 
    public function testGeracaodoExpurgo(){
        $rep = new \Memodoc\Repositorios\RepositorioTransbordoEloquent;
        
        $post = array(
            "id_cliente_antes" => 32,
            "id_cliente_depois" => 242,
            "observacao" => self::OBS_TEST
        );
        $transbordo = $rep->novoTransbordo($post, 2022);

        $ret = $rep->adicionaCaixa($transbordo, self::CAIXA_INICIAL, self::CAIXA_FINAL);

        $rep->expurgaOriginais($transbordo, 2022);
        $transbordo = \Transbordo::find($transbordo->id_transbordo);
        $this->assertNotEmpty($transbordo->id_expurgo_pai);
        $exp_pai = \ExpurgoPai::find($transbordo->id_expurgo_pai);
        $this->assertEquals($transbordo->id_cliente_antes, $exp_pai->id_cliente);
        $this->assertEquals(1, $exp_pai->cortesia);
        
        $transbs = \TransbordoCaixa::where('id_transbordo', '=', $transbordo->id_transbordo)->get();
        foreach($transbs as $trans){
            $doc = \Documento::where('id_caixa', '=', $trans->id_caixa_antes)->first();
            $this->assertEmpty($doc);
            $exp = \Expurgo::where('id_expurgo_pai', '=', $transbordo->id_expurgo_pai)
                    ->where('id_caixa', '=', $trans->id_caixa_antes)
                    ->first();
            $this->assertNotEmpty($exp);
            $this->assertEquals($trans->id_caixapadrao_antes, $exp->id_caixapadrao);
        }        
    }
    
    public function tearDown(){
        parent::tearDown();
        $msg = self::OBS_TEST;
//        \TransbordoCaixa::whereIn('id_transbordo', function($query) use($msg) {
//                $query->select(array('id_transbordo'))
//                      ->from('transbordo')
//                      ->where('observacao','=', $msg);
//        })->delete();
//        \Transbordo::where('observacao','=', $msg)->delete();
    }
}