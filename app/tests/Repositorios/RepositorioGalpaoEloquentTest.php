<?php

use \Memodoc\Repositorios;

class RepositorioGalpaoEloquentTest extends \TestCase {

    /**
     * Verifica se não haverá checkin disponível para uma caixa não relacionada
     * @return [type] [description]
     */
    public function testCheckinDisponivelParaCaixaNaoRelacionada() {
        $this->markTestIncomplete();
        $rep = new Repositorios\RepositorioGalpaoEloquent;
        // Cria ambiente onde a caixa existe mas não existe um checkin disponível para ela.
        // Testa o checkin disponivel
        $id_caixa = 0;
        $rep->CheckinDisponivel($id_caixa);
    }

    /**
     * Verifica se haverá checkin disponível para caixa não relacionada
     * @return [type] [description]
     */
    public function testCheckinDisponivelParaCaixaRelacionada() {
        $this->markTestIncomplete();
        $rep = new Repositorios\RepositorioGalpaoEloquent;
        // Cria ambiente onde a caixa existe mas não existe um checkin disponível para ela.
        // Testa o checkin disponivel
        $id_caixa = 0;
        $rep->CheckinDisponivel($id_caixa);
    }

    /**
     * Verifica como o sistema registra o checkin da caixa
     * - deve alterar o status de todos os documentos da caixa
     * - deve registrar que o checkin da caixa na os foi recebido
     * - deve registrar quem recebeu o checkin da caixa na Os
     */
    public function testReceberCaixa() {
        $this->markTestIncomplete();
        $rep = new Repositorios\RepositorioGalpaoEloquent;
        // Cria ambiente onde a caixa existe mas não existe um checkin disponível para ela.
        // Testa o checkin disponivel
        $id_caixa = 0;
        $rep->ReceberCaixa($id_caixa);
    }

    public function testEmprestaCaixa(){
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $os = new \Os;
        $os->id_cliente = 37;
        $os->id_usuario_alteracao = 1577;
        $os->evento = "Teste";
        
        $os->save();

        \Documento::where('id_cliente', '=', 37)->where('id_caixapadrao', '=', 10)->update(array('emcasa' => 1));

        $doc = \Documento::where('id_cliente', '=', 37)->where('id_caixapadrao', '=', 10)->first();

        $qtdDoc = \Documento::where('id_cliente', '=', 37)->where('id_caixapadrao', '=', 10)->count();

        $empr = new \OsDesmembrada();
        $empr->id_os_chave = $os->id_os_chave;
        $empr->id_caixa = $doc->id_caixa;
        $empr->status = 0;
        $empr->save();
        $this->assertNotEmpty($empr->id_osdesmembrada);
//        var_dump($empr);

        $hora = date('Y-m-d H:i:s');
        $rep->emprestaCaixa($doc->id_caixa, 1405);

        $test = \OsDesmembrada::find($empr->id_osdesmembrada);
        print " id=".$test->id_osdesmembrada;
        $this->assertNotEmpty($test->id_osdesmembrada);
  //      var_dump($test);
        $this->assertGreaterThanOrEqual($hora, $test->data_checkout);
        $this->assertNotEmpty($test->id_usuario_checkout);
        $this->assertEquals(\OsDesmembrada::stATENDIDA, $test->status);

        // Confere se a quantidade de documentos emprestados bate com a quantidade de documentos da caixa
        $qtdEmpr = \Documento::where('id_cliente', '=', 37)->where('id_caixapadrao', '=', 10)->where('emcasa', '=', 0)->count();
        $this->assertEquals($qtdDoc, $qtdEmpr);

        if (isset($empr)){
            $empr->delete();
        }
        if (isset($os)){
            $os->delete();
        }
    }
    
    /**
     * Testa a criação de uma movimentação sem caixas
     */
    public function testEfetivaMovimentacaoErro_SemCaixas(){
        $id_caixas = array();
        $observacao = 'xxx';
        $id_usuario = 1577;
        $fl_cancelar_endereco = 0;
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $ret = $rep->efetivaMovimentacao($id_caixas, $observacao, $fl_cancelar_endereco, $id_usuario);
        $this->assertFalse($ret);
        $errors = $rep->getErrors();
        $this->assertGreaterThan(0, count($errors));
        $this->assertEquals('Informe pelo menos uma caixa para ser movimentada', $errors[0]);
    }
    
    /**
     * Testa a criação de uma movimentação sem caixas
     */
    public function testEfetivaMovimentacaoErro_TiposDiferentes(){
        $id_caixas = array(4621, 4201, 49561, 49562);
        $observacao = 'xxx';
        $id_usuario = 1577;
        $fl_cancelar_endereco = 0;
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $ret = $rep->efetivaMovimentacao($id_caixas, $observacao, $fl_cancelar_endereco, $id_usuario);
        $this->assertFalse($ret);
        $errors = $rep->getErrors();
        $this->assertGreaterThan(0, count($errors));
        $this->assertEquals('Somente documentos de um mesmo tipo podem ser migrados juntos', $errors[0]);
    }
    /**
     * Testa a criação de uma movimentação sem caixas
     */
    public function testEfetivaMovimentacaoErro_SemEspaco(){
        $this->markTestSkipped('Pulado para agilizar');
        $id_caixas = array();
        $i = 20000;
        while($i < 25000){
            $id_caixas[] = $i;
            $i++;
        }
        $observacao = 'xxx';
        $id_usuario = 1577;
        $fl_cancelar_endereco = 0;
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $ret = $rep->efetivaMovimentacao($id_caixas, $observacao, $fl_cancelar_endereco, $id_usuario);
        $this->assertFalse($ret);
        $errors = $rep->getErrors();
        $this->assertGreaterThan(0, count($errors));
        $this->assertEquals('Não há espaço disponível para reservar', substr($errors[0], 0, 42));
    }
    
    /**
     * Testa a criação de uma movimentação sem caixas
     */
    public function testEfetivaMovimentacaoErro_SemCancelar(){
        // Prepara teste
        $id_caixas = array(20000,20001,20002);
        $ends = \Endereco::whereIn('id_caixa', $id_caixas)->get();
        $observacao = 'xxx';
        $id_usuario = 1577;
        $fl_cancelar_endereco = 0;
        $qtdLivres_antes = \Endereco::get_qtdDisponivel(1);
        // Executa
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $ret = $rep->efetivaMovimentacao($id_caixas, $observacao, $fl_cancelar_endereco, $id_usuario);
        // Avalia se retornou uma movimentação
        $this->assertInstanceOf('Movimentacao', $ret);
        // Avalia se não deu erro nenhum
        $errors = $rep->getErrors();
        $this->assertEquals(0, count($errors));
        // Avalia se a quantidade de endereços disponíveis permaneceu a mesma
        $qtdLivres_depois = \Endereco::get_qtdDisponivel(1);
        $this->assertEquals($qtdLivres_antes, $qtdLivres_depois);
        // Avalia os endereços
        $endsNovo = array();
        foreach($id_caixas as $i => $id_caixa){
            $endNovo = \Endereco::where('id_caixa', '=', $id_caixa)->first();
            $endsNovo[] = $endNovo;
            $endAntigo = $ends[$i];
            $endNovoCompleto = $endNovo->enderecoCompleto();
            $endAntigoCompleto = $endAntigo->enderecoCompleto();
            // Avalia se o novo endereço é diferente do endereço original
            $this->assertNotEquals($endAntigoCompleto, $endNovoCompleto);
            // Avalia se o novo endereço está ocupado 
            $this->assertEquals(0, $endNovo->disponivel);
            $this->assertEquals(0, $endNovo->endcancelado);
            // Avalia se só existe uma caixa no endereço novo
            $endPeloEndNovo = \Endereco::where('id_galpao', '=', $endNovo->id_galpao)
                                    ->where('id_rua', '=', $endNovo->id_rua)
                                    ->where('id_predio', '=', $endNovo->id_predio)
                                    ->where('id_andar', '=', $endNovo->id_andar)
                                    ->where('id_unidade', '=', $endNovo->id_unidade)
                                    ->get();
            $this->assertEquals(1, count($endPeloEndNovo));
            // Avalia se só existe uma caixa no endereço antigo
            $endPeloEndAntigo = \Endereco::where('id_galpao', '=', $endAntigo->id_galpao)
                                    ->where('id_rua', '=', $endAntigo->id_rua)
                                    ->where('id_predio', '=', $endAntigo->id_predio)
                                    ->where('id_andar', '=', $endAntigo->id_andar)
                                    ->where('id_unidade', '=', $endAntigo->id_unidade)
                                    ->get();
            $this->assertEquals(1, count($endPeloEndAntigo));
            
        }
        // Garante que a quantidade de caixas movidas é a mesma da quantidade 
        // de caixas passadas para a rotina
        $caixas = \MovimentacaoCaixa::where('id_movimentacao', '=', $ret->id_movimentacao)->orderBy('id_caixa')->get();
        $this->assertEquals(count($id_caixas), count($caixas));
        foreach($caixas as $i => $caixa){
            $end = \Endereco::where('id_caixa', '=', $caixa->id_caixa)->first();
            // Garante que os endereços armazenados conferem com os endereços reais
            // (antes e depois)
            $this->assertEquals($caixa->endereco_antes, $ends[$i]->enderecoCompleto());
            $this->assertEquals($caixa->endereco_depois, $endsNovo[$i]->enderecoCompleto());
            $endTrocado = \Endereco::where('id_caixa', '=', $caixa->id_caixa_destino)->first();
            // Garante que os endereços do id origem e do id destino são diferentes
            $this->assertNotEquals($end->enderecoCompleto(), $endTrocado->enderecoCompleto());
            // Garante que o endereço trocado permanece disponível
            $this->assertEquals(1, $endTrocado->disponivel);
            $this->assertEquals(0, $endTrocado->endcancelado);
        }
    }
    /**
     * Testa a criação de uma movimentação sem caixas
     */
    public function testEfetivaMovimentacaoErro_Cancelando(){
        // Prepara teste
        $id_caixas = array(20000,20001,20002);
        $ends = \Endereco::whereIn('id_caixa', $id_caixas)->get();
        $observacao = 'xxx';
        $id_usuario = 1577;
        $fl_cancelar_endereco = 1;
        $qtdLivres_antes = \Endereco::get_qtdDisponivel(1);
        // Executa
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $ret = $rep->efetivaMovimentacao($id_caixas, $observacao, $fl_cancelar_endereco, $id_usuario);
        // Avalia se retornou uma movimentação
        $this->assertInstanceOf('Movimentacao', $ret);
        // Avalia se não deu erro nenhum
        $errors = $rep->getErrors();
        $this->assertEquals(0, count($errors));
        // Avalia se a quantidade de endereços disponíveis diminui de 3 unidades
        $qtdLivres_depois = \Endereco::get_qtdDisponivel(1) + 3;
        $this->assertEquals($qtdLivres_antes, $qtdLivres_depois);
        // Avalia os endereços
        $endsNovo = array();
        foreach($id_caixas as $i => $id_caixa){
            $endNovo = \Endereco::where('id_caixa', '=', $id_caixa)->first();
            $endsNovo[] = $endNovo;
            $endAntigo = $ends[$i];
            $endNovoCompleto = $endNovo->enderecoCompleto();
            $endAntigoCompleto = $endAntigo->enderecoCompleto();
            // Avalia se o novo endereço é diferente do endereço original
            $this->assertNotEquals($endAntigoCompleto, $endNovoCompleto);
            // Avalia se o novo endereço está ocupado 
            $this->assertEquals(0, $endNovo->disponivel);
            $this->assertEquals(0, $endNovo->endcancelado);
            // Avalia se só existe uma caixa no endereço novo
            $endPeloEndNovo = \Endereco::where('id_galpao', '=', $endNovo->id_galpao)
                                    ->where('id_rua', '=', $endNovo->id_rua)
                                    ->where('id_predio', '=', $endNovo->id_predio)
                                    ->where('id_andar', '=', $endNovo->id_andar)
                                    ->where('id_unidade', '=', $endNovo->id_unidade)
                                    ->get();
            $this->assertEquals(1, count($endPeloEndNovo));
            // Avalia se só existe uma caixa no endereço antigo
            $endPeloEndAntigo = \Endereco::where('id_galpao', '=', $endAntigo->id_galpao)
                                    ->where('id_rua', '=', $endAntigo->id_rua)
                                    ->where('id_predio', '=', $endAntigo->id_predio)
                                    ->where('id_andar', '=', $endAntigo->id_andar)
                                    ->where('id_unidade', '=', $endAntigo->id_unidade)
                                    ->get();
            $this->assertEquals(1, count($endPeloEndAntigo));
            
        }
        // Garante que a quantidade de caixas movidas é a mesma da quantidade 
        // de caixas passadas para a rotina
        $caixas = \MovimentacaoCaixa::where('id_movimentacao', '=', $ret->id_movimentacao)->orderBy('id_caixa')->get();
        $this->assertEquals(count($id_caixas), count($caixas));
        foreach($caixas as $i => $caixa){
            $end = \Endereco::where('id_caixa', '=', $caixa->id_caixa)->first();
            // Garante que os endereços armazenados conferem com os endereços reais
            // (antes e depois)
            $this->assertEquals($caixa->endereco_antes, $ends[$i]->enderecoCompleto());
            $this->assertEquals($caixa->endereco_depois, $endsNovo[$i]->enderecoCompleto());
            $endTrocado = \Endereco::where('id_caixa', '=', $caixa->id_caixa_destino)->first();
            // Garante que os endereços do id origem e do id destino são diferentes
            $this->assertNotEquals($end->enderecoCompleto(), $endTrocado->enderecoCompleto());
            // Garante que o está indisponível
            $this->assertEquals(0, $endTrocado->disponivel);
            $this->assertEquals(1, $endTrocado->endcancelado);
        }
    }
}
