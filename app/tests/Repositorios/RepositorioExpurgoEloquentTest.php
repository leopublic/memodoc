<?php

class RepositorioExpurgoEloquentTest extends \TestCase {

    /**
     * Verifica que a quantidade de documentos após o expurgo está correta
     *
     * Quando expurga uma caixa:
     *  - Os documentos são excluídos e copiados para DocumentoCancelado
     */
    public function testCrie_qtdOk() {
        $cliente = RepositorioClienteEloquentTest::novoClienteAtivo();
        $repReserva = new \Memodoc\Repositorios\RepositorioReservaEloquent;

        $post = array(
            "id_cliente" => $cliente->id_cliente,
            "caixas" => 3,
            "id_tipodocumento" => 1,
        );

        $reserva = $repReserva->criar($post, 1450);
        $qtdEnderecos = \Endereco::where('id_cliente', '=', $cliente->id_cliente)->count();
        $qtdDoc = \Documento::where('id_cliente', '=', $cliente->id_cliente)->count();
        $this->assertEquals(3, $qtdEnderecos);
        $this->assertEquals(3, $qtdDoc);
        $caixa = $reserva->ultimaCaixa();
        $this->assertNotEmpty($caixa);
        $repExpurgo = new \Memodoc\Repositorios\RepositorioExpurgoEloquent;
        $repExpurgo->crie($cliente, $caixa, 1450, date('d/m/Y'));

        // Ceritifica que a quantidade de documentos e caixas diminuiu
        $qtdEnderecosDep = \Endereco::where('id_cliente', '=', $cliente->id_cliente)->where('endcancelado', '=', 0)->count();
        $qtdDocDep = \Documento::where('id_cliente', '=', $cliente->id_cliente)->count();
        $qtdDocCancelado = \DocumentoCancelado::where('id_cliente', '=', $cliente->id_cliente)->count();
        $this->assertEquals(2, $qtdEnderecosDep);
        $this->assertEquals(2, $qtdDocDep);
        $this->assertEquals(1, $qtdDocCancelado);

        //Limpeza!!!
        \Documento::where('id_cliente', '=', $cliente->id_cliente)->delete();
        \DocumentoCancelado::where('id_cliente', '=', $cliente->id_cliente)->delete();
        \Endereco::where('id_cliente', '=', $cliente->id_cliente)
                    ->update(array('disponivel' => 1, 'reservado' => 0, "reserva" => 0, "id_cliente"=> null) );
        \Expurgo::where('id_cliente', '=', $cliente->id_cliente)->delete();
        $desm = $reserva->reservadesmembradas()->get();
        foreach($desm as $desmembrada){
            $desmembrada->delete();
        }
        $reserva->delete();
        $cliente->delete();
    }
    /**
     * Verifica que a reserva feita após um expurgo vai pular a etiqueta cancelada
     */
    public function testCrie_numeroPuladoOk() {
        $cliente = RepositorioClienteEloquentTest::novoClienteAtivo();
        $repReserva = new \Memodoc\Repositorios\RepositorioReservaEloquent;

        $post = array(
            "id_cliente" => $cliente->id_cliente,
            "caixas" => 3,
            "id_tipodocumento" => 1,
        );

        $reserva = $repReserva->criar($post, 1450);
        $qtdEnderecos = \Endereco::where('id_cliente', '=', $cliente->id_cliente)->count();
        $qtdCaixas = \Documento::where('id_cliente', '=', $cliente->id_cliente)->count();
        $this->assertEquals(3, $qtdEnderecos);
        $this->assertEquals(3, $qtdCaixas);
        $caixa = $reserva->ultimaCaixa();
        $this->assertNotEmpty($caixa);
        $repExpurgo = new \Memodoc\Repositorios\RepositorioExpurgoEloquent;
        $repExpurgo->crie($cliente, $caixa, 1450, date('d/m/Y'));

        // Cria um novo endereço para ter certeza que vai pular o excluído
        $postx = array(
            "id_cliente" => $cliente->id_cliente,
            "caixas" => 1,
            "id_tipodocumento" => 1,
        );

        $reserva2 = $repReserva->criar($postx, 1450);
        $caixaNova = $reserva2->ultimaCaixa();
        $this->assertEquals($caixa+1, $caixaNova);

        /**
         * Limpeza do banco!
         */
        \DocumentoCancelado::where('id_cliente', '=', $cliente->id_cliente)->delete();
        \Documento::where('id_cliente', '=', $cliente->id_cliente)->delete();
        \Endereco::where('id_cliente', '=', $cliente->id_cliente)->update(array('disponivel' => 1, 'reservado' => 0, "reserva" => 0, "id_cliente"=> null) );
        \Expurgo::where('id_cliente', '=', $cliente->id_cliente)->delete();
        $desm = $reserva->reservadesmembradas()->get();
        foreach($desm as $desmembrada){
            $desmembrada->delete();
        }
        $reserva->delete();
        $desm2 = $reserva2->reservadesmembradas()->get();
        foreach($desm2 as $desmembrada){
            $desmembrada->delete();
        }
        $reserva2->delete();
        $cliente->delete();
    }
}
