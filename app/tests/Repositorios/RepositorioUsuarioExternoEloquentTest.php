<?php

use \Memodoc\Repositorios;

class RepositorioUsuarioExternoEloquentTest extends \TestCase {
    protected $cliente;
    protected $usuario;
    protected $existente;
    protected $id_usuario;
    protected $senha;

    
    
    public function setUp(){
        parent::setUp();
        $this->ressetaExistente();
        $user = \Usuario::find(1762);        
        $this->be($user);
    }

    public function ressetaExistente(){
        $this->existente = new \Usuario;
        $this->existente->id_cliente = 2;
        $this->existente->id_cliente_principal = 2;
        $this->existente->nomeusuario= \TestCase::generate_random_string();
        $this->existente->username= \TestCase::generate_random_string().'@'.\TestCase::generate_random_string().'.com';
        $this->senha = \TestCase::generate_random_string();
        $this->existente->password = \Hash::make($this->senha);
        $this->existente->save();
        $this->id_usuario = $this->existente->id_usuario;        
    }
    public function usuariosProvider()
    {
        return array(
          array(0, '', 'sdsd', 'sdsdslk@gmail.com', 'windsurf', 'windsurf', 2, '', '', 'O cliente é obrigatório')
          ,array(0, 1, '', 'sdsdslk@gmail.com', 'windsurf', 'windsurf', 2, '', '', 'O nome do usuário é obrigatório')
          ,array(0, 1, 'woewoeiwo', '', 'windsurf', 'windsurf', 2, '', '', 'O login é obrigatório')
          ,array(0, 1, 'woewoeiwo', 'sdsds', 'windsurf', 'windsurf', 2, '', '', 'O login deve ser um e-mail válido')
          ,array(0, 1, 'woewoeiwo', 'sdsdslk@gmail.com', 'windsurf', 'windsurf', '', '', '', 'O nível do usuário é obrigatório')
          ,array(0, 1, 'woewoeiwo', 'sdsdslk@gmail.com', 'wind', '', '1', '', '', 'A senha deve ter entre 8 e 20 caracteres')
        );
    }    
    
    /**
     * @dataProvider usuariosProvider
     */
    public function testEditar_consistencia($id_usuario, $id_cliente, $nomeusuario, $username, $senha, $confirma, $nivel, $validadelogin, $aniversario, $excecao){
        $rep = new \Memodoc\Repositorios\RepositorioUsuarioExternoEloquent;
        
        $post = array(
            "id_usuario" => $id_usuario
            ,"id_cliente" => $id_cliente
            ,"nomeusuario" => $nomeusuario
            ,"username" => $username
            ,"senha" => $senha
            ,"confirmacao" => $confirma
            ,"id_nivel" => $nivel
            ,"validadelogin" => $validadelogin
            ,"aniversario" => $aniversario
        );
        $this->usuario = $rep->edita($post);
        if (count($rep->getErrors()) > 0){
            $errors = $rep->getErrors();
            if (! is_array($errors)){
            	$msgs = $errors->all();
            } else {
            	$msgs = $errors;
            }
            $this->assertContains($excecao, $msgs );
        } else {
            $this->fail('Conseguiu gravar o registro com erro');                
        }
        if ($this->usuario){
        	$this->usuario->delete();
        }
    }

    public function testEditar_usernameExistente(){
        $this->testEditar_consistencia(0, 2, 'wewewe', $this->existente->username, 'windsurf', 'windsurf', 1, '', '', 'Já existe outro usuário com o login escolhido');
    }
    
    public function testEditar_camposSalvos_novo(){
        $rep = new \Memodoc\Repositorios\RepositorioUsuarioExternoEloquent;
        
        $nomeusuario = \TestCase::generate_random_string();
        $username = \TestCase::generate_random_string().'@'.\TestCase::generate_random_string().'.com';
        $senha = \TestCase::generate_random_string();
        $confirma = $senha;
        $nivel = '1';
        $validadelogin = rand(100,999);
        
        $post = array(
            "id_usuario" => 0
            ,"id_cliente" => '2'
            ,"nomeusuario" => $nomeusuario
            ,"username" => $username
            ,"senha" => $senha
            ,"confirmacao" => $confirma
            ,"id_nivel" => $nivel
            ,"validadelogin" => $validadelogin
            ,"aniversario" => ''
        );
        $this->usuario = $rep->edita($post);
        $id_usuario = $this->usuario->id_usuario;
        
        $usuario = \Usuario::find($id_usuario);
        $this->assertEquals(2, $usuario->id_cliente);
        $this->assertEquals(2, $usuario->id_cliente_principal);
        $this->assertEquals($nomeusuario, $usuario->nomeusuario);
        $this->assertEquals($username, $usuario->username);
        $this->assertTrue(\Hash::check($senha, $usuario->password));
        $this->assertEquals(1, $usuario->id_nivel);
        $this->assertEquals(0, $usuario->adm);
        $this->assertEquals($validadelogin, $usuario->validadelogin);
        $usuario->delete();
    }
    
    public function testEditar_camposSalvos_novoAdm(){
        $rep = new \Memodoc\Repositorios\RepositorioUsuarioExternoEloquent;
        
        $nomeusuario = \TestCase::generate_random_string();
        $username = \TestCase::generate_random_string().'@'.\TestCase::generate_random_string().'.com';
        $senha = \TestCase::generate_random_string();
        $confirma = $senha;
        $nivel = '1';
        $validadelogin = rand(100,999);
        
        $post = array(
            "id_usuario" => 0
            ,"id_cliente" => '-1'
            ,"nomeusuario" => $nomeusuario
            ,"username" => $username
            ,"senha" => $senha
            ,"confirmacao" => $confirma
            ,"id_nivel" => $nivel
            ,"validadelogin" => $validadelogin
            ,"aniversario" => ''
        );
        $this->usuario = $rep->edita($post);
        $id_usuario = $this->usuario->id_usuario;
        
        $usuario = \Usuario::find($id_usuario);
        $this->assertEquals('', $usuario->id_cliente);
        $this->assertEquals('', $usuario->id_cliente_principal);
        $this->assertEquals($nomeusuario, $usuario->nomeusuario);
        $this->assertEquals($username, $usuario->username);
        $this->assertTrue(\Hash::check($senha, $usuario->password));
        $this->assertEquals(1, $usuario->id_nivel);
        $this->assertEquals(1, $usuario->adm);
        $this->assertEquals($validadelogin, $usuario->validadelogin);
        $usuario->delete();
    }
    
    public function testEditar_camposSalvos_existente(){
        $rep = new \Memodoc\Repositorios\RepositorioUsuarioExternoEloquent;
        
        $this->ressetaExistente();
        $nomeusuario = \TestCase::generate_random_string();
        $username = \TestCase::generate_random_string().'@'.\TestCase::generate_random_string().'.com';
        $senha = \TestCase::generate_random_string();
        $confirma = $senha;
        $nivel = '1';
        $validadelogin = rand(100,999);
        
        $post = array(
            "id_usuario" => $this->existente->id_usuario
            ,"nomeusuario" => $nomeusuario
            ,"username" => $username
            ,"senha" => $senha
            ,"confirmacao" => $confirma
            ,"id_nivel" => $nivel
            ,"validadelogin" => $validadelogin
            ,"aniversario" => ''
            ,"aniversario_dia" => '11'
            ,"aniversario_mes" => '12'
        );
        $this->usuario = $rep->edita($post);
        
        $usuario = \Usuario::find($this->existente->id_usuario);
        $this->assertEquals($this->existente->id_cliente, $usuario->id_cliente);
        $this->assertEquals($this->existente->id_cliente_principal, $usuario->id_cliente_principal);
        $this->assertEquals($nomeusuario, $usuario->nomeusuario);
        $this->assertEquals($username, $usuario->username);
        $this->assertTrue(\Hash::check($senha, $usuario->password));
        $this->assertEquals(1, $usuario->id_nivel);
        $this->assertEquals($validadelogin, $usuario->validadelogin);
        $this->assertEquals(11, $usuario->aniversario_dia);
        $this->assertEquals(12, $usuario->aniversario_mes);
        $this->usuario->delete();
    }
    
    public function testEditar_camposSalvos_senhaembranco(){
        $rep = new \Memodoc\Repositorios\RepositorioUsuarioExternoEloquent;
        
        $this->ressetaExistente();
        $nomeusuario = \TestCase::generate_random_string();
        $username = \TestCase::generate_random_string().'@'.\TestCase::generate_random_string().'.com';
        $nivel = '1';
        $validadelogin = rand(100,999);
        
        $post = array(
            "id_usuario" => $this->existente->id_usuario
            ,"nomeusuario" => $nomeusuario
            ,"username" => $username
            ,"senha" => ''
            ,"confirmacao" => ''
            ,"id_nivel" => $nivel
            ,"validadelogin" => $validadelogin
            ,"aniversario" => ''
        );
        $this->usuario = $rep->edita($post);
        
        $usuario = \Usuario::find($this->existente->id_usuario);
        $this->assertEquals($this->existente->id_cliente, $usuario->id_cliente);
        $this->assertEquals($this->existente->id_cliente_principal, $usuario->id_cliente_principal);
        $this->assertEquals($nomeusuario, $usuario->nomeusuario);
        $this->assertEquals($username, $usuario->username);
        $this->assertTrue(\Hash::check($this->senha, $usuario->password));
        $this->assertEquals(1, $usuario->id_nivel);
        $this->assertEquals($validadelogin, $usuario->validadelogin);
        $this->usuario->delete();
    }
    
    
    public function testEditar_camposSalvos_existente_sem_senha(){
        $rep = new \Memodoc\Repositorios\RepositorioUsuarioExternoEloquent;
        $this->ressetaExistente();
        
        $nomeusuario = \TestCase::generate_random_string();
        $username = \TestCase::generate_random_string().'@'.\TestCase::generate_random_string().'.com';
        $nivel = '1';
        $validadelogin = rand(100,999);
        
        $post = array(
            "id_usuario" => $this->existente->id_usuario
            ,"nomeusuario" => $nomeusuario
            ,"username" => $username
            ,"id_nivel" => $nivel
            ,"senha" => ''
        	,"validadelogin" => $validadelogin
            ,"aniversario" => ''
        );
        $usuario_antes = \Usuario::find($this->existente->id_usuario);
        $this->usuario = $rep->edita($post);
        
        $usuario = \Usuario::find($this->existente->id_usuario);
        $this->assertEquals($this->existente->id_cliente, $usuario->id_cliente);
        $this->assertEquals($this->existente->id_cliente_principal, $usuario->id_cliente_principal);
        $this->assertEquals($nomeusuario, $usuario->nomeusuario);
        $this->assertEquals($username, $usuario->username);
        $this->assertEquals($usuario_antes->password, $usuario->password);
        $this->assertNotEmpty($usuario->password);
        $this->assertEquals($nivel, $usuario->id_nivel);
        $this->assertEquals($validadelogin, $usuario->validadelogin);
        $this->usuario->delete();
    }
    
    public function testSalvaDoCliente_camposSalvos_novo(){
        $rep = new \Memodoc\Repositorios\RepositorioUsuarioExternoEloquent;
        
        $nomeusuario = \TestCase::generate_random_string();
        $username = \TestCase::generate_random_string().'@'.\TestCase::generate_random_string().'.com';
        $senha = \TestCase::generate_random_string();
        $confirma = $senha;
        $nivel = '1';
        $validadelogin = rand(100,999);
        
        $post = array(
            "id_usuario" => 0
            ,"id_cliente" => '2'
            ,"nomeusuario" => $nomeusuario
            ,"username" => $username
            ,"senha" => $senha
            ,"id_nivel" => $nivel
            ,"validadelogin" => $validadelogin
        );
        $this->usuario = $rep->salvaDoCliente($post);
        $id_usuario = $this->usuario->id_usuario;
        
        $usuario = \Usuario::find($id_usuario);
        $this->assertEquals(2, $usuario->id_cliente);
        $this->assertEquals(2, $usuario->id_cliente_principal);
        $this->assertEquals($nomeusuario, $usuario->nomeusuario);
        $this->assertEquals($username, $usuario->username);
        $this->assertTrue(\Hash::check($senha, $usuario->password));
        $this->assertEquals(1, $usuario->id_nivel);
        $this->assertEquals(0, $usuario->adm);
        $this->assertEquals($validadelogin, $usuario->validadelogin);
        $this->usuario->delete();
    }
    
    public function tearDown(){
    	if (isset($this->existente)){
    		$this->existente->delete();
    	}
    }
}
