<?php
use \Memodoc\Repositorios;
class RepositorioEnderecoEloquentTest extends \TestCase {

    public function testCrieClone(){
        \Endereco::where('id_galpao', '=', 8)->where('id_rua', '=', 9)->where('id_predio', '=', 10)->where('id_andar', '=', 11)->where('id_unidade', '=', 12)->delete();
        $end = new \Endereco;
        $end->id_galpao = 8;
        $end->id_rua = 9;
        $end->id_predio= 10;
        $end->id_andar = 11;
        $end->id_unidade = 12;
        $end->id_cliente= 47;
        $end->id_tipodocumento= 2;
        $end->disponivel= 0;
        $end->endcancelado= 1;
        $end->reservado= 1;
        $end->reserva= 1;
        $end->save();
        $rep = new \Memodoc\Repositorios\RepositorioEnderecoEloquent;

        $evento = 'Teste criacao clone';
        $id_usuario = 1350;
        $clone = $rep->crieClone($end, $id_usuario, $evento);

        $x = \Endereco::find($clone->id_caixa);

        $this->assertEquals($end->id_galpao, $x->id_galpao);
        $this->assertEquals($end->id_rua, $x->id_rua);
        $this->assertEquals($end->id_predio, $x->id_predio);
        $this->assertEquals($end->id_andar, $x->id_andar);
        $this->assertEquals($end->id_unidade, $x->id_unidade);
        $this->assertEquals($end->id_tipodocumento, $x->id_tipodocumento);
        $this->assertEquals(1, $x->disponivel);
        $this->assertEquals(0, $x->reserva);
        $this->assertEquals(0, $x->reservado);
        $this->assertEquals(0, $x->endcancelado);
        $this->assertNull($x->id_cliente);
        $this->assertEquals($id_usuario, $x->id_usuario_criacao);
        $this->assertEquals($evento, $x->evento);

        $qtd = DB::select(DB::raw('select count(*) qtd from Endereco where id_galpao=8 and id_rua=9 and id_predio = 10 and id_andar =11 and id_unidade=12 '));
        $this->assertEquals(2, $qtd[0]->qtd);
        try{
            $end->delete();
            $x->delete();
        } catch (Exception $ex) {

        }
    }

    public function testExclua(){
         \Endereco::where('id_galpao', '=', 8)->where('id_rua', '=', 9)->where('id_predio', '=', 10)->where('id_andar', '=', 11)->where('id_unidade', '=', 12)->delete();
        $end = new \Endereco;
        $end->id_galpao = 8;
        $end->id_rua = 9;
        $end->id_predio= 10;
        $end->id_andar = 11;
        $end->id_unidade = 12;
        $end->id_cliente= 47;
        $end->id_tipodocumento= 2;
        $end->disponivel= 0;
        $end->endcancelado= 1;
        $end->reservado= 1;
        $end->reserva= 1;
        $end->save();
        $rep = new \Memodoc\Repositorios\RepositorioEnderecoEloquent;

        $evento = 'Teste criacao clone';
        $id_usuario = 1350;

        $rep->exclua($end, $id_usuario, $evento);

        $x = \Endereco::where('id_galpao', '=', 8)->where('id_rua', '=', 9)->where('id_predio', '=', 10)->where('id_andar', '=', 11)->where('id_unidade', '=', 12)->first();
        $y = \EnderecoCancelado::where('id_galpao', '=', 8)->where('id_rua', '=', 9)->where('id_predio', '=', 10)->where('id_andar', '=', 11)->where('id_unidade', '=', 12)->first();

        $this->assertNull($x);
        $this->instanceOf('\EnderecoCancelado', $y);
        $this->assertEquals(8, $y->id_galpao);
        $this->assertEquals(9, $y->id_rua);
        $this->assertEquals(10, $y->id_predio);
        $this->assertEquals(11, $y->id_andar);
        $this->assertEquals(12, $y->id_unidade);
        $this->assertEquals($id_usuario, $y->id_usuario);
        $this->assertEquals($evento, $y->evento);
    }
}