<?php
use \Memodoc\Repositorios;
class RepositorioOsEloquentTest extends \TestCase {

    public function testNovaAtendimento(){
        $rep = new Repositorios\RepositorioOsEloquent();
        $os = $rep->novaAtendimento(37, 1450);
        
    }
    public function testAdicionaCaixa_Emprestada(){
        $user = \Usuario::find(1450);
        \Auth::setUser($user);
        $rep = new Repositorios\RepositorioOsEloquent();
        $os = $rep->novaAtendimento(37, 1450);
        $os->save();
        \Documento::where('id_cliente', '=', 37)->where('id_caixapadrao', '=', 10)->update(array('emcasa' => 0));
        $doc = \Documento::where('id_cliente', '=', 37)->where('id_caixapadrao', '=', 10)->first();
        try{
            $rep->adicionaCaixa($os, $doc);
            $this->fail();
        } catch (Exception $ex) {
            $this->assertEquals("Não é possível adicionar essa referência pois a mesma se encontra emprestada", $ex->getMessage());
        }
        $os->delete();
    }

    public function testRetiraCaixaEmprestimo(){
        $rep = new Repositorios\RepositorioOsEloquent();
        $os = $rep->novaAtendimento(37, 1450);
        $os->save();
    }
    
    public function testValorFrete(){
        
    }
}
