<?php

use \Memodoc\Repositorios;

class RepositorioIndiceReajusteEloquentTest extends \TestCase {

    public function testPreencheMesAno(){
        $rep = new \Memodoc\Repositorios\RepositorioIndiceReajusteEloquent;
        
        $mes = '11';
        $ano = '2099';

        \IndiceReajuste::where('mes', '=', $mes)->where('ano', '=', $ano)->delete();
        
        $encontrados = \IndiceReajuste::where('mes', '=', $mes)->where('ano', '=', $ano)->get();
        $this->assertEmpty($encontrados->count());
        
        $rep->preencheMesAno($mes, $ano);

        $indices = \Indice::all();

        $encontrados = \IndiceReajuste::where('mes', '=', $mes)->where('ano', '=', $ano)->get();
        $this->assertGreaterThan(0, $indices->count());
        
        $this->assertEquals($indices->count(), $encontrados->count());

        foreach($indices as $indice){
            $reajuste = \IndiceReajuste::where('mes', '=', $mes)
                            ->where('ano', '=', $ano)
                            ->where('id_indice', '=', $indice->id_indice)
                            ->first();
            $this->assertGreaterThan(0, $reajuste->id_indicereajuste);
        }
    }

    public function testPreencheCheio(){
        $rep = new \Memodoc\Repositorios\RepositorioIndiceReajusteEloquent;
        
        $mes = '11';
        $ano = '2099';

        \IndiceReajuste::where('mes', '=', $mes)->where('ano', '=', $ano)->delete();
        
        $encontrados = \IndiceReajuste::where('mes', '=', $mes)->where('ano', '=', $ano)->get();
        $this->assertEmpty($encontrados->count());
        
        $rep->preencheMesAno($mes, $ano);

        $indices = \Indice::all();

        foreach($indices as $indice){
            $reajuste = \IndiceReajuste::where('mes', '=', $mes)
                            ->where('ano', '=', $ano)
                            ->where('id_indice', '=', $indice->id_indice)
                            ->first();
            $reajuste->percentual = $reajuste->id_indicereajuste;
            $reajuste->save();
        }
        
        $encontrados = \IndiceReajuste::where('mes', '=', $mes)->where('ano', '=', $ano)->get();
        $this->assertGreaterThan(0, $indices->count());
        
        $rep->preencheMesAno($mes, $ano);

        foreach($indices as $indice){
            $reajuste = \IndiceReajuste::where('mes', '=', $mes)
                            ->where('ano', '=', $ano)
                            ->where('id_indice', '=', $indice->id_indice)
                            ->first();
            $this->assertEquals($reajuste->id_indicereajuste, $reajuste->percentual);
        }
        
    }
}
