<?php
use \Memodoc\Repositorios;
class RepositorioClienteEloquentTest extends \TestCase {

    /**
     * Cria um cliente ativo com razaosocial randomica
     * @param type $razaosocial
     * @return \Cliente
     */
    public static function novoClienteAtivo($razaosocial = ''){
        if ($razaosocial == ''){
            $razaosocial = self::generate_random_string(12);
        }
        $cliente = new Cliente;
        $cliente->fl_ativo = 1;
        $cliente->razaosocial = '_teste_'.$razaosocial;
        $cliente->save();
        return $cliente;
    }

    public function testInative(){
        $cliente = new Cliente;
        $cliente->fl_ativo = 1;
        $cliente->save();
        $id_cliente = $cliente->id_cliente;
        $rep = new Repositorios\RepositorioClienteEloquent;
        $rep->inative($cliente);
        $cli = Cliente::find($id_cliente);
        $this->assertEquals(0, $cli->fl_ativo);
    }

}
