<?php

class RepositorioDocumentoEloquentTest extends \TestCase {

    public function novoDoc($id_cliente, $id_caixapadrao, $id_caixa, $id_item){
        $doc = new \Documento;
        $doc->id_cliente           = $id_cliente;
        $doc->id_caixapadrao       = $id_caixapadrao;
        $doc->id_box               = 0;
        $doc->id_item              = $id_item;
        $doc->id_departamento      = 1;
        $doc->id_setor             = 2;
        $doc->id_centro            = 3;
        $doc->id_tipodocumento     = 1;
        $doc->id_caixa             = $id_caixa;
        $doc->id_reserva           = 1234;
        $doc->id_expurgo           = 0;
        $doc->titulo               = 'titulo';
        $doc->migrada_de           = 'xxx';
        $doc->inicio               = '2012-12-12';
        $doc->fim                  = '2011-11-11';
        $doc->expurgo_programado   = '1';
        $doc->expurgarem           = '2009-09-19';
        $doc->conteudo             = 'conteudo';
        $doc->usuario              = 1222;
        $doc->emcasa               = 0;
        $doc->primeira_entrada     = '2010-10-10 10:10:10';
        $doc->data_digitacao       = '2009-09-09';
        $doc->expurgada_em         = '2008-08-08';
        $doc->itemalterado         = 1;
        $doc->planilha_eletronica  = 1;
        $doc->request_recall       = 'xyz';
        $doc->nume_inicial         = '123';
        $doc->nume_final           = '456';
        $doc->alfa_inicial         = 'a';
        $doc->alfa_final           = 'd';
        return $doc;
    }

    public function testExclua_unico(){
        $doc = $this->novoDoc(37, 9999, 99999, 1);
        $doc->save();

        $res = DB::select(DB::raw('select count(*) qtd from Documento where id_cliente = 37 '));
        $qtdAnt = $res[0]->qtd;

        $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent();

        $evento = 'Teste criacao clone';
        $id_usuario = 1350;
        try{
            $rep->exclua($doc, $id_usuario, $evento);
            $this->fail('Não gerou exceção');
        } catch (Exception $ex) {
            $this->assertEquals($ex->getMessage(), 'O primeiro item da caixa não pode ser excluído pois isso vai expurgar a caixa');
        }
        if (isset($doc)){
            $doc->delete();
        }
    }

    public function testExclua_ok(){
        $doc = $this->novoDoc(37, 9999, 99999, 1);
        $doc->save();

        $id1 = $doc->id_documento;
        unset($doc->id_documento) ;
        $doc->exists = false;
        unset($doc->created_at);
        unset($doc->updated_at);
        $doc->id_item = 2;
        $doc->save();
        $id2 = $doc->id_documento;
        unset($doc->id_documento) ;
        $doc->exists = false;
        unset($doc->created_at);
        unset($doc->updated_at);
        $doc->id_item = 3;
        $doc->save();
        $id3 = $doc->id_documento;


        $res = DB::select(DB::raw('select count(*) qtd from Documento where id_cliente = 37 '));
        $qtdAnt = $res[0]->qtd;

        $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent();

        $evento = 'Teste criacao clone';
        $id_usuario = 1350;
        $rep->exclua($doc, $id_usuario, $evento);

        $canc = \DocumentoCancelado::find($id3);

        $this->assertInstanceOf('DocumentoCancelado', $canc);

        $this->assertEquals($doc->id_cliente          ,$canc->id_cliente          );
        $this->assertEquals($doc->id_caixapadrao      ,$canc->id_caixapadrao      );
        $this->assertEquals($doc->id_box              ,$canc->id_box              );
        $this->assertEquals($doc->id_item             ,$canc->id_item             );
        $this->assertEquals($doc->id_departamento     ,$canc->id_departamento     );
        $this->assertEquals($doc->id_setor            ,$canc->id_setor            );
        $this->assertEquals($doc->id_centro           ,$canc->id_centro           );
        $this->assertEquals($doc->id_tipodocumento    ,$canc->id_tipodocumento    );
        $this->assertEquals($doc->id_caixa            ,$canc->id_caixa            );
        $this->assertEquals($doc->id_reserva          ,$canc->id_reserva          );
        $this->assertEquals($doc->id_expurgo          ,$canc->id_expurgo          );
        $this->assertEquals($doc->titulo              ,$canc->titulo              );
        $this->assertEquals($doc->migrada_de          ,$canc->migrada_de          );
        $this->assertEquals($doc->inicio              ,$canc->inicio              );
        $this->assertEquals($doc->fim                 ,$canc->fim                 );
        $this->assertEquals($doc->expurgo_programado  ,$canc->expurgo_programado  );
        $this->assertEquals($doc->expurgarem          ,$canc->expurgarem          );
        $this->assertEquals($doc->conteudo            ,$canc->conteudo            );
        $this->assertEquals($doc->usuario             ,$canc->usuario             );
        $this->assertEquals($doc->emcasa              ,$canc->emcasa              );
        $this->assertEquals($doc->primeira_entrada    ,$canc->primeira_entrada    );
        $this->assertEquals($doc->data_digitacao      ,$canc->data_digitacao      );
        $this->assertEquals($doc->expurgada_em        ,$canc->expurgada_em        );
        $this->assertEquals($doc->itemalterado        ,$canc->itemalterado        );
        $this->assertEquals($doc->planilha_eletronica ,$canc->planilha_eletronica );
        $this->assertEquals($doc->request_recall      ,$canc->request_recall      );
        $this->assertEquals($doc->nume_inicial        ,$canc->nume_inicial        );
        $this->assertEquals($doc->nume_final          ,$canc->nume_final          );
        $this->assertEquals($doc->alfa_inicial        ,$canc->alfa_inicial        );
        $this->assertEquals($doc->alfa_final          ,$canc->alfa_final          );
        $this->assertEquals($doc->created_at          ,$canc->created_at_origin   );
        $this->assertEquals($doc->updated_at          ,$canc->updated_at_origin  );

        $res = DB::select(DB::raw('select count(*) qtd from Documento where id_cliente = 37 '));
        $qtdDepois = $res[0]->qtd;

        $this->assertEquals($qtdAnt, $qtdDepois + 1 );

        try{
            $doc->delete();
            $canc->delete();
        } catch (Exception $ex) {

        }
    }

    public function testExpurgue(){
        $cliente =  RepositorioClienteEloquentTest::novoClienteAtivo();

        $end = \Endereco::find(999999);
        if (!isset($end)){
            $end = new \Endereco();
            $end->id_caixa = 999999;
            $end->id_cliente = 37;
            $end->id_tipodocumento = 1;
            $end->save();
        }


        if (isset($cliente)){
            $cliente->delete();
        }
    }

    /**
     * @
     */
    public function testExcluaPeloEndereco(){
        $this->markTestSkipped();
        $end = \Endereco::find(999999);
        if (!isset($end)){
            $end = new \Endereco();
            $end->id_caixa = 999999;
            $end->id_cliente = 37;
            $end->id_tipodocumento = 1;
            $end->save();
        }
        \Documento::where('id_caixa', '=', 999999)->delete();

        $doc = $this->novoDoc(37, 9999, 999999, 1);
        $doc->save();

        $id1 = $doc->id_documento;
        unset($doc->id_documento) ;
        $doc->exists = false;
        unset($doc->created_at);
        unset($doc->updated_at);
        $doc->id_item = 2;
        $doc->save();
        $id2 = $doc->id_documento;
        unset($doc->id_documento) ;
        $doc->exists = false;
        unset($doc->created_at);
        unset($doc->updated_at);
        $doc->id_item = 3;
        $doc->save();
        $id3 = $doc->id_documento;

        $res = DB::select(DB::raw('select count(*) qtd from Documento where id_cliente = 37 '));
        $qtdAnt = $res[0]->qtd;

        $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent();

        $evento = 'Teste exclusao pelo endereco';
        $id_usuario = 1350;

        $rep->expurgue($end, $id_usuario, $evento);

        $canc = \DocumentoCancelado::find($id1);

        $this->assertInstanceOf('DocumentoCancelado', $canc);
        $this->assertEquals($doc->id_cliente          ,$canc->id_cliente          );
        $this->assertEquals($doc->id_caixapadrao      ,$canc->id_caixapadrao      );
        $this->assertEquals($doc->id_box              ,$canc->id_box              );
        $this->assertEquals(1             ,$canc->id_item             );
        $this->assertEquals($doc->id_departamento     ,$canc->id_departamento     );
        $this->assertEquals($doc->id_setor            ,$canc->id_setor            );
        $this->assertEquals($doc->id_centro           ,$canc->id_centro           );
        $this->assertEquals($doc->id_tipodocumento    ,$canc->id_tipodocumento    );
        $this->assertEquals($doc->id_caixa            ,$canc->id_caixa            );
        $this->assertEquals($doc->id_reserva          ,$canc->id_reserva          );
        $this->assertEquals($doc->id_expurgo          ,$canc->id_expurgo          );
        $this->assertEquals($doc->titulo              ,$canc->titulo              );
        $this->assertEquals($doc->migrada_de          ,$canc->migrada_de          );
        $this->assertEquals($doc->inicio              ,$canc->inicio              );
        $this->assertEquals($doc->fim                 ,$canc->fim                 );
        $this->assertEquals($doc->expurgo_programado  ,$canc->expurgo_programado  );
        $this->assertEquals($doc->expurgarem          ,$canc->expurgarem          );
        $this->assertEquals($doc->conteudo            ,$canc->conteudo            );
        $this->assertEquals($doc->usuario             ,$canc->usuario             );
        $this->assertEquals($doc->emcasa              ,$canc->emcasa              );
        $this->assertEquals($doc->primeira_entrada    ,$canc->primeira_entrada    );
        $this->assertEquals($doc->data_digitacao      ,$canc->data_digitacao      );
        $this->assertEquals($doc->expurgada_em        ,$canc->expurgada_em        );
        $this->assertEquals($doc->itemalterado        ,$canc->itemalterado        );
        $this->assertEquals($doc->planilha_eletronica ,$canc->planilha_eletronica );
        $this->assertEquals($doc->request_recall      ,$canc->request_recall      );
        $this->assertEquals($doc->nume_inicial        ,$canc->nume_inicial        );
        $this->assertEquals($doc->nume_final          ,$canc->nume_final          );
        $this->assertEquals($doc->alfa_inicial        ,$canc->alfa_inicial        );
        $this->assertEquals($doc->alfa_final          ,$canc->alfa_final          );
        $this->assertEquals($doc->created_at          ,$canc->created_at_origin   );
        $this->assertEquals($doc->updated_at          ,$canc->updated_at_origin  );
        $this->assertEquals($id_usuario   ,$canc->id_usuario);
        $this->assertEquals($evento          ,$canc->evento  );

        $canc = \DocumentoCancelado::find($id2);

        $this->assertInstanceOf('DocumentoCancelado', $canc);
        $this->assertEquals($doc->id_cliente          ,$canc->id_cliente          );
        $this->assertEquals($doc->id_caixapadrao      ,$canc->id_caixapadrao      );
        $this->assertEquals($doc->id_box              ,$canc->id_box              );
        $this->assertEquals(2             ,$canc->id_item             );
        $this->assertEquals($doc->id_departamento     ,$canc->id_departamento     );
        $this->assertEquals($doc->id_setor            ,$canc->id_setor            );
        $this->assertEquals($doc->id_centro           ,$canc->id_centro           );
        $this->assertEquals($doc->id_tipodocumento    ,$canc->id_tipodocumento    );
        $this->assertEquals($doc->id_caixa            ,$canc->id_caixa            );
        $this->assertEquals($doc->id_reserva          ,$canc->id_reserva          );
        $this->assertEquals($doc->id_expurgo          ,$canc->id_expurgo          );
        $this->assertEquals($doc->titulo              ,$canc->titulo              );
        $this->assertEquals($doc->migrada_de          ,$canc->migrada_de          );
        $this->assertEquals($doc->inicio              ,$canc->inicio              );
        $this->assertEquals($doc->fim                 ,$canc->fim                 );
        $this->assertEquals($doc->expurgo_programado  ,$canc->expurgo_programado  );
        $this->assertEquals($doc->expurgarem          ,$canc->expurgarem          );
        $this->assertEquals($doc->conteudo            ,$canc->conteudo            );
        $this->assertEquals($doc->usuario             ,$canc->usuario             );
        $this->assertEquals($doc->emcasa              ,$canc->emcasa              );
        $this->assertEquals($doc->primeira_entrada    ,$canc->primeira_entrada    );
        $this->assertEquals($doc->data_digitacao      ,$canc->data_digitacao      );
        $this->assertEquals($doc->expurgada_em        ,$canc->expurgada_em        );
        $this->assertEquals($doc->itemalterado        ,$canc->itemalterado        );
        $this->assertEquals($doc->planilha_eletronica ,$canc->planilha_eletronica );
        $this->assertEquals($doc->request_recall      ,$canc->request_recall      );
        $this->assertEquals($doc->nume_inicial        ,$canc->nume_inicial        );
        $this->assertEquals($doc->nume_final          ,$canc->nume_final          );
        $this->assertEquals($doc->alfa_inicial        ,$canc->alfa_inicial        );
        $this->assertEquals($doc->alfa_final          ,$canc->alfa_final          );
        $this->assertEquals($doc->created_at          ,$canc->created_at_origin   );
        $this->assertEquals($doc->updated_at          ,$canc->updated_at_origin  );
        $this->assertEquals($id_usuario   ,$canc->id_usuario);
        $this->assertEquals($evento          ,$canc->evento  );

        $canc = \DocumentoCancelado::find($id3);

        $this->assertInstanceOf('DocumentoCancelado', $canc);
        $this->assertEquals($doc->id_cliente          ,$canc->id_cliente          );
        $this->assertEquals($doc->id_caixapadrao      ,$canc->id_caixapadrao      );
        $this->assertEquals($doc->id_box              ,$canc->id_box              );
        $this->assertEquals(3             ,$canc->id_item             );
        $this->assertEquals($doc->id_departamento     ,$canc->id_departamento     );
        $this->assertEquals($doc->id_setor            ,$canc->id_setor            );
        $this->assertEquals($doc->id_centro           ,$canc->id_centro           );
        $this->assertEquals($doc->id_tipodocumento    ,$canc->id_tipodocumento    );
        $this->assertEquals($doc->id_caixa            ,$canc->id_caixa            );
        $this->assertEquals($doc->id_reserva          ,$canc->id_reserva          );
        $this->assertEquals($doc->id_expurgo          ,$canc->id_expurgo          );
        $this->assertEquals($doc->titulo              ,$canc->titulo              );
        $this->assertEquals($doc->migrada_de          ,$canc->migrada_de          );
        $this->assertEquals($doc->inicio              ,$canc->inicio              );
        $this->assertEquals($doc->fim                 ,$canc->fim                 );
        $this->assertEquals($doc->expurgo_programado  ,$canc->expurgo_programado  );
        $this->assertEquals($doc->expurgarem          ,$canc->expurgarem          );
        $this->assertEquals($doc->conteudo            ,$canc->conteudo            );
        $this->assertEquals($doc->usuario             ,$canc->usuario             );
        $this->assertEquals($doc->emcasa              ,$canc->emcasa              );
        $this->assertEquals($doc->primeira_entrada    ,$canc->primeira_entrada    );
        $this->assertEquals($doc->data_digitacao      ,$canc->data_digitacao      );
        $this->assertEquals($doc->expurgada_em        ,$canc->expurgada_em        );
        $this->assertEquals($doc->itemalterado        ,$canc->itemalterado        );
        $this->assertEquals($doc->planilha_eletronica ,$canc->planilha_eletronica );
        $this->assertEquals($doc->request_recall      ,$canc->request_recall      );
        $this->assertEquals($doc->nume_inicial        ,$canc->nume_inicial        );
        $this->assertEquals($doc->nume_final          ,$canc->nume_final          );
        $this->assertEquals($doc->alfa_inicial        ,$canc->alfa_inicial        );
        $this->assertEquals($doc->alfa_final          ,$canc->alfa_final          );
        $this->assertEquals($doc->created_at          ,$canc->created_at_origin   );
        $this->assertEquals($doc->updated_at          ,$canc->updated_at_origin  );
        $this->assertEquals($id_usuario   ,$canc->id_usuario);
        $this->assertEquals($evento          ,$canc->evento  );

        $res = DB::select(DB::raw('select count(*) qtd from Documento where id_cliente = 37 '));
        $qtdDepois = $res[0]->qtd;

        $this->assertEquals($qtdAnt, $qtdDepois + 3 );

        try{
            \Documento::where('id_caixa', '=', 999999)->delete();
            $canc->delete();
            $end->delete();
        } catch (Exception $ex) {

        }

    }
}