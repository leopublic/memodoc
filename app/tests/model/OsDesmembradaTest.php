<?php

class OsDesmembradaTest extends TestCase {

    public function testFillable() {
        $input = array('id_os_chave' => 10
            , 'id_caixa' => 20
            , 'status' => 30
            , 'data_checkout' => '24/10/2013 13:24:23'
            , 'id_usuario_inclusao' => 1
            , 'id_usuario_checkout' => 2
        );
        $osd = new \OsDesmembrada;
        $osd->fill($input);
        $this->assertEquals(10, $osd->id_os_chave);
        $this->assertEquals(20, $osd->id_caixa);
        $this->assertEquals(30, $osd->status);
        $this->assertEquals('24/10/2013 13:24:23', $osd->data_checkout);
        $this->assertEquals(1, $osd->id_usuario_inclusao);
        $this->assertEquals(2, $osd->id_usuario_checkout);
    }

    public function testSave() {
        $input = array('id_os_chave' => 10
            , 'id_caixa' => 20
            , 'status' => 30
            , 'data_checkout' => '2013-10-22 13:24:23'
            , 'id_usuario_inclusao' => 1
            , 'id_usuario_checkout' => 2
        );
        $osd = new \OsDesmembrada;
        $osd->fill($input);
        $this->assertEquals(10, $osd->id_os_chave);
        $this->assertEquals(20, $osd->id_caixa);
        $this->assertEquals(30, $osd->status);
        $this->assertEquals('2013-10-22 13:24:23', $osd->data_checkout);
        $this->assertEquals(1, $osd->id_usuario_inclusao);
        $this->assertEquals(2, $osd->id_usuario_checkout);
        $osd->save();

        $x = \OsDesmembrada::find($osd->id_osdesmembrada);
        $this->assertEquals('2013-10-22 13:24:23', $x->data_checkout);
    }

}
