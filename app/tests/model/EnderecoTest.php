<?php

class EnderecoTest extends TestCase {

    public function testget_qtdNuncaUtilizado() {
        // Obtem a quantidade de espacos disponiveis
        $qtd = Endereco::get_qtdNuncaUtilizado();
        $this->assertNotEmpty($qtd);
    }

    public function testget_qtdDisponivel() {
        // Obtem a quantidade de espacos disponiveis
        $qtd = Endereco::get_qtdDisponivel(1);
        $this->assertNotEmpty($qtd);
    }

    public function testget_proximoDisponivel() {
        // Obtem a quantidade de espacos disponiveis
        $enderecos = Endereco::get_cursorDisponiveis(1)->get();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $enderecos);
    }

    public function testget_proximoDisponivel_Loop() {
        // Obtem a quantidade de espacos disponiveis
        $enderecos = Endereco::get_cursorDisponiveis(1)->get();
        $endereco = $enderecos[0];
        $this->assertNotEmpty($endereco);
        $enderecoCompleto = $endereco->enderecoCompleto();
        $this->assertNotEmpty($enderecoCompleto);
        $endereco = $enderecos[1];
        $enderecoCompletox = $endereco->enderecoCompleto();
        $this->assertNotEquals($enderecoCompleto, $enderecoCompletox);
    }

    public function testeEnderecoCompleto() {
        $endereco = new Endereco;
        $endereco->id_galpao = 2;
        $endereco->id_rua = 3;
        $endereco->id_predio = 4;
        $endereco->id_andar = 5;
        $endereco->id_unidade = 6;
        $this->assertEquals('2.03.04.05.0006', $endereco->enderecoCompleto());
    }
}