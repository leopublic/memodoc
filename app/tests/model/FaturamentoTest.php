<?php
class FaturamentoTest extends TestCase {


    public function testDtInicioQuery() {
        $perfat = new \PeriodoFaturamento;
        $perfat->dt_inicio = '2014-02-10';
        $perfat->dt_fim = '2014-03-02';
        $perfat->save();
        $fat = new \Faturamento;
        $fat->id_periodo_faturamento = $perfat->id_periodo_faturamento;
        $fat->save();
        $this->assertEquals('2014-02-09', $fat->dt_inicio_query);
        $this->assertEquals('2014-03-03', $fat->dt_fim_query);

        $fat->delete();
        $perfat->delete();
    }
}