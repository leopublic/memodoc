<?php

class DocumentoTest extends TestCase {

	public function testAssumaProximoNumeroDisponivel_existente()
	{
		$cliente = \Cliente::find(8);
		$doc = new Documento();
		$doc->id_cliente = 8;
		$doc->AssumaProximoNumeroDisponivel();
		$this->assertNotEmpty($doc->id_caixapadrao);
	}

    public function testGetMaiorNumeroDisponivel(){
        $doc = new \Documento;
        $x = $doc->getMaiorCaixaNaoCancelada(37);
        print "Maior caixa disponivel = ".$x;
    }

    public function testScopeTituloConteudo(){
        $termo1 = self::generate_random_string();
        $termo2 = self::generate_random_string();
        $termo3 = self::generate_random_string();
        $termo4 = self::generate_random_string();
        $termo5 = self::generate_random_string();
        $termo6 = self::generate_random_string();

        $caixa = \Documento::where('id_cliente', '=', 37)->first();

        $doc = new \Documento;
        $doc->id_cliente = 37;
        $doc->id_caixa = $caixa->id_caixa;
        $doc->id_caixapadrao = $caixa->id_caixapadrao;
        $doc->id_tipodocumento = 1;
        $doc->titulo = $termo1 & " " & $termo2 & " " & $termo3;
        $doc->conteudo = $termo4 & " " & $termo5 & " " & $termo6;
        $doc->save();

        $doc1 = new \Documento;
        $doc1->id_cliente = 37;
        $doc1->id_caixa = $caixa->id_caixa;
        $doc1->id_caixapadrao = $caixa->id_caixapadrao;
        $doc1->id_tipodocumento = 1;
        $doc1->titulo = $termo4 & " " & $termo5 & " " & $termo6;
        $doc1->conteudo = $termo1 & " " & $termo2 & " " & $termo3;
        $doc1->save();

        $doc2 = new \Documento;
        $doc2->id_cliente = 37;
        $doc2->id_caixa = $caixa->id_caixa;
        $doc2->id_caixapadrao = $caixa->id_caixapadrao;
        $doc2->id_tipodocumento = 1;
        $doc2->titulo = $termo1 ;
        $doc2->conteudo = $termo4;
        $doc2->save();

        $doc3 = new \Documento;
        $doc3->id_cliente = 37;
        $doc3->id_caixa = $caixa->id_caixa;
        $doc3->id_caixapadrao = $caixa->id_caixapadrao;
        $doc3->id_tipodocumento = 1;
        $doc3->titulo = $termo2 & " " & $termo3;
        $doc3->conteudo = $termo5 & " " & $termo1;
        $doc3->save();

        $doc4 = new \Documento;
        $doc4->id_cliente = 37;
        $doc4->id_caixa = $caixa->id_caixa;
        $doc4->id_caixapadrao = $caixa->id_caixapadrao;
        $doc4->id_tipodocumento = 1;
        $doc4->titulo = $termo1 & " " & $termo2 ;
        $doc4->conteudo = $termo4 & " " & $termo5 ;
        $doc4->save();

        $doc5 = new \Documento;
        $doc5->id_cliente = 37;
        $doc5->id_caixa = $caixa->id_caixa;
        $doc5->id_caixapadrao = $caixa->id_caixapadrao;
        $doc5->id_tipodocumento = 1;
        $doc5->titulo =  $termo3;
        $doc5->conteudo =  $termo6;
        $doc5->save();

        $doc6 = new \Documento;
        $doc6->id_cliente = 37;
        $doc6->id_caixa = $caixa->id_caixa;
        $doc6->id_caixapadrao = $caixa->id_caixapadrao;
        $doc6->id_tipodocumento = 1;
        $doc6->titulo = $termo2 & " " & $termo3 & " " & $termo1;
        $doc6->conteudo = $termo4 & " " & $termo5 & " " & $termo6;
        $doc6->save();


        $qtd1 = \Documento::doCliente(37)->tituloConteudo($termo1)->count();
        $this->assertEquals(6, $qtd1);
        $qtd2 = \Documento::tituloConteudo($termo1."+".$termo2)->count();
        $this->assertEquals(4, $qtd2);


        $doc->delete();
        $doc1->delete();
        $doc2->delete();
        $doc3->delete();
        $doc4->delete();
        $doc5->delete();
    }
}