<?php

class OsTest extends TestCase {


	public function testFillable()
	{
		$input = array('id_cliente' =>10
			, 'id_departamento' => 20
			, 'id_endeentrega' =>30
			, 'responsavel' => 40
			, 'solicitado_em' => '24/10/2013 13:24:23'
			, 'entregar_em' =>'22/10/2013'
			, 'entrega_pela' =>1
			, 'tipodeemprestimo' =>2
			, 'qtdenovascaixas' =>120
			, 'apanhanovascaixas' =>55
			, 'apanhacaixasemprestadas' =>66
			, 'cobrafrete' =>1
			, 'observacao' =>'skjdskdjsdj'
			, 'urgente' =>1
			, 'documentos' => 10
			);
		$os = new Os;
		$os->fill($input);
		$this->assertEquals(10, $os->id_cliente);
		$this->assertEquals(20, $os->id_departamento);
		$this->assertEquals(30, $os->id_endeentrega);
		$this->assertEquals(40, $os->responsavel);
		$this->assertEquals('24/10/2013 13:24:23', $os->solicitado_em);
		$this->assertEquals('22/10/2013', $os->entregar_em);
		$this->assertEquals(1, $os->entrega_pela);
		$this->assertEquals(2, $os->tipodeemprestimo);
		$this->assertEquals(120, $os->qtdenovascaixas);
		$this->assertEquals(55, $os->apanhanovascaixas);
		$this->assertEquals(66, $os->apanhacaixasemprestadas);
		$this->assertEquals(1, $os->cobrafrete);
		$this->assertEquals('skjdskdjsdj', $os->observacao);
		$this->assertEquals(1 , $os->urgente);
		$this->assertEquals('' , $os->documentos);
	}

    public function testNumeroFormatado(){
        $os = new Os;
        $os->solicitado_em = '20/10/2013 13:10:23';
        $os->id_os = 45;
        $this->assertEquals('201310000045', $os->getNumeroFormatado());
    }

    public function testSolicitadoEm(){
        $os = new Os;
        $os->solicitado_em_data = '19/01/2014';
        $os->solicitado_em_hora = '18:45:22';
        $this->assertEquals('19/01/2014 18:45:22', $os->solicitado_em);

    }

    public function testTurnoDefault(){
        $cliente = Cliente::where('atendimento','=', 0)->first();
        $os = new \Os;
        $os->id_cliente = $cliente->id_cliente;
        $os->solicitado_em = '19/01/2014 09:00:00';
        $os->turnoDefault();
        $this->assertEquals('20/01/2014', $os->entregar_em);
        $this->assertEquals(0, $os->entrega_pela);

        $os->solicitado_em = '19/01/2014 17:00:00';
        $os->turnoDefault();
        $this->assertEquals('20/01/2014', $os->entregar_em);
        $this->assertEquals(1, $os->entrega_pela);

        $cliente = Cliente::where('atendimento','=', 1)->first();
        $os = new \Os;
        $os->id_cliente = $cliente->id_cliente;
        $os->solicitado_em = '19/01/2014 09:00:00';
        $os->turnoDefault();
        $this->assertEquals('19/01/2014', $os->entregar_em);
        $this->assertEquals(1, $os->entrega_pela);

        $os->solicitado_em = '19/01/2014 17:00:00';
        $os->turnoDefault();
        $this->assertEquals('20/01/2014', $os->entregar_em);
        $this->assertEquals(0, $os->entrega_pela);
    }

    public function testPodeCancelar(){
        $os = \Os::find(39560);
        $this->assertEquals(false, $os->podeCancelar());

    }
    
    public function testGetProximoNumero(){
        $cliente = new \Cliente;
        $cliente->razaosocial = 'zzz';
        $cliente->nomefantasia = 'xxcx';
        $cliente->save();
        $this->assertNotEmpty($cliente->id_cliente);
 
        $num = \Os::getProximoNumero($cliente->id_cliente);
        $this->assertEquals(1, $num);
    }

    public function testProximo_dia_util(){
        $os = new Os;
        $hoje = \Carbon::createFromFormat('Y-m-d', '2016-09-07');
        $x = $os->proximo_util($hoje);
        $this->assertEquals('2016-09-08', $x->format('Y-m-d'));
        $hoje = \Carbon::createFromFormat('Y-m-d', '2016-09-06');
        $x = $os->proximo_util($hoje);
        $this->assertEquals('2016-09-08', $x->format('Y-m-d'));

        $hoje = \Carbon::createFromFormat('Y-m-d', '2016-06-24');
        $x = $os->proximo_util($hoje);
        $this->assertEquals('2016-06-27', $x->format('Y-m-d'));

        $hoje = \Carbon::createFromFormat('Y-m-d', '2016-06-27');
        $x = $os->proximo_util($hoje);
        $this->assertEquals('2016-06-28', $x->format('Y-m-d'));

        $hoje = \Carbon::createFromFormat('Y-m-d', '2017-04-28');
        $x = $os->proximo_util($hoje);
        $this->assertEquals('2017-05-02', $x->format('Y-m-d'));
    }
}
