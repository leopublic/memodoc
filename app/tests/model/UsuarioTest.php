<?php

class UsuarioTest extends TestCase {

    public function testListaClientes_soUm(){
        $usuario = new \Usuario;

        $usuario->nomeusuario = \TestCase::generate_random_string();
        $usuario->id_cliente = 2;
        $usuario->id_cliente_principal = 2;
        $usuario->adm = 0;

        $usuario->save();

        $clientes = $usuario->getListaClientes()->lists('razaosocial', 'id_cliente');
        $this->assertEquals(1, count($clientes));
        $this->assertArrayHasKey(2, $clientes);
        $usuario->delete();
    }

    public function testListaClientes_soUmPrincipal(){
        $usuario = new \Usuario;

        $usuario->nomeusuario = \TestCase::generate_random_string();
        $usuario->id_cliente_principal = 2;
        $usuario->adm = 0;

        $usuario->save();

        $clientes = $usuario->getListaClientes()->lists('razaosocial', 'id_cliente');
        $this->assertEquals(1, count($clientes));
        $this->assertArrayHasKey(2, $clientes);
        $usuario->delete();
    }

    public function testListaClientes_soUmCliente(){
        $usuario = new \Usuario;

        $usuario->nomeusuario = \TestCase::generate_random_string();
        $usuario->id_cliente = 2;
        $usuario->adm = 0;

        $usuario->save();

        $clientes = $usuario->getListaClientes()->lists('razaosocial', 'id_cliente');
        $this->assertEquals(1, count($clientes));
        $this->assertArrayHasKey(2, $clientes);
        $usuario->delete();
    }

    public function testListaClientes_varios(){
        $usuario = new \Usuario;

        $usuario->nomeusuario = \TestCase::generate_random_string();
        $usuario->id_cliente = 2;
        $usuario->id_cliente_principal = 2;
        $usuario->adm = 0;
        $usuario->save();
        $usuario->clientes()->sync(array(4,5,6,7));
        $clientes = $usuario->getListaClientes()->lists('razaosocial', 'id_cliente');
        $this->assertEquals(5, count($clientes));
        $this->assertArrayHasKey(2, $clientes);
        $this->assertArrayHasKey(4, $clientes);
        $this->assertArrayHasKey(5, $clientes);
        $this->assertArrayHasKey(6, $clientes);
        $this->assertArrayHasKey(7, $clientes);
        $usuario->delete();
    }

    public function testListaClientes_adm(){
        $usuario = new \Usuario;

        $usuario->nomeusuario = \TestCase::generate_random_string();
        $usuario->id_cliente = 2;
        $usuario->adm = 1;
        $usuario->save();
        $clientes = $usuario->getListaClientes()->lists('razaosocial', 'id_cliente');
        $this->assertGreaterThan(5, count($clientes));
        $this->assertArrayHasKey(2, $clientes);
        $this->assertArrayHasKey(4, $clientes);
        $this->assertArrayHasKey(5, $clientes);
        $this->assertArrayHasKey(6, $clientes);
        $this->assertArrayHasKey(7, $clientes);
    }
}
