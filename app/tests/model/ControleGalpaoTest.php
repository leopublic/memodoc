<?php

class ControleGalpaoTest extends TestCase {

	/**
	 * @expectedException ExceptionSemEnderecoDisponivel
	 */
	public function testExceptionSemEndereco()
	{
		$reserva = new Reserva;
		$reserva->id_tipodocumento = 1;
		$reserva->caixas = 100000;
		ControleGalpao::ReservarEnderecos($reserva);
        $this->markTestIncomplete('Este teste ainda não foi implementado.');		
	}

	public function testReservar()
	{
		$qtdDispAntes = Endereco::get_qtdDisponivel();
		$doc = new Documento();
		$doc->id_cliente = 8;
		$doc->AssumaProximoNumeroDisponivel();
		$proxCaixaAntes = $doc->id_caixapadrao;

		$doc = new Documento();
		$doc->id_cliente = 10;
		$doc->AssumaProximoNumeroDisponivel();
		$proxCaixaAntes10 = $doc->id_caixapadrao;

		ControleGalpao::ReservarEnderecos(8, 10, 1);		

		$qtdDispDepois = Endereco::get_qtdDisponivel();
		$this->assertEquals($qtdDispAntes - 10, $qtdDispDepois);
		$doc = new Documento();
		$doc->id_cliente = 8;
		$doc->AssumaProximoNumeroDisponivel();
		$proxCaixaDepois = $doc->id_caixapadrao;

		$doc = new Documento();
		$doc->id_cliente = 10;
		$doc->AssumaProximoNumeroDisponivel();
		$proxCaixaDepois10 = $doc->id_caixapadrao;
	}
}