<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase {
    public function createApplication() {
        $unitTesting = true;
        $testEnvironment = 'testing';
        return require __DIR__ . '/../../bootstrap/start.php';
    }

    public static function gen_random_string($name_length = 8) {
        return $this->generate_random_string($name_length);
    }
    public static function generate_random_string($name_length = 8) {
        $alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        return substr(str_shuffle($alpha_numeric), 0, $name_length);
    }
    
    public static function gen_random_data($range = 500){
        \Carbon::now()->subDay(rand(0,$range));
    }
    
    public static function gen_random_num($digitos = 5){
        return rand(10 ^ ($digitos -1), 10 ^ ($digitos));
    }
}
