<?php

class PrecosTest extends TestCase {
    private $cliente;
    
    public function setUp(){
        //Recupera ou cria o cliente de teste
        $this->cliente = \Cliente::find(999999);
        if (!isset($this->cliente)){
            $this->cliente = new \Cliente;
            $this->cliente->razaosocial = 'Cliente de teste';
            $this->cliente->save();
        }
    }
    
    public function testClienteCriadoOk(){
        $this->assertInstanceOf('Cliente', $this->cliente);
    }
    
    public function testValorFrete(){
        $os = \Os::find(39423);
        $preco = new \Memodoc\Servicos\Precos($os->cliente);
        $preco->CarregaPrecos();
        print "\n Valor unitário frete: ".$preco->valorUnitarioFrete();
        print "\n Quantidade movimentada: ".$os->qtdCaixasMovimentadas();
        $calculadora = new \Memodoc\Servicos\CalculadoraOs($preco, $os);

        $valorFrete = $calculadora->valorFrete();
        print "\nValor total:".$valorFrete;
    }

    
    
}
