<?php

class RpsLayoutTest extends TestCase {
    public function testNumerico() {
        $x = 1234;
        $layout = new \Memodoc\Utilitarios\RpsLayout;
        $numero = $layout->numerico($x, 10);
        $this->assertEquals('0000001234', $numero);

        $x = '001.600.647-08';
        $layout = new \Memodoc\Utilitarios\RpsLayout;
        $numero = $layout->numerico($x, 15);
        $this->assertEquals('000000160064708', $numero);
    }

    public function testAtualizaCampo(){
        // $layout = new \Memodoc\Utilitarios\RpsLayout;
        // $layout->campos = [
        //     'campo1' => ['tipo' => 'N', 'tamanho' => '10', 'valor' => '']
        //     ,'campo2' => ['tipo' => 'V', 'tamanho' => '10', 'valor' => '']
        //     ,'campo3' => ['tipo' => 'T', 'tamanho' => '10', 'valor' => '']
        //     ,'campo4' => ['tipo' => 'D', 'tamanho' => '8', 'valor' => '']
        //     ]        ;
        // $layout->atualiza_campo('campo1', 20.03);
        // $layout->atualiza_campo('campo2', 20.03);
        // $layout->atualiza_campo('campo3', 'ABC');
        // $layout->atualiza_campo('campo4', '1968-09-15');
        // $this->assertEquals('0000002003', $layout->campos['campo1']['valor']);
        // $this->assertEquals('0000002003', $layout->campos['campo2']['valor']);
        // $this->assertEquals('ABC       ', $layout->campos['campo3']['valor']);
        // $this->assertEquals('19680915', $layout->campos['campo4']['valor']);
    }

    public function testCampoformatado(){
        $layout = new \Memodoc\Utilitarios\RpsLayout;
        $layout->campos = [
            'campoN' => ['tipo' => 'N', 'tamanho' => '10', 'valor' => '']
            ,'campoV' => ['tipo' => 'V', 'tamanho' => '10', 'valor' => '']
            ,'campoT' => ['tipo' => 'T', 'tamanho' => '10', 'valor' => '']            
        ];
        $layout->atualiza_campo('campoV', "20.03");
        $this->assertEquals('0000002003', $layout->campo_formatado('campoV'));
        $layout->atualiza_campo('campoV', "20.00");
        $this->assertEquals('0000002000', $layout->campo_formatado('campoV'));
        $layout->atualiza_campo('campoV', "20");
        $this->assertEquals('0000002000', $layout->campo_formatado('campoV'));
        $layout->atualiza_campo('campoV', "1");
        $this->assertEquals('0000000100', $layout->campo_formatado('campoV'));
        $layout->atualiza_campo('campoV', "0");
        $this->assertEquals('0000000000', $layout->campo_formatado('campoV'));
        
    }
}