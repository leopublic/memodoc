<?php

class CalculadoraCustodiaTest extends TestCase {

    public function testQtdCaixasMesAnterior() {
        $cliente = Cliente::find(37);
        $preco = new Memodoc\Servicos\Precos($cliente);
        $preco->CarregaPrecos();

        $faturamento = new \Faturamento;
        $faturamento->cliente()->associate($cliente);
        $faturamento->dt_inicio = '2013-10-21';
        $faturamento->dt_fim = '2013-11-20';

        $calculador = new Memodoc\Servicos\CalculadoraCustodia($preco, $faturamento );
        $qtd = $calculador->qtdCaixasMesAnterior();
        print 'qtd='.$qtd;

    }

}