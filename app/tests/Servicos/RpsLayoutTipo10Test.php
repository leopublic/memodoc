<?php

class RpsLayoutTest extends TestCase {
    public function testCarga() {
        $campos = [
            'tipo_documento_prestador' => '1',
            'documento_prestador' => '001.600.647.08',
            'insc_municipal_prestador' => '12-345-6789',
            'dt_inicio' => '20170701',
            'dt_fim' => '20170731'
        ];
        $layout = new \Memodoc\Utilitarios\RpsLayoutTipo10($campos);
        $this->assertEquals($campos['documento'], $layout->documento);
        $this->assertEquals($campos['insc_municipal'], $layout->insc_municipal);
        $this->assertEquals($campos['dt_inicio'], $layout->dt_inicio);
        $this->assertEquals($campos['dt_fim'], $layout->dt_fim);
    }
    public function testRegistro() {
        $campos = [
            'tipo_documento' => '1',
            'documento' => '001.600.647.08',
            'insc_municipal' => '12-345-6789',
            'dt_inicio' => '20170701',
            'dt_fim' => '20170731'
        ];
        $layout = new \Memodoc\Utilitarios\RpsLayoutTipo10($campos);
        $reg = '100031000001600647080000001234567892017070120170731'.chr(13).chr(10);
        $reg_gerado = $layout->registro();
        $this->assertEquals($reg, $reg_gerado);
        $this->assertEquals(53, strlen($reg_gerado));
    }
}