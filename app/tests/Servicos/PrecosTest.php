<?php

class PrecosTest extends TestCase {
    public function testCartonagem() {
        $precos = new Memodoc\Servicos\Precos(Cliente::find(37));
        $precos->CarregaPrecos();
        $this->assertEquals(5.15, $precos->valorUnitarioCartonagem());
        $this->assertEquals(5.15, $precos->valorUnitarioCartonagem(true));
    }

    public function testFrete() {
        $precos = new Memodoc\Servicos\Precos(Cliente::find(37));
        $precos->CarregaPrecos();
        $this->assertEquals(5.15, $precos->valorUnitarioFrete());
        $this->assertEquals(10.28, $precos->valorUnitarioFrete(true));
    }


    public function testSalvaPrecos(){
        $cliente = Cliente::find(37);
        $precos = new Memodoc\Servicos\Precos($cliente);
        $faturamento = new \Faturamento;
        $faturamento->cliente()->associate($cliente);
        $faturamento->save();
        $precos->SalvaPrecos($faturamento);

    }

    public function testCarregaPrecosCliente(){
        $cliente = Cliente::find(37);
        $precos = new Memodoc\Servicos\Precos($cliente);
        $precos->CarregaPrecos();

        $frete = $cliente->tipoprodcliente()->where('id_tipoproduto', '=', 1)->first();
        $this->assertEquals($frete->valor_contrato, $precos->valorUnitarioFrete());
        $this->assertEquals($frete->valor_urgencia, $precos->valorUnitarioFrete(true));
        $this->assertEquals($frete->minimo, $precos->qtdMinimoFrete());

    }

}
