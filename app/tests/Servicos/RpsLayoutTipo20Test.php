<?php

class RpsLayoutTest extends TestCase {
    public function testTamanho() {
        $campos = [];
        $layout = new \Memodoc\Utilitarios\RpsLayoutTipo20($campos);
        $reg_gerado = $layout->registro();
        $this->assertEquals(955, strlen($reg_gerado));
    }
    public function testRegistro() {
        $campos = [
            'tipo_documento_prestador' => '1',
            'documento_prestador' => '001.600.647.08',
            'insc_municipal_prestador' => '12-345-6789',
            'dt_inicio' => '20170701',
            'dt_fim' => '20170731'
        ];
        $layout = new \Memodoc\Utilitarios\RpsLayoutTipo10($campos);
        $reg = '100031000001600647080000001234567892017070120170731'.chr(13).chr(10);
        $reg_gerado = $layout->registro();
        $this->assertEquals($reg, $reg_gerado);
        $this->assertEquals(53, strlen($reg_gerado));
    }
}