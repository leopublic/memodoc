<?php

class AvisoController extends BaseController {

    public function getIndex() {
        $avisos = \Aviso::whereNull('id_cliente')->get();

        return View::make('aviso.home')
                        ->with('avisos', $avisos);
    }

    public function getHome($id_aviso = '') {
        if (intval($id_aviso) == 0) {
            $aviso = new \Aviso();
        } else {
            $aviso = \Aviso::find($id_aviso);
        }
        return View::make('aviso.home')
                        ->with('aviso', $aviso);
    }

    public function postHome() {
        $id_aviso = Input::get('id_aviso');
        if (intval($id_aviso) == 0) {
            $aviso = new \Aviso();
        } else {
            $aviso = \Aviso::find($id_aviso);
        }
        $aviso->texto = Input::get('texto');
        $aviso->texto_popup = Input::get('texto_popup');
        $aviso->observacao = Input::get('observacao');
        $aviso->dt_inicio_fmt = Input::get('dt_inicio_fmt');
        $aviso->dt_fim_fmt = Input::get('dt_fim_fmt');
        $aviso->fl_ativo = Input::get('fl_ativo');
        $aviso->fl_padrao = Input::get('fl_padrao');
        $aviso->save();
        Session::flash('success', 'Aviso atualizado com sucesso!');
        return Redirect::to('/aviso/home/' . $id_aviso);
    }

}
