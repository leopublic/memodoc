<?php

class UsuarioController extends BaseController {

    protected $repUsuarioExterno;
    protected $repUsuarioInterno;
    public function __construct() {
        $this->repUsuarioInterno = new \Memodoc\Repositorios\RepositorioUsuarioInternoEloquent;
        $this->repUsuarioExterno = new \Memodoc\Repositorios\RepositorioUsuarioExternoEloquent;
    }

    public function getInternos() {

        if (isset($_GET['search']) ) {
            $instancias = Usuario::internos()->search($_GET['search'])->paginate(20);
        } else {
            $instancias = Usuario::internos()->paginate(20);
        }

        Input::flash();

        $linkRemocao = null;
        $linkEdicao = null;
        $linkCriacao = null;
        if (\Auth::user()->id_nivel <= 13){
        	$linkRemocao = 'usuario/deleteinterno';
        	$linkEdicao = 'usuario/editarinterno';
        	$linkCriacao = 'usuario/novointerno';
        }

        return View::make('usuario_internos_list')
                        ->with('linkEdicao', $linkEdicao)
                        ->with('linkCriacao', $linkCriacao)
                        ->with('linkRemocao',$linkRemocao)
                        ->with('titulo', 'Usuários internos (administrativos Memodoc)')
                        ->with('instancias', $instancias)
        ;
    }

    public function getAlterarsenha() {
        $usuario = Auth::user();
        return View::make('usuario_senha')
                        ->with('model', $usuario)
        ;
    }

    public function postAlterarsenha() {
        $msg = '';
        $fields = Input::All();
        $br = '';
        if (!Hash::check($fields['password'], Auth::user()->password)) {
            $msg .= $br . "A senha atual está incorreta.";
            $br = '<br/>';
        }
        if ($fields['password_nova'] != $fields['password_nova_rep']) {
            $msg .= $br . "A repetição da senha não confere.";
            $br = '<br/>';
        }


        if ($msg != '') {
            $msg .= '<br/>Por favor tente novamente';
        }
        if ($msg == '') {
            Auth::user()->password = Hash::make($fields['password_nova']);
            Auth::user()->save();
            $msg = "Senha alterada com sucesso!";
            Session::flash('success', $msg);
            return Redirect::to("/")
                            ->with('success', $msg)
            ;
        } else {
            Session::flash('msg', $msg);
            return Redirect::to('/usuario/alterarsenha')
                            ->withInput()
                            ->with('msg', $msg)
            ;
        }
    }

    public function getExternos() {
        $id_cliente = Input::get('id_cliente', Input::old('id_cliente',Session::get('id_cliente')));
        $nomeusuario = Input::get('nomeusuario', Input::old('nomeusuario'));
        $usuarios = Usuario::externos()->filtrarPor($id_cliente, $nomeusuario)
                        ->orderby('cliente.razaosocial')
                        ->orderby('nomeusuario')
                        ->paginate(20);
        $clientes = array('' => '(todos)') + array('-1' => 'Administrador (acesso a todos clientes)') + \Cliente::getAtivos()->lists('razaosocial', 'id_cliente');

        Input::flash();

        return View::make('usuario_externos_list')
                        ->with('linkEdicao', 'usuario/editexternolimpo')
                        ->with('linkCriacao', 'usuario/editexterno/0')
                        ->with('linkRemocao','usuario/deleteexterno')
                        ->with('titulo', 'Usuários externos (clientes)')
                        ->with('instancias', $usuarios)
                        ->with('clientes', $clientes)
        ;
    }


    public function getDeleteinterno($id_usuario){
        $usuario = Usuario::find($id_usuario);
        $usuario->delete();
        Session::reflash();
        return Redirect::to('/usuario/internos')->with('success', 'Usuário excluído com sucesso!');
    }

    public function getDeleteexterno($id_usuario){
        $usuario = Usuario::find($id_usuario);
        $usuario->delete();
        Session::reflash();
        return Redirect::to('/usuario/externos')->with('success', 'Usuário excluído com sucesso!');
    }
    public function getClientes() {
        $clientes = array('-1' => 'Administrador (acesso a todos clientes)') + \Cliente::getAtivos()->lists('razaosocial', 'id_cliente');
        return View::make('usuario_cliente')
                        ->with('clientes', $clientes)
                        ;
    }

    public function postClientes(){
        try{
            $usuario = $this->repUsuarioExterno->cria(Input::all());
            return Redirect::to('/usuario/editexterno/'.$usuario->id_usuario);
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
            return Redirect::to('/usuario/clientes/');
        }
    }

    public function getNovointerno() {
        return View::make('usuario_novo_interno')
                        ->with('model', new Usuario());
    }

    public function postNovointerno() {
        Input::flash();
        if($this->repUsuarioInterno->cria(Input::all())){
            return Redirect::to('/usuario/internos')->with('success', 'Usuário criado com sucesso');
        } else {
            return Redirect::back()->withErrors($this->repUsuarioInterno->validacao->getErrors());
        }
    }

    public function getEditarinterno($id_usuario = '') {
        $model = Usuario::find($id_usuario);
        return View::make('usuario_editar_interno')
                        ->with('model', $model);
    }

    public function postEditarinterno() {
        Input::flash();
        if($this->repUsuarioInterno->edita(Input::all())){
            return Redirect::to('/usuario/internos')->with('success', 'Usuário alterado com sucesso');
        } else {
            return Redirect::back()->withErrors($this->repUsuarioInterno->validacao->getErrors());
        }
    }

    public function postExcluir() {
        Input::flash();
        if($this->repUsuarioInterno->editaInterno(Input::all())){
            return Redirect::to('/usuario/internos')->with('success', 'Usuário alterado com sucesso');
        } else {
            return Redirect::back()->withErrors($this->repUsuarioInterno->validacao->getErrors());
        }
    }

    public function postNovo($id_cliente) {

        Input::flash();
        $cliente = Cliente::find($id_cliente);
        $departamentos = Input::get('id_departamento');
        $setores = Input::get('id_setor');
        $centrocustos = Input::get('id_centrodecusto');
        $fields = Input::all();
        $fields['password'] = Hash::make($fields['password']);

        try {
            DB::connection()->getPdo()->beginTransaction();
            $model = Usuario::create($fields);
            $model->fl_primeira_vez = 1;
            $id_usuario = $model->id_usuario;

            if (is_array($departamentos) and !empty($departamentos)) {
                foreach ($departamentos as $id_departamento) {
                    if (is_numeric($id_departamento)) {
                        $existe = Usuariodepartamento::where('id_usuario', '=', $id_usuario)
                                ->where('id_cliente', '=', $cliente->id_cliente)
                                ->where('id_departamento', '=', $id_departamento)
                                ->count();
                        if (!$existe) {
                            Usuariodepartamento::create(array(
                                'id_usuario' => $id_usuario,
                                'id_cliente' => $cliente->id_cliente,
                                'id_departamento' => $id_departamento,
                            ));
                        }
                    }
                }
            }
            if (is_array($setores) and !empty($setores)) {
                foreach ($setores as $id_setor) {
                    if (is_numeric($id_setor)) {
                        $existe = Usuariosetor::where('id_usuario', '=', $id_usuario)
                                ->where('id_cliente', '=', $cliente->id_cliente)
                                ->where('id_setor', '=', $id_setor)
                                ->count();
                        if (!$existe) {
                            Usuariosetor::create(array(
                                'id_usuario' => $id_usuario,
                                'id_cliente' => $cliente->id_cliente,
                                'id_setor' => $id_setor,
                            ));
                        }
                    }
                }
            }
            if (is_array($centrocustos) and !empty($centrocustos)) {
                foreach ($centrocustos as $id_centrocusto) {
                    if (is_numeric($id_centrocusto)) {
                        $existe = Usuariocentrocusto::where('id_usuario', '=', $id_usuario)
                                ->where('id_cliente', '=', $cliente->id_cliente)
                                ->where('id_centrocusto', '=', $id_centrocusto)
                                ->count();
                        if (!$existe) {
                            Usuariocentrocusto::create(array(
                                'id_usuario' => $id_usuario,
                                'id_cliente' => $cliente->id_cliente,
                                'id_centrocusto' => $id_centrocusto,
                            ));
                        }
                    }
                }
            }
            DB::connection()->getPdo()->commit();
            Session::flash('success', 'Usuário criado com sucesso !');
            return Redirect::to('/usuario/edit/' . $id_usuario);
        } catch (Exception $exc) {
            Session::flash('error', $exc->getTraceAsString());
            return View::make('usuario_novo')
                            ->with('cliente', $cliente)
                            ->with('model', new Usuario());
        }
    }

    public function getEditexternolimpo($id_usuario){
    	return Redirect::to('/usuario/editexterno/'.$id_usuario);
    }

    public function getEditexterno($id_usuario = 0) {
        if ($id_usuario > 0){
            $usuario = Usuario::findOrFail($id_usuario);
        } else {
            $usuario = new \Usuario;
        }

        $bloqueio = '';
        if ($usuario->validadelogin > 0){
            if ($usuario->datacadastro_fmt != ''){
                $dataLimite = Carbon::createFromFormat('Y-m-d', $usuario->datacadastro);
                $dataLimite->addDays($usuario->validadelogin);
                if($dataLimite->isPast()){
                    $bloqueio = 'Atenção: acesso bloqueado desde '.$dataLimite->format('d/m/Y');
                } else {
                    $bloqueio = 'Atenção: acesso será bloqueado a partir de '.$dataLimite->format('d/m/Y');
                }
            }
        }


        $clientes = \Cliente::razaosocialCompleta()
                    ->orderBy('razaosocial')
                    ->lists('razaosocialCompleta', 'id_cliente');
        $clientes = array('' => '(selecione)') + array('-1' => 'Administrador (acesso a todos clientes)') + $clientes;
        return View::make('usuario_editar')
                        ->with('model', $usuario)
                        ->with('clientes', $clientes)
                        ->with('bloqueio', $bloqueio)
            ;
    }

    public function postEditexternovelho() {
        Input::flash();
        $post = Input::all();
        $id_usuario = Input::get('id_usuario');
        if (intval($id_usuario == 0)){
            if (Input::get('senha') == ''){
                $senha = \Usuario::rand_string(8);
                $post['senha'] = $senha;
            } else {
                $senha = $post['senha'];
            }
        }
        if($usuario = $this->repUsuarioExterno->edita(Input::all())){
            Session::set('id_cliente', Input::get('id_cliente', ''));
            $msg = 'Usuário alterado com sucesso.';
            if (intval($id_usuario) == 0){
                $data = array('usuario' => $usuario, 'password' => $senha);
                Mail::send('emails.novo_usuario', $data, function($message) use($usuario) {
                    $message->to($usuario->username, $usuario->nomeusuario)->subject('Confirmação de cadastro de Login para utilização do E-Doc');
                    $message->replyTo(\Config::get('memodoc.email_atendimento'));
                });
                $msg .= " E-mail de notificação enviado com sucesso.";
            }

            return Redirect::to('/usuario/externos')->with('success', $msg);
        } else {
            return Redirect::back()->withErrors($this->repUsuarioExterno->getErrors());
        }
    }

    public function postEditexterno() {
    	Input::flash();
    	$post = Input::all();
    	if($usuario = $this->repUsuarioExterno->edita(Input::all())){
    		Session::set('id_cliente', Input::get('id_cliente', ''));
    		$msg = "";
    		$br = "<br/>";
    		$errors = $this->repUsuarioExterno->errors;
    		if (!is_array($errors)){
    			$errors = $errors->all();
    		}
    		foreach($errors as $error){
    			$msg .= $br.$error;
    		}
    		return Redirect::to('/usuario/externos')->with('success', $msg);
    	} else {
    		return Redirect::back()->withErrors($this->repUsuarioExterno->getErrors());
    	}
    }

    public function getHash() {
        $usuarios = Usuario::all();
        foreach ($usuarios as $usuario) {
            if (strlen($usuario->password) < 50) {
                $usuario->password = Hash::make($usuario->password);
                $usuario->save();
            }
        }
    }

    public function getEnviarnovasenhaexterno($id_usuario){
        Session::reflash();
        $rep = new \Memodoc\Repositorios\RepositorioUsuarioExternoEloquent();
        $usuario = \Usuario::find($id_usuario);
        try{
            $rep->enviarNovaSenha($usuario);
            return Redirect::to('/usuario/externos')->with('success', 'Nova senha enviada com sucesso!');
        } catch (Exception $ex) {
            return Redirect::to('/usuario/externos')->with('errors', 'Não foi possível enviar a nova senha no momento.<br/>'.$ex->getMessage());

        }
    }

}
