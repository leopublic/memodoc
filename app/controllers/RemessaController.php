<?php

class RemessaController extends BaseController {


    public function getList($id_transbordo = ''){
        $remessas = \Remessa::orderBy('created_at', 'desc')->get();
        return View::make('remessa/listar')
                        ->with('remessas', $remessas)
                        
        ;
    }


    public function getIndex() {
        return $this->getList();
    }

    public function getCliente() {

        
    }

    public function postCliente(){

    }

    public function getCaixa(){

    }

    public function postCaixa(){

    }

    public function getItens($id_remessa){
        $remessa = \Remessa::find($id_remessa);
        $itens = \ItemRemessa::where('id_remessa', '=', $id_remessa)->get();
        $data = array(
            'remessa' => $remessa
            ,'itens'    => $itens
            );
        return View::make('remessa/itens', $data);
    }

    public function postItens(){
        Input::flash();
        $ids_flat = Input::get('ids');
        $ids = explode('##', $ids_flat);
        $id_cliente = Input::get('id_cliente');
        $id_caixapadrao = Input::get('id_caixapadrao');
        $msg = '';
        $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent;
        $lock = false;
        $disabled = "";
        if ($id_cliente > 0 && $id_caixapadrao > 0){
            $itens = \Documento::where('id_cliente', '=', $id_cliente)
                             ->where('id_caixapadrao', '=', $id_caixapadrao)
                             ->orderBy('nume_inicial')
                             ->get();
            if (count($itens) > 0){
                $lock = true;
                $disabled = "disabled";
            } else {
                $msg = 'Caixa não encontrada no cliente informado';
            }
        } else {
            $msg = 'Informe o cliente e o número da caixa';
        }



        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        $data = array(
            'clientes' => $clientes
            ,'itens'    => $itens
            ,'lock'     => $lock
            ,'disabled'     => $disabled
            ,'action'   => action('DigitacaoController@postCriaritens')
            ,'id_caixapadrao' => $id_caixapadrao
            ,'id_cliente' => $id_cliente
            ,'msg' => $msg
            );
        return View::make('digitacao/itens', $data);
    }

    public function postCriaritens(){
        if (Input::has('criar')){
            $msg = $rep->adiciona_itens_lote($id_cliente, $id_caixapadrao, $ids);
        }

    }

    public function getItenscadastrados($id_cliente, $id_caixapadrao){
        $docs = \Documento::where('id_cliente', '=', $id_cliente)
                         ->where('id_caixapadrao', '=', $id_caixapadrao)
                         ->orderBy('nume_inicial')
                         ->get();
        $itens = '';
        $virg = '';
        if (count($docs) == 0){
            $msg = 'Essa referência não existe nesse cliente';
            $codigo = '1';
        } else {
            $msg = '';
            $codigo = '0';
            foreach($docs as $doc){
                $itens .= $virg.$doc->nume_inicial;
                $virg = ',';
            }
        }
        $ret = array(
            'codigo' => $codigo
            ,'msg' => $msg
            ,'itens' => $itens
            );
        return json_encode($ret);
    }

    public function getTestaplanilha(){
        $rep = new Memodoc\Repositorios\RepositorioPlanilha;
        //$rep->abra(app_path('arquivos/anexos/Remessa MemoDoc-Excel.XLS'));
        $rep->abra(app_path('arquivos/anexos/xxx.xls'));
    }

}