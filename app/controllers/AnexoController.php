<?php

class AnexoController extends BaseController {

    public function getIndex() {
        $avisos = \Aviso::whereNull('id_cliente')->get();

        return View::make('aviso.home')
                        ->with('avisos', $avisos);
    }

    public function postAdicionar() {
        
        $id_documento = Input::get('id_documento');
        $doc = \Documento::find($id_documento);
        $id_cliente = $doc->id_cliente;
        $id_caixapadrao = $doc->id_caixapadrao;
        $id_item = $doc->id_item;
        try {
            $anexo = new \Anexo;
            $anexo->id_documento = $id_documento;
            $anexo->descricao = Input::get('descricao');
            $anexo->salvaArquivo('arquivo');
            Session::flash('success', 'Arquivo carregado com sucesso!');
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
        }
        return Redirect::to('/documento/editar/' . $id_cliente . '/' . $id_caixapadrao . '/' . $id_item);
    }

    public function getDownload($id_anexo) {
        $anexo = \Anexo::find($id_anexo);
        $headers = array(
            'Content-type' => $anexo->mime,
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'public',
            'Content-Length' => $anexo->tamanho
        );

        return Response::download($anexo->caminho(), Str::ascii($anexo->nome_original), $headers);
    }
    
    public function getExcluir($id_anexo){
        $anexo = \Anexo::find($id_anexo);
        $doc = \Documento::find($anexo->id_documento);
        try {
            $anexo->deleta();
            Session::flash('success', 'Digitalização excluída com sucesso!');
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
        }
        return Redirect::to('/documento/editar/' . $doc->id_cliente . '/' . $doc->id_caixapadrao . '/' . $doc->id_item);
    }

}
