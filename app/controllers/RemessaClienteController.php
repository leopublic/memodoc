<?php

class RemessaClienteController extends BaseController {

    /**
     * Lista as remessas cadastradas pelo cliente
     * @return [type] [description]
     */
    public function getIndex() {
        $instancias = Remessa::where('id_cliente', '=', \Auth::user()->id_cliente)->orderBy('created_at', 'desc')->get();

        return View::make('remessa/cliente_list')
                        ->with('instancias', $instancias);
    }

    public function postCliente(){

    }

    public function getNova(){
        $remessa = new Remessa;

        return View::make('remessa/cliente_edit')
                        ->with('model', $remessa);
    }

    public function postNova(){
        Input::flash();
        $post = Input::all();
        $errors = array();
        $remessa = new Remessa;
        if ($post['dt_inicial'] <> '') {
            $dt_inicial = DateTime::createFromFormat('d/m/Y', $post['dt_inicial']);
            if (is_object($dt_inicial)) {
                $remessa->dt_inicial = $dt_inicial->format('Y-m-d');
            } else {
                $errors[] = "Formato da data inicial inválido. Verifique.";
            }
        } else {
            unset($post['dt_inicial']);
            $remessa->dt_inicial = null;
        }

        if ($post['dt_final'] <> '') {
            $dt_final = DateTime::createFromFormat('d/m/Y', $post['dt_final']);
            if (is_object($dt_final)) {
                $remessa->dt_final = $dt_final->format('Y-m-d');
            } else {
                $errors[] = "Formato da data final inválido. Verifique.";
            }
        } else {
            unset($post['dt_final']);
            $remessa->dt_final = null;
        }

        $remessa->alfa_inicial = Input::get('alfa_inicial');
        $remessa->alfa_final = Input::get('alfa_final');
        $remessa->id_cliente = \Auth::user()->id_cliente;
        $remessa->save();

        try {
            $anexo = new \Anexo;
            $anexo->id_remessa = $remessa->id_remessa;
            $anexo->salvaArquivo('arquivo');
            Session::flash('success', 'Arquivo carregado com sucesso!');
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
        }
        return Redirect::to('/publico/remessa');

    }


}