<?php

class UsuarioClienteController extends BaseController {

    public function getIndex() {
        Input::flash();

        $instancias = Usuario::DoCliente(Auth::user()->id_cliente)
                ->orderBy('nomeusuario')
                ->get()
        ;

        return View::make('usuario_cliente_list')
                        ->with('titulo', 'Usuário')
                        ->with('instancias', $instancias)
        ;
    }

    public function getClientes() {
        return View::make('usuario_cliente')
                        ->with('clientes', Cliente::All())
        ;
    }

    public function getNovo() {
        $clientes= \Auth::user()->getListaClientes();
//        $clientes = $cursor->orderBy('razaosocial')->lists('razaosocial', 'id_cliente');
        return View::make('usuario_cliente_novo')
                        ->with('model', new Usuario())
                        ->with('id_cliente', \Auth::user()->id_cliente)
                        ->with('clientes', $clientes)
        ;
    }

    public function postNovo() {
        Input::flash();
        $rep = new \Memodoc\Repositorios\RepositorioUsuarioExternoEloquent;
        $password = Usuario::rand_string(8);
        $post = Input::all();
        $post['senha'] = $password;
        $usuario = $rep->salvaDoCliente($post);
        if($usuario){
            Session::flash('success', 'Usuário criado com sucesso !');
            $data = array('usuario' => $usuario, 'password' => $password);
            Mail::send('emails.novo_usuario', $data, function($message) use($usuario) {
                $message->to($usuario->username, $usuario->nomeusuario)->subject('Confirmação de cadastro de Login para utilização do E-Doc');
                //$message->bcc('atendimento@memodoc.com.br');
                $message->replyTo(\Config::get('memodoc.email_atendimento'));
            });
            return Redirect::to('/publico/usuario/editar/' . $usuario->id_usuario);
        } else {
            return Redirect::back()->withErrors($rep->getErrors());
        }
    }


    public function getExcluir($id_usuario) {
        $usuario = Usuario::find($id_usuario);

        if (is_object($usuario)) {
            $usuario->departamentos()->detach();
            $usuario->centrocustos()->detach();
            $usuario->setores()->detach();
            $usuario->delete();
        }
        Session::flash('success', 'Usuário excluído com sucesso!');
        return Redirect::to('/publico/usuario/');
    }

    public function getRenovarsenha($id_usuario) {
        $model = Usuario::find($id_usuario);
        $password = Usuario::rand_string(8);
        $model->password = Hash::make($password);
        $model->save();
        $data = array('usuario' => $model, 'password' => $password);

        Mail::send('emails.altera_usuario', $data, function($message) use($model) {
            $message->to($model->username, $model->nomeusuario)->subject('Alteração de Login e senha');
            $message->replyTo(\Config::get('memodoc.email_atendimento'));
        });
        Session::flash('success', 'Uma nova senha foi enviada para o email ' . $model->username . '');
        return Redirect::to('/publico/usuario/');
    }

    public function getEditar($id_usuario) {

        $usuario = Usuario::findOrFail($id_usuario);
        $lista_clientes = $usuario->clientes;
        return View::make('usuario_cliente_editar')
                        ->with('model', $usuario)
                        ->with('lista_clientes', $lista_clientes)
            ;
    }

    public function getMeusdados() {
        $id_usuario = Auth::user()->id_usuario;
        $usuario = Usuario::findOrFail($id_usuario);
        
        return View::make('usuario_cliente_meusdados')
                        ->with('model', $usuario);
    }

    public function postMeusdados() {
        try {
            $campos = Input::all();
            $usuario = Auth::user();
            
            $usuario->nomeusuario = $campos['nomeusuario'];
            $usuario->aniversario_dia = $campos['aniversario_dia'];
            $usuario->aniversario_mes = $campos['aniversario_mes'];
            $usuario->fl_primeira_vez =0;
            $usuario->save();

            $data = array('usuario' => $usuario, 'password' => $password);
            Mail::send('emails.altera_usuario', $data, function($message) use($usuario) {
                $message->to($usuario->username, $usuario->nomeusuario)->subject('Alteração de Login e senha');
                $message->replyTo(\Config::get('memodoc.email_atendimento'));
            });

            Session::flash('success', 'Dados atualizados com sucesso. Obrigado!');
            return Redirect::to('/home/bemvindocliente');
        } catch (\Whoops\Example\Exception $exc) {
            Session::flash('error', $exc->getTraceAsString());
            return Redirect::to('/publico/usuario/meusdados');
        }
    }

    public function postEditar($id_usuario) {
        try {
            $departamentos = Input::get('id_departamento');
            $setores = Input::get('id_setor');
            $centrocustos = Input::get('id_centrodecusto');
            $fields = Input::All();

            DB::connection()->getPdo()->beginTransaction();
            $model = Usuario::findOrFail($id_usuario);
            $lista_clientes = $model->clientes;
            $model->fill($fields);
            $model->save();
            if (is_array($departamentos)) {
                $model->departamentos()->sync($departamentos);
            } else {
                $model->departamentos()->detach();
            }
            if (is_array($centrocustos)) {
                $model->centrocustos()->sync($centrocustos);
            } else {
                $model->centrocustos()->detach();
            }
            if (is_array($setores)) {
                $model->setores()->sync($setores);
            } else {
                $model->setores()->detach();
            }

            DB::connection()->getPdo()->commit();
            Session::flash('success', 'Usuário salvo com sucesso');
        } catch (\Whoops\Example\Exception $exc) {
            Session::flash('error', $exc->getTraceAsString());
        }


        return View::make('usuario_cliente_editar')
                        ->with('model', $model)
                        ->with('lista_clientes', $lista_clientes)
            ;
    }

    public function getHash() {
        $usuarios = Usuario::all();
        foreach ($usuarios as $usuario) {
            if (strlen($usuario->password) < 50) {
                $usuario->password = Hash::make($usuario->password);
                $usuario->save();
            }
        }
    }

    public function getAlterarsenha() {
        $usuario = Auth::user();
        return View::make('usuario_senha')
                        ->with('model', $usuario)
        ;
    }

    public function postAlterarsenha() {
        $msg = '';
        $fields = Input::All();
        $br = '';
        if (!Hash::check($fields['password'], Auth::user()->password)) {
            $msg .= $br . "A senha atual está incorreta.";
            $br = '<br/>';
        }
        if (trim($fields['password_nova']) == '') {
            $msg .= $br . "A nova senha não foi informada.";
            $br = '<br/>';
        } else {
            if ($fields['password_nova'] != $fields['password_nova_rep']) {
                $msg .= $br . "A repetição da senha não confere.";
                $br = '<br/>';
            } 
            if (str_contains($fields['password_nova'], ' ')) {
                $msg .= $br . "A senha não pode conter espaços.";
                $br = '<br/>';
            } 
            if (strlen($fields['password_nova']) < 8) {
                $msg .= $br . "A senha deve conter pelo menos 8 caracteres.";
                $br = '<br/>';
            }
        }

        if ($msg != '') {
            $msg .= '<br/>Por favor tente novamente';
        }
        if ($msg == '') {
            Auth::user()->password = Hash::make($fields['password_nova']);
            Auth::user()->save();
            Session::flash('msg', "Senha alterada com sucesso!");
            return Redirect::to("/");
        } else {
            Session::flash('error', $msg);
            return Redirect::to('/publico/usuario/alterarsenha')
                            ->withInput()
            ;
        }
    }

    public function getEscolherempresa() {
        if (Auth::user()->adm == 1) {
            $clientes = Cliente::getTodos()->lists('razaosocial', 'id_cliente');
        } else {
            $clientes = \Cliente::select(array('id_cliente', DB::raw("concat(razaosocial, ' (', substr(concat('000',id_cliente), -3), ' - ', nomefantasia, ')') as razaosocial")))
                            ->whereIn('id_cliente', function($query){
                                    $query->select('id_cliente')
                                    ->from('usuario_cliente')
                                    ->where('id_usuario', '=',Auth::user()->id_usuario);}
                                    )
                                ->orWhere('id_cliente', '=', Auth::user()->id_cliente_principal)
                            ->orderBy('razaosocial')
                            ->lists('razaosocial', 'id_cliente');
            }
        return View::make('usuario_seleciona_empresa')
                        ->with('clientes', $clientes)
        ;
    }

    public function postEscolherempresa() {
        $empresa = Cliente::find(Input::get('id_cliente'));
        $usuario = Auth::user();
        $usuario->id_cliente = Input::get('id_cliente');
        $usuario->save();
        return Redirect::to('/home/bemvindocliente')
                        ->with('msg', 'A partir de agora você está no cliente ' . $empresa->razaosocial.' ('.$empresa->id_cliente.' '.$empresa->nomefantasia.')')
        ;
    }

}
