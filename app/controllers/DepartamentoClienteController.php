<?php

class DepartamentoClienteController extends BaseController {

    public function getIndex() {
        $centrocustos = DepCliente::where('id_cliente', '=', Auth::user()->id_cliente)->orderBy('nome')
                    ->get();
        return View::make('departamento_cliente_list')
                        ->with('instancias', $centrocustos)
        ;
    }

    public function postAdicionar(){
        $nome = Input::get('nome');
        $centro = new DepCliente();
        $centro->nome = $nome;
        $centro->id_cliente = Auth::user()->id_cliente;
        $centro->save();
        return Redirect::to('/publico/departamento')->with('success', 'Departamento adicionado com sucesso');
    }

    public function getExcluir($id_departamento){
        DB::table('documento')->where('id_departamento', '=', $id_departamento)->update(array('id_departamento' => null));
        DepCliente::destroy($id_departamento);
        return Redirect::to('/publico/departamento')->with('success', 'Departamento excluído com sucesso');
    }

    public function postAlterar(){
        $centro = DepCliente::find(Input::get('id_departamento'));
        $centro->nome = Input::get('nome');
        $centro->save();
        return Redirect::to('/publico/departamento')->with('success', 'Departamento atualizado com sucesso');
    }
}
