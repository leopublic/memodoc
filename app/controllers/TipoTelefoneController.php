<?php

class TipoTelefoneController extends BaseController {

        /**
         * Listagem de Tipo telefone
         * @return type View
         */
	public function getIndex()
	{
		Input::flash();
		$instancias = TipoTelefone::all();

		$colunas = array(
			array('titulo' =>'Descrição', "campo"=> 'descricao')
		);
		
		return View::make('modelo_list')
					->with('linkEdicao','tipotelefone/edit')
					->with('linkCriacao','tipotelefone/create')
					->with('linkRemocao','tipotelefone/delete')
					->with('titulo', 'Tipos de telefone')
					->with('colunas', $colunas)
					->with('instancias', $instancias)
				;		
	}

        /**
         * Editar 
         * @param type $id
         * @return type View
         */
	public function getEdit($id)
	{
                $model = TipoTelefone::find($id);
		return View::make('tipotelefone_form', compact('model'));		
	}
        
	public function postEdit($id)
	{
                Input::flash();
                $model = TipoTelefone::findOrFail($id);
                $model->fill(Input::all());
                $id = $model->save();
                if($id){
                    Session::flash('success', 'Tipo telefone foi atualizado com sucesso !');
                    return  Redirect::to('tipotelefone/index');
                }
			
	}
        
        
        /**
         * Create
         * @return type View
         */
	public function getCreate()
	{
		return View::make('tipotelefone_form');		
	}
        
	public function postCreate()
	{
                Input::flash();
                $model = new TipoTelefone();
                $model->fill(Input::all());
                $id = $model->save();
                if($id){
                    Session::flash('success', 'Tipo de telefone cadastrado com sucesso !');
                    return  Redirect::to('tipotelefone/index');
                }
			
	}
        
	public function getDelete($id)
	{
            if(TipoTelefone::getPermiteDeletar($id)){
                $model = TipoTelefone::find($id);
                $model->delete();
                Session::flash('msg', 'Tipo de tefone '.$model->descricao.' foi deletado com sucesso !');
                return  Redirect::to('tipotelefone/index');
            }else{
                Session::flash('error', 'Este tipo de telefone possui vinculo com outros registros, não é possivel deletar !');
                return  Redirect::to('tipotelefone/index');
            }
	}
        

}