<?php
use Carbon\Carbon;
class CheckinController extends BaseController {
    const raiz = '/checkinxx/listar';
    public function getIndex() {
        $rep = new \Memodoc\Repositorios\RepositorioCheckinEloquent;
        $caixas = $rep->pendentes();
        $repPerFat = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $agora = Carbon::now(new DateTimeZone(Config::get('app.timezone')));
        $faturamentosAbertos = $repPerFat->faturamentosFechadosNaData($agora->format('Y-m-d'));
        $readonly = null;
        if (count($faturamentosAbertos) > 0){
            $readonly = true;
        }
        return View::make('checkin')
                        ->with('caixas', $caixas)
                        ->with('readonly', $readonly)
        ;
    }
    public function postIndex() {
        $erro = array();
        $id_caixa = Input::get('id_caixa');

        $checkin = Checkin::where('id_caixa','=',$id_caixa)
                          ->whereNull('dt_checkin')
                          ->first();
        if(isset($checkin)){
            Galpao::Checkin($checkin);

            Session::flash('success', 'Caixa '.$id_caixa.' recebida com sucesso');
        } else {
            Session::flash('errors', 'Não existe um checkin pendente para o ID '.$id_caixa.'. Verifique.');
        }
        return Redirect::to('/checkin');
    }

    public function getListar($random = '') {
        $rep = new \Memodoc\Repositorios\RepositorioCheckinEloquent;
        $caixas = $rep->pendentes();
        $repPerFat = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $agora = Carbon::now(new DateTimeZone(Config::get('app.timezone')));
        $faturamentosAbertos = $repPerFat->faturamentosFechadosNaData($agora->format('Y-m-d'));
        $readonly = null;
        if (count($faturamentosAbertos) > 0){
            $readonly = true;
        }
        return View::make('checkin')
                        ->with('caixas', $caixas)
                        ->with('readonly', $readonly)
        ;
    }
    public function postListar() {
        $erro = array();
        $id_caixa = Input::get('id_caixa');

        $checkin = Checkin::where('id_caixa','=',$id_caixa)
                          ->whereNull('dt_checkin')
                          ->first();

        if(isset($checkin)){
            $hoje = Carbon::now('America/Sao_Paulo')->format('Y-m-d');
            $entregar_em = Carbon::createFromFormat('d/m/Y', $checkin->os->entregar_em, 'America/Sao_Paulo')->format('Y-m-d');
            if ($entregar_em <= $hoje){
                Galpao::Checkin($checkin);

                Session::flash('success', 'Caixa '.$id_caixa.' recebida com sucesso');
            } else {
                Session::flash('errors', 'Esse checkin pertence a uma OS que só será atendida em '.$checkin->os->entregar_em.' e não poderá ser efetivado no momento. Verifique.');
            }
        } else {
            Session::flash('errors', 'Não existe um checkin pendente para o ID '.$id_caixa.'. Verifique.');
        }
        return Redirect::to(self::raiz.'/'.str_random(40));
    }
    public function getRetirar($id_caixa_retirar) {
        $id_caixas = explode(",", Session::get('id_caixas_checkin'));
        $novas_caixas = "";
        $virg = "";
        foreach ($id_caixas as $id_caixa_atual) {
            if ($id_caixa_atual != $id_caixa_retirar) {
                $novas_caixas .= $virg . $id_caixa_atual;
                $virg = ",";
            }
        }
        Session::flash("msg", "Caixa retirada com sucesso.");
        Session::put('id_caixas_checkin', $novas_caixas);
        return Redirect::to(self::raiz.'/'.str_random(40));
    }
    
    public function getCheckinos($id_os_chave){
    	$checkins = Checkin::where('id_os_chave', '=', $id_os_chave)->get();
    	foreach($checkins as $checkin){
    		print "<br/>Checkin caixa ".$checkin->id_caixa;
    		Galpao::Checkin($checkin);
    	}
    }
}
