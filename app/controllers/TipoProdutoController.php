<?php

class TipoProdutoController extends BaseController {

    /**
     * Listagem de Tipo produto
     * @return type View
     */
    public function getIndex() {
        Input::flash();
        $instancias = TipoProduto::all();

        $colunas = array(
            array('titulo' => 'Descrição', "campo" => 'descricao')
        );

        return View::make('modelo_list')
                        ->with('linkEdicao', 'tipoproduto/edit')
                        ->with('linkCriacao', 'tipoproduto/create')
                        ->with('linkRemocao', 'tipoproduto/delete')
                        ->with('classe', 'tipoproduto')
                        ->with('titulo', 'Tipos de produto')
                        ->with('colunas', $colunas)
                        ->with('instancias', $instancias)
        ;
    }

    /**
     * Editar
     * @param type $id
     * @return type View
     */
    public function getEdit($id) {
        $model = TipoProduto::find($id);
        return View::make('tipoproduto_form', compact('model'));
    }

    public function postEdit($id) {
        Input::flash();
        $model = TipoProduto::findOrFail($id);
        $model->fill(Input::all());
        $id = $model->save();
        if ($id) {
            Session::flash('success', 'Tipo produto foi atualizado com sucesso !');
            return Redirect::to('tipoproduto/index');
        }
    }

    /**
     * Create
     * @return type View
     */
    public function getCreate() {
        return View::make('tipoproduto_form');
    }

    public function postCreate() {
        Input::flash();
        $model = new TipoProduto();
        $model->fill(Input::all());
        $id = $model->save();
        if ($id) {
            Session::flash('success', 'Tipo produto cadastrado com sucesso !');
            return Redirect::to('tipoproduto/index');
        }
    }

    public function getDelete($id) {
        if (TipoProduto::getPermiteDeletar($id)) {
            $model = TipoProduto::find($id);
            $model->delete();
            Session::flash('msg', 'Tipo produto ' . $model->descricao . ' foi deletado com sucesso !');
            return Redirect::to('tipoproduto/index');
        } else {
            Session::flash('error', 'Esse tipo produto  não pode ser deletado pois possui dados pendentes !');
            return Redirect::to('tipoproduto/index');
        }
    }


    public function getPeriodo($ano = "", $mes = ""){
        if ($mes > 0 && $ano > 0){
        	$valores = \IndiceReajuste::where('mes', '=', $mes)
        		->join('indice', 'indice.id_indice', '=', 'indicereajuste.id_indice')
        		->where('ano', '=', $ano)
        		->orderBy('descricao')
        		->get()
        		;
        } else {
        	$valores = array();
        }
        
        return View::make('reajuste.periodo')
                ->with('ano', $ano)
                ->with('mes', $mes)
                ->with('valores', $valores)
            ;
    }

    public function postPeriodo(){
    	Input::flash();
    	$mes = Input::get('mes');
        $ano = Input::get('ano');
        if (is_numeric($mes) && is_numeric($ano) && $mes <=12 && $mes >0 && $ano >2000){
        	$rep = new \Memodoc\Repositorios\RepositorioIndiceReajusteEloquent;
        	$rep->preencheMesAno($mes, $ano);
        	return Redirect::to('tipoproduto/periodo/'.$ano."/".$mes);
        } else {
            return Redirect::to('tipoproduto/periodo')->with('errors', 'Mês ou ano inválidos');
        }

    }

    public function getReajustar($ano, $mes, $id_indice){
        $rep = new \Memodoc\Repositorios\RepositorioIndiceReajusteEloquent;
        $rep->preencheMesAno($mes, $ano);
        $valores = \IndiceReajuste::where('mes', '=', $mes)
                    ->where('ano', '=', $ano)
                    ->where('id_indice', '=', $id_indice)
                    ->get()
                ;
        $indices = \Indice::orderBy("descricao")->get();
        $clientes = \Cliente::whereRaw('month(iniciocontrato) ='.$mes)
        		->where('id_indice', '=', $id_indice)
                ->whereRaw('year(iniciocontrato) <>'.$ano)
                ->orderBy('razaosocial')->get();
        foreach($clientes as $indice => $cliente){
            if ($cliente->fl_ativo == 0 && $cliente->qtdCaixas() == 0){
                unset($clientes[$indice]);
            }
        }

        return View::make('reajuste.indice')
                ->with('valores', $valores)
                ->with('ano', $ano)
                ->with('mes', $mes)
                ->with('id_indice', $id_indice)
                ->with('indices', $indices)
                ->with('clientes', $clientes)
            ;
    }
    public function postReajustar(){
        $rep = new \Memodoc\Repositorios\RepositorioIndiceReajusteEloquent;

        $mes = Input::get('mes');
        $ano = Input::get('ano');
        $id_indice = Input::get('id_indice');

        $post = Input::all();
        $rep->processaAtualizacao($post);
        return Redirect::to('tipoproduto/reajustar/'.$ano."/".$mes.'/'.$id_indice)->with('success', 'Percentuais atualizados com sucesso');

    }

    public function getHistorico($id_cliente = ''){

    }

    public function postHistorico(){
        
    }
    
    public function postAtualizarajax(){
		try{
			$rep = new \Memodoc\Repositorios\RepositorioTipoProdutoCliente();
			$id_cliente = Input::get('id_cliente');
			$mes = Input::get('mes');
			$ano = Input::get('ano');
			\DB::beginTransaction();
                for ( $id_tipoproduto = 1; $id_tipoproduto <= 5; $id_tipoproduto++ ){
                    $valor_contrato = str_replace(",", ".", str_replace(".", "", Input::get('valor_contrato_'.$id_tipoproduto)));
                    $valor_urgencia = str_replace(",", ".", str_replace(".", "", Input::get('valor_urgencia_'.$id_tipoproduto, 0)));
                    $rep->atualizar_preco($mes, $ano, $id_cliente, $id_tipoproduto, $valor_contrato, $valor_urgencia, \Auth::user()->id_usuario);
                }
                $rep_cli = new \Memodoc\Repositorios\RepositorioClienteEloquent();
                $rep_cli->atualize_valor_minimo($mes, $ano, $id_cliente, str_replace(",", ".", str_replace(".", "", Input::get('minimo'))));
                $rep_cli->atualize_data_ultimo_reajuste($id_cliente, \Auth::user()->id_usuario);
			\DB::commit();
			return json_encode(array('ret' => '0', 'msg' => "Valores atualizados com sucesso"));
		} catch(\Exception $e){
			\DB::rollBack();
			$msg = "Não foi possível atualizar os preços do cliente ".$id_cliente."<br/>".$e->getMessage()."<br/>".str_replace("\n", "<br/>", $e->getTraceAsString());
			return json_encode(array('ret' => '1', 'msg' => $msg));
		}
    
    }


    public function getRelatorio($ano, $mes, $id_indice = ""){
    	set_time_limit(0);
    	$rep = new \Memodoc\Repositorios\RepositorioIndiceReajusteEloquent;
    	$rep->preencheMesAno($mes, $ano);
    	$valores = \IndiceReajuste::where('mes', '=', $mes)
    			->join('indice', 'indice.id_indice', '=', 'indicereajuste.id_indice')
    			->where('ano', '=', $ano)
    			;
    	if ($id_indice > 0){
    		$valores = $valores->where('indice.id_indice', '=', $id_indice)
    			->orderBy('descricao')
    			->get()
    			;
    	} else {
    		$valores = $valores->orderBy('descricao')
    		->get()
    		;
    	}
    			
    	
    	$clientes = \Cliente::whereRaw('month(iniciocontrato) ='.$mes)
    			->ativos()
    			;
    	if($id_indice > 0){
    		$clientes = $clientes->where('id_indice', '=', $id_indice);
    	}
    	$clientes = $clientes->orderBy('razaosocial')
    			->get();
    	foreach($clientes as $indice => $cliente){
    		if ($cliente->qtdCaixas() == 0){
    			unset($clientes[$indice]);
    		}
    	}
    	$html = View::make('reajuste.relatorio')
	    	->with('valores', $valores)
	    	->with('ano', $ano)
	    	->with('mes', $mes)
	    	->with('clientes', $clientes)
    	;
    	$cab = View::make('reajuste.relatorio_cab')
	    	->with('valores', $valores)
	    	->with('ano', $ano)
	    	->with('mes', $mes)
    	;
    	$this->mpdf = new \mPDF('utf-8', 'A4', '11px', 'Arial', '10', '10', '30', '10', '10', '10');  // largura x altura
    	$this->mpdf->allow_charset_conversion = false;
    	$this->mpdf->debug = true;
    
    	// Monta o cabeçalho
    	$cab = iconv("UTF-8", "UTF-8//IGNORE", $cab);
    	$this->mpdf->setHTMLHeader($cab);
    
    	$html = iconv("UTF-8", "UTF-8//IGNORE", $html);
    	$this->mpdf->WriteHTML($html);
    	$this->mpdf->Output();
    }
    
    
}
