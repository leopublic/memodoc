<?php

class ReservaController extends BaseController {

    protected $rep;

    public function __construct() {
        $this->rep = new \Memodoc\Repositorios\RepositorioReservaEloquent;
    }

    public function getBuscar($gerar_excel = '0') {
        $reserva = new Reserva();
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        $id_cliente = Input::old('id_cliente', Session::get('id_cliente'));
        $reserva->id_cliente = $id_cliente;

        if($gerar_excel == '1'){

            if ($id_cliente > 0) {
                Session::put('id_cliente', $id_cliente);
                $reservas = Reserva::getListagem($id_cliente, Input::old('inicio'), Input::old('fim'), Input::old('cancelada'))->get();
            } else {
                $reservas = null;
            }
    
            Session::reflash();
            $html =  View::make('reserva_buscar_excel')
            ->with('clientes', $clientes)
            ->with('model', $reserva)
            ->with('reservas', $reservas);

            $conteudo =iconv("UTF8", "ISO-8859-1",  $html);
            return Response::make($conteudo, '200', array(
                    'Content-Type' => 'application/vnd.ms-excel',
                    'Content-Disposition' => 'attachment; filename="reservas.xls"'
            ));
                

        } else {
            if ($id_cliente > 0) {
                Session::put('id_cliente', $id_cliente);
                $reservas = Reserva::getListagem($id_cliente, Input::old('inicio'), Input::old('fim'), Input::old('cancelada'))->paginate(20);
            } else {
                $reservas = null;
            }

            Session::reflash();
            return View::make('reserva_buscar')
            ->with('clientes', $clientes)
            ->with('model', $reserva)
            ->with('reservas', $reservas);
        }
    }

    public function postBuscar() {
        $gerar_excel = Input::get('gerar_excel');
        Input::flashExcept('gerar_excel');
        return Redirect::to('/reserva/buscar/'.$gerar_excel)->withInput();
    }

    public function getBuscarexcel(){

    }

    public function getCriar($id_cliente = '', $caixas = '', $id_os_chave = '') {
        set_time_limit(0);
        $reserva = new Reserva;
        $os = null;
        if ($id_os_chave > 0) {
            $os = Os::find($id_os_chave);

            if ($os->reservas_nao_canceladas > 0) {
                $pode_criar = false;
                Session::flash('errors', 'Ja existe uma reserva para esta OS.');
            } else {
                $pode_criar = true;
                if ($id_cliente != '') {
                    $reserva->id_cliente = $id_cliente;
                }
                if ($caixas > 0) {
                    $reserva->caixas = $caixas;
                }
                if ($id_os_chave > 0) {
                    $reserva->id_os_chave = $id_os_chave;
                }
            }
        } else {
            $pode_criar = true;
            if ($id_cliente != '') {
                $reserva->id_cliente = $id_cliente;
            }
            if ($caixas > 0) {
                $reserva->caixas = $caixas;
            }
        }

        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');

        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $galpoes = $rep->lista_disponiveis();

        return View::make('reserva_editar')
                        ->with('clientes', $clientes)
                        ->with('os', $os)
                        ->with('pode_criar', $pode_criar)
                        ->with('model', $reserva)
                        ->with('galpoes', $galpoes)
        ;
    }

    public function postCriar() {
        set_time_limit(0);
        Input::flash();
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        if (Input::get('id_os_chave') > 0) {
            $os = Os::find(Input::get('id_os_chave'));
        } else {
            $os = null;
        }
        try {
            $reserva = $this->rep->criar(Input::all(), \Auth::user()->id_usuario);
            if ($reserva) {
                Session::set('id_cliente', Input::get('id_cliente'));
                return Redirect::to('/reserva/visualizar/' . $reserva->id_reserva)->with('success', 'Reserva criada com sucesso');
            } else {
                $errors = $this->rep->getErrors();
                Session::flash('errors', $errors);
                return View::make('reserva_editar')
                                ->with('clientes', $clientes)
                                ->with('os', $os)
                                ->with('model', $reserva)
                ;
            }
        } catch (Exception $ex) {
            $reserva = new Reserva;
            Session::flash('errors', $ex->getMessage());
            return View::make('reserva_editar')
                            ->with('clientes', $clientes)
                            ->with('os', $os)
                            ->with('model', $reserva)
            ;
        }
    }

    function getVisualizar($id_reserva, $acao = '') {
        $reserva = Reserva::findOrFail($id_reserva);
        $repEnd = new \Memodoc\Repositorios\RepositorioEnderecoEloquent;
        $enderecos = $repEnd->enderecosDaReserva($reserva);
        $documentos = Documento::where('id_cliente', '=', $reserva->id_cliente)
                        ->where('id_reserva', '=', $id_reserva)
                        ->orderby('id_caixapadrao')->get();

        return View::make('reserva_visualizar')
                        ->with('documentos', $documentos)
                        ->with('enderecos', $enderecos)
                        ->with('reserva', $reserva)
                        ->with('acao', $acao)
        ;
    }
    /**
     * Cancela reserva bloqueando os endereços
     *
     * @param [type] $id_reserva
     * @return void
     */
    function getCancelar($id_reserva) {
        Session::reflash();
        try {
            $reserva = Reserva::findOrFail($id_reserva);
            $this->rep->cancelarReserva($reserva, \Auth::user()->id_usuario);
            Session::flash('success', 'A reserva ' . $id_reserva . ' foi cancelada, e os endereços foram disponibilizados com sucesso! ');
        } catch (Exception $exc) {
            Session::flash('errors', $exc->getMessage().$exc->getTraceAsString());
        }
        return Redirect::to(URL::previous());
    }

    function getDeletar($id_reserva) {
        try {
            $reserva = Reserva::findOrFail($id_reserva);
            $id_cliente = $reserva->id_cliente;
            $this->rep->delete_reserva($reserva, \Auth::user()->id_usuario);
            Session::flash('success', 'A reserva ' . $id_reserva . ' foi cancelada, e os endereços foram disponibilizados com sucesso! ');
        } catch (Exception $exc) {
            Session::flash('errors', $exc->getTraceAsString());
        }
        return Redirect::to('/reserva/buscar');
    }

    function getInventario($id_reserva) {
        set_time_limit(0);
        // Busca a reserva
        //$reserva = Reserva::findOrfail($id_reserva);
        // Busco os enderecos vinculados
        $resdeses = ReservaDesmembrada::where('id_reserva', '=', $id_reserva)
                ->orderBy('id_caixapadrao')
                ->get();

        //pr($documentos);
        //return View::make('reserva_inventario', compact('documentos'));
        $html = View::make('reserva_inventario', compact('resdeses'));

        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);

        $this->mpdf = new mPDF('utf-8', 'A4', '', '', '2', '2', '2', '2', '', '');  // largura x altura
        $this->mpdf->allow_charset_conversion = false;

        $this->mpdf->WriteHTML($html);
        $this->mpdf->debug = true;
        $this->mpdf->Output();
    }

    public function getInventarioavulso($id_cliente = '') {
        if ($id_cliente != '') {
            Session::set('id_cliente', $id_cliente);
        }
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        return View::make('reserva_planilha_reimprimir')
                        ->with('clientes', $clientes)
        ;
    }

    public function postInventarioavulso() {
        set_time_limit(0);
        $msg = '';
        if (Input::get('id_cliente') == '') {
            $msg .= "<br/>- Informe o cliente";
        }
        if (trim(Input::get('id_caixapadrao')) == '' && Input::get('id_caixapadrao_ini') == '' && Input::get('id_caixapadrao_fim') == '') {
            $msg .= "<br/>- Informe alguma caixa para regerar a planilha";
        }
        if ($msg != '') {
            $msg = "Não foi possível gerar as planilhas:" . $msg;
            Session::set('errors', $msg);
            return Redirect::to('/reserva/inventarioavulso');
        }
        // Busca a reserva
        //$reserva = Reserva::findOrfail($id_reserva);
        // Busco os enderecos vinculados
        log::info('ini '.Input::get('id_caixapadrao_ini')).
        log::info('fim '.Input::get('id_caixapadrao_fim')).
        $resdeses = ReservaDesmembrada::docliente(Input::get('id_cliente'))
                ->idcaixapadraoin(Input::get('id_caixapadrao'))
                ->idcaixapadraointervalo(Input::get('id_caixapadrao_ini'), Input::get('id_caixapadrao_fim'))
                ->orderBy('id_caixapadrao')
                ->get();
        $queries = DB::getQueryLog();
        $query = end($queries);
        log::info('Query', $query);
        if (count($resdeses) > 0) {
            $html = View::make('reserva_inventario', compact('resdeses'));

            $html = iconv("UTF-8", "UTF-8//IGNORE", $html);

            $this->mpdf = new mPDF('utf-8', 'A4', '', '', '2', '2', '2', '2', '', '');  // largura x altura
            $this->mpdf->allow_charset_conversion = false;

            $this->mpdf->WriteHTML($html);
            $this->mpdf->debug = true;
            $this->mpdf->Output();
        } else {
            $msg = "Nenhuma caixa encontrada com os números informados";
            Session::set('errors', $msg);
            return Redirect::to('/reserva/inventarioavulso');
        }
    }

    public function getImprimirinventario($id_cliente, $id_caixas) {
        set_time_limit(0);
        $id_caixas_array = explode(",", $id_caixas);
        $documentos = Documento::whereIn('documento.id_caixapadrao', $id_caixas_array)
                ->where('id_cliente', '=', $id_cliente)
                ->with(array('endereco', 'reserva'))
                ->get();
        //pr($documentos);
        //return View::make('reserva_inventario', compact('documentos'));
        $html = View::make('reserva_inventario', compact('documentos'));

        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);

        $this->mpdf = new mPDF('utf-8', 'A4', '', '', '2', '2', '2', '2', '', '');  // largura x altura
        $this->mpdf->allow_charset_conversion = false;

        $this->mpdf->WriteHTML($html);
        $this->mpdf->debug = true;
        $this->mpdf->Output();
    }

    function getRelatorio($id_reserva) {
        set_time_limit(0);
        $qtdRegPag = 198;
        // Busca a reserva
        $reserva = Reserva::findOrfail($id_reserva);
        // Busco os enderecos vinculados
        $mpdf = new mPDF('utf-8', 'A4', '', '', '10', '10', '10', '10', '', '');  // largura x altura
        $mpdf->allow_charset_conversion = false;

        $sql = "select distinct id_caixapadrao from documento join endereco on endereco.id_caixa = documento.id_caixa and documento.id_reserva = ".$id_reserva;

        $res = \DB::select(\DB::raw($sql));
        $qtd = count($res);

        $qtdPag = ceil($qtd/$qtdRegPag);

        $mpdf = $this->geraRelatorio($mpdf, $reserva, $qtdRegPag, $qtd, $qtdPag);
        $mpdf->AddPage('P','', 1, '', '', 10, 10, 10, 10, '', '');
        $mpdf = $this->geraRelatorio($mpdf, $reserva, $qtdRegPag, $qtd, $qtdPag);

        $saida = $mpdf->Output('', 'S');
        $response = Response::make($saida);
        $response->header('Content-Type', 'application/pdf');
        return $response;
    }

    public function geraRelatorio($mpdf, $reserva, $qtdRegPag, $qtd, $qtdPag){

        $pag = 1;
        $ind = 0;
        $documentos = Documento::where('id_reserva', '=', $reserva->id_reserva)
                ->join('endereco', 'endereco.id_caixa', '=', 'documento.id_caixa')
                ->select('id_caixapadrao')->distinct()
                ->chunk($qtdRegPag, function ($documentos) use (&$mpdf, $reserva, &$pag, $qtdPag, &$ind, $qtd, $qtdRegPag ) {
                    $html = View::make('reserva_relatorio', compact('reserva', 'documentos', 'pag', 'qtdPag', 'ind', 'qtd'));
                    $html = iconv("UTF-8", "UTF-8//IGNORE", $html);

                    if ($pag > 1){
                        $mpdf->AddPage('P', '', 1);
                    }

                    $mpdf->WriteHTML($html);

                    $pag++;
                    $ind = $ind+$qtdRegPag;
                });

        return $mpdf;
    }

}
