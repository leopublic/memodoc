<?php

class FaturamentoController extends BaseController {

    public function getNovoperiodo() {
        return View::make('periodo_faturamento_novo')
        ;
    }

    public function postNovoperiodo() {
        return View::make('periodo_faturamento_novo')
        ;
    }

    public function getPeriodo($mes = '', $ano = '') {
        if ($mes == '') {
            $mes = date('m');
        }
        if ($ano == '') {
            $ano = date('Y');
        }
        $clientes = Cliente::ativos()->orderBy('razaosocial')->get();
        return View::make('faturamento_faturar')
                        ->with('clientes', $clientes)
                        ->with('mes', $mes)
                        ->with('ano', $ano)
        ;
    }

    public function postPeriodo() {
        $mes = Input::get('mes');
        $ano = Input::get('ano');
        return Redirect::to('/faturamento/periodo/' . $mes . '/' . $ano);
    }

    public function getRecalcularparalistaporcliente($id_faturamento){
        $this->recalcular($id_faturamento);
        $fat = \Faturamento::find($id_faturamento);
        return Redirect::to('/faturamento/porcliente/'.$fat->id_cliente)->with('success', 'Faturamento recalculado com sucesso');
    }

    public function getRecalcular($id_faturamento) {
        $this->recalcular($id_faturamento);
        return Redirect::to('/faturamento/visualizar/'.$id_faturamento)->with('success', 'Faturamento recalculado com sucesso');
    }

    public function recalcular($id_faturamento){
        set_time_limit(0);
        $fat = \Faturamento::find($id_faturamento);
        $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $repfat = new \Memodoc\Repositorios\RepositorioFaturamentoEloquent;
        $perfat = \PeriodoFaturamento::find($fat->id_periodo_faturamento);
        $rep->recalcular($perfat, Auth::user()->id_usuario, $id_faturamento);
        \DB::commit();
        $repfat->demonstrativoAnaliticoEmDisco($fat);
    }

    public function getCriardemonstrativo($id_faturamento){
        set_time_limit(0);
        $fat = \Faturamento::find($id_faturamento);
        $rep = new \Memodoc\Repositorios\RepositorioFaturamentoEloquent;
        $arq = $rep->demonstrativoAnaliticoEmDisco($fat);
        $content = file_get_contents($arq);
        return Response::make($content, 200, array('content-type'=>'application/pdf'));
    }

    public function getDemonstrativo($id_faturamento){
        $fat = \Faturamento::find($id_faturamento);
        $rep = new \Memodoc\Repositorios\RepositorioFaturamentoEloquent;
        $arq = $rep->demonstrativoAnaliticoEmDisco($fat, true);
        $content = file_get_contents($arq);
        return Response::make($content, 200, array('content-type'=>'application/pdf'));
    }

    public function getExcluir($id_faturamento){
        $fat = \Faturamento::find($id_faturamento);
        $id_periodo_faturamento= $fat->id_periodo_faturamento;
        $rep = new \Memodoc\Repositorios\RepositorioFaturamentoEloquent;
        $rep->excluir($fat);
        Session::flash('success', 'Cliente retirado com sucesso');
        return Redirect::to('/periodofat/detalhe/' . $id_periodo_faturamento);
    }

    /**
     * (Re)Processa um faturamento
     */
    public function getFaturar($id_faturamento) {
        $faturador = new Memodoc\Servicos\FaturamentoEloquent();
        $faturamento = Faturamento::where('id_faturamento', '=', $id_faturamento)
                ->first()
        ;
        $faturador->ProcessaFaturamento($faturamento);
        $rep = new \Memodoc\Repositorios\RepositorioFaturamentoEloquent;
        $mpdf = $rep->demonstrativoAnalitico($faturamento);
        return $mpdf->Output();
    }

    public function getVisualizar($id_faturamento, $origem = ''){
        $fat = \Faturamento::find($id_faturamento);
        $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        if ($origem == ''){
            $fat_prox = $rep->proximoFaturamento($fat);
            $fat_ant = $rep->anteriorFaturamento($fat);
        } else {
            $fat_prox = $rep->proximoFaturamentoCliente($fat);
            $fat_ant = $rep->anteriorFaturamentoCliente($fat);
        }
        $cliente = \Cliente::find($fat->id_cliente);
        $precos = new \Memodoc\Servicos\Precos($cliente);
        $precos->CarregaPrecos($fat);
        return View::make('faturamento_visualizar')
                        ->with('fat', $fat)
                        ->with('fat_ant', $fat_ant)
                        ->with('fat_prox', $fat_prox)
                        ->with('precos', $precos)
                        ->with('cliente', $cliente)
                        ->with('origem', $origem)
        ;
    }

    public function getRelatorioanalitico($id_faturamento) {
        $fat = \Faturamento::find($id_faturamento);
        $rep = new \Memodoc\Repositorios\RepositorioFaturamentoEloquent;
        $mpdf = $rep->demonstrativoAnalitico($fat);
        return $mpdf->Output();
    }

    public function getPorcliente($pid_cliente = ''){
        $id_cliente = Input::get('id_cliente', Input::old('id_cliente', $pid_cliente));
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        if($id_cliente > 0){
            $cliente = \Cliente::find($id_cliente);
            $faturamentos = \Faturamento::where('faturamento.id_cliente', '=', $id_cliente)
                            ->join('periodo_faturamento', 'periodo_faturamento.id_periodo_faturamento', '=', 'faturamento.id_periodo_faturamento')
                            ->join('cliente', 'cliente.id_cliente', '=', 'faturamento.id_cliente')
                            ->orderBy('periodo_faturamento.ano', 'desc')->orderBy('periodo_faturamento.mes', 'desc')
                            ->get()
                            ;
        } else {
            $cliente = null;
            $faturamentos = null;
        }

        return View::make('faturamento.docliente')
                        ->with('faturamentos', $faturamentos)
                        ->with('cliente', $cliente)
                        ->with('clientes', $clientes)
                        ->with('id_cliente', $id_cliente)
        ;
    }

    public function postPorcliente(){
        Input::flash();
        return Redirect::to('/faturamento/porcliente');
    }

    public function getPrevia(){
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');

        $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $dt_inicio = $rep->proximaDtInicio();
        $dt_fim = $rep->proximaDtFim($dt_inicio);

        return View::make('faturamento.previa')
                        ->with('clientes', $clientes)
                        ->with('id_cliente', '')
                        ->with('dt_inicio', $dt_inicio)
                        ->with('dt_fim', $dt_fim)
        ;
    }

    public function postPrevia(){
        Input::flash();
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        $post = Input::all();
        $id_cliente = Input::get('id_cliente', Input::old('id_cliente'));
        $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $fat = $rep->criaTemporario($post, Auth::user()->id_usuario);
        if ($fat){
            $repfat = new \Memodoc\Repositorios\RepositorioFaturamentoEloquent;
            // $caminho = null;
            $cliente = \Cliente::find($id_cliente);
            $precos = new \Memodoc\Servicos\Precos($cliente);
            $precos->CarregaPrecos($fat);
            $arq = $repfat->demonstrativoAnaliticoEmDisco($fat, false);
            $content = file_get_contents($arq);
            $rep->zeraTemporario(Auth::user()->id_usuario);
            return Response::make($content, 200, array('content-type'=>'application/pdf'));
        } else {
            Session::flash('errors', $rep->getErrors());
            $cliente = null;
            $fat = null;
            $precos = null;
            $caminho = null;
            return Redirect::to('/faturamento/previa');
        }
    }

    public function getListadocliente($id_cliente){
        return json_encode(array(array('id_faturamento' => '', 'anomesvalor' => '(não informado)')) + \DB::table('faturamento')
                ->join("periodo_faturamento", 'periodo_faturamento.id_periodo_faturamento', '=', 'faturamento.id_periodo_faturamento')
                ->where('faturamento.id_cliente', '=', $id_cliente)
                ->select('id_faturamento', DB::raw("concat(periodo_faturamento.ano,'/', periodo_faturamento.mes, ' R$ ', REPLACE(REPLACE(REPLACE(FORMAT(val_servicos_sem_iss + val_iss + val_cartonagem_enviadas, 2), '.', '@'), ',', '.'), '@', ',')) anomesvalor"))
                ->orderBy('periodo_faturamento.ano', 'desc')
                ->orderBy('periodo_faturamento.mes', 'desc')
                ->get())
                ;
    }

    public function getConferencia($id_periodo_faturamento, $id_faturamento = ''){
        $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
        $correio = \Faturamento::doperiodo($id_periodo_faturamento)
                    ->with('Cliente')
                    ->envioporcorreio()
                    ->get();

        $email = \Faturamento::doperiodo($id_periodo_faturamento)
                    ->envioporemail()
                    ->get();

        return View::make('faturamento.conferencia_tabela')
                        ->with('correio', $correio)
                        ->with('email', $email)
                        ->with('perfat', $perfat)
                            ->with('id_faturamento', $id_faturamento)
        ;

    }

    public function getConferir($id_faturamento){
        $rep = new \Memodoc\Repositorios\RepositorioFaturamentoEloquent;
        $rep->conferir($id_faturamento);
    }

    public function getDesconferir( $id_faturamento){
        $rep = new \Memodoc\Repositorios\RepositorioFaturamentoEloquent;
        $rep->desconferir($id_faturamento);
    }

    public function getAtualizarpreco( $id_faturamento){
    	$rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
    	$fat = \Faturamento::find($id_faturamento);
    	$rep->salvaEstruturaPrecoCliente($fat, \Auth::user()->id_usuario);
    }

    

}
