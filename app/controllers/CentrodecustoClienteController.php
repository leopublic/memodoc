<?php

class CentrodecustoClienteController extends BaseController {

    public function getIndex() {
        $centrocustos = CentroDeCusto::where('id_cliente', '=', Auth::user()->id_cliente)->orderBy('nome')
                    ->get();
        return View::make('centrodecusto_cliente_list')
                        ->with('instancias', $centrocustos)
        ;
    }

    public function postAdicionar(){
        $nome = Input::get('nome');
        $centro = new CentroDeCusto;
        $centro->nome = $nome;
        $centro->id_cliente = Auth::user()->id_cliente;
        $centro->save();
        return Redirect::to('/publico/centrodecusto')->with('success', 'Centro de custo adicionado com sucesso');
    }

    public function getExcluir($id_centrodecusto){
        DB::table('documento')->where('id_centro', '=', $id_centrodecusto)->update(array('id_centro' => null));
        CentroDeCusto::destroy($id_centrodecusto);
        return Redirect::to('/publico/centrodecusto')->with('success', 'Centro de custo excluído com sucesso');
    }

    public function postAlterar(){
        $centro = CentroDeCusto::find(Input::get('id_centrodecusto'));
        $centro->nome = Input::get('nome');
        $centro->save();
        return Redirect::to('/publico/centrodecusto')->with('success', 'Centro de custo atualizado com sucesso');
    }
}
