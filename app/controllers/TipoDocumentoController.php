<?php

class TipodocumentoController extends BaseController {

        /**
         * Listagem de Tipo documento
         * @return type View
         */
	public function getIndex()
	{
		Input::flash();
		$instancias = TipoDocumento::all();

		$colunas = array(
			array('titulo' =>'Descrição', "campo"=> 'descricao')
		);
		
		return View::make('modelo_list')
					->with('linkEdicao','tipodocumento/edit')
					->with('linkCriacao','tipodocumento/create')
					->with('linkRemocao','tipodocumento/delete')
					->with('titulo', 'Tipos de documento')
					->with('colunas', $colunas)
					->with('instancias', $instancias)
				;		
	}

        /**
         * Editar 
         * @param type $id
         * @return type View
         */
	public function getEdit($id)
	{
                $model = TipoDocumento::find($id);
                
               
                
		return View::make('tipodocumento_form', compact('model'));		
	}
        
	public function postEdit($id)
	{
                Input::flash();
                $model = TipoDocumento::findOrFail($id);
                $model->fill(Input::all());
                $id = $model->save();
                if($id){
                    Session::flash('success', 'Tipo documento foi atualizado com sucesso !');
                    return  Redirect::to('tipodocumento/index');
                }
			
	}
        
        
        /**
         * Create
         * @return type View
         */
	public function getCreate()
	{
		return View::make('tipodocumento_form');		
	}
        
	public function postCreate()
	{
                Input::flash();
                $model = new TipoDocumento();
                $model->fill(Input::all());
                $id = $model->save();
                if($id){
                    Session::flash('success', 'Tipo de documento cadastrado com sucesso !');
                    return  Redirect::to('tipodocumento/index');
                }
			
	}
        
	public function getDelete($id)
	{
            if(TipoDocumento::getPermiteDeletar($id)){
                
                $model = TipoDocumento::find($id);
                $model->delete();
                Session::flash('msg', 'Tipo de documento '.$model->descricao.' foi deletado com sucesso !');
                return  Redirect::to('tipodocumento/index');
                
            }else{
                Session::flash('error', 'Este tipo de documento possui vinculo com outros registros, não é possivel deletar !');
                return  Redirect::to('tipodocumento/index');
            }
				
	}
        
}