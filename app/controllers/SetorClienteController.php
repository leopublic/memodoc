<?php

class SetorClienteController extends BaseController {

    public function getIndex() {
        $centrocustos = SetCliente::where('id_cliente', '=', Auth::user()->id_cliente)->orderBy('nome')
                    ->get();
        return View::make('setor_cliente_list')
                        ->with('instancias', $centrocustos)
        ;
    }

    public function postAdicionar(){
        $nome = Input::get('nome');
        $centro = new SetCliente();
        $centro->nome = $nome;
        $centro->id_cliente = Auth::user()->id_cliente;
        $centro->save();
        return Redirect::to('/publico/setor')->with('success', 'Setor adicionado com sucesso');
    }

    public function getExcluir($id_setor){
        DB::table('documento')->where('id_setor', '=', $id_setor)->update(array('id_setor' => null));
        SetCliente::destroy($id_setor);
        return Redirect::to('/publico/setor')->with('success', 'Setor excluído com sucesso');
    }

    public function postAlterar(){
        $centro = SetCliente::find(Input::get('id_setor'));
        $centro->nome = Input::get('nome');
        $centro->save();
        return Redirect::to('/publico/setor')->with('success', 'Setor atualizado com sucesso');
    }
}
