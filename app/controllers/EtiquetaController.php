<?php

class EtiquetaController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function getGere($id_reserva) {

        $documentos = DB::table('reserva')
                ->join('documento', 'documento.id_reserva', '=', 'reserva.id_reserva')
                ->join('endereco', 'endereco.id_caixa', '=', 'documento.id_caixa')
                ->join('cliente', 'cliente.id_cliente', '=', 'reserva.id_cliente')
                ->join('tipodocumento', 'tipodocumento.id_tipodocumento', '=', 'reserva.id_tipodocumento')
                ->where('reserva.id_reserva', '=', $id_reserva)
                ->select('documento.id_caixapadrao', 'documento.migrada_de', 'documento.id_caixa', 'tipodocumento.descricao', 'reserva.data_reserva', 'cliente.razaosocial',  'cliente.nomefantasia', 'endereco.id_galpao', 'endereco.id_rua', 'endereco.id_predio', 'endereco.id_andar', 'endereco.id_unidade')
                ->distinct()
                ->get()
        ;
        $mpdf = $this->geraPdfEtiquetas($documentos);
        if ($mpdf) {
            return $mpdf->Output('Etiqueta'.date('Y_m_d_H_i_s'), 'D');
        }
    }


    public function getGeremovimentado($id_movimentacao) {
    	set_time_limit(0);
    	session_write_close();
    	 
        $documentos = DB::table('movimentacao_caixa')->where('id_movimentacao', '=', $id_movimentacao)
                ->join('documento', 'documento.id_caixa', '=', 'movimentacao_caixa.id_caixa')
                ->join('endereco', 'endereco.id_caixa', '=', 'documento.id_caixa')
                ->join('cliente', 'cliente.id_cliente', '=', 'endereco.id_cliente')
                ->join('reserva', 'reserva.id_reserva', '=', 'documento.id_reserva')
                ->join('tipodocumento', 'tipodocumento.id_tipodocumento', '=', 'documento.id_tipodocumento')
                ->select('documento.id_caixapadrao', 'documento.migrada_de', 'documento.id_caixa', 'tipodocumento.descricao', 'reserva.data_reserva', 'cliente.razaosocial', 'cliente.nomefantasia', 'endereco.id_galpao', 'endereco.id_rua', 'endereco.id_predio', 'endereco.id_andar', 'endereco.id_unidade')
                ->distinct()
                ->orderBy('endereco.id_galpao')->orderBy('endereco.id_rua')->orderBy('endereco.id_predio')->orderBy('endereco.id_andar')->orderBy('endereco.id_unidade')
                ->get()
        ;
        $mpdf = $this->geraPdfEtiquetas($documentos);
        if ($mpdf) {
            return $mpdf->Output('Etiqueta'.date('Y_m_d_H_i_s'), 'D');
        }
    }

    public function geraPdfEtiquetas($documentos, $config = null) {
        if ($config == null){
            $config = \ConfigEtiqueta::padrao()->first();
        }
        $especs = array(
            "altura" => $config->altura
            ,"largura" => $config->largura
            , "fontsize_titulo" => $config->fontsize_titulo
            , "fontsize_texto" => $config->fontsize_texto
            , "fontsize_endereco" => $config->fontsize_endereco
            , "fontsize_id" => $config->fontsize_id
            , "barcode_size" => $config->barcode_size
            , "barcode_height" => $config->barcode_height
            , "barcode_pr" => $config->barcode_pr
            , "largura_td_barras" => $config->largura_td_barras
            , "letter_spacing_titulo" => $config->letter_spacing_titulo
            , "border" => $config->border
            , "margem_central" => $config->margem_central

            , "doc_largura" => $config->doc_largura
            , "doc_altura" => $config->doc_altura
            , "doc_margem_left" => $config->doc_margem_left
            , "doc_margem_right" => $config->doc_margem_right
            , "doc_margem_top" => $config->doc_margem_top
            , "doc_margem_bottom" => $config->doc_margem_bottom
            , "doc_margem_header" => $config->doc_margem_header
            , "doc_margem_footer" => $config->doc_margem_footer
        );

        // log::info('Config', $especs);
        $data = array();
        if (count($documentos) > 0) {

            $mpdf = new mPDF('utf-8', array($especs['doc_largura'], $especs['doc_altura']), '', '', $especs['doc_margem_left'], $especs['doc_margem_right'], $especs['doc_margem_top'], $especs['doc_margem_bottom'], $especs['doc_margem_header'], $especs['doc_margem_footer'], 'P');  // largura x altura
            //		$stylesheet = file_get_contents('./css/bootstrap.css');
            $mpdf->allow_charset_conversion = false;
            $mpdf->debug = true;
            $lado = 'esq';
            $primeira = true;
            $num = 111111;
            $i = 1;
            foreach ($documentos as $doc) {
                if ($doc->data_reserva != '') {
                    $data_reserva = DateTime::createFromFormat('Y-m-d', $doc->data_reserva)->format('d/m/Y');
                } else {
                    $data_reserva = '!!/!!/!!!!';
                }
//                    "numero" => str_pad($doc->id_caixapadrao, 6, "0", STR_PAD_LEFT)
                $numero = str_pad($doc->id_caixapadrao, 6, "0", STR_PAD_LEFT);
                $endereco = $doc->id_galpao . '.' . substr('00' . $doc->id_rua, -2) . '.' . substr('00' . $doc->id_predio, -2) . '.' . substr('00' . $doc->id_andar, -2) . '.' . substr('0000' . $doc->id_unidade, -4);
                $cliente = $doc->nomefantasia;
                $migrada_de = $doc->migrada_de;

                // $numero = $num;
                // $endereco = $i.'.'.str_repeat($i, 2).'.'.str_repeat($i, 2).'.'.str_repeat($i, 2).'.'.str_repeat($i, 4);
                // $cliente = 'Praesent sit amet mauris id risus porta ultrices';
                // $migrada_de = '1234567890123456789';
                if (strlen($cliente) > 50){
                	$cliente = substr($cliente, 0, 50).'...';
                }
                //log::info('cliente antes:'.$cliente);
                $data[$lado] = array(
                      "numero" => $numero
                    , "tipo" => $doc->descricao
                    , "cliente" => $cliente
                    , "reserva" => $data_reserva
                    , "id" => $doc->id_caixa
                    , "migrada_de" => $migrada_de
                    , "endereco" => $endereco
                );

                // log::info('numero:'.$numero);
                // log::info('tipo:'.$doc->descricao);
                // log::info('cliente:'.$cliente);
                // log::info('reserva:'.$data_reserva);
                // log::info('id:'.$doc->id_caixa);
                // log::info('migrada_de:'.$migrada_de);
                // log::info('Endereco:'.$endereco);
                
                // Se já montou os dois lados, imprime
                if ($lado == 'dir') {
                    $html = View::make('etiqueta_caixa')
                                ->with('etiqueta', $data)
                                ->with('break', !$primeira)
                                ->with('especs', $especs);
                    log::info('html_antes'.$html);
                    $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
                    $mpdf->WriteHTML($html);
                    log::info('html'.$html);
                    $data = array();
                    $lado = 'esq';
                    $primeira = false;
                } else {
                    $lado = 'dir';
                }
                $num = $num + 111111;
                $i++;
            }
            // Se acabou com o lado direito, é porque não imprimiu acumulou a segunda etiqueta
            //
			if ($lado == 'dir') {
                $html = View::make('etiqueta_caixa')->with('etiqueta', $data)->with('break', !$primeira)
                                ->with('especs', $especs);
                $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
                $mpdf->WriteHTML($html);
                // log::info('html'.$html);
            }
            return $mpdf;
        } else {
            return null;
        }
    }

    public function getConfigura(){

    }

    public function getCancelar(){
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        return View::make('etiqueta_cancelar')
                        ->with('clientes', $clientes)
        ;
    }

    public function postCancelar(){
        Input::flash();
        if (Input::get('id_cliente') == ''){
            return Redirect::back()->with('errors', 'Informe o cliente');
        }
        if (Input::get('id_caixapadrao_ini') == ''){
            return Redirect::back()->with('errors', 'Informe o número da(s) caixa(s)');
        }

        $id_caixapadrao_ini = Input::get('id_caixapadrao_ini');
        $id_caixapadrao_fim = Input::get('id_caixapadrao_fim');
        if ($id_caixapadrao_fim == ''){
            $id_caixapadrao_fim = $id_caixapadrao_ini;
        }

        $cliente = \Cliente::find(Input::get('id_cliente'));
        $rep = new \Memodoc\Repositorios\RepositorioEnderecoEloquent;
        $caixas = $rep->enderecosNumIntervalo(Input::get('id_cliente'), $id_caixapadrao_ini, $id_caixapadrao_fim);
        if(count($caixas) == 0){
            return Redirect::back()->with('errors', 'Não foi encontrada nenhuma referência com o número informado para esse cliente. Verifique.');
        }

        $caixasAlt = \Documento::where('id_cliente', '=', Input::get('id_cliente'))
                        ->where('id_caixapadrao', '>=', $id_caixapadrao_ini)
                        ->where('id_caixapadrao', '<=', $id_caixapadrao_fim)
                        ->where('itemalterado', '=', 1)
                        ->where('titulo', '<>', 'CAIXA NOVA ENVIADA PARA CLIENTE')
                        ->count();
        if ($caixasAlt > 0){
            Session::flash('errors', 'Essas etiquetas não podem ser canceladas pois já estão em uso. Para cancelar esses endereços faça um expurgo.');
        }
        return View::make('etiqueta_cancelar_confirma')
                        ->with('caixas', $caixas)
                        ->with('razaosocial', $cliente->razaosocial)
            ;
    }

    public function postCancelarconfirmado(){
        try{
            $id_cliente = Input::get('id_cliente');
            $id_caixapadrao_ini = Input::get('id_caixapadrao_ini');
            $id_caixapadrao_fim = Input::get('id_caixapadrao_fim');
            if ($id_caixapadrao_fim == ''){
                $id_caixapadrao_fim = $id_caixapadrao_ini;
            }
            $rep = new \Memodoc\Repositorios\RepositorioEnderecoEloquent;
            $rep->cancelaIntervaloPelaReferencia($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim, \Auth::user()->id_usuario, "Cancelamento de etiqueta por intervalo");
            return Redirect::to('/etiqueta/cancelar')->with('msg', 'Etiquetas canceladas com sucesso');
        } catch (Exception $ex) {
            return Redirect::to('/etiqueta/cancelar')->with('errors', $ex->getMessage());
        }
    }

    public function getReimprimir($id_cliente = '') {
        if ($id_cliente != '') {
            Session::set('id_cliente', $id_cliente);
        }
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        return View::make('etiqueta_reimprimir')
                        ->with('clientes', $clientes)
        ;
    }

    public function postReimprimir() {
        Input::flash();
        $id_cliente = Input::get('id_cliente');
        $id_caixapadrao_ini = Input::get('id_caixapadrao_ini');
        $id_caixapadrao_fim = Input::get('id_caixapadrao_fim');
        $errors = array();
        if ($id_cliente == '') {
            $errors[] = 'Informe o cliente';
        }
        if ($id_caixapadrao_ini == '' && $id_caixapadrao_fim == '') {
            $errors[] = 'Informe o número das caixas';
        }
        if (count($errors) == 0) {
            $docs = $this->cursorEtiquetasDoIntervalo($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim);
            if(count($docs) > 0){
                Session::flash('ok', 'ok');
                return Redirect::back();
            } else {
                return Redirect::back()->withErrors('Caixas não encontradas');
            }
        } else {
            return Redirect::back()->withErrors($errors);
        }
    }

    public function cursorEtiquetasDoIntervalo($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim = '') {
        $documentos = DB::table('reserva')
                ->join('documento', 'documento.id_reserva', '=', 'reserva.id_reserva')
                ->join('endereco', 'endereco.id_caixa', '=', 'documento.id_caixa')
                ->join('cliente', 'cliente.id_cliente', '=', 'reserva.id_cliente')
                ->join('tipodocumento', 'tipodocumento.id_tipodocumento', '=', 'reserva.id_tipodocumento')
                ->where('reserva.id_cliente', '=', $id_cliente);
        if ($id_caixapadrao_ini > 0) {
            if ($id_caixapadrao_fim > 0) {
                $doc = $documentos->where('documento.id_caixapadrao', '>=', $id_caixapadrao_ini)
                        ->where('documento.id_caixapadrao', '<=', $id_caixapadrao_fim);
            } else {
                $doc = $documentos->where('documento.id_caixapadrao', '=', $id_caixapadrao_ini);
            }
        } else {
            if ($id_caixapadrao_fim > 0) {
                $doc = $documentos->where('documento.id_caixapadrao', '=', $id_caixapadrao_fim);
            }
        }
        $doc = $doc->select('documento.id_caixapadrao', 'documento.migrada_de', 'documento.id_caixa', 'tipodocumento.descricao', 'reserva.data_reserva', 'tipodocumento.descricao', 'cliente.razaosocial',  'cliente.nomefantasia', 'endereco.id_galpao', 'endereco.id_rua', 'endereco.id_predio', 'endereco.id_andar', 'endereco.id_unidade')
                ->distinct()
                ->get();
        return $doc;
    }

    public function getGereintervalo($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim = '') {
        $doc = $this->cursorEtiquetasDoIntervalo($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim);
        if (count($doc) > 0) {
            $mpdf = $this->geraPdfEtiquetas($doc);
            return $mpdf->Output('Etiqueta'.date('Y_m_d_H_i_s').".pdf", 'D');
        } else {
            return 'Caixas não encontradas (' . $id_caixapadrao_ini . ' a ' . $id_caixapadrao_fim . ' do cliente ' . $id_cliente . ')';
        }
    }

    public function getDebugintervalo($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim = '') {
        $doc = $this->cursorEtiquetasDoIntervalo($id_cliente, $id_caixapadrao_ini, $id_caixapadrao_fim);
        if (count($doc) > 0) {
            $mpdf = $this->geraDebugEtiquetas($doc);
//            return $mpdf->Output('Etiqueta'.date('Y_m_d_H_i_s'), 'D');
            return $mpdf->Output('Etiqueta'.date('Y_m_d_H_i_s').".pdf", 'D');
        } else {
            return 'Caixas não encontradas (' . $id_caixapadrao_ini . ' a ' . $id_caixapadrao_fim . ' do cliente ' . $id_cliente . ')';
        }
    }

    public function geraDebugEtiquetas($documentos) {

        $data = array();
        if (count($documentos) > 0) {

            $mpdf = new mPDF('utf-8', array(82, 129), '', '', '0.5', '0.5', '0.5', '0.5', '0', '0');  // largura x altura
            //      $stylesheet = file_get_contents('./css/bootstrap.css');
            $mpdf->allow_charset_conversion = false;
            $mpdf->debug = true;
            $mpdf->shrink_tables_to_fit=0;
            $lado = 'esq';
            $primeira = true;
            foreach ($documentos as $doc) {
                if ($doc->data_reserva != '') {
                    $data_reserva = DateTime::createFromFormat('Y-m-d', $doc->data_reserva)->format('d/m/Y');
                } else {
                    $data_reserva = '!!/!!/!!!!';
                }
                $data[$lado] = array(
                    "numero" => str_pad($doc->id_caixapadrao, 6, "0", STR_PAD_LEFT)
                    , "tipo" => $doc->descricao
                    , "cliente" => $doc->nomefantasia
                    , "reserva" => $data_reserva
                    , "id" => $doc->id_caixa
                    , "migrada_de" => $doc->migrada_de
                    , "endereco" => $doc->id_galpao . '.' . substr('00' . $doc->id_rua, -2) . '.' . substr('00' . $doc->id_predio, -2) . '.' . substr('00' . $doc->id_andar, -2) . '.' . substr('0000' . $doc->id_unidade, -4)
                );

                // Se já montou os dois lados, imprime
                if ($lado == 'dir') {
                    $html = View::make('etiqueta_caixa_debug')->with('etiqueta', $data)->with('break', !$primeira);
                    $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
                    $mpdf->WriteHTML($html);
                    $data = array();
                    $lado = 'esq';
                    $primeira = false;
                } else {
                    $lado = 'dir';
                }
            }
            // Se acabou com o lado direito, é porque não imprimiu acumulou a segunda etiqueta
            //
            if ($lado == 'dir') {
                $html = View::make('etiqueta_caixa_debug')->with('etiqueta', $data)->with('break', !$primeira);
                $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
                $mpdf->WriteHTML($html);
            }
            return $mpdf;
        } else {
            return null;
        }
    }

}