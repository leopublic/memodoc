<?php

class PeriodoFaturamentoController extends BaseController {

    public function getIndex() {
        $periodos = \PeriodoFaturamento::where('fl_temporario', '=', 0)->orderBy('ano', 'desc')->orderBy('mes', 'desc')->get();
        return View::make('periodofaturamento')
                        ->with('instancias', $periodos)
        ;
    }

    public function getNovo() {
        $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $dt_inicio = Input::old('dt_inicio', $rep->proximaDtInicio());
        $dt_fim = Input::old('dt_inicio', $rep->proximaDtFim($dt_inicio));
        $perfat = new \PeriodoFaturamento;
        $perfat->dt_inicio_fmt = $dt_inicio;
        $perfat->dt_fim_fmt = $dt_fim;
        $pendencias = $rep->pendencias($perfat->dt_inicio, $perfat->dt_fim);
        $existem_pendentes = $rep->existemPendentes();
        if (count($pendencias) > 0 || $existem_pendentes){
            $pode_abrir = false;
        } else {
            $pode_abrir = true;
        }
        return View::make('periodofaturamento_novo')
                        ->with('dt_inicio', $dt_inicio)
                        ->with('dt_fim', $dt_fim)
                        ->with('perfat', $perfat)
                        ->with('pode_abrir', $pode_abrir)
                        ->with('existem_pendentes', $existem_pendentes)
                        ->with('pendencias', $pendencias)
                        ;
    }

    public function postNovo() {
        try {
            $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
            $post = Input::all();
            $perfat = $rep->criar($post, Auth::user()->id_usuario);
            if ($perfat) {
                Session::flash('success', 'Faturamento criado com sucesso');
                return Redirect::to('/periodofat/detalhe/' . $perfat->id_periodo_faturamento);
            } else {
                Input::flash();
                Session::flash('errors', $rep->getErrors());
                return Redirect::to('/periodofat/novo');
            }
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('errors', $ex->getMessage());
            return Redirect::to('/periodofat/novo');
        }
    }

    public function getEdit($id_periodo_faturamento) {
        $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
        return View::make('periodofaturamento_novo')
                        ->with('perfat', $perfat)
        ;
    }

    public function postEdit() {
        try {
            $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
            $post = Input::all();
            $perfat = $rep->editar($post, Auth::user()->id_usuario);
            Session::flash('success', 'Faturamento atualizado com sucesso');
            return Redirect::to('/periodofat/detalhe/' . $perfat->id_periodo_faturamento);
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
            return Redirect::to('/periodofat/novo');
        }
    }

    public function getDetalhe($id_periodo_faturamento) {
        $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
        $faturados = \Faturamento::where('id_periodo_faturamento', '=', $id_periodo_faturamento)
                ->join('cliente', 'cliente.id_cliente', '=', 'faturamento.id_cliente')
                ->orderBy('razaosocial')
                ->get();
        $naofaturados = \Cliente::whereRaw('fl_ativo = 1 and id_cliente not in (select id_cliente from faturamento where id_periodo_faturamento = ' . $id_periodo_faturamento . ')')->orderBy('razaosocial')->get();
        return View::make('periodofaturamento_detalhe')
                        ->with('faturados', $faturados)
                        ->with('naofaturados', $naofaturados)
                        ->with('perfat', $perfat)
        ;
    }

    public function getSinteticoexcel($id_periodo_faturamento, $tipo = '', $conferencia = "") {
        $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
        $registros = \Faturamento::where('id_periodo_faturamento', '=', $id_periodo_faturamento)
                ->join('cliente', 'cliente.id_cliente', '=', 'faturamento.id_cliente')
                ->orderBy('razaosocial');

        $titulo_tipo = "Todos os clientes faturados";
        if ($tipo == 'correios'){
            $registros = $registros->envioporcorreio();
            $titulo_tipo = "Somente clientes que recebem faturamento pelo correio";
        } elseif ($tipo == "email"){
            $registros = $registros->envioporcorreio();
            $titulo_tipo = "Somente clientes que recebem faturamento por e-mail";
        }
        $registros = $registros->get();
        $html = View::make('periodofaturamento_excel')
                ->with('registros', $registros)
                ->with('perfat', $perfat)
                ->with('conferencia', $conferencia)
                ->with('titulo_tipo', $titulo_tipo)
        ;
        $nome = 'fat_sintetico_' . substr('00' . $perfat->mes, -2) . '_' . $perfat->ano . '.xls';

        $html = iconv("UTF-8", "ISO-8859-1", $html);

        $response = Response::make($html);
        $response->header('Content-Type', 'application/vnd.ms-excel; ');
        $response->header('Content-Disposition', 'attachment; filename="' . $nome . '"');

        return $response;
    }

    public function getSinteticogeralexcel() {
        $sql = "select per.id_periodo_faturamento, per.dt_inicio, per.dt_fim
                    , concat(date_format(per.dt_inicio, '%d/%m/%Y'),' a ', date_format(per.dt_fim, '%d/%m/%Y')) periodo
                    , year(per.dt_fim) ano
                    , sum(coalesce(qtd_custodia_mes_anterior,0) )  qtd_custodia_mes_anterior
                    , sum(coalesce(qtd_custodia_novas, 0) ) qtd_custodia_novas
                    , sum(coalesce(qtd_expurgos, 0) ) qtd_expurgos
                    , sum(coalesce(qtd_expurgos_usadas, 0) ) qtd_expurgos_usadas
                    , sum(coalesce(qtd_custodia_periodo,0) ) qtd_custodia_periodo
                    , sum(coalesce(valorminimofaturamento,0) ) valorminimofaturamento
                    , sum(coalesce(qtd_custodia_em_carencia, 0) )  qtd_custodia_em_carencia
                    , sum(coalesce(val_custodia_periodo_cobrado, 0)) val_custodia_periodo_cobrado
                    , sum(coalesce(qtd_cartonagem_enviadas, 0)) qtd_cartonagem_enviadas
                    , sum(coalesce(val_cartonagem_enviadas, 0)) val_cartonagem_enviadas
                    , sum(coalesce(qtd_frete_dem, 0)) qtd_frete_dem
                    , sum(coalesce(val_frete, 0) + coalesce(val_frete_urgente, 0)) val_frete
                    , sum(coalesce(qtd_movimentacao, 0) + coalesce(qtd_movimentacao_urgente, 0) ) qtd_movimentacao
                    , sum(coalesce(val_movimentacao, 0) + coalesce(val_movimentacao_urgente, 0)) val_movimentacao
                    , sum(coalesce(val_servicos_sem_iss, 0)) val_servicos_sem_iss
                    , sum(coalesce(val_iss, 0)) val_iss
                    , sum(coalesce(val_dif_minimo, 0)) val_dif_minimo
                    , sum(coalesce(val_servicos_sem_iss, 0) + coalesce(val_iss, 0) + coalesce(val_cartonagem_enviadas, 0)) val_faturado
                from periodo_faturamento per , faturamento fat, cliente
                where fat.id_periodo_faturamento = per.id_periodo_faturamento
                and cliente.id_cliente = fat.id_cliente
                group by per.id_periodo_faturamento, per.dt_inicio, per.dt_fim, year(per.dt_fim)
            ";
        $registros = \DB::select(\DB::raw($sql));
        $titulo_tipo = "TOTALIZADOR FATURAMENTOS EM ".\Carbon::now('America/Sao_Paulo')->format('d/m/Y');
        $html = View::make('periodofaturamento_sintetico_geral_excel')
                ->with('registros', $registros)
                ->with('titulo_tipo', $titulo_tipo)
                ;
        $nome = 'fat_sintetico_geral_'.date('Y-m-d').'.xls';

        $html = iconv("UTF-8", "ISO-8859-1", $html);

        $response = Response::make($html);
        $response->header('Content-Type', 'application/vnd.ms-excel; ');
        $response->header('Content-Disposition', 'attachment; filename="' . $nome . '"');

        return $response;
    }

    public function getStatus($id_periodo_faturamento) {
        $sql = "select progresso, status, finalizado from periodo_faturamento where id_periodo_faturamento = " . $id_periodo_faturamento;
        $res = \DB::getPdo()->query($sql);
        $rs = $res->fetch(PDO::FETCH_BOTH);

//        $ret = array('progresso' => $perfat->progresso, 'status'=> $perfat->status, 'finalizado'=>$perfat->finalizado);
        $ret = array('progresso' => $rs['progresso'], 'status' => $rs['status'], 'finalizado' => $rs['finalizado']);
        return json_encode($ret);
    }

    public function getRecalcular($id_periodo_faturamento) {
        set_time_limit(0);
        try {
            $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
            $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
            $rep->recalcular($perfat, Auth::user()->id_usuario);
            Session::flash('success', 'Faturamento recalculado com sucesso');
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
        }
        return Redirect::to('/periodofat/detalhe/' . $id_periodo_faturamento);
    }

    public function getAdicionatodos($id_periodo_faturamento) {
        try {
            $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
            $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
            $rep->adicionarTodosClientesAtivos($perfat, Auth::user()->id_usuario);
            Session::flash('success', 'Clientes adicionados com sucesso');
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
        }
        return Redirect::to('/periodofat/detalhe/' . $id_periodo_faturamento);
    }

    public function getAdicionar($id_periodo_faturamento, $id_cliente) {
        $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
        $rep->adicionaCliente($perfat, $id_cliente, \Auth::user()->id_usuario);
        Session::flash('success', 'Cliente adicionado com sucesso');
        return Redirect::to('/periodofat/detalhe/' . $id_periodo_faturamento);
    }

    public function getRecarregaprecos($id_periodo_faturamento) {
        try {
            $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
            $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
            $rep->salvaEstruturaPreco($perfat, Auth::user()->id_usuario);
            Session::flash('success', 'Preços atualizados com sucesso');
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
        }
        return Redirect::to('/periodofat/detalhe/' . $id_periodo_faturamento);
    }

    public function getExcluir($id_periodo_faturamento) {
        try {
            $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
            $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
            if (is_object($perfat)) {
                $rep->excluir($perfat, Auth::user()->id_usuario);
                Session::flash('success', 'Faturamento excluído com sucesso');
            } else {
                Session::flash('error', 'Faturamento não foi encontrado');
            }
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
        }
        return Redirect::to('/periodofat');
    }

    public function postPeriodo() {
        $mes = Input::get('mes');
        $ano = Input::get('ano');
        return Redirect::to('/faturamento/periodo/' . $mes . '/' . $ano);
    }

    /**
     * Cria faturamentos pendentes para processamento
     *
     * @param array $id_cliente Clientes selecionados
     * @param string $dt_inicio
     * @param string $dt_fim
     */
    public function postCriafaturamentos() {

    }

    /**
     * (Re)Processa um faturamento
     *
     */
    public function getFaturar($id_cliente, $mes, $ano) {
        $faturador = new Memodoc\Servicos\FaturamentoEloquent();
        $faturamento = Faturamento::where('id_cliente', '=', $id_cliente)
                ->where('mes', '=', $mes)
                ->where('ano', '=', $ano)
                ->first()
        ;
        if (!is_object($faturamento)) {
            $faturamento = new Faturamento();
            $faturamento->id_cliente = $id_cliente;
            $faturamento->mes = $mes;
            $faturamento->ano = $ano;
            $faturamento->dt_inicio = \Carbon::createFromDate($ano, $mes, 21)->subMonth(1)->format('Y-m-d');
            $faturamento->dt_fim = \Carbon::createFromDate($ano, $mes, 20)->format('Y-m-d');
            $faturamento->id_status_faturamento = 0;
            $faturamento->save();
        }
        $faturador->ProcessaFaturamento($faturamento);
        return $this->relatorioAnalitico($faturamento);
    }

    public function relatorioAnalitico($faturamento) {
        // Cria o PDF
        $this->mpdf = new mPDF('utf-8', 'A4-L', '11px', 'Arial', '10', '10', '30', '20', '10', '10');  // largura x altura
        $this->mpdf->allow_charset_conversion = false;
        $this->mpdf->debug = true;

        // Monta o cabeçalho
        $cliente = $faturamento->cliente();
        $html = View::make('faturamento.analitico_cabecalho')
                ->with('faturamento', $faturamento)
                ->with('data', date('d/m/Y'))
                ->with('hora', date('H:i:s'))
        ;
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $this->mpdf->setHTMLHeader($html);

        // Monta do rodapé
        $this->mpdf->setFooter("||Página {PAGENO}/{nb}");

        // Monta a primeira parte do relatório
        $html = View::make('faturamento.analitico_inicio')
                ->with('faturamento', $faturamento)
        ;
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $this->mpdf->WriteHTML($html);

        // Monta as OS
        foreach ($faturamento->ordensdeservico as $os) {

            $html = View::make('faturamento.analitico_os')
                    ->with('os', $os)
            ;
            $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
            $this->mpdf->WriteHTML($html);
        }

        $html = View::make('faturamento.analitico_fim')
                ->with('faturamento', $faturamento)
        ;
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $this->mpdf->WriteHTML($html);


        return $this->mpdf->Output();
    }

    public function getInativar($id_cliente) {
        $cliente = \Cliente::find($id_cliente);
        $rep = new \Memodoc\Repositorios\RepositorioClienteEloquent;
        $rep->inative($cliente);
        Session::flash('success', 'Cliente inativado com sucesso');
        return Redirect::back();
    }

    public function getConcatenar($id_periodo_faturamento) {
        $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
        return $rep->criarDemonstrativoCompleto($perfat, Auth::user()->id_usuario);
    }

    public function getCriardemonstrativos($id_periodo_faturamento) {
        $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $rep->criarDemonstrativos($id_periodo_faturamento, Auth::user()->id_usuario);
    }

    public function getDemonstrativo($id_periodo_faturamento) {
        $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
        $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $name = 'DemonstrativoAnalitico_' . $perfat->ano . '_' . $perfat->mes.'.pdf';
        $caminho = $rep->caminhoArquivoCompleto($perfat);
        if (file_exists($caminho)) {
            return Response::download($rep->caminhoArquivoCompleto($perfat), $name);
        } else {
            return 'Arquivo não encontrado';
        }
    }

    public function getDemonstrativozip($id_periodo_faturamento) {
        $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
        $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $name = 'DemonstrativoAnalitico_Email_' . $perfat->ano . '_' . $perfat->mes;
        $caminho = $rep->caminhoArquivoCompleto($perfat);
        if (file_exists($caminho)) {
            return Response::download($caminho, $name);
        } else {
            return 'Arquivo não encontrado';
        }
    }

    public function getCancelarprocessamento($id_periodo_faturamento) {
        $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
        $perfat->status = "Cancelado pelo usuário";
        $perfat->finalizado = -1;
        $perfat->save();
        return Redirect::to('/periodofat/detalhe/' . $id_periodo_faturamento)->with('success', 'Processamento interrompido com sucesso.');
    }

    public function getProcessamentoiniciado($id_periodo_faturamento) {
        return Redirect::to('/periodofat/detalhe/' . $id_periodo_faturamento)->with('msg', 'Processamento iniciado com sucesso. O processamento continuará em segundo plano até que seja concluído.');
    }

    public function getCriardemonstrativosQueue($id_periodo_faturamento) {
        Queue::push('\Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent@criarDemonstrativosOffline', array('id_periodo_faturamento' => $id_periodo_faturamento, 'id_usuario' => Auth::user()->id_usuario));
//        $perfat = \PeriodoFaturamento::find($id_periodo_faturamento);
//        $rep->criarDemonstrativos($perfat, Auth::user()->id_usuario);
//        Session::flash('success', 'Demonstrativos gerados com sucesso');
        return Redirect::to('/periodofat/detalhe/' . $id_periodo_faturamento);
    }

    public function getFechar($id_periodo_faturamento){
        try{
            $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
            $rep->fechar($id_periodo_faturamento);
            Session::flash('success', 'Faturamento fechado com sucesso!');
        } catch(Exception $e){
            Session::flash('errors', 'Não foi possível fechar o faturamento:'.$e->getMessage());
        }
        return Redirect::to('/periodofat');
    }

    public function getAbrir($id_periodo_faturamento){
        try{
            $rep = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
            $rep->abrir($id_periodo_faturamento);
            Session::flash('success', 'Faturamento fechado com sucesso!');
        } catch(Exception $e){
            Session::flash('errors', 'Não foi possível fechar o faturamento:'.$e->getMessage());
        }
        return Redirect::to('/periodofat');
    }

    public function getGerarnotas($id_periodo_faturamento){
        try{
            $svc = new Memodoc\Servicos\ServicosNotaFiscal;
            $svc->cria_notas_de_faturamento($id_periodo_faturamento, \Auth::user()->id_usuario);    
            Session::flash('success', 'Notas geradas com sucesso!');
        } catch(Exception $e){
            Session::flash('errors', 'Não foi possível gerar as notas do faturamento:'.$e->getMessage());
        }
        return Redirect::to('/periodofat/detalhe/'.$id_periodo_faturamento);
        
    }
}
