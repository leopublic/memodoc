<?php

class OrdemServicoClienteController extends BaseController {

    protected $repOs;

    public function __construct() {
        $this->repOs = new \Memodoc\Repositorios\RepositorioOsEloquent;
    }

    public function getIndex() {
        $oss = Os::with('statusos')
                ->where('id_cliente', '=', Auth::user()->id_cliente)
        ;
        if (Input::old('responsavel') <> '') {
            $oss = $oss->where('responsavel', 'LIKE', '%' . Input::old('responsavel') . '%');
        }

        if (Input::old('id_status_os') <> '') {
            $oss = $oss->where('id_status_os', '=', Input::old('id_status_os'));
        } else {
            $oss = $oss->where('id_status_os', '<>', 1)->where('id_status_os', '<>', 5);
        }

        if (Input::old('solicitado_em') <> '') {
            $solicitado_em = DateTime::createFromFormat('d/m/Y', Input::old('solicitado_em'))->format('Y-m-d');
            $oss = $oss->where(DB::raw('DATE(solicitado_em)'), '=', $solicitado_em);
        }

        $oss = $oss->orderBy('id_os', 'desc')
                ->paginate(10);

        return View::make('ordemservico_cliente_listar')
                        ->with('oss', $oss)
                        ->with('model', new Os())
        ;
    }

    public function postIndex() {
        Input::flash();
        return Redirect::to('/publico/os')->withInput();
    }

    public function getCancelar($id_os_chave) {
        try {
            $os = Os::findOrFail($id_os_chave);
            $this->rep->cancele($os, Auth::user());
            Session::flash('success', 'A os (' . $os->id_os . ') foi cancelada com sucesso !');
        } catch (Exception $ex) {
            Session::flash('errors', 'Não foi possível cancelar a OS pois <br/>' . $ex->getMessage());
        }
        return Redirect::back();
    }

    public function getCriar() {
        $endeentrega = Endeentrega::comboCompletoDoCliente(Auth::user()->id_cliente);

        $rep = new Memodoc\Repositorios\RepositorioOsEloquent();
        $os = $rep->osPendente(Auth::user()->id_cliente, Auth::user()->id_usuario);

        return View::make('ordemservico_cliente_criar')
                        ->with('model', $os)
                        ->with('endeentrega', $endeentrega)
        ;
    }

    public function postCriar() {
        Input::flash();
        $rep = new \Memodoc\Repositorios\RepositorioOsEloquent;
        try {
            $post = Input::all();
            $os = $rep->salvaDoCliente($post, Input::has('acaoEncerrar'));
            Session::flash('success', 'Ordem de serviço ' . $os['id_os'] . ' recebida pela Memodoc com sucesso');
            return Redirect::to('/publico/os/');
        } catch (Exception $exc) {
            $os = Os::find(Input::get('id_os_chave'));
            Session::flash('errors', $exc->getMessage());
            $endeentrega = Endeentrega::comboCompletoDoCliente($os->id_cliente);

            return View::make('ordemservico_cliente_criar')
                            ->with('model', new Os)
                            ->with('endeentrega', $endeentrega)
            ;
        }
    }

    public function getVisualizar($id_os_chave) {

        $os = Os::findOrFail($id_os_chave);
        $qtd_itens_retirados = OsItemRetirado::where('id_os_chave', '=', $id_os_chave)->count();
        $cliente = $os->cliente;
        $documentos = $os->referenciasConsulta();
        $checkins = Checkin::where('id_os_chave', '=', $id_os_chave)->get();
        $endeentrega = Endeentrega::comboCompletoDoCliente($os->id_cliente);
        return View::make('ordemservico_cliente_visualizar')
                        ->with('model', $os)
                        ->with('documentos', $documentos)
                        ->with('qtd_itens_retirados', $qtd_itens_retirados)
                        ->with('checkins', $checkins)
                        ->with('cliente', $cliente)
                        ->with('endeentrega', $endeentrega)
        ;
    }

    /**
     * Adiciona emprestimo de caixa pela referencia
     * @param type $id_os_chave
     * @param type $id_caixapadrao
     * @return type
     */
    public function getAddcaixa($id_os_chave, $id_caixapadrao) {
        $ret = array();
        $id_cliente = Auth::user()->id_cliente;
        $doc = Documento::where('id_cliente', '=', $id_cliente)
                ->where('id_caixapadrao', '=', $id_caixapadrao)
                ->first();
        $os = \Os::find($id_os_chave);
        if ($os->id_cliente != $doc->id_cliente){
            $ret['titulo'] = 'Erro:';
            $ret['msg'] = 'Você mudou de cliente. Atualize a tela para adicionar itens no pedido';
            $ret['status'] = 'warning';
            
        } else {
            if (is_object($doc) && is_object($os)) {
                try {
                    $rep = new Memodoc\Repositorios\RepositorioOsEloquent();
                    $rep->adicionaCaixaComControle($os, $doc, true);
                    $ret['msg'] = 'Referência '.substr('000000'.$id_caixapadrao, -6).' solicitada com sucesso!';
                    $xx = View::make('referencias_os')
                            ->with('id_os_chave', $os->id_os_chave)
                            ->with('caixas', $os->referenciasConsulta())
                            ->render();
                    $ret['conteudo'] = $xx;
                    $ret['status'] = 'success';
                    $ret['titulo'] = 'Ok';
                } catch (\Exception $ex) {
                    $ret['titulo'] = 'Erro:';
                    $ret['msg'] = $ex->getMessage();
                    $ret['status'] = 'warning';
                }
            } else {
                $ret['titulo'] = 'Erro:';
                $ret['msg'] = "Referência ". substr('000000'.$id_caixapadrao, -6)." não encontrada. Tente novamente ";
                $ret['status'] = 'warning';
            }
            
        }
        return json_encode($ret);
    }

    function getAjaxdepartamento($id_cliente) {
        $departamentos = DepCliente::where('id_cliente', '=', $id_cliente)->get();
        $select = FormSelect($departamentos, 'nome');
        echo View::make('padrao.select_option', compact('select'));
    }

    function getAjaxendereco($id_cliente) {
        $enderecos = Endeentrega::where('id_cliente', '=', $id_cliente)->get();
        $select = FormSelect($enderecos, 'logradouro');
        echo View::make('padrao.select_option', compact('select'));
    }

    public function getImporte($pNomeTabela) {
        switch ($pNomeTabela) {
            case "cliente":
                Cliente::Recarregue();
                break;

            case "nivel":
                Nivel::Recarregue();
                break;

            case "usuario":
                Usuario::Recarregue();
                break;
        }
    }

    public function getAdicionarcaixa($id_caixa) {
        try {
            $rep = new \Memodoc\Repositorios\RepositorioOsEloquent();
            $os_pendente = $rep->osPendente(Auth::user()->id_cliente, Auth::user()->id_usuario);
            $doc = \Documento::where('id_caixa', '=', $id_caixa)->first();
            if ($doc->id_cliente != $os_pendente->id_cliente){
                $ret['status'] = 1;
                $ret['msg'] = 'Você mudou de cliente. Certifique-se de que está adicionando a caixa no pedido desejado';                
            } else {
                $rep->adicionaCaixa($os_pendente, $doc);
                $ret['status'] = 0;
                $ret['msg'] = 'Referência '.substr('000000'.$doc->id_caixapadrao, -6).' solicitada com sucesso!';                
            }
        } catch (Exception $ex) {
            $ret['status'] = 1;
            $ret['msg'] = $ex->getMessage();
        }
        return json_encode($ret);
    }

    public function getEncerrarpendente() {
        $rep = new \Memodoc\Repositorios\RepositorioOsEloquent();
        $os_pendente = $rep->osPendente(Auth::user()->id_cliente, Auth::user()->id_usuario);
        return Redirect::to('/publico/os/criar');
    }

    public function getDelemprestimo($id_osdesmembrada) {
        $ret = array();
        $rep = new \Memodoc\Repositorios\RepositorioOsEloquent;
        $osdesmembrada = \OsDesmembrada::find($id_osdesmembrada);
        try {
            $rep->retiraCaixaEmprestimo($id_osdesmembrada, \Auth::user()->id_usuario, 'Exclusão de empréstimo pelo operacional');
            $ret['msg'] = 'Referência removida com sucesso';
            $os = new \Os;
            $os->id_os_chave = $osdesmembrada->id_os_chave;
            $xx = View::make('referencias_os')
                        ->with('caixas', $os->referenciasConsulta())
                        ->render();
            $ret['conteudo'] = $xx;
            $ret['status'] = 'success';
            $ret['titulo'] = 'Ok';
        } catch (Exception $ex) {
            $ret['titulo'] = 'Erro:';
            $ret['msg'] = $ex->getMessage();
            $ret['status'] = 'warning';
        }
        return json_encode($ret);

    }

    public function getItensretirados($id_os_chave){
        $os = Os::findOrFail($id_os_chave);
        $sql = "select documento.id_caixapadrao, documento.id_item, codigo, campo1, campo2 
                from os_item_retirado, documento, item_remessa
                where documento.id_documento = os_item_retirado.id_documento
                  and item_remessa.id_item_remessa = os_item_retirado.id_item_remessa
                  and os_item_retirado.id_os_chave = ".$id_os_chave."
                order by documento.id_caixapadrao, documento.id_item";

        $itens = \DB::select(\DB::raw($sql));
        $printer = new \Memodoc\Servicos\ImpressoraOs;

        $mpdf = new \mPDF('utf-8', 'P', '', '', '10', '10', '65', '10', '10', '');  // largura x altura
        $mpdf->allow_charset_conversion = false;
        $mpdf = $printer->protocoloItens($mpdf, $os, $itens);
        $mpdf->Output();

    }

}
