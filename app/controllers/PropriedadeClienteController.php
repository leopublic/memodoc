<?php

class PropriedadeClienteController extends BaseController {

    public function getIndex() {
        $propriedade = \Propriedade::where('id_cliente', '=', \Auth::user()->id_cliente)->first();
        if ($propriedade->id_propriedade > 0){
            $valores = Valor::where('id_propriedade', '=', $propriedade->id_propriedade)->orderBy('nome')
                        ->get();            
        }
        
        foreach($valores as &$valor){
            $valor->qtd = DB::table('documento')->where('id_valor' , '=', $valor->id_valor)->count();
        }
        
        return View::make('propriedade_cliente_list')
                        ->with('propriedade', $propriedade)
                        ->with('valores', $valores)
        ;
    }

    public function postAdicionar(){
        $propriedade = \Propriedade::where('id_cliente', '=', \Auth::user()->id_cliente)->first();
        $nome = Input::get('nome');
        $valor = new Valor();
        $valor->nome = $nome;
        $valor->id_propriedade = $propriedade->id_propriedade;
        $valor->save();
        return Redirect::to('/publico/propriedade')->with('success', $propriedade->nome.' adicionada(o) com sucesso');
    }

    public function getExcluir($id_valor){
        $propriedade = \Propriedade::where('id_cliente', '=', \Auth::user()->id_cliente)->first();
        DB::table('documento')->where('id_valor', '=', $id_valor)->update(array('id_valor' => null));
        Valor::destroy($id_valor);
        return Redirect::to('/publico/propriedade')->with('success', $propriedade->nome.' excluída(o) com sucesso');
    }

    public function postAlterar(){
        $valor = Valor::find(Input::get('id_valor'));
        $propriedade = \Propriedade::where('id_cliente', '=', \Auth::user()->id_cliente)->first();
        $valor->nome = Input::get('nome');
        $valor->save();
        return Redirect::to('/publico/propriedade')->with('success', $propriedade->nome.' atualizada(o) com sucesso');
    }
}
