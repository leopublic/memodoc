<?php

class CheckoutController extends BaseController {

    public function getIndex() {
        $rep = new \Memodoc\Repositorios\RepositorioCheckoutEloquent;
        $caixas = $rep->doDiaEnviadas(date('Y-m-d'));
        return View::make('checkout')
                        ->with('caixas', $caixas)
        ;
    }

    public function postIndex() {
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        try{
            $rep->emprestaCaixa(Input::get('id_caixa'), \Auth::user()->id_usuario);
            return Redirect::to('/checkout')->with('success', 'Caixa retirada do galpão com sucesso');
        } catch (Exception $ex) {
            return Redirect::to('/checkout')->withErrors($ex->getMessage());
        }
    }

    public function getExpurgos() {
        $rep = new \Memodoc\Repositorios\RepositorioCheckoutEloquent;
        $caixas_expurgadas = $rep->doDiaExpurgadas(date('Y-m-d'));
        $qtd_expurgos = $rep->qtdExpurgosPendentes();
        return View::make('checkout_expurgos')
                        ->with('caixas_expurgadas', $caixas_expurgadas)
                        ->with('qtd_expurgos', $qtd_expurgos)
        ;
    }

    public function postExpurgos() {
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        try{
            $rep->registraSaidaExpurgo(Input::get('id_caixa'), \Auth::user()->id_usuario);
            return Redirect::to('/checkout/expurgos')->with('success', 'Expurgo baixado com sucesso');
        } catch (Exception $ex) {
            return Redirect::to('/checkout/expurgos')->withErrors($ex->getMessage());
        }
    }

    public function getOs($id_os_chave){
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $sql = "select "
                . " c.razaosocial"
                . ", os.id_os"
                . ", os.id_os_chave"
                . ", osd.id_osdesmembrada"
                . ", os.entregar_em"
                . ", date_format(os.entregar_em, '%d/%m/%Y') entregar_em_fmt"
                . ", osd.id_caixa, d.id_caixapadrao"
                . ", if(osd.data_checkout is null, 0, 1) data_checkout_ordem"
                . ",  data_checkout"
                . ", uck.nomeusuario"
                . ", if( date_format(entregar_em , '%Y%m%d') < date_format(now(), '%Y%m%d'), 0, 1) ordem"
                . " from osdesmembrada osd"
                . " join os on os.id_os_chave = osd.id_os_chave"
                . " join cliente c on c.id_cliente = os.id_cliente"
                . " join (select distinct id_caixa, id_caixapadrao from documento ) d on d.id_caixa = osd.id_caixa"
                . " left join usuario uck on uck.id_usuario = osd.id_usuario_checkout"
                . " where (osd.status < 1)"
                . "  and os.id_status_os > 1 "
                . "  and os.id_status_os <> 5 "
                . "  and os.id_os_chave =".$id_os_chave
                . " order by  data_checkout_ordem , ordem, id_os_chave , id_caixapadrao";
        $res = \DB::getPdo()->query($sql);
        $x = $res->fetchAll(\PDO::FETCH_BOTH);
        try{
            if (count($x) > 0){
                foreach($x as $osd){
                    $rep->emprestaCaixa($osd['id_caixa'], \Auth::user()->id_usuario);
                }
                return Redirect::to('/checkout')->with('success', 'Caixas retirada do galpão com sucesso');
            } else {
                return Redirect::to('/checkout')->with('errors', 'Nenhuma caixa encontrada');
            }
        } catch (Exception $ex) {
            return Redirect::to('/checkout')->withErrors($ex->getMessage());
        }
    }


    public function getRetirar($id_caixa_retirar) {
        $id_caixas = explode(",", Session::get('id_caixas'));
        $novas_caixas = "";
        $virg = "";
        foreach ($id_caixas as $id_caixa_atual) {
            if ($id_caixa_atual != $id_caixa_retirar) {
                $novas_caixas .= $virg . $id_caixa_atual;
                $virg = ",";
            }
        }
        Session::flash("msg", "Caixa retirada com sucesso.");
        Session::put('id_caixas', $novas_caixas);
        return Redirect::to('/checkout');
    }

    /**
     * Dá o checkout em todos os expurgos pendentes
     */
    public function getBaixarexpurgos($id_cliente = ''){
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        try{
            $qtd = $rep->checkoutTodosExpurgosPendentes(\Auth::user()->id_usuario, \Auth::user()->nomeusuario, $id_cliente);
            return Redirect::to('/checkout/expurgos')->with('success', $qtd.' expurgos pendentes foram baixados com sucesso');
        } catch (Exception $ex) {
            return Redirect::to('/checkout/expurgos')->withErrors($ex->getMessage());
        }        
    }
}
