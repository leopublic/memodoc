<?php

class ReservaClienteController extends BaseController {

    function getIndex() {
        $cliente = Auth::user()->cliente()->first();
        $reservas = Reserva::where('id_cliente', '=', Auth::user()->id_cliente)
                ->orderby('data_reserva', 'desc')
                ->get();

        return View::make('reserva_cliente_lista')
                        ->with('cliente', $cliente)
                        ->with('reservas', $reservas)
        ;
    }

    function getBuscar() {
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        //$model   = Reserva::orderby('data_reserva','desc');                    
        //$reservas = $model->take(100)->get();
        $reservas = Reserva::getListagem();
        return View::make('reserva_buscar')
                        ->with('clientes', $clientes)
                        ->with('model', new Reserva())
                        ->with('reservas', $reservas);
    }

    function postBuscar() {

        //pr(Input::all());

        $reservas = Reserva::getListagem(Input::get('id_cliente'), Input::get('inicio'), Input::get('fim'), Input::get('cancelada'));
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        $model = new Reserva();
        $model->id_cliente = Input::get('id_cliente');
        $model->inicio = Input::get('inicio');
        $model->fim = Input::get('fim');
        $model->cancelada = Input::get('cancelada');
        return View::make('reserva_buscar')
                        ->with('clientes', $clientes)
                        ->with('model', $model)
                        ->with('reservas', $reservas);
    }

    function getVisualizar($id_cliente, $id_reserva) {


        $cliente = Cliente::findOrFail($id_cliente);
        $reserva = Reserva::findOrFail($id_reserva);
        $documentos = Documento::where('id_cliente', '=', $id_cliente)
                        ->where('id_reserva', '=', $id_reserva)
                        ->orderby('id_caixapadrao')->get();

        return View::make('reserva_visualizar')
                        ->with('cliente', $cliente)
                        ->with('documentos', $documentos)
                        ->with('reserva', $reserva)
        ;
    }

    function getCancelar($id_reserva) {
        try {
            Reserva::cancelar($id_reserva);
            Session::flash('success', 'A reserva ' . $id_reserva . ' foi cancelada, e os endereços foram disponibilizados com sucesso! ');
        } catch (Exception $exc) {
            Session::flash('error', $exc->getTraceAsString());
        }
        return Redirect::to(URL::previous());
    }

    function getEtiqueta($id_reserva) {

        $html = View::make('reserva_etiqueta')->with('etiquetas', array());
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);

        $this->mpdf = new mPDF('utf-8', array(85, 133), '', '', '2', '2', '2', '2', '', '');  // largura x altura
        $this->mpdf->allow_charset_conversion = false;
        $this->mpdf->WriteHTML($html);
        $this->mpdf->debug = true;
        $this->mpdf->Output();
    }

}
