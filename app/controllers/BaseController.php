<?php
class BaseController extends Controller
{

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout() {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    public function date(){
        $datetime = new DateTime();
        return $datetime->setTimezone(new DateTimeZone(Config::get('app.timezone')))->format('Y-m-d H:i:s');
    }

    function simple($oneModel, $action, $manyModel, $id) {

        $attr = array();

        if ($action == 'edit') {

            $objMany = new $manyModel;
            $data = $objMany->find($id)->with($oneModel)->first();

            $one = $data[$oneModel];
            unset($data[$oneModel]);
            $many = $data;

            $attr['one'] = $one;
            $attr['many'] = $many;
            $attr['route_add'] = '';

            if (!empty($_POST)) {
                foreach ($attr['many']->getOriginal() as $col => $value) {
                    if (isset($_POST[$col])) {
                        $attr['many']->$col = $_POST[$col];
                    }
                }
                $attr['many']->save();
            }
            return $attr;
        }

        if ($action == 'create') {
            $objOneModel = new $oneModel;
            $data = $objOneModel->findOrFail($id);
            if (!empty($data)) {
                $objManyModel = new $manyModel;
                if (!empty($_POST)) {
                    $table = \DB::getDoctrineSchemaManager()->listTableDetails($objManyModel->getTable());
                    $columns = $table->getColumns();
                    $_POST[$data->getKeyName() ] = $data->getKey();
                     // relation one To many
                    foreach ($columns as $col => $value) {
                        if (isset($_POST[$col])) {
                            $objManyModel->$col = $_POST[$col];
                        }
                    }
                    $objManyModel->save();

                    $attr['many'] = $objManyModel;
                    $attr['one'] = $data;
                }
            }
        }

        if ($action == 'delete') {
            $objOneModel = new $oneModel;
            $data = $objOneModel->findOrFail($id);
            if (!empty($data)) {
                $objManyModel = new $manyModel;
                $objManyModel->findOrFail($id);
                $objManyModel->delete();

                $attr['many'] = $objManyModel;
                $attr['one'] = $data;
            }
        }
        return $attr;
    }
}
