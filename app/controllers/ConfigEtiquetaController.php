<?php

class ConfigEtiquetaController extends BaseController {

	public function getIndex(){
		$configs = ConfigEtiqueta::all();

		return View::make('config_etiqueta.list')
				->with('configs', $configs)
		;
	}

	public function getEdit($id = ""){
		if ($id > 0){
			$config = ConfigEtiqueta::find($id);
		} else {
			$config = new ConfigEtiqueta;
		}

		return View::make('config_etiqueta.edit')
				->with('objeto', $config)
		;
	}

	public function postEdit(){
		$id_config = Input::get('id_config');

		if ($id_config > 0){
			$config = ConfigEtiqueta::find($id_config);
		} else {
			$config = new ConfigEtiqueta;
		}
		$all = Input::all();
		$config->fill($all);
		$config->save();

	}

	public function getCriadefault(){
		$config = new \ConfigEtiqueta;
        $config->altura = "800";
        $config->largura = "150";
        $config->fontsize_titulo = "128";
        $config->fontsize_texto = "17";
        $config->fontsize_endereco = "43";
        $config->fontsize_id = "18";
        $config->barcode_size = "1.7";
        $config->barcode_height = "2.5";
        $config->barcode_pr = "1.7";
        $config->largura_td_barras = "130";

        $config->doc_largura = "144";
        $config->doc_altura = "84";
        $config->doc_margem_left = "5";
        $config->doc_margem_right = "3";
        $config->doc_margem_top = "4";
        $config->doc_margem_bottom = "3";
        $config->doc_margem_header = "0";
        $config->doc_margem_footer = "0";
        $config->save();
	}
}