<?php

class ExpurgoController extends BaseController {

    public function getIndex() {
        return $this->listaExpurgos();
    }

    public function postIndex() {
        Input::flash();
        return $this->listaExpurgos();
    }

    /**
     * Permite selecionar o cliente do expurgo que será criado
     */
    function getCliente() {
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        $repPerFat = new \Memodoc\Repositorios\RepositorioPeriodoFaturamentoEloquent;
        $agora = Carbon::now(new DateTimeZone(Config::get('app.timezone')));
        $faturamentosAbertos = $repPerFat->faturamentosFechadosNaData($agora->format('Y-m-d'));
        $readonly = null;
        if (count($faturamentosAbertos) > 0){
            $readonly = true;
        }
        return View::make('/expurgo_sel_cliente')
                        ->with('clientes', $clientes)
                        ->with('readonly', $readonly)
        ;
    }

    function postCliente(){
        if (Input::get('id_cliente') > 0){
            Input::flash();
            return Redirect::to('/expurgo/caixas');
        } else {
            Input::flash();
            return Redirect::to('/expurgo/cliente')->with('errors', 'Selecione o cliente');
        }
    }

    public function getPai($id_cliente = '', $id_expurgo_pai = '') {
        return $this->listaExpurgospai(false, $id_cliente, $id_expurgo_pai);
    }

    public function postPai() {
        Input::flash();
        return $this->listaExpurgospai(false);
    }

    public function getPaiinativo($id_cliente = '', $id_expurgo_pai = '') {
        return $this->listaExpurgospai(true, $id_cliente, $id_expurgo_pai);
    }

    public function postPaiinativo() {
        Input::flash();
        return $this->listaExpurgospai(true);
    }

    public function listaExpurgospai($inativos = false, $id_cliente = '', $id_expurgo_pai = ''){
        $id_cliente = Input::old('id_cliente', $id_cliente);
        if ($id_cliente > 0){
            $expurgos = ExpurgoPai::doCliente($id_cliente)->paginate(20);
        } else {
            $expurgos = ExpurgoPai::whereIn('id_cliente', function($query) use($inativos)
                   {
                       $query->select('id_cliente')
                             ->from('cliente')
                             ->where('fl_ativo', '=', !$inativos);
                   })
                        ->orderBy('data_solicitacao', 'desc')
                            ->orderBy('id_expurgo_pai', 'desc')
                            ->paginate(20);
        }
        if ($inativos){
            $titulo = "Consultar expurgos de clientes inativos";
            $clientes = FormSelect(Cliente::getInativos(), 'razaosocial');
        } else {
            $titulo = "Consultar expurgos de clientes (somente clientes ativos)";
            $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        }

        return View::make('expurgopai_listar')
                        ->with('expurgos', $expurgos)
                        ->with('id_cliente', $id_cliente)
                        ->with('id_expurgo_pai', $id_expurgo_pai)
                        ->with('clientes', $clientes)
                        ->with('titulo', $titulo)
            ;
    }

    public function listaExpurgos(){
        $id_cliente = Input::old('id_cliente', Input::get('id_cliente'));
        if ($id_cliente > 0){
            $expurgos = Expurgo::doCliente($id_cliente)->paginate(20);
        } else {
            $expurgos = Expurgo::orderBy('data_solicitacao', 'desc')
                            ->orderBy('id_caixapadrao', 'desc')
                            ->paginate(20);
        }

        return View::make('expurgo_listar')
                        ->with('expurgos', $expurgos)
                        ->with('clientes', FormSelect(Cliente::getAtivos(), 'razaosocial'))
            ;
    }

    public function getNovoos($id_os_chave){
        $trans = \Transbordo::where('id_os_chave', '=', $id_os_chave)->get();
        $exp = \ExpurgoPai::where('id_os_chave', '=', $id_os_chave)->get();

        if (count($trans) > 0){
            Session::flash('errors', 'Já existe um transbordo associado a essa OS');
            return Redirect::to('/osok/visualizar/'.$id_os_chave);
        } else {
            if (count($exp) > 0){
                Session::flash('errors', 'Já existe um expurgo associado a essa OS');
                return Redirect::to('/osok/visualizar/'.$id_os_chave);
            } else {
                $os = \Os::find($id_os_chave);
                $cliente = $os->cliente;
                $razaosocial = $cliente->razaosocial.' ('.substr('000'.$os->id_cliente, -3).')';
                Session::flash('msg', 'Adicione as caixas e clique em "Expurgar" para finalizar.');
                return View::make('expurgo.caixa_os')
                                ->with('os', $os)
                                ->with('id_cliente', $os->id_cliente)
                                ->with('clientes', FormSelect(Cliente::getAtivos(), 'razaosocial'))
                                ->with('razaosocial', $razaosocial)
                                ->with('cortesia', 0)
                                ->with('cortesia_movimentacao', 0)
                                ->with('fl_bloqueia_liberados', 0)
                    ;
            }
        }
    }

    public function postNovoos(){
        set_time_limit(0);
        $id_os_chave = Input::get('id_os_chave');
        $os = \Os::find($id_os_chave);
        $cliente = $os->cliente;
        $id_cliente = $os->id_cliente;
        $cortesia = Input::get('cortesia', 0);
        $cortesia_movimentacao = Input::get('cortesia_movimentacao', 0);
        $fl_bloqueia_liberados = Input::get('fl_bloqueia_liberados', 0);
        $id_caixas = Input::get('id_caixas');
        $all = Input::all();
        // \log::info('Post', $all);
        if ($id_caixas == ''){
            $id_caixas = array();
        }
        $acao = Input::get('acao');
        if ($acao == 'expurgar'){
            try{
                $rep = new \Memodoc\Repositorios\RepositorioExpurgoEloquent;
                // $exp = $rep->expurgueAvulsas($cliente, $id_caixas, Auth::user()->id_usuario, date('d/m/Y'), $cortesia, true, $cortesia_movimentacao, $id_os_chave, $fl_bloqueia_liberados);
                $exp = $rep->expurgueAvulsasComProcessamento($cliente, $id_caixas, Auth::user()->id_usuario, \Carbon::now('America/Sao_Paulo')->format('d/m/Y'), $cortesia, true, $cortesia_movimentacao, $id_os_chave, $fl_bloqueia_liberados);

                Session::flash('success', 'Referências expurgadas com sucesso!');
                return Redirect::to('/expurgo/pai/'.$id_cliente.'/'.$exp->id_expurgo_pai);
            } catch (Exception $ex) {
                Session::flash('errors', $ex->getMessage());
            }
        } else {
            $msg = '';
            $erro = '';
            $br = '';
            $adicionadas = '';
            $virg = '';
            $id_caixapadrao_ini = Input::get('id_caixapadrao_ini');
            $id_caixapadrao_fim = Input::get('id_caixapadrao_fim');
            if ($id_caixapadrao_fim == ''){
                if (strpos($id_caixapadrao_ini, ",") > 0){
                    $ids = explode(",", $id_caixapadrao_ini);
                    foreach($ids as $id){
                        if (in_array($id, $id_caixas)){
                            $erro .= $br.'Caixa número '.$id_caixapadrao_ini.' já foi adicionada anteriormente e foi ignorada.';
                            $br = '<br/>';
                        } else {
                            $doc = \Documento::where('id_cliente', '=', $id_cliente)
                                    ->where('id_caixapadrao', '=', $id)
                                    ->first();
                            if (is_object($doc) && $doc->id_documento > 0){
                                $id_caixas[] = $id;
                                $adicionadas .= $virg.$id;
                                $virg = ', ';
                            } else {
                                $erro .= $br.'Caixa número '.$id_caixapadrao_ini.' não existe no cliente.';
                                $br = '<br/>';
                            }
                        }
                    }
                } else {
                    if (in_array($id_caixapadrao_ini, $id_caixas)){
                        $erro .= $br.'Caixa número '.$id_caixapadrao_ini.' já foi adicionada anteriormente e foi ignorada.';
                        $br = '<br/>';
                    } else {
                        $doc = \Documento::where('id_cliente', '=', $id_cliente)
                                ->where('id_caixapadrao', '=', $id_caixapadrao_ini)
                                ->first();
                        if (is_object($doc) && $doc->id_documento > 0){
                            $id_caixas[] = $id_caixapadrao_ini;
                            $adicionadas .= $virg.$id_caixapadrao_ini;
                            $virg = ', ';
                        } else {
                            $erro .= $br.'Caixa número '.$id_caixapadrao_ini.' não existe no cliente.';
                            $br = '<br/>';
                        }

                    }
                }
            } else {
                for ($index = $id_caixapadrao_ini; $index <= $id_caixapadrao_fim; $index++) {
                    if (in_array($index, $id_caixas)){
                        $erro .= $br.'Caixa número '.$index.' já foi adicionada anteriormente e foi ignorada.';
                        $br = '<br/>';
                    } else {
                        $doc = \Documento::where('id_cliente', '=', $id_cliente)
                                ->where('id_caixapadrao', '=', $index)
                                ->first();
                        if (is_object($doc) && $doc->id_documento > 0){
                            $id_caixas[] = $index;
                            $adicionadas .= $virg.$index;
                            $virg = ', ';
                        } else {
                            $erro .= $br.'Caixa número '.$index.' não existe no cliente.';
                            $br = '<br/>';
                        }
                    }
                }
            }
            if ($erro != ''){
                Session::flash('errors', $erro);
            }
            if ($adicionadas != ''){
                Session::flash('msg', 'Caixa(s) '.$adicionadas.' adicionada(s) com sucesso');
            }
        }
        if (count($id_caixas) > 0){
            $caixas = \Documento::where('id_cliente', '=', $id_cliente)
                            ->whereIn('id_caixapadrao', $id_caixas)
                            ->select(array('id_caixa', 'id_caixapadrao', DB::raw("date_format(primeira_entrada, '%d/%m/%Y') primeira_entrada"), DB::raw('datediff(now(), primeira_entrada) guardaDias'), DB::raw('datediff(now(), primeira_entrada)/365 guardaAnos'), 'emcasa'))
                            ->distinct()
                            ->orderBy('id_caixapadrao')->orderBy('id_item')
                            ->get();
        } else {
            $caixas = null;
        }
        return View::make('expurgo.caixa_os')
                        ->with('os', $os)
                        ->with('id_cliente', $id_cliente)
                        ->with('razaosocial', $cliente->razaosocial.' ('.substr('000'.$cliente->id_cliente, -3).')')
                        ->with('cortesia', $cortesia)
                        ->with('cortesia_movimentacao', $cortesia_movimentacao)
                        ->with('fl_bloqueia_liberados', $fl_bloqueia_liberados)
                        ->with('id_caixas', $id_caixas)
                        ->with('caixas', $caixas)
            ;
    }

    public function getProcesse($id_expurgo_pai){
        set_time_limit(0);
        $rep = new Memodoc\Repositorios\RepositorioExpurgoEloquent;
        $pendentes = Expurgo::where('id_expurgo_pai', '=', $id_expurgo_pai)
                    ->where('fl_processado', '=', 0)
                    ->chunk(50, function($expurgos) use ($rep){
            foreach($expurgos as $expurgo){
                print '<br/> Processando '.$expurgo->id_expurgo;
                $rep->processe_expurgo($expurgo);
            }
        });
    }

    public function getProcessependentes(){
        set_time_limit(0);
        $rep = new Memodoc\Repositorios\RepositorioExpurgoEloquent;
        $pendentes = Expurgo::where('fl_processado', '=', 0)
                    ->chunk(50, function($expurgos) use ($rep){
            foreach($expurgos as $expurgo){
                print '<br/> Processando '.$expurgo->id_expurgo;
                $rep->processe_expurgo($expurgo);
            }
        });
    }

    public function getCaixasgravadas($id_expurgo_pai){
        $exppai = \ExpurgoPai::find($id_expurgo_pai);
        $caixas = \Expurgo::where('id_expurgo_pai', '=', $id_expurgo_pai)
                        ->with('endereco')
                        ->get();
        $cliente = \Cliente::find($exppai->id_cliente);
        if ($exppai->id_os_chave > 0){
            $os = \Os::find($exppai->id_os_chave);
            $id_os = substr('000000'.$os->id_os, -6);
        } else {
            $id_os = "(expurgo sem OS)";
        }

        return View::make('expurgo_caixasgravadas')
                        ->with('exppai', $exppai)
                        ->with('caixas', $caixas)
                        ->with('id_os', $id_os)
                        ->with('razaosocial', $cliente->razaosocial.' ('.substr('000'.$cliente->id_cliente, -3).')')
            ;
    }



    public function getRelacaooperacional($id_expurgo_pai){
        set_time_limit(0);
        $exp = \ExpurgoPai::find($id_expurgo_pai);
        $desmembrada= \Expurgo::where('id_expurgo_pai', '=', $id_expurgo_pai)
                        ->join('endereco', 'endereco.id_caixa', '=', 'expurgo.id_caixa')
                        ->orderBy('id_galpao')
                        ->orderBy('id_rua')
                        ->orderBy('id_predio')
                        ->orderBy('id_andar')
                        ->orderBy('id_unidade')
                        ->get();

        $html = \View::make('expurgo_relacaodecaixas_operacional', compact('exp', 'desmembrada'));

        $pdf = PDF::loadHTML($html);
        $pdf->setOption('zoom', '1.25');
        return $pdf->download('relacao_operacional_'.$exp->id_expurgo_pai.'.pdf');
    }

    public function getRelacaodocumentos($id_expurgo_pai){
        set_time_limit(0);
        $exp = \ExpurgoPai::find($id_expurgo_pai);

        if ($exp->id_os_chave > 0){
            $os = \Os::find($exp->id_os_chave);
        } else {
            $os = new \Os;
        }

        $data = \Carbon::now('America/Sao_Paulo')->format('d/m/Y');
        $head = \View::make('expurgo_relacaodocumentos_cab', compact('exp', 'data', 'os'));

        $expurgos = \DB::table('expurgo_pai')
                ->join('expurgo', 'expurgo.id_expurgo_pai', '=', 'expurgo_pai.id_expurgo_pai')
                ->join('documento_cancelado', 'documento_cancelado.id_caixa', '=', 'expurgo.id_caixa')
                ->where('expurgo_pai.id_expurgo_pai', '=', $id_expurgo_pai)
                ->select(array("documento_cancelado.id_caixapadrao", "documento_cancelado.titulo", "documento_cancelado.id_item" , "documento_cancelado.conteudo", \DB::raw("date_format(documento_cancelado.inicio, '%d/%m/%Y') inicio_fmt"), \DB::raw("date_format(documento_cancelado.fim, '%d/%m/%Y') fim_fmt")))
                ->orderBy('documento_cancelado.id_caixapadrao')
                ->orderBy('documento_cancelado.id_item')
                ->get();
        $html = \View::make('expurgo_relacaodocumentos', compact('exp', 'expurgos'));

        $pdf = PDF::loadHTML($html);
        $pdf->setOption('orientation', 'Landscape');
        $pdf->setOption('zoom', '1.25');
        $pdf->setOption('header-html', $head);
        return $pdf->download('relacao_caixas_expurgadas_'.$exp->id_expurgo_pai.'.pdf');
    }

    public function getCaixas(){
        $id_cliente = Input::old('id_cliente', Input::get('id_cliente'));
        $cliente = \Cliente::find($id_cliente);
        $razaosocial = $cliente->razaosocial.' ('.substr('000'.$id_cliente, -3).')';
        $cortesia = Input::old('cortesia');
        $fl_bloqueia_liberados = Input::old('fl_bloqueia_liberados');
        Session::flash('msg', 'Adicione as caixas e clique em "Expurgar" para finalizar.');
        return View::make('expurgo_caixas')
                        ->with('id_cliente', $id_cliente)
                        ->with('clientes', FormSelect(Cliente::getAtivos(), 'razaosocial'))
                        ->with('razaosocial', $razaosocial)
                        ->with('cortesia', $cortesia)
                        ->with('fl_bloqueia_liberados', $fl_bloqueia_liberados)
            ;
    }

    public function postCaixas(){
        $id_cliente = Input::get('id_cliente');
        $cliente = \Cliente::find($id_cliente);
        $cortesia = Input::get('cortesia', 0);
        $fl_bloqueia_liberados = Input::get('fl_bloqueia_liberados', 0);
        $id_caixas = Input::get('id_caixas');
        if ($id_caixas == ''){
            $id_caixas = array();
        }
        if (Input::has('expurgar')){
            try{
                $rep = new \Memodoc\Repositorios\RepositorioExpurgoEloquent;
                $exp = $rep->expurgueAvulsas($cliente, $id_caixas, Auth::user()->id_usuario, date('d/m/Y'), $cortesia, true, 0, null, $fl_bloqueia_liberados);
                Session::flash('success', 'Referências expurgadas com sucesso!');
                return Redirect::to('/expurgo/pai/'.Input::get('id_cliente').'/'.$exp->id_expurgo_pai);
            } catch (Exception $ex) {
                Session::flash('errors', $ex->getMessage());
            }
        } else {
            $msg = '';
            $erro = '';
            $br = '';
            $adicionadas = '';
            $virg = '';
            $id_caixapadrao_ini = Input::get('id_caixapadrao_ini');
            $id_caixapadrao_fim = Input::get('id_caixapadrao_fim');
            if ($id_caixapadrao_fim == ''){
                if (in_array($id_caixapadrao_ini, $id_caixas)){
                    $erro .= $br.'Caixa número '.$id_caixapadrao_ini.' já foi adicionada anteriormente e foi ignorada.';
                    $br = '<br/>';
                } else {
                    $doc = \Documento::where('id_cliente', '=', Input::get('id_cliente'))
                            ->where('id_caixapadrao', '=', $id_caixapadrao_ini)
                            ->first();
                    if (is_object($doc) && $doc->id_documento > 0){
                        $id_caixas[] = $id_caixapadrao_ini;
                        $adicionadas .= $virg.$id_caixapadrao_ini;
                        $virg = ', ';
                    } else {
                        $erro .= $br.'Caixa número '.$id_caixapadrao_ini.' não existe no cliente.';
                        $br = '<br/>';
                    }
                }
            } else {
                for ($index = $id_caixapadrao_ini; $index <= $id_caixapadrao_fim; $index++) {
                    if (in_array($index, $id_caixas)){
                        $erro .= $br.'Caixa número '.$index.' já foi adicionada anteriormente e foi ignorada.';
                        $br = '<br/>';
                    } else {
                        $doc = \Documento::where('id_cliente', '=', Input::get('id_cliente'))
                                ->where('id_caixapadrao', '=', $index)
                                ->first();
                        if (is_object($doc) && $doc->id_documento > 0){
                            $id_caixas[] = $index;
                            $adicionadas .= $virg.$index;
                            $virg = ', ';
                        } else {
                            $erro .= $br.'Caixa número '.$index.' não existe no cliente.';
                            $br = '<br/>';
                        }
                    }
                }
            }
            if ($erro != ''){
                Session::flash('errors', $erro);
            }
            if ($adicionadas != ''){
                Session::flash('msg', 'Caixa(s) '.$adicionadas.' adicionada(s) com sucesso');
            }
        }
        if (count($id_caixas) > 0){
            $caixas = \Documento::where('id_cliente', '=', Input::get('id_cliente'))
                            ->whereIn('id_caixapadrao', $id_caixas)
                            ->select(array('id_caixa', 'id_caixapadrao', DB::raw("date_format(primeira_entrada, '%d/%m/%Y') primeira_entrada"), DB::raw('datediff(now(), primeira_entrada) guardaDias'), DB::raw('datediff(now(), primeira_entrada)/365 guardaAnos')))
                            ->distinct()
                            ->orderBy('id_caixapadrao')->orderBy('id_item')
                            ->get();
        } else {
            $caixas = null;
        }
        return View::make('expurgo_caixas')
                        ->with('id_cliente', $id_cliente)
                        ->with('razaosocial', $cliente->razaosocial.' ('.substr('000'.$cliente->id_cliente, -3).')')
                        ->with('cortesia', $cortesia)
                        ->with('id_caixas', $id_caixas)
                        ->with('caixas', $caixas)
                        ->with('razaosocial', $cliente->razaosocial)
            ;


    }

    public function postCaixasconfirmado(){
        $rep = new \Memodoc\Repositorios\RepositorioExpurgoEloquent;
        try{
            $cliente = \Cliente::find(Input::get('id_cliente'));
            $id_caixapadrao_ini = Input::get('id_caixapadrao_ini');
            $id_caixapadrao_fim = Input::get('id_caixapadrao_fim');
            $id_caixapadrao_fim = Input::get('id_caixapadrao_fim');
            if ($id_caixapadrao_fim == ''){
                $id_caixapadrao_fim = $id_caixapadrao_ini;
            }
            $exp = $rep->expurgueIntervalo($cliente, $id_caixapadrao_ini, $id_caixapadrao_fim, Auth::user()->id_usuario, date('d/m/Y'));
            Session::flash('success', 'Referências expurgadas com sucesso!');
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
        }
        return Redirect::to('/expurgo/pai/'.Input::get('id_cliente').'/'.$exp->id_expurgo_pai);

    }

    public function getCaixasexpurgadas($inativo = 0){
        $rep = new Memodoc\Repositorios\RepositorioExpurgoEloquent;
        return $rep->lista_caixas_expurgadas($inativo, Input::get('id_cliente'), Input::get('id_caixapadrao'), Input::get('orderby', 'id_caixapadrao'));
    }

    public function postCaixasexpurgadas($inativo = 0){
        $rep = new Memodoc\Repositorios\RepositorioExpurgoEloquent;
        return $rep->lista_caixas_expurgadas($inativo, Input::get('id_cliente'), Input::get('id_caixapadrao'), Input::get('orderby', 'id_caixapadrao'));
    }

}
