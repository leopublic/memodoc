<?php

class InventarioController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getImporte($pNomeTabela)
	{
		switch($pNomeTabela){
			case "cliente":
				Cliente::Recarregue();
				break;
			
			case "nivel":
				Nivel::Recarregue();
				break;

			case "usuario":
				Usuario::Recarregue();
				break;
			
		}
	}

}