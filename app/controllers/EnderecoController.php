<?php

class EnderecoController extends BaseController {

    function getBuscar() {
        return View::make('endereco_buscar')->with('model', new Endereco())->with('tipodocumento', FormSelect(TipoDocumento::All(), 'descricao'));
    }

    function postBuscar() {
        $id_galpao = Input::get('id_galpao');
        $id_predio = Input::get('id_predio');
        $id_rua = Input::get('id_rua');
        $id_andar = Input::get('id_andar');

        $endereco = Endereco::where('id_galpao', '=', $id_galpao)->where('id_predio', '=', $id_predio)->where('id_rua', '=', $id_rua)->where('id_andar', '=', $id_andar)->first();

        $qtd_endereco = Endereco::where('id_galpao', '=', $id_galpao)->where('id_predio', '=', $id_predio)->where('id_rua', '=', $id_rua)->where('id_andar', '=', $id_andar)->count();

        return View::make('endereco_buscar')->with('model', new Endereco())->with('endereco', $endereco)->with('qtd_endereco', $qtd_endereco)->with('tipodocumento', FormSelect(TipoDocumento::All(), 'descricao'));
    }

    function postEditar() {
        $id_galpao = Input::get('id_galpao');
        $id_predio = Input::get('id_predio');
        $id_rua = Input::get('id_rua');
        $id_andar = Input::get('id_andar');
        $qtd_unidades = Input::get('qtd_unidade');
        $id_tipodocumento = Input::get('id_tipodocumento');

        try {
            Endereco::alteracaoQtdPorAndar($id_galpao, $id_predio, $id_rua, $id_andar, $id_tipodocumento, $qtd_unidades);
            Session::flash('success', 'Foram atualizados a quantidade de unidades no andar informado');
        } catch (Exception $exc) {
            Session::flash('error', $exc->getTraceAsString());
        }

        return Redirect::to('/endereco/buscar');
    }

    function getNovo() {
        return View::make('endereco_novo')->with('model', new Endereco())->with('tipodocumento', FormSelect(TipoDocumento::All(), 'descricao'));
    }

    function postNovo() {
        $id_galpao = Input::get('id_galpao');
        $id_rua = Input::get('id_rua');
        $qtd_predio = Input::get('qtd_predio');
        $qtd_andares_par = Input::get('qtd_andar_par');
        $qtd_andares_impar = Input::get('qtd_andar_impar');
        $qtd_apartamento_andar = Input::get('qtd_apartamento_andar');
        $id_tipodocumento = Input::get('id_tipodocumento');

        $tags = Endereco::Novo($id_galpao, $id_rua, $qtd_predio, $qtd_andares_par, $qtd_andares_impar, $qtd_apartamento_andar, $id_tipodocumento);

        if (!empty($tags)) {
            Session::flash('success', 'Os endereços foram criados !');
        }

        return View::make('endereco_novo')->with('model', new Endereco())->with('tipodocumento', FormSelect(TipoDocumento::All(), 'descricao'))->with('tags', $tags);
    }

    public function getConteudoandar($id_galpao = "", $id_rua = "", $id_predio = "", $id_andar = "") {
        $id_galpao = Input::get('id_galpao', Input::old('id_galpao', $id_galpao));
        $id_rua = Input::get('id_rua', Input::old('id_rua', $id_rua));
        $id_predio = Input::get('id_predio', Input::old('id_predio', $id_predio));
        $id_andar = Input::get('id_andar', Input::old('id_andar', $id_andar));
        if ($id_galpao != '' && $id_rua != '' && $id_predio != '' && $id_andar != '') {
            $enderecos = \Endereco::where('id_galpao', '=', $id_galpao)
                    ->where('id_rua', '=', $id_rua)
                    ->where('id_predio', '=', $id_predio)
                    ->where('id_andar', '=', $id_andar)
                    ->orderBy('id_unidade')
                    ->orderBy('id_caixa')
                    ->get();
        } else {
            $enderecos = null;
        }
        return View::make('andar_conteudo')->with('enderecos', $enderecos)->with('id_galpao', $id_galpao)->with('id_rua', $id_rua)->with('id_predio', $id_predio)->with('id_andar', $id_andar);
    }

    public function postConteudoandar() {
        Input::flash();
        return $this->getConteudoandar();
    }

    public function getConteudo($id_galpao = "", $id_rua = "", $id_predio = "", $id_andar = "", $id_unidade = "") {
        $id_galpao = Input::get('id_galpao', Input::old('id_galpao', $id_galpao));
        $id_rua = Input::get('id_rua', Input::old('id_rua', $id_rua));
        $id_predio = Input::get('id_predio', Input::old('id_predio', $id_predio));
        $id_andar = Input::get('id_andar', Input::old('id_andar', $id_andar));
        $id_unidade = Input::get('id_unidade', Input::old('id_unidade', $id_unidade));
        if ($id_galpao != '' && $id_rua != '' && $id_predio != '' && $id_andar != '' && $id_unidade != '') {
            $enderecos = \Endereco::where('id_galpao', '=', $id_galpao)
                    ->where('id_rua', '=', $id_rua)
                    ->where('id_predio', '=', $id_predio)
                    ->where('id_andar', '=', $id_andar)
                    ->where('id_unidade', '=', $id_unidade)
                    ->orderBy('id_caixa')
                    ->get();
        } else {
            $enderecos = null;
        }
        return View::make('endereco_conteudo')->with('enderecos', $enderecos)->with('id_galpao', $id_galpao)->with('id_rua', $id_rua)->with('id_predio', $id_predio)->with('id_andar', $id_andar)->with('id_unidade', $id_unidade);
    }

    public function postConteudo() {
        Input::flash();
        return $this->getConteudo();
    }

    public function getCriarpredio() {
        return View::make('criar_predio')->with('tipodocumento', FormSelect(TipoDocumento::All(), 'descricao'));
    }

    public function postCriarpredio() {
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent ();
        $msg = '';
        if (Input::get('id_galpao') == '') {
            $msg .= '<br/>- o número do galpão é obrigatório';
        }
        if (Input::get('id_rua') == '') {
            $msg .= '<br/>- o número da rua é obrigatório';
        }
        if (Input::get('id_predio') == '') {
            $msg .= '<br/>- o número do prédio é obrigatório';
        }
        if (Input::get('andar_ini') == '') {
            $msg .= '<br/>- o andar inicial é obrigatório';
        }
        if (Input::get('andar_fim') == '') {
            $msg .= '<br/>- o andar final é obrigatório';
        }
        if (Input::get('qtd_unidades') == '') {
            $msg .= '<br/>- a quantidade de unidades é obrigatória';
        }
        if (Input::get('id_tipodocumento') == '') {
            $msg .= '<br/>- o tipo de documento é obrigatório';
        }
        if ($msg != '') {
            Input::flash();
        } else {
            try {
                \DB::beginTransaction();
                $rep->criarPredio(Input::get('id_tipodocumento'), Input::get('id_galpao'), Input::get('id_rua'), Input::get('id_predio'), Input::get('andar_ini'), Input::get('andar_fim'), Input::get('qtd_unidades'));
                \DB::commit();
                $qtd = \Endereco::where('id_tipodocumento', '=', Input::get('id_tipodocumento'))
                        ->where('id_galpao', '=', Input::get('id_galpao'))
                        ->where('id_rua', '=', Input::get('id_rua'))
                        ->where('id_predio', '=', Input::get('id_predio'))
                        ->where('id_andar', '>=', Input::get('andar_ini'))
                        ->where('id_andar', '<=', Input::get('andar_fim'))
                        ->count();
                Session::flash('success', 'Foram criados ' . $qtd . ' endereços com sucesso no prédio '.Input::get('id_predio'));
                Input::flashExcept('id_predio');
            } catch (Exception $exc) {
                \DB::rollBack();
                Input::flash();
                Session::flash('errors', $exc->getMessage());
            }
        }
        return View::make('criar_predio')->with('tipodocumento', FormSelect(TipoDocumento::All(), 'descricao'));
    }

    public function getCriarandar() {

    }

    public function postCriarandar() {

    }

}
