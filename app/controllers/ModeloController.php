<?php

class ModeloController extends BaseController {

    public function getIndex($pclasse) {
        $clientes = Cliente::all();

        $colunas = array(
            array('titulo' => 'Nome', "campo" => 'razaosocial')
            , array('titulo' => 'Fantasia', "campo" => 'nomefantasia')
        );

        return View::make('modelo_list')
                        ->with('nomeId', 'id_cliente')
                        ->with('classe', 'cliente')
                        ->with('titulo', 'Clientes')
                        ->with('colunas', $colunas)
                        ->with('instancias', $clientes)
        ;
    }

}
