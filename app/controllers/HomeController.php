<?php

class HomeController extends BaseController {
    /*
      |--------------------------------------------------------------------------
      | Default Home Controller
      |--------------------------------------------------------------------------
      |
      | You may wish to use controllers instead of, or in addition to, Closure
      | based routes. That's great! Here is an example controller method to
      | get you started. To route to this controller, just add the route:
      |
      |	Route::get('/', 'HomeController@showWelcome');
      |
     */

    public function getBemvindocliente() {
        return View::make('home_bemvindo_cliente');
    }

    public function getMsg($tipo){
        if($tipo == 'info'){
            Session::flash('msg', "Teste msg info!!");
            return Redirect::to('/home/bemvindocliente');
        }
        if($tipo == 'success'){
            Session::flash('success', "Teste msg success!!");
            return Redirect::to('/home/bemvindocliente');
        }
        if($tipo == 'errors'){
            Session::flash('errors', "Teste msg erro!!");
            return Redirect::to('/home/bemvindocliente');
        }
    }
    
    public function getBemvindo() {
        $os = Os::novas()
                ->Deativos()
                ->orderBy('id_os_chave', 'desc')
                ->paginate(10)
        ;
        return View::make('home_bemvindo')
                        ->with('os', $os)
        ;
    }

    public function getInfo() {
        return View::make('info');
    }

    public function getData() {
        return View::make('data')
                ->with('data', date('d/m/Y'))
                ->with('hora', date('H:i:s'));
    }

    public function getNovo(){
        return View::make('base');        
    }
    
    public function Teste() {
        return View::make('teste');
    }

}
