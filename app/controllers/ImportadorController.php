<?php

class ImportadorController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getImporte($pNomeTabela)
	{
		set_time_limit(0);
		$ret = '';
		switch($pNomeTabela){
			case "cliente":
				Cliente::Recarregue();
				break;
			
			case "nivel":
				Nivel::Recarregue();
				break;

			case "usuario":
				$this->ImporteTabela(new Usuario());
				break;
			case "cadastros":
				// $ret .= $this->ImporteTabela(new Nivel());// Carregado pelo seed
				$ret .= $this->ImporteTabela(new Cliente());
				$ret .= $this->ImporteTabela(new Usuario());
				// $ret .= $this->ImporteTabela(new UsuarioInterno(), false); 	// Carregado pelo seed
				$ret .= $this->ImporteTabela(new TipoDocumento());
				$ret .= $this->ImporteTabela(new TipoTelefone());
				$ret .= $this->ImporteTabela(new TipoEmail());
				$ret .= $this->ImporteTabela(new TipoProduto());
				$ret .= $this->ImporteTabela(new DepCliente());
				$ret .= $this->ImporteTabela(new SetCliente());
				$ret .= $this->ImporteTabela(new CentroDeCusto());
				$ret .= $this->ImporteTabela(new Solicitante());
				$ret .= $this->ImporteTabela(new Contato());
				$ret .= $this->ImporteTabela(new EmailCliente());
				$ret .= $this->ImporteTabela(new EmailContato());
				$ret .= $this->ImporteTabela(new EmailSolicitante());
				$ret .= $this->ImporteTabela(new TelefoneCliente());
				$ret .= $this->ImporteTabela(new TelefoneContato());
				$ret .= $this->ImporteTabela(new TelefoneSolicitante());
				break;
			case "endereco":
				$this->ImporteTabela(new Endereco());
				break;
			case "os":
				$ret .= $this->ImporteTabela(new Os());
				break;
			case "osdes":
				$ret .= $this->ImporteTabela(new OsDesmembrada());
				break;
			case "reserva":
				$ret .= $this->ImporteTabela(new Reserva());
				$ret .= $this->ImporteTabela(new ReservaDesmembrada());
				break;
			case "documento":
				$this->ImporteTabela(new Documento());
				break;
			case "expurgo":
				$ret .= $this->ImporteTabela(new Expurgo());
				break;			
			
			case "contatoteste":
				$ret .= $this->GerarSeedTabela(new Contato());
				break;			
			
			case "enderecoteste":
				$ret .= $this->GerarSeedTabela(new Endereco());
				break;			
				
		}
		
		return $ret;
	}

	protected function ImporteTabela(\Modelo $pInstancia, $ptruncate = true)
	{
		$ret = '';
		$modelo = get_class($pInstancia);
		$ret .= '<br/>Importando tabela '.$modelo.'...';
		$dbname = "../app/SIGDOC.fdb";
		$DB = new PDO("firebird:dbname=".$dbname, "SYSDBA", "masterkey");
		$res = $DB->query("select * from ".$pInstancia->get_nomeTabelaFirebird());
		if($ptruncate){
			 DB::statement('SET FOREIGN_KEY_CHECKS=0;');
			$modelo::truncate();
			 DB::statement('SET FOREIGN_KEY_CHECKS=1;');
		}
		$i = 0;
		DB::connection()->getPdo()->beginTransaction();
		while ($rs = $res->fetch(PDO::FETCH_ASSOC)){
			if($i == 1000){
				DB::connection()->getPdo()->commit();
				DB::connection()->getPdo()->beginTransaction();
				$i = 0;
			}
			$obj = new $modelo;
			$obj->CarregaDoRecordsetFirebird($rs);
			$obj->save();
			$i++;
		}	
		DB::connection()->getPdo()->commit();
		$ret .= '  ====>ok!';
		return $ret;
	}

	protected function GerarSeedTabela(\Modelo $pInstancia, $ptruncate = true)
	{

		$ret = '';
		$modelo = get_class($pInstancia);
		$ind = 0;
		
		$cab = '<?php
class '.$modelo.$ind.'TableSeeder extends Seeder {

    public function run()
    {';
		$rod = '	}
}';
		
		$arquivo = "../../".$modelo.$ind.'TableSeeder.php';
		$fp = fopen($arquivo, 'w');
		fwrite($fp, $cab);
		$i = 0;
		$ret .= '<br/>Importando tabela '.$modelo.'...';
		$dbname = "../app/SIGDOC.fdb";
		$DB = new PDO("firebird:dbname=".$dbname, "SYSDBA", "masterkey");
		$res = $DB->query("select * from ".$pInstancia->get_nomeTabelaFirebird());
		while ($rs = $res->fetch(PDO::FETCH_ASSOC)){
			$reg = "";
			$virgula = "";
			foreach($rs as $coluna => $valor){
				$reg .= $virgula.'"'.strtolower($coluna).'"';
				$reg .= '=>"'.utf8_encode($valor).'"';
				$virgula = ',';
			}		
			$reg = "\n\t\t".'$obj = array('.$reg.');';
			$reg .= "\n\t\t".'DB::table("'.$modelo.'")->insert($obj);';
			fwrite($fp, $reg);
			$i++;
			if($i >= 1000){
				fwrite($fp, $rod);
				fclose($fp);
				print '<br/>		$this->call("'.$modelo.$ind.'TableSeeder");';
				$ind++;
				$cab = 'class '.$modelo.$ind.'TableSeeder extends Seeder {

			public function run()
			{';
				$rod = '	}
		}';

				$arquivo = "../../".$modelo.$ind.'TableSeeder.php';
				$fp = fopen($arquivo, 'w');
				fwrite($fp, $cab);
				$i = 0;
			}
		}	
		fwrite($fp, $rod);
		fclose($fp);
		$ret .= '  ====>ok!';
		return $ret;
	}
	
}