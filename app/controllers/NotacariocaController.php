<?php
use Memodoc\Repositorios;

class NotacariocaController extends BaseController {

    public function getLotes(){

    }
    public function getRetorno() {
        $retornos = \NotaCariocaRetorno::orderBy('data_carga', 'desc')->get();

        return View::make('nota_carioca/notas_emitidas')
                        ->with('retornos', $retornos)
        ;
    }

    public function postNovoretorno(){
        set_time_limit(0);
        $rep = new Repositorios\RepositorioImportadorNotasEmitidas;

        $file = Input::file('arquivo');

        $retorno = new \NotaCariocaRetorno;
        $retorno->data_carga = Carbon::now('America/Sao_Paulo')->format('Y-m-d');
        $retorno->nome_arquivo = $file->getClientOriginalName();
        $retorno->observacao = Input::get('observacao');
        $retorno->save();

        $file->move($retorno->caminho(), $retorno->id_notacarioca_retorno);

        $rep->processa_arquivo($retorno->id_notacarioca_retorno);
        
        return Redirect::to('/notacarioca/retorno');
        
    }

    public function getProcessar($id_notacarioca_retorno){
        $rep = new Repositorios\RepositorioImportadorNotasEmitidas;
        $rep->processa_arquivo($id_notacarioca_retorno);
        Session::flash('success', 'Arquivo reprocessado com sucesso!!');
        return Redirect::to('/notacarioca/retorno');
    }
    

    public function getFaturar($id_periodo_fat) {
        // Obtem clientes faturáveis
        // Obtem clientes não faturáveis
        $perfat = \PeriodoFaturamento::find($id_periodo_fat);
        $faturaveis = $this->getClientesfaturaveis($id_periodo_fat);
        $naofaturaveis = $this->getClientesnaofaturaveis($id_periodo_fat);
        $dados = [
            "perfat" => $perfat
            ,"faturaveis" => $faturaveis
            ,"naofaturaveis" => $naofaturaveis
        ];
        // Gera view 
        return View::make('nota_carioca/selecionar', $dados);
    }


    public function getClientesfaturaveis($id_periodo_fat){
        $clientes = \Cliente::faturaveis()->orderBy('razaosocial')->get();
        return $clientes;        
    }

    public function getClientesnaofaturaveis($id_periodo_fat){
        $clientes = \Cliente::naofaturaveis()->orderBy('razaosocial')->get();
        return $clientes;        
    }

    public function postFaturar(){
        $id_periodo_fat = Input::get('id_periodo_fat');
        // 
        //
        //
    }

    

    public function postSuspendecliente($id_cliente){
        $cliente = \Cliente::find($id_cliente);
        $cliente->suspende_faturamento();
        $cliente->save();
        return json_encode(['retcode'=> 0, 'msg' => 'Cliente atualizado com sucesso']);
    }

    public function postHabilitacliente($id_cliente){
        $cliente = \Cliente::find($id_cliente);
        $cliente->habilita_faturamento();
        $cliente->save();
        return json_encode(['retcode'=> 0, 'msg' => 'Cliente atualizado com sucesso']);

    }
}