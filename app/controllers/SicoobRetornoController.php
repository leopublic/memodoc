<?php

use Carbon\Carbon;
use Memodoc\Repositorios;

class SicoobRetornoController extends BaseController {
    public function getIndex() {
        $retornos = \SicoobRetorno::orderBy('data_carga', 'desc')->get();

        return View::make('sicoob_retorno/index')
                        ->with('retornos', $retornos)
        ;
    }

    public function postIndex() {
        return Redirect::to('/sicoobretorno');
    }

    public function postNovaconsulta(){
        set_time_limit(0);
        $rep = new Repositorios\RepositorioSicoobRetorno;

        $file = Input::file('arquivo');

        $retorno = new \SicoobRetorno;
        $retorno->data_carga = Carbon::now('America/Sao_Paulo')->format('Y-m-d');
        $retorno->nome_arquivo = $file->getClientOriginalName();
        $retorno->observacao = Input::get('observacao');
        $retorno->save();

        $file->move($retorno->caminho(), $retorno->id_sicoob_retorno);

        $rep->processa_arquivo($retorno->id_sicoob_retorno);
        
        return Redirect::to('/sicoobretorno');
    }

    public function getDetalhe($id_sicoob_retorno){
        $retorno = \SicoobRetorno::find($id_sicoob_retorno);
        $regs = \RegistroSicoobRetorno::where('id_sicoob_retorno', '=', $id_sicoob_retorno)->orderBy('ordem', 'asc')->get();
        
        return View::make('sicoob_retorno/detalhe')
                    ->with('regs', $regs)
                    ->with('retorno', $retorno)
                    ;        
    }

    public function getProcessar($id_sicoob_retorno){
        $rep = new Repositorios\RepositorioSicoobRetorno;
        $rep->processa_arquivo($id_sicoob_retorno);
        Session::flash('success', 'Arquivo reprocessado com sucesso!!');
        return Redirect::to('/sicoobretorno');
    }

}