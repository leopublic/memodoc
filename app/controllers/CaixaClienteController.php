<?php


class CaixaClienteController extends BaseController {

    public function getSelecionar($id_caixapadrao = '') {
        return View::make('caixa_cliente_selecionar');
    }

    public function postSelecionar() {
        Input::flash();
        $errors = array();
        if (Input::get('id_caixapadrao') == '') {
            $errors[] = 'Informe o número de uma referência (caixa) para continuar';
            $errors[] = 'ssdsd sds ds ';
        } else {
            if (!is_numeric(Input::get('id_caixapadrao'))) {
                $errors[] = 'A referência deve ser um número válido';
            }
        }

        $caixa = Documento::where('id_cliente', '=', Auth::user()->id_cliente)
                ->where('id_caixapadrao', '=', Input::get('id_caixapadrao'))
                ->first()
        ;
        if (count($errors) > 0) {
            Session::flash('errors', $errors);
        } else {
            Session::forget('errors');
        }
        return Redirect::to('/publico/caixa/selecionar')
                        ->withInput();
    }

    public function getCliente() {
        $clientes = Cliente::all();
        return View::make('caixa_cliente')->with('clientes', $clientes);
    }

    public function getLocalizar($id) {

        $cliente = Cliente::findOrFail($id);
        $setores = $cliente->setor()->get();
        $departamentos = $cliente->departamento()->get();
        $centrocustos = $cliente->centrocusto()->get();

        return View::make('caixa_localizar')
                        ->with('cliente', $cliente)
                        ->with('setores', $this->formselect($setores))
                        ->with('centrocustos', $this->formselect($centrocustos))
                        ->with('departamentos', $this->formselect($departamentos))
                        ->with('model', new Documento);
    }

    function postLocalizar($id) {

        Input::flash();

        $titulo = Input::get('titulo');
        $conteudo = Input::get('conteudo');
        $id_departamento = Input::get('id_departamento');
        $id_centro = Input::get('id_centro');
        $id_setor = Input::get('id_setor');
        $id_caixa = Input::get('id_caixa');
        $reservado = Input::get('reservado');

        $model = Documento::where('id_cliente', '=', $id);

        if ($titulo <> '') {
            $model->where('titulo', 'LIKE', "%$titulo%");
        }
        if ($conteudo <> '') {
            $model->where('conteudo', 'LIKE', "%$conteudo%");
        }
        if ($id_setor <> '') {
            $model->where('id_setor', '=', $id_setor);
        }
        if ($id_centro <> '') {
            $model->where('id_centro', '=', $id_centro);
        }
        if ($id_departamento <> '') {
            $model->where('id_departamento', '=', $id_departamento);
        }
        if ($id_caixa <> '') {
            $model->where('id_caixa', '=', $id_caixa);
        }
        if ($reservado == '0') { // caixas em consulta
            $model->where('emcasa', '=', '0');
        }

        if ($reservado == '1') { // caixas reservadas
            $model->where('primeira_entrada', 'IS', DB::raw('null'));
        }


        $documentos = $model->get();

        /*
          $queries = DB::getQueryLog();
          $last_query = end($queries);

          echo '<pre>';
          print_r($last_query);
          echo '</pre>';
         */

        $cliente = Cliente::findOrFail($id);
        $setores = $cliente->setor()->get();
        $departamentos = $cliente->departamento()->get();
        $centrocustos = $cliente->centrocusto()->get();

        return View::make('caixa_localizar')
                        ->with('cliente', $cliente)
                        ->with('setores', $this->formselect($setores))
                        ->with('centrocustos', $this->formselect($centrocustos))
                        ->with('departamentos', $this->formselect($departamentos))
                        ->with('documentos', $documentos)
                        ->with('model', new Documento)


        ;
    }

    /**
     * Transform model in adapter Form::select
     * @link http://laravel.com/docs/html#drop-down-lists DropDown-lists
     * @param type $model
     * @param type $name
     * @return type array
     */
    function formselect($model, $name = 'nome') {
        $data = array();
        if ($model->count() > 0) {
            $data[''] = 'Selecione';
            foreach ($model as $fields) {
                foreach ($fields->getAttributes() as $fieldname => $field) {
                    if ($fieldname == $name) {
                        $data[$fields->getKey()] = $field;
                    }
                }
            }
        }
        return $data;
    }

    public function getEmconsulta() {
        $query = Documento::caixasEmConsulta()
                ->doCliente(Auth::user()->id_cliente)
                ->ordenacaoPadrao()
                ->distinct();
        $quantos = $query->count();
        $documentos = $query->paginate(20);
        return $this->getRelatorio('Referências em consulta', $documentos, $quantos);
    }

    public function getNaoinventariadas() {
        $query = Documento::caixasNaoinventariadasNaoEnviadas()
                ->doCliente(Auth::user()->id_cliente)
                ->ordenacaoPadrao()
                ->distinct();
        $quantos = $query->count();
        $documentos = $query->paginate(20);
        return $this->getRelatorio('Referências não enviadas para custódia e não inventariadas', $documentos, $quantos);
    }

    public function getInventariadas() {
        $query = Documento::caixasInventariadasNaoEnviadas()
                ->doCliente(Auth::user()->id_cliente)
                ->ordenacaoPadrao()
                ->distinct();
        $quantos = $query->count();
        $documentos = $query->paginate(20);
        return $this->getRelatorio('Referências não enviadas para custódia e inventariadas', $documentos, $quantos);
    }

    public function getEmcarencia() {
        $query = Documento::caixasEmCarencia()
                ->doCliente(Auth::user()->id_cliente)
                ->ordenacaoPadrao()
                ->distinct();
        $quantos = $query->count();
        $documentos = $query->paginate(20);
        return $this->getRelatorio('Referências não enviadas mas sendo faturadas (em carência)', $documentos, $quantos);
    }

    public function getExpurgadas($id_expurgo_pai = '') {
        // $sql = "select expurgo.id_expurgo_pai, date_format(expurgo_pai.data_solicitacao, '%d/%m/%Y') data_solicitacao, expurgo.id_caixapadrao 
        //         from expurgo_pai
        //          join expurgo on expurgo.id_expurgo_pai = expurgo_pai.id_expurgo_pai
        //          where id_expurgo_pai.id_cliente = ".Auth::user()->id_cliente."
        //          order by id_expurgo_pai desc, id_caixapadrao asc";

        //$expurgos = \DB::select(\DB::raw($sql));
        if($id_expurgo_pai == ''){
            $expurgos = \ExpurgoPai::where('id_cliente', '=', Auth::user()->id_cliente)->orderBy('id_expurgo_pai', 'desc')->paginate(20);
            $quantos = \ExpurgoPai::where('id_cliente', '=', Auth::user()->id_cliente)->count();

            $cliente = Auth::user()->cliente()->first();
            $titulo = "Referências expurgadas";
            return View::make('expurgo/cliente_index')
                            ->with('titulo', $titulo)
                            ->with('expurgos', $expurgos)
                            ->with('quantos', $quantos)
                            ->with('cliente', $cliente)
            ;
        } else {
            $expurgo_pai = \ExpurgoPai::where('id_expurgo_pai', '=', $id_expurgo_pai)->first();
            $expurgos = \Expurgo::where('id_expurgo_pai', '=', $id_expurgo_pai)
                        ->orderBy('id_caixapadrao')->get();
            $quantos = \Expurgo::where('id_expurgo_pai', '=', $id_expurgo_pai)->count();

            $cliente = Auth::user()->cliente()->first();
            $titulo = "Referências expurgadas - Expurgo ".substr('000000'.$id_expurgo_pai, -6)." em ".$expurgo_pai->data_solicitacao;
            return View::make('expurgo/cliente_caixas')
                            ->with('titulo', $titulo)
                            ->with('expurgos', $expurgos)
                            ->with('quantos', $quantos)
                            ->with('cliente', $cliente)
            ;
        }
    }

    public function getRelatorio($titulo, $documentos, $quantos) {
        $cliente = Auth::user()->cliente()->first();
        return View::make('caixa_cliente_localizar')
                        ->with('titulo', $titulo)
                        ->with('documentos', $documentos)
                        ->with('quantos', $quantos)
                        ->with('cliente', $cliente)
        ;
    }

    public function getHistorico($id_caixapadrao){
        $id_cliente = \Auth::user()->id_cliente;

        $caixa = \Documento::where('id_cliente', '=', $id_cliente)
        				->where('id_caixapadrao', '=', $id_caixapadrao)
        				->first();

        $rep = new \Memodoc\Repositorios\RepositorioCaixaEloquent;
        $oss = $rep->historico($caixa);

        return View::make('caixa_cliente_historico')
                        ->with('id_caixapadrao', $id_caixapadrao)
                        ->with('situacao', $caixa->situacao_galpao_cliente)
                        ->with('instancias', $oss)
                ;

    }

}
