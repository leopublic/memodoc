<?php

class OrdemServicoController extends BaseController {

    protected $repOs;

    public function __construct() {
        $this->repOs = new \Memodoc\Repositorios\RepositorioOsEloquent;
    }

    public function getCancelar($id_os_chave) {
        try {
            $os = Os::find($id_os_chave);
            if (is_a($os, 'Os')) {
                $this->repOs->canceleOperador($os);
                Session::flash('success', 'A ordem de serviço ' . substr('000000' . $os->id_os, -6) . ' foi cancelada com sucesso !');
                return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/listar/');
            } else {
                throw new Exception('Ordem de serviço não encontrada (id=' . $id_os_chave . ')');
            }
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
            return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/listar/');
        }
    }

    public function getCancelarreserva($id_os_chave, $id_reserva) {
        try {
            $rep = new \Memodoc\Repositorios\RepositorioReservaEloquent;
            $reserva = Reserva::findOrFail($id_reserva);
            $rep->cancelarReserva($reserva);
            Session::flash('success', 'Reserva cancelada com sucesso');
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
        }
        return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/visualizar/' . $id_os_chave);
    }

    public function getConcluida($id_os_chave) {
        try {
            $os = Os::findOrFail($id_os_chave);
            $this->repOs->concluir($os);
            Session::flash('success', 'A foi alterada para "atendida" com sucesso! ');
        } catch (Exception $exc) {
            Session::flash('errors', $exc->getTraceAsString());
        }
        return Redirect::to(URL::previous());
    }

    public function getVisualizarlimpo($id_os_chave) {
        return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/visualizar/' . $id_os_chave);
    }

    public function getVisualizar($id_os_chave, $acao = '') {
        $os = Os::findOrFail($id_os_chave);
        if ($os->id_status_os == StatusOs::stINCOMPLETA) {
            $os->solicitado_em = \Carbon::now('America/Sao_Paulo')->format('d/m/Y H:i:s');
            $os->turnoDefault();
        }
        $cliente = $os->cliente;
        $documentos = OsDesmembrada::daOs($os)->get();
        $reserva = $os->reserva;

        $qtd_itens_retirados = \OsItemRetirado::where('id_os_chave', '=', $id_os_chave)->count();

        $menus = array();

        $menu = new \Menu('Ações', 'aweso-gears');
        if ($os->id_status_os < 4 && $os->id_status_os > StatusOs::stINCOMPLETA) {
            $menu->adicionaLink(new Link('Indicar que foi atendida', \Memodoc\Servicos\Rotas::ordemservico().'/concluida/' . $id_os_chave, "_self"));
        }

        if ($os->id_status_os != StatusOs::stATENDIDA && $os->id_status_os > StatusOs::stINCOMPLETA) {
            $menu->adicionaLink(new Link('Cancelar OS', \Memodoc\Servicos\Rotas::ordemservico().'/cancelar/' . $id_os_chave));
        }
        if (count($menu->links()) > 0) {
            $menus[] = $menu;
        }
        if ($os->id_status_os > StatusOs::stINCOMPLETA) {
            $menu = new Menu('Imprimir', 'aweso-print');
            $menu->adicionaLink(new Link('Tudo', \Memodoc\Servicos\Rotas::ordemservico().'/relatoriocompleto/' . $id_os_chave));
            $menu->adicionaLink(new Link('Capa da OS', \Memodoc\Servicos\Rotas::ordemservico().'/relatorio/' . $id_os_chave));
            $menu->adicionaLink(new Link('Relação de caixas (oper.)', \Memodoc\Servicos\Rotas::ordemservico().'/relacaodecaixasoperacional/' . $id_os_chave));
            $menu->adicionaLink(new Link('Relação de caixas (cliente)', \Memodoc\Servicos\Rotas::ordemservico().'/relacaodecaixas/' . $id_os_chave));
            $menu->adicionaLink(new Link('Protocolo de recebimento (cliente)', \Memodoc\Servicos\Rotas::ordemservico().'/protocoloretirada/' . $id_os_chave));
            $menu->adicionaLink(new Link('Protocolo de itens conferidos', \Memodoc\Servicos\Rotas::ordemservico().'/protocoloitens/' . $id_os_chave));
            if (isset($reserva)) {
                $menu->adicionaLink(new Link('Etiquetas da reserva', '/etiqueta/gere/' . $reserva->id_reserva));
            }
        }
        $links = $menu->links();
        if (count($links) > 0) {
            $menus[] = $menu;
        }

        $endeentrega = Endeentrega::comboCompletoDoCliente($os->id_cliente);

        $status_os = StatusOs::disponiveis()->lists('descricao', 'id_status_os');
        //Checkin::where('id_cliente','='$id)

        $readonly = null;
        if (count($os->faturamento) > 0){
            $perfat = $os->faturamento->periodofaturamento;
            if ($perfat->status == 2){
                $readonly = true;
            }
        }

        $expurgo = \ExpurgoPai::where('id_os_chave', '=', $os->id_os_chave)->first();
        if (count($expurgo) > 0){
            $caixas_expurgo = \Expurgo::where('id_expurgo_pai', '=', $expurgo->id_expurgo_pai)
                            ->with('endereco')
                            ->get();
        } else {
            $caixas_expurgo = array();
        }

        $transbordo = \Transbordo::where('id_os_chave', '=', $os->id_os_chave)->first();
        if (count($transbordo) > 0){
            $caixas_transbordo = \TransbordoCaixa::where('id_transbordo', '=', $transbordo->id_transbordo)
                            ->with('enderecoantes', 'enderecodepois')
                            ->get();

        } else {
            $caixas_transbordo = array();
        }


        return View::make('ordemservico_visualizar')
                        ->with('model', $os)
                        ->with('enderecos', $endeentrega)
                        ->with('documentos', $documentos)
                        ->with('cliente', $cliente)
                        ->with('menus', $menus)
                        ->with('reserva', $reserva)
                        ->with('endeentrega', $endeentrega)
                        ->with('status_os', $status_os)
                        ->with('acao', $acao)
                        ->with('readonly', $readonly)
                        ->with('expurgo', $expurgo)
                        ->with('caixas_expurgo', $caixas_expurgo)
                        ->with('transbordo', $transbordo)
                        ->with('caixas_transbordo', $caixas_transbordo)
                        ->with('qtd_itens_retirados', $qtd_itens_retirados)
        ;
    }

    public function getLocalizarpornumero() {
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');

        return View::make('ordemservico_pornumero')
                        ->with('clientes', $clientes)
        ;
    }

    public function postLocalizarpornumero() {
        Input::flash();
        $id_cliente = Input::get('id_cliente');
        $id_os = Input::get('id_os');
        $errors = array();
        if ($id_cliente == '') {
            $errors[] = 'Informe o cliente';
        }
        if ($id_os == '') {
            $errors[] = 'Informe o número da ordem de serviço';
        }
        if (count($errors) == 0) {
            $os = Os::where('id_cliente', '=', $id_cliente)->where('id_os', '=', $id_os)->first();
            if (!is_object($os)) {
                $errors[] = "Não foi encontrada uma ordem de serviço com esse número nesse cliente";
                return Redirect::back()->withErrors($errors);
            } else {
                return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/visualizar/' . $os->id_os_chave);
            }
        } else {
            return Redirect::back()->withErrors($errors);
        }
    }

    public function postVisualizar() {
        $rep = new \Memodoc\Repositorios\RepositorioOsEloquent;
        try {
            $post = Input::get();
            $os = $rep->salvaDoOperacional($post);
            Session::flash('success', 'Ordem de serviço atualizada com sucesso.');
        } catch (Exception $e) {
            Input::flash();
            Session::flash('errors', 'Não foi possível salvar a OS:' . $e->getMessage());
        }
        Return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/visualizar/' . Input::get('id_os_chave'));
    }

    /**
     * Remove os Itens listados no Pré-checkin
     * @param type $id_caixa_padrao
     * @return type View
     */
    public function getRemoveprecheckin($id_caixa_padrao) {
        $id_caixas = explode(",", Session::get('id_caixas_precheckin'));
        $novas_caixas = "";
        $virg = "";
        foreach ($id_caixas as $id_caixa_atual) {
            if ($id_caixa_atual != $id_caixa_padrao) {
                $novas_caixas .= $virg . $id_caixa_atual;
                $virg = ",";
            }
        }
        Session::flash("msg", "Caixa retirada com sucesso.");
        Session::put('id_caixas_precheckin', $novas_caixas);
        return Redirect::back();
    }

    /**
     * Submit do formulário Pré-checkin
     * @param type $id_os_chave
     * @return type View
     */
    public function postPrecheckin($id_os_chave) {

        $os = Os::find($id_os_chave);
        //pr(Input::get());
        $erro = array();
        if (Input::has('Limpar')) {
            Session::forget('id_caixas_precheckin');
            Session::flash('msg', 'Checkin cancelado');
            $id_caixas = "";
        } elseif (Input::has('Checkin')) {

            $sessionCaixas = Session::get('id_caixas_precheckin');
            if ($sessionCaixas != '' and strpos($sessionCaixas, ',')) {
                // varias caixas
                $id_caixas = explode(",", Input::get('id_caixas'));
            } else {
                // se tiver somente 1 caixa informada
                $id_caixas = array(Input::get('id_caixas'));
            }

            foreach ($id_caixas as $id_caixa_padrao) {
                $success = Galpao::PreCheckin($id_os_chave, $id_caixa_padrao);
                if (!$success) {
                    $erro[] = ' ' . $id_caixa_padrao . ' não foi possível realizar o pré-checkin';
                }
            }
            $id_caixas = "";
            Session::forget('id_caixas_precheckin');
            if (empty($erro)) {
                Session::flash('msg', 'Pré-Checkin realizado com sucesso!');
            }
        } else {
            $id_caixas = Input::get('id_caixas');
            $id_caixa_nova = Input::get('id_caixa_nova');
            $caixa = Documento::where('id_caixapadrao', '=', $id_caixa_nova)
                    ->where('id_cliente', '=', $os->id_cliente)
                    ->first();

            if (isset($caixa)) {
                if ($caixa->emcasa == '1') {
                    $erro[] = 'O id informado já está na MEMODOC';
                } else {
                    $id_caixas .= "," . $id_caixa_nova;
                }
            } else {
                $erro[] = 'O id informado não existe';
            }
        }


        if (substr($id_caixas, 0, 1) == ',') {
            $id_caixas = substr($id_caixas, 1);
        }

        if (count($erro) > 0) {
            Session::flash('error', $erro);
        }

        Session::put('id_caixas_precheckin', $id_caixas);
        return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/visualizar/' . $id_os_chave);
    }

    public function getListar($id_status = '') {
        return $this->listar($id_status, 0);
    }

    public function getListarteste() {
        return 'OK';
    }

    public function getListarinativos($id_status = '') {
        return $this->listar($id_status, 1);
    }

    public function listar($id_status = '', $inativos = 0) {
        $responsavel = Input::get('responsavel');
        $solicitado_em_ini = Input::get('solicitado_em_ini');
        $solicitado_em_fim = Input::get('solicitado_em_fim');
        $cancelado = Input::get('cancelado');
        $atendido = Input::get('atendido');
        $id_status_os = Input::get('id_status_os', $id_status);
        if ($id_status == '') {
            $id_cliente = Input::get('id_cliente', Session::get('id_cliente'));
        } else {
            $id_cliente = '';
        }

        if ($inativos) {
            $titulo = "Ordens de serviço - INATIVOS";
            $clientes = FormSelect(Cliente::getInativos(), 'razaosocial');
            $model = Os::Deinativos()->with(array('osdesmembradascomcheckout', 'checkin', 'cliente', 'reserva', 'statusos', 'usuariocadastro'));
        } else {
            $titulo = "Ordens de serviço";
            $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
            $model = Os::Deativos()->with(array('osdesmembradascomcheckout', 'checkin', 'cliente', 'reserva', 'statusos', 'usuariocadastro'));
        }


        if ($id_status_os != '') {
            $model->where('id_status_os', '=', $id_status_os);
        } else {
            $model->where('id_status_os', '>', 1);
        }

        if ($responsavel != '') {
            $model->where('responsavel', 'LIKE', '%' . $responsavel . '%');
        }

        if ($id_cliente != '') {
            $model->where('id_cliente', '=', $id_cliente);
        }

        if ($solicitado_em_ini != '') {
            $solicitado_em = DateTime::createFromFormat('d/m/Y', $solicitado_em_ini)->format('Y-m-d');
            $model->where(DB::raw('DATE(solicitado_em)'), '>=', $solicitado_em . ' 00:00:00');
        }

        if ($solicitado_em_fim != '') {
            $solicitado_em = DateTime::createFromFormat('d/m/Y', $solicitado_em_fim)->format('Y-m-d');
            $model->where(DB::raw('DATE(solicitado_em)'), '<=', $solicitado_em . ' 23:59:59');
        }
        $oss = $model->orderby('id_os', 'desc')->paginate(20);

        $status = FormSelect(StatusOs::where('id_status_os', '>', 1)->get(), 'descricao');

        $model->id_status_os = Input::get('id_status_os', $id_status);
        if ($id_status == '') {
            $model->id_cliente = Input::get('id_cliente', Session::get('id_cliente'));
        }
        Input::flash();
        return View::make('ordemservico_listar', compact('oss', 'clientes', 'model', 'status', 'titulo'));
    }

    public function getPendentes() {
        $os = Os::novas()
                ->orderBy('solicitado_em')
                ->get()
        ;

        return View::make('ordemservico_pendentes')
                        ->with('os', $os)
        ;
    }

    public function postListar() {
        return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/listar')->withInput();
    }

    public function postListarinativos() {
        return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/listarinativos')->withInput();
    }

    public function postListarxx() {
        Input::flash();

        $id_cliente = Input::get('id_cliente');
        $responsavel = Input::get('responsavel');
        $solicitado_em = Input::get('solicitado_em');
        $cancelado = Input::get('cancelado');
        $atendido = Input::get('atendido');
        $id_status_os = Input::get('id_status_os');
        $status = FormSelect(StatusOs::all(), 'descricao');

//        $model = Os::where('cancelado','<>','1');


        if ($id_status_os <> '') {
            $model->where('id_status_os', '=', $id_status_os);
        }

        if ($responsavel <> '') {
            $model->where('responsavel', 'LIKE', '%' . $responsavel . '%');
        }

        if ($id_cliente <> '') {
            $model->where('id_cliente', '=', $id_cliente);
        }

        if ($solicitado_em <> '') {
            $solicitado_em = DateTime::createFromFormat('d/m/Y', $solicitado_em)->format('Y-m-d');
            $model->where(DB::raw('DATE(solicitado_em)'), '=', $solicitado_em);
        }

        $oss = $model->with(array('cliente', 'statusos'))
                        ->orderby('id_os_chave', 'desc')->paginate(20);
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');

        return View::make('ordemservico_listar')
                        ->with('oss', $oss)
                        ->with('model', new Os())
                        ->with('status', $status)
                        ->with('clientes', $clientes);
    }

    public function getRealizar($id_cliente) {
        $id_os = Os::getProximoNumero($id_cliente);
        return View::make('ordemservico_realizar')
                        ->with('model', new Os)
                        ->with('id_os', $id_os)
                        ->with('cliente', Cliente::findOrFail($id_cliente));
    }

    /**
     * Permite selecionar o cliente da OS que será criada
     */
    function getCliente() {
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        return View::make('/ordemservico_sel_cliente')
                        ->with('clientes', $clientes)
        ;
    }

    /**
     * Cria a OS e redireciona para a edicao
     */
    function postCliente() {
        $post = Input::all();
        if (isset($post['id_cliente']) && $post['id_cliente'] > 0) {
            // Cria a OS nova para o cliente selecionado
            $cliente = Cliente::find($post['id_cliente']);
            if ($cliente->nivel_inadimplencia == 2) {
                return Redirect::back()->with('errors', 'Não e possível abrir OS para esse cliente pois ele foi bloqueado pela gerência por inadimplência.');
            } elseif ($cliente->fl_os_bloqueada == 1 && !Auth::user()->nivel->minimoAdministrador()) {
                return Redirect::back()->with('errors', 'Somente o administrador está autorizado a abrir novas OS para esse cliente. Consulte a gerência');
            } else {

                $rep = new \Memodoc\Repositorios\RepositorioOsEloquent;
                $os = $rep->novaAtendimento($post['id_cliente'], Auth::user()->id_usuario);
                // Direciona para edicao
                return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/visualizar/' . $os->id_os_chave);
            }
        } else {
            return Redirect::back()->with('errors', 'Por favor, selecione o cliente para criar a Ordem de Serviço');
        }
    }

    function getEditar() {
        $rep = new \Memodoc\Repositorios\RepositorioOsEloquent;
        $os = $rep->osIncompleta(Auth::user()->id_usuario);
        $endeentrega = Endeentrega::where('id_cliente', '=', $os->id_cliente)
                ->select(array(DB::raw("concat(coalesce(concat(logradouro, ' '), ''), coalesce(concat(endereco, ' '), ''), coalesce(concat(numero, ' '), ''), coalesce(concat(complemento, ' '), ''), coalesce(concat(bairro, ' '), '') ) endereco"), 'id_endeentrega'))
                ->get()
                ->lists('endereco', 'id_endeentrega');

        return View::make('ordemservico_editar')
                        ->with('model', $os)
                        ->with('endeentrega', $endeentrega)
        ;
    }

    public function postEditar() {
        Input::flash();
        $post = Input::all();
        $rep = new \Memodoc\Repositorios\RepositorioOsEloquent;
        $os = Os::find($post['id_os_chave']);
        if (Input::has('Cancelar')) {
            $rep->excluirOs($os);
            return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/cliente')->with('success', 'Ordem de serviço cancelada com sucesso');
        } else {
            $val = new \Memodoc\Validadores\OsAtendimento(Input::all());
            if ($val->passes()) {
                // Salva a OS
                $os = $rep->atribuiDoInput($os, $post);
                $os->id_usuario_alteracao = \Auth::user()->id_usuario;
                $os->evento = 'Alterada pelo operacional';
                $os->save();
                if (Input::has('Concluir')) {
                    // Conclui a OS
                    $rep->concluir($os);
                    // Redireciona para visualizar imprimindo relatórios
                    return Redirect::to(\Memodoc\Servicos\Rotas::ordemservico().'/visualizar/' . $os->id_os_chave . '/print')->with('success', 'Ordem de serviço liberada com sucesso');
                } else {
                    return Redirect::back()->with('success', 'Ordem de serviço atualizada com sucesso.');
                }
            } else {
                return Redirect::back()->withErrors($val->getErrors());
            }
        }
    }

    public function postAddcaixa() {
        $os = Os::find(Input::get('id_os_chave'));
        $id_caixapadrao = Input::get('id_caixapadrao');
        $this->repOs->adicionaCaixa($os, $id_caixapadrao);

        $osdesmembradas = OsDesmembrada::daOs($os)->get();

        return View::make('ordemservico_caixasenviadas')->with('osdesmembradas', $osdesmembradas);
    }

    function getAjaxdepartamento($id_cliente) {
        $departamentos = DepCliente::where('id_cliente', '=', $id_cliente)->get();
        $select = FormSelect($departamentos, 'nome');
        echo View::make('padrao.select_option', compact('select'));
    }

    public function getAjaxendereco($id_cliente) {
        $enderecos = Endeentrega::where('id_cliente', '=', $id_cliente)->select(array(DB::raw("concat(coalesce(concat(logradouro, ' '), ''), coalesce(concat(endereco, ' '), ''), coalesce(concat(numero, ' '), ''), coalesce(concat(complemento, ' '), ''), coalesce(concat(bairro, ' '), '') ) endereco"), 'id_endeentrega'))->get()->lists('endereco', 'id_endeentrega');
//           $select = FormSelect($enderecos, 'logradouro');
        echo View::make('padrao.select_option')->with('select', $enderecos);
    }

    public function getEmailabertura($id_os_chave) {
        $os = Os::findOrFail($id_os_chave);
        $html = View::make('emails.nova_os', compact('os'));
//        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);

        $this->mpdf = new mPDF('utf-8', 'A4', '', '', '2', '2', '2', '2', '', '');  // largura x altura
        $this->mpdf->allow_charset_conversion = false;

//        $this->mpdf->WriteHTML($html);
//        $this->mpdf->debug = true;
//        $this->mpdf->Output();
        return $html;
    }

    public function getRelatorio($id_os_chave) {
        $os = Os::findOrFail($id_os_chave);
        $printer = new \Memodoc\Servicos\ImpressoraOs;

        $mpdf = $printer->novoPdf();
        $mpdf = $printer->capa($mpdf, $os);
        $mpdf = $printer->capa($mpdf, $os);
        $mpdf->Output();
    }

    public function getRelatoriohtml($id_os_chave){
        $os = Os::findOrFail($id_os_chave);
        $html = \View::make('ordemservico_relatorio')->with('os', $os);
        return $html;
    }

    public function getRelacaodecaixas($id_os_chave) {
        set_time_limit(0);
        $os = Os::findOrFail($id_os_chave);
        $printer = new \Memodoc\Servicos\ImpressoraOs;

        $mpdf = $printer->novoPdf();
        $mpdf = $printer->relacaoCaixas($mpdf, $os);
        $mpdf = $printer->relacaoCaixas($mpdf, $os);
        $mpdf->Output();
    }

    public function getRelacaodecaixasoperacional($id_os_chave) {
        set_time_limit(0);
        $os = Os::findOrFail($id_os_chave);
        $printer = new \Memodoc\Servicos\ImpressoraOs;

        $mpdf = $printer->novoPdf();
        $mpdf = $printer->relacaoCaixasOperacional($mpdf, $os);
        $mpdf->Output();
    }

    public function getProtocoloretirada($id_os_chave) {
        $os = Os::findOrFail($id_os_chave);
        $printer = new \Memodoc\Servicos\ImpressoraOs;

        $mpdf = $printer->novoPdf();
        $mpdf = $printer->protocoloRetirada($mpdf, $os);
        $mpdf = $printer->protocoloRetirada($mpdf, $os);
        $mpdf->Output();
    }

    public function getProtocoloitens($id_os_chave) {
        $os = Os::findOrFail($id_os_chave);
        $sql = "select documento.id_caixapadrao, documento.id_item, codigo, campo1, campo2 
                from os_item_retirado, documento, item_remessa
                where documento.id_documento = os_item_retirado.id_documento
                  and item_remessa.id_item_remessa = os_item_retirado.id_item_remessa
                  and os_item_retirado.id_os_chave = ".$id_os_chave."
                order by documento.id_caixapadrao, documento.id_item";

        $itens = \DB::select(\DB::raw($sql));
        // $printer = new \Memodoc\Servicos\ImpressoraOs;

        // $mpdf = new \mPDF('utf-8', 'P', '', '', '10', '10', '65', '10', '10', '');  // largura x altura
        // $mpdf->allow_charset_conversion = false;
        // $mpdf = $printer->protocoloItens($mpdf, $os, $itens);
        // $mpdf->Output();


        $brtitulo = '<br/>RELAÇÃO DE ITENS RETIRADOS DO CLIENTE';
        $header = \View::make('ordemservico_cabecalho_rel_novo', compact('os', 'brtitulo'));

        $html = \View::make('ordemservico_protocolo_itens', compact('os', 'itens'));


        $pdf = PDF::loadHTML($html);
        $pdf->setOption('zoom', '1.25');
        $pdf->setOption('header-html', $header);
        return $pdf->download('protocolo_itens.pdf');
    }

    public function getRelatoriocompleto($id_os_chave) {
        $os = Os::findOrFail($id_os_chave);
        $printer = new \Memodoc\Servicos\ImpressoraOs;

        $mpdf = $printer->novoPdf();
        if ($os->tipodeemprestimo == 0) {
            $mpdf = $printer->capa($mpdf, $os);
            $mpdf = $printer->capa($mpdf, $os);
        }
        if ($os->qtdcaixasconsulta() > 0) {
            $mpdf = $printer->relacaoCaixas($mpdf, $os);
            $mpdf = $printer->relacaoCaixas($mpdf, $os);
            $mpdf = $printer->relacaoCaixasOperacional($mpdf, $os);
        }
        if ($os->totalCheckin() > 0 || $os->qtd_caixas_retiradas_cliente > 0 || $os->qtd_caixas_novas_cliente > 0) {
            $mpdf = $printer->protocoloRetirada($mpdf, $os);
            $mpdf = $printer->protocoloRetirada($mpdf, $os);
        }
//        $mpdf->Output('', 'D');
        $saida = $mpdf->Output('', 'S');
        $response = Response::make($saida);
        $response->header('Content-Type', 'application/pdf');
        return $response;
    }

    public function getAddcheckin($id_os_chave, $id_caixapadrao, $id_caixapadrao_fim = '') {
        $rep = new \Memodoc\Repositorios\RepositorioCheckinEloquent;
        $ret = $rep->adicionaCaixa($id_os_chave, $id_caixapadrao, $id_caixapadrao_fim);
        return json_encode($ret, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP );
    }

    public function getAddcheckinconfirmado($id_os_chave, $id_caixapadrao) {
        $rep = new \Memodoc\Repositorios\RepositorioCheckinEloquent;
        $ret = $rep->adicionaCaixaConfirmada($id_os_chave, $id_caixapadrao);
        $saida = json_encode($ret, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP );
        return $saida;
    }

    public function getAddemprestimo($id_os_chave, $id_caixapadrao, $id_caixapadrao_fim = '') {
        set_time_limit(0);
        $os = Os::find($id_os_chave);
        if (is_object($os)) {

            if ($id_caixapadrao_fim == '' & strpos($id_caixapadrao, ",") > 0){
                $ids = explode(",", $id_caixapadrao);
                $docs = Documento::where('id_cliente', '=', $os->id_cliente)
                ->caixapadraolista($ids)
                ->orderBy('id_caixapadrao')
                ->get();
            } else {
                $docs = Documento::where('id_cliente', '=', $os->id_cliente)
                ->caixapadraoentre($id_caixapadrao, $id_caixapadrao_fim)
                ->orderBy('id_caixapadrao')
                ->get();
            }

            if (count($docs) > 0) {
                $ret = $this->adicionaCaixas($os, $docs);
            } else {
                $ret = [];
                $xx = '';
                $ret['status'] = 'error';
                $icone = '<i class="aweso-exclamation"></i>';
                $msg = 'Caixas não encontrada!!! Verifique!';
                $xx .= '<tr id="emprestimo' . $id_caixapadrao . '" class="error">';
                $xx .= '<td style="text-align:center">-</td>';
                $xx .= '<td style="text-align:center">' . $icone . '</td>';
                $xx .= '<td style="text-align:center;">' . $id_caixapadrao . '</a></td>';
                $xx .= '<td style="text-align:center;">--</td>';
                $xx .= '<td style="text-align:center;">--</td>';
                $xx .= '<td>' . $msg . '</td>';
                $xx .= '<td style="text-align:center;">--</td>';
                $xx .= '<td style="text-align:center;">--</td>';
                $xx .= '</tr>';
                $ret['conteudo'] = $xx;
            }
        }

        return json_encode($ret);
    }

    public function adicionaCaixas($os, $docs){
        $ret = array();
        $xx = '';
        $docant = new Documento;
        foreach ($docs as $doc) {
            if ($doc->id_caixapadrao != $docant->id_caixapadrao) {
                try {
                    $osdesmembrada = $this->repOs->adicionaCaixa($os, $doc);
                    $id_caixa = substr('000000' . $doc->id_caixa, -6);
                    $icone = '<button title="Retirar essa caixa do empréstimo" onclick="removeEmprestimo(\'' . $osdesmembrada->id_osdesmembrada . '\'); return false;" class="btn btn-mini btn-danger"><i class="aweso-trash"></i></button>';
                    if ($doc->emcasa == 0) {
                        $msg = "caixa incluída com sucesso (ATENÇÃO: CAIXA CONSTA COMO EMPRESTADA NO SISTEMA!! VERIFIQUE)";
                        $msg .= "<br/>Último checkout: " . $doc->ultimoCheckout();
                        $ret['status'] = 'info';
                    } else {
                        $msg = 'caixa incluída com sucesso';
                        $ret['status'] = '';
                    }
                } catch (Exception $ex) {
                    $ret['status'] = 'error';
                    $icone = '<i class="aweso-exclamation"></i>';
                    $msg = $ex->getMessage();
                    if (is_object($doc)) {
                        $id_caixa = substr('000000' . $doc->id_caixa, -6);
                    } else {
                        $id_caixa = '<i class="aweso-exclamation"></i>';
                    }
                }
                $xx .= '<tr id="emprestimo' . $doc->id_caixapadrao . '" class="' . $ret['status'] . '">';
                $xx .= '<td style="text-align:center">-</td>';
                $xx .= '<td style="text-align:center">' . $icone . '</td>';
                $xx .= '<td style="text-align:center;"><a href="' . \URL::to('/caixa/historico/' . $doc->id_cliente . '/' . $doc->id_caixapadrao) . '" class="btn btn-small" target="_blank">' . substr('000000' . $doc->id_caixapadrao, -6) . '</a></td>';
                $xx .= '<td style="text-align:center;">' . $doc->id_caixa . '</td>';
                $xx .= '<td style="text-align:center;">' . $doc->endereco->enderecoCompleto() . '</td>';
                $xx .= '<td>' . $msg . '</td>';
                $xx .= '<td style="text-align:center;">--</td>';
                $xx .= '<td style="text-align:center;">--</td>';
                $xx .= '</tr>';
            }
            $docant = $doc;
        }
        $ret['conteudo'] = $xx;
        return $ret;
    }

    public function getHistorico($id_os_chave) {
        $os = Os::find($id_os_chave);
        $historico = LogOs::historico($id_os_chave);
        return View::make('ordemservico_historico')
                        ->with('os', $os)
                        ->with('historico', $historico)
        ;
    }

    public function getDelcheckin($id_os_chave, $id_caixa) {
        $ret = array();
        $os = Os::find($id_os_chave);
        try {
            $this->repOs->retiraCheckin($os, $id_caixa);
            $ret['msg'] = 'Referência removida com sucesso';
            $xx = View::make('os_checkins')
                            ->with('checkins', $os->checkin)->render();
            $ret['conteudo'] = $xx;
            $ret['status'] = 'success';
            $ret['titulo'] = 'Ok';
        } catch (Exception $ex) {
            $ret['titulo'] = 'Erro:';
            $ret['msg'] = $ex->getMessage();
            $ret['status'] = 'warning';
        }
        return json_encode($ret);
    }

    public function getDelemprestimo($id_osdesmembrada) {
        $ret = array();
        $osd = \OsDesmembrada::find($id_osdesmembrada);
        $os = Os::find($osd->id_os_chave);
        try {
            $this->repOs->retiraCaixaEmprestimo($id_osdesmembrada, \Auth::user()->id_usuario, 'Exclusão de empréstimo pelo operacional');
            $ret['msg'] = 'Referência removida com sucesso';
            $xx = View::make('os_emprestimos')
                            ->with('emprestimos', $os->referenciasConsulta())->render();
            $ret['conteudo'] = $xx;
            $ret['status'] = 'success';
            $ret['titulo'] = 'Ok';
        } catch (Exception $ex) {
            $ret['titulo'] = 'Erro:';
            $ret['msg'] = $ex->getMessage();
            $ret['status'] = 'warning';
        }
        return json_encode($ret);
    }

    public function getImportadatacheckout() {
        $sql = "select od.id_osdesmembrada, ck.data_checkout "
                . " from osdesmembrada od , checkout ck"
                . " where ck.id_cliente = od.id_cliente"
                . " and ck.id_os = od.id_os"
                . " and ck.id_caixapadrao = od.id_caixapadrao"
                . " and od.data_checkout is null"
                . " and ck.data_checkout is not null";
        $conn = \DB::getPdo();
        $res = $conn->query($sql);
        $i = 0;
        while ($rs = $res->fetch(PDO::FETCH_BOTH)) {
            $sql = "update osdesmembrada set data_checkout = '" . $rs['data_checkout'] . "' where id_osdesmembrada =" . $rs['id_osdesmembrada'];
            $conn->exec($sql);
            $i++;
            print "<br/>" . $i . " registro processado. id=" . $rs['id_osdesmembrada'];
        }
    }

    public function getLecaixas($id_os_chave){
        $os = \Os::find($id_os_chave);
        return View::make('ordemservico_lecaixas')
                        ->with('os', $os)
        ;

    }

    public function getEnviarprotocolo($id_os_chave, $email){
        $os = Os::findOrFail($id_os_chave);
        $sql = "select documento.id_caixapadrao, documento.id_item, codigo, campo1, campo2 
                from os_item_retirado, documento, item_remessa
                where documento.id_documento = os_item_retirado.id_documento
                  and item_remessa.id_item_remessa = os_item_retirado.id_item_remessa
                  and os_item_retirado.id_os_chave = ".$id_os_chave."
                order by documento.id_caixapadrao, documento.id_item";

        $itens = \DB::select(\DB::raw($sql));
        $printer = new \Memodoc\Servicos\ImpressoraOs;

        $mpdf = new \mPDF('utf-8', 'P', '', '', '10', '10', '65', '10', '10', '');  // largura x altura
        $mpdf->allow_charset_conversion = false;
        $mpdf = $printer->protocoloItens($mpdf, $os, $itens);

        $caminho = storage_path().'temp';
        if(!is_dir($caminho)){
            mkdir($caminho, 0775, true);
        }
        $nome_arquivo = $caminho.'/'.\Auth::user()->id_usuario.'_relatorio_digitacao.pdf';
        if(file_exists($nome_arquivo)){
            unlink($nome_arquivo);
        }
        $mpdf->Output($nome_arquivo);

        $data = array('os' => $os);

        \Mail::send('emails.digitacao', $data, function($message) use($email, $data, $nome_arquivo) {
            $message->to($emails)->subject('MEMODOC - Relatório de itens retirados '.$data);
            $message->attach($nome_arquivo);
          });

    }

    public function getTestenotificaabertura($id_os_chave){
        $rep = new Memodoc\Repositorios\RepositorioOsEloquent;
        $os = Os::find($id_os_chave);
        $rep->notificar_abertura($os);
        print "Notificação enviada com sucesso";
    }
}
