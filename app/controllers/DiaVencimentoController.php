<?php

use Memodoc\Repositorios;

class DiaVencimentoController extends BaseController {
    public $repDias;
    public function __construct (){
        $this->repDias = new Repositorios\RepositorioDiaVencimento;
    }
    public function getIndex() {
        return $this->getList();
    }

    public function postIndex(){

    }

    public function postNovo(){
        $dia = Input::get('dia');
        $tem = \DiaVencimento::where('dia', '=', $dia)->first();
        $ret = [];
        if(count($tem) > 0){
            $retcode = -1;
            $msg = 'Dia já cadastrado';
        } else {
            $dia_def = \DiaVencimento::where('fl_default', '=', 1)->first();
            $diavenc = new \DiaVencimento;
            $diavenc->dia = $dia;
            if(count($dia_def) ==0){
                $diavenc->fl_default = 1;
            }
            $diavenc->save();
            $retcode = 0;
            $msg = 'Dia cadastrado com sucesso';            
        }
        return json_encode(['retcode' => $retcode, 'msg' => $msg]);
    }
    
    public function postExclui(){
        $dia = Input::get('dia');
        \Cliente::where('dia_vencimento', '=', $dia)->update(['dia_vencimento' => null]);
        \DiaVencimento::where('dia', '=', $dia)->delete();
        $retcode = 0;
        $msg = 'Dia excluído com sucesso';            
        return json_encode(['retcode' => $retcode, 'msg' => $msg]);        
    }

    public function getTabeladias(){
        $dias_vencimento = $this->repDias->lista();
        
        $data = [
            'dias_vencimento' => $dias_vencimento
        ];

        return View::make('dias_vencimento.tabela', $data);
                
    }

    public function getList(){

        $dias_vencimento = $this->repDias->lista();

        $data = [
            'dias_vencimento' => $dias_vencimento
        ];

        return View::make('dias_vencimento.listar', $data);
    }

    public function getTornardefault($dia){
        $sql = "update dia_vencimento set fl_default = 0";
        \DB::update(\DB::raw($sql));
        $dia = \DiaVencimento::find($dia);
        $dia->fl_default = 1;
        $dia->save();
    }
}