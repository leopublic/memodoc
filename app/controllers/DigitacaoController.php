<?php

class DigitacaoController extends BaseController {

    public function getCliente() {

        
    }

    public function postCliente(){

    }

    public function getCaixa(){

    }

    public function postCaixa(){

    }

    public function getItens($id_os_chave, $id_caixa){
        $os = \Os::find($id_os_chave);
        $id_cliente = $os->id_cliente;
        $itens = \Documento::where('id_caixa', '=', $id_caixa)
                         ->orderBy('nume_inicial')
                         ->get();
        
        $msg = '';
        $itens_remessa_a = ItemRemessa::whereRaw('id_remessa in (select id_remessa from remessa where id_cliente ='.$id_cliente.')')->whereNull('id_documento')->get();
        $itens_remessa = "";
        $documento = \Documento::where('id_caixa', '=', $id_caixa)->first();
        $sep = "";
        foreach($itens_remessa_a as $item_remessa){
            $itens_remessa .= $sep.'"'.$item_remessa->codigo.'"';
            $sep = ',';
        }
        $data = array(
            'itens'    => $itens
            ,'id_caixa' => $id_caixa
            ,'id_os_chave' => $id_os_chave
            ,'documento' => $documento
            ,'os' => $os
            ,'nomecliente' => $os->cliente->razaosocial.' - '.$os->cliente->nomefantasia.' ('.$os->id_cliente.')'
            ,'itens_remessa' => $itens_remessa
            ,'msg' => $msg
            );
        return View::make('digitacao/itens', $data);
    }


    public function postItens(){
        try{
            $id_os_chave = Input::get('id_os_chave');
            $id_caixa = Input::get('id_caixa');
            $ids = Input::get('ids');

            if(substr($ids, 0,2) == '##'){
                $ids = substr($ids, 2);
            }
            if (substr($ids, -2) == '##'){
                $ids = substr($ids, 0, strlen($ids) -2);
            }

            $os = \Os::find($id_os_chave);
            $rep = new Memodoc\Repositorios\RepositorioDocumentoEloquent;
            $ret =  $rep->adiciona_itens_lote($os, $id_caixa, $ids);
            Session::flash('msg', $ret['msg']);
        } catch(\Exception $e){
            Session::flash('errors', $e->getMessage()."<br/>".$e->getTraceAsString());
        }
        return Redirect::to('/digitacao/itens/'.$id_os_chave.'/'.$id_caixa);
    }

    public function getItenscadastrados($id_cliente, $id_caixapadrao){
        $docs = \Documento::where('id_cliente', '=', $id_cliente)
                         ->where('id_caixapadrao', '=', $id_caixapadrao)
                         ->orderBy('nume_inicial')
                         ->get();
        $itens = '';
        $virg = '';

        if (count($docs) == 0){
            $msg = 'Essa referência não existe nesse cliente';
            $codigo = '1';
        } else {
            $msg = '';
            $codigo = '0';
            foreach($docs as $doc){
                $itens .= $virg.$doc->nume_inicial;
                $virg = ',';
            }
        }
        $ret = array(
            'codigo' => $codigo
            ,'msg' => $msg
            ,'itens' => $itens
            );
        return json_encode($ret);
    }

    public function getTestaplanilha(){
        $rep = new Memodoc\Repositorios\RepositorioPlanilha;
        //$rep->abra(app_path('arquivos/anexos/Remessa MemoDoc-Excel.XLS'));
        $rep->abra(app_path('arquivos/anexos/xxx.xls'));
    }

    public function getCriaremessateste($id_cliente){
        $rep = new Memodoc\Repositorios\RepositorioRemessa;
        $rep->cria_remessa($id_cliente);
        return 'Remessa criada com sucesso';
    }

    public function getTestadicionaritens(){
        $rep = new Memodoc\Repositorios\RepositorioDocumentoEloquent;
        $ids = '11112##11113##11114##11115##11116##11117##11118';
        $id_caixapadrao = 10;
        $id_cliente = 662;
        \Documento::where('id_cliente', '=', 662)->where('id_caixapadrao', '=', '10')->where('id_item', '>', '2')->delete();
        \ItemRemessa::where('id_cliente', '=', 662)->update(array('id_documento' => null));
        $ret =  $rep->adiciona_itens_lote($id_cliente, $id_caixapadrao, $ids);

        $docs = \Documento::where('id_cliente', '=', 662)->where('id_caixapadrao', '=', '10')->orderBy('id_item')->get();
        print '<table>';
        foreach($docs as $doc){
            print '<tr><td>'.$doc->id_item.'</td><td>'.$doc->titulo.'</td><td>'.$doc->nume_inicial.'</td><td>'.$doc->conteudo.'</td></tr>';
        }

       $itens = \ItemRemessa::whereRaw('id_remessa in (select id_remessa from remessa where id_cliente=662)')->orderBy('id_remessa')->orderBy('ordem')->get();
        print '<table>';
        foreach($itens as $item){
            print '<tr><td>'.$item->id_item_remessa.'</td><td>'.$item->id_documento.'</td><td>'.$item->codigo.'</td></tr>';
        }

    }

    public function getRelatorio(){

        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        $data = array(
            'clientes' => $clientes
            ,'data' => Carbon::now('America/Sao_Paulo')->format('d/m/Y')
            ,'email' => ''
            );
        // return View::make('digitacao/itens', $data);
        return View::make('digitacao/relatorio', $data);
 
    }

    public function postRelatorio(){
        $id_cliente = Input::get('id_cliente');
        $data = Input::get('data');
        $email = Input::get('email');
        Input::flash();
        If ($id_cliente == '' || $data == '' || $email == ''){
            Session::set('errors', 'Informe todos os campos');
            return Redirect::to('/digitacao/relatorio');
        } else {
            Session::set('id_cliente', $id_cliente);
            Session::set('data', $data);
            Session::set('email', $email);
            Session::set('enviar', true);
        }

    }

    public function getDodia(){
        set_time_limit(0);
        $id_cliente = Session::get('id_cliente');
        $data = Session::get('data');
        $email = Session::get('email');

        Session::forget('id_cliente');
        Session::forget('data');
        Session::forget('email');
        Session::forget('enviar');

        $docs = \Documento::where('id_cliente', '=', $id_cliente)
                        ->where('created_at', '>', Carbon::createFromFormat('d/m/Y', $data)->format('Y-m-d'))
                        ->where('created_at', '<', Carbon::createFromFormat('d/m/Y', $data)->format('Y-m-d'))
                        ->where('fl_digitacao_massa', '=', 1)
                        ->get();

        $html = View::make('digitacao.relatorio_digitado')
                        ->with('transbordo', $transbordo)
                        ->with('transbordo_caixas', $transbordo_caixas)
        ;
        $cab = View::make('digitacao.relatorio_digitado_cab')
                        ->with('cliente', $cliente)
                        ->with('dia', $data)
        ;
        $mpdf = new \mPDF('utf-8', 'A4', '', '', '10', '10', '42', '14', '10', '10');  // largura x altura
        $mpdf->allow_charset_conversion = false;
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $cab = iconv("UTF-8", "UTF-8//IGNORE", $cab);

        $mpdf->SetHTMLHeader($cab);

        $mpdf->setFooter("||Página {PAGENO}/{nbpg}");

        $mpdf->WriteHTML($html);
        $caminho = storage_path().'temp';
        if(!is_dir($caminho)){
            mkdir($caminho, 0775, true);
        }
        $nome_arquivo = $caminho.'/'.\Auth::user()->id_usuario.'_relatorio_digitacao.pdf';
        if(file_exists($nome_arquivo)){
            unlink($nome_arquivo);
        }
        $mpdf->Output($nome_arquivo);



        \Mail::send('emails.digitacao', $data, function($message) use($email, $data, $nome_arquivo) {
            $message->to($emails)->subject('MEMODOC - Relatório de digitação '.$data);
            $message->attach($nome_arquivo);
          });


    }

    public function getTestebarras(){
        $itens = ItemRemessa::  take(80)->get();
        $mpdf = new \mPDF('utf-8', 'A4', '', '', '10', '10', '10', '10', '', '');  // largura x altura
        $mpdf->allow_charset_conversion = false;

        $data = array('itens' => $itens);
        $html = \View::make('digitacao/teste_codigos', $data);
        $mpdf->WriteHTML($html);
        $mpdf->Output();

    }

    public function getRessetadigitacao(){
        \Documento::whereRaw("id_documento in (select id_documento from item_remessa)")->delete();
        \ItemRemessa::update(array('id_documento' => null));
        DB::statement('truncate os_item_retirado');
    }
}