<?php

class GalpaoController extends BaseController {
    public function getIndex(){
        $galpoes = \Galpao::orderBy('id_galpao')->get();
        foreach($galpoes as &$galpao){
            $galpao->qtd = Endereco::naousados()->where('id_tipodocumento', '=', "1")
                            ->where('id_galpao', '=', $galpao->id_galpao)->count();
        }

        return View::make('galpao.index')
                        ->with('galpoes', $galpoes);
    }

    public function getVagasdisponiveis() {
        $tiposdocumentos = TipoDocumento::orderBy('descricao')->get();
        $galpoes = Endereco::galpoes();

        return View::make('espaco_disponivel')
                        ->with('tipodocumentos', $tiposdocumentos)
                        ->with('galpoes', $galpoes);
    }

    public function getBuracosdisponiveis() {
        $data = $this->dadosBuracosDisponiveis();
        return View::make('buracos_disponivel', $data);
    }

    public function getBuracosdisponiveisexcel() {
        $data = $this->dadosBuracosDisponiveis();
        $saida = View::make('buracos_disponivel_tabela', $data);
        $data['xls'] = true;
        $conteudo = iconv("UTF8", "ISO-8859-1", $saida);
        return Response::make($conteudo, '200', array(
                    'Content-Type' => 'application/vnd.ms-excel',
                    'Content-Disposition' => 'attachment; filename="relatorio_buracos.xls"'
        ));
    }

    public function dadosBuracosDisponiveis() {
        $data = array();
        $total = Endereco::get_qtdNuncaUtilizado();
        $totais = Endereco::nuncaUtilizadoPorCliente();

        foreach ($totais as &$cliente) {
            $sql = "select count(distinct id_caixa) qtd "
                    . " from documento"
                    . " join reserva on reserva.id_reserva = documento.id_reserva"
                    . " where primeira_entrada is null"
                    . " and id_caixa not in (select id_caixa from documento where titulo = 'CAIXA NOVA ENVIADA PARA CLIENTE' and conteudo is null) "
                    . " and reserva.data_reserva < DATE_SUB(now(), interval 180 day )"
                    . " and documento.id_cliente = " . $cliente->id_cliente;
            $total_180_inventariado = \DB::select(\DB::raw($sql));
            $cliente->qtd_180_inventariado = intval($total_180_inventariado[0]->qtd);
            $sql = "select count(distinct id_caixa) qtd "
                    . " from documento"
                    . " join reserva on reserva.id_reserva = documento.id_reserva"
                    . " where primeira_entrada is null"
                    . " and titulo = 'CAIXA NOVA ENVIADA PARA CLIENTE'"
                    . " and conteudo is null"
                    . " and reserva.data_reserva < DATE_SUB(now(), interval 180 day )"
                    . " and documento.id_cliente = " . $cliente->id_cliente;
            $total_180_nao_inventariado = \DB::select(\DB::raw($sql));
            $cliente->qtd_180_nao_inventariado = intval($total_180_nao_inventariado[0]->qtd);
            $sql = "select count(distinct id_caixa) qtd "
                    . " from documento "
                    . " where id_cliente = " . $cliente->id_cliente;
            $total_geral = \DB::select(\DB::raw($sql));
            $cliente->qtd_total = intval($total_geral[0]->qtd);
            $cli = \Cliente::find($cliente->id_cliente);
            $cliente->nomefantasia = $cli->nomefantasia;
            $cliente->razaosocial = $cli->razaosocial;
            $cliente->franquiacustodia = $cli->franquiacustodia;
        }
        $data['total'] = $total;
        $data['totais'] = $totais;
        return $data;
    }

    public function getDisponibilizar($id_galpao){
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $rep->disponibiliza($id_galpao);
        Session::flash('msg', 'Galpão disponibilizado com sucesso!');
        return Redirect::to('/galpao');
    }

    public function getIndisponibilizar($id_galpao){
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $rep->indisponibiliza($id_galpao);
        Session::flash('msg', 'Galpão indisponibilizado com sucesso!');
        return Redirect::to('/galpao');
    }

    public function getDisponibilizartodos(){
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $rep->disponibiliza_todos();
        Session::flash('msg', 'Todos os galpões estão foram disponibilizados para endereçamento!');
        return Redirect::to('/galpao');
    }

    public function getIndisponibilizartodos(){
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $rep->indisponibiliza_todos();
        Session::flash('msg', 'Todos os galpões estão foram indisponibilizados para endereçamento!');
        return Redirect::to('/galpao');
    }

    public function getMoviveisporcliente(){
        $sql = "select documento.id_cliente, cliente.razaosocial, cliente.nomefantasia, count(distinct documento.id_caixapadrao ) qtd from documento 
                join reserva on reserva.id_reserva = documento.id_reserva
                join cliente on cliente.id_cliente = documento.id_cliente
                where coalesce(cliente.franquiacustodia,0) > 0
                and datediff(now(), reserva.data_reserva) > coalesce(cliente.franquiacustodia, 0)
                and documento.primeira_entrada is null
                group by id_cliente
                order by count(distinct documento.id_caixapadrao ) desc
                ";
        $regs = \DB::select(\DB::raw($sql));
        $data = array(
            'regs' => $regs
            );
        return View::make('galpao.moviveis', $data);
    }
}