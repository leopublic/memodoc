<?php

use Carbon\Carbon;
use Memodoc\Repositorios;

class SicoobRemessaController extends BaseController {
    public function getIndex() {
        $lotes_rps = \LoteRps::orderBy('id_lote_rps', 'desc')->lists('id_lote_rps', 'id_lote_rps');
        $remessas = \SicoobRemessa::orderBy('id_sicoob_remessa', 'desc')->get();
        return View::make('sicoob_remessa/index')
                        ->with('lotes_rps', $lotes_rps)
                        ->with('remessas', $remessas)
                        ;
    }

    public function postIndex() {
        return Redirect::to('/sicoobremessa');
    }

    public function getGerarlote($id_lote_rps ){
        set_time_limit(0);

        $lote = \LoteRps::find($id_lote_rps);
        $remessa = new \SicoobRemessa;
        $remessa->data_carga = Carbon::now('America/Sao_Paulo')->format('Y-m-d');
        $remessa->id_lote_rps = $id_lote_rps;
        $remessa->save();

        $arquivo = new \Memodoc\Utilitarios\ArquivoSicoobRemessa($remessa);
        
        $caminho = $arquivo->gerar();
        $headers = array(
            'Content-type' => "application/txt",
            'Content-Transfer-Encoding' => 'binary',
            'Content-Description' => 'File Transfer',
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'public'
        );
        return Response::download($caminho, 'remessa_'.$remessa->id_sicoob_remessa.'.txt', $headers);
    }

    public function getRegerar($id_sicoob_remessa){
        set_time_limit(0);
        
        $remessa = \SicoobRemessa::find($id_sicoob_remessa);

        $arquivo = new \Memodoc\Utilitarios\ArquivoSicoobRemessa($remessa);
        
        $caminho = $arquivo->gerar();

        $headers = array(
            'Content-type' => "text/plain",
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'public'
        );
        return Response::download($caminho, 'remessa_'.$remessa->id_sicoob_remessa.'.txt', $headers);
        
    }

    public function getDetalhe($id_sicoob_retorno){
        $retorno = \SicoobRetorno::find($id_sicoob_retorno);
        $regs = \RegistroSicoobRetorno::where('id_sicoob_retorno', '=', $id_sicoob_retorno)->orderBy('ordem', 'asc')->get();
        
        return View::make('sicoob_retorno/detalhe')
                    ->with('regs', $regs)
                    ->with('retorno', $retorno)
                    ;        
    }

    public function getProcessar($id_lote_rps){
        $rep = new Repositorios\RepositorioImportadorSicoobRetorno;
        $rep->processa_arquivo($id_sicoob_retorno);
        Session::flash('success', 'Arquivo reprocessado com sucesso!!');
        return Redirect::to('/sicoobretorno');
    }

    public function getBoleto($id_nota_fiscal){
        $boleto = new \Boleto;
        
        return View::make('sicoob_remessa/boleto')
            ->with('boleto', $boleto)
        ;        
    }
}