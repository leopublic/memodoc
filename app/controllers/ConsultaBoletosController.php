<?php

use Carbon\Carbon;
use Memodoc\Repositorios;

class ConsultaBoletosController extends BaseController {
    public function getIndex() {
        $consultas = \ConsultaBoletos::orderBy('data_carga', 'desc')->get();

        return View::make('consulta_boletos/index')
                        ->with('consultas', $consultas)
        ;
    }

    public function postIndex() {
        return Redirect::to('/consultaboleto');
    }

    public function postNovaconsulta(){
        set_time_limit(0);
        $rep = new Repositorios\RepositorioConsultaBoleto;

        $file = Input::file('arquivo');

        $consulta = new \ConsultaBoletos;
        $consulta->data_carga = Carbon::now('America/Sao_Paulo')->format('Y-m-d');
        $consulta->nome_arquivo = $file->getClientOriginalName();
        $consulta->observacao = Input::get('observacao');
        $consulta->save();

        $file->move($consulta->caminho(), $consulta->id_consulta_boletos);

        $rep->processa_arquivo($consulta->id_consulta_boletos);
        
        return Redirect::to('/consultaboletos');
    }

    public function getDetalhe($id_consulta_boletos){
        $consulta = \ConsultaBoletos::find($id_consulta_boletos);
        $regs = \RegistroConsultaBoletos::where('id_consulta_boletos', '=', $id_consulta_boletos)->orderBy('ordem', 'asc')->get();
        
        return View::make('consulta_boletos/detalhe')
                    ->with('regs', $regs)
                    ->with('consulta', $consulta)
                    ;        
    }

    public function getProcessar($id_consulta_boletos){
        $rep = new Repositorios\RepositorioConsultaBoleto;
        $rep->processa_arquivo($id_consulta_boletos);
        Session::flash('success', 'Arquivo reprocessado com sucesso!!');
        return Redirect::to('/consultaboletos');
    }

}