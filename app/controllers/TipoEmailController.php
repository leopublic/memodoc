<?php

class TipoEmailController extends BaseController {

        /**
         * Listagem de Tipo email
         * @return type View
         */
	public function getIndex()
	{
		Input::flash();
		$instancias = TipoEmail::all();

		$colunas = array(
			array('titulo' =>'Descrição', "campo"=> 'descricao')
		);
		
		return View::make('modelo_list')
					->with('linkEdicao','tipoemail/edit')
					->with('linkCriacao','tipoemail/create')
					->with('linkRemocao','tipoemail/delete')
					->with('titulo', 'Tipos de email')
					->with('colunas', $colunas)
					->with('instancias', $instancias)
				;		
	}

        /**
         * Editar 
         * @param type $id
         * @return type View
         */
	public function getEdit($id)
	{
                $model = TipoEmail::find($id);
		return View::make('tipoemail_form', compact('model'));		
	}
        
	public function postEdit($id)
	{
                Input::flash();
                $model = TipoEmail::findOrFail($id);
                $model->fill(Input::all());
                $id = $model->save();
                if($id){
                    Session::flash('success', 'Tipo email foi atualizado com sucesso !');
                    return  Redirect::to('tipoemail/index');
                }
			
	}
        
        
        /**
         * Create
         * @return type View
         */
	public function getCreate()
	{
		return View::make('tipoemail_form');		
	}
        
	public function postCreate()
	{
                Input::flash();
                $model = new TipoEmail();
                $model->fill(Input::all());
                $id = $model->save();
                if($id){
                    Session::flash('success', 'Tipo de email cadastrado com sucesso !');
                    return  Redirect::to('tipoemail/index');
                }
			
	}
        
	public function getDelete($id)
	{
            
            if(TipoEmail::getPermiteDeletar($id)){
                $model = TipoEmail::find($id);
                $model->delete();
                Session::flash('msg', 'Tipo de email '.$model->descricao.' foi deletado com sucesso !');
                return  Redirect::to('tipoemail/index');
            }else{
                Session::flash('error', 'Este tipo de email nao pode ser deletado, pois possui dados dependentes !');
                return  Redirect::to('tipoemail/index');
            }
				
	}
        

    
}