<?php

class DocumentoClienteController extends BaseController {

    function getCliente() {
        $clientes = Cliente::all();
        return View::make('documento_cliente')->with('clientes', $clientes);
    }

    function getEditar($id_caixapadrao, $id_item = 0) {
        if ($id_item > 0) {
            $doc = Documento::where('id_cliente', '=', Auth::user()->id_cliente)
                    ->where('id_caixapadrao', '=', $id_caixapadrao)
                    ->where('id_item', '=', $id_item)
                    ->first()
            ;
        } else {
            $doc = new Documento;
            $doc->id_cliente = Auth::user()->id_cliente;
            $doc->id_caixapadrao = $id_caixapadrao;
        }
        $cliente = \Cliente::find(Auth::user()->id_cliente);
        if ($cliente->fl_controle_revisao && $doc->fl_revisado) {
            $disabled = 'disabled';
        } else {
            $disabled = '';
        }

        $propriedade = \Propriedade::where('id_cliente', '=', \Auth::user()->id_cliente)->first();
        if (!is_object($propriedade)) {
            $propriedade = null;
            $valores = array();
        } else {
            $valores = array("" => '(nao informado)') + \Valor::where('id_propriedade', '=', $propriedade->id_propriedade)->orderBy('nome')->lists('nome', 'id_valor');
        }


        return View::make('documento_cliente_editar')
                        ->with('documento', $doc)
                        ->with('cliente', $cliente)
                        ->with('propriedade', $propriedade)
                        ->with('valores', $valores)
                        ->with('disabled', $disabled)
        ;
    }

    // Trata o post dos novos
    function postEditar($id_caixapadrao, $id_item = 0) {
        if (Input::has('Cancelar')) {
            if ($id_item > 0) {
                $msg = "Edição cancelada com sucesso";
            } else {
                $msg = "Inclusão cancelada com sucesso";
            }
            Session::flash('msg', $msg);
            return Redirect::to('/publico/documento/cadastrar/' . $id_caixapadrao);
        } else {
            Input::flash();
            $post = Input::all();
            $errors = array();
            if ($id_item == 0) {
                $xdoc = Documento::where('id_cliente', '=', Auth::user()->id_cliente)
                        ->where('id_caixapadrao', '=', $id_caixapadrao)
                        ->first();

                $doc = new Documento();
                $doc->id_caixapadrao = $id_caixapadrao;
                $doc->id_caixa = $xdoc->id_caixa;
                $doc->id_tipodocumento = $xdoc->id_tipodocumento;
                $doc->id_reserva = $xdoc->id_reserva;
                $doc->emcasa = $xdoc->emcasa;
                $doc->primeira_entrada = $xdoc->primeira_entrada;
                $doc->data_digitacao = date('Y-m-d');
                $doc->usuario = Auth::user()->id_usuario;
                $doc->id_cliente = Auth::user()->id_cliente;
                $id_item = Documento::where('id_cliente', '=', Auth::user()->id_cliente)
                        ->where('id_caixapadrao', '=', $id_caixapadrao)
                        ->max('id_item');

                $doc->id_item = $id_item + 1;
            } else {
                $doc = Documento::where('id_cliente', '=', Auth::user()->id_cliente)
                        ->where('id_caixapadrao', '=', $id_caixapadrao)
                        ->where('id_item', '=', $id_item)
                        ->first()
                ;
                // Pedro 21/08/2014: A primeira entrada só deve ser marcada pelo checkin. A digitação do documento não deve fazer com que 
                // a caixa seja cobrada. Somente a primeira entrada pode fazer a caixa ser cobrada.
                // 
//                if ($doc->primeira_entrada == ''){
//                    $doc->primeira_entrada = date('Y-m-d');
//                }
                if ($doc->data_digitacao == '') {
                    $doc->data_digitacao = date('Y-m-d');
                }
                if ($doc->usuario == '') {
                    $doc->usuario = Auth::user()->id_usuario;
                }
            }
            $doc->id_usuario_alteracao = Auth::user()->id_usuario;
            $doc->id_expurgo = 0;

            $doc->inicio_fmt = Input::get('inicio_fmt', null);
            $doc->fim_fmt = Input::get('fim_fmt', null);

            $expurgo_programado = Input::get('expurgo_programado');
            if (intval($expurgo_programado) > 0 ){
                if ($expurgo_programado < date('Y')){
                    $errors[] = 'Informe um ano maior ou igual a '.date('Y');
                } else {
                    $doc->expurgo_programado = $expurgo_programado;
                }
            } else {
                $doc->expurgo_programado = null;
            }

            $expurgo_programado_mes = Input::get('expurgo_programado_mes');
            if (intval($expurgo_programado_mes) > 0 ){
                $doc->expurgo_programado_mes = $expurgo_programado_mes;
            } else {
                $doc->expurgo_programado_mes = null;
            }

            if (!isset($post['id_centro']) || !$post['id_centro'] > 0) {
                unset($post['id_centro']);
                $doc->id_centro = null;
            }
            if (!isset($post['id_setor']) || !$post['id_setor'] > 0) {
                unset($post['id_setor']);
                $doc->id_setor = null;
            }
            if (!isset($post['id_departamento']) || !$post['id_departamento'] > 0) {
                unset($post['id_departamento']);
                $doc->id_departamento = null;
            }

            if ($post['nume_inicial'] == '') {
                unset($post['nume_inicial']);
                $doc->nume_inicial = null;
            }

            if ($post['nume_final'] == '') {
                unset($post['nume_final']);
                $doc->nume_final = null;
            }

            if (count($errors) > 0) {
                Session::flash('errors', $errors);
                return Redirect::to('/publico/documento/editar/' . $id_caixapadrao . '/' . $id_item)
                                ->withInput()
                ;
            }

            try {
                $doc->fill($post);
                $doc->itemalterado = 1;
                $doc->id_usuario_alteracao = Auth::user()->id_usuario;
                $doc->conteudo = str_replace("\n", '<br/>', $post['conteudo']);
                $doc->id_usuario_alteracao = \Auth::user()->id_usuario;
                $doc->evento = __CLASS__ . '.' . __FUNCTION__;
                $doc->save();

                Session::flash('success', 'Dados alterados com sucesso');
                return Redirect::to('/publico/documento/redirectcadastrar/' . $id_caixapadrao);
            } catch (\Whoops\Example\Exception $exc) {
                Session::flash('errors', $exc->getTraceAsString());
                return Redirect::to('/publico/documento/editar/' . $id_caixapadrao . '/' . $id_item)
                                ->withInput()
                ;
            }
        }
    }

    function getRedirectcadastrar($id_caixapadrao) {
        Session::reflash();
        return Redirect::to('/publico/documento/cadastrar/' . $id_caixapadrao);
    }

    function getCadastrar($id_caixapadrao = '', $id_item = '') {
        $id_cliente = Auth::user()->id_cliente;
        $cliente = Cliente::findOrFail($id_cliente);

        if ($id_caixapadrao > 0) {
            $documentos = Documento::where('id_cliente', '=', Auth::user()->id_cliente)
                    ->where('id_caixapadrao', '=', $id_caixapadrao)
                    ->get()
            ;
            if(count($documentos) > 0){
                if ($id_item > 0) {
                    $documento = Documento::where('id_cliente', '=', Auth::user()->id_cliente)
                            ->where('id_caixapadrao', '=', $id_caixapadrao)
                            ->where('id_item', '=', $id_item)
                            ->first()
                    ;
                } else {
                    $documento = new Documento;
                }
            } else{
                $documento = new Documento;
                Session::flash('errors', 'Essa caixa não existe. Verifique.');
            }
        } else {
            $documentos = null;
            $documento = new Documento;
            Session::flash('errors', 'Informe o número da caixa.');
        }
        $propriedade = \Propriedade::where('id_cliente', '=', \Auth::user()->id_cliente)->first();

        $metodo = Session::get('metodo');
        return View::make('documento_cliente_cadastrar')
                        ->with('documento', $documento)
                        ->with('documentos', $documentos)
                        ->with('cliente', $cliente)
                        ->with('id_caixapadrao', $id_caixapadrao)
                        ->with('id_item', $id_item)
                        ->with('metodo', $metodo)
                        ->with('propriedade', $propriedade)
        ;
    }

    function postCarregacaixa() {
        Input::flash();
        return Redirect::to('/publico/documento/cadastrar/' . Input::get('id_caixapadrao'))
                        ->with('metodo', 'criar')
                        ->withInput();
    }

    function postCriar($id_caixapadrao) {
        Input::flash();

        $documento = Documento::where('id_cliente', '=', Auth::user()->id_cliente)
                ->where('id_caixapadrao', '=', $id_caixapadrao)
                ->where('id_item', '=', Input::get('id_item'))
                ->first()
        ;
        $msg = '';
        if (isset($documento)) {
            $msg = "Já existe um item com esse número. Verifique";
        } else {
            $docx = Documento::where('id_cliente', '=', Auth::user()->id_cliente)
                    ->where('id_caixapadrao', '=', $id_caixapadrao)
                    ->first()
            ;

            $documento = new Documento();
            $documento->id_item = Input::get('id_item');
            $documento->id_cliente = Auth::user()->id_cliente;
            $documento->id_caixapadrao = $id_caixapadrao;
            $documento->id_caixa = $docx->id_caixa;
            $documento->id_tipodocumento = $docx->id_tipodocumento;
            $documento->titulo = Input::get('titulo');
            $documento->conteudo = nl2br(Input::get('conteudo'));
            $documento->inicio = Input::get('inicio');
            $documento->fim = Input::get('fim');
            $documento->nume_inicial = Input::get('nume_inicial');
            $documento->nume_final = Input::get('nume_final');
            $documento->alfa_inicial = Input::get('alfa_inicial');
            $documento->alfa_final = Input::get('alfa_final');
            if (Input::get('id_centro') > 0) {
                $documento->id_centro = Input::get('id_centro');
            }
            if (Input::get('id_setor') > 0) {
                $documento->id_setor = Input::get('id_setor');
            }
            if (Input::get('id_departamento') > 0) {
                $documento->id_departamento = Input::get('id_departamento');
            }
            $documento->id_usuario_alteracao = \Auth::user()->id_usuario;
            $documento->evento = __CLASS__ . '.' . __FUNCTION__;
            $documento->save();
            $msg = 'Documento criado com sucesso!';
        }
        return Redirect::to('/publico/documento/cadastrar/' . $id_caixapadrao)
                        ->with('metodo', 'criar')
                        ->with('msg', $msg)
                        ->withInput();
    }

    function getIndex() {
        $texto = Input::old('texto');
        $id_caixapadrao_ini = Input::old('id_caixapadrao_ini');
        $id_caixapadrao_fim = Input::old('id_caixapadrao_fim');
        $id_departamento = Input::old('id_departamento');
        $id_centro = Input::old('id_centro');
        $id_setor = Input::old('id_setor');
        $inicio = Input::old('inicio');
        $fim = Input::old('fim');
        $nume_inicial = Input::old('nume_inicial');
        $nume_final = Input::old('nume_final');
        $alfa_inicial = Input::old('alfa_inicial');
        $alfa_final = Input::old('alfa_final');
        $referencia = Input::old('referencia');
        $id_valor = Input::old('id_valor', null);
        $status = Input::old('status');
        $exp_progr_ano_ini = Input::old('exp_progr_ano_ini');
        $exp_progr_mes_ini = Input::old('exp_progr_mes_ini');
        $exp_progr_ano_fim = Input::old('exp_progr_ano_fim');
        $exp_progr_mes_fim = Input::old('exp_progr_mes_fim');
        $propriedade = \Propriedade::where('id_cliente', '=', \Auth::user()->id_cliente)->first();
        if (!is_object($propriedade)) {
            $valores = array();
            $propriedade = null;
        } else {
            $valores = array('' => '(todos)') + \Valor::where('id_propriedade', '=', $propriedade->id_propriedade)->orderBy('nome')->lists('nome', 'id_valor');
        }
        $cliente = Auth::user()->cliente;
        $setores = Auth::user()->setores()->get();
        if (count($setores) == 0) {
            $setores = $cliente->setor()->get();
        }
        $departamentos = Auth::user()->departamentos()->get();
        if (count($departamentos) == 0) {
            $departamentos = $cliente->departamento()->get();
        }
        $centrocustos = Auth::user()->centrocustos()->get();
        if (count($centrocustos) == 0) {
            $centrocustos = $cliente->centrocusto()->get();
        }
        $documentos = Documento::filtrarPor(Auth::user()->id_cliente, $texto, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, null, $id_caixapadrao_ini, $referencia, null, null,null,$id_caixapadrao_fim,null,null, null, null, $id_valor, $exp_progr_mes_ini, $exp_progr_ano_ini, $exp_progr_mes_fim, $exp_progr_ano_fim);
        $documentos->status($status);
        $documentos = $documentos->disponiveisPara(Auth::user());
        $doc = new Documento;
        $qtdDocumentos = $doc->qtdDocumentosSelecionados2($documentos);
        $qtdCaixas = $doc->qtdCaixasSelecionados2($documentos);

        $documentos = $documentos->ordenacaoPadrao()->paginate(20);
        $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent();
        $documentos = $rep->adicionaStatusCaixas($documentos, \Auth::user()->cliente->franquiacustodia);
//        $queries = DB::getQueryLog();
//        $last_query = end($queries);
//        var_dump($last_query);
//        $queries = DB::getQueryLog();
//        $last_query = end($queries);
//        var_dump($last_query);
        Session::reflash();
        return View::make('documento_cliente_buscar')
                        ->with('cliente', $cliente)
                        ->with('setores', FormSelect($setores))
                        ->with('centrocustos', FormSelect($centrocustos))
                        ->with('departamentos', FormSelect($departamentos))
                        ->with('documentos', $documentos)
                        ->with('model', new Documento)
                        ->with('qtdDocumentos', $qtdDocumentos)
                        ->with('qtdCaixas', $qtdCaixas)
                        ->with('propriedade', $propriedade)
                        ->with('valores', $valores)
        ;
    }

    function postIndex() {
        Input::flash();
        return Redirect::to('/publico/documento/')->withInput();
    }

    // Ajax - departamentos
    function getAjaxdepartamento($id_cliente) {
        $departamentos = DepCliente::where('id_cliente', '=', Auth::user()->id_cliente)->get();
        $select = FormSelect($departamentos, 'nome');
        echo View::make('padrao.select_option', compact('select'));
    }

    // Ajax - setores
    function getAjaxsetor($id_cliente) {
        $data = SetCliente::where('id_cliente', '=', Auth::user()->id_cliente)->get();
        $select = FormSelect($data, 'nome');
        echo View::make('padrao.select_option', compact('select'));
    }

    // Ajax - centro de custos
    function getAjaxcentrocusto($id_cliente) {
        $data = CentroDeCusto::where('id_cliente', '=', Auth::user()->id_cliente)->get();
        $select = FormSelect($data, 'nome');
        echo View::make('padrao.select_option', compact('select'));
    }

    function getLocalizar($id) {

        $cliente = Cliente::findOrFail($id);
        $setores = $cliente->setor()->get();
        $departamentos = $cliente->departamento()->get();
        $centrocustos = $cliente->centrocusto()->get();

        return View::make('documento_localizar')
                        ->with('cliente', $cliente)
                        ->with('setores', $this->formselect($setores))
                        ->with('centrocustos', $this->formselect($centrocustos))
                        ->with('departamentos', $this->formselect($departamentos))
                        ->with('model', new Documento);
    }

    function postLocalizar($id) {

        Input::flash();

        $titulo = Input::get('titulo');
        $conteudo = Input::get('conteudo');
        $id_departamento = Input::get('id_departamento');
        $id_centro = Input::get('id_centro');
        $id_setor = Input::get('id_setor');
        $inicio = Input::get('inicio');
        $fim = Input::get('fim');
        $nume_inicial = Input::get('nume_inicial');
        $nume_final = Input::get('nume_final');
        $alfa_inicial = Input::get('alfa_inicial');
        $alfa_final = Input::get('alfa_final');
        $id_caixa = Input::get('id_caixa');

        $documentos = Documento::getLocalizar($id, $titulo, $conteudo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa);

        $cliente = Cliente::findOrFail($id);
        $setores = $cliente->setor()->get();
        $departamentos = $cliente->departamento()->get();
        $centrocustos = $cliente->centrocusto()->get();

        return View::make('documento_localizar')
                        ->with('cliente', $cliente)
                        ->with('setores', $this->formselect($setores))
                        ->with('centrocustos', $this->formselect($centrocustos))
                        ->with('departamentos', $this->formselect($departamentos))
                        ->with('documentos', $documentos)
                        ->with('model', new Documento);
    }

    public function postPdf($id) {

        $titulo = Input::get('titulo');
        $id_departamento = Input::get('id_departamento');
        $id_centro = Input::get('id_centro');
        $id_setor = Input::get('id_setor');
        $inicio = Input::get('inicio');
        $fim = Input::get('fim');
        $nume_inicial = Input::get('nume_inicial');
        $nume_final = Input::get('nume_final');
        $alfa_inicial = Input::get('alfa_inicial');
        $alfa_final = Input::get('alfa_final');
        $id_caixa = Input::get('id_caixa');

        $cliente = Cliente::findOrFail($id);
        $documentos = Documento::getLocalizar($id, $titulo, null, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa);

        $html = View::make('documento_pdf')
                ->with('documentos', $documentos)
                ->with('cliente', $cliente);


        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);

        $this->mpdf = new mPDF('utf-8', 'A4', '', '', '2', '2', '2', '2', '', '');  // largura x altura
        $this->mpdf->allow_charset_conversion = false;

        $this->mpdf->WriteHTML($html);
        $this->mpdf->debug = true;
        $this->mpdf->Output();
    }

    /**
     * Transform model in adapter Form::select
     * @link http://laravel.com/docs/html#drop-down-lists DropDown-lists
     * @param type $model
     * @param type $name
     * @return type array
     */
    function formselect($model, $name = 'nome') {
        $data = array();
        if ($model->count() > 0) {
            $data[''] = 'Selecione';
            foreach ($model as $fields) {
                foreach ($fields->getAttributes() as $fieldname => $field) {
                    if ($fieldname == $name) {
                        $data[$fields->getKey()] = $field;
                    }
                }
            }
        }
        return $data;
    }

    public function getImporte($pNomeTabela) {
        switch ($pNomeTabela) {
            case "cliente":
                Cliente::Recarregue();
                break;

            case "nivel":
                Nivel::Recarregue();
                break;

            case "usuario":
                Usuario::Recarregue();
                break;
        }
    }

    function array_rename(&$array, $name, $name_update) {
        foreach ($array as $k => $v) {
            $array[$k] [$name_update] = $array[$k] [$name];
            unset($array[$k][$name]);
        }
    }
    

}
