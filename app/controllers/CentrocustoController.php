<?php

class CentrocustoController extends BaseController {

	public function getIndex()
	{

                $centrocustos = CentroDeCusto::all();

		return View::make('centrocusto_list')
					->with('centrocustos', $centrocustos)
				;
	}

	public function postIndex()
	{
		Input::flash();
		$usuarios = Usuario::all();

		return View::make('usuario_list')
					->with('usuarios', $usuarios)
				;
	}
}