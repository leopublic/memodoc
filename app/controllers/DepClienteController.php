<?php

class DepClienteController extends BaseController {

    public function getIndex($id_cliente) {
        $cliente = Cliente::findOrFail($id_cliente);
        $servicos = $cliente->tipoprodcliente;
        return View::make('cliente_list_servicos', compact('cliente', 'servicos'));
    }

    public function getServicoseditar($id_cliente) {
        $cliente = Cliente::findOrFail($id_cliente);
        $servicos = $cliente->tipoprodcliente;
        return View::make('cliente_list_servicoseditar', compact('cliente', 'servicos'));
    }

    public function postServicoseditar($id_cliente) {
        $valor_contratos = Input::get('valor_contrato');
        $valor_urgencias = Input::get('valor_urgencia');
        $minimos = Input::get('minimo');
        try {
            DB::connection()->getPdo()->beginTransaction();
            foreach ($valor_contratos as $id_tipoproduto => $valor_contrato) {
                TipoProdCliente::salvar(array(
                    'id_tipoproduto' => $id_tipoproduto, // chave composta
                    'id_cliente' => $id_cliente, // chave composta
                    'valor_contrato' => str_replace(',', '.', str_replace('.', '', $valor_contrato)),
                    'valor_urgencia' => str_replace(',', '.', str_replace('.', '', $valor_urgencias[$id_tipoproduto])),
                    'minimo' => $minimos[$id_tipoproduto],
                ));
            }
            DB::connection()->getPdo()->commit();
            Session::flash('success', 'Valores dos serviços foram alterados com sucesso');
        } catch (Exception $exc) {
            Session::flash('error', $exc->getTraceAsString());
        }
        return Redirect::to('/cliente/servicos/' . $id_cliente . '#tab_servicos');
        //$cliente = Cliente::findOrFail($id_cliente);
        //$servicos = $cliente->tipoprodcliente;
        //return View::make('cliente_list_servicoseditar', compact('cliente','servicos'));
    }

}
