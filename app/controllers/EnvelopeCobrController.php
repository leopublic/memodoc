<?php

class EnvelopeCobrController extends BaseController {

    public function getIndex(){
        $clientes_correio = \Cliente::ativos()
                    ->where('id_tipo_faturamento', '=', \TipoFaturamento::tfCORREIO)
                    ->orderBy('razaosocial')
                    ->get();
        $clientes_outros = \Cliente::ativos()
                    ->select(array('id_cliente', DB::raw("concat(razaosocial, ' (', substr(concat('000',id_cliente), -3), ' - ', nomefantasia, ')') as razaosocial")))
                    ->where('id_tipo_faturamento', '<>', \TipoFaturamento::tfCORREIO)
                    ->orderBy('razaosocial')
                    ->get();
        
        return View::make('envelope.index')
                ->with('clientes_correio', $clientes_correio)
                ->with('clientes_outros', $clientes_outros)
        ;
    }
    
    public function getGerar($id_cliente) {
        $cliente = \Cliente::find($id_cliente);
        $mpdf = new \mPDF('utf-8', 'A4', '', '', '10', '10', '10', '10', '', '');  // largura x altura
        $mpdf->allow_charset_conversion = false;
        $html = $this->criaEnvelope($cliente);
        $mpdf->WriteHTML(iconv("UTF-8", "UTF-8//IGNORE", $html));
        $mpdf->Output();
    }
    
    public function criaEnvelope(\Cliente $cliente){
        return View::make('envelope.envelope')
                  ->with('cliente', $cliente)
        ;        
    }
    
    public function getGerartodos(){
        set_time_limit(0);
        session_write_close();
        $clientes_correio = \Cliente::ativos()
                    ->where('id_tipo_faturamento', '=', \TipoFaturamento::tfCORREIO)
                    ->orderBy('razaosocial')
                    ->get();

        $mpdf = new \mPDF('utf-8', 'A4', '', '', '10', '10', '10', '10', '', '');  // largura x altura
        $mpdf->allow_charset_conversion = false;
        $novaPagina = false;
        foreach($clientes_correio as $cliente){
            $html = $this->criaEnvelope($cliente);
            if($novaPagina){
                $mpdf->AddPage('P');
            }
            $mpdf->WriteHTML(iconv("UTF-8", "UTF-8//IGNORE", $html));
            $novaPagina = true;
        }
        $mpdf->Output();
    }
    
    public function getEmail($id_cliente){
        $cliente = \Cliente::find($id_cliente);
        $cliente->id_tipo_faturamento = \TipoFaturamento::tfEMAIL;
        $cliente->save();
        Session::flash('success', 'Cliente "'.$cliente->razaosocial.' - '.substr('000'.$id_cliente, -3).'" alterado para E-MAIL com sucesso');                
        return Redirect::to('/envelopecobr/');
    }
    
    public function getCorreio($id_cliente){
        $cliente = \Cliente::find($id_cliente);
        $cliente->id_tipo_faturamento = \TipoFaturamento::tfCORREIO;
        $cliente->save();
        Session::flash('success', 'Cliente "'.$cliente->razaosocial.' - '.substr('000'.$id_cliente, -3).'" alterado para CORREIO com sucesso');                
        return Redirect::to('/envelopecobr/');
    }
}
