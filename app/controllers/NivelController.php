<?php

class NivelController extends BaseController {

    public function getIndex() {
        Input::flash();
        $instancias = Nivel::all();

        $colunas = array(
            array('titulo' => 'Id', "campo" => 'id_nivel')
            ,array('titulo' => 'Descrição', "campo" => 'descricao')
        );

        return View::make('modelo_list')
                        ->with('nomeId', 'id_nivel')
                        ->with('classe', 'nivel')
                        ->with('titulo', 'Níveis de acesso')
                        ->with('colunas', $colunas)
                        ->with('instancias', $instancias)
        ;
    }

}
