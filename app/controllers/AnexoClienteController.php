<?php

class AnexoClienteController extends BaseController {


    public function getDownload($id_anexo) {
        $anexo = \Anexo::find($id_anexo);
        $doc = \Documento::find($anexo->id_documento);
        if ($doc->id_cliente == Auth::user()->id_cliente){
            $headers = array(
                'Content-type' => $anexo->mime,
                'Expires' => 0,
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Pragma' => 'public',
                'Content-Length' => $anexo->tamanho
            );
            return Response::download($anexo->caminho(), Str::ascii($anexo->nome_original), $headers);
        } else {

            return Redirect::to('/home/bemvindocliente')
                        ->with('msg', 'Você não tem acesso a esse arquivo.');
        }
        }


}
