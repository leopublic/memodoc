<?php

class GerencialController extends BaseController {

    function getSituacaocaixas() {
        Input::flush();
        return $this->situacaocaixas();
    }

    function postSituacaocaixas(){
        Input::flash();
        if (Input::has('xls')){
            $conteudo =iconv("UTF8", "ISO-8859-1",  $this->situacaocaixas(Input::get('fl_ativo'), Input::get("search"), 'xls'));
            return Response::make($conteudo, '200', array(
                'Content-Type' => 'application/vnd.ms-excel',
                'Content-Disposition' => 'attachment; filename="estatistica_caixas.xls"'
            ));

        } else {
            return $this->situacaocaixas(Input::get('fl_ativo'), Input::get("search"));
        }
    }

    function situacaocaixas($fl_ativo = 1, $search = '', $saida = ''){
        $sql = "select cliente.id_cliente, cliente.fl_ativo, razaosocial, nomefantasia,  qtd_caixas_reservadas, valorminimofaturamento, nivel_inadimplencia"
                . ", qtd_caixas_consulta, qtd_caixas_total"
                . ", qtd_caixas_nunca_movimentadas, qtd_caixas_galpao"
                . ", tipoprodcliente.valor_contrato, cliente.obs"
                . " from cliente "
                . " left join tipoprodcliente on cliente.id_cliente = tipoprodcliente.id_cliente and tipoprodcliente.id_tipoproduto = 3"
                . " where 1=1 ";
        $where  = array();
        if ($search != ''){
            $sql .= " and concat(cliente.id_cliente, razaosocial, nomefantasia) like '%".$search."%'";
        }
        if ($fl_ativo == '1' || $fl_ativo == '0'){
            $sql .= " and coalesce(fl_ativo, 0) = ".$fl_ativo;
        }
        $disponiveis = \Endereco::get_qtdDisponivel(1);
        $clientes = \DB::select(\DB::raw($sql));
        $titulo = "Situação das caixas por cliente";
        if ($saida == 'xls'){
            return View::make('gerencial.situacaocaixas_xls')->with('clientes', $clientes)->with('disponiveis', $disponiveis)->with('titulo', $titulo)->with('xls',true);
        } else {
            return View::make('gerencial.situacaocaixas')->with('clientes', $clientes)->with('disponiveis', $disponiveis)->with('titulo', $titulo);
        }
    }

    function getSituacaoinadimplentes() {
        Input::flush();
        return $this->situacaoinadimplentes();
    }

    function postSituacaoinadimplentes(){
        Input::flash();
        if (Input::has('xls')){
            $conteudo =iconv("UTF8", "ISO-8859-1",  $this->situacaoinadimplentes(Input::get('fl_ativo'), Input::get("search"), 'xls'));
            return Response::make($conteudo, '200', array(
                'Content-Type' => 'application/vnd.ms-excel',
                'Content-Disposition' => 'attachment; filename="inadimplentes.xls"'
            ));

        } else {
            return $this->situacaoinadimplentes(Input::get('fl_ativo'), Input::get("search"));
        }
    }

    function situacaoinadimplentes($fl_ativo = 1, $search = '', $saida = ''){
        $sql = "select cliente.id_cliente, cliente.fl_ativo, razaosocial, nomefantasia,  qtd_caixas_reservadas, valorminimofaturamento, nivel_inadimplencia"
                . ", qtd_caixas_consulta, qtd_caixas_total"
                . ", qtd_caixas_nunca_movimentadas, qtd_caixas_galpao"
                . ", tipoprodcliente.valor_contrato, cliente.obs"
                . " from cliente "
                . " left join tipoprodcliente on cliente.id_cliente = tipoprodcliente.id_cliente and tipoprodcliente.id_tipoproduto = 3"
                . " where cliente.nivel_inadimplencia > 0 ";
        $where  = array();
        if ($search != ''){
            $sql .= " and concat(cliente.id_cliente, razaosocial, nomefantasia) like '%".$search."%'";
        }
        if ($fl_ativo == '1' || $fl_ativo == '0'){
            $sql .= " and coalesce(fl_ativo, 0) = ".$fl_ativo;
        }
        $disponiveis = \Endereco::get_qtdDisponivel(1);
        $clientes = \DB::select(\DB::raw($sql));
        $titulo = "Situação dos clientes inadimplentes";
        if ($saida == 'xls'){
            return View::make('gerencial.situacaocaixas_xls')->with('clientes', $clientes)->with('disponiveis', $disponiveis)->with('titulo', $titulo)->with('xls',true);
        } else {
            return View::make('gerencial.situacaocaixas')->with('clientes', $clientes)->with('disponiveis', $disponiveis)->with('titulo', $titulo);
        }
    }

    function getFranquia() {
        Input::flush();
        return $this->monitoramento_franquia();
    }

    function postFranquia(){
        Input::flash();
        if (Input::has('xls')){
            $conteudo =iconv("UTF8", "ISO-8859-1",  $this->monitoramento_franquia(Input::get('fl_ativo'), Input::get("search"), 'xls'));
            return Response::make($conteudo, '200', array(
                'Content-Type' => 'application/vnd.ms-excel',
                'Content-Disposition' => 'attachment; filename="monitoramento_franquia.xls"'
            ));

        } else {
            return $this->monitoramento_franquia(Input::get('fl_ativo'), Input::get("search"));
        }
    }
    function monitoramento_franquia($fl_ativo = 1, $search = '', $saida = ''){
        $sql = "select cliente.id_cliente, cliente.fl_ativo, razaosocial"
                . ", nomefantasia"
                . ", date_format(iniciocontrato, '%d/%m/%Y') iniciocontrato_fmt"
                . ", franquiacustodia"
                . ", nivel_inadimplencia"
                . ", cliente.obs"
                . " from cliente "
                . " where 1=1";
        $where  = array();
        if ($search != ''){
            $sql .= " and concat(cliente.id_cliente, razaosocial, nomefantasia) like '%".$search."%'";
        }
        if ($fl_ativo == '1' || $fl_ativo == '0'){
            $sql .= " and coalesce(fl_ativo, 0) = ".$fl_ativo;
        }
        $sql .= " order by iniciocontrato";
        $disponiveis = \Endereco::get_qtdDisponivel(1);
        $clientes = \DB::select(\DB::raw($sql));
        if ($saida == 'xls'){
            return View::make('gerencial.monitoramento_franquia_xls')
                        ->with('clientes', $clientes)
                        ->with('disponiveis', $disponiveis)
                        ->with('xls',true);
        } else {
            return View::make('gerencial.monitoramento_franquia')
                        ->with('clientes', $clientes)
                        ->with('disponiveis', $disponiveis)
                        ;
        }
    }

    public function getAtualizeestatisticas(){
        $rep = new Memodoc\Repositorios\RepositorioClienteEloquent;
        $rep->atualizeEstatisticas();
        \Session::flash('success','Estatísticas atualizadas com sucesso!');
        return Redirect::to('/gerencial/situacaocaixas');
    }

}
