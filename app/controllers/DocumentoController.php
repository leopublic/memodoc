<?php

class DocumentoController extends BaseController {

    function getCliente() {
        $clientes = Cliente::all();
        return View::make('documento_cliente')->with('clientes', $clientes);
    }

    function getNovo($cliente_id) {

        $cliente = Cliente::findOrFail($cliente_id);

        return View::make('documento_novo')
                        ->with('model', new Documento)
                        ->with('tipodocumento', FormSelect(TipoDocumento::All(), 'descricao'))
                        ->with('cliente', $cliente);
    }

    function postNovo($cliente_id) {

        Input::flash();
        $post = Input::All();

        if ($post['inicio'] <> '') {
            $post['inicio'] = DateTime::createFromFormat('d/m/Y', $post['inicio'])->format('Y-m-d');
        }
        if ($post['fim'] <> '') {
            $post['fim'] = DateTime::createFromFormat('d/m/Y', $post['fim'])->format('Y-m-d');
        }
        if ($post['expurgo_programado_data'] <> '') {
            $post['expurgo_programado_data'] = DateTime::createFromFormat('d/m/Y', $post['expurgo_programado_data'])->format('Y-m-d');
        }
        $post['itemalterado'] = 0;
        $post['id_usuario_alteracao'] = \Auth::user()->id_usuario;
        $post['evento'] = __CLASS__ . '.' . __FUNCTION__;
        // try {
        Documento::create($post);
        Session::flash('success', 'O documento foi cadastrado com sucesso ' . $post['titulo']);
        return Redirect::to('documento/novo/' . $cliente_id);
        // } catch (Exception $exc) {
        //Session::flash('error', $exc->getTraceAsString());
        //return Redirect::to('documento/novo/'.$cliente_id);
        //}
    }

    function getBuscar() {
        $id_cliente = Input::old('id_cliente', Session::get('id_cliente'));
        $cliente = \Cliente::find($id_cliente);

        if (is_object($cliente) && !$cliente->fl_ativo == 1) {
            Session::forget('id_cliente');
        }
        return $this->buscar('');
    }

    function getBuscarinativos() {
        $id_cliente = Input::old('id_cliente', Session::get('id_cliente'));
        $cliente = \Cliente::find($id_cliente);
        if (is_object($cliente) && $cliente->fl_ativo == 1) {
            Session::forget('id_cliente');
        }
        return $this->buscar('s');
    }

    function buscar($inativos = '') {
        $id_cliente = Input::old('id_cliente', Session::get('id_cliente'));
        $titulo = Input::old('titulo', Session::get('titulo'));
        $id_departamento = Input::old('id_departamento', Session::get('id_departamento'));
        $id_centro = Input::old('id_centro', Session::get('id_centro'));
        $id_setor = Input::old('id_setor', Session::get('id_setor'));
        $inicio = Input::old('inicio', Session::get('inicio'));
        $fim = Input::old('fim', Session::get('fim'));
        $primeira_entrada_inicio = Input::old('primeira_entrada_inicio', Session::get('primeira_entrada_inicio'));
        $primeira_entrada_fim = Input::old('primeira_entrada_fim', Session::get('primeira_entrada_fim'));
        $nume_inicial = Input::old('nume_inicial', Session::get('nume_inicial'));
        $nume_final = Input::old('nume_final', Session::get('nume_final'));
        $alfa_inicial = Input::old('alfa_inicial', Session::get('alfa_inicial'));
        $alfa_final = Input::old('alfa_final', Session::get('alfa_final'));
        $id_caixa = Input::old('id_caixa', Session::get('id_caixa'));
        $id_caixapadrao = Input::old('id_caixapadrao', Session::get('id_caixapadrao'));
        $id_caixapadrao_fim = Input::old('id_caixapadrao_fim', Session::get('id_caixapadrao_fim'));
        $referencia = Input::old('referencia', Session::get('referencia'));
        $status = Input::old('status', Session::get('status'));
        $reserva_inicio = Input::old('reserva_inicio', Session::get('reserva_inicio'));
        $reserva_fim = Input::old('reserva_fim', Session::get('reserva_fim'));
        $migradas = Input::old('migradas', Session::get('migradas'));
        $movimentacao = Input::old('movimentacao', Session::get('movimentacao'));
        $id_valor = Input::old('id_valor', Session::get('id_valor'));

        $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent;

        $cliente = Cliente::find($id_cliente);
        if ($id_cliente > 0){
            $propriedade = \Propriedade::where('id_cliente', '=', $id_cliente)->first();
            if (!is_object($propriedade)) {
                $valores = array();
                $propriedade = null;
            } else {
                $valores = array('' => '(todos)') + \Valor::where('id_propriedade', '=', $propriedade->id_propriedade)->orderBy('nome')->lists('nome', 'id_valor');
            }            
            $franquiacustodia = $cliente->franquiacustodia;
            if ($cliente->franquiacustodia > 0){
                $titulo_carencia = 'excedente de franquia (não enviadas mas cobradas por excederem a franquia)';
                $titulo_franquia = 'em franquia (não enviadas e não cobradas por estarem dentro da franquia)';
                $status_carencia = '';
                $status_franquia = '';
            } else {
                $status_carencia = 'disabled';
                $status_franquia = 'disabled';
                $titulo_carencia = 'em excedente de franquia - NÃO SE APLICA: CLIENTE SEM FRANQUIA';
                $titulo_franquia = 'em franquia - NÃO SE APLICA: CLIENTE SEM FRANQUIA';
            }
        } else {
            $franquiacustodia = 0;
            $propriedade = null;
            $valores = array();
            $titulo_carencia = 'excedente de franquia (não enviadas mas cobradas por excederem a franquia)';
            $titulo_franquia = 'em franquia (não enviadas e não cobradas por estarem dentro da franquia)';
            $status_carencia = '';
            $status_franquia = '';
        }
        
        $erro = false;
        if (is_object($cliente) && $cliente->nivel_inadimplencia == 2){
            Session::flash('errors', 'Não é possível fazer consultas a esse cliente pois ele foi bloqueado pela gerência por inadimplência ');
            $erro = true;
        } elseif($primeira_entrada_inicio != '') {
            if ($primeira_entrada_inicio != '' && $primeira_entrada_inicio != '__/__/____' && $primeira_entrada_inicio != 'de' && $primeira_entrada_inicio != 'até') {
                $data = Carbon::createFromFormat('d/m/Y', $primeira_entrada_inicio)->format('Y-m-d');
                if ($data < '2014-04-18'){
                    Session::flash('errors', 'Não é possível fazer consultas por data de primeira entrada anteriores a 18/04/2014, pois não podemos garantir essa informação.');
                    $erro = true;
                }
            }
        } 

        if (!$erro){
            $documentos  = Documento::documentosSelecionados($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim)
                ->with('reserva');
            $documentos = $documentos->paginate(20);
            $documentos = $rep->adicionaStatusCaixas($documentos, $franquiacustodia);
            $doc = new Documento;
            $qtdDocumentos = $doc->qtdDocumentosSelecionados($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim);
            $qtdCaixas         = $doc->qtdCaixasSelecionados($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim);
        } else {
            $documentos  = null;
            $qtdDocumentos = 0;
            $qtdCaixas = 0;
        }
        

        if ($inativos) {
            $clientes = FormSelect(Cliente::getInativos(), 'razaosocial');
            $titulo = 'Localizar documentos - INATIVOS';
        } else {
            $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
            $titulo = 'Localizar documentos';
        }

        $setores = array();
        $departamentos = array();
        $centrocustos = array();

        if (is_object($cliente)) {
            $setores = FormSelect($cliente->setor()->get());
            $departamentos = FormSelect($cliente->departamento()->get());
            $centrocustos = FormSelect($cliente->centrocusto()->get());
        }

        Session::reflash();

        return View::make('documento_buscar')
                        ->with('clientes', $clientes)
                        ->with('id_cliente', $id_cliente)
                        ->with('setores', $setores)
                        ->with('centrocustos', $centrocustos)
                        ->with('departamentos', $departamentos)
                        ->with('documentos', $documentos)
                        ->with('qtdDocumentos', $qtdDocumentos)
                        ->with('qtdCaixas', $qtdCaixas)
                        ->with('titulo', $titulo)
                        ->with('cliente', $cliente)
                        ->with('propriedade', $propriedade)
                        ->with('valores', $valores)
                        ->with('status_carencia', $status_carencia)
                        ->with('titulo_carencia', $titulo_carencia)
                        ->with('status_franquia', $status_franquia)
                        ->with('titulo_franquia', $titulo_franquia)
        ;
    }

    function postBuscar() {
        Input::flash();
        return Redirect::to('/documento/buscar')->withInput();
    }

    function postBuscarinativos() {
        Input::flash();
        return Redirect::to('/documento/buscarinativos')->withInput();
    }

    // Ajax - departamentos
    function getAjaxdepartamento($id_cliente) {
        $departamentos = DepCliente::where('id_cliente', '=', $id_cliente)->get();
        $select = FormSelect($departamentos, 'nome');
        echo View::make('padrao.select_option', compact('select'));
    }

    // Ajax - setores
    function getAjaxsetor($id_cliente) {
        $data = SetCliente::where('id_cliente', '=', $id_cliente)->get();
        $select = FormSelect($data, 'nome');
        echo View::make('padrao.select_option', compact('select'));
    }

    // Ajax - centro de custos
    function getAjaxcentrocusto($id_cliente) {
        $data = CentroDeCusto::where('id_cliente', '=', $id_cliente)->get();
        $select = FormSelect($data, 'nome');
        echo View::make('padrao.select_option', compact('select'));
    }

    public function getAcervoselecionado() {
        set_time_limit(0);
        $id_cliente = Input::old('id_cliente', Session::get('id_cliente'));
        $titulo = Input::old('titulo', Session::get('titulo'));
        $id_departamento = Input::old('id_departamento', Session::get('id_departamento'));
        $id_centro = Input::old('id_centro', Session::get('id_centro'));
        $id_setor = Input::old('id_setor', Session::get('id_setor'));
        $inicio = Input::old('inicio', Session::get('inicio'));
        $fim = Input::old('fim', Session::get('fim'));
        $primeira_entrada_inicio = Input::old('primeira_entrada_inicio', Session::get('primeira_entrada_inicio'));
        $primeira_entrada_fim = Input::old('primeira_entrada_fim', Session::get('primeira_entrada_fim'));
        $nume_inicial = Input::old('nume_inicial', Session::get('nume_inicial'));
        $nume_final = Input::old('nume_final', Session::get('nume_final'));
        $alfa_inicial = Input::old('alfa_inicial', Session::get('alfa_inicial'));
        $alfa_final = Input::old('alfa_final', Session::get('alfa_final'));
        $id_caixa = Input::old('id_caixa', Session::get('id_caixa'));
        $id_caixapadrao = Input::old('id_caixapadrao', Session::get('id_caixapadrao'));
        $id_caixapadrao_fim = Input::old('id_caixapadrao_fim', Session::get('id_caixapadrao_fim'));
        $referencia = Input::old('referencia', Session::get('referencia'));
        $status = Input::old('status', Session::get('status'));
        $reserva_inicio = Input::old('reserva_inicio', Session::get('reserva_inicio'));
        $reserva_fim = Input::old('reserva_fim', Session::get('reserva_fim'));
        $migradas = Input::old('migradas', Session::get('migradas'));
        $movimentacao = Input::old('movimentacao', Session::get('movimentacao'));
        $id_valor = Input::old('id_valor', Session::get('id_valor'));
        $ordenacao = Input::old("ordenarpor", Session::get('ordernarpor'));
        set_time_limit(0);
        session_write_close();

        // Imprime o cabeçalho
        $cliente = \Cliente::find($id_cliente);

        $filtro = '';
        $virg = '';

        if ($titulo != '') {
            $filtro .= $virg . ' contendo "' . $titulo . '"';
            $virg = ',';
        }

        if ($id_departamento != '') {
            $filtro .= $virg . ' do departamento ' . \DepCliente::find($id_departamento)->nome;
            $virg = ',';
        }

        if ($id_centro != '') {
            $filtro .= $virg . ' do centro de custos ' . \CentroDeCusto::find($id_centro)->nome;
            $virg = ',';
        }

        if ($status != 'todas') {
            if ($status == 'emconsulta') {
                $filtro .= $virg . ' somente caixas em consulta';
            } elseif ($status == 'reservadas') {
                $filtro .= $virg . ' somente caixas reservadas';
            }
            $virg = ',';
        }

        if ($inicio != '') {
            $filtro .= $virg . ' desde ' . $inicio;
            if ($fim != '') {
                $filtro .= ' até ' . $fim;
            }
            $virg = ',';
        } elseif ($fim != '') {
            $filtro .= $virg . ' até ' . $fim;
        }

        if ($primeira_entrada_inicio != '') {
            $filtro .= $virg . ' com primeira entrada desde ' . $primeira_entrada_inicio;
            if ($primeira_entrada_fim != '') {
                $filtro .= ' até ' . $primeira_entrada_fim;
            }
            $virg = ',';
        } elseif ($primeira_entrada_fim != '') {
            $filtro .= $virg . ' até ' . $primeira_entrada_fim;
        }

        if ($nume_inicial != '') {
            $filtro .= $virg . ' desde ' . $nume_inicial;
            if ($nume_final != '') {
                $filtro .= ' até ' . $nume_final . '"';
            }
            $virg = ',';
        } elseif ($nume_final != '') {
            $filtro .= $virg . ' até "' . $nume_final . '"';
        }

        if ($alfa_inicial != '') {
            $filtro .= $virg . ' desde ' . $alfa_inicial;
            if ($alfa_final != '') {
                $filtro .= ' até ' . $alfa_final . '"';
            }
            $virg = ',';
        } elseif ($alfa_final != '') {
            $filtro .= $virg . ' até "' . $alfa_final . '"';
        }

        if ($id_caixapadrao != '') {
            $filtro .= $virg . ' da(s) referência(s) ' . $id_caixapadrao;
            $virg = ',';
        }

        if ($id_caixapadrao_fim != '') {
            $filtro .= $virg . ' até a referência ' . $id_caixapadrao_fim;
            $virg = ',';
        }

        if ($id_caixa != '') {
            $filtro .= $virg . ' do ID ' . $id_caixa;
            $virg = ',';
        }

        if ($referencia != '') {
            $filtro .= $virg . ' da referência de migração "' . $referencia . '"';
            $virg = ',';
        }


        if ($reserva_inicio != '') {
            $filtro .= $virg . 'reservadas desde ' . $reserva_inicio;
            if ($reserva_fim != '') {
                $filtro .= " até " . $reserva_fim;
            }
            $virg = ',';
        } elseif ($reserva_fim != '') {
            $filtro .= $virg . 'reservadas até ' . $reserva_fim;
            $virg = ',';
        }

        if ($migradas != '' && $migradas != 'todas') {
            if ($migradas == 'sim') {
                $filtro .= $virg . "somente migradas";
            } else {
                $filtro .= $virg . "somente não migradas";
            }
            $virg = ',';
        }

        if ($movimentacao != '' && $movimentacao != 'todas') {
            if ($movimentacao == 'sim') {
                $filtro .= $virg . "já movimentadas";
            } else {
                $filtro .= $virg . "nunca movimentadas";
            }
            $virg = ',';
        }

        if ($id_valor != '' ) {
            $val = \Valor::find($id_valor);
            $filtro .= $virg . "somente da origem ".$val->nome;
            $virg = ',';
        }
        if (trim($filtro) != '') {
            $filtro = "Filtros aplicados: " . $filtro;
        }

        $docx = new \Documento;
        $qtdDocumentos = $docx->qtdDocumentosSelecionados($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim);
        $qtdCaixas = $docx->qtdCaixasSelecionados($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim);
        $html = View::make('documento.acervo.acervo_inicio')
                ->with('qtdDocumentos', number_format($qtdDocumentos, 0, ',', '.'))
                ->with('qtdCaixas', number_format($qtdCaixas, 0, ',', '.'))
                ->with('cliente', $cliente)
                ->with('filtros', $filtro)
        ;
        print $html;

        $id_caixapadraoant = '';
        $i = 0;
        $html = '';
        $arqs = 0;

        $documentos = Documento::filtrarPor($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim);
        $documentos->leftJoin('dep_cliente', 'dep_cliente.id_departamento', '=', 'documento.id_departamento')
                ->leftJoin('centrodecusto', 'centrodecusto.id_centrodecusto', '=', 'documento.id_centro')
                ->leftJoin('set_cliente', 'set_cliente.id_setor', '=', 'documento.id_setor')
                ->select(array(\DB::raw('centrodecusto.nome nome_centrodecusto')
                    , 'documento.id_centro'
                    , 'documento.id_departamento'
                    , \DB::raw('dep_cliente.nome as nome_depcliente')
                    , \DB::raw('set_cliente.nome as nome_setcliente')
                    , 'documento.id_caixapadrao'
                    , \DB::raw("substr(concat('000',documento.id_item), -3) id_item_fmt")
                    , 'documento.titulo'
                    , 'documento.conteudo'
                    , \DB::raw("date_format(inicio, '%d/%m/%Y') inicio_fmt ")
                    , \DB::raw("date_format(fim, '%d/%m/%Y') fim_fmt ")
                    , 'expurgo_programado'
                    , \DB::raw("date_format(expurgarem, '%d/%m/%Y') expurgarem_fmt")
                    , \DB::raw("date_format(primeira_entrada, '%d/%m/%Y') primeira_entrada_fmt")
                    , 'migrada_de'
                    , 'request_recall'
                ));
        $ordens = explode(",", $ordenacao);
        foreach ($ordens as $ordem){
            $documentos->orderBy($ordem);
        }
        $qtd = 0;
        $documentos->chunk(200, function ($documentos) use (&$id_caixapadraoant, &$i, &$qtd ) {
                    foreach ($documentos as $doc) {
                        if ($id_caixapadraoant != $doc->id_caixapadrao) {
                            $qtd++;
                            $html = View::make('documento.acervo.acervo_caixa')->with('doc', $doc)->with('qtd', $qtd);
                            $id_caixapadraoant = $doc->id_caixapadrao;
                            print $html;
                        }
                        $html = View::make('documento.acervo.acervo_item')->with('doc', $doc);
                        print $html;
                        $i++;
                    }
                });

        $html = View::make('documento.acervo_fim');
        print $html;
    }

    public function getAcervoselecionadoxls() {
        $id_cliente = Input::old('id_cliente', Session::get('id_cliente'));
        $titulo = Input::old('titulo', Session::get('titulo'));
        $id_departamento = Input::old('id_departamento', Session::get('id_departamento'));
        $id_centro = Input::old('id_centro', Session::get('id_centro'));
        $id_setor = Input::old('id_setor', Session::get('id_setor'));
        $inicio = Input::old('inicio', Session::get('inicio'));
        $fim = Input::old('fim', Session::get('fim'));
        $primeira_entrada_inicio = Input::old('primeira_entrada_inicio', Session::get('primeira_entrada_inicio'));
        $primeira_entrada_fim = Input::old('primeira_entrada_fim', Session::get('primeira_entrada_fim'));
        $nume_inicial = Input::old('nume_inicial', Session::get('nume_inicial'));
        $nume_final = Input::old('nume_final', Session::get('nume_final'));
        $alfa_inicial = Input::old('alfa_inicial', Session::get('alfa_inicial'));
        $alfa_final = Input::old('alfa_final', Session::get('alfa_final'));
        $id_caixa = Input::old('id_caixa', Session::get('id_caixa'));
        $id_caixapadrao = Input::old('id_caixapadrao', Session::get('id_caixapadrao'));
        $id_caixapadrao_fim = Input::old('id_caixapadrao_fim', Session::get('id_caixapadrao_fim'));
        $referencia = Input::old('referencia', Session::get('referencia'));
        $status = Input::old('status', Session::get('status'));
        $reserva_inicio = Input::old('reserva_inicio', Session::get('reserva_inicio'));
        $reserva_fim = Input::old('reserva_fim', Session::get('reserva_fim'));
        $migradas = Input::old('migradas', Session::get('migradas'));
        $movimentacao = Input::old('movimentacao', Session::get('movimentacao'));
        $id_valor = Input::old('id_valor', Session::get('id_valor'));
        $ordenacao = Input::old("ordenarpor", Session::get('ordernarpor'));
        set_time_limit(0);
        session_write_close();

        // Imprime o cabeçalho
        $cliente = \Cliente::find($id_cliente);

        $filtro = '';
        $virg = '';

        if ($titulo != '') {
            $filtro .= $virg . ' contendo "' . $titulo . '"';
            $virg = ',';
        }

        if ($id_departamento != '') {
            $filtro .= $virg . ' do departamento ' . \DepCliente::find($id_departamento)->nome;
            $virg = ',';
        }

        if ($id_centro != '') {
            $filtro .= $virg . ' do centro de custos ' . \CentroDeCusto::find($id_centro)->nome;
            $virg = ',';
        }

        if ($status != 'todas') {
            if ($status == 'emconsulta') {
                $filtro .= $virg . ' somente caixas em consulta';
            } elseif ($status == 'reservadas') {
                $filtro .= $virg . ' somente caixas reservadas';
            }
            $virg = ',';
        }

        if ($inicio != '') {
            $filtro .= $virg . ' desde ' . $inicio;
            if ($fim != '') {
                $filtro .= ' até ' . $fim;
            }
            $virg = ',';
        } elseif ($fim != '') {
            $filtro .= $virg . ' até ' . $fim;
        }

        if ($primeira_entrada_inicio != '') {
            $filtro .= $virg . ' com primeira entrada desde ' . $primeira_entrada_inicio;
            if ($primeira_entrada_fim != '') {
                $filtro .= ' até ' . $primeira_entrada_fim;
            }
            $virg = ',';
        } elseif ($primeira_entrada_fim != '') {
            $filtro .= $virg . ' até ' . $primeira_entrada_fim;
        }

        if ($nume_inicial != '') {
            $filtro .= $virg . ' desde ' . $nume_inicial;
            if ($nume_final != '') {
                $filtro .= ' até ' . $nume_final . '"';
            }
            $virg = ',';
        } elseif ($nume_final != '') {
            $filtro .= $virg . ' até "' . $nume_final . '"';
        }

        if ($alfa_inicial != '') {
            $filtro .= $virg . ' desde ' . $alfa_inicial;
            if ($alfa_final != '') {
                $filtro .= ' até ' . $alfa_final . '"';
            }
            $virg = ',';
        } elseif ($alfa_final != '') {
            $filtro .= $virg . ' até "' . $alfa_final . '"';
        }

        if ($id_caixapadrao != '') {
            $filtro .= $virg . ' da(s) referência(s) ' . $id_caixapadrao;
            $virg = ',';
        }

        if ($id_caixapadrao_fim != '') {
            $filtro .= $virg . ' até a referência ' . $id_caixapadrao_fim;
            $virg = ',';
        }

        if ($id_caixa != '') {
            $filtro .= $virg . ' do ID ' . $id_caixa;
            $virg = ',';
        }

        if ($referencia != '') {
            $filtro .= $virg . ' da referência de migração "' . $referencia . '"';
            $virg = ',';
        }


        if ($reserva_inicio != '') {
            $filtro .= $virg . 'reservadas desde ' . $reserva_inicio;
            if ($reserva_fim != '') {
                $filtro .= " até " . $reserva_fim;
            }
            $virg = ',';
        } elseif ($reserva_fim != '') {
            $filtro .= $virg . 'reservadas até ' . $reserva_fim;
            $virg = ',';
        }

        if ($migradas != '' && $migradas != 'todas') {
            if ($migradas == 'sim') {
                $filtro .= $virg . "somente migradas";
            } else {
                $filtro .= $virg . "somente não migradas";
            }
            $virg = ',';
        }

        if ($movimentacao != '' && $movimentacao != 'todas') {
            if ($movimentacao == 'sim') {
                $filtro .= $virg . "já movimentadas";
            } else {
                $filtro .= $virg . "nunca movimentadas";
            }
            $virg = ',';
        }

        if ($id_valor != '' ) {
            $val = \Valor::find($id_valor);
            $filtro .= $virg . "somente da origem ".$val->nome;
            $virg = ',';
        }
        if (trim($filtro) != '') {
            $filtro = "Filtros aplicados: " . $filtro;
        }

        $dir = base_path() . '/arquivos/acervo' . '/';
        if (!file_exists($dir)) {
            mkdir($dir, 755, true);
        }

        if (file_exists($dir . $id_cliente)) {
            unlink($dir . $id_cliente);
        }

        $fp = fopen($dir . $id_cliente, 'a+');


        $docx = new \Documento;
        $qtdDocumentos = $docx->qtdDocumentosSelecionados($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim);
        $qtdCaixas = $docx->qtdCaixasSelecionados($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim);
        $html = View::make('documento.acervo.acervo_inicio_xls')
                ->with('qtdDocumentos', number_format($qtdDocumentos, 0, ',', '.'))
                ->with('qtdCaixas', number_format($qtdCaixas, 0, ',', '.'))
                ->with('cliente', $cliente)
                ->with('filtros', $filtro)
        ;
        fwrite($fp, utf8_decode($html));

        $id_caixapadraoant = '';
        $i = 0;
        $html = '';
        $arqs = 0;

        $documentos = Documento::filtrarPor($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $migradas, $movimentacao, $id_valor, $primeira_entrada_inicio, $primeira_entrada_fim);
        $documentos->leftJoin('dep_cliente', 'dep_cliente.id_departamento', '=', 'documento.id_departamento')
                ->leftJoin('centrodecusto', 'centrodecusto.id_centrodecusto', '=', 'documento.id_centro')
                ->leftJoin('set_cliente', 'set_cliente.id_setor', '=', 'documento.id_setor')
                ->select(array(\DB::raw('centrodecusto.nome nome_centrodecusto')
                    , 'documento.id_centro'
                    , 'documento.id_departamento'
                    , 'documento.id_setor'
                    , \DB::raw('dep_cliente.nome as nome_depcliente')
                    , \DB::raw('set_cliente.nome as nome_setcliente')
                    , 'documento.id_caixapadrao'
                    , \DB::raw("substr(concat('000',documento.id_item), -3) id_item_fmt")
                    , 'documento.titulo'
                    , 'documento.conteudo'
                    , \DB::raw("date_format(inicio, '%d/%m/%Y') inicio_fmt ")
                    , \DB::raw("date_format(fim, '%d/%m/%Y') fim_fmt ")
                    , 'expurgo_programado'
                    , \DB::raw("date_format(expurgarem, '%d/%m/%Y') expurgarem_fmt")
                    , \DB::raw("date_format(primeira_entrada, '%d/%m/%Y') primeira_entrada_fmt")
                    , 'migrada_de'
                    , 'request_recall'
                ));
        $ordens = explode(",", $ordenacao);
        foreach ($ordens as $ordem){
            $documentos->orderBy($ordem);
        }
        $qtd = 0;
        if ($ordenacao == "id_caixapadrao,id_item"){
            $exibe_numero_caixa = true;
        } else {
            $exibe_numero_caixa = false;
        }
        $documentos->chunk(200, function ($documentos) use (&$id_caixapadraoant, &$i, &$fp, &$qtd, $exibe_numero_caixa ) {
                    foreach ($documentos as $doc) {

                        if ($id_caixapadraoant != $doc->id_caixapadrao) {
                            $qtd++;
                            $html = View::make('documento.acervo.acervo_caixa_xls')
                                    ->with('doc', $doc)
                                    ->with('qtd', $qtd)
                                    ->with('exibe_numero_caixa', $exibe_numero_caixa);
                            $id_caixapadraoant = $doc->id_caixapadrao;
                            //print $html;
                            fwrite($fp, utf8_decode($html));
                        }
                        $html = View::make('documento.acervo.acervo_item_xls')
                                ->with('doc', $doc);
                        //print $html;
                        fwrite($fp, utf8_decode($html));
                        $i++;
                    }
                });

        $html = View::make('documento.acervo_fim_xls');
        //print $html;
        fwrite($fp, utf8_decode($html));
        fclose($fp);
        //print $html;
        $headers = array(
            'Content-type' => 'application/vnd.ms-excel'
        );
        $filename = $cliente->razaosocial."_".$cliente->nomefantasia;
        $filename = str_replace(" ", "_", $filename);
        $filename = preg_replace("/[^A-Za-z0-9_-]/", "", $filename);
        $filename = preg_replace("/_+/", "_", $filename);
        //make sure that the function doesn't return an empty result
        if (strlen($filename) < 3) {
            $filename = "acervo_" . time();
        }
        return Response::download($dir . $id_cliente, $filename . '.xls', $headers);
    }

    public function getAcervoselecionadoporsetor() {
        $id_cliente = Input::old('id_cliente', Session::get('id_cliente'));
        $titulo = Input::old('titulo', Session::get('titulo'));
        $id_departamento = Input::old('id_departamento', Session::get('id_departamento'));
        $id_centro = Input::old('id_centro', Session::get('id_centro'));
        $id_setor = Input::old('id_setor', Session::get('id_setor'));
        $inicio = Input::old('inicio', Session::get('inicio'));
        $fim = Input::old('fim', Session::get('fim'));
        $primeira_entrada_inicio = Input::old('primeira_entrada_inicio', Session::get('primeira_entrada_inicio'));
        $primeira_entrada_fim = Input::old('primeira_entrada_fim', Session::get('primeira_entrada_fim'));
        $nume_inicial = Input::old('nume_inicial', Session::get('nume_inicial'));
        $nume_final = Input::old('nume_final', Session::get('nume_final'));
        $alfa_inicial = Input::old('alfa_inicial', Session::get('alfa_inicial'));
        $alfa_final = Input::old('alfa_final', Session::get('alfa_final'));
        $id_caixa = Input::old('id_caixa', Session::get('id_caixa'));
        $id_caixapadrao = Input::old('id_caixapadrao', Session::get('id_caixapadrao'));
        $id_caixapadrao_fim = Input::old('id_caixapadrao_fim', Session::get('id_caixapadrao_fim'));
        $referencia = Input::old('referencia', Session::get('referencia'));
        $status = Input::old('status', Session::get('status'));
        $reserva_inicio = Input::old('reserva_inicio', Session::get('reserva_inicio'));
        $reserva_fim = Input::old('reserva_fim', Session::get('reserva_fim'));
        set_time_limit(0);
        session_write_close();

        // Imprime o cabeçalho
        $cliente = \Cliente::find($id_cliente);

        $filtro = '';
        $virg = '';

        if ($titulo != '') {
            $filtro .= $virg . ' contendo "' . $titulo . '"';
            $virg = ',';
        }

        if ($id_departamento != '') {
            $filtro .= $virg . ' do departamento ' . \DepCliente::find($id_departamento)->nome;
            $virg = ',';
        }

        if ($id_centro != '') {
            $filtro .= $virg . ' do centro de custos ' . \CentroDeCusto::find($id_centro)->nome;
            $virg = ',';
        }

        if ($status != 'todas') {
            if ($status == 'emconsulta') {
                $filtro .= $virg . ' somente caixas em consulta';
            } elseif ($status == 'reservadas') {
                $filtro .= $virg . ' somente caixas reservadas';
            }
            $virg = ',';
        }

        if ($inicio != '') {
            $filtro .= $virg . ' desde ' . $inicio;
            if ($fim != '') {
                $filtro .= ' até ' . $fim;
            }
            $virg = ',';
        } elseif ($fim != '') {
            $filtro .= $virg . ' até ' . $fim;
        }

        if ($primeira_entrada_inicio != '') {
            $filtro .= $virg . ' com primeira entrada desde ' . $primeira_entrada_inicio;
            if ($primeira_entrada_fim != '') {
                $filtro .= ' até ' . $primeira_entrada_fim;
            }
            $virg = ',';
        } elseif ($primeira_entrada_fim != '') {
            $filtro .= $virg . ' até ' . $primeira_entrada_fim;
        }

        if ($nume_inicial != '') {
            $filtro .= $virg . ' desde ' . $nume_inicial;
            if ($nume_final != '') {
                $filtro .= ' até ' . $nume_final . '"';
            }
            $virg = ',';
        } elseif ($nume_final != '') {
            $filtro .= $virg . ' até "' . $nume_final . '"';
        }

        if ($alfa_inicial != '') {
            $filtro .= $virg . ' desde ' . $alfa_inicial;
            if ($alfa_final != '') {
                $filtro .= ' até ' . $alfa_final . '"';
            }
            $virg = ',';
        } elseif ($alfa_final != '') {
            $filtro .= $virg . ' até "' . $alfa_final . '"';
        }

        if ($id_caixapadrao != '') {
            $filtro .= $virg . ' da(s) referência(s) ' . $id_caixapadrao;
            $virg = ',';
        }

        if ($id_caixapadrao_fim != '') {
            $filtro .= $virg . ' até a referência ' . $id_caixapadrao_fim;
            $virg = ',';
        }

        if ($id_caixa != '') {
            $filtro .= $virg . ' do ID ' . $id_caixa;
            $virg = ',';
        }

        if ($referencia != '') {
            $filtro .= $virg . ' da referência de migração "' . $referencia . '"';
            $virg = ',';
        }


        if ($reserva_inicio != '') {
            $filtro .= $virg . 'reservadas desde ' . $reserva_inicio;
            if ($reserva_fim != '') {
                $filtro .= " até " . $reserva_fim;
            }
            $virg = ',';
        } elseif ($reserva_fim != '') {
            $filtro .= $virg . 'reservadas até ' . $reserva_fim;
            $virg = ',';
        }

        if (trim($filtro) != '') {
            $filtro = "Filtros aplicados: " . $filtro;
        }

        $docx = new \Documento;
        $qtdDocumentos = $docx->qtdDocumentosSelecionados($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $primeira_entrada_inicio, $primeira_entrada_fim);
        $qtdCaixas = $docx->qtdCaixasSelecionados($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $primeira_entrada_inicio, $primeira_entrada_fim);
        $html = View::make('documento.acervo_inicio')
                ->with('qtdDocumentos', number_format($qtdDocumentos, 0, ',', '.'))
                ->with('qtdCaixas', number_format($qtdCaixas, 0, ',', '.'))
                ->with('cliente', $cliente)
                ->with('filtros', $filtro)
        ;
        print $html;

        $id_centroant = '';
        $id_depclienteant = '';
        $id_caixapadraoant = '';
        $i = 0;
        $html = '';
        $arqs = 0;

        $documentos = Documento::filtrarPor($id_cliente, $titulo, $id_setor, $id_centro, $id_departamento, $inicio, $fim, $nume_inicial, $nume_final, $alfa_inicial, $alfa_final, $id_caixa, $id_caixapadrao, $referencia, $status, $reserva_inicio, $reserva_fim, $id_caixapadrao_fim, $primeira_entrada_inicio, $primeira_entrada_fim);
        $documentos->leftJoin('dep_cliente', 'dep_cliente.id_departamento', '=', 'documento.id_departamento')
                ->leftJoin('centrodecusto', 'centrodecusto.id_centrodecusto', '=', 'documento.id_centro')
                ->select(array(\DB::raw('centrodecusto.nome nome_centrodecusto')
                    , 'documento.id_centro'
                    , 'documento.id_departamento'
                    , \DB::raw('dep_cliente.nome as nome_depcliente')
                    , 'documento.id_caixapadrao'
                    , \DB::raw("substr(concat('000',documento.id_item), -3) id_item_fmt")
                    , 'documento.titulo'
                    , 'documento.conteudo'
                    , \DB::raw("date_format(inicio, '%d/%m/%Y') inicio_fmt ")
                    , \DB::raw("date_format(fim, '%d/%m/%Y') fim_fmt ")
                    , 'expurgo_programado'
                    , \DB::raw("date_format(expurgarem, '%d/%m/%Y') expurgarem_fmt")
                    , \DB::raw("date_format(primeira_entrada, '%d/%m/%Y') primeira_entrada_fmt")
                    , 'migrada_de'
                    , 'request_recall'
                ))
                ->orderBy('centrodecusto.nome')
                ->orderBy('dep_cliente.nome')
                ->orderBy('documento.id_caixapadrao')
                ->orderBy('documento.id_item')
                ->chunk(200, function ($documentos) use (&$id_centroant, &$id_depclienteant, &$id_caixapadraoant, &$i ) {
                    foreach ($documentos as $doc) {
                        if ($id_centroant != $doc->id_centro || $id_depclienteant != $doc->id_departamento || $i == 0) {
                            $html = View::make('documento.acervo_centro_dep')->with('doc', $doc);
                            print $html;
                            $id_centroant = $doc->id_centro;
                            $id_depclienteant = $doc->id_departamento;
                        }
                        if ($id_caixapadraoant != $doc->id_caixapadrao) {
                            $html = View::make('documento.acervo_caixa')->with('doc', $doc);
                            $id_caixapadraoant = $doc->id_caixapadrao;
                            print $html;
                        }
                        $html = View::make('documento.acervo_item')->with('doc', $doc);
                        print $html;
                        $i++;
                    }
                });

        $html = View::make('documento.acervo_fim');
        print $html;
    }

    public function filtroTexto($descricao, $valor, &$virg) {
        $texto = '';
        if ($valor != '') {
            $texto .= $virg . ' ' . $descricao . '"' . $valor . '"';
            $virg = ',';
        }
        return $texto;
    }

    public function getAcervocliente($formato = '') {
        if ($formato == 'xls'){
            $caminho = '/documento/acervoxls/';
            $titulo = 'Gerar cópia de acervo de cliente em EXCEL';
        }elseif($formato == 'pdf'){
            $caminho = '/documento/acervopdf/';
            $titulo = 'Gerar cópia de acervo de cliente em PDF';
        }else{
            $titulo = 'Gerar cópia de acervo de cliente para impressão';
            $caminho = '/documento/acervo/';
        }
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        return View::make('documento.acervo_cliente')
                        ->with('caminho', $caminho)
                        ->with('titulo', $titulo)
                        ->with('clientes', $clientes)
        ;
    }

    public function getAcervo($id_cliente) {
        set_time_limit(0);
        session_write_close();
        $cliente = \Cliente::find($id_cliente);
        $sql = "select c.nome nome_centrodecusto"
                . ", d.id_centro"
                . ", d.id_departamento"
                . ", dep.nome nome_depcliente"
                . ", d.id_caixapadrao"
                . ", substr(concat('000',d.id_item), -3) id_item_fmt"
                . ", d.titulo, d.conteudo"
                . ", date_format(inicio, '%d/%m/%Y') inicio_fmt "
                . ", date_format(fim, '%d/%m/%Y') fim_fmt "
                . ", expurgo_programado "
                . ", date_format(expurgarem, '%d/%m/%Y') expurgarem_fmt"
                . ", date_format(primeira_entrada, '%d/%m/%Y') primeira_entrada_fmt"
                . ", migrada_de "
                . ", request_recall "
                . " from documento d"
                . " left join dep_cliente dep on dep.id_departamento = d.id_departamento"
                . " left join centrodecusto c on c.id_centrodecusto = d.id_centro"
                . " where d.id_cliente = " . $id_cliente;
        $sql .= " order by c.nome, dep.nome, d.id_caixapadrao, d.id_item ";
//        $documentos = \DB::select(\DB::raw($sql));

        $docx = new \Documento;
        $qtdDocumentos = $docx->qtdDocumentosSelecionados($id_cliente, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        $qtdCaixas = $docx->qtdCaixasSelecionados($id_cliente, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        $html = View::make('documento.acervo_inicio')
                ->with('qtdDocumentos', number_format($qtdDocumentos, 0, ',', '.'))
                ->with('qtdCaixas', number_format($qtdCaixas, 0, ',', '.'))
                ->with('cliente', $cliente)
                ->with('filtros', '')
        ;
        print $html;

        $id_centroant = '';
        $id_depclienteant = '';
        $id_caixapadraoant = '';
        $i = 0;
        $html = '';
        $arqs = 0;
        $res = \DB::getPdo()->query($sql);
        while ($doc = $res->fetch(PDO::FETCH_OBJ)) {
            if ($id_centroant != $doc->id_centro || $id_depclienteant != $doc->id_departamento || $i == 0) {
                $html = View::make('documento.acervo_centro_dep')->with('doc', $doc);
                print $html;
                $id_centroant = $doc->id_centro;
                $id_depclienteant = $doc->id_departamento;
            }
            if ($id_caixapadraoant != $doc->id_caixapadrao) {
                $html = View::make('documento.acervo_caixa')->with('doc', $doc);
                $id_caixapadraoant = $doc->id_caixapadrao;
                print $html;
            }
            $html = View::make('documento.acervo_item')->with('doc', $doc);
            print $html;
            $i++;
        }

        $html = View::make('documento.acervo_fim');
        print $html;
    }

    public function getAcervopdf($id_cliente) {
        set_time_limit(0);
        session_write_close();
        $cliente = \Cliente::find($id_cliente);
        $sql = "select c.nome nome_centrodecusto"
                . ", d.id_centro"
                . ", d.id_departamento"
                . ", dep.nome nome_depcliente"
                . ", d.id_caixapadrao"
                . ", substr(concat('000',d.id_item), -3) id_item_fmt"
                . ", d.titulo, d.conteudo"
                . ", date_format(inicio, '%d/%m/%Y') inicio_fmt "
                . ", date_format(fim, '%d/%m/%Y') fim_fmt "
                . ", expurgo_programado "
                . ", date_format(expurgarem, '%d/%m/%Y') expurgarem_fmt"
                . ", date_format(primeira_entrada, '%d/%m/%Y') primeira_entrada_fmt"
                . ", migrada_de "
                . ", request_recall "
                . " from documento d"
                . " left join dep_cliente dep on dep.id_departamento = d.id_departamento"
                . " left join centrodecusto c on c.id_centrodecusto = d.id_centro"
                . " where d.id_cliente = " . $id_cliente;
        $sql .= " order by c.nome, dep.nome, d.id_caixapadrao, d.id_item ";
//        $documentos = \DB::select(\DB::raw($sql));

        $docx = new \Documento;
        $qtdDocumentos = $docx->qtdDocumentosSelecionados($id_cliente, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        $qtdCaixas = $docx->qtdCaixasSelecionados($id_cliente, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        $html = View::make('documento.acervo_inicio')
                ->with('qtdDocumentos', number_format($qtdDocumentos, 0, ',', '.'))
                ->with('qtdCaixas', number_format($qtdCaixas, 0, ',', '.'))
                ->with('cliente', $cliente)
                ->with('filtros', '')
        ;

        $this->mpdf = new mPDF('utf-8', 'A4-L', '11px', 'Arial', '10', '10', '30', '20', '10', '10');  // largura x altura
        $this->mpdf->allow_charset_conversion = false;
        $this->mpdf->packTableData =  true;
        $this->mpdf->SetHTMLHeader($html);


        // print $html;

        $id_centroant = '';
        $id_depclienteant = '';
        $id_caixapadraoant = '';
        $i = 0;
        $html = '';
        $arqs = 0;
        $res = \DB::getPdo()->query($sql);
        while ($doc = $res->fetch(PDO::FETCH_OBJ)) {
            if ($id_centroant != $doc->id_centro || $id_depclienteant != $doc->id_departamento || $i == 0) {
                $html = View::make('documento.acervo_centro_dep')->with('doc', $doc);
                // print $html;
                $this->mpdf->WriteHTML($html);
                $id_centroant = $doc->id_centro;
                $id_depclienteant = $doc->id_departamento;
            }
            if ($id_caixapadraoant != $doc->id_caixapadrao) {
                $html = View::make('documento.acervo_caixa')->with('doc', $doc);
                $id_caixapadraoant = $doc->id_caixapadrao;
                $this->mpdf->WriteHTML($html);
                // print $html;
            }
            $html = View::make('documento.acervo_item')->with('doc', $doc);
            $this->mpdf->WriteHTML($html);
            // print $html;
            $i++;
        }

        $html = View::make('documento.acervo_fim');
        // print $html;
        $this->mpdf->WriteHTML($html);
        $this->mpdf->Output();
    }

    public function getAcervoxls($id_cliente) {
        set_time_limit(0);
        session_write_close();

        $dir = base_path() . '/arquivos/acervo' . '/';
        if (!file_exists($dir)) {
            mkdir($dir, 755, true);
        }
        if (file_exists($dir . $id_cliente)) {
            unlink($dir . $id_cliente);
        }

        $fp = fopen($dir . $id_cliente, 'a+');

        $cliente = \Cliente::find($id_cliente);
        $sql = "select c.nome nome_centrodecusto"
                . ", d.id_centro"
                . ", d.id_departamento"
                . ", d.id_setor"
                . ", d.id_caixapadrao"
                . ", d.titulo, d.conteudo"
                . ", dep.nome nome_depcliente"
                . ", substr(concat('000',d.id_item), -3) id_item_fmt"
                . ", date_format(inicio, '%d/%m/%Y') inicio_fmt "
                . ", date_format(fim, '%d/%m/%Y') fim_fmt "
                . ", expurgo_programado "
                . ", date_format(expurgarem, '%d/%m/%Y') expurgarem_fmt"
                . ", date_format(primeira_entrada, '%d/%m/%Y') primeira_entrada_fmt"
                . ", migrada_de "
                . ", request_recall "
                . ", s.nome nome_setor "
                . " from documento d"
                . " left join dep_cliente dep on dep.id_departamento = d.id_departamento"
                . " left join centrodecusto c on c.id_centrodecusto = d.id_centro"
                . " left join set_cliente s on s.id_setor = d.id_setor"
                . " where d.id_cliente = " . $id_cliente;
        $sql .= " order by c.nome, dep.nome, d.id_caixapadrao, d.id_item ";
//        $documentos = \DB::select(\DB::raw($sql));

        $docx = new \Documento;
        $qtdDocumentos = $docx->qtdDocumentosSelecionados($id_cliente, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        $qtdCaixas = $docx->qtdCaixasSelecionados($id_cliente, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

        $html = View::make('documento.acervo_inicio_xls')
                ->with('qtdDocumentos', number_format($qtdDocumentos, 0, ',', '.'))
                ->with('qtdCaixas', number_format($qtdCaixas, 0, ',', '.'))
                ->with('cliente', $cliente)
                ->with('filtros', '')
        ;
//        print $html;
        fwrite($fp, utf8_decode($html));

        $id_centroant = '';
        $id_depclienteant = '';
        $id_caixapadraoant = '';
        $i = 0;
        //      $html = '';
        $arqs = 0;
        $res = \DB::getPdo()->query($sql);
        while ($doc = $res->fetch(PDO::FETCH_OBJ)) {
//            if ($id_centroant != $doc->id_centro || $id_depclienteant != $doc->id_departamento || $i == 0) {
//                $html = View::make('documento.acervo_centro_dep_xls')->with('doc', $doc);
////                print $html;
//                fwrite($fp, utf8_decode($html));
//                $id_centroant = $doc->id_centro;
//                $id_depclienteant = $doc->id_departamento;
//            }
//            if ($id_caixapadraoant != $doc->id_caixapadrao) {
//                $html = View::make('documento.acervo_caixa_xls')->with('doc', $doc);
//                $id_caixapadraoant = $doc->id_caixapadrao;
////                print $html;
////                File::append($dir . $id_cliente, $html);
//                fwrite($fp, utf8_decode($html));
//            }
            $html = View::make('documento.acervo_item_xls')->with('doc', $doc);
            //          print $html;
            //          File::append($dir . $id_cliente, $html);
            fwrite($fp, utf8_decode($html));
            $i++;
        }

        $html = View::make('documento.acervo_fim_xls');
//        File::append($dir . $id_cliente, $html);
        fwrite($fp, utf8_decode($html));
        fclose($fp);
        //print $html;
        $headers = array(
            'Content-type' => 'application/vnd.ms-excel'
        );
        $filename = $cliente->razaosocial."_".$cliente->nomefantasia;
        $filename = str_replace(" ", "_", $filename);
        $filename = preg_replace("/[^A-Za-z0-9_-]/", "", $filename);
        $filename = preg_replace("/_+/", "_", $filename);
        //make sure that the function doesn't return an empty result
        if (strlen($filename) < 3) {
            $filename = "acervo_" . time();
        }
        return Response::download($dir . $id_cliente, $filename . '.xls', $headers);
    }

    /**
     * Transform model in adapter Form::select
     * @link http://laravel.com/docs/html#drop-down-lists DropDown-lists
     * @param type $model
     * @param type $name
     * @return type array
     */
    function formselect($model, $name = 'nome') {
        $data = array();
        if ($model->count() > 0) {
            $data[''] = 'Selecione';
            foreach ($model as $fields) {
                foreach ($fields->getAttributes() as $fieldname => $field) {
                    if ($fieldname == $name) {
                        $data[$fields->getKey()] = $field;
                    }
                }
            }
        }
        return $data;
    }

    public function getImporte($pNomeTabela) {
        switch ($pNomeTabela) {
            case "cliente":
                Cliente::Recarregue();
                break;

            case "nivel":
                Nivel::Recarregue();
                break;

            case "usuario":
                Usuario::Recarregue();
                break;
        }
    }

    function array_rename(&$array, $name, $name_update) {
        foreach ($array as $k => $v) {
            $array[$k] [$name_update] = $array[$k] [$name];
            unset($array[$k][$name]);
        }
    }

    function getCadastrar($id_cliente = '', $id_caixapadrao = '', $id_item = '') {

        if ($id_cliente > 0) {
            $cliente = Cliente::findOrFail($id_cliente);

            if ($id_caixapadrao > 0) {
                $documentos = Documento::where('id_cliente', '=', $id_cliente)
                        ->where('id_caixapadrao', '=', $id_caixapadrao)
                        ->get()
                ;
                if ($id_item > 0) {
                    $documento = Documento::where('id_cliente', '=', $id_cliente)
                            ->where('id_caixapadrao', '=', $id_caixapadrao)
                            ->where('id_item', '=', $id_item)
                            ->first()
                    ;
                } else {
                    $documento = new Documento;
                }
            } else {
                $documentos = null;
                $documento = new Documento;
            }
        } else {
            $documentos = null;
            $documento = new Documento;
            $cliente = null;
        }
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        $metodo = Session::get('metodo');
        return View::make('documento_cadastrar')
                        ->with('documento', $documento)
                        ->with('documentos', $documentos)
                        ->with('id_cliente', $id_cliente)
                        ->with('clientes', $clientes)
                        ->with('id_caixapadrao', $id_caixapadrao)
                        ->with('id_item', $id_item)
                        ->with('metodo', $metodo)
                        ->with('cliente', $cliente)
        ;
    }

    public function getExclua($id_cliente, $id_caixapadrao, $id_documento) {
        try {
            $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent;
            $doc = \Documento::find($id_documento);
            $rep->exclua($doc, \Auth::user()->id_usuario, "Item excluído pelo inventário");
            Session::flash('success', 'Item excluído com sucesso');
        } catch (Exception $exc) {
            Session::flash('errors', $exc->getMessage());
        }
        return Redirect::to('documento/cadastrar/' . $id_cliente . '/' . $id_caixapadrao);
    }

    function postCarregacaixa() {
        Input::flash();
        if (Input::has('adicionar')) {
            $itens_em_branco = \Documento::where('id_cliente', '=', Input::get('id_cliente'))
                        ->where('id_caixapadrao', '=', Input::get('id_caixapadrao'))
                        ->whereRaw(" (titulo = 'CAIXA NOVA ENVIADA PARA CLIENTE' or titulo = 'EFETUADO CHECK-IN E CAIXA NAO INVENTARIADA') ")
                        ->count();
            if ($itens_em_branco > 0){
                Session::flash('errors', 'Existem itens não preenchidos na caixa. Preencha os itens não utilizados antes de adicionar novos itens na caixa');
                return Redirect::to('/documento/cadastrar/' . Input::get('id_cliente') . '/' . Input::get('id_caixapadrao'))
                            ->with('metodo', 'criar')
                            ->withInput();

            }
            return Redirect::to('/documento/editar/' . Input::get('id_cliente') . '/' . Input::get('id_caixapadrao'));
        } else {
            return Redirect::to('/documento/cadastrar/' . Input::get('id_cliente') . '/' . Input::get('id_caixapadrao'))
                            ->with('metodo', 'criar')
                            ->withInput();
        }
    }

    function getEditar($id_cliente, $id_caixapadrao, $id_item = 0) {
        if ($id_item > 0) {
            $doc = Documento::where('id_cliente', '=', $id_cliente)
                    ->where('id_caixapadrao', '=', $id_caixapadrao)
                    ->where('id_item', '=', $id_item)
                    ->first()
            ;
            if ($doc->inicio != '') {
                $doc->inicio = DateTime::createFromFormat('Y-m-d', $doc->inicio)->format('d/m/Y');
            }
            if ($doc->fim != '') {
                $doc->fim = DateTime::createFromFormat('Y-m-d', $doc->fim)->format('d/m/Y');
            }
            $anexos = $doc->anexos;
        } else {
            $doc = new Documento;
            $doc->id_cliente = $id_cliente;
            $doc->id_caixapadrao = $id_caixapadrao;
            $anexos = array();
        }
        $cliente = \Cliente::find($id_cliente);
        $centros =  array('' => '(não informado)') + \CentroDeCusto::where('id_cliente', $id_cliente)->orderBy('nome')->lists('nome', 'id_centrodecusto');
        $departamentos = array('' => '(não informado)') + \DepCliente::where('id_cliente', $id_cliente)->orderBy('nome')->lists('nome', 'id_departamento');
        $setores = array('' => '(não informado)') + \SetCliente::where('id_cliente', $id_cliente)->orderBy('nome')->lists('nome', 'id_setor');
        return View::make('documento_editar')
                        ->with('id_cliente', $id_cliente)
                        ->with('documento', $doc)
                        ->with('anexos', $anexos)
                        ->with('cliente', $cliente)
                        ->with('centros', $centros)
                        ->with('departamentos', $departamentos)
                        ->with('setores', $setores)
        ;
    }

    // Trata o post dos novos
    function postEditar($id_cliente, $id_caixapadrao, $id_item = 0) {
        Input::flash();
        $post = Input::all();
        $errors = array();
        $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent;
        if ($id_item == 0) {
            $doc = $rep->novoItem($id_cliente, $id_caixapadrao, \Auth::user()->id_usuario);
        } else {
            $doc = Documento::where('id_cliente', '=', $id_cliente)
                    ->where('id_caixapadrao', '=', $id_caixapadrao)
                    ->where('id_item', '=', $id_item)
                    ->first()
            ;
            // Pedro 21/08/2014: A primeira entrada só deve ser marcada pelo checkin. A digitação do documento não deve fazer com que 
            // a caixa seja cobrada. Somente a primeira entrada pode fazer a caixa ser cobrada.
            // 
//            if ($doc->primeira_entrada == ''){
//                $doc->primeira_entrada = date('Y-m-d');
//            }
            if ($doc->data_digitacao == '') {
                $doc->data_digitacao = date('Y-m-d');
            }
            if ($doc->usuario == '') {
                $doc->usuario = Auth::user()->id_usuario;
            }
        }
        $doc->id_expurgo = 0;
        if ($post['titulo'] == 'EFETUADO CHECK-IN E CAIXA NAO INVENTARIADA' || $post['titulo'] == 'CAIXA NOVA ENVIADA PARA CLIENTE'){
            $errors[''] = "Informe um título válido";
        }


        if ($post['inicio'] <> '') {
            $inicio = DateTime::createFromFormat('d/m/Y', $post['inicio']);
            if (is_object($inicio)) {
                $post['inicio'] = $inicio->format('Y-m-d');
            } else {
                $errors[] = "Formato da data de início inválido. Verifique.";
            }
        } else {
            unset($post['inicio']);
            $doc->inicio = null;
        }

        if ($post['fim'] <> '') {
            $fim = DateTime::createFromFormat('d/m/Y', $post['fim']);
            if (is_object($fim)) {
                $post['fim'] = $fim->format('Y-m-d');
            } else {
                $errors[] = "Formato da data de fim inválido. Verifique.";
            }
        } else {
            unset($post['fim']);
            $doc->fim = null;
        }

        if (!Input::has('id_centro') || !$post['id_centro'] > 0) {
            unset($post['id_centro']);
            $doc->id_centro = null;
        }
        if (!Input::has('id_setor') || !$post['id_setor'] > 0) {
            unset($post['id_setor']);
            $doc->id_setor = null;
        }
        if (!Input::has('id_departamento') || !$post['id_departamento'] > 0) {
            unset($post['id_departamento']);
            $doc->id_departamento = null;
        }

        if ($post['nume_inicial'] == '') {
            unset($post['nume_inicial']);
            $doc->nume_inicial = null;
        }

        if ($post['nume_final'] == '') {
            unset($post['nume_final']);
            $doc->nume_final = null;
        }

        if (count($errors) > 0) {
            Session::flash('errors', $errors);
            return Redirect::to('/documento/editar/' . $id_cliente . '/' . $id_caixapadrao . '/' . $id_item)
                            ->withInput()
            ;
        }

        try {
            $doc->fill($post);
            $doc->itemalterado = 1;
            $doc->conteudo = str_replace("\n", '<br/>', $post['conteudo']);
            $doc->id_usuario_alteracao = \Auth::user()->id_usuario;
            $doc->evento = __CLASS__ . '.' . __FUNCTION__;
            $doc->save();

            if (Input::has('liberar')) {
                $rep->liberar($doc->id_documento, Auth::user()->id_usuario);
                Session::flash('success', 'Dados alterados com sucesso e status alterado para liberado (o cliente pode alterar o item)');
                return Redirect::to('/documento/cadastrar/' . $id_cliente . '/' . $id_caixapadrao);
            } elseif (Input::has('revisar')) {
                $rep->revisar($doc->id_documento, Auth::user()->id_usuario);
                Session::flash('success', 'Dados alterados com sucesso e status alterado para revisado (o cliente não pode alterar o item)');
                return Redirect::to('/documento/cadastrar/' . $id_cliente . '/' . $id_caixapadrao);
            } else {
                Session::flash('success', 'Dados alterados com sucesso');
                return Redirect::to('/documento/cadastrar/' . $id_cliente . '/' . $id_caixapadrao);
            }
        } catch (\Whoops\Example\Exception $exc) {
            Session::flash('errors', $exc->getTraceAsString());
            return Redirect::to('/documento/editar/' . $id_cliente . '/' . $id_caixapadrao . '/' . $id_item)
                            ->withInput()
            ;
        }
    }

    public function postRevisar() {
        $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent;
        $id_documento = Input::get('id_documento');
        $id_caixapadrao = Input::get('id_caixapadrao');
        $id_cliente = Input::get('id_cliente');
        $id_item = Input::get('id_item');
//        try {
        $rep->revisar($id_documento, \Auth::user()->id_usuario);
        Session::flash('success', 'Item liberado com sucesso');
        return Redirect::to('/documento/editar/' . $id_cliente . '/' . $id_caixapadrao . '/' . $id_item);
//        } catch (Exception $exc) {
//            Session::flash('errors', 'Não foi possível liberar o item:'.$exc->getTraceAsString());
//            return Redirect::to('/documento/editar/' . $id_cliente . '/' . $id_caixapadrao . '/' . $id_item);
//        }
    }

    public function postLiberar() {
        $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent;
        $id_documento = Input::get('id_documento');
        $id_caixapadrao = Input::get('id_caixapadrao');
        $id_cliente = Input::get('id_cliente');
        $id_item = Input::get('id_item');
        try {
            $rep->liberar($id_documento, \Auth::user()->id_usuario);
            Session::flash('success', 'Item liberado com sucesso');
            return Redirect::to('/documento/editar/' . $id_cliente . '/' . $id_caixapadrao . '/' . $id_item);
        } catch (Exception $exc) {
            Session::flash('errors', 'Não foi possível liberar o item:' . $exc->getTraceAsString());
            return Redirect::to('/documento/editar/' . $id_cliente . '/' . $id_caixapadrao . '/' . $id_item);
        }
    }

    public function getLog($id_documento) {
        $doc = Documento::find($id_documento);
        $historico = LogDocumento::historico($id_documento);
        return View::make('documento_historico')
                        ->with('doc', $doc)
                        ->with('historico', $historico)
        ;
    }
}