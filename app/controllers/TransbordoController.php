<?php

class TransbordoController extends BaseController {

    public function getNovo() {
        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $galpoes = $rep->lista_disponiveis();
        return View::make('transbordo_novo')
                        ->with('clientes', $clientes)
                        ->with('galpoes', $galpoes)
        ;
    }

    public function postNovo() {
        Input::flash();
        $errors = array();
        if (intval(Input::get('id_cliente_antes')) == 0) {
            $errors[] = "Informe o cliente de onde as caixas serão transferidas";
        }
        if (intval(Input::get('id_cliente_depois')) == 0) {
            $errors[] = "Informe o cliente para onde as caixas serão transferidas";
        }
        if (count($errors) > 0) {
            Session::flash('errors', $errors);
            return Redirect::to('/transbordo/novo');
        } else {
            Session::flash('msg', 'Informe as caixas a serem transferidas');
            return Redirect::to('/transbordo/novocaixas');
        }
    }

    public function getNovocaixas() {
        $cliente_antes = \Cliente::find(Input::old('id_cliente_antes'));
        $cliente_depois = \Cliente::find(Input::old('id_cliente_depois'));
        $qtd_disp = \Endereco::get_qtdDisponivel(1);
        return View::make('transbordo_criar')
                        ->with('id_caixas', array())
                        ->with('cliente_antes', $cliente_antes)
                        ->with('cliente_depois', $cliente_depois)
                        ->with('qtd_disp', $qtd_disp)
        ;
    }

    public function postNovocaixas() {
        set_time_limit(0);

        Input::flashOnly(array('id_cliente_antes', 'id_cliente_depois', 'observacao'));
        $cliente_antes = \Cliente::find(Input::get('id_cliente_antes'));
        $cliente_depois = \Cliente::find(Input::get('id_cliente_depois'));
        $id_cliente = Input::get('id_cliente_antes');
        $id_caixas = Input::get('id_caixas', array());
        $qtd_disp = \Endereco::get_qtdDisponivel(1);
        $post = Input::all();
        if (Input::has('efetivar')) {
            // Efetiva o transbordo
            $rep_trans = new \Memodoc\Repositorios\RepositorioTransbordoEloquent;
            $transbordo = $rep_trans->novoTransbordo($post, Auth::user()->id_usuario);
            if ($transbordo) {
                if ($rep_trans->adicionaCaixas($transbordo, $id_caixas)) {
                    if ($rep_trans->processaTransbordo($transbordo, Auth::user()->id_usuario)) {
                        Session::flash('msg', 'Transbordo efetivado com sucesso.');
                        return Redirect::to('/transbordo/caixas/'.$transbordo->id_transbordo);
                    } else {
                        Session::flash('errors', $rep_trans->errors);
                    }
                } else {
                    Session::flash('errors', $rep_trans->errors);
                }
            } else {
                Session::flash('errors', $rep_trans->errors);
            }
        } else {
            // Adiciona as caixas informadas
            $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent;
            $novas = $rep->atualizaRelacaoDeCaixas($id_caixas, Input::get('id_caixapadrao_ini'), Input::get('id_caixapadrao_fim'), $id_cliente);
            $id_caixas = array_merge($id_caixas, $novas);
            if (count($rep->errors) > 0) {
                Session::flash('errors', $rep->errors);
            } else {
                $adicionadas = implode(", ", $novas);
                Session::flash('msg', 'Caixa(s) ' . $adicionadas . ' adicionada(s) com sucesso');
            }
        }

        if (count($id_caixas) > 0) {
            $caixas = \Documento::where('id_cliente', '=', $id_cliente)
                    ->whereIn('id_caixapadrao', $id_caixas)
                    ->select(array('id_caixa', 'id_caixapadrao'))
                    ->distinct()
                    ->orderBy('id_caixapadrao')->orderBy('id_item')
                    ->get();
        } else {
            $caixas = null;
        }

        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        return View::make('transbordo_criar')
                        ->with('id_caixas', $id_caixas)
                        ->with('caixas', $caixas)
                        ->with('clientes', $clientes)
                        ->with('cliente_antes', $cliente_antes)
                        ->with('cliente_depois', $cliente_depois)
                        ->with('qtd_disp', $qtd_disp)
        ;
    }

    public function getNovoos($id_os_chave) {
        $trans = \Transbordo::where('id_os_chave', '=', $id_os_chave)->get();
        $exp = \ExpurgoPai::where('id_os_chave', '=', $id_os_chave)->get();

        if (count($trans) > 0){
            Session::flash('errors', 'Já existe um transbordo associado a essa OS');
            return Redirect::to('/osok/visualizar/'.$id_os_chave);
        } else {
            if (count($exp) > 0){
                Session::flash('errors', 'Já existe um expurgo associado a essa OS');
                return Redirect::to('/osok/visualizar/'.$id_os_chave);
            } else {
                $os = Os::find($id_os_chave);
                $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');

                $rep = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
                $galpoes = $rep->lista_disponiveis();
                return View::make('transbordo_novoos')
                                ->with('os', $os)
                                ->with('clientes', $clientes)
                                ->with('galpoes', $galpoes)
                ;
            }
        }
    }

    public function postNovoos() {
        Input::flash();
        $id_os_chave = Input::get('id_os_chave');
        $errors = array();
        if (intval(Input::get('id_cliente_antes')) == 0) {
            $errors[] = "Informe o cliente onde as caixas estão atualmente";
        }
        if (count($errors) > 0) {
            Session::flash('errors', $errors);
            return Redirect::to('/transbordo/novoos/'.$id_os_chave);
        } else {
            Session::flash('msg', 'Informe as caixas a serem transferidas');
            return Redirect::to('/transbordo/novocaixasos/'.$id_os_chave);
        }
    }

    public function getNovocaixasos($id_os_chave) {
        $os = \Os::find($id_os_chave);
        $cortesia_movimentacao = Input::get('cortesia_movimentacao');
        $cliente_depois = $os->cliente;
        $cliente_antes = \Cliente::find(Input::old('id_cliente_antes'));
        $qtd_disp = \Endereco::get_qtdDisponivel(1);
        return View::make('transbordo_criaros')
                        ->with('id_caixas', array())
                        ->with('os', $os)
                        ->with('cliente_antes', $cliente_antes)
                        ->with('cliente_depois', $cliente_depois)
                        ->with('qtd_disp', $qtd_disp)
                        ->with('cortesia_movimentacao', $cortesia_movimentacao)
        ;
    }

    public function postNovocaixasos() {
        set_time_limit(0);
        $id_os_chave = Input::get('id_os_chave');
        Input::flashOnly(array('id_os_chave', 'id_cliente_antes', 'cortesia_movimentacao', 'observacao'));
        $os = \Os::find($id_os_chave);
        $cortesia_movimentacao = Input::get('cortesia_movimentacao');
        $cliente_depois = $os->cliente;
        $cliente_antes = \Cliente::find(Input::get('id_cliente_antes'));
        $id_cliente = Input::get('id_cliente_antes');
        $id_caixas = Input::get('id_caixas', array());
        $qtd_disp = \Endereco::get_qtdDisponivel(1);
        $post = Input::all();
        if (Input::has('efetivar') && Input::get('efetivar') == 'sim') {
            // Efetiva o transbordo
            $rep_trans = new \Memodoc\Repositorios\RepositorioTransbordoEloquent;
            $transbordo = $rep_trans->novoTransbordoOs($post, Auth::user()->id_usuario);
            if ($transbordo) {
                if ($rep_trans->adicionaCaixas($transbordo, $id_caixas)) {
                    if ($rep_trans->processaTransbordo($transbordo, Auth::user()->id_usuario)) {
                        Session::flash('msg', 'Transbordo efetivado com sucesso.');
                        return Redirect::to('/transbordo/caixas/'.$transbordo->id_transbordo);
                    } else {
                        Session::flash('errors', $rep_trans->errors);
                    }
                } else {
                    Session::flash('errors', $rep_trans->errors);
                }
            } else {
                Session::flash('errors', $rep_trans->errors);
            }
        } else {
            // Adiciona as caixas informadas
            $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent;
            $novas = $rep->atualizaRelacaoDeCaixas($id_caixas, Input::get('id_caixapadrao_ini'), Input::get('id_caixapadrao_fim'), $id_cliente);
            $id_caixas = array_merge($id_caixas, $novas);
            if (count($rep->errors) > 0) {
                Session::flash('errors', $rep->errors);
            } else {
                $adicionadas = implode(", ", $novas);
                Session::flash('msg', 'Caixa(s) ' . $adicionadas . ' adicionada(s) com sucesso');
            }
        }

        if (count($id_caixas) > 0) {
            $caixas = \Documento::where('id_cliente', '=', $id_cliente)
                    ->whereIn('id_caixapadrao', $id_caixas)
                    ->select(array('id_caixa', 'id_caixapadrao'))
                    ->distinct()
                    ->orderBy('id_caixapadrao')->orderBy('id_item')
                    ->get();
        } else {
            $caixas = null;
        }

        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
        return View::make('transbordo_criaros')
                        ->with('id_caixas', $id_caixas)
                        ->with('os', $os)
                        ->with('caixas', $caixas)
                        ->with('clientes', $clientes)
                        ->with('cliente_antes', $cliente_antes)
                        ->with('cliente_depois', $cliente_depois)
                        ->with('cortesia_movimentacao', $cortesia_movimentacao)
                        ->with('qtd_disp', $qtd_disp)
        ;
    }

    public function getList($id_transbordo = ''){
        $transbordos = \Transbordo::orderBy('created_at', 'desc')->get();
        return View::make('transbordo_listar')
                        ->with('transbordos', $transbordos)
                        ->with('id_transbordo', $id_transbordo)
        ;
    }


    public function getIndex() {
        return $this->getList();
    }

    public function getDepara($id_transbordo){
        set_time_limit(0);
        $transbordo = \Transbordo::find($id_transbordo);
        $transbordo_caixas = \TransbordoCaixa::where('id_transbordo', '=', $id_transbordo)->orderBy('id_caixapadrao_antes')->get();
        $html = View::make('transbordo_depara')
                        ->with('transbordo', $transbordo)
                        ->with('transbordo_caixas', $transbordo_caixas)
        ;
        $cab = View::make('transbordo_depara_cab')
                        ->with('transbordo', $transbordo)
        ;
        $mpdf = new \mPDF('utf-8', 'A4', '', '', '10', '10', '42', '14', '10', '10');  // largura x altura
        $mpdf->allow_charset_conversion = false;
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $cab = iconv("UTF-8", "UTF-8//IGNORE", $cab);

        $mpdf->SetHTMLHeader($cab);

        $mpdf->setFooter("||Página {PAGENO}/{nbpg}");

        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }

    public function getCaixas($id_transbordo){
        $transbordo = \Transbordo::find($id_transbordo);
        if($transbordo->id_os_chave > 0){
            $os = \Os::find($transbordo->id_os_chave);
        } else {
            $os = new \Os;
        }
        $caixas = \TransbordoCaixa::where('id_transbordo', '=', $id_transbordo)->orderBy('id_caixapadrao_antes')->get();
        return View::make('transbordo_caixas')
                        ->with('os', $os)
                        ->with('transbordo', $transbordo)
                        ->with('caixas', $caixas)
                ;

    }
}
