<?php
/**
 * Controla os retornos de pagamento dos clientes
 */
class RetornoController extends BaseController{
	public function getIndex(){
		return $this->listaRetornos('', '');
	}

	public function postIndex(){
		Input::flash();
		return $this->listaRetornos(Input::get("inicio"), Input::get("fim"));
	}

	public function listaRetornos($inicio, $fim){
		try{
			$dt_inicio = \Carbon::createFromFormat('d/m/Y', $inicio);
		} catch(Exception $e){
			$dt_inicio = null;
		}
		try{
			$dt_fim = \Carbon::createFromFormat('d/m/Y', $fim);
		} catch(Exception $e){
			$dt_fim = null;
		}
		if ($dt_inicio && $dt_fim){
			if ($dt_inicio->gt($dt_fim)){
		        Session::flash('errors', 'A data de início deve ser menor ou igual à data fim.');
			}
		}
		$retornos = \ArquivoRetorno::desde($inicio)
					->ate($fim)
					->orderBy('id_arquivo_retorno', 'desc')
					->paginate(20);
		$clientes = \Cliente::ativos()->orderBy('razaosocial')->lists(DB::raw("concat(nomefantasia, ' - ', razaosocial, '(', id_cliente, ')')"), 'id_cliente');
        return View::make('retorno.retorno_list')
                        ->with('retornos', $retornos)
                      	->with('clientes',$clientes)
        ;
	}

	public function getPagamentos($id_arquivo_retorno){
		$arquivo = \ArquivoRetorno::find($id_arquivo_retorno);
		$pagamentos = \Pagamento::where('id_arquivo_retorno', '=', $id_arquivo_retorno)
					->paginate(20);
        return View::make('retorno.pagamento_list')
                        ->with('pagamentos', $pagamentos)
                        ->with('arquivo', $arquivo)
        ;

	}

	public function getNovo(){
        return View::make('retorno.retorno_edit');
	}

	public function postNovo(){
		if (Input::hasFile('fileupload') && Input::file('fileupload')->isValid()){
			$arquivo = Input::file('fileupload');
			$rep = new \Memodoc\Repositorios\RepositorioPagamentos;
			$rep->cria_arquivo($arquivo, \Auth::user()->id_usuario);
			Session::flash('success', "Arquivo adicionado com sucesso");
			return Redirect::to("/retorno/");
		} else {
			Session::flash('errors', "Arquivo não recebido");
			return Redirect::to("/retorno/novo");
		}
	}

	public function getProcessar($id_arquivo_retorno){
		$arq = \ArquivoRetorno::find($id_arquivo_retorno);
		$rep = new \Memodoc\Repositorios\RepositorioPagamentos;
		$rep->processa_arquivo($arq);
		Session::flash('success', "Arquivo reprocessado com sucesso");
		return Redirect::to("/retorno/");
	}

	public function getDownload($id_arquivo_retorno){
		$arq = \ArquivoRetorno::find($id_arquivo_retorno);
		$headers = array(
              'Content-Type: '.$arq->mime_type,
            );
		return Response::download($arq->caminhoComNome(), $arq->nome_arquivo, $headers);
	}

	public function getExcluir($id_arquivo_retorno){
		\Pagamento::where('id_arquivo_retorno', '=', $id_arquivo_retorno)->delete();
		\ArquivoRetorno::where('id_arquivo_retorno', '=', $id_arquivo_retorno)->delete();
		Session::flash('success', "Arquivo excluído com sucesso");
		return Redirect::to("/retorno/");

	}

	public function getRevisarpagamento($id_pagamento){
		$pagamento = \Pagamento::find($id_pagamento);
		//$clientes = \Cliente::ativos()->orderBy('razaosocial')->lists(DB::raw($descricao), 'id_cliente');
		$clientes_ativos = \Cliente::ativos()->orderBy('razaosocial')->get();
		$clientes = array('' => "(não localizado)");
		foreach($clientes_ativos as $cliente){
			$descricao = $cliente->nomefantasia.' - '.$cliente->razaosocial.'('.$cliente->id_cliente.')';
			if($cliente->fl_pessoa_fisica){
				$descricao.= ' CPF:';
			} else {
				$descricao.= ' CNPJ:';
			}
			$descricao.= $cliente->cgc_formatado;
			$clientes[$cliente->id_cliente ] =  $descricao;
		}
		$pagadores = \Pagador::where('id_cliente', '=', $pagamento->id_cliente);
		$faturamentos = array('' => '(não informado)') + \DB::table('faturamento')
                ->join("periodo_faturamento", 'periodo_faturamento.id_periodo_faturamento', '=', 'faturamento.id_periodo_faturamento')
                ->where('faturamento.id_cliente', '=', $pagamento->id_cliente)
                ->select('id_faturamento', DB::raw("concat(periodo_faturamento.ano,'/', periodo_faturamento.mes, ' R$ ', REPLACE(REPLACE(REPLACE(FORMAT(val_servicos_sem_iss + val_iss + val_cartonagem_enviadas, 2), '.', '@'), ',', '.'), '@', ',')) anomesvalor"))
                ->orderBy('periodo_faturamento.ano', 'desc')
                ->orderBy('periodo_faturamento.mes', 'desc')
                ->lists('anomesvalor', 'id_faturamento');
       	return View::make('retorno.pagamento_edit')
                        ->with('pagamento', $pagamento)
                        ->with('clientes', $clientes)
						->with('pagadores', $pagadores)
                        ->with('faturamentos', $faturamentos)
        ;
	}

	public function postRevisarpagamento(){
		$pag = \Pagamento::find(Input::get('id_pagamento'));
		$pag->id_cliente = Input::get('id_cliente');
		$pag->id_faturamento = Input::get('id_faturamento');
		$pag->data_emissao_fmt = Input::get('data_emissao_fmt');
		$pag->data_vencimento_fmt = Input::get('data_vencimento_fmt');
		$pag->data_pagamento_fmt = Input::get('data_pagamento_fmt');
		$pag->valor_pago = Input::get('valor_pago');

		$pag->save();
		Session::flash('success', "Pagamento atualizado com sucesso");
		return Redirect::to("/retorno/pagamentos/".$pag->id_arquivo_retorno);
	}

	public function getPesquisar(){
		Input::flash();
		return $this->pesquisar();
	}

	public function postPesquisar(){
		Input::flash();
		return $this->pesquisar();
	}

	public function pesquisar(){
		$ordenacao = Input::get("ordenacao");
		if ($ordenacao == ''){
			$ordenacao = 'numero';
		}
		$semcliente = false;
		$duplicidadecnpj = false;
		if (Input::get('status_processamento') == 'sem_cliente'){
			$semcliente = true;
		}
		if (Input::get('status_processamento') == 'duplicados'){
			$duplicidadecnpj = true;
		}
		if (Input::has('xls')){
			$file = app_path().'/arquivos/_tmp_'.\Auth::user()->id_usuario;
			File::delete($file);
			$conteudo = View::make('retorno.pagamento_list_excel_cab');
			$conteudo =iconv("UTF8", "ISO-8859-1",  $conteudo);
			File::put($file, $conteudo);

			$pagamentos = \Pagamento::docliente(Input::get('id_cliente_filtro'))
						->vencimento(Input::get('data_vencimento_ini'), Input::get('data_vencimento_fim'))
						->emissao(Input::get('data_emissao_ini'), Input::get('data_emissao_fim'))
						->pagamento(Input::get('data_pagamento_ini'), Input::get('data_pagamento_fim'))
						->valor(Input::get('valor'))
						->numero(Input::get('numero'))
						->status(Input::get('status'))
						->semcliente($semcliente)
						->duplicidadecnpj($duplicidadecnpj)
						->orderBy($ordenacao)
						->chunk(200, function ($pagamentos) use($file) {
							$conteudo = View::make('retorno.pagamento_list_excel')
									->with('pagamentos', $pagamentos)
									;
							$conteudo =iconv("UTF8", "ISO-8859-1",  $conteudo);
							File::append($file, $conteudo);
				        });

			$conteudo = View::make('retorno.pagamento_list_excel_rod');
			File::append($file, $conteudo);
			return Response::download($file, 'pagamento.xls',array(
                'Content-Type' => 'application/vnd.ms-excel',
                'Content-Disposition' => 'attachment; filename="pagamentos.xls"'
            ) );

        } else {
			$qtd = \Pagamento::docliente(Input::get('id_cliente_filtro'))
						->vencimento(Input::get('data_vencimento_ini'), Input::get('data_vencimento_fim'))
						->emissao(Input::get('data_emissao_ini'), Input::get('data_emissao_fim'))
						->pagamento(Input::get('data_pagamento_ini'), Input::get('data_pagamento_fim'))
						->valor(Input::get('valor'))
						->numero(Input::get('numero'))
						->status(Input::get('status'))
						->semcliente($semcliente)
						->duplicidadecnpj($duplicidadecnpj)
						->count();
			$pagamentos = \Pagamento::docliente(Input::get('id_cliente_filtro'))
						->vencimento(Input::get('data_vencimento_ini'), Input::get('data_vencimento_fim'))
						->emissao(Input::get('data_emissao_ini'), Input::get('data_emissao_fim'))
						->pagamento(Input::get('data_pagamento_ini'), Input::get('data_pagamento_fim'))
						->valor(Input::get('valor'))
						->numero(Input::get('numero'))
						->status(Input::get('status'))
						->semcliente($semcliente)
						->duplicidadecnpj($duplicidadecnpj)
						->orderBy($ordenacao)
						->paginate(20);
			$clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');
			return View::make('retorno.pesquisar')
					->with('pagamentos', $pagamentos)
					->with('clientes', $clientes)
					->with('qtd', $qtd)
					->with('ordenacoes', $this->array_ordenacao(Input::get('ordenacao', 'numero')))
					;
        }
	}

	public function array_ordenacao($ordenacao_atual){
		$ordenacao = array();
		$ordenacao['id_pagamento'] = '<a href="javascript:ordena(\'id_pagamento\')" class="'.$this->classe_link('id_pagamento', $ordenacao_atual).'"><i class="aweso-long-arrow-down "></i></a>';
		$ordenacao['numero'] = '<a href="javascript:ordena(\'numero\')" class="'.$this->classe_link('numero', $ordenacao_atual).'"><i class="aweso-long-arrow-down"></i></a>';
		$ordenacao['nosso_numero'] = '<a href="javascript:ordena(\'nosso_numero\')" class="'.$this->classe_link('nosso_numero', $ordenacao_atual).'"><i class="aweso-long-arrow-down"></i></a>';
		$ordenacao['cnpj'] = '<a href="javascript:ordena(\'cnpj\')" class="'.$this->classe_link('cnpj', $ordenacao_atual).'"><i class="aweso-long-arrow-down"></i></a>';
		$ordenacao['valor'] = '<a href="javascript:ordena(\'valor\')" class="'.$this->classe_link('valor', $ordenacao_atual).'"><i class="aweso-long-arrow-down"></i></a>';
		$ordenacao['valor_pago'] = '<a href="javascript:ordena(\'valor_pago\')" class="'.$this->classe_link('valor_pago', $ordenacao_atual).'"><i class="aweso-long-arrow-down"></i></a>';
		$ordenacao['data_emissao'] = '<a href="javascript:ordena(\'data_emissao\')" class="'.$this->classe_link('data_emissao', $ordenacao_atual).'"><i class="aweso-long-arrow-down"></i></a>';
		$ordenacao['data_vencimento'] = '<a href="javascript:ordena(\'data_vencimento\')" class="'.$this->classe_link('data_vencimento', $ordenacao_atual).'"><i class="aweso-long-arrow-down"></i></a>';
		$ordenacao['data_pagamento'] = '<a href="javascript:ordena(\'data_pagamento\')" class="'.$this->classe_link('data_pagamento', $ordenacao_atual).'"><i class="aweso-long-arrow-down"></i></a>';
		return $ordenacao;
	}

	public function classe_link($campo, $ordenacao){
		if ($campo == $ordenacao){
			return "primary";
		} else {
			return "muted";
		}
	}

	public function pesquisa(){

	}

	public function getAssociarcliente($id_pagamento){
		$pagamento = \Pagamento::find($id_pagamento);
		$clientes = \Cliente::where('cgc', '=', $pagamento->cnpj)
				->orderBy('razaosocial')
				->lists(DB::raw("concat(nomefantasia, ' - ', razaosocial, '(', id_cliente, ')')"), 'id_cliente');
		if ($pagamento->id_cliente == ''){
			$clientes = array("" => '(selecione)') + $clientes;
			$pagadores = array('' => '(selecione o cliente para recarregar)');
		} else {
			$pagadores = array("" => 's');
		}
		return View::make('retorno.pagamento_escolhe_cliente')
				->with('clientes', $clientes)
				->with('pagamento', $pagamento)
				->with('pagadores', $pagadores)
				;

	}

	public function getPagadores($id_cliente){

	}

    public function postAtualizapagamentoduplicidade(){
        $id_pagamento = Input::get('id_pagamento');
        $pagamento = \Pagamento::find($id_pagamento);
        $pagamento->id_cliente = Input::get('id_cliente');
        $pagamento->id_pagador = Input::get("id_pagador");
		if (Input::get("id_cliente") != ''){
			$pagamento->fl_duplicidade_cnpj = 0;
		} else {
			$pagamento->fl_duplicidade_cnpj = 1;
		}
        $pagamento->save();
        $ret = array("status" => 1, "msg"=> 'Pagamento atualizado com sucesso');
        return Response::json($ret);
    }
}
