<?php

class CaixaController extends BaseController {

    function getCliente() {
        $clientes = Cliente::all();
        return View::make('caixa_cliente')->with('clientes', $clientes);
    }

    function getLocalizar($id) {

        $cliente = Cliente::findOrFail($id);
        $setores = $cliente->setor()->get();
        $departamentos = $cliente->departamento()->get();
        $centrocustos = $cliente->centrocusto()->get();

        return View::make('caixa_localizar')
                        ->with('cliente', $cliente)
                        ->with('setores', $this->formselect($setores))
                        ->with('centrocustos', $this->formselect($centrocustos))
                        ->with('departamentos', $this->formselect($departamentos))
                        ->with('model', new Documento);
    }

    function postLocalizar($id) {

        Input::flash();

        $titulo = Input::get('titulo');
        $conteudo = Input::get('conteudo');
        $id_departamento = Input::get('id_departamento');
        $id_centro = Input::get('id_centro');
        $id_setor = Input::get('id_setor');
        $id_caixa = Input::get('id_caixa');
        $reservado = Input::get('reservado');

        $model = Documento::where('id_cliente', '=', $id);

        if ($titulo <> '') {
            $model->where('titulo', 'LIKE', "%$titulo%");
        }
        if ($conteudo <> '') {
            $model->where('conteudo', 'LIKE', "%$conteudo%");
        }
        if ($id_setor <> '') {
            $model->where('id_setor', '=', $id_setor);
        }
        if ($id_centro <> '') {
            $model->where('id_centro', '=', $id_centro);
        }
        if ($id_departamento <> '') {
            $model->where('id_departamento', '=', $id_departamento);
        }
        if ($id_caixa <> '') {
            $model->where('id_caixa', '=', $id_caixa);
        }
        if ($reservado == '0') { // caixas em consulta
            $model->where('emcasa', '=', '0');
        }

        if ($reservado == '1') { // caixas reservadas
            $model->where('primeira_entrada', 'IS', DB::raw('null'));
        }


        $documentos = $model->get();

        /*
          $queries = DB::getQueryLog();
          $last_query = end($queries);

          echo '<pre>';
          print_r($last_query);
          echo '</pre>';
         */

        $cliente = Cliente::findOrFail($id);
        $setores = $cliente->setor()->get();
        $departamentos = $cliente->departamento()->get();
        $centrocustos = $cliente->centrocusto()->get();

        return View::make('caixa_localizar')
                        ->with('cliente', $cliente)
                        ->with('setores', $this->formselect($setores))
                        ->with('centrocustos', $this->formselect($centrocustos))
                        ->with('departamentos', $this->formselect($departamentos))
                        ->with('documentos', $documentos)
                        ->with('model', new Documento)


        ;
    }

    /**
     * Transform model in adapter Form::select
     * @link http://laravel.com/docs/html#drop-down-lists DropDown-lists
     * @param type $model
     * @param type $name
     * @return type array
     */
    function formselect($model, $name = 'nome') {
        $data = array();
        if ($model->count() > 0) {
            $data[''] = 'Selecione';
            foreach ($model as $fields) {
                foreach ($fields->getAttributes() as $fieldname => $field) {
                    if ($fieldname == $name) {
                        $data[$fields->getKey()] = $field;
                    }
                }
            }
        }
        return $data;
    }

    public function getHistorico($id_cliente = '', $id_caixapadrao = 0) {
        if ($id_cliente == '') {
            $id_cliente = Session::get('id_cliente');
        }
        $situacao = '';
        if ($id_cliente > 0 && $id_caixapadrao > 0) {
            $caixa = \Documento::where('id_cliente', '=', $id_cliente)->where('id_caixapadrao', '=', $id_caixapadrao)->first();
            if (count($caixa) == 0){
                $caixa = \DocumentoCancelado::where('id_cliente', '=', $id_cliente)->where('id_caixapadrao', '=', $id_caixapadrao)->first();
                $exp = \Expurgo::where('id_cliente', '=', $id_cliente)->where('id_caixapadrao', '=', $id_caixapadrao)->first();
                if (count($exp) > 0) {
                    $situacao .= 'Expurgada no expurgo <a class="btn btn-mini" href="/expurgo/caixasgravadas/'.$exp->id_expurgo_pai.'" target="_blank">'.$exp->id_expurgo_pai.'</a>';
                } else {
                    $situacao .= "Expurgada (nem nenhum expurgo encontrado)";
                }
            } else {
                $situacao = $caixa->situacao_galpao;                
            }
        
            if (count($caixa) > 0){
                $rep = new Memodoc\Repositorios\RepositorioCaixaEloquent;
                $oss = $rep->historico($caixa);
            } else {
                $caixa = new Documento;
                $oss = null;
                $situacao = 'Caixa não encontrada';
            }
            
        } else {
            $caixa = new Documento;
            $oss = null;
            $situacao = '-';
        }

        $clientes = FormSelect(Cliente::getAtivos(), 'razaosocial');

        return View::make('caixa_historico')
                        ->with('caixa', $caixa)
                        ->with('id_cliente', $id_cliente)
                        ->with('id_caixapadrao', $id_caixapadrao)
                        ->with('clientes', $clientes)
                        ->with('situacao', $situacao)
                        ->with('instancias', $oss)
        ;
    }

    public function postHistorico() {
        Session::set('id_cliente', Input::get('id_cliente'));
        return Redirect::to('/caixa/historico/' . Input::get('id_cliente') . '/' . Input::get('id_caixapadrao'));
    }

    public function getListarmovimentacoes($id_movimentacao = '') {
        $movimentacoes = \Movimentacao::orderBy('id_movimentacao', 'desc')->get();

        return View::make('movimentacao_listar')
                        ->with('movimentacoes', $movimentacoes)
                        ->with('id_movimentacao', $id_movimentacao)
        ;
    }

    public function getMovimentacaodetalhe($id_movimentacao) {
        $movimentacao = \Movimentacao::find($id_movimentacao);
        // $caixas = \MovimentacaoCaixa::where('id_movimentacao', '=', $id_movimentacao)
        //         ->orderBy('id_caixa')
        //         ->get();
        $sql = "select movimentacao_caixa.id_caixa, movimentacao_caixa.endereco_antes,  movimentacao_caixa.endereco_depois, documento.id_caixapadrao
                from movimentacao_caixa
                join documento on documento.id_caixa = movimentacao_caixa.id_caixa and documento.id_item = 1
                where movimentacao_caixa.id_movimentacao = ".$id_movimentacao."
                order by documento.id_caixapadrao";
        $caixas = \DB::select(\DB::raw($sql));
        return View::make('movimentacao_caixas')
                        ->with('movimentacao', $movimentacao)
                        ->with('id_movimentacao', $id_movimentacao)
                        ->with('caixas', $caixas)
        ;
    }

    public function getMovimentar() {
        $id_caixas = Input::get('id_caixas', Input::old('id_caixas', array()));
        $qtd_disp = \Endereco::get_qtdDisponivel(1);
        $rep_galpao = new \Memodoc\Repositorios\RepositorioGalpaoEloquent;
        $galpoes = $rep_galpao->lista_disponiveis();
        if (Input::has('efetivar')) {
            $movimentacao = $rep_galpao->efetivaMovimentacao($id_caixas, Input::get('observacao'), Input::get('fl_cancelar_endereco'), Auth::user()->id_usuario);
            if (is_object($movimentacao)) {
                Session::flash('msg', 'Movimentação criada com sucesso. Gere as etiquetas');
                return Redirect::to('/caixa/movimentacaodetalhe/' . $movimentacao->id_movimentacao);
            } else {
                Session::flash('errors', $rep_galpao->errors);
                if (count($id_caixas) > 0) {
                    $caixas = \Endereco::whereIn('id_caixa', $id_caixas)
                            ->orderBy('id_galpao')->orderBy('id_rua')->orderBy('id_predio')->orderBy('id_andar')->orderBy('id_unidade')
                            ->get();
                } else {
                    $caixas = null;
                }
                return View::make('caixa_mudar_endereco')
                                ->with('id_caixas', $id_caixas)
                                ->with('caixas', $caixas)
                                ->with('qtd_disp', $qtd_disp)
                                ->with('galpoes', $galpoes)
                ;
            }
        } else {
            $rep = new \Memodoc\Repositorios\RepositorioDocumentoEloquent;
            $id_caixa = Input::get('id_caixa');
            $id_caixa = str_replace(' ', '', $id_caixa);
            $novas = array();

            if (str_contains($id_caixa, ',')) {
                $id_caixas_novas = explode(',', $id_caixa);
                foreach ($id_caixas_novas as $id_caixa_nova) {
                    $cada_uma = $rep->atualizaRelacaoDeCaixasPorId($id_caixas, $id_caixa_nova);
                    $novas = array_merge($novas, $cada_uma);
                }
            } else {
                $novas = $rep->atualizaRelacaoDeCaixasPorId($id_caixas, $id_caixa);
            }
            $id_caixas = array_merge($id_caixas, $novas);

            if (count($rep->errors) > 0) {
                Session::flash('errors', $rep->errors);
            } else {
                $adicionadas = implode(", ", $novas);
                Session::flash('msg', 'Caixa(s) ' . $adicionadas . ' adicionada(s) com sucesso');
            }

            if (count($id_caixas) > 0) {
                $caixas = \Endereco::whereIn('id_caixa', $id_caixas)
                        ->orderBy('id_caixa')
                        ->get();
            } else {
                $caixas = null;
            }

            return View::make('caixa_mudar_endereco')
                            ->with('id_caixas', $id_caixas)
                            ->with('caixas', $caixas)
                            ->with('qtd_disp', $qtd_disp)
                            ->with('galpoes', $galpoes)
            ;
        }
    }

    public function postMovimentar() {
        Input::flashExcept('id_caixa');
        return $this->getMovimentar();
    }

    public function getMovimentacaodepara($id_movimentacao) {
            set_time_limit(0);
            $movimentacao = \Movimentacao::find($id_movimentacao);
//            $caixas = \MovimentacaoCaixa::where('id_movimentacao', '=', $id_movimentacao)->orderBy('endereco_antes')->get();
            $sql = "select movimentacao_caixa.id_caixa, movimentacao_caixa.endereco_antes,  movimentacao_caixa.endereco_depois, documento.id_caixapadrao, cliente.razaosocial, documento.id_cliente
                    from movimentacao_caixa
                    join documento on documento.id_caixa = movimentacao_caixa.id_caixa and documento.id_item = 1
                    join cliente on cliente.id_cliente = documento.id_cliente
                    where movimentacao_caixa.id_movimentacao = ".$id_movimentacao."
                    order by documento.id_caixapadrao";
            $caixas = \DB::select(\DB::raw($sql));
            $html = View::make('movimentacao_depara')
                    ->with('movimentacao', $movimentacao)
                    ->with('caixas', $caixas)
            ;
            $cab = View::make('movimentacao_depara_cab')
                    ->with('movimentacao', $movimentacao)
            ;
            return $cab.$html;
    }

    public function getMovimentacaodeparapdf($id_movimentacao) {
        set_time_limit(0);
        $movimentacao = \Movimentacao::find($id_movimentacao);
        $caixas = \MovimentacaoCaixa::where('id_movimentacao', '=', $id_movimentacao)->orderBy('endereco_antes')->get();
        $html = View::make('movimentacao_depara')
                ->with('movimentacao', $movimentacao)
                ->with('caixas', $caixas)
        ;
        $cab = View::make('movimentacao_depara_cab')
                ->with('movimentacao', $movimentacao)
        ;
        $mpdf = new \mPDF('utf-8', 'A4', '', '', '10', '10', '30', '14', '10', '10');  // largura x altura
        $mpdf->allow_charset_conversion = false;
        $html = iconv("UTF-8", "UTF-8//IGNORE", $html);
        $cab = iconv("UTF-8", "UTF-8//IGNORE", $cab);

        $mpdf->SetHTMLHeader($cab);

        $mpdf->setFooter("||Página {PAGENO}/{nbpg}");

        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }

    public function getRateio(){
        $clientes = \Cliente::ativos()
                 ->orderBy('razaosocial')
                 ->lists(DB::raw("concat(nomefantasia, ' - ', razaosocial, '(', id_cliente, ')')"), 'id_cliente');

        $departamentos = array();
        $centros = array();
        $setores = array();
        return View::make('caixa/rateio')
                        ->with('clientes', $clientes)
                        ->with('departamentos', $departamentos)
                        ->with('centros', $centros)
                        ->with('setores', $setores)
                ;
    }

    public function postRateio(){
        Input::flash();
        $id_cliente = Input::get('id_cliente');
        $clientes = \Cliente::ativos()->orderBy('razaosocial')
                    ->lists(DB::raw("concat(nomefantasia, ' - ', razaosocial, '(', id_cliente, ')')"), 'id_cliente');

        $rep = new \Memodoc\Repositorios\RepositorioCaixaEloquent;
        $data = $rep->dados_para_rateio($id_cliente);
        $data['clientes'] = $clientes;

        return View::make('caixa/rateio', $data);

    }

    public function getRateioxls($id_cliente){
        $rep = new \Memodoc\Repositorios\RepositorioCaixaEloquent;
        $data = $rep->dados_para_rateio($id_cliente);

        $html = View::make('caixa/tabela_rateio_xls', $data);
        $conteudo =iconv("UTF8", "ISO-8859-1",  $html);
        return Response::make($conteudo, '200', array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="rateio_'.$id_cliente.'.xls"'
        ));

    }

    public function getRateiofaturamento($id_faturamento){
        $rep = new \Memodoc\Repositorios\RepositorioCaixaEloquent;
        $data = $rep->dados_para_rateio_faturamento($id_faturamento);
        $fat = \Faturamento::find($id_faturamento);

        $html = View::make('caixa/tabela_rateio_fat', $data);
        $conteudo =iconv("UTF8", "ISO-8859-1",  $html);
        return Response::make($conteudo, '200', array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'attachment; filename="rateio_'.$fat->id_cliente.'_'.$fat->periodofaturamento->ano.'_'.$fat->periodofaturamento->mes.'.xls"'
        ));

    }
}
