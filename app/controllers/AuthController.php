<?php

/**
 * Controla a verificação de acessos
 */
class AuthController extends \BaseController {
    public function getLogin() {
    	$aviso = \Aviso::where('fl_padrao', '=', 1)->first();
    	if (!is_object($aviso) ){
    		$aviso = null;
    	}
        $username = Input::get('username');
        return View::make('login')
        	->with('aviso', $aviso)
            ->with('username', $username);
    }

    public function postLogin() {
        $autenticador = new \Memodoc\Servicos\Autenticacao();
        if ($autenticador->temAcesso(Input::all())) {
            if (!\Auth::user()->adm && \Auth::user()->temClienteBloqueado()){
                Session::flash('msg', 'Existem pendências financeiras, e por conta disso seu acesso foi bloqueado. Por favor entre em contato com a Memodoc para regularizar a sua situação. (21) 2269-8040 ou (21) 9 9883-8493.');
                return Redirect::to('auth/login')->with('login_errors', true);                
            } else {
                $rota = $autenticador->rotaHome(Auth::user());
                return Redirect::to($rota);                
            }
        } else {
            Session::flash('msg', 'Usuário ou senha inválidos..');
            return Redirect::to('auth/login')->with('login_errors', true);
        }
    }
    /**
     * Gera um token de renovação de senha para o usuário
     * @return [type] [description]
     */
    public function getReenviarsenha(){
        return View::make('reenviarsenha');
    }

    public function postReenviarsenha(){
        $usuario = \Usuario::where('username', '=', Input::get('username'))->first();
        if ( count($usuario) > 0 ){
            $rep = new Memodoc\Repositorios\RepositorioUsuarioExternoEloquent;
            $rep->enviarNovaSenha($usuario);
            Session::flash('msg','Um e-mail com o link para a renovação da senha de acesso foi enviado para sua caixa de entrada. Esse link tem validade de 2 horas.');
            return Redirect::to('auth/login');
        } else {
            Session::flash('msg', 'E-mail não cadastrado');
            return Redirect::back();
        }
    }

    /**
     * A partir de um token válido, altera a senha do usuário
     * @param  [type] $token [description]
     * @return [type]        [description]
     */
    public function getRenovarsenha($token){
        $usuario = \Usuario::where('token_nova_senha', '=', $token)->first();
        if (count($usuario) > 0){
            $dif = Carbon::now('America/Sao_Paulo')->diffInMinutes($usuario->dt_emissao_token_senha);
            \log::info($dif);
            if ( $dif > 120 ){
                Session::flash('msg', 'A validade do link de renovação de senha expirou. Por favor, tente novamente.');
                return Redirect::to('auth/login')->with('login_errors', true);
            } else {
                return View::make('renovarsenha_token')->with('token', $token);
            }
        } else {
            Session::flash('msg', 'Credencial inválida');
            return Redirect::to('auth/login')->with('login_errors', true);
        }

    }
    public function postRenovarsenha(){
        $token = Input::get('token');
        $usuario = \Usuario::where('token_nova_senha', '=', $token)->first();
        if ( count($usuario) > 0 ){
            // Verifica se o link foi gerado a menos de duas horas
            $rep = new Memodoc\Repositorios\RepositorioUsuarioExternoEloquent;
            if ( Input::get('senha') != Input::get('senha_rep') ){
                Session::flash('msg', 'Senhas informadas não batem. Por favor tente novamente');
                return Redirect::to('auth/renovarsenha/'.$token)->with('login_errors', true);
            } else {
                if ( strlen(Input::get('senha')) < 8 ){
                    Session::flash('msg', 'Por segurança, as senhas devem ter no mínimo 8 letras.');
                    return Redirect::to('auth/renovarsenha/'.$token)->with('login_errors', true);
                } else {
                    $usuario->password = \Hash::make(Input::get('senha'));
                    $usuario->token_nova_senha = null;
                    $usuario->dt_emissao_token_senha = null;
                    $usuario->save();
                    $rep->notificarNovaSenha($usuario);
                    Session::flash('msg', 'Senha alterada com sucesso. Teste seu acesso.');
                    return Redirect::to('auth/login')->with('login_errors', true);
                }
            }
        } else {
            Session::flash('msg', 'Credencial inválida');
            return Redirect::to('auth/login')->with('login_errors', true);
        }
    }
    public function getLogout() {
        Auth::logout();
        return Redirect::to('/auth/login');
    }

}
