<?php

class ClienteController extends BaseController {

    protected $repCliente;

    public function __construct() {
        $this->repCliente = new \Memodoc\Repositorios\RepositorioClienteEloquent;
    }

    public function getIndex() {
        $cliente = new Cliente;
        $search = Input::old('search');
        if ($search != '') {
            $cliente = $cliente->search($search);
        }
        $fl_ativo = Input::old('fl_ativo');
        if ($fl_ativo == '') {
            $fl_ativo = 1;
        }
        if ($fl_ativo != '-1') {
            $cliente = $cliente->where('fl_ativo', '=', $fl_ativo);
        }
        $clientes = $cliente->orderby('razaosocial')->get();

        $colunas = array(
            array('titulo' => 'Nome', "campo" => 'razaosocial', "width" => "auto"),
            array('titulo' => 'Fantasia', "campo" => 'nomefantasia', "width" => "auto"),
        );

        return View::make('cliente_list')
                        ->with('linkEdicao', 'cliente/edit')
                        ->with('linkCriacao', 'cliente/novo')
                        ->with('titulo', 'Clientes')
                        ->with('colunas', $colunas)
                        ->with('instancias', $clientes);
    }

    public function postIndex() {
        Input::flash();
        return Redirect::to('cliente')->withInput();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getNovo() {
        $cliente = new \Cliente;
        return View::make('ClienteAbaIdentificacao')->with('cliente', $cliente);
    }

    public function postNovo() {
        Input::flash();
        $post = Input::All();

        try {
            $cliente = new \Cliente;
            $post['cgc'] = preg_replace('/\D/', '', $post['cgc']);
            $cliente->fill($post);
            $cliente->save();
            Cliente::getFresh();
            $sql = "insert into tipoprodcliente (id_cliente, id_tipoproduto, valor_contrato, valor_urgencia, minimo) select " . $cliente->id_cliente . ", id_tipoproduto, 0, 0, 0 from tipoproduto";
            \DB::insert(\DB::raw($sql));
            Session::flash('success', 'Cliente criado com sucesso !');

            return Redirect::to('cliente/edit/' . $cliente->id_cliente)->withInput();
        } catch (Exception $ex) {
            Session::flash('errors', $ex->getMessage());
            return Redirect::to('cliente/edit/' . $cliente->id_cliente)->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function getEdit($id) {
        $cliente = Cliente::find($id);
        if($cliente->fl_pessoa_fisica == ''){
            $cliente->fl_pessoa_fisica = 0;
        }
        return View::make('ClienteAbaIdentificacao')->with('cliente', $cliente);
    }

    public function postEdit($id) {
        Input::flash();
        $post = Input::All();

        $cliente = Cliente::find($id);
        $cliente->fill($post);
        $cliente->cgc = str_replace('.', '', str_replace('/', '', str_replace('-', '', $cliente->cgc)));
        $cliente->save();
        Cliente::getFresh();
        Session::flash('success', 'Cliente atualizado com sucesso !');

        return Redirect::to('cliente/edit/' . $cliente->id_cliente)->withInput();
    }

    public function getContrato($id) {
        $cliente = \Cliente::find($id);
        $pagadores = array('' => '(o próprio)') + \Cliente::where('id_cliente', '<>', $id)->orderBy('id_cliente')->lists(\DB::raw("concat('(', id_cliente, ') ', razaosocial)"), 'id_cliente');
        $indice = \Indice::orderBy('descricao')->lists('descricao', 'id_indice');
        $tipo_faturamento = \TipoFaturamento::orderBy('descricao')->lists('descricao', 'id_tipo_faturamento');
        $dias_vencimento = \DiaVencimento::orderBy('dia')->lists('dia', 'dia');
        return View::make('ClienteAbaContrato')
                        ->with('cliente', $cliente)
                        ->with('indice', $indice)
                        ->with('tipo_faturamento', $tipo_faturamento)
                        ->with('dias_vencimento', $dias_vencimento)
                        ->with('pagadores', $pagadores)
                        ;
    }

    public function postContrato($id) {
        Input::flash();
        $post = Input::All();
        try{
            $msg = '';
            // formatando data
            if (Input::has('iniciocontrato')) {
                $iniciocontrato = DateTime::createFromFormat('d/m/Y', $post['iniciocontrato']);
                if (isset($iniciocontrato)) {
                    $post['iniciocontrato'] = $iniciocontrato->format('Y-m-d');
                } else {
                    throw new Exception ('Data de início de contrato inválida');
                }
            }

            if (Input::has('em_atraso_desde')) {
                $em_atraso_desde = DateTime::createFromFormat('d/m/Y', $post['em_atraso_desde']);
                if (isset($em_atraso_desde)) {
                    $post['em_atraso_desde'] = $em_atraso_desde->format('Y-m-d');
                } else {
                    $post['em_atraso_desde'] = null;
                }
            }

            if (trim($post['franquiacustodia']) != '') {
                if (!is_numeric(trim($post['franquiacustodia']))) {
                    $msg .= '<br/>- A franquia de custódia deve ser um número';
                }
            }
            
            if (trim($post['descontocustodialimite']) != '') {
                if (!is_numeric(trim($post['descontocustodialimite']))) {
                    $msg .= '<br/>- O limite de caixas para o desconto de custódia deve ser um número';
                }
            }

            if($msg != ''){
                throw new Exception ($msg);
            }


            $cliente = Cliente::find($id);
            $cliente->fill($post);
            $cliente->valorminimofaturamento = str_replace(',', '.', str_replace('.', '', Input::get('valorminimofaturamento')));
            $cliente->franquiamovimentacao = str_replace(',', '.', str_replace('.', '', Input::get('franquiamovimentacao')));
            $cliente->descontocustodiapercentual = str_replace(',', '.', str_replace('.', '', Input::get('descontocustodiapercentual')));

            if (Auth::user()->nivel->minimoAdministrador()) {
                $cliente->fl_frete_manual = Input::get('fl_frete_manual');
            }
            $cliente->save();
            Cliente::getFresh();
            Session::flash('success', 'Cliente atualizado com sucesso !');
        } catch(\Exception $e){
            Session::flash('errors', 'Não foi possível atualizar pois: '.$e->getMessage());
        }
        return Redirect::to('cliente/contrato/' . $id . '#tab_contrato')->withInput();
    }

    public function getEnderecoprincipal($id) {
        $cliente = \Cliente::find($id);
        return View::make('ClienteAbaEnderecoprincipal')
                        ->with('cliente', $cliente)
        ;
    }

    public function postEnderecoprincipal($id) {
        Input::flash();
        $post = Input::All();

        $cliente = Cliente::find($id);
        $cliente->fill($post);
        $cliente->clonaEndParaCorrespondencia();
        $cliente->clonaEndParaEntregaSeNaoHouver();
        $cliente->save();
        Cliente::getFresh();
        Session::flash('success', 'Cliente atualizado com sucesso !');

        return Redirect::to('cliente/enderecoprincipal/' . $id . '#tab_enderecoprincipal')->withInput();
    }

    public function getEnderecocor($id) {
        $cliente = \Cliente::find($id);
        return View::make('ClienteAbaEnderecoCor')
                        ->with('cliente', $cliente)
        ;
    }

    public function postEnderecocor($id) {
        $post = Input::All();
        try {
            $cliente = Cliente::find($id);
            if (Input::has('Clonar')) {
                $cliente->clonaEndParaCorrespondencia(true);
            } else {
                $cliente->fill($post);
            }
            $cliente->save();
            Cliente::getFresh();
            Session::flash('success', 'Cliente atualizado com sucesso !');
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', 'Nao foi possivel atualizar o endereço:' . $ex->getMessage());
        }

        return Redirect::to('/cliente/enderecocor/' . $id . '#tab_enderecocor');
    }

    public function getAuto($model, $action, $id) {

        $selects = array(
            'TelefoneCliente' => array('TipoTelefone' => 1),
            'TelefoneContato' => array('TipoTelefone', 'Contato'),
            'EmailCliente' => array('TipoEmail'),
            'Solicitante' => array('SetCliente' => "id_cliente = $id ",
                'CentroDeCusto' => "id_cliente = $id ",
                'DepCliente' => "id_cliente = $id "),
        );



        $cliente = new Cliente;
        if ($action == "list") {
            $data = $cliente->simpleList($model, $id, $selects);
        } else {
            $data = $cliente->simple($action, $model, $id, $selects);
        }
        // pr(queryLog());
        $data['cliente'] = isset($data['one']) ? $data['one'] : array();

        // Create or Edit
        if ($action == 'create' or $action == 'edit') {
            $view = 'cliente_form_' . strtolower($model);
        }

        // List
        if ($action == 'list') {
            $view = 'cliente_list_' . strtolower($model);
        }

        // Delete
        if ($action == 'delete') {
            Session::flash('success', 'Registro foi excluído com sucesso!');
            return Redirect::to('cliente/auto/' . $model . '/list/' . $data['one']['id_cliente'] . '#tab_' . $model);
        }

        return View::make($view, $data);
    }

    public function postAuto($model, $action, $id) {

        $cliente = new Cliente;
        $data = $cliente->simple($action, $model, $id);

        if ($action == 'edit') {
            Session::flash('success', 'Editado com sucesso!');
        }
        if ($action == 'create') {
            Session::flash('success', 'Incluído com sucesso!');
        }

        return Redirect::to('cliente/auto/' . $model . '/list/' . $data['one']['id_cliente'] . '#tab_' . $model);
    }

    public function getInative($id_cliente) {
        $cliente = Cliente::find($id_cliente);
        if ($this->repCliente->inative($cliente)) {
            return Redirect::to('/cliente')->with('success', 'Cliente inativado com sucesso!');
        } else {
            return Redirect::to('/cliente')->with('errors', $this->repCliente->errors);
        }
    }

    public function getServicos($id_cliente) {
        $cliente = Cliente::findOrFail($id_cliente);
        $servicos = $cliente->tipoprodcliente;
        return View::make('cliente_list_servicos', compact('cliente', 'servicos'));
    }

    public function getServicoseditar($id_cliente) {
        $cliente = Cliente::findOrFail($id_cliente);
        $servicos = $cliente->tipoprodcliente;
        return View::make('cliente_list_servicoseditar', compact('cliente', 'servicos'));
    }

    public function postServicoseditar($id_cliente) {
        $cliente = Cliente::findOrFail($id_cliente);
        $valor_contratos = Input::get('valor_contrato');
        $valor_urgencias = Input::get('valor_urgencia');
        $minimos = Input::get('minimo');
        $minimofat = Input::get('valorminimofaturamento');
        try {
            DB::connection()->getPdo()->beginTransaction();
            foreach ($valor_contratos as $id_tipoproduto => $valor_contrato) {
                TipoProdCliente::salvar(array(
                    'id_tipoproduto' => $id_tipoproduto, // chave composta
                    'id_cliente' => $id_cliente, // chave composta
                    'valor_contrato' => str_replace(',', '.', str_replace('.', '', $valor_contrato)),
                    'valor_urgencia' => str_replace(',', '.', str_replace('.', '', $valor_urgencias[$id_tipoproduto])),
                    'minimo' => $minimos[$id_tipoproduto],
                ));
            }
            $cliente->valorminimofaturamento = str_replace(',', '.', str_replace('.', '', $minimofat));
            $cliente->save();
            DB::connection()->getPdo()->commit();
            Session::flash('success', 'Valores dos serviços foram alterados com sucesso');
        } catch (Exception $exc) {
            Session::flash('error', $exc->getTraceAsString());
        }
        return Redirect::to('/cliente/servicos/' . $id_cliente . '#tab_servicos');
        //$cliente = Cliente::findOrFail($id_cliente);
        //$servicos = $cliente->tipoprodcliente;
        //return View::make('cliente_list_servicoseditar', compact('cliente','servicos'));
    }

    public function getEndeentrega($id) {
        $enderecos = \Endeentrega::where('id_cliente', '=', $id)->get();
        $cliente = \Cliente::find($id);
        foreach($enderecos as &$end){
            $end->qtdOs = Os::where('id_endeentrega', '=', $end->id_endeentrega)->count();
        }
        $data = array(
            'enderecos' => $enderecos
            , 'id_cliente' => $id
            , 'cliente' => $cliente
        );

        return View::make('cliente_list_enderecoentrega', $data);
    }

    public function getEndeentregaclonarprincipal($id) {
        $cliente = \Cliente::find($id);
        $cliente->clonaEndParaEntrega(true);
            Session::flash('success', 'Endereço de entrega criado a partir do endereço principal com sucesso!');
            return Redirect::to('/cliente/endeentrega/' . $id . '#tab_EnderecoEntrega');

    }

    public function getEndeentregaclonarcorr($id) {
        $cliente = \Cliente::find($id);
        $cliente->clonaCorrespondenciaParaEntrega(true);
            Session::flash('success', 'Endereço de entrega criado a partir do endereço principal com sucesso!');
            return Redirect::to('/cliente/endeentrega/' . $id . '#tab_EnderecoEntrega');

    }

    public function getEndeentregadel($id, $id_endeentrega = 0) {
        \Endeentrega::where('id_endeentrega', '=', $id_endeentrega)->delete();
            Session::flash('success', 'Endereço de entrega excluido com sucesso!');
            return Redirect::to('/cliente/endeentrega/' . $id . '#tab_EnderecoEntrega');

    }

    public function getEndeentregaedit($id, $id_endeentrega = 0) {
        if ($id_endeentrega > 0) {
            $endeentrega = \Endeentrega::find($id_endeentrega);
            $titulo = 'Alterar endereço de entrega';
        } else {
            $endeentrega = new \Endeentrega;
            $endeentrega->id_cliente = $id;
            $titulo = 'Novo endereço de entrega';
        }
        $cliente = \Cliente::find($endeentrega->id_cliente);

        $data = array(
            'endeentrega' => $endeentrega
            , 'cliente' => $cliente
            , 'titulo' => $titulo
            , 'estados' => \Estado::valores()
        );
        return View::make('ClienteEnderecoEntrega', $data);
    }

    public function postEndeentregaedit($id, $id_endeentrega = 0) {
        try {

            if ($id_endeentrega > 0) {
                $endeentrega = \Endeentrega::find($id_endeentrega);
            } else {
                $endeentrega = new \Endeentrega;
                $endeentrega->id_cliente = $id;
            }
            $endeentrega->fill(Input::all());
            $endeentrega->save();
            Session::flash('success', 'Endereço de entrega atualizado com sucesso!');
            return Redirect::to('/cliente/endeentrega/' . $id . '#tab_EnderecoEntrega');
        } catch (Exception $ex) {
            Input::flash();
            Session::flash('error', $ex->getTraceAsString());
            return Redirect::to('/cliente/endeentregaedit/' . $id . '/' . $id_endeentrega . '#tab_EnderecoEntrega');
        }
    }

    public function getPropriedade($id){
        $cliente = \Cliente::find($id);
        $prop = \Propriedade::where('id_cliente', '=', $id)->first();
        if (!is_object($prop) || intval($prop->id_propriedade) == 0){
            $prop = new \Propriedade;
            $prop->id_cliente = $id;
            $valores = array();
        } else {
            $valores = \Valor::where('id_propriedade', '=', $prop->id_propriedade)->get();
        }

        foreach($valores as &$valor){
            $valor->qtdDocs = \Documento::where('id_valor', '=', $valor->id_valor)->count();
        }

        $data = array(
            'cliente'   => $cliente,
            'prop'      => $prop,
            'valores'   => $valores
        );

        return View::make('ClientePropriedade', $data);
    }

    public function getPropriedadeedit($id){
        $cliente = \Cliente::find($id);
        $prop = \Propriedade::where('id_cliente', '=', $id)->first();
        if (!is_object($prop) || intval($prop->id_propriedade) == 0){
            $prop = new \Propriedade;
            $prop->id_cliente = $id;
            $titulo = 'Criar campo personalizado';
        } else {
            $titulo = 'Alterar campo personalizado';
        }

        $data = array(
            'cliente'   => $cliente,
            'prop'      => $prop,
            'titulo'    => $titulo
        );

        return View::make('ClientePropriedade_edit', $data);
    }

    public function postPropriedadeedit($id){
        $prop = \Propriedade::where('id_cliente', '=', $id)->first();
        if (!is_object($prop) || intval($prop->id_propriedade) == 0){
            $prop = new \Propriedade;
            $prop->id_cliente = $id;
            $msg = "Campo criado com sucesso!";
        } else {
            $msg = "Campo alterado com sucesso!";
        }
        $prop->nome = Input::get('nome');
        $prop->save();

        Session::flash('success', $msg);
        return Redirect::to('/cliente/propriedade/'.$id.'#tab_Propriedade');
    }

    public function getValoredit($id, $id_valor){
        $cliente = \Cliente::find($id);
        $prop = \Propriedade::where('id_cliente', '=', $id)->first();
        $valor = \Valor::where('id_valor', '=', $id_valor)->first();
        if (!is_object($valor)){
            $valor = new \Valor;
            $titulo = 'Adicionar valor ao campo personalizado "'.$prop->nome.'"';
        } else {
            $titulo = 'Alterar valor do campo personalizado "'.$prop->nome.'"';
        }

        $data = array(
            'cliente'   => $cliente,
            'prop'      => $prop,
            'titulo'    => $titulo,
            'valor'     => $valor
        );

        return View::make('ClienteValor_edit', $data);
    }

    public function postValoredit($id, $id_valor){
        $cliente = \Cliente::find($id);
        $prop = \Propriedade::where('id_cliente', '=', $id)->first();
        $valor = \Valor::where('id_valor', '=', $id_valor)->first();
        if (!is_object($valor) ){
            $valor = new \Valor;
            $valor->id_propriedade = $prop->id_propriedade;
            $msg = "Valor criado com sucesso!";
        } else {
            $msg = "Valor alterado com sucesso!";
        }
        $valor->nome = Input::get('nome');
        $valor->save();

        Session::flash('success', $msg);
        return Redirect::to('/cliente/propriedade/'.$id.'#tab_Propriedade');
    }

    public function getValordel($id_valor){
        $valor = \Valor::find($id_valor);
        $prop = \Propriedade::find($valor->id_propriedade);
        $valor->delete();

        Session::flash('success', 'Valor removido com sucesso');
        return Redirect::to('/cliente/propriedade/'.$prop->id_cliente.'#tab_Propriedade');
    }
    public function getRateio($id_cliente){
        $cliente = \Cliente::find($id_cliente);

        $rep = new \Memodoc\Repositorios\RepositorioCaixaEloquent;
        $data = $rep->dados_para_rateio($id_cliente);

        return View::make('cliente_rateio', $data);

    }

    public function getPagadores($id_cliente) {
        $cliente = Cliente::findOrFail($id_cliente);
        $pagadores = \Pagador::where("id_cliente", '=', $id_cliente)->orderBy('razaosocial')->get();
        return View::make('cliente_list_pagadores', compact('cliente', 'pagadores'));
    }

    public function getPagadoreseditar($id_cliente, $id_pagador, $id_pagamento = '') {
        $cliente = \Cliente::find($id_cliente);
        $pagador = \Pagador::where('id_pagador', '=', $id_pagador)->first();
        if (!is_object($pagador) || intval($pagador->id_pagador) == 0){
            $pagador = new \Pagador;
            $pagador->id_cliente = $id_cliente;
            $titulo = 'Criar pagador';
            if ($id_pagamento != ''){
                $pag = \Pagamento::find($id_pagamento);
                $pagador->razaosocial = $pag->nome_cliente;
                $pagador->cnpj = $pag->cnpj;
            }
        } else {
            $titulo = 'Alterar pagador';
        }

        $data = array(
            'cliente'   => $cliente,
            'pagador'      => $pagador,
            'titulo'    => $titulo
        );

        return View::make('ClientePagador_edit', $data);
    }

    public function postPagadoreseditar($id_cliente, $id_pagador, $id_pagamento = '') {
        if ($id_pagador > 0){
            $pagador = \Pagador::find($id_pagador);
            $msg = "Pagador atualizado com sucesso!";
        } else {
            $pagador = new \Pagador;
            $pagador->id_cliente = $id_cliente;
            $msg = "Pagador criado com sucesso!";
        }
        $pagador->razaosocial = Input::get('razaosocial');
        $pagador->cnpj = preg_replace('/\D/', '', Input::get('cnpj'));
        $pagador->logradouro = Input::get('logradouro');
        $pagador->numero = Input::get('numero');
        $pagador->complemento = Input::get('complemento');
        $pagador->bairro = Input::get('bairro');
        $pagador->cidade = Input::get('cidade');
        $pagador->estado = Input::get('estado');
        $pagador->cep = Input::get('cep');
        $pagador->insc_municipal = Input::get('insc_municipal');
        $pagador->insc_estadual = Input::get('insc_estadual');
        $pagador->observacao = Input::get('observacao');
        $pagador->save();

        Session::flash('success', $msg);
        return Redirect::to('/cliente/pagadores/'.$id_cliente.'#tab_pagadores');
    }

    public function getPagadordel($id_cliente, $id_pagador){
        \Pagador::where('id_pagador','=', $id_pagador)->delete();

        Session::flash('success', 'Pagador excluído com sucesso.');
        return Redirect::to('/cliente/pagadores/'.$id_cliente.'#tab_pagadores');
    }

    public function getPagadoresjson($id_cliente){
        $pagadores = \Pagador::where('id_cliente', '=', $id_cliente)
            ->orderBy('razaosocial')
            ->lists('razaosocial', 'id_pagador')
            ;
        if (count($pagadores) > 0){
            $pagadores = array('' => '(selecione)') + $pagadores;
        } else {
            $pagadores = array('' => '(nenhum pagador cadastrado para o cliente)');
        }
        return Response::json($pagadores);
    }
    
    public function getConferencia($area){
    	$data = array();
    	$data['registros'] = \Cliente::ativos()->conferencia($area)->get();
    	$titulos = array(
    			"franquiacustodia" => 'Clientes sem franquia de custódia' 
    			,"descontocustodiapercentual" => 'Clientes com desconto custódia percentual' 
    			,"franquiamovimentacao" => 'Clientes com franquia de movimentação sobre o total de caixas (%)' 
    			,"descontocustodialimite" => 'Clientes com limite de caixas para o desconto de custódia' 
    	);
    	$titulos_campo = array(
    			"franquiacustodia" => 'Franquia'
    			,"descontocustodiapercentual" => 'Desconto (%)'
    			,"franquiamovimentacao" => 'Franquia (%)'
    			,"descontocustodialimite" => 'Limite'
    	);
    	 
    	
    	$data['titulo'] = $titulos[$area];
    	$data['titulo_campo'] = $titulos_campo[$area];
    	$data['nomecampo'] = $area;
    	
    	return View::make('cliente/conferencia_contratos', $data);   	 
    }
    
    public function getExcel(){
    	$clientes = \Cliente::all();
    	$data = array('clientes' => $clientes);
    	$html= View::make('cliente/index_excel', $data);

    	$conteudo =iconv("UTF8", "ISO-8859-1",  $html);
    	return Response::make($conteudo, '200', array(
    			'Content-Type' => 'application/vnd.ms-excel',
    			'Content-Disposition' => 'attachment; filename="clientes.xls"'
    	));
    	 
    }
    
    public function getComparavalores($id_tipoproduto){
    	$sql = "select cliente.razaosocial, cliente.nomefantasia, cliente.id_cliente, date_format(cliente.iniciocontrato, '%d/%m/%Y') iniciocontrato_fmt, cliente.valorminimofaturamento
				, tipoproduto.descricao, tipoprodcliente.valor_contrato, tipoprodcliente.valor_urgencia, tipoproduto.id_tipoproduto, tipoprodcliente.minimo
    			, (select count(distinct id_caixa) from documento where primeira_entrada <= now() and id_cliente = cliente.id_cliente) qtd_caixas
    			, (select count(distinct d.id_caixa)
    				from documento d,  reserva r , reservadesmembrada rd
    			    where d.id_cliente = cliente.id_cliente 
    				and r.id_cliente = cliente.id_cliente
    				and rd.id_caixa = d.id_caixa
    				and r.id_reserva = rd.id_reserva
    				and d.primeira_entrada is null
    				and datediff(now(), r.data_reserva) > coalesce(cliente.franquiacustodia, 0)
    				and coalesce(cliente.franquiacustodia, 0) > 0"
                . " ) qtd_carencia
				from tipoprodcliente, cliente, tipoproduto
				where tipoprodcliente.id_tipoproduto = ".$id_tipoproduto."
    			and cliente.fl_ativo = 1
				and tipoproduto.id_tipoproduto = tipoprodcliente.id_tipoproduto
				and cliente.id_cliente = tipoprodcliente.id_cliente";
    	$clientes = \DB::select(\DB::raw($sql));
    	
    	switch($id_tipoproduto){
    		case 1:
    			$titulo = 'FRETE';
    			break;
    		case 2:
    			$titulo = 'CARTONAGEM';
    			break;
    		case 3:
    			$titulo = 'ARMAZENAMENTO';
    			break;
    		case 4:
    			$titulo = 'MOVIMENTAÇÃO';
    			break;
    		case 5:
    			$titulo = 'EXPURGO';
    			break;    				 
    	}
    	$data = array('clientes' => $clientes, 'titulo' => $titulo, 'id_tipoproduto' => $id_tipoproduto);
    	 
    	$html= View::make('cliente/conferencia_valores', $data);
    	
    	$conteudo =iconv("UTF8", "ISO-8859-1",  $html);
    	return Response::make($conteudo, '200', array(
    			'Content-Type' => 'application/vnd.ms-excel',
    			'Content-Disposition' => 'attachment; filename="valores_clientes_'.$titulo.'.xls"'
    	));
    }

    public function getVerificautf8(){
        set_time_limit(0);
        print "<br/>Iniciou";
        $clientes = Cliente::orderby('razaosocial')->get();
        foreach($clientes as $cliente){
            $nomefantasia = false;
            $razaosocial = false;
            if (!$this->is_utf8($cliente->razaosocial)){
                $razaosocial = true;
            }
            if (!$this->is_utf8($cliente->nomefantasia)){
                $nomefantasia = true;
            }
            if($razaosocial || $nomefantasia){
                print '<br/>Cliente com caracteres inválidos: ';
                if($razaosocial){
                    print ' razaosocial='.$cliente->razaosocial;
                }
                if($nomefantasia){
                    print ' nomefantasia='.$cliente->$nomefantasia;
                }
            }
        }
        print "<br/>Finalizou";
    }

    function is_utf8($string)
	{
		if ($string === mb_convert_encoding(mb_convert_encoding($string, "UTF-32", "UTF-8"), "UTF-8", "UTF-32")) {
			return true;
		} else {
			if ($this->ignore_invalid_utf8) {
				$string = mb_convert_encoding(mb_convert_encoding($string, "UTF-32", "UTF-8"), "UTF-8", "UTF-32");
				return true;
			} else {
				return false;
			}
		}
	}

}