<?php
use Memodoc\Utilitarios;

class LoteRpsController extends BaseController {
    public $svcLoteRps;
    
    public function __construct(){
        $this->svcLoteRps = new \Memodoc\Servicos\ServicosLoteRps;
    }
    public function getIndex() {
        return $this->getList();
    }

    public function postIndex(){

    }

    public function getEditar($id_lote_rps){
        $lote_rps = \LoteRps::find($id_lote_rps);
        if($lote_rps->id_periodo_faturamento > 0){
            $perfat = \PeriodoFaturamento::find($lote_rps->id_periodo_faturamento);
        } else {
            $perfat = null;
        }
        $notas_incluidas = \NotaFiscal::dolote($id_lote_rps)
            ->leftjoin('faturamento', 'faturamento.id_faturamento', '=', 'nota_fiscal.id_faturamento')
            ->leftjoin('cliente', 'cliente.id_cliente', '=', \DB::raw('coalesce(nota_fiscal.id_cliente, faturamento.id_cliente)'))
            ->leftjoin('tipo_faturamento', 'tipo_faturamento.id_tipo_faturamento', '=', 'cliente.id_tipo_faturamento')
            ->select(['nota_fiscal.*', 'cliente.razaosocial', 'cliente.id_cliente', 'cliente.nomefantasia', 'cliente.fl_emissaonf_suspensa', 'cliente.dia_vencimento', 'tipo_faturamento.descricao' ])
            ->orderBy('cliente.razaosocial')
            ->get();


        $notas_fora = \NotaFiscal::whereNull('id_lote_rps')
            ->join('faturamento', 'faturamento.id_faturamento', '=', 'nota_fiscal.id_faturamento')
            ->join('cliente', 'cliente.id_cliente', '=', 'faturamento.id_cliente')
            ->leftjoin('tipo_faturamento', 'tipo_faturamento.id_tipo_faturamento', '=', 'cliente.id_tipo_faturamento')
            ->select(['nota_fiscal.*', 'cliente.razaosocial', 'cliente.id_cliente', 'cliente.nomefantasia', 'cliente.fl_emissaonf_suspensa', 'cliente.dia_vencimento', 'tipo_faturamento.descricao' ])
            ->where('faturamento.id_periodo_faturamento', '=', $lote_rps->id_periodo_faturamento)
            ->whereRaw("coalesce(cliente.fl_emissaonf_suspensa, 0) = 0")
            ->orderBy('cliente.razaosocial')
            ->get();

        $notas_suspensas = \NotaFiscal::whereNull('id_lote_rps')
            ->join('faturamento', 'faturamento.id_faturamento', '=', 'nota_fiscal.id_faturamento')
            ->join('cliente', 'cliente.id_cliente', '=', 'faturamento.id_cliente')
            ->leftjoin('tipo_faturamento', 'tipo_faturamento.id_tipo_faturamento', '=', 'cliente.id_tipo_faturamento')
            ->select(['nota_fiscal.*', 'cliente.razaosocial', 'cliente.id_cliente', 'cliente.nomefantasia', 'cliente.fl_emissaonf_suspensa', 'cliente.dia_vencimento', 'tipo_faturamento.descricao' ])
            ->where('faturamento.id_periodo_faturamento', '=', $lote_rps->id_periodo_faturamento)
            ->where("cliente.fl_emissaonf_suspensa", "=", "1")
            ->orderBy('cliente.razaosocial')
            ->get();

        $data = [
            'lote_rps'              => $lote_rps,
            'perfat'              => $perfat,
            'notas_incluidas'     => $notas_incluidas,
            'notas_fora'     => $notas_fora,
            'notas_suspensas'     => $notas_suspensas,
            'memodoc' => \Cliente::memodoc()->first(),
            
        ];
        return View::make('rps.detalhe', $data);
    }

    public function getGerarnotas($id_lote_rps){

    }

    public function postNovodefaturamento(){
        $id_perfat = Input::get('id_periodo_faturamento');
        $dt_emissao = Input::get('lrps_dt_emissao');
        $dt_geracao = \Carbon::now('America/Sao_Paulo')->format('Y-m-d H:i:s');
        $dia_vencimento = Input::get('dia_vencimento');
        $id_tipo_faturamento = Input::get('id_tipo_faturamento');
        // Cria um novo
        try{
            $loterps = $this->svcLoteRps->cria_novo_de_faturamento($id_perfat, $dt_emissao, $dt_geracao, $dia_vencimento, $id_tipo_faturamento, \Auth::user()->id_usuario);
            Session::flash('success', "Nova remessa criada com sucesso");
            return Redirect::to("/loterps/editar/".$loterps->id_lote_rps);
    
        } catch(\Exception $e){
            Input::flash();
            Session::flash('errors', $e->getMessage());
            return Redirect::to("/loterps");
        }
    }
    
    public function postAdicionarfaturamento(){

    }

    public function postAdicionarnotaavulsa(){

    }

    public function getRemovernota($id_nota_fiscal){
        try{
            $nota = \NotaFiscal::find($id_nota_fiscal);
            $id_lote_rps = $nota->id_lote_rps;
            $nota->id_lote_rps = null;
            $nota->save();
            $ret['retcode'] = 0;
            $ret['msg'] = 'Ok';
        } catch(\Exception $e){
            $ret['retcode'] = 1;
            $ret['msg'] = $e->getMessage();
        }
        return json_encode($ret);
    }

    public function getAdicionarnota($id_lote_rps, $id_nota_fiscal){
        $nota = \NotaFiscal::find($id_nota_fiscal);
        $nota->id_lote_rps = $id_lote_rps;
        $nota->save();
        return Redirect::to('/loterps/editar/'.$id_lote_rps);
    }

    public function postAdicionarpendentes(){
    }

    public function getList(){
        $lotes_rps = \LoteRps::orderBy('lrps_dt_geracao', 'desc')->get();
        $perfats = \PeriodoFaturamento::orderBy('ano', 'desc')
                        ->orderBy('mes', 'desc')
                        ->lists(DB::raw("concat(mes, '/', ano, ' - de ', date_format(dt_inicio, '%d/%m/%y'), ' a ', date_format(dt_fim, '%d/%m/%Y') )"), "id_periodo_faturamento");
        $dias_vencimento = array("" => '(todos)') + \DiaVencimento::orderBy('dia')->lists('dia', 'dia');
        $tipos_faturamento = array("" => '(todos)') + \TipoFaturamento::orderBy('descricao')->lists('descricao', 'id_tipo_faturamento');
        $data = [
            'lotes_rps'     => $lotes_rps,
            'perfats'       => $perfats,
            'dias_vencimento'   => $dias_vencimento,
            'tipos_faturamento' => $tipos_faturamento
        ];
        return View::make('rps.listar', $data);
    }

    public function getQtdpendentes($id_periodo_faturamento, $dia_vencimento, $id_tipo_faturamento){

    }

    public function postExcluir(){
        $id_lote_rps = Input::get('id_lote_rps');
        try{
            $this->svcLoteRps->excluir($id_lote_rps);
            $msg = "Lote excluido com sucesso";
            $retcode = 0;
        } catch(\Exception $e){
            $retcode = -1;
            $msg = "Não foi possível excluir o lote: ".$e->getMessage();
        }
        return json_encode(['retcode' => $retcode, 'msg' => $msg]);
    }

    public function getGerararquivo($id_lote_rps){
        $lote = \LoteRps::find($id_lote_rps);
        $arquivo = new Utilitarios\ArquivoLoteRps($lote);
        $caminho = $arquivo->gerar();

        $headers = array(
            'Content-type' => 'application/txt',
            'Content-Description' => 'File Transfer',
            'Content-Transfer-Encoding' => 'binary',
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Pragma' => 'public'
        );
        return Response::download($caminho, 'notas.txt', $headers);
    }
}