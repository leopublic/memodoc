<?php

function pr($arrayOrObj){
     echo '<pre>';
     print_r($arrayOrObj);
     echo '</pre>';
 }

function queryLog(){
    $queries = DB::getQueryLog();
    $last_query = end($queries);
    return $last_query;
 }


function FormSelectValue($model){
    $data = array();
    foreach($model as $fields){
       $data[] = $fields->getKey();
    }
    return $data;
}

/**
 * FormSelect é adapter para o Form::select()
 * @param type $model
 * @param type $name
 * @return array()
 */
/*
function FormSelect($model, $name = 'nome'){
   $data = array();
   if($model->count() > 0){
       $data[''] = 'Selecione';
       foreach($model as $fields){
           if(!is_array($name)){
               foreach($fields->getAttributes() as $fieldname => $field){
                 if($fieldname == $name){
                     $data[$fields->getKey()] = $field;
                 }
               }
           }else{
                $parts = '';
                foreach($name as $part){
                    $parts .= ' - '.$fields->$part;
                }
                $data[$fields->getKey()] = $parts;
           }
       }

   }
   return $data;
} */

        /**
         * Transform model in adapter Form::select
         * @link http://laravel.com/docs/html#drop-down-lists DropDown-lists
         * @param type $model
         * @param type $name
         * @return type array
         */
        function FormSelect($model, $name = 'nome'){
            $data = array();
            if($model->count() > 0){
                $data[''] = 'Selecione';
                foreach($model as $fields){
                    $fields_attributes = $fields->getAttributes();
                    if(is_array($name)){
                        $name_parts = '';
                        foreach($name as $parts){
                            if($name_parts){
                                $name_parts .= ' ('.$fields_attributes[$parts].') ';
                            }else{
                                $name_parts = $fields_attributes[$parts];
                            }
                        }
                        $data[$fields->getKey()] = $name_parts;
                    }else{
                        foreach($fields_attributes as $fieldname => $field){
                            if($fieldname == $name){
                                $data[$fields->getKey()] = $field;
                            }
                        }
                    }
                }
            }
            return $data;
        }
