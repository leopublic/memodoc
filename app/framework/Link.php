<?php
class Link
{
    protected $nome;
    protected $url;
    protected $target;
    protected $title;

    public function __construct($nome, $url, $target = "_blank", $title = "")
    {
        $this->nome     = $nome;
        $this->url      = $url;
        $this->target   = $target;
        $this->title    = $title;
    }

    public function nome()
    {
        return $this->nome;
    }

    public function url()
    {
        return $this->url;
    }

    public function target()
    {
        return $this->target;
    }

    public function title()
    {
        return $this->title;
    }
}