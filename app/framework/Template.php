<?php
class Template{
	protected $campos;
	protected $subtitulo;

	public function __construct()
	{
		$this->campos = array();
		$this->subtitulo = "";
	}

	public function AdicioneCampo($pCampo)
	{
		$nome = $pCampo->get_name();
		$this->campos[$nome] = $pCampo;
	}

	public function get_campos(){
		return $this->campos;
	}
}