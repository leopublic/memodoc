<?php
class PreencheCombo{
	public function cliente()
	{
		return Cliente::ativos()
					->select(array(DB::raw("concat(coalesce(razaosocial, ''), ' (', coalesce(nomefantasia, '-') , ')' ) as nome "), 'id_cliente'))
					->orderBy('razaosocial')
					->get()
					->lists('nome', 'id_cliente')
					;
	}
}