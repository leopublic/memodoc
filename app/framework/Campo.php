<?php
/**
 *
 */
class Campo{
	protected $atributos;
	protected $disponivel;
	protected $subTitulo;
	protected $name;
	protected $value;

	public function __construct($patributos = null, $psubTitulo = null)
	{
		$this->atributos = array();
		if($patributos != null){
			if(is_array($patributos)){
				$this->atributos = array_merge($this->atributos, $patributos);
			}
		}
		$this->subTitulo = $psubTitulo;
	}

	public function set_Atributo($pNome, $pValor)
	{
		$this->atributos[$pNome] = $pValor;
	}

	public function get_atributo($pnome)
	{
		$ret = '';
		if(isset($this->atributos[$pnome])){
			$ret = $this->atributos[$pnome];
		}
		return $ret;
	}

	public function get_label()
	{
		return $this->get_atributo('label');
	}

	public function get_name()
	{
		return $this->get_atributo('name');
	}

	public function get_subTitulo()
	{
		return $this->subTitulo;
	}

	public function set_name($pValor, $pGerarId = true)
	{
		$this->SetAtributo("name", $pValor);
		if($pGerarId){
			$this->SetAtributo("id", $pValor);			
		}
	}

	public function set_class($pValor)
	{
		$this->SetAtributo("class", $pValor);
	}

	public function set_title($pValor)
	{
		$this->SetAtributo("title", $pValor);
	}

	public function set_type($pValor)
	{
		$this->SetAtributo("type", $pValor);
	}

	public function set_value($pValor)
	{
		$this->value = $pValor;
	}
	
	public function set_disponivel($Valor = true)
	{
		$this->disponivel = $pValor;
	}
	
	public function set_indisponivel()
	{
		$this->disponivel = false;
	}

	public function get_type()
	{
		if(isset($this->atributos['type'])){
			return $this->atributos['type'];
		}
		else{
			return null;
		}
	}

	public function get_atributos_html()
	{
		$attr = '';
		foreach($this->atributos as $nome => $valor){
			$attr = " ".$nome.'="'.$valor.'"';
		}
		return $attr;
	}
	/**
	 * Cria um novo campo text
	 */
	public static function text($pName)
	{
		$campo = new Campo();
		$campo->set_name($pName);
		$campo->set_type('text');
		return $campo;
	}

	public function html()
	{
		switch($this->get_type()){
			case "text":
				return Form::text($this->name, $this->value , $this->atributos );
				break;
			case "checkbox":
				return Form::checkbox($this->name, $this->value , $this->atributos );
				break;
			default:
				return Form::text($this->name, $this->value , $this->atributos );
		}
	}
}