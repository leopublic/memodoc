<?php
class Menu
{
    protected $links;
    protected $nome;
    protected $icon;
    protected $cor;

    public function __construct($nome, $icon, $cor = 'silver')
    {
        $this->nome = $nome;
        $this->icon = $icon;
        $this->links = array();
    }

    public function adicionaLink($link)
    {
        $this->links[] = $link;
    }

    public function nome()
    {
        return $this->nome;
    }

    public function links()
    {
        return $this->links;
    }

    public function icon()
    {
        return $this->icon;
    }

    public function cor()
    {
        return $this->cor;
    }
}