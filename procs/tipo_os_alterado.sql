delimiter ; 
drop procedure if exists tipoemprestimo_alterado;
delimiter $$
CREATE procedure tipoemprestimo_alterado (id_atual int, id_novo int, INOUT ret varchar(1000))
  BEGIN
	DECLARE x_desc varchar(1000);
	DECLARE x_old_desc varchar(1000);
	set x_desc = '';
	set x_old_desc = '';
	IF ret != '' THEN
		set @virg = ', ';
	ELSE
		set @virg = '';
	END IF;
	IF ifnull(id_atual, 0) <> ifnull(id_novo, 0) THEN 
		IF coalesce(id_atual, 0) > 0 THEN
			IF id_atual = 0 THEN
				set x_old_desc = 'Entrega Memodoc';
			ELSEIF id_atual = 1 THEN
				set x_old_desc = 'Consulta no local';
			ELSEIF id_atual = 2 THEN
				set x_old_desc = 'Retirado pelo cliente';
			END IF;
		ELSE
			set x_old_desc = '';
		END IF;
		IF coalesce(id_novo, 0) > 0 THEN
			IF id_novo = 0 THEN
				set x_desc = 'Entrega Memodoc';
			ELSEIF id_novo = 1 THEN
				set x_desc = 'Consulta no local';
			ELSEIF id_novo = 2 THEN
				set x_desc = 'Retirado pelo cliente';
			END IF;
		ELSE
			set x_desc = '';
		END IF;

		SET ret= concat(ret, @virg, 'tipo de os alterado de ', coalesce(concat('"', x_old_desc, '"'), '(nulo)'), ' para ', coalesce(concat('"', x_desc, '"'), '(nulo)')) ;
	END IF;
END
$$
