delimiter ; 
drop procedure if exists entrega_pela_alterado;
delimiter $$
CREATE procedure entrega_pela_alterado (id_atual int, id_novo int, INOUT ret varchar(1000))
  BEGIN
	DECLARE x_desc varchar(1000);
	DECLARE x_old_desc varchar(1000);
	set x_desc = '';
	set x_old_desc = '';
	IF ret != '' THEN
		set @virg = ', ';
	ELSE
		set @virg = '';
	END IF;
	IF coalesce(id_atual, -1) <> coalesce(id_novo, -1) THEN 
		IF coalesce(id_atual, -1) > -1 THEN
			IF id_atual = 0 THEN
				set x_old_desc = 'Manhã';
			ELSEIF id_atual = 1 THEN
				set x_old_desc = 'Tarde';
			END IF;
		ELSE
			set x_old_desc = null;
		END IF;
		IF coalesce(id_novo, -1) > -1 THEN
			IF id_novo = 0 THEN
				set x_desc = 'Manhã';
			ELSEIF id_novo = 1 THEN
				set x_desc = 'Tarde';
			END IF;
		ELSE
			set x_desc = null;
		END IF;

		SET ret= concat(ret, @virg, 'turno de entrega de ', coalesce(concat('"', x_old_desc, '"'), '(nulo)'), ' para ', coalesce(concat('"', x_desc, '"'), '(nulo)')) ;
	END IF;
END
$$
