DELIMITER ;
drop trigger if exists documento_update_log  ;
DELIMITER $$
CREATE TRIGGER  documento_update_log  AFTER UPDATE ON  documento  FOR EACH ROW
BEGIN
  	DECLARE msg varchar(2000);
    
  	SET msg = '';
	call varchar_alterado (OLD.id_caixa, NEW.id_caixa, 'ID alterado de ', msg);
	call varchar_alterado (OLD.id_reserva, NEW.id_reserva, 'reserva alterado de ', msg);
	call varchar_alterado (OLD.id_tipodocumento, NEW.id_tipodocumento, 'tipo de documento alterado de ', msg);
	call varchar_alterado (OLD.id_cliente, NEW.id_cliente, 'cliente alterado de ', msg);
	call varchar_alterado (OLD.id_caixapadrao, NEW.id_caixapadrao, 'refer�ncia alterada de ', msg);
	call varchar_alterado (OLD.id_box, NEW.id_box, 'box alterado de ', msg);
	call varchar_alterado (OLD.id_item, NEW.id_item, 'item alterado de ', msg);
	call varchar_alterado (OLD.id_expurgo, NEW.id_expurgo, 'expurgo alterado de ', msg);
	call varchar_alterado (OLD.id_departamento, NEW.id_departamento, 'departamento alterado de ', msg);
	call varchar_alterado (OLD.id_setor, NEW.id_setor, 'setor alterado de ', msg);
	call varchar_alterado (OLD.id_centro, NEW.id_centro, 'centro de custos alterado de ', msg);
	call varchar_alterado (OLD.titulo, NEW.titulo, 'titulo alterado de ', msg);
	call varchar_alterado (OLD.migrada_de, NEW.migrada_de, 'migrada de alterada de ', msg);
	call date_alterado (OLD.inicio, NEW.inicio, 'inicio alterada de ', msg);
	call date_alterado (OLD.fim, NEW.fim, 'fim alterada de ', msg);
	call varchar_alterado (OLD.expurgo_programado, NEW.expurgo_programado, 'expurgo programado alterado de ', msg);
	call date_alterado (OLD.expurgarem, NEW.expurgarem, 'expurgar em alterado de ', msg);
	call varchar_alterado (OLD.conteudo, NEW.conteudo, 'conteudo alterado de ', msg);
	call varchar_alterado (OLD.usuario, NEW.usuario, 'usuario alterado de ', msg);
	call varchar_alterado (OLD.emcasa, NEW.emcasa, 'em casa alterado de ', msg);
	call datetime_alterado (OLD.primeira_entrada, NEW.primeira_entrada, 'primeira entrada alterado de ', msg);
	call date_alterado (OLD.data_digitacao, NEW.data_digitacao, 'data de digita��o alterado de ', msg);
	call varchar_alterado (OLD.expurgada_em, NEW.expurgada_em, 'expurgado em alterado de ', msg);
	call varchar_alterado (OLD.itemalterado, NEW.itemalterado, 'item alterado alterado de ', msg);
	call varchar_alterado (OLD.planilha_eletronica, NEW.planilha_eletronica, 'planilha eletronica alterado de ', msg);
	call varchar_alterado (OLD.request_recall, NEW.request_recall, 'request recall alterado de ', msg);
	call varchar_alterado (OLD.nume_inicial, NEW.nume_inicial, 'n�mero inicial alterado de ', msg);
	call varchar_alterado (OLD.nume_final, NEW.nume_final, 'n�mero final alterado de ', msg);
	call varchar_alterado (OLD.alfa_inicial, NEW.alfa_inicial, 'alfa inicial alterado de ', msg);
	call varchar_alterado (OLD.alfa_final, NEW.alfa_final, 'alfa final alterado de ', msg);
	call varchar_alterado (OLD.fl_revisado, NEW.fl_revisado, 'situa��o de revis�o alterado de ', msg);
	call varchar_alterado (OLD.id_usuario_revisao, NEW.id_usuario_revisao, 'usu�rio revis�o alterado de ', msg);
	call datetime_alterado (OLD.dt_revisao, NEW.dt_revisao, 'data de revis�o alterado de ', msg);
	call varchar_alterado (OLD.id_usuario_liberacao, NEW.id_usuario_liberacao, 'usu�rio libera��o alterado de ', msg);
	call datetime_alterado (OLD.dt_liberacao, NEW.dt_liberacao, 'data libera��o alterado de ', msg);

    IF msg <> '' THEN
		SET msg = concat('Altera��es: ', msg);
		insert into log_documento(id_documento, id_usuario, evento, descricao, created_at, updated_at) values (NEW.id_documento, NEW.id_usuario_alteracao, NEW.evento, msg, now(), now());
    END IF;
END
$$
DELIMITER ;