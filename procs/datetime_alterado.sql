
delimiter ; 
drop procedure if exists datetime_alterado;
delimiter $$
CREATE procedure datetime_alterado (atual datetime, novo datetime, titulo varchar(1000), INOUT ret varchar(1000))
  BEGIN
	IF ret != '' THEN
		set @virg = ', ';
	ELSE
		set @virg = '';
	END IF;
	IF coalesce(atual, '') <> coalesce(novo, '') THEN 
		SET ret= concat(@virg, ret, ' ', titulo, coalesce(concat('"', date_format(atual, '%d/%m/%Y %H:%i:%s'), '"'), '(nulo)'), ' para ', coalesce(concat('"',  date_format(novo, '%d/%m/%Y %H:%i:%s'), '"'), '(nulo)')) ;

	END IF;
END
$$
