DELIMITER ;
drop trigger if exists os_cria_log  ;
DELIMITER $$
CREATE TRIGGER  os_cria_log  AFTER INSERT ON  os  FOR EACH ROW
BEGIN
  	DECLARE msg varchar(2000);
    
  	SET msg = '';
	call varchar_alterado (null, NEW.responsavel, 'responsável alterado de ', msg);
	call depcliente_alterado (null, NEW.id_departamento, msg);
	call endeentrega_alterado (null, NEW.id_endeentrega, msg);
	call tipoemprestimo_alterado (null, NEW.tipodeemprestimo, msg);
	call entrega_pela_alterado (null, NEW.entrega_pela, msg);
	call varchar_alterado (null, coalesce(date_format(NEW.solicitado_em, '%d/%m/%Y'), null), 'data de solicitação de ', msg);
	call varchar_alterado (null, coalesce(date_format(NEW.entregar_em, '%d/%m/%Y'), null), 'data de entrega de ', msg);
	call varchar_alterado (null, NEW.id_os, 'número da OS de ', msg);
	call varchar_alterado (null, NEW.qtdenovascaixas, ' qtd novas caixas com endereçamento de ', msg);
	call varchar_alterado (null, NEW.apanhanovascaixas, ' qtd caixas a retirar (novas) de ', msg);
	call varchar_alterado (null, NEW.apanhacaixasemprestadas, ' qtd caixas a retirar (emprestadas) de ', msg);
	call varchar_alterado (null, NEW.qtd_caixas_retiradas_cliente, ' qtd caixas retiradas (estimativa OS) de ', msg);
	call varchar_alterado (null, NEW.qtd_cartonagens_cliente, ' qtd cartonagem adicional de ', msg);
	call varchar_alterado (null, NEW.cobrafrete, ' cobrar frete de ', msg);
	call varchar_alterado (null, NEW.urgente, ' urgente de ', msg);
	call varchar_alterado (null, NEW.naoecartonagemcortesia, ' cobrar cartonagem de ', msg);
	call varchar_alterado (null, NEW.cancelado, ' cancelada de ', msg);
	call varchar_alterado (null, NEW.id_reserva, ' id da reserva associada de ', msg);
	call varchar_alterado (null, NEW.id_faturamento, ' id do faturamento associado de ', msg);
	call varchar_alterado (null, NEW.valorfrete, ' valor frete de ', msg);
	call varchar_alterado (null, NEW.valormovimento, ' valor movimento de ', msg);
	call varchar_alterado (null, NEW.observacao, ' observação de ', msg);

    IF msg <> '' THEN
		SET msg = concat('Alterações: ', msg);
		insert into log_os(id_os_chave, id_usuario, evento, descricao, created_at, updated_at) values (NEW.id_os_chave, NEW.id_usuario_alteracao, NEW.evento, msg, now(), now());
    END IF;
END
$$
DELIMITER ;