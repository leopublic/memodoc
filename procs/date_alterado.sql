
delimiter ; 
drop procedure if exists date_alterado;
delimiter $$
CREATE procedure date_alterado (atual date, novo date, titulo varchar(1000), INOUT ret varchar(1000))
  BEGIN
	IF ret != '' THEN
		set @virg = ', ';
	ELSE
		set @virg = '';
	END IF;
	IF coalesce(atual, '') <> coalesce(novo, '') THEN 
		SET ret= concat(@virg, ret, ' ', titulo, coalesce(concat('"', date_format(atual, '%d/%m/%Y'), '"'), '(nulo)'), ' para ', coalesce(concat('"',  date_format(novo, '%d/%m/%Y'), '"'), '(nulo)')) ;

	END IF;
END
$$
