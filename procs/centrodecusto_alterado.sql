delimiter ; 
drop procedure if exists centrodecusto_alterado;
delimiter $$
CREATE procedure centrodecusto_alterado (id_atual int, id_novo int, INOUT ret varchar(1000))
  BEGIN
	DECLARE x_desc varchar(1000);
	DECLARE x_old_desc varchar(1000);
	set x_desc = '';
	set x_old_desc = '';
	IF ret != '' THEN
		set @virg = ', ';
	ELSE
		set @virg = '';
	END IF;
	IF ifnull(id_atual, 0) <> ifnull(id_novo, 0) THEN 
		IF coalesce(id_atual, 0) > 0 THEN
			select nome into x_old_desc from centrodecusto where id_centrodecusto = id_atual;
		ELSE
			set x_old_desc = '';
		END IF;
		IF coalesce(id_novo, 0) > 0 THEN
			select nome into x_desc from centrodecusto where id_centrodecusto = id_novo;
		ELSE
			set x_desc = '';
		END IF;

		SET ret= concat(ret, @virg, 'centro de custo alterado de ', coalesce(concat('"', x_old_desc, '"'), '(nulo)'), ' para ', coalesce(concat('"', x_desc, '"'), '(nulo)')) ;
	END IF;
END
$$
