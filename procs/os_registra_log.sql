DELIMITER ;
drop trigger if exists os_registra_log  ;
DELIMITER $$
CREATE TRIGGER `os_registra_log` AFTER UPDATE ON `os` FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
  	DECLARE msg varchar(2000);
    
  	SET msg = '';
	call varchar_alterado (OLD.responsavel, NEW.responsavel, 'responsável alterado de ', msg);
	call depcliente_alterado (OLD.id_departamento, NEW.id_departamento, msg);
	call endeentrega_alterado (OLD.id_endeentrega, NEW.id_endeentrega, msg);
	call tipoemprestimo_alterado (OLD.tipodeemprestimo, NEW.tipodeemprestimo, msg);
	call entrega_pela_alterado (OLD.entrega_pela, NEW.entrega_pela, msg);
	call varchar_alterado (coalesce(date_format(OLD.solicitado_em, '%d/%m/%Y'), null), coalesce(date_format(NEW.solicitado_em, '%d/%m/%Y'), null), 'data de solicitação de ', msg);
	call varchar_alterado (coalesce(date_format(OLD.entregar_em, '%d/%m/%Y'), null), coalesce(date_format(NEW.entregar_em, '%d/%m/%Y'), null), 'data de entrega de ', msg);
	call varchar_alterado (OLD.id_os, NEW.id_os, 'número da OS de ', msg);
	call varchar_alterado (OLD.qtdenovascaixas, NEW.qtdenovascaixas, ' qtd novas caixas com endereçamento de ', msg);
	call varchar_alterado (OLD.apanhanovascaixas, NEW.apanhanovascaixas, ' qtd caixas a retirar (novas) de ', msg);
	call varchar_alterado (OLD.apanhacaixasemprestadas, NEW.apanhacaixasemprestadas, ' qtd caixas a retirar (emprestadas) de ', msg);
	call varchar_alterado (OLD.qtd_caixas_retiradas_cliente, NEW.qtd_caixas_retiradas_cliente, ' qtd caixas retiradas (estimativa OS) de ', msg);
	call varchar_alterado (OLD.qtd_cartonagens_cliente, NEW.qtd_cartonagens_cliente, ' qtd cartonagem adicional de ', msg);
	call varchar_alterado (OLD.cobrafrete, NEW.cobrafrete, ' cobrar frete de ', msg);
	call varchar_alterado (OLD.urgente, NEW.urgente, ' urgente de ', msg);
	call varchar_alterado (OLD.naoecartonagemcortesia, NEW.naoecartonagemcortesia, ' cobrar cartonagem de ', msg);
	call varchar_alterado (OLD.cancelado, NEW.cancelado, ' cancelada de ', msg);
	call varchar_alterado (OLD.id_reserva, NEW.id_reserva, ' id da reserva associada de ', msg);
	call varchar_alterado (OLD.id_faturamento, NEW.id_faturamento, ' id do faturamento associado de ', msg);
	call varchar_alterado (OLD.valorfrete, NEW.valorfrete, ' valor frete de ', msg);
	call varchar_alterado (OLD.valormovimento, NEW.valormovimento, ' valor movimento de ', msg);
	call varchar_alterado (OLD.observacao, NEW.observacao, ' observação de ', msg);

    IF msg <> '' THEN
		SET msg = concat('Alterações: ', msg);
		insert into log_os(id_os_chave, id_status_os, id_usuario, evento, descricao, created_at, updated_at) values (OLD.id_os_chave, NEW.id_status_os, NEW.id_usuario_alteracao, NEW.evento, msg, now(), now());
    END IF;
END
$$
DELIMITER ;