DELIMITER ;
drop trigger if exists documento_insert_log;
DELIMITER $$
CREATE TRIGGER  documento_insert_log  AFTER INSERT ON  documento  FOR EACH ROW
BEGIN
  	DECLARE msg varchar(2000);
    
  	SET msg = '';
	call varchar_alterado (null, NEW.id_caixa, 'ID alterado de ', msg);
	call varchar_alterado (null, NEW.id_reserva, 'reserva alterado de ', msg);
	call varchar_alterado (null, NEW.id_tipodocumento, 'tipo de documento alterado de ', msg);
	call varchar_alterado (null, NEW.id_cliente, 'cliente alterado de ', msg);
	call varchar_alterado (null, NEW.id_caixapadrao, 'referência alterada de ', msg);
	call varchar_alterado (null, NEW.id_box, 'box alterado de ', msg);
	call varchar_alterado (null, NEW.id_item, 'item alterado de ', msg);
	call varchar_alterado (null, NEW.id_expurgo, 'expurgo alterado de ', msg);
	call varchar_alterado (null, NEW.id_departamento, 'departamento alterado de ', msg);
	call varchar_alterado (null, NEW.id_setor, 'setor alterado de ', msg);
	call varchar_alterado (null, NEW.id_centro, 'centro de custos alterado de ', msg);
	call varchar_alterado (null, NEW.titulo, 'titulo alterado de ', msg);
	call varchar_alterado (null, NEW.migrada_de, 'migrada de alterada de ', msg);
	call date_alterado (null, NEW.inicio, 'inicio alterada de ', msg);
	call date_alterado (null, NEW.fim, 'fim alterada de ', msg);
	call varchar_alterado (null, NEW.expurgo_programado, 'expurgo programado alterado de ', msg);
	call date_alterado (null, NEW.expurgarem, 'expurgar em alterado de ', msg);
	call varchar_alterado (null, NEW.conteudo, 'conteudo alterado de ', msg);
	call varchar_alterado (null, NEW.usuario, 'usuario alterado de ', msg);
	call varchar_alterado (null, NEW.emcasa, 'em casa alterado de ', msg);
	call datetime_alterado (null, NEW.primeira_entrada, 'primeira entrada alterado de ', msg);
	call date_alterado (null, NEW.data_digitacao, 'data de digitação alterado de ', msg);
	call varchar_alterado (null, NEW.expurgada_em, 'expurgado em alterado de ', msg);
	call varchar_alterado (null, NEW.itemalterado, 'item alterado alterado de ', msg);
	call varchar_alterado (null, NEW.planilha_eletronica, 'planilha eletronica alterado de ', msg);
	call varchar_alterado (null, NEW.request_recall, 'request recall alterado de ', msg);
	call varchar_alterado (null, NEW.nume_inicial, 'número inicial alterado de ', msg);
	call varchar_alterado (null, NEW.nume_final, 'número final alterado de ', msg);
	call varchar_alterado (null, NEW.alfa_inicial, 'alfa inicial alterado de ', msg);
	call varchar_alterado (null, NEW.alfa_final, 'alfa final alterado de ', msg);
	call varchar_alterado (null, NEW.evento, 'evento alterado de ', msg);
	call varchar_alterado (null, NEW.fl_revisado, 'situação de revisão alterado de ', msg);
	call varchar_alterado (null, NEW.id_usuario_revisao, 'usuário revisão alterado de ', msg);
	call datetime_alterado (null, NEW.dt_revisao, 'data de revisão alterado de ', msg);
	call varchar_alterado (null, NEW.id_usuario_liberacao, 'usuário liberação alterado de ', msg);
	call datetime_alterado (null, NEW.dt_liberacao, 'data liberação alterado de ', msg);

    IF msg <> '' THEN
		SET msg = concat('Alterações: ', msg);
		insert into log_documento(id_documento, id_usuario, evento, descricao, created_at, updated_at) values (NEW.id_documento, NEW.id_usuario_alteracao, NEW.evento, msg, now(), now());
    END IF;
END
$$
DELIMITER ;