delimiter ; 
drop procedure if exists varchar_alterado;
delimiter $$
CREATE procedure varchar_alterado (atual varchar(5000), novo varchar(5000), titulo varchar(1000), INOUT ret varchar(1000))
  BEGIN
	IF ret != '' THEN
		set @virg = ', ';
	ELSE
		set @virg = '';
	END IF;
	IF coalesce(atual, '') <> coalesce(novo, '') THEN 
		SET ret= concat(ret, @virg, titulo, coalesce(concat('"', atual, '"'), '(nulo)'), ' para ', coalesce(concat('"', novo, '"'), '(nulo)')) ;
	END IF;
END
$$
