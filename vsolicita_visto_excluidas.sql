drop view if exists vsolicita_visto_com_despesas;
create view vsolicita_visto_com_despesas as 
select solicita_visto.id_solicita_visto
, solicita_visto.nu_solicitacao
, solicita_visto.resc_id
, date_format(solicita_visto.dt_solicitacao, '%d/%m/%Y') dt_solicitacao
, date_format(solicita_visto.dt_cadastro, '%d/%m/%Y') dt_cadastro
, date_format(solicita_visto.soli_dt_liberacao_cobranca, '%d/%m/%Y') soli_dt_liberacao_cobranca
, solicita_visto.soli_vl_cobrado
, solicita_visto.soli_vl_total
, solicita_visto.soli_tx_codigo_servico
, solicita_visto.resc_id resc_id_solicitacao
, solicita_visto.no_solicitador
, coalesce(solicita_visto.soli_tx_solicitante_cobranca, solicita_visto.no_solicitador) soli_tx_solicitante_cobranca
, solicita_visto.nu_empresa
, solicita_visto.nu_embarcacao_projeto
, autorizacao_candidato.nu_candidato
, servico.ID_TIPO_ACOMPANHAMENTO
, servico.no_servico_resumido
, servico.no_servico_resumido_em_ingles
, servico.nu_servico_sankhya
, empresa.no_razao_social
, empresa.fl_despesas_embutidas
, empresa.vl_taxa_administrativa
, embarcacao_projeto.no_embarcacao_projeto
, candidato.nome_completo
, status_cobranca_os.stco_tx_nome
, usuarios.nome nome_responsavel
, date_format(resumo_cobranca.resc_dt_faturamento, '%d/%m/%Y') resc_dt_faturamento_fmt
, resumo_cobranca.resc_nu_numero
, resumo_cobranca.resc_nu_numero_mv
, resumo_cobranca.resc_dt_faturamento
, concat(fornecedor.nome, ' (', fornecedor.forn_tx_fantasia, ')') nome_fornecedor
, 0 as desp_vl_taxa_admnistrativa
, case servico.ID_TIPO_ACOMPANHAMENTO
   when 1 then (select max(nu_processo) from processo_mte where id_solicita_visto = solicita_visto.id_solicita_visto and cd_candidato = autorizacao_candidato.nu_candidato)
   when 2 then (select max(nu_processo) from processo_mte where id_solicita_visto = solicita_visto.id_solicita_visto and cd_candidato = autorizacao_candidato.nu_candidato)
   when 3 then (select max(nu_protocolo) from processo_regcie where id_solicita_visto = solicita_visto.id_solicita_visto and cd_candidato = autorizacao_candidato.nu_candidato)
   when 4 then (select max(nu_protocolo) from processo_prorrog where id_solicita_visto = solicita_visto.id_solicita_visto and cd_candidato = autorizacao_candidato.nu_candidato)
end as nu_processo
from solicita_visto
join servico on servico.NU_SERVICO = solicita_visto.NU_SERVICO  
join autorizacao_candidato on autorizacao_candidato.id_solicita_visto = solicita_visto.id_solicita_visto
join candidato on candidato.NU_CANDIDATO = autorizacao_candidato.NU_CANDIDATO
join empresa on empresa.nu_empresa = solicita_visto.nu_empresa_requerente
join resumo_cobranca on resumo_cobranca.resc_id = solicita_visto.resc_id
left join embarcacao_projeto on embarcacao_projeto.nu_empresa = solicita_visto.nu_empresa_requerente and embarcacao_projeto.NU_EMBARCACAO_PROJETO = solicita_visto.NU_EMBARCACAO_PROJETO_COBRANCA
left join status_cobranca_os on status_cobranca_os.stco_id = solicita_visto.stco_id
left join usuarios on usuarios.cd_usuario = solicita_visto.cd_tecnico
left join fornecedor on fornecedor.codparc = solicita_visto.codparc_fornecedor
union 
select solicita_visto.id_solicita_visto
, solicita_visto.nu_solicitacao
, solicita_visto.resc_id
, date_format(solicita_visto.dt_solicitacao, '%d/%m/%Y') dt_solicitacao
, date_format(solicita_visto.dt_cadastro, '%d/%m/%Y') dt_cadastro
, date_format(solicita_visto.soli_dt_liberacao_cobranca, '%d/%m/%Y') soli_dt_liberacao_cobranca
, despesa.desp_vl_valor as soli_vl_cobrado
, solicita_visto.soli_vl_total
, solicita_visto.soli_tx_codigo_servico
, solicita_visto.resc_id resc_id_solicitacao
, solicita_visto.no_solicitador
, coalesce(solicita_visto.soli_tx_solicitante_cobranca, solicita_visto.no_solicitador) soli_tx_solicitante_cobranca
, solicita_visto.nu_empresa
, solicita_visto.nu_embarcacao_projeto
, despesa.nu_candidato
, '' as ID_TIPO_ACOMPANHAMENTO
, tipo_despesa.nome as no_servico_resumido
, tipo_despesa.nome as no_servico_resumido_em_ingles
, tipo_despesa.id_tipo_despesa_sankhya as nu_servico_sankhya
, empresa.no_razao_social
, empresa.fl_despesas_embutidas
, empresa.vl_taxa_administrativa
, embarcacao_projeto.no_embarcacao_projeto
, candidato.nome_completo
, status_cobranca_os.stco_tx_nome
, usuarios.nome nome_responsavel
, date_format(resumo_cobranca.resc_dt_faturamento, '%d/%m/%Y') resc_dt_faturamento_fmt
, resumo_cobranca.resc_nu_numero
, resumo_cobranca.resc_nu_numero_mv
, resumo_cobranca.resc_dt_faturamento
, concat(fornecedor.nome, ' (', fornecedor.forn_tx_fantasia, ')') nome_fornecedor
, despesa.desp_vl_taxa_admnistrativa
, '' as nu_processo
from despesa
join tipo_despesa on tipo_despesa.id_tipo_despesa = despesa.id_tipo_despesa
join solicita_visto on solicita_visto.id_solicita_visto = despesa.id_solicita_visto
join empresa on empresa.nu_empresa = solicita_visto.nu_empresa_requerente
join resumo_cobranca on resumo_cobranca.resc_id = solicita_visto.resc_id
left join embarcacao_projeto on embarcacao_projeto.nu_empresa = solicita_visto.nu_empresa_requerente and embarcacao_projeto.NU_EMBARCACAO_PROJETO = solicita_visto.NU_EMBARCACAO_PROJETO_COBRANCA
left join candidato on candidato.NU_CANDIDATO = despesa.nu_candidato
left join status_cobranca_os on status_cobranca_os.stco_id = solicita_visto.stco_id
left join usuarios on usuarios.cd_usuario = solicita_visto.cd_tecnico
left join fornecedor on fornecedor.codparc = solicita_visto.codparc_fornecedor
where despesa.desp_dt_inicio is not null 
;